package com.mac.custom.services.report.beans;

import java.sql.Connection;
import java.util.ArrayList;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.hibernate.classic.Session;

import com.mac.custom.bo.MacSetup;
import com.mac.custom.reporting.DailyQuotationsReport;
import com.mac.custom.services.report.interfaces.local.MacReportServicesLocal;
import com.mac.custom.services.report.interfaces.remote.MacReportServicesRemote;
import com.mac.custom.utils.ImpressionJasper;
import com.netappsid.bo.model.Model;
import com.netappsid.erp.server.services.interfaces.local.ResourceEfficiencyServicesLocal;
import com.netappsid.erp.server.services.utils.SetupUtils;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.Loader;
import com.netappsid.services.interfaces.local.LoaderLocal;

@Stateless
public class MacReportServicesBean implements MacReportServicesLocal, MacReportServicesRemote
{
	@EJB
	private LoaderLocal loaderLocal;

	@EJB
	private ResourceEfficiencyServicesLocal resourceEfficiencyLocal;

	@PersistenceContext
	private EntityManager manager;

	private final ServiceLocator<Loader> loaderServiceLocator = new ServiceLocator<Loader>(LoaderBean.class);

	private static final Logger logger = Logger.getLogger(MacReportServicesBean.class);

	@Override
	public ArrayList<Object> exportRapport(String objet, String inParameter) throws Exception
	{

		Connection connection = null;
		connection = ((Session) manager.getDelegate()).connection();
		return ImpressionJasper.genereImpression(inParameter, connection, objet);

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public void runTodaysQuotationPDF() throws Exception
	{
		DailyQuotationsReport dailyQuotationReport = new DailyQuotationsReport();

		MacSetup setup = (MacSetup) Model.getTarget(SetupUtils.INSTANCE.getMainSetup());

		if (setup.getMediaFolder() != null)
		{
			String reportPath = setup.getMediaFolder() + "/devis/push";
			dailyQuotationReport.reportDailyQuotationsAndSendEmails(reportPath);
		}
	}
}