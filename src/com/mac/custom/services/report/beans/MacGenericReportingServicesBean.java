package com.mac.custom.services.report.beans;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;
import reports.NAIDBeanDataSource;

import com.mac.custom.bo.MacQuotation;
import com.mac.custom.services.report.interfaces.local.MacGenericReportingServicesLocal;
import com.mac.custom.services.report.interfaces.remote.MacGenericReportingServicesRemote;
import com.netappsid.bo.model.Model;
import com.netappsid.erp.server.bo.SaleTransactionSalesRep;
import com.netappsid.erp.server.bo.SalesRep;
import com.netappsid.erp.server.bo.Setup;
import com.netappsid.erp.server.services.utils.SetupUtils;
import com.netappsid.services.interfaces.local.LoaderLocal;
import com.netappsid.services.interfaces.local.PersistenceLocal;

@Stateless
public class MacGenericReportingServicesBean implements MacGenericReportingServicesLocal, MacGenericReportingServicesRemote
{
	public static String DATEFORMAT_FR = "dd/MM/yyyy";
	public static String DATEFORMAT_FILE = "yyyyMMdd";

	private static final String SUBREPORT_DIR = "SUBREPORT_DIR";
	private static final String REPORT_LOCALE = "REPORT_LOCALE";

	@EJB
	private LoaderLocal loader;

	@EJB(mappedName = "erp/PersistenceBean/local")
	public PersistenceLocal persistence;

	/**
	 * Genere et enregistre le rapport passe en parametre
	 * 
	 * @param saleOrder
	 *            Document
	 * @param reportName
	 *            Nom du rapport
	 * @param fileName
	 *            Nom du fichier (.pdf)
	 * @return Succes ou echec
	 * @throws Exception
	 *             Exceptions lors du processus
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean generateAndPersistPDF(Model model, String reportName, String fileName, String filePath, String reportsPath, String configsPath)
			throws Exception
	{
		// System.out.println("===== Generate PDF : " + filePath + fileName);

		boolean result = false;
		File directory = new File(filePath);
		File file = new File(filePath + fileName);

		if (!directory.exists())
		{
			// createDirectory(filePath);
			directory.mkdirs();
		}
		if (file.exists())
		{
			// file.delete();
			return true;
		}

		file.createNewFile();

		JasperPrint print = null;
		Locale locale = null;

		try
		{
			JasperReport report = getReport(reportName, reportsPath);

			if (report != null)
			{
				locale = Locale.getDefault();

				HashMap<String, Object> parameters = generateReportParameters(locale, reportsPath);

				try
				{

					print = JasperFillManager.fillReport(report, parameters, new NAIDBeanDataSource(model));

					createPDF(print, file);
					result = true;
				}
				catch (Exception e)
				{
					file.delete();
					throw new Exception("Impossible de generer l'edition: " + reportName);
				}
			}
			else
			{
				throw new Exception("Impossible de trouver le rapport: " + reportName);
			}
		}
		finally
		{}

		return result;
	}

	/**
	 * Creation du pdf base sur le JasperPrint
	 * 
	 * @param print
	 *            JasperPrint
	 * @param file
	 *            Fichier de sortie
	 * @return Fichier de sortie
	 */
	@Override
	public File createPDF(JasperPrint print, File file) throws Exception
	{
		JRPdfExporter exporter = new JRPdfExporter();

		exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
		exporter.setParameter(JRExporterParameter.OUTPUT_FILE, file);
		exporter.setParameter(JRPdfExporterParameter.IS_COMPRESSED, Boolean.TRUE);
		exporter.exportReport();

		return file;
	}

	/**
	 * Generation des parametres du rapport
	 * 
	 * @param locale
	 *            Locale
	 * @param reportPath
	 *            Chemin d'acces des rapports
	 * @return Parametres
	 */
	private HashMap<String, Object> generateReportParameters(Locale locale, String reportPath)
	{
		HashMap<String, Object> parameters = new HashMap<String, Object>();

		parameters.put(REPORT_LOCALE, locale);
		parameters.put(SUBREPORT_DIR, reportPath);

		return parameters;
	}

	/**
	 * Retourne le rapport Jasper
	 * 
	 * @param reportName
	 *            Nom du rapport
	 * @return Jasper Rapport
	 */
	private JasperReport getReport(String reportName, String reportsPath) throws Exception
	{
		FileInputStream reportStream = null;
		JasperReport jasperReport = null;

		try
		{
			reportStream = new FileInputStream(reportsPath + reportName);
			jasperReport = (JasperReport) JRLoader.loadObject(reportStream);
		}

		catch (Exception e)
		{
			throw new Exception("Impossible de recuperer le rapport " + reportsPath + reportName + " " + e.getLocalizedMessage());
		}
		finally
		{
			if (reportStream != null)
			{
				try
				{
					reportStream.close();
				}
				catch (IOException e)
				{
					throw new Exception("Impossible de recuperer le rapport " + reportsPath + reportName + " " + e.getLocalizedMessage());
				}
			}
			else
			{
				throw new Exception("Impossible de recuperer le rapport " + reportsPath + reportName);
			}
		}

		return jasperReport;
	}

	/**
	 * Retourne le chemin d'acces pour acceder aux rapports
	 * 
	 * @return Chemin d'acces pour acceder aux rapports
	 * @throws Exception
	 */
	@Override
	public String getReportPath() throws Exception
	{
		String reportsPath = "";

		Setup setup = SetupUtils.INSTANCE.getMainSetup();
		reportsPath = setup.getPathReporting();

		if (System.getProperty("path.reporting") != null)
		{
			reportsPath = System.getProperty("path.reporting");
		}

		if (reportsPath != null && !reportsPath.endsWith(File.separator))
		{
			reportsPath = reportsPath + File.separator;
		}

		if (reportsPath == null || reportsPath.isEmpty())
		{
			throw new Exception("Aucun repertoire de defini dans les reglages pour les rapports 360");
		}

		return reportsPath;
	}

	/**
	 * Retourne le chamin d'acces de la config
	 * 
	 * @return Chemin d'acces de la config
	 * @throws Exception
	 *             Exception
	 */
	@Override
	public String getConfigPath() throws Exception
	{
		String configsPath = "";

		Setup setup = SetupUtils.INSTANCE.getMainSetup();

		configsPath = setup.getPathConfig();

		if (System.getProperty("path.config") != null)
		{
			configsPath = System.getProperty("path.config");
		}

		if (configsPath == null || configsPath.isEmpty())
		{
			throw new Exception("Aucun repertoire de defini dans les reglages pour la configuration 360");
		}

		return configsPath;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<SalesRep> generateQuotationToBeSent(Serializable macQuotationID, String destFolder, Date dateDuTraitement, String reportName) throws Exception
	{
		MacQuotation macQuotation = loader.findById(MacQuotation.class, MacQuotation.class, macQuotationID);
		List<SalesRep> emailsAEnvoyerList = new ArrayList<SalesRep>();

		for (SaleTransactionSalesRep saleTransactionSalesRep : macQuotation.getSalesReps())
		{
			SalesRep commercial = saleTransactionSalesRep.getSalesRep();
			loader.loadAssociations(SalesRep.class, commercial);
			String destToTreatFolder = getCommercialFolderToTreat(destFolder, commercial, dateDuTraitement) + "/";

			// ======================================================
			// === start a new transaction service to generate each PDF
			// === avoiding overload of heap size
			generateAndPersistPDF(macQuotation, reportName, macQuotation.getNumber() + ".pdf", destToTreatFolder, getReportPath(), getConfigPath());
			// ======================================================

			if (!emailsAEnvoyerList.contains(commercial))
			{
				emailsAEnvoyerList.add(commercial);
			}
		}

		return emailsAEnvoyerList;
	}

	public String getCommercialFolderToTreat(String baseFolder, SalesRep commercial, Date date)
	{
		DateFormat df = new SimpleDateFormat(DATEFORMAT_FILE);
		String folder = baseFolder + "/PDFATraiter/" + df.format(date) + "/" + commercial.getName();
		return folder;
	}

	private String getCommercialFolderTreated(String baseFolder, SalesRep commercial, Date date)
	{
		DateFormat df = new SimpleDateFormat(DATEFORMAT_FILE);
		String folder = baseFolder + "/TraitementOK/" + df.format(date) + "/" + commercial.getName();
		return folder;
	}

}
