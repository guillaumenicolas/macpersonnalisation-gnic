package com.mac.custom.services.report.interfaces.local;

import javax.ejb.Local;

import com.mac.custom.services.report.interfaces.MacReportServices;

@Local
public interface MacReportServicesLocal extends MacReportServices
{
}
