package com.mac.custom.services.report.interfaces.local;

import javax.ejb.Local;

import com.mac.custom.services.report.interfaces.MacGenericReportingServices;

@Local
public interface MacGenericReportingServicesLocal extends MacGenericReportingServices
{
}
