package com.mac.custom.services.report.interfaces;

import java.io.File;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import net.sf.jasperreports.engine.JasperPrint;

import com.netappsid.bo.model.Model;
import com.netappsid.erp.server.bo.SalesRep;

public interface MacGenericReportingServices
{
	public String getConfigPath() throws Exception;

	public String getReportPath() throws Exception;

	public File createPDF(JasperPrint print, File file) throws Exception;

	public boolean generateAndPersistPDF(Model model, String reportName, String fileName, String filePath, String reportsPath, String configsPath)
			throws Exception;

	List<SalesRep> generateQuotationToBeSent(Serializable macQuotationID, String destFolder, Date dateDuTraitement, String reportName) throws Exception;

}