package com.mac.custom.services.report.interfaces.remote;

import javax.ejb.Remote;

import com.mac.custom.services.report.interfaces.MacReportServices;

@Remote
public interface MacReportServicesRemote extends MacReportServices
{
}
