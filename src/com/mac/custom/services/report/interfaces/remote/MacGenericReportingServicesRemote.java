package com.mac.custom.services.report.interfaces.remote;

import javax.ejb.Remote;

import com.mac.custom.services.report.interfaces.MacGenericReportingServices;

@Remote
public interface MacGenericReportingServicesRemote extends MacGenericReportingServices
{
}
