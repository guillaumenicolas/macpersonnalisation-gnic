package com.mac.custom.services.report.interfaces;

import java.util.ArrayList;

public interface MacReportServices
{
	public ArrayList<Object> exportRapport(String objet, String inParameter) throws Exception;

	void runTodaysQuotationPDF() throws Exception;
}