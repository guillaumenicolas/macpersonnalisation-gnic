package com.mac.custom.services.receiving.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.mac.custom.services.receiving.interfaces.local.MacReceivingServicesLocal;
import com.mac.custom.services.receiving.interfaces.remote.MacReceivingServicesRemote;
import com.netappsid.services.interfaces.local.LoaderLocal;

@Stateless
public class MacReceivingServicesBean implements MacReceivingServicesLocal, MacReceivingServicesRemote
{
	@EJB
	protected static LoaderLocal loaderLocal;

	@Override
	public boolean isReceivingInvoiced(Serializable receivingId)
	{
		Map<String, Object> parameters = new HashMap<String, Object>();

		StringBuilder queryBuilder = new StringBuilder();

		queryBuilder.append("SELECT ReceivingDetail.id ");
		queryBuilder.append("FROM ReceivingDetail ");
		queryBuilder.append("LEFT JOIN PurchaseInvoiceDetail ON ReceivingDetail.id = PurchaseInvoiceDetail.receivingDetail_id ");
		queryBuilder.append("LEFT JOIN TransactionDetail ON PurchaseInvoiceDetail.id  = TransactionDetail.id ");
		queryBuilder.append("WHERE ReceivingDetail.receiving_id = :receivingId ");
		queryBuilder.append("GROUP BY ReceivingDetail.id , ReceivingDetail.quantityConvertedValue ");
		queryBuilder.append("HAVING ReceivingDetail.quantityConvertedValue > SUM(ISNULL(TransactionDetail.quantityConvertedValue,0)) ");

		parameters.put("receivingId", receivingId.toString());

		List result = loaderLocal.findByNativeQuery(queryBuilder.toString(), parameters, -1);
		boolean invoiced = false;

		if (result.isEmpty())
		{
			invoiced = true;
		}

		return invoiced;
	}
}