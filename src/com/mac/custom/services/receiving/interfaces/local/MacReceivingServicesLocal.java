package com.mac.custom.services.receiving.interfaces.local;

import javax.ejb.Local;

import com.mac.custom.services.receiving.interfaces.MacReceivingServices;

@Local
public interface MacReceivingServicesLocal extends MacReceivingServices
{
}
