package com.mac.custom.services.receiving.interfaces.remote;

import javax.ejb.Remote;

import com.mac.custom.services.receiving.interfaces.MacReceivingServices;

@Remote
public interface MacReceivingServicesRemote extends MacReceivingServices
{
}
