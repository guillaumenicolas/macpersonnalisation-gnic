package com.mac.custom.services.receiving.interfaces;

import java.io.Serializable;

public interface MacReceivingServices
{
	public boolean isReceivingInvoiced(Serializable receivingId);
}