package com.mac.custom.services;

import static org.torpedoquery.jpa.Torpedo.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.hibernate.Hibernate;
import org.torpedoquery.jpa.Query;

import com.mac.custom.bo.MacOrderDetail;
import com.mac.custom.bo.WorkLoadDetail;
import com.netappsid.bo.model.Model;
import com.netappsid.erp.server.bo.ExplodableItem;
import com.netappsid.erp.server.bo.Production;
import com.netappsid.erp.server.bo.ProductionBasket;
import com.netappsid.erp.server.bo.WorkOrder;
import com.netappsid.erp.server.bo.WorkOrderRoutingStep;
import com.netappsid.erp.server.datatypes.GroupMeasureValue;
import com.netappsid.erp.server.services.resultholders.ProductionResult;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.beans.PersistenceBean;
import com.netappsid.services.interfaces.Loader;
import com.netappsid.services.interfaces.Persistence;

public class ProductionServicesInterceptor
{
	private final ServiceLocator<Loader> loaderLocator = new ServiceLocator<Loader>(LoaderBean.class);
	private final ServiceLocator<Persistence> persistenceLocator = new ServiceLocator<Persistence>(PersistenceBean.class);

	@AroundInvoke
	public Object beanInterceptor(InvocationContext ctx) throws Exception
	{
		if (ctx.getMethod().getName().equals("generateProduction"))
		{
			Object returnObject = ctx.proceed();
			ProductionResult result = (ProductionResult) returnObject;
			for (Production production : result.getProductions())
			{
				updateWorkLoadDetail(production);
			}
			return returnObject;
		}
		else if (ctx.getMethod().getName().equals("getProductionBasketToProduce"))
		{
			return getProductionBasketToProduce();
		}
		else
		{
			return ctx.proceed();
		}
	}

	private List<Serializable> getProductionBasketToProduce()
	{
		// same query from BC but without an "error = false" condition
		ProductionBasket productionBasket = from(ProductionBasket.class);
		where(productionBasket.isInProcess()).eq(false).and(productionBasket.isComplete()).eq(false).and(productionBasket.isReversed()).eq(false);

		Query<Serializable> query = select(productionBasket.getId());
		return loaderLocator.get().findByQuery(query.getQuery(), query.getParameters(), false, null, null);
	}

	private void updateWorkLoadDetail(Production production)
	{
		if (production != null && production.getWorkOrderRoutingStep() != null)
		{
			WorkOrderRoutingStep workOrderRoutingStep = production.getWorkOrderRoutingStep();
			if (workOrderRoutingStep != null && workOrderRoutingStep.isLastWorkOrderRoutingStep())
			{
				WorkOrder workOrder = workOrderRoutingStep.getWorkOrder();
				if (workOrder != null && workOrder.getWorkOrderDetail() == null)
				{
					ExplodableItem explodableItem = workOrder.getExplodableItem();
					if (explodableItem != null && MacOrderDetail.class.isAssignableFrom(Hibernate.getClass(explodableItem)))
					{
						MacOrderDetail macOrderDetail = Model.getTarget(explodableItem);

						// prend la quantite totale des uo pour le details
						GroupMeasureValue qtyToProduce = macOrderDetail.getWorkLoadDetailTotalQuantity();

						if (qtyToProduce != null)
						{
							// si le detail a une quantite > 1 ...
							if (macOrderDetail.getQuantity().getValue().compareTo(BigDecimal.ONE) > 0)
							{
								// ... diviser la quantite total d'uo par la quantite du detail pour avoir le nombre de uo unitaire
								qtyToProduce = qtyToProduce.divide(macOrderDetail.getQuantity().getValue());
							}

							GroupMeasureValue qtyProducedRemaining = new GroupMeasureValue(qtyToProduce);

							if (qtyToProduce.isGreaterThanZero())
							{
								List<WorkLoadDetail> sortedWorkLoadDetails = sortWorkLoadDetailsByDateAsc(macOrderDetail.getWorkLoadDetails());

								for (WorkLoadDetail workLoadDetail : sortedWorkLoadDetails)
								{
									GroupMeasureValue qtyAvailable = workLoadDetail.getQuantityAvailable();

									if (qtyAvailable != null && qtyAvailable.isGreaterThanZero())
									{
										if (qtyAvailable.compareTo(qtyToProduce) >= 0)
										{
											// on ajoute les UO produits (tous) à la quantité produite
											qtyProducedRemaining.setValue(BigDecimal.ZERO);
											workLoadDetail.updateQuantityProduced(qtyToProduce);
										}
										else
										{
											qtyProducedRemaining = qtyProducedRemaining.substract(qtyAvailable);
											workLoadDetail.updateQuantityProduced(workLoadDetail.getQuantityAvailable());
										}

										persistenceLocator.get().save(workLoadDetail, false);
									}

									if (qtyProducedRemaining.getValue().compareTo(BigDecimal.ZERO) <= 0)
									{
										return;
									}
								}
							}
						}
					}
				}
			}

		}
	}

	public List<WorkLoadDetail> sortWorkLoadDetailsByDateAsc(List<WorkLoadDetail> workLoadDetails)
	{
		Collections.sort(workLoadDetails, new Comparator<WorkLoadDetail>()
			{
				@Override
				public int compare(WorkLoadDetail o1, WorkLoadDetail o2)
				{
					int result;
					if (o1.getWorkLoad().getDate().compareTo(o2.getWorkLoad().getDate()) >= 0)
					{
						result = 1;
					}
					else
					{
						result = -1;
					}

					return result;
				}
			});

		return workLoadDetails;
	}
}
