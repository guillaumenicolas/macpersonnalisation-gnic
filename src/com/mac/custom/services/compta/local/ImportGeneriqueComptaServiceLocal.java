package com.mac.custom.services.compta.local;

import javax.ejb.Local;

import com.mac.custom.services.compta.interfaces.ImportGeneriqueComptaService;

@Local
public interface ImportGeneriqueComptaServiceLocal extends ImportGeneriqueComptaService
{

}
