package com.mac.custom.services.compta.local;

import javax.ejb.Local;

import com.mac.custom.services.compta.interfaces.ImportComptaService;

@Local
public interface ImportComptaServiceLocal extends ImportComptaService
{

}
