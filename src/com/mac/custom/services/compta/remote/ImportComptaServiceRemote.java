package com.mac.custom.services.compta.remote;

import javax.ejb.Remote;

import com.mac.custom.services.compta.interfaces.ImportComptaService;

@Remote
public interface ImportComptaServiceRemote extends ImportComptaService
{

}
