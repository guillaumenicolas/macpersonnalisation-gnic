package com.mac.custom.services.compta.remote;

import javax.ejb.Remote;

import com.mac.custom.services.compta.interfaces.ImportGeneriqueComptaService;

@Remote
public interface ImportGeneriqueComptaServiceRemote extends ImportGeneriqueComptaService
{

}
