package com.mac.custom.services.compta.interfaces;

import java.util.Map;

import com.mac.custom.exception.ExceptionInterface;

public interface ImportGeneriqueComptaService
{
	/**
	 * Suppression des lignes de la table avec statut 2 (Lignes d�j� trait�s lors d'ex�cution pr�c�dentes)
	 * 
	 * @param connection
	 *            Connection � la BD
	 * @param nomTable
	 *            Nom de la table
	 */
	public void suppressionEntrees(String nomTable) throws ExceptionInterface;

	/**
	 * Methode permettant de changer le statut d'un enregistrement avec code d'integration et code d'erreur
	 * 
	 * @param clesColonnes
	 *            Nom de colonne et valeur
	 * @param nomTable
	 *            Nom de la table SQL
	 * @param nomColonne
	 *            Nom de la colonne qui contient le code 0,1,2
	 * @param code
	 *            0: Non int�gr�, 1: En cours d'int�gration, 2: Correctement int�gr�, 3: Erreur
	 * @param messageErreur
	 *            Message d'erreur (null si correctement int�gr�)
	 */
	public void updateChampsInterface(Map<String, String> clesColonnes, String nomTable, String nomColonne, int code, String messageErreur)
			throws ExceptionInterface;

	/**
	 * Methode qui indique s'il y a encore des r�sultats � traiter dans la table
	 * 
	 * @param nomTable
	 *            Nom de la table
	 * @param nomColonne
	 *            Nom de la colonne qui contient le code 0,1,2
	 * @return True ou false
	 */
	public boolean encoreResultats(String nomTable, String nomColonne);

}
