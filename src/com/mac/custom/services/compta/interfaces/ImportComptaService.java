package com.mac.custom.services.compta.interfaces;

import com.mac.custom.exception.ExceptionInterface;

public interface ImportComptaService
{
	public void importationCompta() throws ExceptionInterface;
}
