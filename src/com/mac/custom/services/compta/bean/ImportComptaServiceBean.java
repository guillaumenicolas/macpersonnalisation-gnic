package com.mac.custom.services.compta.bean;

import java.sql.Connection;
import java.sql.Date;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mac.custom.exception.ExceptionInterface;
import com.mac.custom.services.compta.local.ImportComptaServiceLocal;
import com.mac.custom.services.compta.local.ImportGeneriqueComptaServiceLocal;
import com.mac.custom.services.compta.remote.ImportComptaServiceRemote;
import com.netappsid.erp.server.bo.Customer;
import com.netappsid.erp.server.bo.Note;
import com.netappsid.erp.server.bo.TypeNote;
import com.netappsid.erp.server.bo.User;
import com.netappsid.services.interfaces.local.LoaderLocal;
import com.netappsid.services.interfaces.local.PersistenceLocal;

@Stateless
public class ImportComptaServiceBean implements ImportComptaServiceLocal, ImportComptaServiceRemote
{
	private static Logger logger = Logger.getLogger(ImportComptaServiceBean.class);

	private static String TYPE_NOTE_COMPTA = "ORIGINECOMPTA";
	private static String UTILISATEUR_INTERFACE = "INTERFACE";
	private static String TABLE_CLIENT = "Client";
	// private static String TABLE_FOURNISSEUR = "Fournisseur";

	// Dans quel contexte on est: local ou remote
	private static String IMPORT_EXPORT_CONTEXT = "importExportContext";

	@Resource(mappedName = "java:/INTCOMPTADS")
	private DataSource importDS;

	@EJB(mappedName = "erp/LoaderBean/local")
	public LoaderLocal loader;

	@EJB(mappedName = "erp/PersistenceBean/local")
	public PersistenceLocal persistence;

	@EJB(mappedName = "erp/ImportGeneriqueComptaServiceBean/local")
	public ImportGeneriqueComptaServiceLocal importGeneriqueComptaServiceBean;

	private String context = null;

	@Override
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public void importationCompta() throws ExceptionInterface
	{
		logger.debug("Debut de l'importation...");

		context = System.getProperty(IMPORT_EXPORT_CONTEXT);

		Connection connection = getConnection();

		TypeNote typeNote = getTypeNoteImport();
		User user = getUserImport();

		// Si une des valeurs est manquante...
		if (typeNote == null || user == null)
		{
			throw new ExceptionInterface("Configuration invalide pour importation des notes compta", logger);
		}

		ResultSet rowset = null;

		try
		{
			Statement statement = connection.createStatement();

			rowset = statement.executeQuery(getRequeteNoteCompta());
		}
		catch (Exception e)
		{
			try
			{
				connection.close();
				connection = null;
			}
			catch (SQLException e1)
			{
				throw new ExceptionInterface("Impossible de terminer la connexion: " + e1.getLocalizedMessage(), logger);
			}

			throw new ExceptionInterface("Impossible d'ex�cuter la requ�te: " + e.getLocalizedMessage(), logger);
		}

		try
		{
			while (rowset.next())
			{
				// Les contraintes "non null" sont g�r�s au niveau de la BD
				String tableClient = rowset.getString(1);
				String noentite = rowset.getString(2);
				String noligne = rowset.getString(3);
				Date datesaisie = rowset.getDate(4);
				String note = rowset.getString(5);
				String societe = rowset.getString(6);
				Date datecreation = rowset.getDate(7);
				Integer codeinte = rowset.getInt(8);

				if (tableClient != null && tableClient.equals(TABLE_CLIENT))
				{
					importGeneriqueComptaServiceBean.updateChampsInterface(getMapParametres(noentite, noligne), "importationnotes", "codeinte", 1, null);

					// Ici, on charge les clients qui ont le code : noentite
					List<Customer> list = loader.findByField(Customer.class, Customer.class, Customer.PROPERTYNAME_CODE, noentite);

					Customer customer = null;

					for (Customer customerTmp : list)
					{
						// Valider la societe
						customer = customerTmp;
					}

					if (customer != null)
					{
						// Ici, on charge les notes du client
						List<Note> notes = customer.getNotes();

						// Note � cr�er ou modifier
						Note note360 = null;

						for (Note noteTmp : notes)
						{
							// Valider si le # de la ligne de commentaire Navision existe deja (nouveau champ)
							// MAJ si elle existe deja
							// note360 = noteTmp (si c'est une modification)
						}

						note360 = new Note();

						// Pas besoin de valider les null puisque la validation est effectuee au niveau de la BD
						note360.setNote("fr", note);
						note360.setTypeNote(typeNote);
						note360.setUser(user);
						note360.setDate(datesaisie);

						// Ajout de la note aux notes du client
						customer.addToNotes(note360);

						sauvegardeClient(customer);
					}
				}
				// Il s'agit d'un fournisseur
				else
				{
					// A faire
				}
			}
		}
		catch (SQLException e)
		{
			try
			{
				connection.close();
				connection = null;
			}
			catch (SQLException e1)
			{
				throw new ExceptionInterface("Impossible de terminer la connexion: " + e1.getLocalizedMessage(), logger);
			}
		}
	}

	/**
	 * Retourne les cl�/valeurs pour la table
	 * 
	 * @param noentite
	 *            Num�ro de l'entir�
	 * @param noligne
	 *            Num�ro de ligne
	 * @return Map contenant les cl�s/valeurs
	 */
	private Map<String, String> getMapParametres(String noentite, String noligne)
	{
		Map<String, String> parametres = new HashMap<String, String>();

		parametres.put("noentite", noentite);
		parametres.put("noligne", noligne);

		return parametres;
	}

	/**
	 * Sauvegarde le client � l'int�rieur d'une transaction
	 * 
	 * @param customer
	 *            Client � sauvegarder
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void sauvegardeClient(Customer customer)
	{
		persistence.save(customer, null);
	}

	/**
	 * Retourne l'utilisateur UTILISATEUR_INTERFACE
	 * 
	 * @return Utilisateur UTILISATEUR_INTERFACE
	 */
	private User getUserImport()
	{
		return loader.findFirstByField(User.class, User.class, User.PROPERTYNAME_FIRSTNAME, UTILISATEUR_INTERFACE);
	}

	/**
	 * Retourne le type de note TYPE_NOTE_COMPTA
	 * 
	 * @return Type de note TYPE_NOTE_COMPTA
	 */
	private TypeNote getTypeNoteImport()
	{
		return loader.findFirstByField(TypeNote.class, TypeNote.class, TypeNote.PROPERTYNAME_CODE, TYPE_NOTE_COMPTA);
	}

	/**
	 * Retourne la requete qui permet d'obtenir les notes compta
	 * 
	 * @return Requete qui retourne les notes compta
	 */
	private String getRequeteNoteCompta()
	{
		return "SELECT tableclient,noentite,noligne,datesaisie,note,societe,datecreation,codeinte FROM importationnotes where codeinte = 0";
	}

	/**
	 * Retourne la connexion JDBC � la BD.
	 * 
	 * @return Retourne la connextion � la BD
	 */
	private Connection getConnection() throws ExceptionInterface
	{
		String context = System.getProperty(IMPORT_EXPORT_CONTEXT);

		Connection connection = null;

		try
		{
			connection = null;

			if (context == null || context.equalsIgnoreCase("remote"))
			{
				connection = importDS.getConnection();
			}
			// Temporaire en attendant un correctif de 360
			else
			{
				DriverManager.registerDriver((Driver) Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance());
				connection = DriverManager.getConnection("jdbc:jtds:sqlserver://127.0.0.1:1433/BC360MACExemples", "sa", "victor");
				connection.setAutoCommit(false);
			}

			return connection;
		}
		catch (Exception e)
		{
			logger.error("Impossible d'obtenir une connexion � la DB: " + e.getMessage());

			try
			{
				if (connection != null)
				{
					connection.close();
					connection = null;
				}
			}
			catch (Exception e1)
			{
				logger.error("Impossible de terminer la connexion: " + e1.getLocalizedMessage());
			}
		}

		return null;
	}

}
