package com.mac.custom.services.workflow.interfaces.local;

import java.io.Serializable;
import java.util.Set;

import javax.ejb.Local;

import com.google.common.cache.Cache;
import com.mac.custom.bo.MacWorkflowMaster;
import com.mac.custom.services.workflow.interfaces.MacWorkflowServices;
import com.netappsid.erp.server.bo.DocumentStatus;

@Local
public interface MacWorkflowServicesLocal extends MacWorkflowServices
{
	public static final String WF_ADMIN = "WF_ADMIN";
	public static final String EMAIL = "Courriel";
	public static final String FAX = "Fax";
	public static final String WFMASTER = "wfMaster";
	
	public String generateARCFile(Serializable saleOrderID) throws Exception;

	public String generateClientFile(Serializable customerID) throws Exception;

	public void unblockOrder(Serializable customerId) throws Exception;
	
	//Ajouts 13-02 afin de sortir le code des methodes static
	
	public boolean isARCReportValid(String documentId);
	
	public String getARCCommunications(String documentId, Class type);
	
	public void changeStatusToExplode(String documentId);
	
	public Set<String> getADVsActorsForSaleOrder(String documentId, boolean addAdmins);
	
	public boolean isReportValid(String customerId);
	
	public Set<String> getADVsActorsForCustomer(String customerId, boolean addAdmins);
	
	public String getCommunications(String customerId, Class type);
	
	public void setDetailM5PAccepted(boolean isAccepted, String id, String internalNote, String comment, String confirmationNumber);
	
	public void setDetailPrice(String id, String price);
	
	public boolean saleOrderDetailsAllValid(String id);
	
	public String getTransactionDetailIdentifier(String id);
	
	//Utils
	public void changeDocumentStatus(String documentId, String newStatus);
	
	public void changeDocumentStatus(String documentId, DocumentStatus newStatus);
	
	
	
	public Set<String> getAllActors();
	
	public Set<String> getAllActorsForGroup(String groupName, boolean addAdmins);
	
	public Set<String> addWFAdmin(Set<String> currentUsers);
	
	public String getADVsEmailAddressesForSaleOrder(String documentId);
	
	public String getATCsEmailAddressesForSaleOrder(String documentId);
	
	public String getADVsEmailAddressesForCustomer(String customerId);
	
	public String getATCsEmailAddressesForCustomer(String customerId);
}
