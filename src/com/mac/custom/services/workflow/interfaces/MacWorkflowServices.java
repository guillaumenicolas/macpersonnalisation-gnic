package com.mac.custom.services.workflow.interfaces;

import com.mac.custom.bo.MacWorkflowMaster;

public interface MacWorkflowServices 
{
	public MacWorkflowMaster getWFMaster();
}
