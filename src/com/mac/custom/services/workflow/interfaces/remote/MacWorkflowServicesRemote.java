package com.mac.custom.services.workflow.interfaces.remote;

import java.io.Serializable;

import javax.ejb.Remote;

import com.mac.custom.services.workflow.interfaces.MacWorkflowServices;

@Remote
public interface MacWorkflowServicesRemote extends MacWorkflowServices
{
	//TESTS SEULEMENT, TOUT DOIT �TRE FAIT EN LOCAL
	
	public String generateClientFile(Serializable customerID) throws Exception;
	
	public String generateARCFile(Serializable saleOrderID) throws Exception;
}
