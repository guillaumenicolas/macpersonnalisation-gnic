package com.mac.custom.services.workflow.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.log4j.Logger;

import reports.NAIDBeanDataSource;

import com.mac.custom.bo.MacBlockingReason;
import com.mac.custom.bo.MacCustomer;
import com.mac.custom.bo.MacDocumentStatus;
import com.mac.custom.bo.MacOrder;
import com.mac.custom.bo.MacQuotation;
import com.mac.custom.bo.MacReport;
import com.mac.custom.bo.MacWorkflowMaster;
import com.mac.custom.dao.WorkflowARCUtilsDAO;
import com.mac.custom.services.workflow.interfaces.local.MacWorkflowServicesLocal;
import com.mac.custom.services.workflow.interfaces.remote.MacWorkflowServicesRemote;
import com.mac.custom.workflow.WorkflowUtils;
import com.netappsid.bo.EntityStatus;
import com.netappsid.bo.model.Model;
import com.netappsid.bonitasoft.services.enums.BonitaParameters;
import com.netappsid.bonitasoft.services.services.interfaces.BonitaServices;
import com.netappsid.datatypes.MonetaryAmount;
import com.netappsid.erp.client.utils.DocumentInformationStatusUtils;
import com.netappsid.erp.server.bo.Attachment;
import com.netappsid.erp.server.bo.BlockingReason;
import com.netappsid.erp.server.bo.CommercialEntityDefaultReport;
import com.netappsid.erp.server.bo.CommercialEntityReportContact;
import com.netappsid.erp.server.bo.Communication;
import com.netappsid.erp.server.bo.Customer;
import com.netappsid.erp.server.bo.CustomerSalesRep;
import com.netappsid.erp.server.bo.DocumentStatus;
import com.netappsid.erp.server.bo.Email;
import com.netappsid.erp.server.bo.Fax;
import com.netappsid.erp.server.bo.Order;
import com.netappsid.erp.server.bo.Report;
import com.netappsid.erp.server.bo.ReportDetail;
import com.netappsid.erp.server.bo.ReportType;
import com.netappsid.erp.server.bo.SaleOrder;
import com.netappsid.erp.server.bo.SaleTransaction;
import com.netappsid.erp.server.bo.SaleTransactionDetail;
import com.netappsid.erp.server.bo.SaleTransactionSalesRep;
import com.netappsid.erp.server.bo.Setup;
import com.netappsid.erp.server.bo.TransactionDetail;
import com.netappsid.erp.server.services.interfaces.local.DocumentStatusServicesLocal;
import com.netappsid.erp.server.services.utils.SetupUtils;
import com.netappsid.europe.naid.custom.bo.CustomerSalesRepEuro;
import com.netappsid.europe.naid.custom.bo.OrderDetailEuro;
import com.netappsid.europe.naid.custom.bo.QuotationDetailEuro;
import com.netappsid.europe.naid.custom.bo.SalesRepEuro;
import com.netappsid.europe.naid.custom.bo.UserEuro;
import com.netappsid.security.auth.server.SecurityGroup;
import com.netappsid.security.auth.server.SecurityUser;
import com.netappsid.services.interfaces.local.LoaderLocal;

@Stateless
public class MacWorkflowServicesBean implements MacWorkflowServicesLocal, MacWorkflowServicesRemote
{
	private static Logger logger = Logger.getLogger(MacWorkflowServicesBean.class);

	private static final String SUBREPORT_DIR = "SUBREPORT_DIR";
	private static final String REPORT_LOCALE = "REPORT_LOCALE";

	@EJB(mappedName = "erp/LoaderBean/local")
	private LoaderLocal loader;

	@EJB(mappedName = "erp/BonitaServicesBean/local")
	private BonitaServices wfServices;

	@EJB(mappedName = "erp/DocumentStatusServicesBean/local")
	private DocumentStatusServicesLocal documentStatusServices;

	@Override
	public String generateARCFile(Serializable saleOrderID) throws Exception
	{
		String result = null;

		SaleOrder saleOrder = loader.findById(SaleOrder.class, null, saleOrderID);

		String filePath = getFilePathARC();
		String filePathLocal = getFilePathLocalARC();
		String configPath = getConfigPath();
		String reportPath = getReportPath();

		if (saleOrder != null)
		{
			List<Report> reportList = getReportsListARC(saleOrder);

			for (Report report : reportList)
			{
				if (report.getDetails() != null && report.getDetails().size() > 0)
				{
					ReportDetail reportDetail = report.getDetails().get(0);
					GregorianCalendar gcalendar = new GregorianCalendar();

					String fileName = "AR" + "_" + saleOrder.getDocumentNumber() + "_";

					String year = Integer.toString(gcalendar.get(Calendar.YEAR));
					String month = Integer.toString(gcalendar.get(Calendar.MONTH));
					String date = Integer.toString(gcalendar.get(Calendar.DATE));
					String hour = Integer.toString(gcalendar.get(Calendar.HOUR_OF_DAY));
					String minute = Integer.toString(gcalendar.get(Calendar.MINUTE));
					String second = Integer.toString(gcalendar.get(Calendar.SECOND));

					if (month.length() == 1)
					{
						month = "0" + month;
					}
					if (date.length() == 1)
					{
						date = "0" + date;
					}
					if (hour.length() == 1)
					{
						hour = "0" + hour;
					}
					if (minute.length() == 1)
					{
						minute = "0" + minute;
					}
					if (second.length() == 1)
					{
						second = "0" + second;
					}

					fileName = fileName + year + month + date + "-" + hour + minute + second + ".pdf";

					try
					{
						if (generateAndPersistPDF(saleOrder, reportDetail.getFilePath(), fileName, filePath, reportPath, configPath))
						{
							result = filePath + fileName;
						}

						if (fileName != null)
						{
							attachFileToOrder(saleOrder, "ARC", fileName, filePathLocal);
						}
					}
					catch (Exception e)
					{
						logger.error("Erreur dans generateARCFile: " + e.getMessage());
						throw e;
					}
				}
			}
		}

		return result;
	}

	@Override
	public String generateClientFile(Serializable customerID) throws Exception
	{
		MacWorkflowMaster wfMaster = com.mac.custom.workflow.WorkflowUtils.getWFMaster();

		String result = null;

		MacCustomer customer = loader.findById(MacCustomer.class, null, customerID);

		String filePath = getFilePathClient();
		String filePathLocal = getFilePathLocalClient();
		String configPath = getConfigPath();
		String reportPath = getReportPath();

		if (customer != null && wfMaster.getWfClientValidation() != null)
		{
			Report report = wfMaster.getWfClientValidation().getReport();

			if (report.getDetails() != null && report.getDetails().size() > 0)
			{
				ReportDetail reportDetail = report.getDetails().get(0);

				GregorianCalendar gcalendar = new GregorianCalendar();

				String fileName = "VC" + "_" + customer.getCode() + "_";

				String year = Integer.toString(gcalendar.get(Calendar.YEAR));
				String month = Integer.toString(gcalendar.get(Calendar.MONTH));
				String date = Integer.toString(gcalendar.get(Calendar.DATE));
				String hour = Integer.toString(gcalendar.get(Calendar.HOUR_OF_DAY));
				String minute = Integer.toString(gcalendar.get(Calendar.MINUTE));
				String second = Integer.toString(gcalendar.get(Calendar.SECOND));

				if (month.length() == 1)
				{
					month = "0" + month;
				}
				if (date.length() == 1)
				{
					date = "0" + date;
				}
				if (hour.length() == 1)
				{
					hour = "0" + hour;
				}
				if (minute.length() == 1)
				{
					minute = "0" + minute;
				}
				if (second.length() == 1)
				{
					second = "0" + second;
				}

				fileName = fileName + year + month + date + "-" + hour + minute + second + ".pdf";

				try
				{
					if (generateAndPersistPDF(customer, reportDetail.getFilePath(), fileName, filePath, reportPath, configPath))
					{
						result = filePath + fileName;
					}

					if (fileName != null)
					{
						attachFileToCustomer(customer, "VC", fileName, filePathLocal);
					}
				}
				catch (Exception e)
				{
					logger.error("Erreur dans generateARCFile: " + e.getMessage());
					throw e;
				}
			}
		}

		return result;
	}

	/**
	 * Retourne la liste des rapports de type "ARC" pour le client du document
	 * 
	 * @param saleOrder
	 *            Document
	 * @return Liste des rapports de type "ARC"
	 */
	private List<Report> getReportsListARC(SaleOrder saleOrder)
	{
		List<Report> list = new ArrayList<Report>();

		for (CommercialEntityDefaultReport defaultReport : saleOrder.getCustomer().getDefaultReports())
		{
			MacReport report = (MacReport) Model.getTarget(defaultReport.getReport());

			Long internalId = ReportType.ORDER_ID;

			if (saleOrder instanceof MacQuotation)
			{
				internalId = ReportType.QUOTATION_ID;
			}

			// Si c'est un ARC et que "envoyer email" ou "envoyer fax" est selectionne
			if (report.isArc() && defaultReport.getReportType().getInternalId().compareTo(internalId) == 0
					&& (defaultReport.isEmailToDocumentContact() || defaultReport.isFaxToDocumentContact()))
			{
				list.add(report);
			}
		}

		return list;
	}

	/**
	 * Attache le fichier passe en parametre a la commande
	 * 
	 * @param order
	 *            Commande
	 * @param description
	 *            Description du fichier
	 * @param fileName
	 *            Chemin d'acces du fichier
	 * @param filePathLocal
	 *            Chemin d'acces
	 */
	private void attachFileToOrder(SaleOrder saleOrder, String description, String fileName, String filePathLocal)
	{
		Attachment attachment = new Attachment();
		attachment.setDescription("fr", description);
		attachment.setDescription("fr_FR", description);
		attachment.setFilePath(filePathLocal + fileName);

		saleOrder.addToAttachments(attachment);
	}

	/**
	 * Attache le fichier passe en parametre au clienr
	 * 
	 * @param customer
	 *            Client
	 * @param description
	 *            Description du fichier
	 * @param fileName
	 *            Nom du fichier
	 * @param filePathLocal
	 *            Chemin d'acces
	 */
	private void attachFileToCustomer(Customer customer, String description, String fileName, String filePathLocal)
	{
		Attachment attachment = new Attachment();
		attachment.setDescription("fr", description);
		attachment.setDescription("fr_FR", description);
		attachment.setFilePath(filePathLocal + fileName);

		customer.addToAttachments(attachment);
	}

	/**
	 * Genere et enregistre le rapport passe en parametre
	 * 
	 * @param saleOrder
	 *            Document
	 * @param reportName
	 *            Nom du rapport
	 * @param fileName
	 *            Nom du fichier (.pdf)
	 * @return Succes ou echec
	 * @throws Exception
	 *             Exceptions lors du processus
	 */
	private boolean generateAndPersistPDF(Model model, String reportName, String fileName, String filePath, String reportsPath, String configsPath)
			throws Exception
	{
		boolean result = false;

		File file = new File(filePath + fileName);

		if (!file.exists())
		{
			file.createNewFile();

			JasperPrint print = null;
			Locale locale = null;

			ClassLoader current = Thread.currentThread().getContextClassLoader();

			ClassLoader cl = new URLClassLoader(new URL[] { new File(filePath).toURI().toURL(), new File(reportsPath).toURI().toURL(),
					new File(configsPath).toURI().toURL() }, current);
			Thread.currentThread().setContextClassLoader(cl);

			try
			{
				JasperReport report = getReport(reportName, reportsPath);

				if (report != null)
				{
					locale = Locale.getDefault();

					HashMap<String, Object> parameters = generateReportParameters(locale, reportsPath);

					try
					{
						print = JasperFillManager.fillReport(report, parameters, new NAIDBeanDataSource(model));

						createPDF(print, file);
						result = true;
					}
					catch (Exception e)
					{
						file.delete();
						throw new Exception("Impossible de generer l'editiont: " + reportName);
					}
				}
				else
				{
					throw new Exception("Impossible de trouver le rapport: " + reportName);
				}
			}
			finally
			{
				Thread.currentThread().setContextClassLoader(current);
			}
		}
		else
		{
			throw new Exception("Le fichier suivant existe deja: " + fileName);
		}

		return result;
	}

	/**
	 * Creation du pdf base sur le JasperPrint
	 * 
	 * @param print
	 *            JasperPrint
	 * @param file
	 *            Fichier de sortie
	 * @return Fichier de sortie
	 */
	public File createPDF(JasperPrint print, File file) throws Exception
	{
		JRPdfExporter exporter = new JRPdfExporter();

		exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
		exporter.setParameter(JRExporterParameter.OUTPUT_FILE, file);
		exporter.setParameter(JRPdfExporterParameter.IS_COMPRESSED, Boolean.TRUE);
		exporter.exportReport();

		return file;
	}

	/**
	 * Generation des parametres du rapport
	 * 
	 * @param locale
	 *            Locale
	 * @param reportPath
	 *            Chemin d'acces des rapports
	 * @return Parametres
	 */
	private HashMap<String, Object> generateReportParameters(Locale locale, String reportPath)
	{
		HashMap<String, Object> parameters = new HashMap<String, Object>();

		parameters.put(REPORT_LOCALE, locale);
		parameters.put(SUBREPORT_DIR, reportPath);

		return parameters;
	}

	/**
	 * Retourne le rapport Jasper
	 * 
	 * @param reportName
	 *            Nom du rapport
	 * @return Jasper Rapport
	 */
	private JasperReport getReport(String reportName, String reportsPath) throws Exception
	{
		FileInputStream reportStream = null;
		JasperReport jasperReport = null;

		try
		{
			reportStream = new FileInputStream(reportsPath + reportName);
			jasperReport = (JasperReport) JRLoader.loadObject(reportStream);
		}
		catch (Exception e)
		{
			throw new Exception("Impossible de recuperer le rapport " + reportsPath + reportName + " " + e.getLocalizedMessage());
		}
		finally
		{
			if (reportStream != null)
			{
				try
				{
					reportStream.close();
				}
				catch (IOException e)
				{
					throw new Exception("Impossible de recuperer le rapport " + reportsPath + reportName + " " + e.getLocalizedMessage());
				}
			}
			else
			{
				throw new Exception("Impossible de recuperer le rapport " + reportsPath + reportName);
			}
		}

		return jasperReport;
	}

	/**
	 * Retourne le chemin d'acces du fichier ARC
	 * 
	 * @return Chemin d'acces du fichier ARC
	 */
	private String getFilePathARC()
	{
		MacWorkflowMaster wfMaster = com.mac.custom.workflow.WorkflowUtils.getWFMaster();

		String filePath = wfMaster.getWfARC().getArcFilePath();

		if (filePath.contains("\\"))
		{
			filePath = filePath.replaceAll("\\\\", "/");
		}

		if (!filePath.endsWith("/"))
		{
			filePath = filePath.concat("/");
		}

		return filePath;
	}

	/**
	 * Retourne le chemin d'acces du fichier ARC (local)
	 * 
	 * @return Chemin d'acces du fichier ARC (local)
	 */
	private String getFilePathLocalARC()
	{
		MacWorkflowMaster wfMaster = com.mac.custom.workflow.WorkflowUtils.getWFMaster();

		String filePathLocal = wfMaster.getWfARC().getArcFilePathLocal();

		if (!filePathLocal.endsWith("/"))
		{
			filePathLocal = filePathLocal.concat("/");
		}

		return filePathLocal;
	}

	/**
	 * Retourne le chemin d'acces du fichier client
	 * 
	 * @return Chemin d'acces du fichier client
	 */
	private String getFilePathClient()
	{
		MacWorkflowMaster wfMaster = com.mac.custom.workflow.WorkflowUtils.getWFMaster();

		String filePath = wfMaster.getWfClientValidation().getFilePath();

		if (filePath.contains("\\"))
		{
			filePath = filePath.replaceAll("\\\\", "/");
		}

		if (!filePath.endsWith("/"))
		{
			filePath = filePath.concat("/");
		}

		return filePath;
	}

	/**
	 * Retourne le chemin d'acces du fichier client (local)
	 * 
	 * @return Chemin d'acces du fichier client (local)
	 */
	private String getFilePathLocalClient()
	{
		MacWorkflowMaster wfMaster = com.mac.custom.workflow.WorkflowUtils.getWFMaster();

		String filePathLocal = wfMaster.getWfClientValidation().getFilePathLocal();

		if (filePathLocal.contains("\\"))
		{
			filePathLocal = filePathLocal.replaceAll("\\\\", "/");
		}

		if (!filePathLocal.endsWith("/"))
		{
			filePathLocal = filePathLocal.concat("/");
		}

		return filePathLocal;
	}

	/**
	 * Retourne le chamin d'acces de la config
	 * 
	 * @return Chemin d'acces de la config
	 * @throws Exception
	 *             Exception
	 */
	private String getConfigPath() throws Exception
	{
		String configsPath = "";

		Setup setup = SetupUtils.INSTANCE.getMainSetup();

		configsPath = setup.getPathConfig();

		if (System.getProperty("path.config") != null)
		{
			configsPath = System.getProperty("path.config");
		}

		if (configsPath == null || configsPath.isEmpty())
		{
			throw new Exception("Aucun repertoire de defini dans les reglages pour la configuration 360");
		}

		return configsPath;
	}

	/**
	 * Retourne le chemin d'acces pour acceder aux rapports
	 * 
	 * @return Chemin d'acces pour acceder aux rapports
	 * @throws Exception
	 */
	private String getReportPath() throws Exception
	{
		String reportsPath = "";

		Setup setup = SetupUtils.INSTANCE.getMainSetup();
		reportsPath = setup.getPathReporting();

		if (System.getProperty("path.reporting") != null)
		{
			reportsPath = System.getProperty("path.reporting");
		}

		if (reportsPath != null && !reportsPath.endsWith(File.separator))
		{
			reportsPath = reportsPath + File.separator;
		}

		if (reportsPath == null || reportsPath.isEmpty())
		{
			throw new Exception("Aucun repertoire de defini dans les reglages pour les rapports 360");
		}

		return reportsPath;
	}

	@Override
	public void unblockOrder(Serializable customerId) throws Exception
	{
		MacWorkflowMaster wfMaster = com.mac.custom.workflow.WorkflowUtils.getWFMaster();
		BlockingReason automaticBlockingReason = null;
		BlockingReason automaticUnblockingReason = null;

		if (wfMaster != null)
		{
			if (wfMaster.getWfUnblockAuto() != null && wfMaster.getWfUnblockAuto().isActive())
			{
				automaticUnblockingReason = wfMaster.getWfUnblockAuto().getUnblockingReason();
			}
			if (wfMaster.getWfBlockAuto() != null)
			{
				automaticBlockingReason = wfMaster.getWfBlockAuto().getBlockingReason();
			}
		}

		if (automaticBlockingReason != null && automaticUnblockingReason != null)
		{
			MacCustomer macCustomer = loader.findById(MacCustomer.class, null, customerId);

			if (macCustomer.getEncoursClient() != null && macCustomer.getEncoursClient().getDepassement() != null
					&& macCustomer.getEncoursClient().getDepassement().isNegative())
			{
				BigDecimal unblockedTotal = BigDecimal.ZERO;
				MonetaryAmount limit = macCustomer.getEncoursClient().getEncoursAccepte().subtract(macCustomer.getEncoursClient().getEncoursTotal());

				StringBuffer query = new StringBuffer();

				query.append("select macOrder." + MacOrder.PROPERTYNAME_ID + ", macOrder." + MacOrder.PROPERTYNAME_TOTAL);
				query.append(" from MacOrder macOrder");
				query.append(" INNER JOIN macOrder." + MacOrder.PROPERTYNAME_CUSTOMER + " customer");
				query.append(" INNER JOIN macOrder." + MacOrder.PROPERTYNAME_DOCUMENTSTATUS + " documentStatus");
				query.append(" INNER JOIN macOrder." + MacOrder.PROPERTYNAME_BLOCKINGPRODUCTIONREASON + " orderProductionBlockingReason ");
				query.append(" INNER JOIN macOrder." + MacOrder.PROPERTYNAME_BLOCKINGSHIPPINGREASON + " orderShippingBlockingReason ");
				query.append(" where customer." + MacCustomer.PROPERTYNAME_ID + " = :customerId");
				query.append(" and (documentStatus." + DocumentStatus.PROPERTYNAME_INTERNALID + " is null ");
				query.append("      or (documentStatus." + DocumentStatus.PROPERTYNAME_INTERNALID + " not in (").append(DocumentStatus.CANCEL).append(",")
						.append(DocumentStatus.CLOSE).append(")))");
				query.append(" and macOrder." + MacOrder.PROPERTYNAME_TOTAL + "." + MonetaryAmount.PROPERTYNAME_VALUE + " < :limit");
				query.append(" and macOrder." + MacOrder.PROPERTYNAME_BLOCKPRODUCTION + " = true");
				query.append(" and macOrder." + MacOrder.PROPERTYNAME_BLOCKSHIPPING + " = true ");
				query.append(" and orderProductionBlockingReason." + MacBlockingReason.PROPERTYNAME_ID + " = :automaticBlockingReasonId ");
				query.append(" and orderShippingBlockingReason." + MacBlockingReason.PROPERTYNAME_ID + " = :automaticBlockingReasonId ");
				query.append(" order by macOrder.total.value, macOrder.date");

				HashMap<String, Object> args = new HashMap<String, Object>();

				// TODO ajouter nullcheck
				args.put("customerId", customerId);
				args.put("limit", limit.getValue());
				args.put("automaticBlockingReasonId", automaticBlockingReason.getId().toString());

				List<Object[]> result = loader.findByQuery(query.toString(), args, false);

				if (result != null)
				{
					for (Object[] object : result)
					{
						Serializable orderId = (Serializable) object[0];
						MonetaryAmount amount = (MonetaryAmount) object[1];

						if (orderId != null && amount != null)
						{
							MacOrder macOrder = loader.findFirstByField(MacOrder.class, null, MacOrder.PROPERTYNAME_ID, orderId);

							if (macOrder != null && macOrder.getTotal().compareTo(limit.subtract(unblockedTotal)) < 0 && !macOrder.isProformaEffective())
							{
								macOrder.setEntityStatus(EntityStatus.READY);
								macOrder.resetToCustomerBlockProductionDefault(wfMaster.getUserWF());
								macOrder.resetToCustomerBlockShippingDefault(wfMaster.getUserWF());

								startUnblockAutoWF(macOrder);

								unblockedTotal = unblockedTotal.add(macOrder.getTotal().getValue());
							}
							else
							{
								return;
							}
						}
					}
				}
			}
		}
		return;
	}

	/**
	 * Demarre le WF wfMACUnblockAuto
	 * 
	 * @param order
	 *            Commande
	 * @throws Exception
	 *             Exception
	 */
	private void startUnblockAutoWF(MacOrder order) throws Exception
	{
		Map<String, Object> params = new HashMap<String, Object>();
		// The name of the bean (the object)
		params.put(BonitaParameters.ENTITY_CLASSNAME.toString(), Order.class.getName());
		// The name of the form i.e. the name of the XML without .xml
		params.put(BonitaParameters.ENTITY_FORMNAME.toString(), "order");
		// The uuid of the entity
		params.put(BonitaParameters.ENTITY_ID.toString(), order.getId().toString());
		// The refenrece label
		params.put(BonitaParameters.ENTITY_REFERENCE.toString(), "Commande " + order.getDocumentNumber());

		try
		{
			wfServices.initiateProcess(com.mac.custom.utils.Utils.getLoginName(), "wfMACUnblockAuto", params);
		}
		catch (Exception e)
		{
			logger.error("Probleme lors de l'appel du WF de déblocage automatique: " + e.getLocalizedMessage());
		}
	}

	// ******************************************

	@Override
	public boolean isARCReportValid(String documentId)
	{
		;

		SaleOrder saleOrder = loader.findById(SaleOrder.class, WorkflowARCUtilsDAO.class, documentId);

		// Est-ce que le document existe?
		if (saleOrder != null)
		{
			for (CommercialEntityDefaultReport defaultReport : saleOrder.getCustomer().getDefaultReports())
			{
				MacReport report = (MacReport) Model.getTarget(defaultReport.getReport());

				// Si c'est un ARC et que "envoyer email" ou "envoyer fax" est selectionne
				if (report.isArc() && (defaultReport.isEmailToDocumentContact() || defaultReport.isFaxToDocumentContact()))
				{
					// On valide si on a au moins un contact valide pour ce rapport
					for (CommercialEntityReportContact contact : defaultReport.getContacts())
					{
						if (contact.getCommunication() != null && contact.getCommunication().getValue() != null
								&& !contact.getCommunication().getValue().equals(""))
						{
							return true;
						}
					}
				}
			}
		}

		return false;
	}

	@Override
	public String getARCCommunications(String documentId, Class type)
	{
		SaleOrder saleOrder = loader.findById(SaleOrder.class, documentId);

		ArrayList<String> list = new ArrayList<String>();
		String result = "";

		// Est-ce que le document existe?
		if (saleOrder != null)
		{
			for (CommercialEntityDefaultReport defaultReport : saleOrder.getCustomer().getDefaultReports())
			{
				MacReport report = (MacReport) Model.getTarget(defaultReport.getReport());

				if (report.isArc()
						&& ((type.equals(Email.class) && defaultReport.isEmailToDocumentContact()) || (type.equals(Fax.class) && defaultReport
								.isFaxToDocumentContact())))
				{
					// On valide si on a au moins un contact valide pour ce rapport
					for (CommercialEntityReportContact contact : defaultReport.getContacts())
					{
						if (contact.getCommunication() != null && contact.getCommunication().getValue() != null
								&& !contact.getCommunication().getValue().equals(""))
						{
							if (Model.getTarget(contact.getCommunication()).getClass().equals(type))
							{
								if (type.equals(Fax.class))
								{
									String value = contact.getCommunication().getValue();

									value.replace("-", "");
									value.replace("(", "");
									value.replace(")", "");

									list.add(value + "@" + SetupUtils.INSTANCE.getMainSetup().getFaxDomain());
								}
								else
								{
									list.add(contact.getCommunication().getValue());
								}
							}
						}
					}
				}
			}
		}

		for (int cmp = 0; cmp < list.size(); cmp++)
		{
			result = result + list.get(cmp);

			if (cmp != list.size() - 1)
			{
				result = result + ",";
			}
		}

		return result;
	}

	@Override
	public void changeStatusToExplode(String documentId)
	{
		SaleOrder saleOrder = loader.findById(SaleOrder.class, documentId);

		MacWorkflowMaster wfMaster = WorkflowUtils.getWFMaster();

		if (saleOrder != null && wfMaster != null && wfMaster.getWfARC().getArcSentStatus().hasSameID(saleOrder.getDocumentStatus()))
		{
			changeDocumentStatus(documentId, documentStatusServices.getDocumentStatusFromInternalIds(DocumentStatus.TOEXPLODE).get(0));
		}
	}

	@Override
	public Set<String> getADVsActorsForSaleOrder(String documentId, boolean addAdmins)
	{
		Set<String> usersCode = new HashSet<String>();

		SaleTransaction saleTransaction = loader.findById(SaleTransaction.class, documentId);

		if (saleTransaction != null)
		{
			for (SaleTransactionSalesRep saleTransactionsalesRep : saleTransaction.getSalesReps())
			{
				SalesRepEuro salesRep = (SalesRepEuro) Model.getTarget(saleTransactionsalesRep.getSalesRep());

				if (salesRep != null && salesRep.getAdvUser() != null && !usersCode.contains(salesRep.getAdvUser().getCode()))
				{
					usersCode.add(salesRep.getAdvUser().getCode());
				}
			}
		}

		if (addAdmins)
		{
			return addWFAdmin(usersCode);
		}
		else
		{
			return usersCode;
		}
	}

	@Override
	public boolean isReportValid(String customerId)
	{
		MacCustomer customer = loader.findById(MacCustomer.class, customerId);

		// Est-ce que le document existe?
		if (customer != null && WorkflowUtils.getWFMaster() != null && WorkflowUtils.getWFMaster().getWfClientValidation() != null)
		{
			for (CommercialEntityDefaultReport defaultReport : customer.getDefaultReports())
			{
				MacReport report = (MacReport) Model.getTarget(defaultReport.getReport());

				if (report.hasSameID(WorkflowUtils.getWFMaster().getWfClientValidation().getReport())
						&& (defaultReport.isEmailToDocumentContact() || defaultReport.isFaxToDocumentContact()))
				{
					// On valide si on a au moins un contact valide pour ce rapport
					for (CommercialEntityReportContact contact : defaultReport.getContacts())
					{
						if (contact.getCommunication() != null && contact.getCommunication().getValue() != null
								&& !contact.getCommunication().getValue().equals(""))
						{
							return true;
						}
					}
				}
			}
		}

		return false;
	}

	@Override
	public Set<String> getADVsActorsForCustomer(String customerId, boolean addAdmins)
	{
		Set<String> usersCode = new HashSet<String>();

		MacCustomer customer = loader.findById(MacCustomer.class, customerId);

		if (customer != null)
		{
			for (CustomerSalesRep customerSalesRep : customer.getSalesReps())
			{
				CustomerSalesRepEuro salesRepEuro = (CustomerSalesRepEuro) Model.getTarget(customerSalesRep);

				if (salesRepEuro != null && salesRepEuro.getAdvUser() != null && !usersCode.contains(salesRepEuro.getAdvUser().getCode()))
				{
					usersCode.add(salesRepEuro.getAdvUser().getCode());
				}
			}
		}

		if (addAdmins)
		{
			return addWFAdmin(usersCode);
		}
		else
		{
			return usersCode;
		}
	}

	@Override
	public String getCommunications(String customerId, Class type)
	{
		MacCustomer customer = loader.findById(MacCustomer.class, customerId);

		ArrayList<String> list = new ArrayList<String>();
		String result = "";

		if (customer != null)
		{
			for (CommercialEntityDefaultReport defaultReport : customer.getDefaultReports())
			{
				MacReport report = (MacReport) Model.getTarget(defaultReport.getReport());

				if (report.hasSameID(WorkflowUtils.getWFMaster().getWfClientValidation().getReport())
						&& (defaultReport.isEmailToDocumentContact() || defaultReport.isFaxToDocumentContact()))
				{
					// On valide si on a au moins un contact valide pour ce rapport
					for (CommercialEntityReportContact contact : defaultReport.getContacts())
					{
						if (contact.getCommunication() != null && contact.getCommunication().getValue() != null
								&& !contact.getCommunication().getValue().equals(""))
						{
							if (Model.getTarget(contact.getCommunication()).getClass().equals(type))
							{
								if (type.equals(Fax.class))
								{
									String value = contact.getCommunication().getValue();

									value.replace("-", "");
									value.replace("(", "");
									value.replace(")", "");

									list.add(value + "@" + SetupUtils.INSTANCE.getMainSetup().getFaxDomain());
								}
								else
								{
									list.add(contact.getCommunication().getValue());
								}
							}
						}
					}
				}
			}
		}

		for (int cmp = 0; cmp < list.size(); cmp++)
		{
			result = result + list.get(cmp);

			if (cmp != list.size() - 1)
			{
				result = result + ",";
			}
		}

		return result;
	}

	@Override
	public void setDetailM5PAccepted(boolean isAccepted, String id, String internalNote, String comment, String confirmationNumber)
	{
		TransactionDetail transactionDetail = loader.findById(TransactionDetail.class, null, id);

		if (transactionDetail != null)
		{
			if (transactionDetail instanceof OrderDetailEuro)
			{
				OrderDetailEuro detailOrder = (OrderDetailEuro) transactionDetail;

				if (isAccepted)
				{
					detailOrder.setRareBirdAccepted(true);
					detailOrder.setRareBirdProcessedDate(new Date());
					if (confirmationNumber != null)
					{
						detailOrder.setRareBirdConfirmationNumber(confirmationNumber);
					}
				}
				else
				{
					detailOrder.setRareBirdAccepted(false);
					detailOrder.setRareBirdProcessedDate(new Date());
				}

				if (internalNote != null && !internalNote.equals(""))
				{
					if (detailOrder.getInternalNote() == null || detailOrder.getInternalNote().equals(""))
					{
						detailOrder.setInternalNote(internalNote);
					}
					else
					{
						detailOrder.setInternalNote(detailOrder.getInternalNote() + "\n\nDetail MP5:\n\n" + internalNote);
					}
				}

				if (comment != null && !comment.equals(""))
				{
					if (detailOrder.getComment() == null || detailOrder.getComment().equals(""))
					{
						detailOrder.setComment(comment);
					}
					else
					{
						detailOrder.setComment(detailOrder.getComment() + "\n\nCommentaire MP5:\n\n" + comment);
					}
				}
			}
			else if (transactionDetail instanceof QuotationDetailEuro)
			{
				QuotationDetailEuro detailQuotation = (QuotationDetailEuro) transactionDetail;

				if (isAccepted)
				{
					detailQuotation.setRareBirdAccepted(true);
					detailQuotation.setRareBirdProcessedDate(new Date());
					if (confirmationNumber != null)
					{
						detailQuotation.setRareBirdConfirmationNumber(confirmationNumber);
					}
				}
				else
				{
					detailQuotation.setRareBirdAccepted(false);
					detailQuotation.setRareBirdProcessedDate(new Date());
				}

				if (internalNote != null && !internalNote.equals(""))
				{
					if (detailQuotation.getInternalNote() == null || detailQuotation.getInternalNote().equals(""))
					{
						detailQuotation.setInternalNote(internalNote);
					}
					else
					{
						detailQuotation.setInternalNote(detailQuotation.getInternalNote() + "\n\nDetail MP5:\n\n" + internalNote);
					}
				}
			}
		}
	}

	@Override
	public void setDetailPrice(String id, String price)
	{
		SaleTransactionDetail detail = loader.findById(SaleTransactionDetail.class, null, id);

		if (detail != null && price != null && !price.equals(""))
		{
			String formattedPrice = price.replace(",", ".");
			formattedPrice = formattedPrice.replace(" ", "");

			if (detail instanceof OrderDetailEuro)
			{
				OrderDetailEuro detailEuro = (OrderDetailEuro) detail;

				if (detailEuro.getPrice() != null)
				{
					MonetaryAmount newAmout = new MonetaryAmount(detailEuro.getPrice().getCurrency());

					try
					{
						detailEuro.setApplyDiscount(false);
						newAmout.setValue(new BigDecimal(formattedPrice));
						detailEuro.setPrice(newAmout);
					}
					catch (NumberFormatException nfe)
					{}
				}
			}
			else if (detail instanceof QuotationDetailEuro)
			{
				QuotationDetailEuro detailEuro = (QuotationDetailEuro) detail;

				if (detailEuro.getPrice() != null)
				{
					MonetaryAmount newAmout = new MonetaryAmount(detailEuro.getPrice().getCurrency());

					try
					{
						detailEuro.setApplyDiscount(false);
						newAmout.setValue(new BigDecimal(formattedPrice));
						detailEuro.setPrice(newAmout);
					}
					catch (NumberFormatException nfe)
					{}
				}
			}
		}
	}

	@Override
	public boolean saleOrderDetailsAllValid(String id)
	{
		boolean allValid = true;

		SaleTransaction saleTransaction = loader.findById(SaleTransaction.class, null, id);

		if (saleTransaction != null)
		{
			for (TransactionDetail transactionDetail : saleTransaction.getDetails())
			{
				if (transactionDetail instanceof OrderDetailEuro)
				{
					OrderDetailEuro orderDetailEuro = (OrderDetailEuro) transactionDetail;

					if (orderDetailEuro.isRareBird() && orderDetailEuro.getRareBirdAccepted() != null && orderDetailEuro.getRareBirdAccepted() == false)
					{
						return false;
					}
					else if (orderDetailEuro.getRareBirdConfirmationNumber() == null && orderDetailEuro.getRareBirdProcessedDate() == null)
					{
						return false;
					}
				}
				else if (transactionDetail instanceof QuotationDetailEuro)
				{
					QuotationDetailEuro quotationDetailEuro = (QuotationDetailEuro) transactionDetail;

					if (quotationDetailEuro.isRareBird() && quotationDetailEuro.getRareBirdAccepted() != null
							&& quotationDetailEuro.getRareBirdAccepted() == false)
					{
						return false;
					}
					else if (quotationDetailEuro.getRareBirdConfirmationNumber() == null && quotationDetailEuro.getRareBirdProcessedDate() == null)
					{
						return false;
					}
				}

			}
		}

		return allValid;
	}

	@Override
	public String getTransactionDetailIdentifier(String id)
	{
		TransactionDetail detail = loader.findById(TransactionDetail.class, null, id);

		if (detail != null && detail.getIdentifier() != null)
		{
			return detail.getIdentifier().toString();
		}

		return "";
	}

	@Override
	public void changeDocumentStatus(String documentId, String newStatus)
	{
		SaleOrder saleOrder = loader.findById(SaleOrder.class, documentId);

		if (saleOrder != null)
		{
			List<DocumentStatus> docStatusList = loader.findByField(DocumentStatus.class, DocumentStatus.class, DocumentStatus.PROPERTYNAME_CODE, newStatus);

			if (docStatusList != null && docStatusList.size() > 0)
			{
				changeDocumentStatus(documentId, docStatusList.get(0));
			}
			else
			{
				// TODO
			}
		}
	}

	@Override
	public void changeDocumentStatus(String documentId, DocumentStatus newStatus)
	{
		SaleOrder saleOrder = loader.findById(SaleOrder.class, documentId);

		if (saleOrder != null && newStatus != saleOrder.getDocumentStatus())
		{
			saleOrder.setEntityStatus(EntityStatus.READY);

			DocumentInformationStatusUtils.setNewStatusHistory(saleOrder);
			DocumentInformationStatusUtils.adjustDocumentStatusHistory(saleOrder, newStatus, saleOrder.getNewStatusHistory());

			// sets the first communicated shipping date if the status requires it
			if (MacOrder.class.isAssignableFrom(saleOrder.getClass()))
			{
				MacOrder macOrder = Model.getTarget(saleOrder);
				MacDocumentStatus macDocumentStatus = Model.getTarget(newStatus);
				if (macOrder.getFirstCommunicatedShippingDate() == null && macDocumentStatus.isSaveFirstCommunicatedShippingDateRequired())
				{
					macOrder.setFirstCommunicatedShippingDate(macOrder.getShippingDate());
				}
			}
			saleOrder.setDocumentStatus(newStatus);
		}
	}

	@Override
	public MacWorkflowMaster getWFMaster()
	{
		List<MacWorkflowMaster> wfMaster = loader.findAll(MacWorkflowMaster.class, MacWorkflowMaster.class);

		if (wfMaster != null && !wfMaster.isEmpty())
		{
			return wfMaster.get(0);
		}

		return null;
	}

	@Override
	public Set<String> getAllActors()
	{
		Set<String> usersCode = new HashSet<String>();

		List<SecurityUser> users = loader.findAll(SecurityUser.class, SecurityUser.class);

		for (SecurityUser securityUser : users)
		{
			usersCode.add(securityUser.getCode());
		}

		return usersCode;
	}

	@Override
	public Set<String> getAllActorsForGroup(String groupName, boolean addAdmins)
	{
		Set<String> usersCode = new HashSet<String>();

		List<SecurityGroup> groups = loader.findByField(SecurityGroup.class, SecurityGroup.class, SecurityGroup.PROPERTYNAME_CODE, groupName);

		if (groups != null && groups.size() > 0)
		{
			List<SecurityUser> users = groups.get(0).getUsers();

			for (SecurityUser securityUser : users)
			{
				usersCode.add(securityUser.getCode());
			}
		}

		if (addAdmins)
		{
			return addWFAdmin(usersCode);
		}
		else
		{
			return usersCode;
		}
	}

	@Override
	public Set<String> addWFAdmin(Set<String> currentUsers)
	{
		SecurityGroup securityGroup = loader.findFirstByField(SecurityGroup.class, SecurityGroup.class, SecurityGroup.PROPERTYNAME_CODE, WF_ADMIN);

		if (securityGroup != null)
		{
			List<SecurityUser> users = securityGroup.getUsers();

			for (SecurityUser securityUser : users)
			{
				if (!currentUsers.contains(securityUser.getCode()))
				{
					currentUsers.add(securityUser.getCode());
				}
			}
		}

		return currentUsers;
	}

	@Override
	public String getADVsEmailAddressesForSaleOrder(String documentId)
	{
		SaleTransaction saleTransaction = loader.findById(SaleTransaction.class, documentId);

		ArrayList<String> list = new ArrayList<String>();
		String result = "";

		if (saleTransaction != null)
		{
			for (SaleTransactionSalesRep saleTransactionsalesRep : saleTransaction.getSalesReps())
			{
				SalesRepEuro salesRep = (SalesRepEuro) Model.getTarget(saleTransactionsalesRep.getSalesRep());

				if (salesRep != null && salesRep.getAdvUser() != null)
				{
					UserEuro advUser = salesRep.getAdvUser();

					for (Communication comm : advUser.getCommunications())
					{
						if (comm.getValue() != null && !comm.getValue().equals("") && comm.getType() != null && comm.getType().equals(EMAIL)
								&& !list.contains(comm.getValue()))
						{
							list.add(comm.getValue());
						}
					}
				}
			}
		}

		for (int cmp = 0; cmp < list.size(); cmp++)
		{
			result = result + list.get(cmp);

			if (cmp != list.size() - 1)
			{
				result = result + ",";
			}
		}

		return result;
	}

	@Override
	public String getATCsEmailAddressesForSaleOrder(String documentId)
	{
		SaleTransaction saleTransaction = loader.findById(SaleTransaction.class, documentId);

		ArrayList<String> list = new ArrayList<String>();
		String result = "";

		if (saleTransaction != null)
		{
			for (SaleTransactionSalesRep saleTransactionsalesRep : saleTransaction.getSalesReps())
			{
				SalesRepEuro salesRep = (SalesRepEuro) Model.getTarget(saleTransactionsalesRep.getSalesRep());

				if (salesRep != null && salesRep.getAdvUser() != null)
				{
					for (Communication comm : salesRep.getCommunications())
					{
						if (comm.getValue() != null && !comm.getValue().equals("") && comm.getType() != null && comm.getType().equals(EMAIL)
								&& !list.contains(comm.getValue()))
						{
							list.add(comm.getValue());
						}
					}
				}
			}
		}

		for (int cmp = 0; cmp < list.size(); cmp++)
		{
			result = result + list.get(cmp);

			if (cmp != list.size() - 1)
			{
				result = result + ",";
			}
		}

		return result;
	}

	@Override
	public String getADVsEmailAddressesForCustomer(String customerId)
	{
		MacCustomer customer = loader.findById(MacCustomer.class, customerId);

		ArrayList<String> list = new ArrayList<String>();
		String result = "";

		if (customer != null)
		{
			for (CustomerSalesRep csr : customer.getSalesReps())
			{
				CustomerSalesRepEuro csrEuro = (CustomerSalesRepEuro) Model.getTarget(csr);

				if (csrEuro != null && csrEuro.getAdvUser() != null)
				{
					UserEuro advUser = csrEuro.getAdvUser();

					for (Communication comm : advUser.getCommunications())
					{
						if (comm.getValue() != null && !comm.getValue().equals("") && comm.getType() != null && comm.getType().equals(EMAIL)
								&& !list.contains(comm.getValue()))
						{
							list.add(comm.getValue());
						}
					}
				}
			}
		}

		for (int cmp = 0; cmp < list.size(); cmp++)
		{
			result = result + list.get(cmp);

			if (cmp != list.size() - 1)
			{
				result = result + ",";
			}
		}

		return result;
	}

	@Override
	public String getATCsEmailAddressesForCustomer(String customerId)
	{
		MacCustomer customer = loader.findById(MacCustomer.class, customerId);

		ArrayList<String> list = new ArrayList<String>();
		String result = "";

		if (customer != null)
		{
			for (CustomerSalesRep csr : customer.getSalesReps())
			{
				SalesRepEuro salesRep = (SalesRepEuro) Model.getTarget(csr.getSalesRep());

				if (salesRep != null && salesRep.getAdvUser() != null)
				{
					for (Communication comm : salesRep.getCommunications())
					{
						if (comm.getValue() != null && !comm.getValue().equals("") && comm.getType() != null && comm.getType().equals(EMAIL)
								&& !list.contains(comm.getValue()))
						{
							list.add(comm.getValue());
						}
					}
				}
			}
		}

		for (int cmp = 0; cmp < list.size(); cmp++)
		{
			result = result + list.get(cmp);

			if (cmp != list.size() - 1)
			{
				result = result + ",";
			}
		}

		return result;
	}
}
