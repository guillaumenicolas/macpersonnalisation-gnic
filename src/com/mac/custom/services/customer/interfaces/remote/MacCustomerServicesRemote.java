package com.mac.custom.services.customer.interfaces.remote;

import javax.ejb.Remote;

import com.mac.custom.services.customer.interfaces.MacCustomerServices;

@Remote
public interface MacCustomerServicesRemote extends MacCustomerServices
{
}
