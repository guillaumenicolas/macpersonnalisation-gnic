package com.mac.custom.services.customer.interfaces.remote;

import javax.ejb.Remote;

import com.mac.custom.services.customer.interfaces.DeclencheursEnCoursServices;

@Remote
public interface DeclencheursEnCoursServicesRemote extends DeclencheursEnCoursServices
{
}
