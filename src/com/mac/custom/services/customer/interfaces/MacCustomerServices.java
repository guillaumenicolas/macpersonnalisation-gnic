package com.mac.custom.services.customer.interfaces;

import java.io.Serializable;
import java.util.List;

import com.mac.custom.bo.CustomerLedgerEntry;
import com.mac.custom.bo.MacCustomer;
import com.netappsid.datatypes.MonetaryAmount;

public interface MacCustomerServices
{
	/**
	 * Gets total invoiced for the corresponding customer and lag year.
	 * 
	 * @param customer
	 *            customer
	 * @param lagYear
	 *            e.g. 0 - current year, -1 last year, -2 last last year... 1 next year and so on...
	 * @return a MonetaryAmount containing the total in question.
	 */
	MonetaryAmount getTotalMontantFactureesParAnneeLag(MacCustomer customer, int lagYear);

	/**
	 * Gets total invoiced for the corresponding customer and specific year.
	 * 
	 * @param customer
	 *            customer
	 * @param year
	 *            the year (e.g 2013)
	 * @return a MonetaryAmount containing the total in question.
	 */
	MonetaryAmount getTotalMontantFactureesParAnnee(MacCustomer customer, int year);

	List<CustomerLedgerEntry> fetchCustomerLegderEntry(String customerCode, String currencyCode);

	public MonetaryAmount getTotalShippedNotInvoiced(MacCustomer macCustomer);

	public MonetaryAmount getTotalInvoicesNotPaid(MacCustomer macCustomer);

	public MonetaryAmount getTotalAmountOfOrdersByCriteria(MacCustomer macCustomer, Boolean newAndIncomplete, Boolean blocked, Boolean exploded,
			Boolean shipped, Serializable orderToIgnoreId);
}