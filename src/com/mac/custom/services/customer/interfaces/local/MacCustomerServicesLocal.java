package com.mac.custom.services.customer.interfaces.local;

import javax.ejb.Local;

import com.mac.custom.services.customer.interfaces.MacCustomerServices;

@Local
public interface MacCustomerServicesLocal extends MacCustomerServices
{
}
