package com.mac.custom.services.customer.interfaces.local;

import java.io.Serializable;

import javax.ejb.Local;

import com.mac.custom.services.customer.interfaces.DeclencheursEnCoursServices;

@Local
public interface DeclencheursEnCoursServicesLocal extends DeclencheursEnCoursServices
{
	/**
	 * Cette methode ajoutera une nouveau declencheur dans la table DeclencheursEnCours, les entrees de cette table seront ensuite traitees par le webservice.
	 * 
	 * @param entityClassName
	 *            type de l'entite qui vient d'etre modifie
	 * @param entityId
	 *            id de l'entite qui vient d'etre modifie
	 */
	public void ajouterDeclencheurEnCours(String entityClassName, Serializable entityId);

	public void ajouterDeclencheurEnCours(Serializable documentId);
}
