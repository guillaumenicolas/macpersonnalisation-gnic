package com.mac.custom.services.customer.beans;

import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.net.Authenticator;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.xml.namespace.QName;

import org.apache.log4j.Logger;

import com.mac.custom.bo.CustomerLedgerEntry;
import com.mac.custom.bo.MacBlockingReason;
import com.mac.custom.bo.MacCustomer;
import com.mac.custom.services.custledgerentry.CredentialProviderAuthenticator;
import com.mac.custom.services.custledgerentry.CustLedgerEntry;
import com.mac.custom.services.custledgerentry.CustLedgerEntryFields;
import com.mac.custom.services.custledgerentry.CustLedgerEntryFilter;
import com.mac.custom.services.custledgerentry.CustLedgerEntryList;
import com.mac.custom.services.custledgerentry.CustLedgerEntryPort;
import com.mac.custom.services.custledgerentry.CustLedgerEntryService;
import com.mac.custom.services.customer.interfaces.local.MacCustomerServicesLocal;
import com.mac.custom.services.customer.interfaces.remote.MacCustomerServicesRemote;
import com.netappsid.datatypes.MonetaryAmount;
import com.netappsid.erp.server.bo.Customer;
import com.netappsid.erp.server.bo.DocumentStatus;
import com.netappsid.erp.server.bo.ItemToShip;
import com.netappsid.erp.server.bo.Order;
import com.netappsid.erp.server.bo.ShippingDetail;
import com.netappsid.erp.server.services.interfaces.local.CustomerServicesLocal;
import com.netappsid.services.interfaces.local.LoaderLocal;

@Stateless
public class MacCustomerServicesBean implements MacCustomerServicesLocal, MacCustomerServicesRemote
{
	@EJB
	private LoaderLocal loader;

	@EJB
	private CustomerServicesLocal customerServicesLocal;

	private static final Logger logger = Logger.getLogger(MacCustomerServicesBean.class);

	public MacCustomerServicesBean()
	{}

	public MacCustomerServicesBean(LoaderLocal loaderServices)
	{
		this.loader = loaderServices;
	}

	/**
	 * Convenient function for calculating a targeted year based on a lag parameter.
	 * 
	 * @param lagYear
	 * @return targeted year
	 */
	private int getTargetedYear(int lagYear)
	{
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		int targetedYear = currentYear + lagYear;

		return targetedYear;
	}

	@Override
	public MonetaryAmount getTotalMontantFactureesParAnneeLag(MacCustomer customer, int lagYear)
	{
		return getTotalMontantFactureesParAnnee(customer, getTargetedYear(lagYear));
	}

	@Override
	public MonetaryAmount getTotalMontantFactureesParAnnee(MacCustomer customer, int year)
	{
		if (customer == null || customer.getId() == null)
		{
			return null;
		}

		Map<String, Object> parameters = new HashMap<String, Object>();

		StringBuilder query = new StringBuilder();
		query.append("select sum(saleInvoice.total.value) ");
		query.append("from SaleInvoice saleInvoice ");
		query.append("where saleInvoice.customer.id = :customerId ");
		query.append("   and saleInvoice.reversed = false ");
		query.append("   and saleInvoice.reverse is null ");
		query.append("   and YEAR(saleInvoice.date) = :targetedYear ");

		parameters.put("customerId", customer.getId().toString());
		parameters.put("targetedYear", year);

		BigDecimal total = loader.findByQuery(query.toString(), parameters, true);

		return total == null ? new MonetaryAmount(BigDecimal.ZERO, customer.getCurrency().getCode()) : new MonetaryAmount(total, customer.getCurrency()
				.getCode());
	}

	@Override
	public List<CustomerLedgerEntry> fetchCustomerLegderEntry(String customerCode, String currencyCode)
	{
		URL systemServiceURL;
		List<CustomerLedgerEntry> list = new ArrayList<CustomerLedgerEntry>();
		try
		{
			if (System.getProperty("naid.dev") != null && System.getProperty("naid.dev").equals("true"))
			{
				systemServiceURL = new URL("file:" + System.getProperty("wsdl.path"));
			}
			else
			{
				systemServiceURL = new URL("file:" + System.getProperty("jboss.server.data.dir") + File.separator + "Navision.wsdl");
			}
			QName systemServiceQName = new QName("urn:microsoft-dynamics-schemas/page/custledgerentry", "CustLedgerEntry_Service");
			Authenticator.setDefault(new CredentialProviderAuthenticator());
			CustLedgerEntryService service = new CustLedgerEntryService(systemServiceURL, systemServiceQName);
			CustLedgerEntryPort serviceport = service.getCustLedgerEntryPort();
			List<CustLedgerEntryFilter> filters = new ArrayList<CustLedgerEntryFilter>();
			CustLedgerEntryFilter filterCustomerNo = new CustLedgerEntryFilter();
			CustLedgerEntryFilter filterOpen = new CustLedgerEntryFilter();
			filterCustomerNo.setField(CustLedgerEntryFields.CUSTOMER_NO);
			filterCustomerNo.setCriteria(customerCode);
			filterOpen.setField(CustLedgerEntryFields.OPEN);
			filterOpen.setCriteria("true");
			filters.add(filterCustomerNo);
			filters.add(filterOpen);
			CustLedgerEntryList results = serviceport.readMultiple(filters, null, 1000);
			for (CustLedgerEntry cust : results.getCustLedgerEntry())
			{
				list.add(new CustomerLedgerEntry(cust.getDocumentNo(), cust.getAmount(), cust.getPostingDate().toGregorianCalendar().getTime(), cust
						.getDueDate().toGregorianCalendar().getTime(), currencyCode));
			}
		}
		catch (MalformedURLException e)
		{
			logger.error(e, e);
		}
		return list;
	}

	@Override
	public MonetaryAmount getTotalShippedNotInvoiced(MacCustomer macCustomer)
	{
		return customerServicesLocal.getCustomerTotalOfShippingsNotInvoiced(macCustomer);
	}

	@Override
	public MonetaryAmount getTotalInvoicesNotPaid(MacCustomer macCustomer)
	{
		return customerServicesLocal.getCustomerTotalOfInvoicesNotPaid(macCustomer);
	}

	@Override
	public MonetaryAmount getTotalAmountOfOrdersByCriteria(MacCustomer macCustomer, Boolean newAndIncomplete, Boolean blocked, Boolean exploded,
			Boolean shipped, Serializable orderToIgnoreId)
	{
		Serializable customerId = null;

		if (macCustomer != null && macCustomer.getId() != null)
		{
			customerId = macCustomer.getId();
		}

		StringBuilder hqlQuery = new StringBuilder();
		hqlQuery.append("SELECT ord.{total}.value").append("\n");
		hqlQuery.append("FROM ");
		hqlQuery.append("MacOrder ord ");
		hqlQuery.append("INNER JOIN ord.{customer} customer ");
		hqlQuery.append("INNER JOIN ord.{documentStatus} documentStatus ");
		hqlQuery.append("INNER JOIN ord.{currency} currency ");
		hqlQuery.append("LEFT JOIN ord.{itemsToShip} itemToShip ");
		hqlQuery.append("LEFT JOIN itemToShip.{shippings} shippingDetail ");
		hqlQuery.append("LEFT JOIN ord.{blockingProductionReason} orderProductionBlockingReason ");
		hqlQuery.append("LEFT JOIN ord.{blockingShippingReason} orderShippingBlockingReason ");
		hqlQuery.append("LEFT JOIN customer.{blockingProductionReason} customerProductionBlockingReason ");
		hqlQuery.append("LEFT JOIN customer.{blockingShippingReason} customerShippingBlockingReason ");

		hqlQuery.append("WHERE ");
		hqlQuery.append("ord.{total}.currency = ord.{customer}.{customercurrency}.code").append("\n");

		// Ignorer la commande sp�cifi�e, s'il y'a lieu
		if (orderToIgnoreId != null)
		{
			hqlQuery.append("AND ord.id <> :orderToIgnoreId").append("\n");
		}

		if (newAndIncomplete == null || !newAndIncomplete)
		{
			// Ignorer les commandes nouvelles et incompl�tes � MOINS qu'elles soient bloqu�es
			hqlQuery.append("AND (");
			hqlQuery.append("documentStatus.{documentStatusInternalId} NOT IN (").append(DocumentStatus.CANCEL).append(",").append(DocumentStatus.CLOSE)
					.append(",").append(DocumentStatus.NEW).append(",").append(DocumentStatus.INCOMPLETE).append(") ");
			hqlQuery.append("OR documentStatus.{documentStatusInternalId} IS NULL ");
			hqlQuery.append("OR (documentStatus.{documentStatusInternalId} IN (").append(DocumentStatus.NEW).append(",").append(DocumentStatus.INCOMPLETE)
					.append(") ");
			hqlQuery.append("AND ((ord.{blockProduction} = true OR (ord.{blockProduction} = null AND customer.{blockProduction} = true))");
			hqlQuery.append("      OR (ord.{blockShipping} = true OR (ord.{blockShipping} = null AND customer.{blockShipping} = true)))").append(")");
			hqlQuery.append(") \n");
		}
		else
		{
			// Ne consid�rer que les commandes nouvelles et incompl�tes � MOINS qu'elles soient bloqu�es
			hqlQuery.append("AND (documentStatus.{documentStatusInternalId} IN (").append(DocumentStatus.NEW).append(",").append(DocumentStatus.INCOMPLETE)
					.append(") ");
			hqlQuery.append("AND (ord.{blockProduction} = false OR (ord.{blockProduction} = null AND customer.{blockProduction} = false)) ");
			hqlQuery.append("AND (ord.{blockShipping} = false OR (ord.{blockShipping} = null AND customer.{blockShipping} = false)) ");
			hqlQuery.append(") \n");
		}

		if (shipped != null)
		{
			if (!shipped.booleanValue())
			{
				hqlQuery.append("AND itemToShip.{planned} is empty ");
				hqlQuery.append("AND itemToShip.{palletLoad} is empty ");
			}
			else
			{
				hqlQuery.append("AND itemToShip.{planned} is not empty ");
				hqlQuery.append("AND itemToShip.{palletLoad} is not empty ");
			}
		}

		if (customerId != null)
		{
			hqlQuery.append("AND ord.{customer}.{customerid} = :customerId ");
		}

		if (exploded != null)
		{
			String operator = exploded.booleanValue() ? "<>" : "=";

			hqlQuery.append("AND (SELECT COUNT(od.id) ");
			hqlQuery.append("   FROM Order ord2 LEFT JOIN ord2.details od ");
			hqlQuery.append("   WHERE ord2.id = ord.id ");
			hqlQuery.append("   AND od.dateExploded is not null) ").append(operator).append(" 0 ").append("\n");
		}

		if (blocked != null)
		{
			if (blocked.booleanValue())
			{
				if (!exploded.booleanValue())
				{
					hqlQuery.append("AND (orderProductionBlockingReason.{excludeFormOrdersInProductionTotal} IS NULL OR orderProductionBlockingReason.{excludeFormOrdersInProductionTotal} = false) ");
					hqlQuery.append("AND (orderShippingBlockingReason.{excludeFormOrdersInProductionTotal} IS NULL OR orderShippingBlockingReason.{excludeFormOrdersInProductionTotal} = false) ");
					hqlQuery.append("AND (customerProductionBlockingReason.{excludeFormOrdersInProductionTotal} IS NULL OR customerProductionBlockingReason.{excludeFormOrdersInProductionTotal} = false) ");
					hqlQuery.append("AND (customerShippingBlockingReason.{excludeFormOrdersInProductionTotal} IS NULL OR customerShippingBlockingReason.{excludeFormOrdersInProductionTotal} = false) ");
				}
				hqlQuery.append("AND ((ord.{blockProduction} = true OR (ord.{blockProduction} = null AND customer.{blockProduction} = true))");
				hqlQuery.append("    OR (ord.{blockShipping} = true OR (ord.{blockShipping} = null AND customer.{blockShipping} = true)))");
			}
			else
			{
				hqlQuery.append("AND (ord.{blockProduction} = false OR (ord.{blockProduction} = null AND customer.{blockProduction} = false))");
				hqlQuery.append("AND (ord.{blockShipping} = false OR (ord.{blockShipping} = null AND customer.{blockShipping} = false))");
			}
		}

		if (shipped != null)
		{
			if (!shipped.booleanValue())
			{
				hqlQuery.append("AND (select coalesce(sum(innerItemToShip.{itemToShipQuantityConvertedValue}), 0) ");
				hqlQuery.append("     from ItemToShip innerItemToShip where innerItemToShip.{itemToShipId}=itemToShip.{itemToShipId}) ");
				hqlQuery.append(" >= (select coalesce(sum(innerShippingDetail.{shippingDetailQuantityConvertedValue}), 0) ");
				hqlQuery.append("     from ShippingDetail innerShippingDetail where innerShippingDetail.{shippingDetailId}=shippingDetail.{shippingDetailId}) ");
			}
			else
			{
				hqlQuery.append("AND (select coalesce(sum(innerItemToShip.{itemToShipQuantityConvertedValue}), 0) ");
				hqlQuery.append("     from ItemToShip innerItemToShip where innerItemToShip.{itemToShipId}=itemToShip.{itemToShipId}) ");
				hqlQuery.append(" <  (select coalesce(sum(innerShippingDetail.{shippingDetailQuantityConvertedValue}), 0) ");
				hqlQuery.append("     from ShippingDetail innerShippingDetail where innerShippingDetail.{shippingDetailId}=shippingDetail.{shippingDetailId}) ");
			}
		}
		// hqlQuery.append(") ");
		hqlQuery.append("GROUP BY ord.id, ord.{total}.value");

		// the map for tokens substitutions (for easing re-factoring of fields, if happens)
		Map<String, String> tokenParams = new HashMap<String, String>();
		tokenParams.put("total", Order.PROPERTYNAME_TOTAL);
		tokenParams.put("customer", Order.PROPERTYNAME_CUSTOMER);
		tokenParams.put("documentStatus", Order.PROPERTYNAME_DOCUMENTSTATUS);
		tokenParams.put("currency", Order.PROPERTYNAME_CURRENCY);
		tokenParams.put("itemsToShip", Order.PROPERTYNAME_ITEMSTOSHIP);
		tokenParams.put("shippings", ItemToShip.PROPERTYNAME_SHIPPINGS);
		tokenParams.put("customercurrency", Customer.PROPERTYNAME_CURRENCY);
		tokenParams.put("planned", ItemToShip.PROPERTYNAME_PLANNED);
		tokenParams.put("palletLoad", ItemToShip.PROPERTYNAME_PALLETLOAD);
		tokenParams.put("customerid", Customer.PROPERTYNAME_ID);
		tokenParams.put("documentStatusInternalId", DocumentStatus.PROPERTYNAME_INTERNALID);
		tokenParams.put("blockProduction", Customer.PROPERTYNAME_BLOCKPRODUCTION);
		tokenParams.put("blockProduction", Order.PROPERTYNAME_BLOCKPRODUCTION);
		tokenParams.put("blockShipping", Customer.PROPERTYNAME_BLOCKSHIPPING);
		tokenParams.put("blockShipping", Order.PROPERTYNAME_BLOCKSHIPPING);
		tokenParams.put("itemToShipQuantityConvertedValue", ItemToShip.PROPERTYNAME_QUANTITYCONVERTEDVALUE);
		tokenParams.put("shippingDetailQuantityConvertedValue", ShippingDetail.PROPERTYNAME_QUANTITYCONVERTEDVALUE);
		tokenParams.put("itemToShipId", ItemToShip.PROPERTYNAME_ID);
		tokenParams.put("shippingDetailId", ShippingDetail.PROPERTYNAME_ID);
		tokenParams.put("blockingProductionReason", Order.PROPERTYNAME_BLOCKINGPRODUCTIONREASON);
		tokenParams.put("blockingShippingReason", Order.PROPERTYNAME_BLOCKINGSHIPPINGREASON);
		tokenParams.put("excludeFormOrdersInProductionTotal", MacBlockingReason.PROPERTYNAME_EXCLUDEFROMORDERINPRODUCTIONTOTAL);

		String hql = replaceTokens(hqlQuery.toString(), tokenParams);

		// executing the query
		Map<String, Object> parameters = new HashMap<String, Object>();
		if (customerId != null)
		{
			parameters.put("customerId", macCustomer.getId().toString());
		}
		if (orderToIgnoreId != null)
		{
			parameters.put("orderToIgnoreId", orderToIgnoreId);
		}
		List<BigDecimal> totals = loader.findByQuery(hql, parameters, false);

		BigDecimal total = BigDecimal.ZERO;
		if (total != null)
		{
			for (BigDecimal orderTotal : totals)
			{
				total = total.add(orderTotal);
			}
		}

		return new MonetaryAmount(total, macCustomer.getCurrency().getCode());
	}

	/**
	 * Convenient method for replacing tokens in a query.
	 * 
	 * @param source
	 *            the String where tokens will be replaced.
	 * @param params
	 *            the Map containing the substitutions.
	 * @return the modified source having all tokens replaced.
	 */
	private String replaceTokens(String source, Map<String, String> params)
	{
		Pattern tokenPattern = Pattern.compile("\\{([^}]*)\\}");
		StringBuffer sb = new StringBuffer();
		Matcher myMatcher = tokenPattern.matcher(source);

		while (myMatcher.find())
		{
			String field = myMatcher.group(1);
			myMatcher.appendReplacement(sb, "");
			sb.append(params.get(field));
		}

		myMatcher.appendTail(sb);

		return sb.toString();
	}
}
