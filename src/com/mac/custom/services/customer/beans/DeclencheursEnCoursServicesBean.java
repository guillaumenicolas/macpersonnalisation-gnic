package com.mac.custom.services.customer.beans;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.mac.custom.bo.DeclencheurEnCours;
import com.mac.custom.factory.DeclencheurEnCoursFactory;
import com.mac.custom.services.customer.interfaces.local.DeclencheursEnCoursServicesLocal;
import com.mac.custom.services.customer.interfaces.remote.DeclencheursEnCoursServicesRemote;
import com.netappsid.bo.model.Model;
import com.netappsid.erp.server.bo.Customer;
import com.netappsid.erp.server.bo.ExplodableDocument;
import com.netappsid.erp.server.bo.SaleTransaction;
import com.netappsid.services.interfaces.local.LoaderLocal;
import com.netappsid.services.interfaces.local.PersistenceLocal;

@Stateless
public class DeclencheursEnCoursServicesBean implements DeclencheursEnCoursServicesLocal, DeclencheursEnCoursServicesRemote
{
	@EJB
	private PersistenceLocal persistenceLocal;

	@EJB
	private LoaderLocal loaderLocal;

	@PersistenceContext
	private EntityManager manager;

	@Override
	public void ajouterDeclencheurEnCours(String entityClassName, Serializable entityId)
	{
		DeclencheurEnCoursFactory declencheurEnCoursFactory = new DeclencheurEnCoursFactory();
		DeclencheurEnCours declencheurEnCours = declencheurEnCoursFactory.create(entityClassName, entityId);
		persistenceLocal.save(declencheurEnCours, false);
	}

	@Override
	public void ajouterDeclencheurEnCours(Serializable documentId)
	{
		ExplodableDocument explodableDocument = loaderLocal.findById(ExplodableDocument.class, null, documentId);
		explodableDocument = Model.getTarget(explodableDocument);
		if (explodableDocument instanceof SaleTransaction)
		{
			Customer customer = ((SaleTransaction) explodableDocument).getCustomer();
			ajouterDeclencheurEnCours(Model.getTarget(customer).getClass().getCanonicalName(), customer.getId());
		}
	}

}
