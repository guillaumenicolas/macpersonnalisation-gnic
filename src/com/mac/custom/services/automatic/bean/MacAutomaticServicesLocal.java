package com.mac.custom.services.automatic.bean;

import javax.ejb.Local;

import com.mac.custom.services.automatic.interfaces.MacAutomaticServices;

@Local
public interface MacAutomaticServicesLocal extends MacAutomaticServices {

}
