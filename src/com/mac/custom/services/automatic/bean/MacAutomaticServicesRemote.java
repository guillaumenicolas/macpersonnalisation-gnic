package com.mac.custom.services.automatic.bean;

import javax.ejb.Remote;

import com.mac.custom.services.automatic.interfaces.MacAutomaticServices;

@Remote
public interface MacAutomaticServicesRemote extends MacAutomaticServices
{

	public void generateAutomaticProductionBatch();

	public void generateAutomaticDailyQuotationsBatch();

}
