package com.mac.custom.services.automatic.bean;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.mac.custom.listmodel.MacProductionPlanning;
import com.mac.custom.listmodel.MacWorkOrderRoutingStepList;
import com.mac.custom.services.report.interfaces.local.MacReportServicesLocal;
import com.netappsid.erp.server.bo.ProductionPlanningTemplate;
import com.netappsid.erp.server.bo.Setup;
import com.netappsid.erp.server.bo.User;
import com.netappsid.erp.server.services.interfaces.local.ProductionPlanningServicesLocal;
import com.netappsid.erp.server.services.utils.SetupUtils;
import com.netappsid.services.interfaces.local.LoaderLocal;

@Stateless
public class MacAutomaticServicesBean implements MacAutomaticServicesLocal, MacAutomaticServicesRemote
{

	@EJB
	private LoaderLocal loader;

	@EJB
	private ProductionPlanningServicesLocal productionPlanningServicesLocal;

	@EJB
	private MacReportServicesLocal macReportServicesLocal;

	@Override
	public void generateAutomaticProductionBatch()
	{
		Setup setup = SetupUtils.INSTANCE.getMainSetup();
		User user = setup.getAutomaticServiceUser();

		if (user != null)
		{
			MacProductionPlanning productionPlanning = new MacProductionPlanning();
			productionPlanning.setWorkOrderRoutingStepList(new MacWorkOrderRoutingStepList());
			productionPlanning.addManyToProductionPlanningTemplates(loader.findAll(ProductionPlanningTemplate.class, null));
			productionPlanning.getWorkOrderRoutingStepList().setExcludeBundle(true);
			productionPlanning.getMacWorkOrderRoutingStepList().setExplodedOrderOnly(true);
			productionPlanningServicesLocal.generateAutomaticProductionBatches(productionPlanning, user, null, true);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public void generateAutomaticDailyQuotationsBatch()
	{
		Setup setup = SetupUtils.INSTANCE.getMainSetup();
		User user = setup.getAutomaticServiceUser();

		if (user != null)
		{
			try
			{
				macReportServicesLocal.runTodaysQuotationPDF();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

}
