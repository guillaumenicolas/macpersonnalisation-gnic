package com.mac.custom.services.workCalendar.interfaces.local;

import javax.ejb.Local;

import com.mac.custom.services.workCalendar.interfaces.WorkCalendarServices;

@Local
public interface WorkCalendarServicesLocal extends WorkCalendarServices
{

}
