package com.mac.custom.services.workCalendar.interfaces.remote;

import javax.ejb.Remote;

import com.mac.custom.services.workCalendar.interfaces.WorkCalendarServices;

@Remote
public interface WorkCalendarServicesRemote extends WorkCalendarServices
{

}
