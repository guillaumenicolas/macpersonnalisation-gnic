package com.mac.custom.services.workCalendar.interfaces;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.mac.custom.bo.WorkCalendarDetail;
import com.netappsid.erp.server.services.resultholders.ResultHolder;

public interface WorkCalendarServices
{
	public ResultHolder<WorkCalendarDetail> find(Date dateFrom, Date dateTo, Serializable workShiftId, Serializable resourceId, Serializable macOrderId,
			Serializable macCustomerId);

	public void deleteResourceEfficiencies(List<Serializable> resourceEfficiencyIds);
}
