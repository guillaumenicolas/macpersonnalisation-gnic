package com.mac.custom.services.workCalendar.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import com.mac.custom.bo.WorkCalendarDetail;
import com.mac.custom.bo.WorkLoad;
import com.mac.custom.factory.WorkCalendarDetailFactory;
import com.mac.custom.services.workCalendar.interfaces.local.WorkCalendarServicesLocal;
import com.mac.custom.services.workCalendar.interfaces.remote.WorkCalendarServicesRemote;
import com.mac.custom.services.workLoad.interfaces.local.WorkLoadServicesLocal;
import com.netappsid.erp.server.bo.Resource;
import com.netappsid.erp.server.bo.ResourceEfficiency;
import com.netappsid.erp.server.services.interfaces.local.ResourceEfficiencyServicesLocal;
import com.netappsid.erp.server.services.resultholders.ResultHolder;
import com.netappsid.services.interfaces.local.LoaderLocal;
import com.netappsid.services.interfaces.local.PersistenceLocal;

@Stateless
public class WorkCalendarServicesBean implements WorkCalendarServicesLocal, WorkCalendarServicesRemote
{
	private static final Logger LOGGER = Logger.getLogger(WorkCalendarServicesBean.class);

	@EJB
	private LoaderLocal loader;

	@EJB
	private PersistenceLocal persistenceLocal;

	@EJB
	private ResourceEfficiencyServicesLocal resourceEfficiencyServicesLocal;

	@EJB
	private WorkLoadServicesLocal workLoadServicesLocal;

	protected LoaderLocal getLoader()
	{
		return loader;
	}

	protected PersistenceLocal getPersistenceLocal()
	{
		return persistenceLocal;
	}

	protected ResourceEfficiencyServicesLocal getResourceEfficiencyServicesLocal()
	{
		return resourceEfficiencyServicesLocal;
	}

	protected WorkLoadServicesLocal getWorkLoadServicesLocal()
	{
		return workLoadServicesLocal;
	}

	@Override
	public ResultHolder<WorkCalendarDetail> find(Date dateFrom, Date dateTo, Serializable workShiftId, Serializable resourceId, Serializable macOrderId,
			Serializable macCustomerId)
	{
		ResultHolder<WorkCalendarDetail> result = new ResultHolder<WorkCalendarDetail>();
		List<WorkLoad> workLoads = getWorkLoadServicesLocal().findByCriteria(dateFrom, dateTo, workShiftId, resourceId, macOrderId, macCustomerId);
		List<ResourceEfficiency> resourceEfficiencies = new ArrayList<ResourceEfficiency>();

		if (macOrderId != null || macCustomerId != null)
		{
			if (!workLoads.isEmpty())
			{
				resourceEfficiencies.addAll(findMatchingResourceEfficiency(workLoads));
			}
		}
		else
		{
			ResultHolder<ResourceEfficiency> resourceEfficienciesResult = getResourceEfficiencyServicesLocal().findAndGenerateResourceEfficiencies(dateFrom,
					dateTo, workShiftId, resourceId);
			resourceEfficiencies.addAll(resourceEfficienciesResult.getResults());
		}

		result.setResults(generateWorkCalendarDetails(workLoads, resourceEfficiencies));

		return result;
	}

	private List<WorkCalendarDetail> generateWorkCalendarDetails(List<WorkLoad> workLoads, List<ResourceEfficiency> resourceEfficiencies)
	{
		List<WorkCalendarDetail> workCalendarDetails = new ArrayList<WorkCalendarDetail>();
		WorkCalendarDetailFactory workCalendarDetailFactory = new WorkCalendarDetailFactory();

		for (WorkLoad workLoad : workLoads)
		{
			WorkCalendarDetail workCalendarDetail = null;

			for (ResourceEfficiency resourceEfficiency : resourceEfficiencies)
			{
				if (resourceEfficiency.getResource().hasSameID(workLoad.getResource()) && resourceEfficiency.getDate().equals(workLoad.getDate()))
				{
					workCalendarDetail = workCalendarDetailFactory.create(workLoad, resourceEfficiency);

					resourceEfficiencies.remove(resourceEfficiency);
					break;
				}
			}

			if (workCalendarDetail != null)
			{
				workCalendarDetails.add(workCalendarDetail);
			}
		}

		return workCalendarDetails;
	}

	/**
	 * Find the resource efficiency records that match the work load list (same resource, same dates)
	 * 
	 * @param workLoads
	 * @return
	 */
	private List<ResourceEfficiency> findMatchingResourceEfficiency(List<WorkLoad> workLoads)
	{
		Map<Resource, Date> minimumDatesPerResource = new HashMap<Resource, Date>();
		Map<Resource, Date> maximumDatesPerResource = new HashMap<Resource, Date>();

		for (WorkLoad workLoad : workLoads)
		{
			Date dateFrom = minimumDatesPerResource.get(workLoad.getResource());
			if (dateFrom == null || workLoad.getDate().before(dateFrom))
			{
				minimumDatesPerResource.put(workLoad.getResource(), workLoad.getDate());
			}

			Date dateTo = maximumDatesPerResource.get(workLoad.getResource());
			if (dateTo == null || workLoad.getDate().after(dateTo))
			{
				maximumDatesPerResource.put(workLoad.getResource(), workLoad.getDate());
			}
		}

		List<ResourceEfficiency> matchingResourceEfficiencies = new ArrayList<ResourceEfficiency>();

		for (Map.Entry<Resource, Date> cursor : minimumDatesPerResource.entrySet())
		{
			Resource resource = cursor.getKey();
			Date dateFrom = cursor.getValue();
			Date dateTo = maximumDatesPerResource.get(resource);

			if (dateFrom != null && dateTo != null)
			{
				ResultHolder<ResourceEfficiency> resourceEfficienciesResult = getResourceEfficiencyServicesLocal().findAndGenerateResourceEfficiencies(
						dateFrom, dateTo, null, resource.getId());
				matchingResourceEfficiencies.addAll(resourceEfficienciesResult.getResults());
			}
		}

		return matchingResourceEfficiencies;
	}

	@Override
	public void deleteResourceEfficiencies(List<Serializable> resourceEfficiencyIds)
	{
		List<ResourceEfficiency> resourceEfficiencies = loader.findByIdCollection(ResourceEfficiency.class, resourceEfficiencyIds);

		for (ResourceEfficiency resourceEfficiency : resourceEfficiencies)
		{
			getPersistenceLocal().delete(resourceEfficiency);
		}
	}
}
