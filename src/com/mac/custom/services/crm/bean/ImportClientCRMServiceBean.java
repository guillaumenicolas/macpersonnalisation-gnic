package com.mac.custom.services.crm.bean;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mac.custom.exception.ExceptionInterface;
import com.mac.custom.services.crm.local.ImportClientCRMGeneriqueServiceLocal;
import com.mac.custom.services.crm.local.ImportClientCRMServiceLocal;
import com.mac.custom.services.crm.remote.ImportClientCRMServiceRemote;
import com.netappsid.erp.server.bo.Address;
import com.netappsid.erp.server.bo.AddressType;
import com.netappsid.erp.server.bo.AddressType.Type;
import com.netappsid.erp.server.bo.City;
import com.netappsid.erp.server.bo.CommercialClass;
import com.netappsid.erp.server.bo.CommercialEntityAddress;
import com.netappsid.erp.server.bo.CommercialEntityDefaultAddress;
import com.netappsid.erp.server.bo.Country;
import com.netappsid.erp.server.bo.Currency;
import com.netappsid.erp.server.bo.Customer;
import com.netappsid.erp.server.bo.District;
import com.netappsid.erp.server.bo.Term;
import com.netappsid.services.interfaces.local.LoaderLocal;
import com.netappsid.services.interfaces.local.PersistenceLocal;

@Stateless
public class ImportClientCRMServiceBean implements ImportClientCRMServiceLocal, ImportClientCRMServiceRemote
{
	private static Logger logger = Logger.getLogger(ImportClientCRMServiceBean.class);

	private static String IMPORT_EXPORT_CONTEXT = "importExportContext";

	private static String NOM_TABLE = "PROSPECT_EXPORT";
	private static String NOM_COLONNE = "PROS_EXPO_CODE_INTE";
	private static String DEFAULTCOUNTRYCODE = "fr";

	private static int MAX_RESULTATS = 100;

	@EJB(mappedName = "erp/ImportClientCRMGeneriqueServiceBean/local")
	public ImportClientCRMGeneriqueServiceLocal importBDServiceLocal;

	@EJB(mappedName = "erp/LoaderBean/local")
	public LoaderLocal loader;

	@EJB(mappedName = "erp/PersistenceBean/local")
	public PersistenceLocal persistence;

	@Resource(mappedName = "java:/INTCLIENTDS")
	private DataSource importDS;

	private String context = null;

	// Map de ville
	private Map<String, City> cityMap = new HashMap<String, City>();

	@Override
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public void methodeImportation() throws ExceptionInterface
	{
		while (importBDServiceLocal.encoreResultats(NOM_TABLE, NOM_COLONNE))
		{
			logger.debug("D�but de l'importation client");

			importationBatch();

			logger.debug("FIN de l'importation client");
		}
	}

	@TransactionAttribute(TransactionAttributeType.NEVER)
	private void importationBatch() throws ExceptionInterface
	{
		Connection connection = getConnection();

		if (connection == null)
		{
			throw new ExceptionInterface("Aucune connection de sp�cifi�", logger);
		}

		// ----------------------

		// Ici on va chercher les valeurs par d�faut et on valide que tout est d�fini

		// /---------------------

		ResultSet rowset = null;

		try
		{
			// Statement statement = connection.createStatement();
			PreparedStatement preparedStatement = connection.prepareStatement(getRequete(NOM_TABLE, NOM_COLONNE));

			preparedStatement.setInt(1, 0);

			preparedStatement.setMaxRows(MAX_RESULTATS);

			rowset = preparedStatement.executeQuery();
		}
		catch (Exception e)
		{
			try
			{
				connection.close();
				connection = null;
			}
			catch (SQLException e1)
			{
				throw new ExceptionInterface("Impossible de terminer la connexion: " + e1.getLocalizedMessage(), logger);
			}

			throw new ExceptionInterface("Impossible d'ex�cuter la requ�te: " + e.getLocalizedMessage(), logger);
		}

		try
		{
			while (rowset.next())
			{
				Map<String, String> clesColonnes = getClesColonnes(rowset);

				try
				{
					if (clesColonnes != null)
					{
						// Modification du statut
						importBDServiceLocal.updateChampsInterface(clesColonnes, NOM_TABLE, NOM_COLONNE, 1, "");

						traitementDeLaLigne(rowset);

						// Modification du statut
						importBDServiceLocal.updateChampsInterface(clesColonnes, NOM_TABLE, NOM_COLONNE, 2, "");
					}
					else
					{
						throw new ExceptionInterface("Cles/colonnes non specifies pour la table " + NOM_TABLE, logger);
					}

				}
				catch (ExceptionInterface ei)
				{
					// Modification du statut
					importBDServiceLocal.updateChampsInterface(clesColonnes, NOM_TABLE, NOM_COLONNE, 2, ei.getMessage());
				}

			}
		}
		catch (SQLException e)
		{
			try
			{
				connection.close();
				connection = null;
			}
			catch (SQLException e1)
			{
				throw new ExceptionInterface("Impossible de terminer la connexion: " + e1.getLocalizedMessage(), logger);
			}
		}

	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void traitementDeLaLigne(ResultSet resultset) throws ExceptionInterface
	{
		// ----------------------

		// Ici on va chercher les valeurs de la requete
		// String valeur = resultset.getString(1);

		System.out.println("Debut Traitement");
		// on boucle sur le rowset
		try
		{
			String v_PROS_EXPO_CODE_PROSPECT = resultset.getString(1);
			String v_PROS_EXPO_CODE_CLIENT = resultset.getString(2);

			// Traitement du cas g�n�ral client
			logger.debug("code client:" + v_PROS_EXPO_CODE_CLIENT);
			logger.info("Code Prospect:" + v_PROS_EXPO_CODE_PROSPECT);
			if (v_PROS_EXPO_CODE_CLIENT != null)
			{
				// Ajout info client
				Customer customer = null;
				customer = ajoutClientCRM(resultset);
				customer = ajoutAdresseClientCRM(resultset, "CDE", customer);
				customer = ajoutAdresseClientCRM(resultset, "FACT", customer);
				customer = ajoutAdresseClientCRM(resultset, "LIVR", customer);

				// Sauvegarde du client
				sauvegardeClient(customer);

			}
		}
		catch (SQLException e)
		{
			throw new ExceptionInterface("Impossible de traiter la ligne: " + e.getLocalizedMessage(), logger);
		}
		logger.debug("Fin de l'importation des clients dans 360");
	}

	// Ajout des clients dans 360 - Information g�n�rales
	@TransactionAttribute(TransactionAttributeType.NEVER)
	private Customer ajoutClientCRM(ResultSet resultset)
	{

		Customer customer = null;

		// Champs � mettre � jour
		// Les contraintes "non null" sont g�r�s au niveau de la BD
		try
		{
			String v_PROS_EXPO_CODE_PROSPECT = resultset.getString(1);
			String v_PROS_EXPO_CODE_CLIENT = resultset.getString(2);
			String v_PROS_EXPO_TYPE_COMPTE = resultset.getString(3);
			String v_PROS_EXPO_CODE_VENDEUR = resultset.getString(4);
			String v_PROS_EXPO_NOM = resultset.getString(5);
			String v_PROS_EXPO_CPLT_NOM = resultset.getString(6);
			String v_PROS_EXPO_FAM_CLIENT = resultset.getString(7);
			String v_PROS_EXPO_CODE_CONT_PRINCIPAL = resultset.getString(8);
			String v_PROS_EXPO_TYPE_GRILLE = resultset.getString(9);
			String v_PROS_EXPO_CODE_GRILLE = resultset.getString(10);
			String v_PROS_EXPO_TYPE_CLIENT = resultset.getString(11);
			String v_PROS_EXPO_CLASSIFICATION = resultset.getString(12);
			String v_PROS_EXPO_ACTIVITE = resultset.getString(13);
			String v_PROS_EXPO_EMAIL_SOCIETE = resultset.getString(14);
			String v_PROS_EXPO_SITE_WEB = resultset.getString(15);
			String v_PROS_EXPO_COND_REGLEMENT = resultset.getString(26);
			String v_PROS_EXPO_MODE_REGLEMENT = resultset.getString(27);
			String v_PROS_EXPO_NBRE_SALARIE = resultset.getString(28);
			String v_PROS_EXPO_NBRE_POSEURS = resultset.getString(29);
			String v_PROS_EXPO_NBRE_COMMERCIAUX = resultset.getString(30);
			String v_PROS_EXPO_SALL_EXPO = resultset.getString(31);
			String v_PROS_EXPO_SUPERFICIE = resultset.getString(32);
			String v_PROS_EXPO_VR = resultset.getString(33);
			String v_PROS_EXPO_PGS = resultset.getString(34);
			String v_PROS_EXPO_VB = resultset.getString(35);
			String v_PROS_EXPO_PE = resultset.getString(36);
			String v_PROS_EXPO_JAL = resultset.getString(37);
			String v_PROS_EXPO_RID = resultset.getString(38);
			String v_PROS_EXPO_STO = resultset.getString(39);
			String v_PROS_EXPO_OUVR_SAMEDI = resultset.getString(40);
			String v_PROS_EXPO_TVA_INTRA = resultset.getString(41);
			String v_PROS_EXPO_CODE_NAF = resultset.getString(42);
			String v_PROS_EXPO_FORM_JURIDIQUE = resultset.getString(43);
			String v_PROS_EXPO_SIREN = resultset.getString(44);
			String v_PROS_EXPO_COMP_BANQUAIRE = resultset.getString(45);
			String v_PROS_EXPO_CODE_BANQUE = resultset.getString(46);
			String v_PROS_EXPO_CODE_GUICHET = resultset.getString(47);
			String v_PROS_EXPO_CLEF = resultset.getString(48);
			String v_PROS_EXPO_DEVISE = resultset.getString(49);
			String v_PROS_EXPO_BANQUE = resultset.getString(50);
			String v_PROS_EXPO_DOMICILIATION = resultset.getString(51);
			String v_PROS_EXPO_PLV1 = resultset.getString(72);
			String v_PROS_EXPO_PLV4 = resultset.getString(73);
			String v_PROS_EXPO_PLV5 = resultset.getString(74);
			String v_PROS_EXPO_TOTEM = resultset.getString(75);
			String v_PROS_EXPO_CA_SOCIETE = resultset.getString(76);
			String v_PROS_EXPO_CA_FERMETURE = resultset.getString(77);
			String v_PROS_EXPO_REGION = resultset.getString(78);
			String v_PROS_EXPO_SEGMENTATION = resultset.getString(79);
			String v_PROS_EXPO_SIRET = resultset.getString(80);

			// Ajout des champs ent�te client dans 360

			// Champs obligatoire dans 360 sur l'entit� client
			// - Code client
			// - locale
			// - classe de clients
			// - terme
			// - d�lais d'exp�dition = 0
			// - devise

			// Ici, on charge les clients qui ont le code : v_PROS_EXPO_CODE_PROSPECT
			List<Customer> list = loader.findByField(Customer.class, Customer.class, Customer.PROPERTYNAME_CODE, v_PROS_EXPO_CODE_CLIENT);

			for (Customer customerTmp : list)
			{
				// Valider la soci�t�
				customer = customerTmp;
			}
			if (customer != null)
			{
				// Le client existe dans 360, MAJ � faire
				customer.setName(v_PROS_EXPO_NOM);
				System.out.println("MAJ du client :" + v_PROS_EXPO_CODE_CLIENT);

			}
			else
			{
				// Cr�ation du client dans 360
				customer = new Customer();
				customer.setName(v_PROS_EXPO_NOM);
				customer.setCode(v_PROS_EXPO_CODE_CLIENT);

				// On affecte la variable langue
				Locale malocale = new Locale("fr", "FR");
				customer.setLocale(malocale);

				// Parcourir la classe de clients
				CommercialClass customerclient = null;
				List<CommercialClass> listclasseclient = loader.findByField(CommercialClass.class, CommercialClass.class, CommercialClass.PROPERTYNAME_CODE,
						"DEF");
				for (CommercialClass customerClassClient : listclasseclient)
				{
					customerclient = customerClassClient;
				}
				customer.setCustomerClass(customerclient);

				// Parcourir et ins�rer le terme de client
				Term customerterm = null;
				List<Term> listclasseterm = loader.findByField(Term.class, Term.class, Term.PROPERTYNAME_CODE, "DEF");
				for (Term customerClassterm : listclasseterm)
				{
					customerterm = customerClassterm;
				}
				customer.setTerm(customerterm);

				// D�lai exp�dition
				Short delai = 0;
				customer.setShippingDelay(delai);

				// Parcourir et ins�rer la devise
				Currency madevise = null;
				List<Currency> listcurrency = loader.findByField(Currency.class, Currency.class, Currency.PROPERTYNAME_CODE, "EUR");
				for (Currency currency : listcurrency)
				{
					madevise = currency;
				}
				customer.setCurrency(madevise);

				System.out.println("Int�gration du client :" + v_PROS_EXPO_CODE_CLIENT);
			}

		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return customer;

	}

	// A D R E S S E S
	// Ajout des adresses clients pour FACTURATION, LIVRAISON, SOCIETE
	private Customer ajoutAdresseClientCRM(ResultSet rowset, String Type, Customer customer)
	{
		Country defaultCountry = null;
		// AddressType defaultShippingAddressType = null;
		// AddressType defaultInvoicingAddressType = null;
		// AddressType defaultJobSiteAddressType = null;

		AddressType addressType = null; // permet de recuper un type d'adresse, siege, expedition,facturation....
		Type type = null;

		// On initialise l'objet adresse
		CommercialEntityDefaultAddress monAdresse = null;

		try
		{
			defaultCountry = getDefaultCountry();
			// defaultShippingAddressType = getDefautShippingAddressType();
			// defaultInvoicingAddressType = getDefautInvoicingAddressType();
			// defaultJobSiteAddressType = getDefautJobSiteAddressType();
		}
		catch (Exception e)
		{
			logger.error("Invalid required default value: " + e.getMessage());
		}

		String v_PROS_EXPO_CODE_CLIENT = null;

		// Initialisation des variables
		String nomAdresse = null;
		String adresse1 = null;
		String adresse2 = null;
		String adresse3 = null;
		String codePostal = null;
		String ville = null;
		String pays = null;
		String telephone = null;
		String fax = null;
		String email = null;

		try
		{
			v_PROS_EXPO_CODE_CLIENT = rowset.getString(2);

			if (Type == "CDE")
			{
				try
				{
					// Attribution du type d'adresse par d�faut
					addressType = getDefautJobSiteAddressType();

					nomAdresse = rowset.getString(16);
					adresse1 = rowset.getString(17);
					adresse2 = rowset.getString(18);
					adresse3 = rowset.getString(19);
					codePostal = rowset.getString(20);
					ville = rowset.getString(21);
					pays = rowset.getString(22);
					telephone = rowset.getString(23);
					fax = rowset.getString(24);
					email = rowset.getString(25);
					type = AddressType.Type.JOBSITE;

					System.out.println("Adresse CDE : " + nomAdresse + " " + adresse1);
				}
				catch (Exception e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			else if (Type == "FACT")
			{
				try
				{
					// Attribution du type d'adresse par d�faut
					addressType = getDefautInvoicingAddressType();

					nomAdresse = rowset.getString(52);
					adresse1 = rowset.getString(53);
					adresse2 = rowset.getString(54);
					adresse3 = rowset.getString(55);
					codePostal = rowset.getString(56);
					ville = rowset.getString(57);
					pays = rowset.getString(58);
					telephone = rowset.getString(59);
					fax = rowset.getString(60);
					email = rowset.getString(61);
					type = AddressType.Type.INVOICE;

					System.out.println("Adresse FACT : " + nomAdresse + " " + adresse1);
				}
				catch (Exception e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			else if (Type == "LIVR")
			{
				try
				{
					// Attribution du type d'adresse par d�faut
					addressType = getDefautShippingAddressType();

					nomAdresse = rowset.getString(62);
					adresse1 = rowset.getString(63);
					adresse2 = rowset.getString(64);
					adresse3 = rowset.getString(65);
					codePostal = rowset.getString(66);
					ville = rowset.getString(67);
					pays = rowset.getString(68);
					telephone = rowset.getString(69);
					fax = rowset.getString(70);
					email = rowset.getString(71);
					type = AddressType.Type.SHIPPING;

					System.out.println("Adresse LIVR : " + nomAdresse + " " + adresse1);
				}
				catch (Exception e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			// Concatener tous les champs adresse
			String adresseConcat = adresse1 + " " + adresse2 + " " + adresse3;

			// Si t�l�phone, Fax � blanc les initialiser � null
			if (telephone != null && telephone.equals(""))
			{
				telephone = null;
			}

			if (fax != null && fax.equals(""))
			{
				fax = null;
			}

			City city = null;
			// Recherche du client
			// Customer customer = loader.findFirstByField(Customer.class, Customer.class, Customer.PROPERTYNAME_CODE, v_PROS_EXPO_CODE_CLIENT);

			if (customer != null)
			{
				// Recherche le nom des villes avec des blancs entre les mots
				city = findCity(defaultCountry.getId(), null, ville.toUpperCase(), 0);

				if (city == null)
				{
					// Recherche le nom des villes si �chec avec des - entre les mots
					city = findCity(defaultCountry.getId(), null, ville.toUpperCase(), 1);

					if (city == null)
					{
						System.out.println("Ville null : " + ville);
					}
					else
					{
						System.out.println(city.getCode());
					}
				}
				else
				{
					System.out.println(city.getCode());
				}

			}
			else
			{
				System.out.println("Pas de client en retour !!!");
			}

			// on verifie que l'adresse Existe pas d�ja dans la liste des adresses globales
			boolean addressAlreadyExist = false;

			for (CommercialEntityAddress commercialEntityAddress : customer.getAddresses())
			{

				if ((nomAdresse == null && commercialEntityAddress.getAddress().getName() == null)
						|| nomAdresse.equals(commercialEntityAddress.getAddress().getName())
						&& ((adresseConcat == null && commercialEntityAddress.getAddress().getAddress() == null) || adresseConcat
								.equals(commercialEntityAddress.getAddress().getAddress()))
						&& ((city == null && commercialEntityAddress.getAddress().getCity().getCode() == null) || city.getCode().equals(
								commercialEntityAddress.getAddress().getCity().getCode()))
						&& ((codePostal == null && commercialEntityAddress.getAddress().getZipCode() == null) || codePostal.equals(commercialEntityAddress
								.getAddress().getZipCode())
								&& ((telephone == null && commercialEntityAddress.getAddress().getPhone() == null) || telephone.equals(commercialEntityAddress
										.getAddress().getPhone()))))
				{
					addressAlreadyExist = true;

					// l'adresse existe donc je vais le remove
					if (!customer.getDefaultAddresses().isEmpty())
					{
						CommercialEntityDefaultAddress removeDefaulttAddress = null;

						for (CommercialEntityDefaultAddress commercialEntityDefaultAddress : customer.getDefaultAddresses())
						{
							if (commercialEntityDefaultAddress.getAddressType().isThisTypeOfAddress(type))
							{
								removeDefaulttAddress = commercialEntityDefaultAddress;
								break;
							}
						}

						if (removeDefaulttAddress != null)
						{
							customer.removeFromDefaultAddresses(removeDefaulttAddress);
						}
					}

					// On alimente dans l'adresse globale le type d'adresse
					if (!commercialEntityAddress.getTypes().contains(type))
					{
						commercialEntityAddress.addToTypes(addressType);
					}

					// On d�finit l'adresse par defaut
					monAdresse = new CommercialEntityDefaultAddress();
					monAdresse.setCommercialEntityAddress(commercialEntityAddress);
					monAdresse.setAddressType(addressType);

					// Sur le client on add l'adresse par defaut
					customer.addToDefaultAddresses(monAdresse);

					break;
				}
			}

			if (!addressAlreadyExist)
			{

				// l'adresse existe donc je vais le remove
				if (!customer.getDefaultAddresses().isEmpty())
				{
					CommercialEntityDefaultAddress removeDefaulttAddress = null;

					for (CommercialEntityDefaultAddress commercialEntityDefaultAddress : customer.getDefaultAddresses())
					{
						if (commercialEntityDefaultAddress.getAddressType().isThisTypeOfAddress(type))
						{
							removeDefaulttAddress = commercialEntityDefaultAddress;
							break;
						}
					}

					if (removeDefaulttAddress != null)
					{
						customer.removeFromDefaultAddresses(removeDefaulttAddress);
					}
				}

				// on cree l'adresse
				Address newAddress = new Address();
				newAddress.setCity(city);
				newAddress.setName(nomAdresse);
				newAddress.setAddress(adresseConcat);
				newAddress.setPhone(telephone);
				newAddress.setZipCode(codePostal);
				newAddress.setActive(true);

				// on d�finit cette adresse dans la liste des adresses
				CommercialEntityAddress adresseCommercial = new CommercialEntityAddress();
				adresseCommercial.setAddress(newAddress);
				adresseCommercial.addToTypes(addressType);

				// on d�finit l'adresse par defaut
				monAdresse = new CommercialEntityDefaultAddress();
				monAdresse.setCommercialEntityAddress(adresseCommercial);
				monAdresse.setAddressType(addressType);

				// sur le client on add l'adresse par defaut
				// et on ajoute l'adresse dans la liste
				customer.addToAddresses(adresseCommercial);
				customer.addToDefaultAddresses(monAdresse);

			}
			else
			{
				logger.info("l'adresse existe deja");
			}

		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return customer;
	}

	public City findCity(Serializable countryId, String stateString, String cityString, Integer formatville)
	{
		String originalCityString = null;
		City city = null;
		if (!cityMap.containsKey(originalCityString))
		{

			try
			{
				originalCityString = cityString.trim();
				if (formatville == 0)
				{
					cityString = cityString.trim().toUpperCase().replace("ST", "S%T%").replace("STE", "S%T%").replace("STES", "S%T%").replace("STS", "S%T%");
				}
				else
				{
					cityString = cityString.trim().toUpperCase().replace(" ", "-");
				}

				city = findCityByName(countryId, stateString, originalCityString);
				if (city == null)
				{
					city = findCityLargeScale(countryId, stateString, originalCityString);
				}
				if (city == null)
				{
					city = findCityByDistrict(countryId, stateString, originalCityString);
				}
			}
			catch (Exception e)
			{}
			finally
			{
				if (city == null)
				{
					List<City> listNA = loader.findByField(City.class, City.class, City.PROPERTYNAME_CODE, cityString);
					if (listNA != null && !listNA.isEmpty())
					{
						city = listNA.get(0);
					}
				}

				if (city != null)
				{
					cityMap.put(originalCityString, city);
				}

			}
		}
		else
		{
			city = cityMap.get(cityString);
		}

		return city;
	}

	private City findCityByName(Serializable countryId, String stateString, String cityString)
	{
		City city = null;
		List<String> parameterNames = new ArrayList<String>();
		List<Object> parameterValues = new ArrayList<Object>();

		StringBuffer buffer = new StringBuffer();
		buffer.append("select distinct city.id ");
		buffer.append("from City city ");
		buffer.append("inner join city.region region ");
		buffer.append("inner join city.name cityName ");
		buffer.append("inner join region.state state ");
		buffer.append("inner join state.name stateName ");
		buffer.append("inner join state.country country ");
		buffer.append("where country.id = :countryId AND (state.code = :state OR stateName.value = :state) AND (city.code = :city OR cityName.value = :city OR cityName.value like :city OR city.code like :city OR city.upperCode like :city OR city.upperCode = :city)");

		parameterNames.add("countryId");
		parameterValues.add(countryId);

		parameterNames.add("state");
		parameterValues.add(stateString);

		parameterNames.add("city");
		parameterValues.add(cityString);

		List<?> list = loader.findByQuery(buffer.toString(), parameterNames, parameterValues, -1);

		if (!list.isEmpty())
		{
			city = loader.findById(City.class, City.class, list.get(0).toString());
		}

		return city;
	}

	private City findCityLargeScale(Serializable countryId, String stateString, String cityString)
	{
		List<String> parameterNames = new ArrayList<String>();
		List<Object> parameterValues = new ArrayList<Object>();
		City city = null;
		StringBuffer buffer = new StringBuffer();
		buffer.append("select distinct city.id ");
		buffer.append("from city ");
		buffer.append("inner join cityName ON cityName.city_id = city.id ");
		buffer.append("inner join region ON City.region_id = region.id ");
		buffer.append("inner join state ON Region.state_id = state.id ");
		buffer.append("inner join stateName ON stateName.state_id = state.id ");
		buffer.append("inner join country ON state.country_id = country.id ");

		buffer.append("where country.id = :countryId AND (state.code = :state OR stateName.value = :state) AND (((charindex((cityName.value COLLATE SQL_Latin1_General_Cp850_CI_AI) , (:city COLLATE SQL_Latin1_General_Cp850_CI_AI) ) > 0) OR (charindex ((city.code COLLATE SQL_Latin1_General_Cp850_CI_AI), (:city COLLATE SQL_Latin1_General_Cp850_CI_AI)) > 0) OR (charindex ((city.upperCode COLLATE SQL_Latin1_General_Cp850_CI_AI), (:city COLLATE SQL_Latin1_General_Cp850_CI_AI))) > 0) OR ");
		buffer.append("(charindex(replace((cityName.value COLLATE SQL_Latin1_General_Cp850_CI_AI), ' - ' + state.code ,''), (:city COLLATE SQL_Latin1_General_Cp850_CI_AI)) > 0 OR charindex(replace((city.code COLLATE SQL_Latin1_General_Cp850_CI_AI), ' - ' + state.code ,''), (:city COLLATE SQL_Latin1_General_Cp850_CI_AI)) > 0 OR charindex(replace((city.upperCode COLLATE SQL_Latin1_General_Cp850_CI_AI), ' - ' + state.code ,''), (:city COLLATE SQL_Latin1_General_Cp850_CI_AI)) > 0))");

		parameterNames.add("countryId");
		parameterValues.add(countryId.toString());

		parameterNames.add("state");
		parameterValues.add(stateString);

		parameterNames.add("city");
		parameterValues.add(cityString);

		List<?> list = loader.findByNativeQuery(buffer.toString(), parameterNames, parameterValues, -1);
		if (list.size() == 1)
		{
			Serializable cityId = ((Object[]) list.get(0))[0].toString();
			city = loader.findById(City.class, City.class, cityId);
		}

		return city;
	}

	private City findCityByDistrict(Serializable countryId, String stateString, String cityString)
	{
		City city = null;
		List<String> parameterNames = new ArrayList<String>();
		List<Object> parameterValues = new ArrayList<Object>();

		StringBuffer buffer = new StringBuffer();
		buffer.append("select distinct district.id ");
		buffer.append("from District district ");
		buffer.append("inner join district.name districtName ");
		buffer.append("inner join district.city city ");
		buffer.append("inner join city.region region ");
		buffer.append("inner join city.name cityName ");
		buffer.append("inner join region.state state ");
		buffer.append("inner join state.name stateName ");
		buffer.append("inner join state.country country ");
		buffer.append("where country.id = :countryId AND (state.code = :state OR stateName.value = :state) AND (districtName.value = :city OR districtName.value like :city)");

		parameterNames.add("countryId");
		parameterValues.add(countryId);

		parameterNames.add("state");
		parameterValues.add(stateString);

		parameterNames.add("city");
		parameterValues.add(cityString);

		List<?> list = loader.findByQuery(buffer.toString(), parameterNames, parameterValues, -1);

		if (!list.isEmpty())
		{
			District district = loader.findById(District.class, null, list.get(0).toString());
			city = district.getCity();
		}

		return city;
	}

	// Sauvegarde du client
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void sauvegardeClient(Customer customer)
	{
		persistence.save(customer, null);
	}

	// Cl�s d'acc�s � la table
	private Map<String, String> getClesColonnes(ResultSet resultset)
	{
		Map<String, String> parametres = new HashMap<String, String>();

		try
		{
			// Les contraintes "non null" sont g�r�s au niveau de la BD
			parametres.put("PROS_EXPO_CODE_PROSPECT", resultset.getString(1));
		}

		catch (Exception e)
		{
			return null;
		}

		return parametres;
	}

	private String getRequete(String NOM_TABLE, String NOM_COLONNE)
	{
		// Renseigner la requete dans une variable de type StringBuffer
		StringBuffer sql = new StringBuffer();

		sql.append("SELECT TOP 10 PROS_EXPO_CODE_PROSPECT ,PROS_EXPO_CODE_CLIENT ,PROS_EXPO_TYPE_COMPTE ,PROS_EXPO_CODE_VENDEUR ,PROS_EXPO_NOM"
				+ " ,PROS_EXPO_CPLT_NOM ,PROS_EXPO_FAM_CLIENT ,PROS_EXPO_CODE_CONT_PRINCIPAL ,PROS_EXPO_TYPE_GRILLE ,PROS_EXPO_CODE_GRILLE"
				+ " ,PROS_EXPO_TYPE_CLIENT ,PROS_EXPO_CLASSIFICATION ,PROS_EXPO_ACTIVITE ,PROS_EXPO_EMAIL_SOCIETE ,PROS_EXPO_SITE_WEB"
				+ " ,PROS_EXPO_NOM_ADDR_CDE ,PROS_EXPO_ADDR_1_CDE ,PROS_EXPO_ADDR_2_CDE ,PROS_EXPO_ADDR_3_CDE ,PROS_EXPO_CODE_POSTAL_CDE"
				+ " ,PROS_EXPO_VILLE_CDE ,PROS_EXPO_PAYS_CDE ,PROS_EXPO_TEL_CDE ,PROS_EXPO_FAX_CDE ,PROS_EXPO_EMAIL_CDE"
				+ " ,PROS_EXPO_COND_REGLEMENT ,PROS_EXPO_MODE_REGLEMENT ,PROS_EXPO_NBRE_SALARIE ,PROS_EXPO_NBRE_POSEURS"
				+ " ,PROS_EXPO_NBRE_COMMERCIAUX ,PROS_EXPO_SALL_EXPO ,PROS_EXPO_SUPERFICIE ,PROS_EXPO_VR ,PROS_EXPO_PGS ,PROS_EXPO_VB"
				+ " ,PROS_EXPO_PE ,PROS_EXPO_JAL ,PROS_EXPO_RID ,PROS_EXPO_STO ,PROS_EXPO_OUVR_SAMEDI ,PROS_EXPO_TVA_INTRA"
				+ " ,PROS_EXPO_CODE_NAF ,PROS_EXPO_FORM_JURIDIQUE ,PROS_EXPO_SIREN ,PROS_EXPO_COMP_BANQUAIRE ,PROS_EXPO_CODE_BANQUE"
				+ " ,PROS_EXPO_CODE_GUICHET ,PROS_EXPO_CLEF ,PROS_EXPO_DEVISE ,PROS_EXPO_BANQUE ,PROS_EXPO_DOMICILIATION"
				+ " ,PROS_EXPO_NOM_ADDR_FACT ,PROS_EXPO_ADDR_1_FACT ,PROS_EXPO_ADDR_2_FACT ,PROS_EXPO_ADDR_3_FACT ,PROS_EXPO_CODE_POSTAL_FACT"
				+ " ,PROS_EXPO_VILLE_FACT ,PROS_EXPO_PAYS_FACT ,PROS_EXPO_TEL_FACT ,PROS_EXPO_FAX_FACT ,PROS_EXPO_EMAIL_FACT"
				+ " ,PROS_EXPO_NOM_ADDR_LIVR ,PROS_EXPO_ADDR_1_LIVR ,PROS_EXPO_ADDR_2_LIVR ,PROS_EXPO_ADDR_3_LIVR ,PROS_EXPO_CODE_POSTAL_LIVR"
				+ " ,PROS_EXPO_VILLE_LIVR ,PROS_EXPO_PAYS_LIVR ,PROS_EXPO_TEL_LIVR ,PROS_EXPO_FAX_LIVR ,PROS_EXPO_EMAIL_LIVR"
				+ " ,PROS_EXPO_PLV1 ,PROS_EXPO_PLV4 ,PROS_EXPO_PLV5 ,PROS_EXPO_TOTEM ,PROS_EXPO_CA_SOCIETE ,PROS_EXPO_CA_FERMETURE"
				+ " ,PROS_EXPO_REGION ,PROS_EXPO_SEGMENTATION ,PROS_EXPO_SIRET ");
		sql.append("FROM " + NOM_TABLE + " ");
		sql.append("WHERE " + NOM_COLONNE + "=?");
		return sql.toString();
	}

	/**
	 * Retourne la connexion JDBC � la BD.
	 * 
	 * @return Retourne la connextion � la BD
	 */
	private Connection getConnection() throws ExceptionInterface
	{
		String context = System.getProperty(IMPORT_EXPORT_CONTEXT);

		Connection connection = null;

		try
		{
			connection = null;

			if (context == null || context.equalsIgnoreCase("remote"))
			{
				connection = importDS.getConnection();
			}
			// Temporaire en attendant un correctif de 360
			else
			{
				DriverManager.registerDriver((Driver) Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance());
				connection = DriverManager.getConnection("jdbc:jtds:sqlserver://s8-crm-t02:1433/CRM_INTERFACE", "ADMIN_CRM", "octal");
				connection.setAutoCommit(false);
			}

			return connection;
		}
		catch (Exception e)
		{
			logger.error("Impossible d'obtenir une connexion � la DB: " + e.getMessage());

			try
			{
				if (connection != null)
				{
					connection.close();
					connection = null;
				}
			}
			catch (Exception e1)
			{
				logger.error("Impossible de terminer la connexion: " + e1.getLocalizedMessage());
			}
		}

		return null;
	}

	// A D D R E S S E S
	/**
	 * permet de retourner le pays
	 * 
	 * @return
	 */
	public Country getDefaultCountry()
	{
		Country country = loader.findFirstByField(Country.class, null, Country.PROPERTYNAME_CODE, DEFAULTCOUNTRYCODE);

		if (country == null)
		{

			logger.debug("Invalid default country");
		}
		return country;
	}

	/**
	 * permet de retourner le type d'adresse defaut job site
	 * 
	 * @return
	 * @throws Exception
	 */
	public AddressType getDefautJobSiteAddressType() throws Exception
	{
		List<AddressType> jobSiteTypes = loader.findByField(AddressType.class, null, AddressType.PROPERTYNAME_INTERNALID,
				AddressType.Type.JOBSITE.getInternalId(), true);

		if (!jobSiteTypes.isEmpty())
		{
			return jobSiteTypes.get(0);
		}
		else
		{
			throw new Exception("Impossible to find the default job site address type");
		}
	}

	// R�cup�ration de l'adresse d�faut de Livraison
	public AddressType getDefautShippingAddressType() throws Exception
	{
		List<AddressType> shippingTypes = loader.findByField(AddressType.class, null, AddressType.PROPERTYNAME_INTERNALID,
				AddressType.Type.SHIPPING.getInternalId(), true);

		if (!shippingTypes.isEmpty())
		{
			return shippingTypes.get(0);
		}
		else
		{
			throw new Exception("Impossible de trouver l'adresse d�faut de livraison");
		}
	}

	// R�cup�ration de l'adresse d�faut de Facturation
	public AddressType getDefautInvoicingAddressType() throws Exception
	{
		List<AddressType> invoiceTypes = loader.findByField(AddressType.class, null, AddressType.PROPERTYNAME_INTERNALID,
				AddressType.Type.INVOICE.getInternalId(), true);

		if (!invoiceTypes.isEmpty())
		{
			return invoiceTypes.get(0);
		}
		else
		{
			throw new Exception("Impossible de trouver l'adresse d�faut de facturation");
		}
	}

}
