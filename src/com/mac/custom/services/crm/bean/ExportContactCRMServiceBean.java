package com.mac.custom.services.crm.bean;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import com.mac.custom.exception.ExceptionInterface;
import com.mac.custom.gabarits.ExportBDGabaritGeneriqueServiceLocal;
import com.mac.custom.services.crm.local.ExportContactCRMServiceLocal;
import com.mac.custom.services.crm.remote.ExportContactCRMServiceRemote;
import com.netappsid.erp.server.bo.Communication;
import com.netappsid.erp.server.bo.Contact;
import com.netappsid.erp.server.bo.Customer;
import com.netappsid.erp.server.bo.Email;
import com.netappsid.erp.server.bo.Fax;
import com.netappsid.erp.server.bo.PhoneCommunication;
import com.netappsid.services.interfaces.local.LoaderLocal;
import com.netappsid.services.interfaces.local.PersistenceLocal;

@Stateless
public class ExportContactCRMServiceBean implements ExportContactCRMServiceLocal, ExportContactCRMServiceRemote
{
	private static Logger logger = Logger.getLogger(ExportContactCRMServiceBean.class);

	private static String IMPORT_EXPORT_CONTEXT = "importExportContext";

	private String context = System.getProperty(IMPORT_EXPORT_CONTEXT);

	@EJB(mappedName = "erp/ExportBDGabaritGeneriqueServiceBean/local")
	public ExportBDGabaritGeneriqueServiceLocal exportBDServiceLocal;

	@EJB(mappedName = "erp/LoaderBean/local")
	public LoaderLocal loader;

	@EJB(mappedName = "erp/PersistenceBean/local")
	public PersistenceLocal persistence;

	@Resource(mappedName = "java:/INTCRMDS")
	private DataSource importDS;

	@Override
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public void methodeExportation() throws ExceptionInterface
	{
		logger.debug("D�but de l'exportation des contacts de 360 ");

		exportationBatch();

		logger.debug("FIN de l'exportation des contacts de 360");

	}

	@TransactionAttribute(TransactionAttributeType.NEVER)
	private void exportationBatch() throws ExceptionInterface
	{
		Connection connection = getConnection();

		if (connection == null)
		{
			throw new ExceptionInterface("Aucune connection de sp�cifi�", logger);
		}

		try
		{
			// ----------------------

			// Ici on va chercher les valeurs par d�faut et on valide que tout est d�fini

			// /---------------------

			//

			// On r�cup�re la liste des Objets � exporter

			List<Contact> listContacts = getListeObjetsAExporter();

			final StringBuffer sql = new StringBuffer();

			sql.append("INSERT INTO CONTACTS_IMPORT (");
			sql.append("CONT_IMPO_SOCI");
			sql.append(", CONT_IMPO_DTCR");
			sql.append(", CONT_IMPO_CODE_INTE");
			// sql.append(", CONT_IMPO_ERREUR");
			sql.append(", CONT_IMPO_CODE_CRM");
			// sql.append(", CONT_IMPO_CODE_ERP");
			sql.append(", CONT_IMPO_CIVILITE");
			sql.append(", CONT_IMPO_NOM");
			sql.append(", CONT_IMPO_PRENOM");
			sql.append(", CONT_IMPO_FONCTION");
			sql.append(", CONT_IMPO_TEL");
			sql.append(", CONT_IMPO_PORTABLE");
			sql.append(", CONT_IMPO_FAX");
			sql.append(", CONT_IMPO_EMAIL");
			sql.append(", CONT_IMPO_CODE_COMPTE");
			sql.append(", CONT_IMPO_ACCE_ESPA_PRO");
			sql.append(", CONT_IMPO_ACCE_COMPLET_CCS");
			sql.append(", CONT_IMPO_ACCE_GRAND_PUBLIQUE");

			sql.append(") VALUES (");

			sql.append("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

			PreparedStatement statement = connection.prepareStatement(sql.toString());

			for (Contact contact360 : listContacts)
			{

				traitementDeLaLigne(statement, contact360);
				if (context != null && context.equalsIgnoreCase("local"))
				{
					connection.commit();
				}

			}

		}

		catch (SQLException e)
		{
			try
			{
				connection.close();
				connection = null;
			}
			catch (SQLException e1)
			{
				throw new ExceptionInterface("Impossible de terminer la connexion: " + e1.getLocalizedMessage(), logger);
			}
			throw new ExceptionInterface("Erreur de la m�thode traitementDeLaLigne : " + e.getLocalizedMessage(), logger);

		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void traitementDeLaLigne(PreparedStatement statement, Contact contact) throws ExceptionInterface, SQLException
	{
		// ----------------------

		// Date du Jour
		// String dateJour = new SimpleDateFormat("dd/MM/YYYY").format(new Date());
		// Date dateJour = new Date();

		// System.out.println("Date du Jour :" + dateJour);

		// Liste des communications reli�es au contact (n� de t�l�phone, faxe, Email)
		List<Communication> listCommunications = contact.getCommunications();

		String numeroMobile = "";
		String numeroFixe = "";
		String numeroFax = "";
		String eMail = "";
		String fonction = "";

		for (Communication communication : listCommunications)
		{
			// R�cup�ration du Num�ro de t�l�phone
			if (communication instanceof PhoneCommunication)
			{
				String numeroTelephone = communication.getValue();

				String phoneCommunicationType = ((PhoneCommunication) communication).getPhoneCommunicationType().getDescription("fr");

				// test du type de Num�ro de t�l�phone (Mobile ou Fixe)
				if (phoneCommunicationType.equals("Mobile"))
				{
					numeroMobile = numeroTelephone;
				}
				else
				{
					numeroFixe = numeroTelephone;
				}

			}
			// R�cup�ration du Num�ro de Fax
			else if (communication instanceof Fax)
			{
				numeroFax = communication.getValue();
			}
			// R�cup�ration de l'Email
			else if (communication instanceof Email)
			{
				eMail = communication.getValue();
			}

		}

		// R�cup�ration de la Fonction du Contact (Occupation)

		if (contact.getOccupations() != null && !contact.getOccupations().isEmpty())
		{
			fonction = contact.getOccupations().get(0).getCode();
		}
			
		// Recherche du Client Li�

		List<Criterion> criteria = new ArrayList<Criterion>();
		List<String> subcriteria = new ArrayList<String>();
		List<Integer> joinSpec = new ArrayList<Integer>();

		// Le code ci-dessous correspond au code SQL suivant

		// Select cust from customer Customer inner join customer.contacts Contact where contact.id = monID

		criteria.add(Restrictions.eq(Contact.PROPERTYNAME_ID, contact.getId()));
		subcriteria.add(Customer.PROPERTYNAME_CONTACTS);
		joinSpec.add(CriteriaSpecification.INNER_JOIN);

		List<Customer> listCustomer = loader.findByCriteria(Customer.class, Customer.class, criteria, subcriteria, joinSpec);
		Customer customer = listCustomer.get(0);

		System.out.println("\nExport du Contact : " + contact.getLastName() + "\n NumeroFixe = " + numeroFixe + "\n  Num�roMobile = " + numeroMobile
				+ "\n   Num�roFax = " + numeroFax + "\n   eMail = " + eMail);

		// MAJ des param�tres de la requ�te
		statement.setString(1, "FA");
		statement.setString(2, "21/03/2013");
		statement.setInt(3, 0);
		statement.setString(4, "555555");
		statement.setString(5, "Monsieur"); // La civilit� n'existe pas sur le contact dans 360
		statement.setString(6, contact.getLastName());
		statement.setString(7, contact.getFirstName());
		statement.setString(8, fonction);
		statement.setString(9, numeroFixe);
		statement.setString(10, numeroMobile);
		statement.setString(11, numeroFax);
		statement.setString(12, eMail);
		statement.setString(13, customer.getCode());
		statement.setString(14, customer.getCode());
		statement.setString(15, customer.getCode());
		statement.setString(16, customer.getCode());
		statement.executeUpdate();

	}

	private List<Contact> getListeObjetsAExporter()
	{
		// calcul de la date du jour - 10J
		Calendar calendrier = Calendar.getInstance();
		calendrier.add(Calendar.DAY_OF_MONTH, -10);

		// Recherche des Contacts
		List<Criterion> criteria = new ArrayList<Criterion>();
		List<String> subcriteria = new ArrayList<String>();
		List<Integer> joinSpec = new ArrayList<Integer>();

		criteria.add(Restrictions.ilike(Contact.PROPERTYNAME_LASTNAME, "A", MatchMode.START));
		subcriteria.add(null);
		joinSpec.add(null);

		// On met customer en crit�re 2 qui correspond au Context afin de loader les contacts ET les objets communications (tous les objets de l'�cran customer)
		List<Contact> contacts = loader.findByCriteria(Contact.class, Customer.class, criteria, subcriteria, joinSpec);

		return contacts;
	}

	/**
	 * Retourne la connexion JDBC � la BD.
	 * 
	 * @return Retourne la connextion � la BD
	 */
	private Connection getConnection() throws ExceptionInterface
	{

		Connection connection = null;

		try
		{
			connection = null;

			if (context == null || context.equalsIgnoreCase("remote"))
			{
				connection = importDS.getConnection();
			}
			// Temporaire en attendant un correctif de 360
			else
			{
				DriverManager.registerDriver((Driver) Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance());
				connection = DriverManager.getConnection("jdbc:jtds:sqlserver://s8-crm-t02:1433/CRM_INTERFACE", "ADMIN_CRM", "octal");
				connection.setAutoCommit(false);
			}

			return connection;
		}
		catch (Exception e)
		{
			logger.error("Impossible d'obtenir une connexion � la DB: " + e.getMessage());

			try
			{
				if (connection != null)
				{
					connection.close();
					connection = null;
				}
			}
			catch (Exception e1)
			{
				logger.error("Impossible de terminer la connexion: " + e1.getLocalizedMessage());
			}
		}

		return null;
	}
}
