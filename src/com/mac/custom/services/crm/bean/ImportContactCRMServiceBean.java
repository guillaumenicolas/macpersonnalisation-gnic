package com.mac.custom.services.crm.bean;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.mac.custom.exception.ExceptionInterface;
import com.mac.custom.services.crm.local.ImportCRMGeneriqueServiceLocal;
import com.mac.custom.services.crm.local.ImportContactCRMServiceLocal;
import com.mac.custom.services.crm.remote.ImportContactCRMServiceRemote;
import com.netappsid.erp.server.bo.Contact;
import com.netappsid.erp.server.bo.Customer;
import com.netappsid.services.interfaces.local.LoaderLocal;
import com.netappsid.services.interfaces.local.PersistenceLocal;

@Stateless
public class ImportContactCRMServiceBean implements ImportContactCRMServiceLocal, ImportContactCRMServiceRemote
{
	private static Logger logger = Logger.getLogger(ImportContactCRMServiceBean.class);

	private static String IMPORT_EXPORT_CONTEXT = "importExportContext";

	private static String NOM_TABLE = "CONTACTS_EXPORT";
	private static String NOM_COLONNE = "CONT_EXPO_CODE_INTE";

	private static int MAX_RESULTATS = 100;

	@EJB(mappedName = "erp/ImportCRMGeneriqueServiceBean/local")
	public ImportCRMGeneriqueServiceLocal importBDServiceLocal;

	@EJB(mappedName = "erp/LoaderBean/local")
	public LoaderLocal loader;

	@EJB(mappedName = "erp/PersistenceBean/local")
	public PersistenceLocal persistence;

	@Resource(mappedName = "java:/INTCRMDS")
	private DataSource importDS;

	@Override
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public void methodeImportation() throws ExceptionInterface
	{
		while (importBDServiceLocal.encoreResultats(NOM_TABLE, NOM_COLONNE))
		{
			logger.debug("D�but de l'importation des contacts de la CRM");

			importationBatch();

			logger.debug("FIN de l'importation des contacts de la CRM");
		}
	}

	@TransactionAttribute(TransactionAttributeType.NEVER)
	private void importationBatch() throws ExceptionInterface
	{
		Connection connection = getConnection();

		if (connection == null)
		{
			throw new ExceptionInterface("Aucune connection de sp�cifi�", logger);
		}

		// ----------------------

		// Ici on va chercher les valeurs par d�faut et on valide que tout est d�fini

		// /---------------------

		ResultSet rowset = null;

		try
		{
			Statement statement = connection.createStatement();
			statement.setMaxRows(MAX_RESULTATS);

			rowset = statement.executeQuery(getRequete());
		}
		catch (Exception e)
		{
			try
			{
				connection.close();
				connection = null;
			}
			catch (SQLException e1)
			{
				throw new ExceptionInterface("Impossible de terminer la connexion: " + e1.getLocalizedMessage(), logger);
			}

			throw new ExceptionInterface("Impossible d'ex�cuter la requ�te: " + e.getLocalizedMessage(), logger);
		}

		try
		{
			while (rowset.next())
			{
				Map<String, String> clesColonnes = getClesColonnes(rowset);

				try
				{
					if (clesColonnes != null)
					{
						// Modification du statut
						importBDServiceLocal.updateChampsInterface(clesColonnes, NOM_TABLE, NOM_COLONNE, 1, "");

						traitementDeLaLigne(rowset);

						// Modification du statut
						importBDServiceLocal.updateChampsInterface(clesColonnes, NOM_TABLE, NOM_COLONNE, 2, "");
					}
					else
					{
						throw new ExceptionInterface("Cles/colonnes non specifies pour la table " + NOM_TABLE, logger);
					}

				}
				catch (ExceptionInterface ei)
				{
					// Modification du statut
					importBDServiceLocal.updateChampsInterface(clesColonnes, NOM_TABLE, NOM_COLONNE, 2, ei.getMessage());
				}

			}
		}
		catch (SQLException e)
		{
			try
			{
				connection.close();
				connection = null;
			}
			catch (SQLException e1)
			{
				throw new ExceptionInterface("Impossible de terminer la connexion: " + e1.getLocalizedMessage(), logger);
			}
		}

	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void traitementDeLaLigne(ResultSet resultset) throws ExceptionInterface, SQLException
	{
		// Ici on va chercher les valeurs de la requete

		// String cont_expo_soci = resultset.getString(1);
		// Date cont_expo_dtcr = resultset.getDate(2);
		// Integer cont_expo_code_inte = resultset.getInt(3);
		// String cont_expo_erreur = resultset.getString(4);
		// String cont_expo_code_crm = resultset.getString(5);
		// String cont_expo_code_erp = resultset.getString(6);
		// String cont_expo_civilite = resultset.getString(7);
		String cont_expo_nom = resultset.getString(8);
		String cont_expo_prenom = resultset.getString(9);
		// String cont_expo_fonction = resultset.getString(10);
		// String cont_expo_tel = resultset.getString(11);
		// String cont_expo_portable = resultset.getString(12);
		// String cont_expo_fax = resultset.getString(13);
		// String cont_expo_email = resultset.getString(14);
		// String cont_expo_code_compte_prospect = resultset.getString(15);
		String cont_expo_code_compte_client = resultset.getString(16);
		// String cont_expo_acce_espa_pro = resultset.getString(17);
		// String cont_expo_acce_complet_ccs = resultset.getString(18);
		// String cont_expo_acce_grand_publique = resultset.getString(19);

		// Cas du pr�nom non renseign� dans la CRM (le pr�nom est oblogatoire dans 360)
		if (cont_expo_prenom == null)
		{
			cont_expo_prenom = " ";
		}

		// Creation de l'objet
		// Recherche l'existence du client dans 360
		Customer customer360 = null;
		// Recherche l'existence du contact sur le code Contact CRM
		//

		List<Criterion> criteria = new ArrayList<Criterion>();
		List<String> subcriteria = new ArrayList<String>();
		List<Integer> joinSpec = new ArrayList<Integer>();

		criteria.add(Restrictions.eq(Contact.PROPERTYNAME_FIRSTNAME, cont_expo_prenom));
		subcriteria.add(null);
		joinSpec.add(null);

		criteria.add(Restrictions.eq(Contact.PROPERTYNAME_LASTNAME, cont_expo_nom));
		subcriteria.add(null);
		joinSpec.add(null);

		// Ici, je recherche le client auquel le contact est li�
		customer360 = loader.findFirstByField(Customer.class, Customer.class, Customer.PROPERTYNAME_CODE, cont_expo_code_compte_client);

		System.out.println("\nClient : " + cont_expo_code_compte_client + "- Contact : " + cont_expo_nom + " " + cont_expo_prenom);
		// si le client existe j'applique les modifs sinon je passe au contact suivant
		if (customer360 != null)
		{
			Contact contact360 = null;
			List<Contact> listContactCustomer = customer360.getContacts();
			List<Contact> contacts = loader.findByCriteria(Contact.class, Contact.class, criteria, subcriteria, joinSpec);
			Boolean existContact = false;

			for (Contact contact : listContactCustomer)
			{
				if (contacts.contains(contact))
				{
					existContact = true;
					contact360 = contact;
					System.out.println("Le contact existe d�j�");
					break;
				}
			}

			// Si le contact n'existe pas on initialise l'objet contact360
			if (!existContact)
			{
				// Cr�ation du nouveau contact
				contact360 = new Contact();
				customer360.addToContacts(contact360);
				System.out.println("Cr�ation d'un nouveau contact");

			}

			// MAJ des attributes du contact
			System.out.println("MAJ des attributs");

			if (cont_expo_prenom != null)
			{
				contact360.setFirstName(cont_expo_prenom);
			}

			contact360.setLastName(cont_expo_nom);

			// Sauvegarde de l,objet
			persistence.save(customer360, null);

		}
		else
		{
			System.out.println("Le client n'existe pas");
		}

	}

	private Map<String, String> getClesColonnes(ResultSet resultset)
	{
		Map<String, String> parametres = new HashMap<String, String>();

		try
		{
			// parametres.put("CONT_EXPO_SOCI", resultset.getString(1));
			parametres.put("CONT_EXPO_CODE_CRM", resultset.getString(1));
			parametres.put("CONT_EXPO_NOM", resultset.getString(2));
			parametres.put("CONT_EXPO_PRENOM", resultset.getString(3));
			// ...
		}
		catch (Exception e)
		{
			return null;
		}

		return parametres;
	}

	private String getRequete()
	{
		final StringBuffer sql = new StringBuffer();

		sql.append("SELECT TOP 50");
		sql.append("CONT_EXPO_SOCI");
		sql.append(", CONT_EXPO_DTCR");
		sql.append(", CONT_EXPO_CODE_INTE");
		sql.append(", CONT_EXPO_ERREUR");
		sql.append(", CONT_EXPO_CODE_CRM");
		sql.append(", CONT_EXPO_CODE_ERP");
		sql.append(", CONT_EXPO_CIVILITE");
		sql.append(", CONT_EXPO_NOM");
		sql.append(", CONT_EXPO_PRENOM");
		sql.append(", CONT_EXPO_FONCTION");
		sql.append(", CONT_EXPO_TEL");
		sql.append(", CONT_EXPO_PORTABLE");
		sql.append(", CONT_EXPO_FAX");
		sql.append(", CONT_EXPO_EMAIL");
		sql.append(", CONT_EXPO_CODE_COMPTE_PROSPECT");
		sql.append(", CONT_EXPO_CODE_COMPTE_CLIENT");
		sql.append(", CONT_EXPO_ACCE_ESPA_PRO");
		sql.append(", CONT_EXPO_ACCE_COMPLET_CCS");
		sql.append(", CONT_EXPO_ACCE_GRAND_PUBLIQUE");
		sql.append(", CONT_EXPO_NFPPS");

		sql.append(" FROM CONTACTS_EXPORT WHERE CONT_EXPO_CODE_INTE=0 ORDER BY CONT_EXPO_DTCR");

		return sql.toString();
	}

	/**
	 * Retourne la connexion JDBC � la BD.
	 * 
	 * @return Retourne la connextion � la BD
	 */
	private Connection getConnection() throws ExceptionInterface
	{
		String context = System.getProperty(IMPORT_EXPORT_CONTEXT);

		Connection connection = null;

		try
		{
			connection = null;

			if (context == null || context.equalsIgnoreCase("remote"))
			{
				connection = importDS.getConnection();
			}
			// Temporaire en attendant un correctif de 360
			else
			{
				DriverManager.registerDriver((Driver) Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance());
				connection = DriverManager.getConnection("jdbc:jtds:sqlserver://s8-crm-t02:1433/CRM_INTERFACE", "ADMIN_CRM", "octal");
				connection.setAutoCommit(false);
			}

			return connection;
		}
		catch (Exception e)
		{
			logger.error("Impossible d'obtenir une connexion � la DB: " + e.getMessage());

			try
			{
				if (connection != null)
				{
					connection.close();
					connection = null;
				}
			}
			catch (Exception e1)
			{
				logger.error("Impossible de terminer la connexion: " + e1.getLocalizedMessage());
			}
		}

		return null;
	}
}
