package com.mac.custom.services.crm.remote;

import javax.ejb.Remote;

import com.mac.custom.services.crm.interfaces.ExportContactCRMService;

@Remote
public interface ExportContactCRMServiceRemote extends ExportContactCRMService
{

}
