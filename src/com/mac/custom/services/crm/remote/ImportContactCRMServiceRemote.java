package com.mac.custom.services.crm.remote;

import javax.ejb.Remote;

import com.mac.custom.services.crm.interfaces.ImportContactCRMService;

@Remote
public interface ImportContactCRMServiceRemote extends ImportContactCRMService
{

}
