package com.mac.custom.services.crm.remote;

import javax.ejb.Remote;

import com.mac.custom.services.crm.interfaces.ImportCRMGeneriqueService;

@Remote
public interface ImportCRMGeneriqueServiceRemote extends ImportCRMGeneriqueService
{

}
