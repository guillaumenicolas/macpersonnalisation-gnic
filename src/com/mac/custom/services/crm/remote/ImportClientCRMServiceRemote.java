package com.mac.custom.services.crm.remote;

import javax.ejb.Remote;

import com.mac.custom.services.crm.interfaces.ImportClientCRMService;

@Remote
public interface ImportClientCRMServiceRemote extends ImportClientCRMService
{

}
