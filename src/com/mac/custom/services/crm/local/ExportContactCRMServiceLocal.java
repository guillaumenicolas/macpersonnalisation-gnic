package com.mac.custom.services.crm.local;

import javax.ejb.Local;

import com.mac.custom.services.crm.interfaces.ExportContactCRMService;

@Local
public interface ExportContactCRMServiceLocal extends ExportContactCRMService
{

}
