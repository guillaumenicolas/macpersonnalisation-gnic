package com.mac.custom.services.crm.local;

import javax.ejb.Local;

import com.mac.custom.services.crm.interfaces.ImportContactCRMService;

@Local
public interface ImportContactCRMServiceLocal extends ImportContactCRMService
{

}
