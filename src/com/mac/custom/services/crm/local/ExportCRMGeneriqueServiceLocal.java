package com.mac.custom.services.crm.local;

import javax.ejb.Local;

import com.mac.custom.services.crm.interfaces.ExportCRMGeneriqueService;

@Local
public interface ExportCRMGeneriqueServiceLocal extends ExportCRMGeneriqueService
{

}
