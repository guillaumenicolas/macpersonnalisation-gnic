package com.mac.custom.services.crm.local;

import javax.ejb.Local;

import com.mac.custom.services.crm.interfaces.ImportClientCRMGeneriqueService;

@Local
public interface ImportClientCRMGeneriqueServiceLocal extends ImportClientCRMGeneriqueService
{

}
