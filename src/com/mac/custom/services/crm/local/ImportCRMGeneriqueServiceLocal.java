package com.mac.custom.services.crm.local;

import javax.ejb.Local;

import com.mac.custom.services.crm.interfaces.ImportCRMGeneriqueService;

@Local
public interface ImportCRMGeneriqueServiceLocal extends ImportCRMGeneriqueService
{

}
