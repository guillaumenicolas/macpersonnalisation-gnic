package com.mac.custom.services.crm.local;

import javax.ejb.Local;

import com.mac.custom.services.crm.interfaces.ImportClientCRMService;

@Local
public interface ImportClientCRMServiceLocal extends ImportClientCRMService
{

}
