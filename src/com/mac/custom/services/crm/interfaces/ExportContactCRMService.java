package com.mac.custom.services.crm.interfaces;

import com.mac.custom.exception.ExceptionInterface;

public interface ExportContactCRMService
{
	/**
	 * Methode sans param�tre appel�e � partir des services c�dul�s
	 */
	public void methodeExportation() throws ExceptionInterface;
}
