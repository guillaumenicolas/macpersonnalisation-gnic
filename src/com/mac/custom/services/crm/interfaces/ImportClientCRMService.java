package com.mac.custom.services.crm.interfaces;

import com.mac.custom.exception.ExceptionInterface;

public interface ImportClientCRMService
{
	public void methodeImportation() throws ExceptionInterface;
}
