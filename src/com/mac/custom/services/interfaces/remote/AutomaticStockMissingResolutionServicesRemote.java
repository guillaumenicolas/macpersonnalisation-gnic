package com.mac.custom.services.interfaces.remote;

import javax.ejb.Remote;

import com.mac.custom.services.interfaces.AutomaticStockMissingResolutionServices;

@Remote
public interface AutomaticStockMissingResolutionServicesRemote extends AutomaticStockMissingResolutionServices
{

}
