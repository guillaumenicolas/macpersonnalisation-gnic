package com.mac.custom.services.interfaces.remote;

import javax.ejb.Remote;

import com.mac.custom.services.interfaces.MacQuotationServices;

@Remote
public interface MacQuotationServicesRemote extends MacQuotationServices
{

}
