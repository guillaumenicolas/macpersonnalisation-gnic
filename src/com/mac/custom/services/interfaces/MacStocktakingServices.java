package com.mac.custom.services.interfaces;

import java.io.Serializable;
import java.util.List;

import com.jgoodies.validation.ValidationMessage;
import com.netappsid.erp.server.bo.Stocktaking;

public interface MacStocktakingServices
{
	/**
	 * Check if the status should be changed to incomplete. The change will occur if the status is NEW and some details contains measures. The details are those
	 * taken in the stocktaking so they must be up-to-date.
	 * 
	 * @param stocktaking
	 *            The stocktaking to update.
	 * @return <code>true</code> if the stocktaking was modified
	 */
	boolean checkAndUpdateIncompleteStatus(Stocktaking stocktaking);

	/**
	 * Check if the status should be changed to close. The change will occur if all the details are applied. The details are read from the database for the
	 * check.
	 * 
	 * @param stocktakingId
	 *            The stocktaking to update.
	 * @return <code>true</code> if the stocktaking was modified
	 */
	boolean checkAndUpdateCloseStatus(Serializable stocktakingId);

	/**
	 * Validate if any detail in the stocktaking conflict with any other opened stocktaking in the database.
	 * 
	 * @param languageCode
	 *            The language to return the message in.
	 * @param stocktaking
	 *            The stockataking to validate for conflict. The method doesn't modify it.
	 * @return A list error message (one for each conflicting stocktaking).
	 */
	List<ValidationMessage> validateConflicts(String languageCode, Stocktaking stocktaking);
}
