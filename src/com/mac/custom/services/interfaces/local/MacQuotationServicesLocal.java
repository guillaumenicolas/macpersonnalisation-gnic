package com.mac.custom.services.interfaces.local;

import javax.ejb.Local;

import com.mac.custom.services.interfaces.MacQuotationServices;

@Local
public interface MacQuotationServicesLocal extends MacQuotationServices
{

}