package com.mac.custom.services.interfaces.local;

import javax.ejb.Local;

import com.mac.custom.services.interfaces.AutomaticStockMissingResolutionServices;

@Local
public interface AutomaticStockMissingResolutionServicesLocal extends AutomaticStockMissingResolutionServices
{

}
