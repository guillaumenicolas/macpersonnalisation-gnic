package com.mac.custom.services.interfaces;


public interface AutomaticStockMissingResolutionServices
{
	public void resolveStocksMissing();
}
