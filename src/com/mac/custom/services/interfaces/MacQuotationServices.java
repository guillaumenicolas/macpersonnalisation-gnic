package com.mac.custom.services.interfaces;

import java.util.List;

public interface MacQuotationServices
{
	public void setSentToCommercial(List<String> quotationNumbers, boolean sent);
}