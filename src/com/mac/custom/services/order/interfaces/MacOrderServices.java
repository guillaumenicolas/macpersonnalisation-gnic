package com.mac.custom.services.order.interfaces;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.joda.time.DateMidnight;

import com.jgoodies.validation.ValidationResult;
import com.mac.custom.bo.MacOrderDetail;
import com.netappsid.erp.server.bo.Resource;
import com.netappsid.erp.server.bo.Shipping;

public interface MacOrderServices
{
	public Date getExpectedProductionDate(Serializable orderId);

	public Date getExplosionLaunchDate(Serializable orderId);

	public Date getProductionEndDate(Serializable orderId);

	public Date getRealShippingDate(Serializable orderId);

	public Date getRealDeliveryDate(Serializable orderId);

	public Date getRealBillingDate(Serializable orderId);

	/**
	 * Generates the shipping dates of the specified orders. This method starts by setting the shipping dates of the order's details based on the work load for
	 * the ASAP mode, and based on the specified date if a date has been specified (in which case the work load is ignored). If all details have a shipping
	 * date, the order's date is modified to the latest detail's date, unless unless it was set to be confirmed or unless it was specified to a later date.
	 * 
	 * @param orderIds
	 * @return
	 */
	public ValidationResult generateOrderShippingDates(List<Serializable> orderIds);

	/**
	 * Returns whether everything in the provided order has been shipped.<br/>
	 * When calling this, be aware that this will not include any data that has been saved but not flushed. If you need to include a {@link Shipping} that
	 * hasn't been flushed, consider calling {@link #isOrderAllShippedAfterShipping(Serializable, Serializable)}.
	 * 
	 * @param orderId
	 * @return
	 */
	public boolean isOrderAllShipped(Serializable orderId);

	/**
	 * Returns whether everything in the provided order will have been shipped if we include the provided shipping.
	 * 
	 * @param orderId
	 * @param shippingId
	 *            The ID of a shipping to include in the calculations. This shipping does not need to have been committed or flushed yet, however it does need
	 *            to have at least been saved so that the EntityManager can find it.<br/>
	 *            If the Shipping has been flushed, however, this method will realize it and react accordingly.
	 * @throws IllegalArgumentException
	 *             If the EntityManager cannot find a Shipping that matches the shippingID.
	 * @return
	 */
	public boolean isOrderAllShippedAfterShipping(Serializable orderId, Serializable shippingId) throws IllegalArgumentException;

	/**
	 * Genere les blocages de production d'une liste de commandes
	 * 
	 **/
	public ValidationResult blockOrderProduction(List<Serializable> orderIds, Serializable blockingReasonId, Serializable userId);

	/**
	 * Genere les blocages d'expedition d'une liste de commandes
	 * 
	 **/
	public ValidationResult blockOrderShipping(List<Serializable> orderIds, Serializable blockingReasonId, Serializable userId);

	/**
	 * Genere les blocages de production d'une liste de commandes
	 * 
	 **/
	public ValidationResult unblockOrderProduction(List<Serializable> orderIds, Serializable blockingReasonId, Serializable userId);

	/**
	 * Genere les blocages d'expedition d'une liste de commandes
	 * 
	 **/
	public ValidationResult unblockOrderShipping(List<Serializable> orderIds, Serializable blockingReasonId, Serializable userId);

	/**
	 * Applique le blocage/deblocage de production du client pour une liste de commandes
	 * 
	 **/
	public ValidationResult resetToCustomerBlockProduction(List<Serializable> orderIds, Serializable userId);

	/**
	 * Applique le blocage/deblocage d'expedition du client pour une liste de commandes
	 * 
	 **/
	public ValidationResult resetToCustomerBlockShipping(List<Serializable> orderIds, Serializable userId);

	/**
	 * Clears the work load details of the specified orders, except for exploded orders.
	 * 
	 * @param orderIds
	 * @return List of orders that were cleared (even if they didn't contain work load details).
	 */

	List<Serializable> clearWorkLoadDetails(List<Serializable> orderIds);

	public Date getNextValidShippingDate(MacOrderDetail orderDetail, Date date);

	/**
	 * Calculate and returns shipping date for an order detail from the last workload date
	 * 
	 * @param lastWorkLoadDate
	 * @param orderDetail
	 * @param resource
	 * @param manufacturingDelay
	 * @param dateForward
	 */
	public Date getShippingDateFromLastWorkLoadDate(DateMidnight lastWorkLoadDate, MacOrderDetail orderDetail, Resource resource, Integer manufacturingDelay,
			boolean dateForward);
}
