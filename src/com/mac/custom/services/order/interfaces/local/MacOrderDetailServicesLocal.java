package com.mac.custom.services.order.interfaces.local;

import javax.ejb.Local;

import com.mac.custom.services.order.interfaces.MacOrderDetailServices;

@Local
public interface MacOrderDetailServicesLocal extends MacOrderDetailServices
{
}
