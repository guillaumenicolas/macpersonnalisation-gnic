package com.mac.custom.services.order.interfaces.local;

import javax.ejb.Local;

import com.mac.custom.services.order.interfaces.MacShippingServices;

@Local
public interface MacShippingServicesLocal extends MacShippingServices
{

}
