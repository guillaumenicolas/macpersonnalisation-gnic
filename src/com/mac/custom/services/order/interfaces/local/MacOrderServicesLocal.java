package com.mac.custom.services.order.interfaces.local;

import javax.ejb.Local;

import com.mac.custom.services.order.interfaces.MacOrderServices;

@Local
public interface MacOrderServicesLocal extends MacOrderServices
{
}
