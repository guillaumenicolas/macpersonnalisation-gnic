package com.mac.custom.services.order.interfaces.remote;

import javax.ejb.Remote;

import com.mac.custom.services.order.interfaces.MacOrderServices;

@Remote
public interface MacOrderServicesRemote extends MacOrderServices
{

}
