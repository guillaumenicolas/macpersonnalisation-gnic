package com.mac.custom.services.order.interfaces.remote;

import java.io.Serializable;
import java.util.Date;

import javax.ejb.Remote;

import com.mac.custom.services.order.interfaces.MacOrderDetailServices;

@Remote
public interface MacOrderDetailServicesRemote extends MacOrderDetailServices
{
	public Date getExpectedProductionDate(Serializable orderDetailId);

	public Date getExplosionLaunchDate(Serializable orderDetailId);

	public Date getProductionEndDate(Serializable orderDetailId);

	public Date getRealShippingDate(Serializable orderDetailId);

	public Date getRealDeliveryDate(Serializable orderDetailId);

	public Date getRealBillingDate(Serializable orderDetailId);
}
