package com.mac.custom.services.order.interfaces.remote;

import javax.ejb.Remote;

import com.mac.custom.services.order.interfaces.MacShippingServices;

@Remote
public interface MacShippingServicesRemote extends MacShippingServices
{

}
