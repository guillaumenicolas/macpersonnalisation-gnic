package com.mac.custom.services.order.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.mac.custom.services.order.interfaces.local.MacOrderDetailServicesLocal;
import com.mac.custom.services.order.interfaces.remote.MacOrderDetailServicesRemote;
import com.netappsid.services.interfaces.local.LoaderLocal;

@Stateless
public class MacOrderDetailServicesBean implements MacOrderDetailServicesLocal, MacOrderDetailServicesRemote
{
	@EJB
	protected static LoaderLocal loaderLocal;

	@Override
	public Date getExpectedProductionDate(Serializable orderDetailId)
	{
		Date date = null;
		StringBuffer sqlQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("orderDetailId", orderDetailId.toString());

		sqlQuery.append("SELECT MAX(endDate) FROM WorkOrder WHERE explodableItem_id = :orderDetailId ");
		List results = loaderLocal.findByNativeQuery(sqlQuery.toString(), parameters, 1);

		if (!results.isEmpty())
		{
			date = ((Date) ((Object[]) results.get(0))[0]);
		}

		return date;
	}

	@Override
	public Date getExplosionLaunchDate(Serializable orderDetailId)
	{
		Date date = null;
		StringBuffer sqlQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("orderDetailId", orderDetailId.toString());

		sqlQuery.append("SELECT MAX(dateExploded) FROM ExplodableItem WHERE ExplodableItem.id = :orderDetailId ");

		List results = loaderLocal.findByNativeQuery(sqlQuery.toString(), parameters, 1);

		if (!results.isEmpty())
		{
			date = ((Date) ((Object[]) results.get(0))[0]);
		}

		return date;
	}

	@Override
	public Date getProductionEndDate(Serializable orderDetailId)
	{
		Date date = null;
		StringBuffer sqlQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("orderDetailId", orderDetailId.toString());

		sqlQuery.append("SELECT MAX(ProductionBasket.date) ");
		sqlQuery.append("FROM ProductionBasket  ");
		sqlQuery.append("INNER JOIN WorkOrderRoutingStep ON ProductionBasket.workOrderRoutingStep_id = WorkOrderRoutingStep.id ");
		sqlQuery.append("INNER JOIN WorkOrder ON WorkOrderRoutingStep.workOrder_id = WorkOrder.id ");
		sqlQuery.append("WHERE WorkOrder.explodableItem_id = :orderDetailId ");

		List results = loaderLocal.findByNativeQuery(sqlQuery.toString(), parameters, 1);

		if (!results.isEmpty())
		{
			date = ((Date) ((Object[]) results.get(0))[0]);
		}

		return date;
	}

	@Override
	public Date getRealShippingDate(Serializable orderDetailId)
	{
		Date date = null;
		StringBuffer sqlQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("orderDetailId", orderDetailId.toString());

		sqlQuery.append("SELECT MAX(date) ");
		sqlQuery.append("FROM Shipping ");
		sqlQuery.append("INNER JOIN ShippingDetail ON Shipping.id = ShippingDetail.shipping_id ");
		sqlQuery.append("INNER JOIN Document ON Shipping.id = Document.id ");
		sqlQuery.append("WHERE ShippingDetail.orderDetail_id = :orderDetailId ");

		List results = loaderLocal.findByNativeQuery(sqlQuery.toString(), parameters, 1);

		if (!results.isEmpty())
		{
			date = ((Date) ((Object[]) results.get(0))[0]);
		}

		return date;
	}

	@Override
	public Date getRealDeliveryDate(Serializable orderDetailId)
	{
		Date date = null;
		StringBuffer sqlQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("orderDetailId", orderDetailId.toString());

		sqlQuery.append("SELECT MAX(deliveryDate) ");
		sqlQuery.append("FROM Shipping ");
		sqlQuery.append("INNER JOIN ShippingDetail ON Shipping.id = ShippingDetail.shipping_id ");
		sqlQuery.append("WHERE ShippingDetail.orderDetail_id = :orderDetailId ");

		List results = loaderLocal.findByNativeQuery(sqlQuery.toString(), parameters, 1);

		if (!results.isEmpty())
		{
			date = ((Date) ((Object[]) results.get(0))[0]);
		}

		return date;
	}

	@Override
	public Date getRealBillingDate(Serializable orderDetailId)
	{
		Date date = null;
		StringBuffer sqlQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("orderDetailId", orderDetailId.toString());

		sqlQuery.append("SELECT MAX(date) ");
		sqlQuery.append("FROM SaleInvoice ");
		sqlQuery.append("INNER JOIN Shipping ON SaleInvoice.shipping_id = Shipping.id ");
		sqlQuery.append("INNER JOIN ShippingDetail ON Shipping.id = ShippingDetail.shipping_id ");
		sqlQuery.append("INNER JOIN Document ON Shipping.id = Document.id ");
		sqlQuery.append("WHERE ShippingDetail.orderDetail_id = :orderDetailId ");

		List results = loaderLocal.findByNativeQuery(sqlQuery.toString(), parameters, 1);

		if (!results.isEmpty())
		{
			date = ((Date) ((Object[]) results.get(0))[0]);
		}

		return date;
	}

}
