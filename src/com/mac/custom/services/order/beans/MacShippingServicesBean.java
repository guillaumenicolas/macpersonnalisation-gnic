package com.mac.custom.services.order.beans;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.mac.custom.services.order.interfaces.local.MacShippingServicesLocal;
import com.mac.custom.services.order.interfaces.remote.MacShippingServicesRemote;
import com.netappsid.erp.server.services.interfaces.local.ShippingServicesLocal;
import com.netappsid.services.interfaces.local.LoaderLocal;
import com.netappsid.services.interfaces.local.PersistenceLocal;

@Stateless
public class MacShippingServicesBean implements MacShippingServicesLocal, MacShippingServicesRemote
{
	@EJB
	private LoaderLocal loader;

	@EJB
	private PersistenceLocal persistence;

	@EJB
	private ShippingServicesLocal shippingServices;

	protected PersistenceLocal getPersistence()
	{
		return persistence;
	}

	public LoaderLocal getLoader()
	{
		return loader;
	}

}
