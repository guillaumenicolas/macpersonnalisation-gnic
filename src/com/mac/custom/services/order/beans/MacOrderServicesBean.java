package com.mac.custom.services.order.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateMidnight;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.eaio.uuid.UUID;
import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.message.SimpleValidationMessage;
import com.mac.custom.bo.MacOrder;
import com.mac.custom.bo.MacOrderDetail;
import com.mac.custom.bo.WorkLoad;
import com.mac.custom.bo.WorkLoadDetail;
import com.mac.custom.factory.WorkLoadDetailFactory;
import com.mac.custom.factory.WorkLoadFactory;
import com.mac.custom.services.order.interfaces.local.MacOrderServicesLocal;
import com.mac.custom.services.order.interfaces.remote.MacOrderServicesRemote;
import com.netappsid.bo.EntityStatus;
import com.netappsid.bo.model.Model;
import com.netappsid.dao.utils.boquery.BOQuery;
import com.netappsid.erp.server.bo.BlockingReason;
import com.netappsid.erp.server.bo.DocumentStatus;
import com.netappsid.erp.server.bo.OrderDetail;
import com.netappsid.erp.server.bo.Resource;
import com.netappsid.erp.server.bo.SaleTransaction;
import com.netappsid.erp.server.bo.Shipping;
import com.netappsid.erp.server.bo.ShippingDetail;
import com.netappsid.erp.server.bo.User;
import com.netappsid.erp.server.bo.WorkingStation;
import com.netappsid.erp.server.datatypes.GroupMeasureValue;
import com.netappsid.erp.server.services.interfaces.local.ResourceEfficiencyServicesLocal;
import com.netappsid.erp.server.services.interfaces.local.ShippingServicesLocal;
import com.netappsid.erp.server.services.resultholders.validationmessage.LocalizableSimpleValidationMessage;
import com.netappsid.erp.server.utils.WorkShiftUtils;
import com.netappsid.erp.utils.iterator.DateBetweenIterator;
import com.netappsid.europe.naid.custom.bo.PostponedReason;
import com.netappsid.europe.naid.custom.bo.ShippingDateHistory;
import com.netappsid.europe.naid.custom.utils.OrderEuroUtils;
import com.netappsid.services.interfaces.local.LoaderLocal;
import com.netappsid.services.interfaces.local.PersistenceLocal;

@Stateless
public class MacOrderServicesBean implements MacOrderServicesLocal, MacOrderServicesRemote
{
	private static final Logger LOGGER = Logger.getLogger(MacOrderServicesBean.class);

	// TODO localize the date format
	private static final DateTimeFormatter ERROR_DATE_FORMAT = DateTimeFormat.forPattern("d MMM");
	private static final int MAX_SHIPPING_DATE_ATTEMPTS = 365;
	private static final int MAX_MANUFACTURING_DELAY_ATTEMPTS = MAX_SHIPPING_DATE_ATTEMPTS;
	private static final int MAX_SHIPPING_ROUTE_ATTEMPTS = 31;
	private static final int DEFAULT_RESOURCE_EFFICIENCY_LOOKUP_DAYS = 5;

	private static final int ORDER_ALL_SHIPPED_QUERY_ITEMTOSHIPID_INDEX = 0;
	private static final int ORDER_ALL_SHIPPED_QUERY_QUANTITYCONVERTEDTOSHIP_INDEX = 1;
	private static final int ORDER_ALL_SHIPPED_QUERY_QUANTITYCONVERTEDSHIPPED_INDEX = 2;

	@EJB
	private LoaderLocal loaderLocal;

	@EJB
	private ResourceEfficiencyServicesLocal resourceEfficiencyLocal;

	@EJB
	private ShippingServicesLocal shippingLocal;

	@PersistenceContext
	private EntityManager entityManager;

	@EJB
	private PersistenceLocal persistence;

	private final WorkLoadFactory workLoadFactory = new WorkLoadFactory();
	private final WorkLoadDetailFactory workLoadFactoryDetail = new WorkLoadDetailFactory();
	private final WorkShiftUtils workShiftUtils = new WorkShiftUtils();
	private final OrderEuroUtils orderEuroUtils = new OrderEuroUtils();

	@Override
	public Date getExpectedProductionDate(Serializable orderId)
	{
		Date date = null;
		StringBuffer sqlQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("orderId", orderId.toString());

		sqlQuery.append("SELECT MAX(endDate) FROM WorkOrder WHERE explodableDocument_id = :orderId ");
		List results = loaderLocal.findByNativeQuery(sqlQuery.toString(), parameters, 1);

		if (!results.isEmpty())
		{
			date = ((Date) ((Object[]) results.get(0))[0]);
		}

		return date;
	}

	@Override
	public Date getExplosionLaunchDate(Serializable orderId)
	{
		Date date = null;
		StringBuffer sqlQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("orderId", orderId.toString());

		sqlQuery.append("SELECT MAX(dateExploded) ");
		sqlQuery.append("FROM OrderDetail ");
		sqlQuery.append("INNER JOIN ExplodableItem  ON OrderDetail.id = ExplodableItem.id ");
		sqlQuery.append("WHERE OrderDetail.saleTransaction_id = :orderId ");

		List results = loaderLocal.findByNativeQuery(sqlQuery.toString(), parameters, 1);

		if (!results.isEmpty())
		{
			date = ((Date) ((Object[]) results.get(0))[0]);
		}

		return date;
	}

	@Override
	public Date getProductionEndDate(Serializable orderId)
	{
		Date date = null;
		StringBuffer sqlQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("orderId", orderId.toString());

		sqlQuery.append("SELECT MAX(ProductionBasket.date) ");
		sqlQuery.append("FROM ProductionBasket  ");
		sqlQuery.append("INNER JOIN WorkOrderRoutingStep ON ProductionBasket.workOrderRoutingStep_id = WorkOrderRoutingStep.id ");
		sqlQuery.append("INNER JOIN WorkOrder ON WorkOrderRoutingStep.workOrder_id = WorkOrder.id ");
		sqlQuery.append("WHERE WorkOrder.explodableDocument_id = :orderId ");

		List results = loaderLocal.findByNativeQuery(sqlQuery.toString(), parameters, 1);

		if (!results.isEmpty())
		{
			date = ((Date) ((Object[]) results.get(0))[0]);
		}

		return date;
	}

	@Override
	public Date getRealShippingDate(Serializable orderId)
	{
		Date date = null;
		StringBuffer sqlQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("orderId", orderId.toString());

		sqlQuery.append("SELECT MAX(date) ");
		sqlQuery.append("FROM Shipping ");
		sqlQuery.append("INNER JOIN Document ON Shipping.id = Document.id ");
		sqlQuery.append("WHERE Shipping.order_id = :orderId ");

		List results = loaderLocal.findByNativeQuery(sqlQuery.toString(), parameters, 1);

		if (!results.isEmpty())
		{
			date = ((Date) ((Object[]) results.get(0))[0]);
		}

		return date;
	}

	@Override
	public Date getRealDeliveryDate(Serializable orderId)
	{
		Date date = null;
		StringBuffer sqlQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("orderId", orderId.toString());

		sqlQuery.append("SELECT MAX(deliveryDate) ");
		sqlQuery.append("FROM Shipping ");
		sqlQuery.append("WHERE Shipping.order_id = :orderId ");

		List results = loaderLocal.findByNativeQuery(sqlQuery.toString(), parameters, 1);

		if (!results.isEmpty())
		{
			date = ((Date) ((Object[]) results.get(0))[0]);
		}

		return date;
	}

	@Override
	public Date getRealBillingDate(Serializable orderId)
	{
		Date date = null;
		StringBuffer sqlQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("orderId", orderId.toString());

		sqlQuery.append("SELECT MAX(date) ");
		sqlQuery.append("FROM SaleInvoice ");
		sqlQuery.append("INNER JOIN Shipping ON SaleInvoice.shipping_id = Shipping.id ");
		sqlQuery.append("INNER JOIN Document ON Shipping.id = Document.id ");
		sqlQuery.append("WHERE Shipping.order_id = :orderId ");

		List results = loaderLocal.findByNativeQuery(sqlQuery.toString(), parameters, 1);

		if (!results.isEmpty())
		{
			date = ((Date) ((Object[]) results.get(0))[0]);
		}

		return date;
	}

	@Override
	public boolean isOrderAllShipped(Serializable orderId)
	{
		boolean shipped = false;
		List<Object[]> results = selectNotFullyShippedItems(orderId);

		if (results.isEmpty())
		{
			shipped = true;
		}

		return shipped;
	}

	/**
	 * For the provided order, selects one row per OrderDetailToShip which isn't fully shipped (and isn't cancelled). The columns in the Object[] representing
	 * each returned row must match the ORDER_ALL_SHIPPED_QUERY_* constants.<br>
	 * This uses a query, so it will not take into account Shippings that aren't flushed.
	 * 
	 * @param orderId
	 * @return
	 */
	private List<Object[]> selectNotFullyShippedItems(Serializable orderId)
	{
		StringBuffer sqlQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("orderId", orderId.toString());
		parameters.put("cancel", false);

		sqlQuery.append("SELECT ItemToShip.id, ItemToShip.quantityConvertedValue, SUM(COALESCE(ShippingDetail.quantityConvertedValue, 0)) ");
		sqlQuery.append("FROM OrderDetailToShip ");
		sqlQuery.append("INNER JOIN ItemToShip ON OrderDetailToShip.id = ItemToShip.id ");
		sqlQuery.append("INNER JOIN OrderDetail ON OrderDetailToShip.orderDetail_id = OrderDetail.id ");
		sqlQuery.append("LEFT JOIN ShippingDetail ON OrderDetailToShip.id = ShippingDetail.itemToShip_id ");
		sqlQuery.append("WHERE OrderDetail.saleTransaction_id = :orderId ");
		// There doesn't seem to be a NOT NULL constraint for this boolean. We will interpret null as false, so check for either false or null
		sqlQuery.append("AND (ItemToShip.cancel = :cancel OR ItemToShip.cancel IS NULL ) ");
		sqlQuery.append("GROUP BY ItemToShip.id, ItemToShip.quantityConvertedValue ");
		sqlQuery.append("HAVING ItemToShip.quantityConvertedValue >  SUM(COALESCE(ShippingDetail.quantityConvertedValue, 0))  ");

		List<Object[]> results = loaderLocal.findByNativeQuery(sqlQuery.toString(), parameters, 1);
		return results;
	}

	@Override
	public boolean isOrderAllShippedAfterShipping(Serializable orderId, Serializable shippingId)
	{

		// If the Shipping is in the data source, then no need to do any extra work to include its details in our calculations
		// We cannot use loaderLocal.findById() here because it will find the element even if it hasn't been flushed, so we use a query instead

		StringBuffer sqlQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("shippingId", shippingId.toString());

		sqlQuery.append("SELECT Shipping.id ");
		sqlQuery.append("FROM Shipping ");
		sqlQuery.append("WHERE Shipping.id = :shippingId ");

		List foundShippings = loaderLocal.findByNativeQuery(sqlQuery.toString(), parameters, 1);
		if (!foundShippings.isEmpty())
		{
			return isOrderAllShipped(orderId);
		}

		// Get the Shipping from the EntityManager so we can work with it. If it's not there, throw an Exception as stated in the javadoc.
		Shipping shipping = entityManager.find(Shipping.class, shippingId);

		if (shipping == null)
		{
			throw new IllegalArgumentException("No Shipping with ID " + shippingId.toString() + " could be found by entity manager! Has it been saved?");
		}

		// Obtain all ItemToShip from the order which aren't all shipped according to the data source
		List<Object[]> notFullyShippedItems = selectNotFullyShippedItems(orderId);

		// Not sure this should happen, but if the order is all shipped without this Shipping, then no need to check further
		if (notFullyShippedItems.isEmpty())
		{
			return true;
		}

		// Add the quantities from our uncommitted Shipping.
		// At the end, we'll check and see whether it solves all remaining details or not.
		// Iterate over all details of our Shipping to do this.
		for (ShippingDetail currentShippingDetail : shipping.getDetails())
		{
			if (currentShippingDetail.getItemToShip() != null)
			{
				// Look for a row in our notFullyShippedItems
				for (Object[] currentItem : notFullyShippedItems)
				{
					// The ID gets returned as a String. Convert to UUID so we can properly compare
					UUID uuid = new UUID((String) currentItem[ORDER_ALL_SHIPPED_QUERY_ITEMTOSHIPID_INDEX]);
					if (currentShippingDetail.getItemToShip().getId().equals(uuid))
					{
						BigDecimal previousQuantityShipped = (BigDecimal) currentItem[ORDER_ALL_SHIPPED_QUERY_QUANTITYCONVERTEDSHIPPED_INDEX];
						BigDecimal newQuantityShipped = previousQuantityShipped.add(currentShippingDetail.getQuantityConvertedValue());
						currentItem[ORDER_ALL_SHIPPED_QUERY_QUANTITYCONVERTEDSHIPPED_INDEX] = newQuantityShipped;
						break;
					}
				}

			}
		}

		// We've added all quantities from our shipping. Iterate a final time on our results.
		// If we find any row that's still not fully shipped, order is not fully shipped.
		for (Object[] currentItem : notFullyShippedItems)
		{
			BigDecimal quantityShipped = (BigDecimal) currentItem[ORDER_ALL_SHIPPED_QUERY_QUANTITYCONVERTEDSHIPPED_INDEX];
			BigDecimal quantityToShip = (BigDecimal) currentItem[ORDER_ALL_SHIPPED_QUERY_QUANTITYCONVERTEDTOSHIP_INDEX];

			if (quantityToShip.compareTo(quantityShipped) > 0)
			{
				return false;
			}

		}

		// If we found no item that is still not fully shipped, then our Shipping completes our order
		return true;
	}

	@Override
	public ValidationResult generateOrderShippingDates(List<Serializable> orderIds)
	{
		ValidationResult result = new ValidationResult();

		List<MacOrder> orders = loaderLocal.findByIdCollection(MacOrder.class, null, orderIds);

		// we use a cache since we will need to find newly persisted WorkLoad objects in the same transaction
		Cache<ResourceAndDateCacheKey, WorkLoad> workLoadCache = CacheBuilder.newBuilder().build();

		// we use a cache to avoid duplicate queries for the same resource efficiencies (Optional is used to allow null entries)
		Cache<ResourceAndDateCacheKey, Optional<GroupMeasureValue>> resourceEfficiencyCache = CacheBuilder.newBuilder().build();

		for (MacOrder order : orders)
		{
			if (order.isSaveStatusComplete())
			{
				// we must set the entity status to READY in order for the Order to be ready for modifications
				order.setEntityStatus(EntityStatus.READY);

				result.addAllFrom(generateOrderShippingDate(order, workLoadCache, resourceEfficiencyCache));
			}
			else
			{
				// order has an incomplete status, we clear all work load details associated with it
				for (OrderDetail orderDetail : order.getDetails())
				{
					MacOrderDetail macOrderDetail = Model.getTarget(orderDetail);
					macOrderDetail.resetWorkLoadDetails();
				}
			}
		}

		workLoadCache.invalidateAll();
		workLoadCache.cleanUp();

		resourceEfficiencyCache.invalidateAll();
		resourceEfficiencyCache.cleanUp();

		return result;
	}

	private ValidationResult generateOrderShippingDate(MacOrder order, Cache<ResourceAndDateCacheKey, WorkLoad> workLoadCache,
			Cache<ResourceAndDateCacheKey, Optional<GroupMeasureValue>> resourceEfficiencyCache)
	{
		ValidationResult result = new ValidationResult();

		boolean missingOrderDetailShippingDate = false;
		Date lastOrderDetailShippingDate = null;

		for (OrderDetail orderDetail : order.getDetails())
		{
			MacOrderDetail macOrderDetail = Model.getTarget(orderDetail);
			if (macOrderDetail.isCancel()
					|| (order.getDocumentStatus().getInternalId() != null && order.getDocumentStatus().getInternalId().equals(DocumentStatus.CANCEL)))
			{
				macOrderDetail.resetWorkLoadDetails();
			}
			else if (com.netappsid.erp.Severity.ERROR.equals(macOrderDetail.getConfiguratorValidationSeverity()))
			{
				missingOrderDetailShippingDate = true;
				macOrderDetail.resetWorkLoadDetails();
			}
			else if (order.isBlockProductionEffective())
			{
				macOrderDetail.resetWorkLoadDetails();
			}
			else
			{
				List<WorkLoadDetail> workLoadDetails = macOrderDetail.getWorkLoadDetails();
				if (workLoadDetails.isEmpty())
				{
					// no work load details were generated for this order detail (or they were deleted), generating them
					generateOrderDetailShippingDate(macOrderDetail, result, workLoadCache, resourceEfficiencyCache);
				}

				if (macOrderDetail.getShippingDate() == null)
				{
					missingOrderDetailShippingDate = true;
				}
				else if (lastOrderDetailShippingDate == null || macOrderDetail.getShippingDate().compareTo(lastOrderDetailShippingDate) > 0)
				{
					lastOrderDetailShippingDate = macOrderDetail.getShippingDate();
				}
			}
		}

		if (missingOrderDetailShippingDate || order.isShipToConfirm())
		{
			lastOrderDetailShippingDate = null;
		}

		if (!Objects.equal(lastOrderDetailShippingDate, order.getShippingDate()))
		{
			if (lastOrderDetailShippingDate != null && order.getDesiredDate() != null
					&& !DateUtils.isSameDay(lastOrderDetailShippingDate, order.getDesiredDate()))
			{
				result.add(new LocalizableSimpleValidationMessage("desiredDateWasNotRespected", Severity.WARNING, new Serializable[0]));
			}

			if (order.isShipASAP() || order.isShipToConfirm())
			{
				// each individual order detail has been validated for their shipping date, so we simply take the last detail's shipping date
				order.setShippingDate(lastOrderDetailShippingDate);

				// set and update the shipping date history
				ShippingDateHistory shippingDateHistory = orderEuroUtils.updateShippingDateHistory(order);
				if (shippingDateHistory != null)
				{
					PostponedReason postponedReason = loaderLocal.findFirstByField(PostponedReason.class, null, PostponedReason.PROPERTYNAME_CODE,
							MacOrder.AUTOMATIC_DATE_POSTPONED_REASON_CODE);
					if (postponedReason == null)
					{
						LOGGER.warn("Failed to locate a postponedReason for code: " + MacOrder.AUTOMATIC_DATE_POSTPONED_REASON_CODE);
					}
					else
					{
						shippingDateHistory.setPostponedReason(postponedReason);
					}
				}
			}
		}

		return result;
	}

	/**
	 * Creates {@link WorkLoad} and {@link WorkLoadDetail} objects for an order detail. The order detail is expected to have properties describing the amount of
	 * required work units and the working station associated with it, otherwise an appropriate date can't be determined.
	 * 
	 * @param orderDetail
	 * @param result
	 * @param workLoadCache
	 * @param resourceEfficiencyCache
	 */
	private void generateOrderDetailShippingDate(MacOrderDetail orderDetail, ValidationResult result, Cache<ResourceAndDateCacheKey, WorkLoad> workLoadCache,
			Cache<ResourceAndDateCacheKey, Optional<GroupMeasureValue>> resourceEfficiencyCache)
	{
		GroupMeasureValue workUnits = orderDetail.getWorkUnits();
		String workingStationCode = orderDetail.getWorkingStationCode();
		Integer supplyingDelay = orderDetail.getSupplyingDelay();
		Integer manufacturingDelay = orderDetail.getManufacturingDelay();

		if (workUnits == null || workingStationCode == null)
		{
			result.add(new LocalizableSimpleValidationMessage("orderDetailMissingOrInvalidWorkLoadProperties", Severity.ERROR, new Serializable[0]));
		}
		else
		{
			WorkingStation workingStation = findWorkingStation(workingStationCode);
			if (workingStation == null)
			{
				// failed to locate the working station, can't create a work load for this date
				result.add(new LocalizableSimpleValidationMessage("failedToFindWorkingStation", Severity.WARNING, new Serializable[] { workingStationCode }));
			}
			else
			{
				if (orderDetail.isShipASAP())
				{
					generateWorkLoadDetailAsapDate(orderDetail, workingStation, supplyingDelay, manufacturingDelay, workUnits, result, workLoadCache,
							resourceEfficiencyCache);
				}
				else if (orderDetail.getShippingDate() != null)
				{
					generateWorkLoadDetailSpecificDate(orderDetail, workingStation, supplyingDelay, manufacturingDelay, workUnits, workLoadCache,
							resourceEfficiencyCache, result);
				}
			}
		}
	}

	private void generateWorkLoadDetailAsapDate(MacOrderDetail orderDetail, WorkingStation workingStation, Integer supplyingDelay, Integer manufacturingDelay,
			GroupMeasureValue workUnits, ValidationResult result, Cache<ResourceAndDateCacheKey, WorkLoad> workLoadCache,
			Cache<ResourceAndDateCacheKey, Optional<GroupMeasureValue>> resourceEfficiencyCache)
	{
		MacOrder order = orderDetail.getMacOrder();

		// if the order is blocked we don't add the work loads, unless it's exploded
		if (order.isBlockProductionEffective() && !order.isExploded())
		{
			result.add(new LocalizableSimpleValidationMessage("orderDetailShippingDetailNotCalculatedBecauseOrderProductionIsBlocked", Severity.WARNING,
					new Serializable[] { orderDetail.getIdentifier() }));
		}
		else
		{
			AsapDateIterator dateIterator = new AsapDateIterator(this, order.getDesiredDate(), workingStation, supplyingDelay, manufacturingDelay,
					resourceEfficiencyCache, orderDetail);

			DateMidnight lastWorkLoadDate = null;

			if (!workUnits.isGreaterThanZero())
			{
				// there are no UOs associated with this orderDetail, we simply use the first date found by the iterator
				lastWorkLoadDate = dateIterator.next();
			}
			else
			{
				while (workUnits.isGreaterThanZero() && dateIterator.hasNext())
				{
					DateMidnight nextDate = dateIterator.next();

					GroupMeasureValue resourceEfficiency = getResourceEfficiencyTimeMeasure(resourceEfficiencyCache, nextDate.toDate(), workingStation.getId(),
							dateIterator.isDateForward());
					if (resourceEfficiency != null && resourceEfficiency.isGreaterThanZero())
					{
						WorkLoad workLoad = getOrCreateWorkLoad(workLoadCache, nextDate.toDate(), workingStation);
						GroupMeasureValue workLoadTotalQuantity = workLoad.getQuantityAvailable();
						GroupMeasureValue availableWorkLoad = workLoadTotalQuantity == null ? resourceEfficiency : resourceEfficiency
								.substract(workLoadTotalQuantity);

						if (availableWorkLoad.isGreaterThanZero())
						{
							GroupMeasureValue workLoadDetailQuantity;
							if (workUnits.compareTo(availableWorkLoad) > 0)
							{
								workLoadDetailQuantity = new GroupMeasureValue(workUnits.getGroupMeasure());
								workLoadDetailQuantity.setValue(BigDecimal.ZERO);
								workLoadDetailQuantity = workLoadDetailQuantity.add(availableWorkLoad);
							}
							else
							{
								workLoadDetailQuantity = workUnits;
							}

							// adjust workUnits to the amount left to be distributed
							workUnits = workUnits.substract(workLoadDetailQuantity);

							WorkLoadDetail workLoadDetail = workLoadFactoryDetail.create();
							workLoadDetail.setQuantity(workLoadDetailQuantity);
							orderDetail.addToWorkLoadDetails(workLoadDetail);
							workLoad.addToDetails(workLoadDetail);

							entityManager.persist(workLoadDetail);
							if (lastWorkLoadDate == null || lastWorkLoadDate.isBefore(nextDate))
							{
								lastWorkLoadDate = nextDate;
							}
						}
					}
				}
			}

			if (lastWorkLoadDate != null)
			{
				Date shippingDate = getShippingDateFromLastWorkLoadDate(lastWorkLoadDate, orderDetail, workingStation, manufacturingDelay, true);

				orderDetail.setShippingDateWithoutResetingWorkLoadDetails(shippingDate);
			}
			else
			{
				result.add(new LocalizableSimpleValidationMessage("unableToDistributeOrderDetailWorkLoad", Severity.ERROR, new Serializable[0]));
			}
		}
	}

	@Override
	public Date getShippingDateFromLastWorkLoadDate(DateMidnight lastWorkLoadDate, MacOrderDetail orderDetail, Resource resource, Integer manufacturingDelay,
			boolean dateForward)
	{
		// add manufacturing delay (working days)
		lastWorkLoadDate = getDateWithManufacturingDelay(lastWorkLoadDate, resource, manufacturingDelay, dateForward);

		// find the closest shipping date (based on the route)
		return getNextValidShippingDate(orderDetail, lastWorkLoadDate.toDate());
	}

	/**
	 * Generates the {@link WorkLoadDetail} for an order detail on a specific date (validation is assumed to have been done prior to invoking this method). This
	 * method doesn't attempt to distribute the work load, it simply adds it all on the same day regardless of that day's current work load.
	 * 
	 * @param orderDetail
	 * @param workingStation
	 * @param supplyingDelay
	 * @param manufacturingDelay
	 * @param workUnits
	 * @param workLoadCache
	 * @param resourceEfficiencyCache
	 * @param result
	 */
	private void generateWorkLoadDetailSpecificDate(MacOrderDetail orderDetail, WorkingStation workingStation, Integer supplyingDelay,
			Integer manufacturingDelay, GroupMeasureValue workUnits, Cache<ResourceAndDateCacheKey, WorkLoad> workLoadCache,
			Cache<ResourceAndDateCacheKey, Optional<GroupMeasureValue>> resourceEfficiencyCache, ValidationResult result)
	{
		DateMidnight minDate = getEarliestWorkDate(supplyingDelay);
		DateMidnight date = getDateWithManufacturingDelay(new DateMidnight(orderDetail.getShippingDate()), workingStation, manufacturingDelay, false);

		if (date.isBefore(minDate))
		{
			// date doesn't respect delays, we allow it anyway but log a warning
			result.add(new LocalizableSimpleValidationMessage("orderDetailSpecifiedDateNotFeasibleButAllowingAnyway", Severity.WARNING, new Serializable[] {
					orderDetail.getIdentifier(), ERROR_DATE_FORMAT.print(new DateMidnight(orderDetail.getShippingDate().getTime())),
					ERROR_DATE_FORMAT.print(date) }));
		}

		WorkLoad workLoad = getOrCreateWorkLoad(workLoadCache, date.toDate(), workingStation);

		WorkLoadDetail workLoadDetail = workLoadFactoryDetail.create();
		workLoadDetail.setQuantity(workUnits);
		orderDetail.addToWorkLoadDetails(workLoadDetail);
		workLoad.addToDetails(workLoadDetail);

		entityManager.persist(workLoadDetail);
	}

	private WorkingStation findWorkingStation(String code)
	{
		BOQuery boQuery = new BOQuery(WorkingStation.class);
		boQuery.addEqual(WorkingStation.PROPERTYNAME_UPPERCODE, code.toUpperCase());
		boQuery.addEqual(WorkingStation.PROPERTYNAME_ACTIVE, true);

		List<WorkingStation> workingStations = loaderLocal.findByBOQuery(WorkingStation.class, null, boQuery, false);
		return workingStations.isEmpty() ? null : workingStations.get(0);
	}

	private WorkLoad getOrCreateWorkLoad(Cache<ResourceAndDateCacheKey, WorkLoad> workLoadCache, Date date, WorkingStation workingStation)
	{
		// first look in the work load cache
		ResourceAndDateCacheKey workLoadCacheKey = new ResourceAndDateCacheKey(workingStation.getId(), date);
		WorkLoad workLoad = workLoadCache.getIfPresent(workLoadCacheKey);

		if (workLoad == null)
		{
			// not found in cache, look for an existing one in the database
			BOQuery boQuery = new BOQuery(WorkLoad.class);
			boQuery.addEqual(WorkLoad.PROPERTYNAME_DATE, date);
			boQuery.addEqual(WorkLoad.PROPERTYNAME_RESOURCE + "." + WorkingStation.PROPERTYNAME_ID, workingStation.getId());
			List<WorkLoad> workLoads = loaderLocal.findByBOQuery(WorkLoad.class, null, boQuery, false);

			if (workLoads.isEmpty())
			{
				// work load doesn't exist for that date/working station combination, we create one
				workLoad = workLoadFactory.create(date, workingStation);
				entityManager.persist(workLoad);
			}
			else
			{
				workLoad = workLoads.get(0);
			}
		}
		else
		{
			// found in cache, we must invalidate the quantities before returning since the work load details might have been modified
			workLoad.invalidateQuantities();
		}

		workLoadCache.put(workLoadCacheKey, workLoad);
		return workLoad;
	}

	/**
	 * Returns today's date augmented by one day (constant safety buffer) and the specified supplying delay (not working days).
	 * 
	 * @param supplyingDelay
	 *            Number of days (not working days) represented by the delay.
	 * @return
	 */
	private DateMidnight getEarliestWorkDate(Integer supplyingDelay)
	{
		// regardless of the supplying delay, add an extra day as a buffer (starts as of tomorrow)
		DateMidnight date = new DateMidnight().plusDays(1);

		if (supplyingDelay != null)
		{
			date = date.plusDays(supplyingDelay);
		}
		return date;
	}

	/**
	 * Returns the date modified by the the specified manufacturing delay (working days based on resource efficiency). If the date specified is not a working
	 * day, the date is moved to the next or previous working day before applying the actual delay (depending on <code>dateForward</code>). Note that this
	 * method might return a date before today's date when looking for a backward delay.
	 * 
	 * @param date
	 * @param resource
	 * @param manufacturingDelay
	 *            Number of working days represented by the delay.
	 * @param dateForward
	 *            Whether dates are being looked forward or backward.
	 * @return
	 */
	public DateMidnight getDateWithManufacturingDelay(DateMidnight date, Resource resource, Integer manufacturingDelay, boolean dateForward)
	{
		// we must start on a working day (even if there is no delay), so adjust the delay if today is not a working day
		GroupMeasureValue workShiftProductionTime = workShiftUtils.getWorkShiftProductionTime(date.toDate(), resource.getWorkShift());
		if (workShiftProductionTime == null || !workShiftProductionTime.isGreaterThanZero())
		{
			manufacturingDelay = manufacturingDelay == null ? 1 : manufacturingDelay + 1;
		}

		if (manufacturingDelay != null)
		{
			int nbAttempt = 0;
			while (manufacturingDelay > 0 && nbAttempt < MAX_MANUFACTURING_DELAY_ATTEMPTS)
			{
				date = dateForward ? date.plusDays(1) : date.minusDays(1);
				workShiftProductionTime = workShiftUtils.getWorkShiftProductionTime(date.toDate(), resource.getWorkShift());
				if (workShiftProductionTime != null && workShiftProductionTime.isGreaterThanZero())
				{
					manufacturingDelay--;
				}
				nbAttempt++;
			}
		}
		return date;
	}

	/**
	 * Based on the order detail's route, find the previous available shipping date (fallback: leave the date as is).
	 * 
	 * @param orderDetail
	 * @param date
	 * @return
	 */
	private Date getPreviousValidShippingDate(MacOrderDetail orderDetail, Date date)
	{
		if (orderDetail.getRoute() != null)
		{
			Date previousShippingDate = shippingLocal.findPreviousValidShippingDate(orderDetail.getRoute().getId(), orderDetail.getTransactionDate(), date);
			if (previousShippingDate != null)
			{
				date = previousShippingDate;
			}
		}
		return date;
	}

	/**
	 * Based on the order detail's route, find the next available shipping date (fallback: leave the date as is).
	 * 
	 * @param orderDetail
	 * @param date
	 * @return
	 */
	@Override
	public Date getNextValidShippingDate(MacOrderDetail orderDetail, Date date)
	{
		if (orderDetail.getRoute() != null)
		{
			Date nextShippingDate = shippingLocal.findNextValidShippingDate(orderDetail.getRoute().getId(), orderDetail.getTransactionDate(), date,
					MAX_SHIPPING_ROUTE_ATTEMPTS);
			if (nextShippingDate != null)
			{
				date = nextShippingDate;
			}
		}
		return date;
	}

	/**
	 * Returns the resource efficiency for a specific date/resource combination. A cache is being used to avoid duplicate queries.
	 * 
	 * @param resourceEfficiencyCache
	 * @param date
	 * @param resourceId
	 * @param dateForward
	 *            Whether dates are being looked forward or backward (used as an optimization to avoid multiple queries).
	 * @return
	 */
	private GroupMeasureValue getResourceEfficiencyTimeMeasure(Cache<ResourceAndDateCacheKey, Optional<GroupMeasureValue>> resourceEfficiencyCache, Date date,
			Serializable resourceId, boolean dateForward)
	{
		return getResourceEfficiencyTimeMeasures(resourceEfficiencyCache, date, date, resourceId, dateForward).get(0);
	}

	/**
	 * Returns the resource efficiency for the resource on each date between <code>dateFrom</code> and <code>dateTo</code>. A cache is being used to avoid
	 * duplicate queries. This method is optimized to query the resource efficiency service at most once.
	 * 
	 * @param resourceEfficiencyCache
	 * @param dateFrom
	 * @param dateTo
	 * @param resourceId
	 * @param dateForward
	 *            Whether dates are being looked forward or backward (used as an optimization to avoid multiple queries).
	 * @return
	 */
	private List<GroupMeasureValue> getResourceEfficiencyTimeMeasures(Cache<ResourceAndDateCacheKey, Optional<GroupMeasureValue>> resourceEfficiencyCache,
			Date dateFrom, Date dateTo, Serializable resourceId, boolean dateForward)
	{
		return getResourceEfficiencyTimeMeasures(resourceEfficiencyCache, dateFrom, dateTo, resourceId, dateForward, DEFAULT_RESOURCE_EFFICIENCY_LOOKUP_DAYS);
	}

	/**
	 * Returns the resource efficiency for the resource on each date between <code>dateFrom</code> and <code>dateTo</code>. A cache is being used to avoid
	 * duplicate queries. This method is optimized to query the resource efficiency service at most once. It's also optimized to lookup a minimum number of days
	 * at a time (to improve the performance of loops that query one day at a time).
	 * 
	 * @param resourceEfficiencyCache
	 * @param dateFrom
	 * @param dateTo
	 * @param resourceId
	 * @param dateFoward
	 *            Whether dates are being looked forward or backward (used as an optimization to avoid multiple queries).
	 * @param resourceEfficiencyLookupDays
	 *            Minimum number of days used when querying the resource efficiency service.
	 * @return
	 */
	private List<GroupMeasureValue> getResourceEfficiencyTimeMeasures(Cache<ResourceAndDateCacheKey, Optional<GroupMeasureValue>> resourceEfficiencyCache,
			Date dateFrom, Date dateTo, Serializable resourceId, boolean dateFoward, int resourceEfficiencyLookupDays)
	{
		// verify if all resource efficiencies are in the cache (find any missing date)
		List<Date> missingDates = new ArrayList<Date>();
		for (DateBetweenIterator dateIterator = new DateBetweenIterator(dateFrom, dateTo); dateIterator.hasNext();)
		{
			Date date = dateIterator.next();
			if (resourceEfficiencyCache.getIfPresent(new ResourceAndDateCacheKey(resourceId, date)) == null)
			{
				missingDates.add(date);
			}
		}

		if (!missingDates.isEmpty())
		{
			// some resource efficiencies are missing, query the resourceEfficiency service and populate the cache
			DateMidnight firstMissingDate = new DateMidnight(missingDates.get(0));
			DateMidnight lastMissingDate = new DateMidnight(missingDates.get(missingDates.size() - 1));
			DateMidnight today = new DateMidnight();

			// optimization to populate multiple days at a time when querying the cache, to avoid multiple queries
			if (dateFoward)
			{
				DateMidnight dateWithLookupDays = firstMissingDate.plusDays(resourceEfficiencyLookupDays - 1);
				if (dateWithLookupDays.isAfter(lastMissingDate))
				{
					lastMissingDate = dateWithLookupDays;
				}
			}
			else if (!lastMissingDate.isBefore(today))
			{
				DateMidnight dateWithLookupDays = firstMissingDate.minusDays(resourceEfficiencyLookupDays - 1);
				if (dateWithLookupDays.isBefore(lastMissingDate))
				{
					firstMissingDate = dateWithLookupDays.isBefore(today) ? today : dateWithLookupDays;
				}
			}

			List<GroupMeasureValue> resourceEfficiencyTimeMeasures = resourceEfficiencyLocal.getResourceEfficiencyTimeMeasures(firstMissingDate.toDate(),
					lastMissingDate.toDate(), resourceId);
			DateBetweenIterator dateIterator = new DateBetweenIterator(firstMissingDate.toDate(), lastMissingDate.toDate());
			for (GroupMeasureValue resourceEfficiencyTimeMeasure : resourceEfficiencyTimeMeasures)
			{
				resourceEfficiencyCache.put(new ResourceAndDateCacheKey(resourceId, dateIterator.next()), Optional.fromNullable(resourceEfficiencyTimeMeasure));
			}
		}

		// the cache necessarily contains all the required resource efficiencies at this point, query it and return the results
		List<GroupMeasureValue> resourceEfficiencyTimeMeasures = new ArrayList<GroupMeasureValue>();
		for (DateBetweenIterator dateIterator = new DateBetweenIterator(dateFrom, dateTo); dateIterator.hasNext();)
		{
			resourceEfficiencyTimeMeasures.add(resourceEfficiencyCache.getIfPresent(new ResourceAndDateCacheKey(resourceId, dateIterator.next())).orNull());
		}
		return resourceEfficiencyTimeMeasures;
	}

	public static class ResourceAndDateCacheKey
	{
		private final Serializable resourceId;
		private final Date date;

		public ResourceAndDateCacheKey(WorkLoad workLoad)
		{
			this(workLoad.getResource().getId(), workLoad.getDate());
		}

		public ResourceAndDateCacheKey(Serializable resourceId, Date date)
		{
			this.resourceId = resourceId;
			this.date = date;
		}

		public Serializable getResourceId()
		{
			return resourceId;
		}

		public Date getDate()
		{
			return date;
		}

		@Override
		public boolean equals(Object o)
		{
			if (o instanceof ResourceAndDateCacheKey)
			{
				ResourceAndDateCacheKey workLoadCacheKey = (ResourceAndDateCacheKey) o;
				return workLoadCacheKey.getResourceId().equals(getResourceId()) && DateUtils.isSameDay(workLoadCacheKey.getDate(), getDate());
			}
			return false;
		}

		@Override
		public int hashCode()
		{
			return getResourceId().hashCode() + getDate().hashCode();
		}
	}

	/**
	 * Date iterator that allows a maximum number of iterations before stopping to return dates (to avoid infinite loops when no more future dates will be
	 * available).
	 */
	private static abstract class AbstractNextDateIterator implements Iterator<DateMidnight>
	{
		private int nbAttempts = 0;

		@Override
		public final boolean hasNext()
		{
			return nbAttempts < MAX_SHIPPING_DATE_ATTEMPTS;
		}

		@Override
		public final DateMidnight next()
		{
			if (hasNext())
			{
				nbAttempts++;
				return getAndUpdateNextDate();
			}
			else
			{
				return null;
			}
		}

		@Override
		public final void remove()
		{
			throw new UnsupportedOperationException();
		}

		/**
		 * Returns the next date (and prepares the upcoming next date), this method will only be invoked if {@link #hasNext()} returns <code>true</code>;
		 * 
		 * @return
		 */
		abstract protected DateMidnight getAndUpdateNextDate();

		/**
		 * Returns whether we are looking for a date looking forward (i.e. Wednesday, then Thursday, then Friday, ...) or vice-versa.
		 * 
		 * @return
		 */
		abstract public boolean isDateForward();
	}

	/**
	 * Date iterator for an ASAP date. If there is no desired date, the iterator simply starts after the supplying delay and increases the date one day at a
	 * time. If there is a desired date, we want the work to be distributed as close to that date as possible... so we start by making sure the desired date is
	 * a valid shipping date (if not we take the first earlier valid shipping date), then we apply the manufacturing delay, and then we go backwards until the
	 * minimum date is reached (today's + supplying delay). After that date is reached, it means the desired date won't be reachable... so we resume with dates
	 * after the earliest valid desired date that was previously calculated, and continue day by day.
	 */
	private static class AsapDateIterator extends AbstractNextDateIterator
	{
		private final DateMidnight minDate;
		private DateMidnight nextDate;
		private DateMidnight validDesiredDate;
		private boolean dateForward;

		public AsapDateIterator(MacOrderServicesBean macOrderServicesBean, Date desiredDate, WorkingStation workingStation, Integer supplyingDelay,
				Integer manufacturingDelay, Cache<ResourceAndDateCacheKey, Optional<GroupMeasureValue>> resourceEfficiencyCache, MacOrderDetail orderDetail)
		{
			minDate = macOrderServicesBean.getEarliestWorkDate(supplyingDelay);

			if (desiredDate == null)
			{
				dateForward = true;
				nextDate = minDate;
			}
			else
			{
				// find the closest earlier (or equal) shipping date since the the desired date might not a valid shipping date
				validDesiredDate = new DateMidnight(macOrderServicesBean.getPreviousValidShippingDate(orderDetail, desiredDate));

				// remove the manufacturing delay
				validDesiredDate = macOrderServicesBean.getDateWithManufacturingDelay(validDesiredDate, workingStation, manufacturingDelay, false);

				if (validDesiredDate == null || validDesiredDate.compareTo(minDate) < 0)
				{
					dateForward = true;
					nextDate = minDate;
				}
				else
				{
					dateForward = false;
					nextDate = validDesiredDate;
				}
			}
		}

		@Override
		protected DateMidnight getAndUpdateNextDate()
		{
			DateMidnight dateToReturn = nextDate;
			if (dateForward)
			{
				nextDate = nextDate.plusDays(1);
			}
			else
			{
				if (!nextDate.isAfter(minDate))
				{
					dateForward = false;
					nextDate = validDesiredDate.plusDays(1);
				}
				else
				{
					nextDate = nextDate.minusDays(1);
				}
			}

			return dateToReturn;
		}

		@Override
		public boolean isDateForward()
		{
			return dateForward;
		}
	}

	@Override
	public ValidationResult blockOrderProduction(List<Serializable> orderIds, Serializable blockingReasonId, Serializable userId)
	{
		List<MacOrder> orders = loaderLocal.findByIdCollection(MacOrder.class, orderIds);
		BlockingReason blockingReason = loaderLocal.findById(BlockingReason.class, blockingReasonId);
		User user = loaderLocal.findById(User.class, userId);
		ValidationResult vr = new ValidationResult();

		for (MacOrder order : orders)
		{
			if (order.getBlockProduction() == null || order.getBlockProduction().equals(Boolean.FALSE))
			{
				order.blockUnblockProduction(blockingReason, user);
				persistence.save(order, false);
				vr.add(new SimpleValidationMessage("La commande " + order.getNumber() + " a bien \u00E9t\u00E9 bloqu\u00E9e pour la production"));
			}
			else
			{
				vr.add(new SimpleValidationMessage("La commande " + order.getNumber() + " est d\u00E9j\u00E0 bloqu\u00E9e pour la production"));
			}
		}
		return vr;
	}

	@Override
	public ValidationResult blockOrderShipping(List<Serializable> orderIds, Serializable blockingReasonId, Serializable userId)
	{
		List<MacOrder> orders = loaderLocal.findByIdCollection(MacOrder.class, orderIds);
		BlockingReason blockingReason = loaderLocal.findById(BlockingReason.class, blockingReasonId);
		User user = loaderLocal.findById(User.class, userId);
		ValidationResult vr = new ValidationResult();

		for (MacOrder order : orders)
		{
			if (order.getBlockShipping() == null || order.getBlockShipping().equals(Boolean.FALSE))
			{
				order.blockUnblockShipping(blockingReason, user);
				persistence.save(order, false);
				vr.add(new SimpleValidationMessage("La commande " + order.getNumber() + " a bien \u00E9t\u00E9 bloqu\u00E9e en exp\u00E9dition"));
			}
			else
			{
				vr.add(new SimpleValidationMessage("La commande " + order.getNumber() + " est d\u00E9j\u00E0 bloqu\u00E9e pour l'exp\u00E9dition"));
			}
		}
		return vr;
	}

	@Override
	public ValidationResult unblockOrderProduction(List<Serializable> orderIds, Serializable blockingReasonId, Serializable userId)
	{
		List<MacOrder> orders = loaderLocal.findByIdCollection(MacOrder.class, orderIds);
		BlockingReason blockingReason = loaderLocal.findById(BlockingReason.class, blockingReasonId);
		User user = loaderLocal.findById(User.class, userId);
		ValidationResult vr = new ValidationResult();

		for (MacOrder order : orders)
		{
			if (order.getBlockProduction() == null || order.getBlockProduction().equals(Boolean.FALSE))
			{
				vr.add(new SimpleValidationMessage("La commande " + order.getNumber() + " est d\u00E9j\u00E0 d\u00E9bloqu\u00E9e pour la production"));
			}
			else
			{
				order.blockUnblockProduction(blockingReason, user);
				persistence.save(order, false);
				vr.add(new SimpleValidationMessage("La commande " + order.getNumber() + " a bien \u00E9t\u00E9 d\u00E9bloqu\u00E9e pour la production"));
			}
		}
		return vr;
	}

	@Override
	public ValidationResult unblockOrderShipping(List<Serializable> orderIds, Serializable blockingReasonId, Serializable userId)
	{
		List<MacOrder> orders = loaderLocal.findByIdCollection(MacOrder.class, orderIds);
		BlockingReason blockingReason = loaderLocal.findById(BlockingReason.class, blockingReasonId);
		User user = loaderLocal.findById(User.class, userId);
		ValidationResult vr = new ValidationResult();

		for (MacOrder order : orders)
		{
			if (order.getBlockShipping() == null || order.getBlockShipping().equals(Boolean.FALSE))
			{
				vr.add(new SimpleValidationMessage("La commande " + order.getNumber() + " est d\u00E9j\u00E0 d\u00E9bloqu\u00E9e pour l'exp\u00E9dition"));
			}
			else
			{
				order.blockUnblockShipping(blockingReason, user);
				persistence.save(order, false);
				vr.add(new SimpleValidationMessage("La commande " + order.getNumber() + " a bien \u00E9t\u00E9 d\u00E9bloqu\u00E9e en exp\u00E9dition"));
			}
		}
		return vr;
	}

	@Override
	public ValidationResult resetToCustomerBlockProduction(List<Serializable> orderIds, Serializable userId)
	{
		// PBYTODO resetToCustomerBlockProduction
		List<MacOrder> orders = loaderLocal.findByIdCollection(MacOrder.class, orderIds);
		User user = loaderLocal.findById(User.class, userId);
		ValidationResult vr = new ValidationResult();

		for (MacOrder order : orders)
		{
			if (order.getBlockProduction() != null)
			{
				order.resetToCustomerBlockProductionDefault(user);
				persistence.save(order, false);
				if (order.getCustomer().isBlockProduction())
				{
					vr.add(new SimpleValidationMessage("la commande " + order.getNumber()
							+ " est d\u00E9bloqu\u00E9e pour la production, mais le client est bloqu\u00E9 pour la production"));
				}
				else
				{
					vr.add(new SimpleValidationMessage("la commande " + order.getNumber() + " est d\u00E9bloqu\u00E9e en production"));
				}
			}
			else
			{
				vr.add(new SimpleValidationMessage("La commande " + order.getNumber() + " est d\u00E9j\u00E0 d\u00E9bloqu\u00E9e pour la production"));
			}
		}
		return vr;
	}

	@Override
	public ValidationResult resetToCustomerBlockShipping(List<Serializable> orderIds, Serializable userId)
	{
		// PBYTODO resetToCustomerBlockProduction
		List<MacOrder> orders = loaderLocal.findByIdCollection(MacOrder.class, orderIds);
		User user = loaderLocal.findById(User.class, userId);
		ValidationResult vr = new ValidationResult();

		for (MacOrder order : orders)
		{
			if (order.getBlockShipping() != null)
			{
				order.resetToCustomerBlockShippingDefault(user);
				persistence.save(order, false);
				if (order.getCustomer().isBlockShipping())
				{
					vr.add(new SimpleValidationMessage("la commande " + order.getNumber()
							+ " est d\u00E9bloqu\u00E9e pour l'exp\u00E9dition, mais le client est bloqu\u00E9 pour la production"));
				}
				else
				{
					vr.add(new SimpleValidationMessage("Apr\u00E8s application du blocage/d\u00E9blocage client, la commande " + order.getNumber()
							+ " est bloqu\u00E9e en exp\u00E9dition"));
				}
			}
			else
			{
				vr.add(new SimpleValidationMessage("La commande " + order.getNumber() + " est d\u00E9j\u00E0 d\u00E9bloqu\u00E9e pour l'exp\u00E9dition"));
			}
		}
		return vr;
	}

	@Override
	public List<Serializable> clearWorkLoadDetails(List<Serializable> orderIds)
	{
		List<MacOrder> orders = loaderLocal.findByIdCollection(MacOrder.class, null, orderIds);

		ArrayList<Serializable> resultOrderIds = new ArrayList<Serializable>();
		for (MacOrder order : orders)
		{
			if (order.getMacDocumentStatus().isAllowWorkLoadModifications() && !order.isExploded())
			{
				resultOrderIds.add(order.getId());
			}
		}

		if (!resultOrderIds.isEmpty())
		{
			List<Criterion> criteria = new ArrayList<Criterion>();
			List<String> subcriteria = new ArrayList<String>();
			List<Integer> subcriterionJoinSpec = new ArrayList<Integer>();

			criteria.add(Restrictions.in(SaleTransaction.PROPERTYNAME_ID, resultOrderIds));
			subcriteria.add(WorkLoadDetail.PROPERTYNAME_ORDERDETAIL + "." + OrderDetail.PROPERTYNAME_SALETRANSACTION);
			subcriterionJoinSpec.add(CriteriaSpecification.INNER_JOIN);

			List<WorkLoadDetail> workLoadDetails = loaderLocal.findByCriteria(WorkLoadDetail.class, null, criteria, subcriteria, subcriterionJoinSpec);
			for (WorkLoadDetail workLoadDetail : workLoadDetails)
			{
				persistence.delete(workLoadDetail);
			}
		}

		return resultOrderIds;
	}
}
