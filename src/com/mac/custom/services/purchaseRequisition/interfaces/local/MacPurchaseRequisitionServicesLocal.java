package com.mac.custom.services.purchaseRequisition.interfaces.local;

import javax.ejb.Local;

import com.mac.custom.services.purchaseRequisition.interfaces.MacPurchaseRequisitionServices;

@Local
public interface MacPurchaseRequisitionServicesLocal extends MacPurchaseRequisitionServices
{

}
