package com.mac.custom.services.purchaseRequisition.interfaces.remote;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Remote;

import com.mac.custom.bo.PurchaseRequisitionRepartition;
import com.mac.custom.services.purchaseRequisition.interfaces.MacPurchaseRequisitionServices;

@Remote
public interface MacPurchaseRequisitionServicesRemote extends MacPurchaseRequisitionServices
{
	public PurchaseRequisitionRepartition generatePurchaesRequisitionRepartition(List<Serializable> purchaseRequisitionIds);

	public void distributePurchaseRequisition(PurchaseRequisitionRepartition purchaseRequisitionRepartition);
}
