package com.mac.custom.services.purchaseRequisition.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.mac.custom.bo.PurchaseRequisitionProduct;
import com.mac.custom.bo.PurchaseRequisitionRepartition;
import com.mac.custom.bo.PurchaseRequisitionSupplier;
import com.mac.custom.dao.PurchaseRequisitionRepartitionDAO;
import com.mac.custom.services.purchaseRequisition.interfaces.local.MacPurchaseRequisitionServicesLocal;
import com.mac.custom.services.purchaseRequisition.interfaces.remote.MacPurchaseRequisitionServicesRemote;
import com.netappsid.erp.server.bo.Product;
import com.netappsid.erp.server.bo.PurchaseRequisition;
import com.netappsid.erp.server.bo.Supplier;
import com.netappsid.erp.server.datatypes.GroupMeasureValue;
import com.netappsid.services.interfaces.local.LoaderLocal;
import com.netappsid.services.interfaces.local.PersistenceLocal;

@Stateless
public class MacPurchaseRequisitionServicesBean implements MacPurchaseRequisitionServicesLocal, MacPurchaseRequisitionServicesRemote
{

	@EJB(mappedName = "erp/PersistenceBean/local")
	public PersistenceLocal persistence;

	@EJB
	public LoaderLocal loaderLocal;

	@Override
	public PurchaseRequisitionRepartition generatePurchaesRequisitionRepartition(List<Serializable> purchaseRequisitionIds)
	{
		StringBuffer hql = new StringBuffer();
		List<String> ids = new ArrayList<String>();
		Map<String, Object> parameters = new HashMap<String, Object>();
		List result = new ArrayList();
		PurchaseRequisitionRepartition purchaseRequisitionRepartition = new PurchaseRequisitionRepartition();
		Product oldProduct = null;
		PurchaseRequisition purchaseRequisition;
		Supplier supplier;

		for (Serializable id : purchaseRequisitionIds)
		{
			ids.add(id.toString());
		}

		parameters.put("ids", ids);

		hql.append("SELECT purchaseRequisition, productSupplier.supplier ").append("\n");
		hql.append("FROM PurchaseRequisition purchaseRequisition ").append("\n");
		hql.append("INNER JOIN purchaseRequisition.product.suppliers productSupplier ").append("\n");
		hql.append("LEFT JOIN purchaseRequisition.purchaseOrderDetail purchaseOrderDetail ").append("\n");
		hql.append("WHERE purchaseRequisition.id IN (:ids) ").append("\n");
		hql.append("AND purchaseOrderDetail.id IS NULL ").append("\n");
		hql.append("ORDER BY purchaseRequisition.product ").append("\n");

		result = loaderLocal.findByQuery(hql.toString(), parameters, false);
		PurchaseRequisitionProduct purchaseRequisitionProduct = null;
		PurchaseRequisitionSupplier purchaseRequisitionSupplier = null;

		PurchaseRequisitionRepartitionDAO purchaseRequisitionRepartitionDAO = new PurchaseRequisitionRepartitionDAO();

		for (int i = 0; i < result.size(); i++)
		{
			purchaseRequisition = ((PurchaseRequisition) ((Object[]) result.get(i))[0]);
			supplier = ((Supplier) ((Object[]) result.get(i))[1]);

			purchaseRequisitionRepartitionDAO.loadAssociations(purchaseRequisition);

			if (oldProduct == null || !oldProduct.hasSameID(purchaseRequisition.getProduct()))
			{
				oldProduct = purchaseRequisition.getProduct();
				if (purchaseRequisitionProduct != null)
				{
					purchaseRequisitionRepartition.addToProducts(purchaseRequisitionProduct);
					purchaseRequisitionProduct = new PurchaseRequisitionProduct();
				}

				purchaseRequisitionProduct = new PurchaseRequisitionProduct();
				purchaseRequisitionProduct.setProduct(purchaseRequisition.getProduct());
				purchaseRequisitionProduct.setPercentage(BigDecimal.ZERO);
			}

			if (!containsSupplier(purchaseRequisitionProduct, supplier))
			{
				purchaseRequisitionRepartitionDAO.loadAssociations(supplier);
				purchaseRequisitionSupplier = new PurchaseRequisitionSupplier();
				purchaseRequisitionSupplier.setSupplier(supplier);
				purchaseRequisitionSupplier.setPercentage(BigDecimal.ZERO);
				purchaseRequisitionProduct.addToSuppliers(purchaseRequisitionSupplier);
			}

			if (!containsPurchaseRequisition(purchaseRequisitionProduct, purchaseRequisition))
			{
				purchaseRequisitionProduct.addToPurchaseRequisitions(purchaseRequisition);
			}
		}

		if (purchaseRequisitionProduct != null)
		{
			purchaseRequisitionRepartition.addToProducts(purchaseRequisitionProduct);
		}

		return purchaseRequisitionRepartition;
	}

	private boolean containsSupplier(PurchaseRequisitionProduct purchaseRequisitionProduct, Supplier supplier)
	{
		boolean containsSupplier = false;

		for (PurchaseRequisitionSupplier purchaseRequisitionSupplier : purchaseRequisitionProduct.getSuppliers())
		{
			if (purchaseRequisitionSupplier.getSupplier().hasSameID(supplier))
			{
				containsSupplier = true;
				break;
			}
		}

		return containsSupplier;
	}

	private boolean containsPurchaseRequisition(PurchaseRequisitionProduct purchaseRequisitionProduct, PurchaseRequisition purchaseRequisition)
	{
		boolean containsPurchaseRequisition = false;

		for (PurchaseRequisition requisition : purchaseRequisitionProduct.getPurchaseRequisitions())
		{
			if (requisition.hasSameID(purchaseRequisition))
			{
				containsPurchaseRequisition = true;
				break;
			}
		}

		return containsPurchaseRequisition;
	}

	@Override
	public void distributePurchaseRequisition(PurchaseRequisitionRepartition purchaseRequisitionRepartition)
	{
		Supplier supplier;

		Comparator<PurchaseRequisitionSupplier> purchaseRequisitionSupplierComparator = new Comparator<PurchaseRequisitionSupplier>()
			{

				@Override
				public int compare(PurchaseRequisitionSupplier arg0, PurchaseRequisitionSupplier arg1)
				{
					return arg0.getPercentage().compareTo(arg1.getPercentage());
				}

			};

		for (PurchaseRequisitionProduct product : purchaseRequisitionRepartition.getProducts())
		{
			GroupMeasureValue totalQuantity = null;
			for (PurchaseRequisition purchaseRequisition : product.getPurchaseRequisitions())
			{
				if (totalQuantity == null)
				{
					totalQuantity = purchaseRequisition.getQuantity();
				}
				else
				{
					totalQuantity = totalQuantity.add(purchaseRequisition.getQuantity());
				}
			}

			Collections.sort(product.getSuppliers(), purchaseRequisitionSupplierComparator);
			for (PurchaseRequisitionSupplier purchaseRequisitionSupplier : product.getSuppliers())
			{
				supplier = loaderLocal.findById(Supplier.class, null, purchaseRequisitionSupplier.getSupplier().getId());

				for (PurchaseRequisition purchaseRequisition : product.getPurchaseRequisitions())
				{
					if (purchaseRequisitionSupplier.getPercentage().setScale(0, BigDecimal.ROUND_UP).equals(new BigDecimal(100)))
					{
						updatePurchaseRequisition(purchaseRequisition, supplier, totalQuantity.divide(new BigDecimal(product.getPurchaseRequisitions().size())));
					}
					else
					{
						createOrUpdatePurchaseRequisition(purchaseRequisition, supplier, product,
								totalQuantity.multiply(purchaseRequisitionSupplier.getPercentage().divide(new BigDecimal(100))));
					}
				}
			}
		}
	}

	private void updatePurchaseRequisition(PurchaseRequisition purchaseRequisition, Supplier supplier, GroupMeasureValue newQuantity)
	{
		purchaseRequisition.setSupplier(supplier);
		purchaseRequisition.setQuantity(newQuantity);
		persistence.save(purchaseRequisition);
	}

	private void createPurchaseRequisition(PurchaseRequisition existingPurchaseRequisition, Supplier supplier, GroupMeasureValue newQuantity)
	{
		PurchaseRequisition purchaseRequisition = (PurchaseRequisition) existingPurchaseRequisition.clone();
		purchaseRequisition.setSupplier(supplier);
		purchaseRequisition.setQuantity(newQuantity);
		persistence.save(purchaseRequisition);
	}

	private void createOrUpdatePurchaseRequisition(PurchaseRequisition purchaseRequisition, Supplier supplier, PurchaseRequisitionProduct product,
			GroupMeasureValue newQuantity)
	{
		List<Criterion> criterions = new ArrayList<Criterion>();
		criterions.add(Restrictions.eq(PurchaseRequisition.PROPERTYNAME_SUPPLIER, supplier));
		criterions.add(Restrictions.eq(PurchaseRequisition.PROPERTYNAME_PRODUCT, product.getProduct()));
		criterions.add(Restrictions.eq(PurchaseRequisition.PROPERTYNAME_NEEDEDDATE, purchaseRequisition.getNeededDate()));
		List<PurchaseRequisition> purchaseRequisitions = loaderLocal.findByCriteria(PurchaseRequisition.class, null, criterions, null, null);

		if (purchaseRequisitions.isEmpty() && newQuantity.isGreaterThanZero())
		{
			createPurchaseRequisition(purchaseRequisition, supplier, newQuantity);
		}
		else if (!purchaseRequisitions.isEmpty())
		{
			updatePurchaseRequisition(purchaseRequisitions.get(0), supplier, newQuantity);
		}
		else
		{
			updatePurchaseRequisition(purchaseRequisition, supplier, newQuantity);
		}
	}
}
