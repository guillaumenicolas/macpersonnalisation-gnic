package com.mac.custom.services.workOrder.interfaces.local;

import javax.ejb.Local;

import com.mac.custom.services.workOrder.interfaces.MacWorkOrderServices;

@Local
public interface MacWorkOrderServicesLocal extends MacWorkOrderServices
{

}
