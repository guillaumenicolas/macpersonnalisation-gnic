package com.mac.custom.services.workOrder.interfaces.remote;

import javax.ejb.Remote;

import com.mac.custom.services.workOrder.interfaces.MacWorkOrderServices;

@Remote
public interface MacWorkOrderServicesRemote extends MacWorkOrderServices
{

}
