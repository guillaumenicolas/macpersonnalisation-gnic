package com.mac.custom.services.workOrder.interfaces;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.netappsid.erp.server.bo.RelocationDetail;

public interface MacWorkOrderServices
{
	public List<RelocationDetail> getStockToTransfertFromWorkOrderDetail(Date dateFrom, Date dateTo, Serializable productionLocationId,
			Serializable storageLocationId);
}
