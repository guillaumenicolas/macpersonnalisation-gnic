package com.mac.custom.services.workOrder.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.mac.custom.bo.MacLocation;
import com.mac.custom.services.workOrder.interfaces.local.MacWorkOrderServicesLocal;
import com.mac.custom.services.workOrder.interfaces.remote.MacWorkOrderServicesRemote;
import com.netappsid.erp.server.bo.InventoryProduct;
import com.netappsid.erp.server.bo.Location;
import com.netappsid.erp.server.bo.ProductLocation;
import com.netappsid.erp.server.bo.ProductSupplier;
import com.netappsid.erp.server.bo.PurchaseOrderDetail;
import com.netappsid.erp.server.bo.PurchaseRequisition;
import com.netappsid.erp.server.bo.RelocationDetail;
import com.netappsid.erp.server.bo.RelocationDetailFrom;
import com.netappsid.erp.server.bo.RelocationDetailTo;
import com.netappsid.erp.server.bo.WorkOrderDetail;
import com.netappsid.erp.server.datatypes.GroupMeasureValue;
import com.netappsid.erp.server.factory.PurchaseOrderDetailFactory;
import com.netappsid.services.interfaces.local.LoaderLocal;

@Stateless
public class MacWorkOrderServicesBean implements MacWorkOrderServicesLocal, MacWorkOrderServicesRemote
{
	@EJB
	private LoaderLocal loader;

	@Override
	public List<RelocationDetail> getStockToTransfertFromWorkOrderDetail(Date dateFrom, Date dateTo, Serializable productionLocationId,
			Serializable storageLocationId)
	{
		StringBuffer hql = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		Map<Serializable, Object[]> groupWorkOrderDetails = new HashMap<Serializable, Object[]>();

		hql.append("SELECT workOrderDetail ");
		hql.append("FROM WorkOrderDetail workOrderDetail ");
		hql.append("INNER JOIN workOrderDetail.inventoryProduct inventoryProduct ");
		hql.append("INNER JOIN workOrderDetail.workOrderRoutingStep workOrderRoutingStep ");
		hql.append("INNER JOIN workOrderRoutingStep.workOrder workOrder ");
		hql.append("WHERE workOrderRoutingStep.quantityProduceConvertedValue = 0 ");
		hql.append("AND workOrderDetail.purchaseRequisition IS NULL ");
		hql.append("AND workOrderDetail.parents IS EMPTY ");
		hql.append("AND inventoryProduct.affectsInventory = true ");
		hql.append("AND workOrder.startDate >= :dateFrom ");
		hql.append("AND workOrder.startDate <= :dateTo ");
		parameters.put("dateFrom", dateFrom);
		parameters.put("dateTo", dateTo);

		List<WorkOrderDetail> workOrderDetails = loader.findByQuery(hql.toString(), parameters, false);

		for (WorkOrderDetail workOrderDetail : workOrderDetails)
		{
			Object[] products;
			Serializable productId = workOrderDetail.getInventoryProduct().getId();

			if (groupWorkOrderDetails.containsKey(productId))
			{
				// Si produit inventaire trouvé dans la liste de groupe
				products = groupWorkOrderDetails.get(productId);
			}
			else
			{
				// Ajoute un nouvel élément dans la liste de groupe
				products = new Object[2];
				products[0] = workOrderDetail.getInventoryProduct();
				products[1] = workOrderDetail.getInventoryProduct().getDefaultGroupMeasureValue(null);
			}

			// Additionner ce que contient le produit du groupe avec la quantité de workOrderDetail
			products[1] = ((GroupMeasureValue) products[1]).add(workOrderDetail.getTotalQuantity());

			groupWorkOrderDetails.put(productId, products);
		}

		// Prendre la localisation du produit (source)
		MacLocation initialStorageLocation = loader.findById(MacLocation.class, storageLocationId);
		List<Location> storageLocations = new ArrayList<Location>();
		if (initialStorageLocation.isStore())
		{
			// no location to take the stock from, stop
			if (initialStorageLocation.getSubLocations().isEmpty())
			{
				return Collections.emptyList();
			}
			storageLocations.addAll(initialStorageLocation.getSubLocations());
		}
		else
		{
			storageLocations.add(initialStorageLocation);
		}

		// Prendre la localisation du produit (destination)
		MacLocation productionLocation = loader.findById(MacLocation.class, productionLocationId);
		if (productionLocation.isStore())
		{
			productionLocation = productionLocation.getDefaultSubLocation();
			// no default location, stop here
			if (productionLocation == null)
			{
				return Collections.emptyList();
			}
		}

		List<RelocationDetail> relocationDetails = new ArrayList<RelocationDetail>();

		for (Object[] value : groupWorkOrderDetails.values())
		{
			// Prendre les valeurs contenues dans le groupe
			InventoryProduct inventoryProduct = (InventoryProduct) value[0];
			GroupMeasureValue quantity = (GroupMeasureValue) value[1];

			GroupMeasureValue quantityToReloc = null;

			// Destination
			ProductLocation productionProductLocation = inventoryProduct.getProductLocation(productionLocation);

			List<ProductLocation> storageProductLocations = new ArrayList<ProductLocation>();
			for (Location storageLocation : storageLocations)
			{
				ProductLocation storageProductLocation = inventoryProduct.getProductLocation(storageLocation);
				if (storageProductLocation != null)
				{
					storageProductLocations.add(storageProductLocation);
				}
			}
			// sort by quantity to remove from location with the less stock first
			Collections.sort(storageProductLocations, new Comparator<ProductLocation>()
				{
					@Override
					public int compare(ProductLocation o1, ProductLocation o2)
					{
						return o1.getQuantityInStockToInventoryUnit().compareTo(o2.getQuantityInStockToInventoryUnit());
					}
				});

			// Si le produit est dans le storage (source)
			if (!storageProductLocations.isEmpty())
			{
				// Weirdo this name....
				GroupMeasureValue missingQuantity = null;

				if (productionProductLocation == null)
				{
					// pas de product location (pas de destination)
					missingQuantity = quantity;
				}
				else if (productionProductLocation.getQuantityInStockToInventoryUnit().compareTo(quantity) < 0)
				{
					// Si il reste assez de qty en stock à la product location (destination)
					// On va prendre la différence entre la qty demandée et celle qui reste en stock
					missingQuantity = quantity.substract(productionProductLocation.getQuantityInStockToInventoryUnit());
				}

				// Si on a une qty
				if (missingQuantity != null)
				{
					missingQuantity = updateQuantityToProductSupplier(inventoryProduct, missingQuantity);

					for (ProductLocation storageProductLocation : storageProductLocations)
					{
						if (storageProductLocation.getQuantityInStockToInventoryUnit().compareTo(missingQuantity) >= 0)
						{
							quantityToReloc = missingQuantity;
						}
						else
						{
							quantityToReloc = storageProductLocation.getQuantityInStockToInventoryUnit();
						}

						if (quantityToReloc.isGreaterThanZero())
						{
							// Create relocation detail master record
							RelocationDetail relocationDetail = new RelocationDetail();
							relocationDetail.setInventoryProduct(inventoryProduct);
							relocationDetail.setQuantity(quantityToReloc);

							// add relocation detail FROM
							RelocationDetailFrom relocationDetailFrom = new RelocationDetailFrom();
							relocationDetailFrom.setLocation(storageProductLocation.getLocation());
							relocationDetailFrom.setRelocationDetail(relocationDetail);
							relocationDetail.setRelocationDetailFrom(relocationDetailFrom);

							// add relocation detail TO
							RelocationDetailTo relocationDetailTo = new RelocationDetailTo();
							relocationDetailTo.setLocation(productionLocation);
							relocationDetailTo.setRelocationDetail(relocationDetail);
							relocationDetailTo.setQuantity(quantityToReloc);
							relocationDetail.addToRelocationDetailsTo(relocationDetailTo);

							// Add to return list
							relocationDetails.add(relocationDetail);
						}

						if (quantityToReloc != null)
						{
							quantity = quantity.substract(quantityToReloc);

							// A valider mais il me semble que si je n'ai plus rien à déplacer j'arrête
							if (quantity.getValue().compareTo(BigDecimal.ZERO) <= 0)
							{
								quantity = null;
								break;
							}
						}
					}
				}
			}
		}

		loader.loadAssociations(RelocationDetail.class, relocationDetails);
		return relocationDetails;
	}

	private GroupMeasureValue updateQuantityToProductSupplier(InventoryProduct inventoryProduct, GroupMeasureValue missingQuantity)
	{
		ProductSupplier productDefaultSupplier = inventoryProduct.getDefaultSupplier();
		GroupMeasureValue updatedQuantity = missingQuantity;

		if (productDefaultSupplier != null && productDefaultSupplier.getDefaultPrice() != null)
		{
			PurchaseOrderDetail purchaseOrderDetail = new PurchaseOrderDetail();
			PurchaseRequisition purchaseRequisition = new PurchaseRequisition();
			purchaseRequisition.setProduct(inventoryProduct);
			purchaseRequisition.setQuantity(missingQuantity);

			PurchaseOrderDetailFactory purchaseOrderDetailFactory = new PurchaseOrderDetailFactory();
			purchaseOrderDetailFactory.updateQuantity(purchaseOrderDetail, productDefaultSupplier.getDefaultPrice(), purchaseRequisition);

			updatedQuantity = purchaseOrderDetail.getQuantity();
		}

		return updatedQuantity;
	}

}