package com.mac.custom.services.workLoad.beans;

import static org.torpedoquery.jpa.Torpedo.*;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.time.DateUtils;
import org.joda.time.DateMidnight;
import org.torpedoquery.jpa.OnGoingLogicalCondition;
import org.torpedoquery.jpa.Query;

import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationResult;
import com.mac.custom.bo.MacOrder;
import com.mac.custom.bo.MacOrderDetail;
import com.mac.custom.bo.WorkLoad;
import com.mac.custom.bo.WorkLoadDetail;
import com.mac.custom.bo.model.WorkLoadDetailQuantityMoveModel;
import com.mac.custom.factory.WorkLoadDetailFactory;
import com.mac.custom.factory.WorkLoadFactory;
import com.mac.custom.services.order.interfaces.local.MacOrderServicesLocal;
import com.mac.custom.services.workLoad.interfaces.local.WorkLoadServicesLocal;
import com.mac.custom.services.workLoad.interfaces.remote.WorkLoadServicesRemote;
import com.netappsid.bo.model.Model;
import com.netappsid.dao.utils.boquery.BOQuery;
import com.netappsid.dao.utils.boquery.restriction.NotNullRestriction;
import com.netappsid.erp.server.bo.OrderDetail;
import com.netappsid.erp.server.bo.Resource;
import com.netappsid.erp.server.bo.WorkOrder;
import com.netappsid.erp.server.bo.WorkOrderDetail;
import com.netappsid.erp.server.bo.WorkOrderRoutingStep;
import com.netappsid.erp.server.bo.WorkShift;
import com.netappsid.erp.server.dao.ResourceEfficiencyConfigurationDAO;
import com.netappsid.erp.server.datatypes.GroupMeasureValue;
import com.netappsid.erp.server.services.interfaces.local.ExplodableItemServicesLocal;
import com.netappsid.erp.server.services.resultholders.validationmessage.LocalizableSimpleValidationMessage;
import com.netappsid.erp.utils.iterator.DateBetweenIterator;
import com.netappsid.europe.naid.custom.bo.PostponedReason;
import com.netappsid.europe.naid.custom.bo.ShippingDateHistory;
import com.netappsid.services.interfaces.local.LoaderLocal;
import com.netappsid.services.interfaces.local.PersistenceLocal;

@Stateless
public class WorkLoadServicesBean implements WorkLoadServicesLocal, WorkLoadServicesRemote
{
	// TODO localize the date format
	private static final SimpleDateFormat ERROR_DATE_FORMAT = new SimpleDateFormat("d MMM");

	@EJB
	private LoaderLocal loader;

	@EJB
	private PersistenceLocal persistenceLocal;

	@EJB
	private MacOrderServicesLocal macOrderLocal;

	@EJB
	private ExplodableItemServicesLocal explodableItemServicesLocal;

	@PersistenceContext
	private javax.persistence.EntityManager entityManager;

	private final WorkLoadFactory workLoadFactory = new WorkLoadFactory();
	private final WorkLoadDetailFactory workLoadDetailFactory = new WorkLoadDetailFactory();

	protected LoaderLocal getLoader()
	{
		return loader;
	}

	protected PersistenceLocal getPersistenceLocal()
	{
		return persistenceLocal;
	}

	@Override
	public List<WorkLoad> findByCriteria(Date dateFrom, Date dateTo, Serializable workShiftId, Serializable resourceId, Serializable macOrderId,
			Serializable macCustomerId)
	{
		List<WorkLoad> workLoads = new ArrayList<WorkLoad>();

		if (macOrderId != null || macCustomerId != null)
		{
			workLoads = findExistingWorkLoads(dateFrom, dateTo, workShiftId, resourceId, macOrderId, macCustomerId);
		}
		else if (dateFrom != null && dateTo != null)
		{
			workLoads = findAndGenerateWorkLoads(dateFrom, dateTo, workShiftId, resourceId);
		}

		return workLoads;
	}

	/**
	 * This method retrieves WorkLoads using criteria. The list is unmodifiable and ordered by date.
	 * 
	 * @param dateFrom
	 * @param dateTo
	 * @param workShiftId
	 * @param resourceId
	 * @param macOrderId
	 * @param macCustomerId
	 * 
	 * @return List<WorkShift> a list if WorkShift
	 */
	private List<WorkLoad> findExistingWorkLoads(Date dateFrom, Date dateTo, Serializable workShiftId, Serializable resourceId, Serializable macOrderId,
			Serializable macCustomerId)
	{
		WorkLoad workLoad = from(WorkLoad.class);
		List<OnGoingLogicalCondition> conditions = new ArrayList<OnGoingLogicalCondition>();

		// Date From Criteria
		if (dateFrom != null)
		{
			conditions.add(condition(workLoad.getDate()).gte(dateFrom));
		}

		// Date To Criteria
		if (dateTo != null)
		{
			conditions.add(condition(workLoad.getDate()).lte(dateTo));
		}

		// Resource Work Shift Criteria
		if (workShiftId != null)
		{
			conditions.add(condition(innerJoin(innerJoin(workLoad.getResource()).getWorkShift()).getId()).eq(workShiftId));
		}

		// Resource Criteria
		if (resourceId != null)
		{
			conditions.add(condition(innerJoin(workLoad.getResource()).getId()).eq(resourceId));
		}

		// Order Criteria
		if (macOrderId != null)
		{
			conditions.add(condition(innerJoin(innerJoin(innerJoin(workLoad.getDetails()).getOrderDetail()).getSaleTransaction()).getId()).eq(macOrderId));
		}

		// Customer Criteria
		if (macCustomerId != null)
		{
			conditions.add(condition(
					innerJoin(innerJoin(innerJoin(innerJoin(workLoad.getDetails()).getOrderDetail()).getSaleTransaction()).getCustomer()).getId()).eq(
					macCustomerId));
		}

		// Build where
		OnGoingLogicalCondition where = null;
		for (OnGoingLogicalCondition condition : conditions)
		{
			if (where == null)
			{
				where = where(condition);
			}
			else
			{
				where = where.and(condition);
			}
		}

		// order
		orderBy(workLoad.getDate());

		// executer query with parameters
		Query<WorkLoad> query = select(workLoad);
		ArrayList<String> parameterKeys = new ArrayList<String>(query.getParameters().keySet());
		ArrayList<Object> parameterValues = new ArrayList<Object>(query.getParameters().values());

		List<WorkLoad> workLoads = getLoader().findByQuery(WorkLoad.class, null, query.getQuery(), parameterKeys, parameterValues, -1);
		for (WorkLoad load : workLoads)
		{
			load.loadQuantities();
		}

		return workLoads;
	}

	private List<WorkLoad> findAndGenerateWorkLoads(Date dateFrom, Date dateTo, Serializable workShiftId, Serializable resourceId)
	{
		List<WorkLoad> existingWorkLoads = new ArrayList<WorkLoad>(findExistingWorkLoads(dateFrom, dateTo, workShiftId, resourceId, null, null));
		List<Resource> resources = findResources(workShiftId, resourceId);
		List<WorkLoad> workLoads = new ArrayList<WorkLoad>(existingWorkLoads);

		for (DateBetweenIterator dateIterator = new DateBetweenIterator(dateFrom, dateTo); dateIterator.hasNext();)
		{
			Date date = dateIterator.next();

			for (Resource resource : resources)
			{
				// look for an existing resource efficiency for that date/resource
				boolean found = false;
				Iterator<WorkLoad> iterator = existingWorkLoads.iterator();
				while (iterator.hasNext() && !found)
				{
					WorkLoad currentWorkLoad = iterator.next();
					if (DateUtils.isSameDay(currentWorkLoad.getDate(), date))
					{
						if (resource.hasSameID(currentWorkLoad.getResource()))
						{
							found = true;
							iterator.remove();
						}
					}
					else
					{
						// the workload are ordered by date
						break;
					}
				}

				if (!found)
				{
					// there is no existing workload, we create a new one
					WorkLoad created = workLoadFactory.create(date, resource);
					created.loadQuantities();
					workLoads.add(created);
				}
			}
		}

		return workLoads;
	}

	private List<Resource> findResources(Serializable workShiftId, Serializable resourceId)
	{
		BOQuery boQuery = new BOQuery(Resource.class);
		boQuery.addEqual(Resource.PROPERTYNAME_ACTIVE, true);
		boQuery.addRestriction(new NotNullRestriction(Resource.PROPERTYNAME_WORKSHIFT, null));

		if (workShiftId != null)
		{
			boQuery.addEqual(Resource.PROPERTYNAME_WORKSHIFT + "." + WorkShift.PROPERTYNAME_ID, workShiftId);
		}

		if (resourceId != null)
		{
			boQuery.addEqual(Resource.PROPERTYNAME_ID, resourceId);
		}

		return getLoader().findByBOQuery(Resource.class, ResourceEfficiencyConfigurationDAO.class, boQuery, true);
	}

	private ValidationResult validatePriorMove(List<WorkLoadDetail> workLoadDetailsToMove, WorkLoadDetailQuantityMoveModel moveModelBean)
	{
		ValidationResult result = new ValidationResult();
		List<Serializable> checkedOrders = new ArrayList<Serializable>();

		for (WorkLoadDetail workLoadDetailToMove : workLoadDetailsToMove)
		{
			MacOrderDetail macOrderDetail = workLoadDetailToMove.getOrderDetail();
			if (explodableItemServicesLocal.isProduced(macOrderDetail))
			{
				result.add(new LocalizableSimpleValidationMessage("cannotMoveWorkOrderProduced", Severity.ERROR, new Serializable[] {}));
			}
			MacOrder macOrder = (MacOrder) Model.getTarget(macOrderDetail.getSaleTransaction());

			if (!checkedOrders.contains(macOrder.getId()))
			{
				Date currentOrderShippingDate = macOrder.getShippingDate();

				if (moveModelBean.isMoveOrderShippingDate() && !moveModelBean.isAutoSelectShippingDate())
				{
					if (moveModelBean.getOrderShippingDate().before(getMinimumShippingDate(macOrder, moveModelBean, workLoadDetailsToMove)))
					{
						result.add(new LocalizableSimpleValidationMessage("cannotMoveToSpecifiedShippingDate", Severity.ERROR, new Serializable[] {
								macOrderDetail.getIdentifier(), ERROR_DATE_FORMAT.format(moveModelBean.getOrderShippingDate()) }));
					}

					if (!isValidShippingDate(macOrderDetail, moveModelBean))
					{
						result.add(new LocalizableSimpleValidationMessage("cannotMoveToShippingDateBecauseItIsNotValidForOrder", Severity.ERROR,
								new Serializable[] { macOrderDetail.getIdentifier(), ERROR_DATE_FORMAT.format(moveModelBean.getOrderShippingDate()) }));
					}
				}

				if (currentOrderShippingDate != null && moveModelBean.getDate() != null && moveModelBean.getDate().compareTo(currentOrderShippingDate) > 0
						&& !moveModelBean.isMoveOrderShippingDate())
				{
					result.add(new LocalizableSimpleValidationMessage("cannotMoveWorkLoadDetailQuantityBecauseDateExceedsShippingdate", Severity.ERROR,
							new Serializable[] { macOrderDetail.getIdentifier(), ERROR_DATE_FORMAT.format(currentOrderShippingDate) }));
				}
			}
		}

		return result;
	}

	private Date getMinimumShippingDate(MacOrder macOrder, WorkLoadDetailQuantityMoveModel moveModelBean, List<WorkLoadDetail> workLoadDetailsToMove)
	{
		Date minimumShippingDate = moveModelBean.getDate();

		if (!moveModelBean.isAutoSelectShippingDate())
		{
			minimumShippingDate = moveModelBean.getOrderShippingDate();
		}
		for (OrderDetail orderDetail : macOrder.getDetails())
		{
			MacOrderDetail macOrderDetail = (MacOrderDetail) Model.getTarget(orderDetail);

			// get the latest date from the workloaddetails of the orderdetail
			for (WorkLoadDetail workLoadDetail : macOrderDetail.getWorkLoadDetails())
			{
				if (!isWorkLoadDetailInWorkLoadDetailsToMove(workLoadDetail, workLoadDetailsToMove))
				{
					if (workLoadDetail.getWorkLoad().getDate().after(minimumShippingDate))
					{
						minimumShippingDate = workLoadDetail.getWorkLoad().getDate();
					}
				}
			}
			// if order is exploded, check the detail shipping dates
			if (macOrder.isExploded())
			{
				if (orderDetail.getShippingDate().after(minimumShippingDate))
				{
					minimumShippingDate = orderDetail.getShippingDate();
				}
			}
		}

		return minimumShippingDate;
	}

	/**
	 * Verify if a given workload detail is in the workloaddetail list to move
	 * 
	 * @param workLoadDetail
	 * @param workLoadDetailsToMove
	 * @return
	 */
	private boolean isWorkLoadDetailInWorkLoadDetailsToMove(WorkLoadDetail workLoadDetail, List<WorkLoadDetail> workLoadDetailsToMove)
	{
		for (WorkLoadDetail workLoadDetailToMove : workLoadDetailsToMove)
		{
			if (workLoadDetailToMove.hasSameID(workLoadDetail))
			{
				return true;
			}
		}
		return false;
	}

	@Override
	public ValidationResult moveWorkLoadDetailQuantity(List<Serializable> workLoadDetailIds, WorkLoadDetailQuantityMoveModel moveModelBean)
	{
		ValidationResult result = new ValidationResult();

		GroupMeasureValue quantityToMove = moveModelBean.getQuantity();
		Date moveToDate = moveModelBean.getDate();

		// list of work loads that were either already loaded or that were newly created, necessary to avoid creating a same work load twice
		List<WorkLoad> workLoadCache = new ArrayList<WorkLoad>();
		List<MacOrder> modifiedOrders = new ArrayList<MacOrder>();

		List<WorkLoadDetail> workLoadDetailsToMove = getLoader().findByIdCollection(WorkLoadDetail.class, null, workLoadDetailIds);
		result.addAll(validatePriorMove(workLoadDetailsToMove, moveModelBean).getMessages());
		if (result.hasErrors())
		{
			return result;
		}

		for (WorkLoadDetail workLoadDetailToMove : workLoadDetailsToMove)
		{
			WorkLoad oldWorkLoad = workLoadDetailToMove.getWorkLoad();

			MacOrderDetail macOrderDetail = workLoadDetailToMove.getOrderDetail();
			MacOrder macOrder = (MacOrder) Model.getTarget(macOrderDetail.getSaleTransaction());

			if (moveModelBean.isMoveWorkLoadDetail() && !DateUtils.isSameDay(moveToDate, oldWorkLoad.getDate()))
			{
				moveQuantity(oldWorkLoad, workLoadDetailToMove, quantityToMove, moveToDate, workLoadCache, macOrderDetail, result);
			}

			// get the new shipping date after move
			Date newShippingDate = getShippingDate(macOrderDetail, moveModelBean, oldWorkLoad.getResource());

			if (moveModelBean.isMoveOrderShippingDate())
			{
				if (macOrderDetail.getShippingDate() != null && newShippingDate.compareTo(macOrderDetail.getShippingDate()) != 0)
				{
					refreshOrderDetailShippingDate(macOrderDetail, macOrder, newShippingDate, result);
				}

				if (!modifiedOrders.contains(macOrder))
				{
					modifiedOrders.add(macOrder);
				}
			}
		}

		// refresh order shipping dates
		for (MacOrder macOrder : modifiedOrders)
		{
			refreshOrderShippingDate(macOrder, moveModelBean, result);
		}

		return result;
	}

	/**
	 * Checks if the new shipping date is valid for orderdetail (shippingdatemodel)
	 * 
	 * @param macOrderDetail
	 * @param moveModelBean
	 * @return
	 */
	private boolean isValidShippingDate(MacOrderDetail macOrderDetail, WorkLoadDetailQuantityMoveModel moveModelBean)
	{
		boolean validShippingDate = true;

		if (moveModelBean.isMoveOrderShippingDate() && !moveModelBean.isAutoSelectShippingDate())
		{
			Date newShippingDate = macOrderLocal.getNextValidShippingDate(macOrderDetail, moveModelBean.getOrderShippingDate());
			validShippingDate = DateUtils.isSameDay(newShippingDate, moveModelBean.getOrderShippingDate());
		}

		return validShippingDate;
	}

	/**
	 * Get the shipping date to apply in order based on WorkLoadDetailQuantityMoveModel
	 * 
	 * @param macOrderDetail
	 * @param moveModelBean
	 * @return
	 */
	private Date getShippingDate(MacOrderDetail macOrderDetail, WorkLoadDetailQuantityMoveModel moveModelBean, Resource resource)
	{
		Date shippingDate = moveModelBean.getDate();

		if (!moveModelBean.isAutoSelectShippingDate())
		{
			// set specific date
			shippingDate = moveModelBean.getOrderShippingDate();
		}
		else
		{
			// get midnight date
			DateMidnight lastWorkLoadDate = new DateMidnight(getLastWorkLoadDetailDate(macOrderDetail));

			shippingDate = macOrderLocal.getShippingDateFromLastWorkLoadDate(lastWorkLoadDate, macOrderDetail, resource,
					macOrderDetail.getManufacturingDelay(), true);
		}

		return shippingDate;
	}

	private void refreshOrderShippingDate(MacOrder macOrder, WorkLoadDetailQuantityMoveModel moveModelBean, ValidationResult result)
	{
		Date orderShippingDate = getOrderShippingDate(macOrder, moveModelBean);

		if (macOrder.getShippingDate().compareTo(orderShippingDate) != 0)
		{
			macOrder.setShippingDate(orderShippingDate);
			macOrder.setPostponedReason(moveModelBean.getPostponedReason());

			// align arrival date with shipping date
			if (macOrder.getArrivalDate().before(orderShippingDate))
			{
				macOrder.setArrivalDate(orderShippingDate);
			}

			// change datation mode
			changeOrderToSpecificDating(macOrder);

			macOrder.addToShippingDateHistories(createShippingDateHistory(macOrder.getShippingDate(), moveModelBean.getPostponedReason()));

			result.add(new LocalizableSimpleValidationMessage("workLoadMovedOrderShippingDate", Severity.WARNING, new Serializable[] { macOrder.getNumber(),
					ERROR_DATE_FORMAT.format(orderShippingDate) }));
		}
	}

	private void changeOrderToSpecificDating(MacOrder macOrder)
	{
		if (macOrder.isShipASAP() || macOrder.isShipToConfirm())
		{
			macOrder.setShipASAP(false);
			macOrder.setShipToConfirm(false);

			for (OrderDetail orderDetail : macOrder.getDetails())
			{
				if (orderDetail.isShipASAP() || orderDetail.isShipToConfirm())
				{
					orderDetail.setShipASAP(false);
					orderDetail.setShipToConfirm(false);
				}
			}
		}
	}

	/**
	 * Get the further shipping date between order details shipping date, workload details date, new shipping date
	 * 
	 * @param macOrder
	 * @param moveModelBean
	 * @return
	 */
	private Date getOrderShippingDate(MacOrder macOrder, WorkLoadDetailQuantityMoveModel moveModelBean)
	{
		Date newShippingDate = moveModelBean.getDate();

		if (!moveModelBean.isAutoSelectShippingDate())
		{
			newShippingDate = moveModelBean.getOrderShippingDate();
		}

		Date shippingDateToApply = getLastOrderDetailShippingDate(macOrder);

		if (shippingDateToApply != null && shippingDateToApply.after(newShippingDate))
		{
			newShippingDate = shippingDateToApply;
		}

		Date lastWorkLoadDetailDate = getLastWorkLoadDetailDate(macOrder);

		if (lastWorkLoadDetailDate != null && lastWorkLoadDetailDate.after(newShippingDate))
		{
			newShippingDate = lastWorkLoadDetailDate;
		}

		return newShippingDate;
	}

	private ShippingDateHistory createShippingDateHistory(Date shippingDate, PostponedReason postponedReason)
	{
		ShippingDateHistory shippingDateHistory = new ShippingDateHistory();
		shippingDateHistory.setDate(shippingDate);
		shippingDateHistory.setPostponedReason(postponedReason);

		return shippingDateHistory;
	}

	private Date getLastWorkLoadDetailDate(MacOrder macOrder)
	{
		Date lastWorkLoadDetailDate = null;

		for (OrderDetail orderDetail : macOrder.getDetails())
		{
			Date workLoadDetailDate = getLastWorkLoadDetailDate((MacOrderDetail) Model.getTarget(orderDetail));

			if (lastWorkLoadDetailDate == null || workLoadDetailDate.after(lastWorkLoadDetailDate))
			{
				lastWorkLoadDetailDate = workLoadDetailDate;
			}
		}

		return lastWorkLoadDetailDate;
	}

	private Date getLastOrderDetailShippingDate(MacOrder macOrder)
	{
		Date lastShippingDate = null;

		for (OrderDetail orderDetail : macOrder.getDetails())
		{
			if (lastShippingDate == null || orderDetail.getShippingDate().after(lastShippingDate))
			{
				lastShippingDate = orderDetail.getShippingDate();
			}
		}

		return lastShippingDate;
	}

	private void refreshOrderDetailShippingDate(MacOrderDetail macOrderDetail, MacOrder macOrder, Date newShippingDate, ValidationResult result)
	{

		if (macOrderDetail.getShippingDate().compareTo(newShippingDate) != 0)
		{
			if (!macOrder.isExploded())
			{
				macOrderDetail.setShippingDateWithoutResetingWorkLoadDetails(newShippingDate);
				result.add(new LocalizableSimpleValidationMessage("workLoadMovedOrderDetailShippingDate", Severity.WARNING, new Serializable[] {
						macOrderDetail.getIdentifier(), ERROR_DATE_FORMAT.format(newShippingDate) }));
			}
			else
			{
				result.add(new LocalizableSimpleValidationMessage("workLoadCannotMovedOrderDetailShippingDateBecauseOrderIsExploded", Severity.WARNING,
						new Serializable[] { macOrderDetail.getIdentifier(), ERROR_DATE_FORMAT.format(newShippingDate) }));
			}
		}
	}

	private Date getLastWorkLoadDetailDate(MacOrderDetail macOrderDetail)
	{
		Date lastWorkLoadDate = null;

		for (WorkLoadDetail workLoadDetail : macOrderDetail.getWorkLoadDetails())
		{
			if (workLoadDetail.getWorkLoad() != null && workLoadDetail.getWorkLoad().getDate() != null)
			{
				if (lastWorkLoadDate == null || workLoadDetail.getWorkLoad().getDate().after(lastWorkLoadDate))
				{
					lastWorkLoadDate = workLoadDetail.getWorkLoad().getDate();
				}
			}
		}

		return lastWorkLoadDate;
	}

	private void moveQuantity(WorkLoad oldWorkLoad, WorkLoadDetail workLoadDetailToMove, GroupMeasureValue quantityToMove, Date moveToDate,
			List<WorkLoad> workLoadCache, MacOrderDetail macOrderDetail, ValidationResult result)
	{
		// check for the start date before the change
		final Date oldOlderDate = getOlderDate(macOrderDetail.getWorkLoadDetails());

		GroupMeasureValue quantityMoved;
		if (quantityToMove == null || quantityToMove.getValue() == null || workLoadDetailToMove.getQuantityToProduce().compareTo(quantityToMove) <= 0)
		{
			// move the full quantity, the current workLoadDetail is deleted
			quantityMoved = new GroupMeasureValue(workLoadDetailToMove.getQuantityToProduce());
			oldWorkLoad.removeFromDetails(workLoadDetailToMove);
			macOrderDetail.removeFromWorkLoadDetails(workLoadDetailToMove);
			getPersistenceLocal().delete(workLoadDetailToMove);
		}
		else
		{
			// move a partial quantity, the current workLoadDetail is simply updated
			quantityMoved = new GroupMeasureValue(quantityToMove);
			workLoadDetailToMove.setQuantity(workLoadDetailToMove.getQuantityToProduce().substract(quantityToMove));
		}

		// determine whether a workLoadDetail already exists for that orderDetail on that date
		BOQuery boQuery = new BOQuery(WorkLoadDetail.class);
		boQuery.addEqual(WorkLoadDetail.PROPERTYNAME_ORDERDETAIL, macOrderDetail);
		boQuery.addEqual(WorkLoadDetail.PROPERTYNAME_WORKLOAD + "." + WorkLoad.PROPERTYNAME_DATE, moveToDate);
		List<WorkLoadDetail> workLoadDetails = getLoader().findByBOQuery(WorkLoadDetail.class, null, boQuery, false);

		if (workLoadDetails.isEmpty())
		{
			// the workLoadDetail for that orderDetail/date doesn't exist, we will create it
			WorkLoadDetail workLoadDetail = workLoadDetailFactory.create();
			// workLoadDetail.setOrderDetail(workLoadDetailToMove.getOrderDetail());
			macOrderDetail.addToWorkLoadDetails(workLoadDetail);
			workLoadDetail.setQuantity(quantityMoved);

			WorkLoad workLoad = findOrCreateAndSaveWorkLoad(workLoadCache, moveToDate, oldWorkLoad.getResource());
			workLoad.addToDetails(workLoadDetail);
			workLoad.invalidateQuantities();
		}
		else
		{
			// the workLoadDetail for that orderDetail/date exists, we update its quantity
			WorkLoadDetail workLoadDetail = workLoadDetails.get(0);
			workLoadDetail.setQuantity(workLoadDetail.getQuantity().add(quantityMoved));
			workLoadDetail.getWorkLoad().invalidateQuantities();
		}

		// if the start date has changed, change it
		if (!getOlderDate(macOrderDetail.getWorkLoadDetails()).equals(oldOlderDate))
		{
			alterWorkOrderToFitWorkLoad(macOrderDetail);
		}

		result.add(new LocalizableSimpleValidationMessage("moveWorkLoadDetailQuantityMoved", Severity.WARNING, new Serializable[] { quantityMoved.toString(),
				ERROR_DATE_FORMAT.format(moveToDate), macOrderDetail.getIdentifier() }));

		oldWorkLoad.invalidateQuantities();
	}

	private WorkLoad findOrCreateAndSaveWorkLoad(List<WorkLoad> workLoadCache, Date date, Resource resource)
	{
		// linear search in the work load cache since it should never grow big
		for (WorkLoad workLoad : workLoadCache)
		{
			if (workLoad.getDate().equals(date) && workLoad.getResource().hasSameID(resource))
			{
				return workLoad;
			}
		}

		BOQuery boQuery = new BOQuery(WorkLoad.class);
		boQuery.addEqual(WorkLoad.PROPERTYNAME_DATE, date);
		boQuery.addEqual(WorkLoad.PROPERTYNAME_RESOURCE, resource);
		List<WorkLoad> workLoads = getLoader().findByBOQuery(WorkLoad.class, null, boQuery, false);

		WorkLoad workLoad;
		if (workLoads.isEmpty())
		{
			// work load doesn't exist, we create and save it
			workLoad = workLoadFactory.create(date, resource);
			workLoad = getPersistenceLocal().save(workLoad, null);
		}
		else
		{
			// work load exists, we can return it
			workLoad = workLoads.get(0);
		}

		workLoadCache.add(workLoad);
		return workLoad;
	}

	private static Date getOlderDate(List<WorkLoadDetail> workLoadDetails)
	{
		Date date = null;
		if (workLoadDetails != null)
		{
			for (WorkLoadDetail wld : workLoadDetails)
			{
				if (wld.getWorkLoad() != null && wld.getWorkLoad().getDate() != null)
				{
					if (date == null || wld.getWorkLoad().getDate().before(date))
					{
						date = wld.getWorkLoad().getDate();
					}
				}
			}
		}
		return date;
	}

	private static void setWorkOrdersDate(Date date, List<WorkOrder> workOrders)
	{
		if (workOrders != null)
		{
			for (WorkOrder wo : workOrders)
			{
				wo.setStartDate(date);
				wo.setEndDate(date);

				List<WorkOrderRoutingStep> routing = wo.getRouting();
				if (routing != null)
				{
					for (WorkOrderRoutingStep wors : routing)
					{
						wors.setStartDate(date);
						wors.setEndDate(date);

						List<WorkOrderDetail> workOrderDetails = wors.getWorkOrderDetails();
						if (workOrderDetails != null)
						{
							for (WorkOrderDetail wod : workOrderDetails)
							{
								setWorkOrdersDate(date, wod.getParents());
							}
						}
					}
				}
			}
		}
	}

	@Override
	public void alterWorkOrderToFitWorkLoad(MacOrderDetail detail)
	{
		Date date = getOlderDate(detail.getWorkLoadDetails());

		if (date != null)
		{
			setWorkOrdersDate(date, detail.getWorkOrders());
		}
	}

	@Override
	public List<WorkLoadDetail> getDetails(WorkLoad workLoad)
	{
		workLoad = entityManager.merge(workLoad);
		return loader.findByField(WorkLoadDetail.class, WorkLoad.class, WorkLoadDetail.PROPERTYNAME_WORKLOAD, workLoad);
	}
}
