package com.mac.custom.services.workLoad.interfaces.remote;

import javax.ejb.Remote;

import com.mac.custom.services.workLoad.interfaces.WorkLoadServices;

@Remote
public interface WorkLoadServicesRemote extends WorkLoadServices
{

}
