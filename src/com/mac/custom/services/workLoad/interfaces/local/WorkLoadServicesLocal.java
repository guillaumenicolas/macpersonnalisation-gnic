package com.mac.custom.services.workLoad.interfaces.local;

import javax.ejb.Local;

import com.mac.custom.services.workLoad.interfaces.WorkLoadServices;

@Local
public interface WorkLoadServicesLocal extends WorkLoadServices
{

}
