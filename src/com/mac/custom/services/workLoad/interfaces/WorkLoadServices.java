package com.mac.custom.services.workLoad.interfaces;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.jgoodies.validation.ValidationResult;
import com.mac.custom.bo.MacOrderDetail;
import com.mac.custom.bo.WorkLoad;
import com.mac.custom.bo.WorkLoadDetail;
import com.mac.custom.bo.model.WorkLoadDetailQuantityMoveModel;

public interface WorkLoadServices
{
	public List<WorkLoad> findByCriteria(Date dateFrom, Date dateTo, Serializable workShiftId, Serializable resourceId, Serializable macOrderId,
			Serializable macCustomerId);

	/**
	 * Moves a certain quantity for each specified {@link WorkLoadDetail} to another date. If a matching detail is found for that other date, it will be used
	 * (quantity will be updated); otherwise a new detail is created for that date. If the full quantity is moved (<code>null</code> represents a full
	 * quantity), the old detail is deleted; otherwise its quantity is simply updated.
	 * 
	 * @param workLoadDetailIds
	 * @param WorkLoadDetailQuantityMoveModel
	 *            moveModelBean
	 * @return
	 */
	public ValidationResult moveWorkLoadDetailQuantity(List<Serializable> workLoadDetailIds, WorkLoadDetailQuantityMoveModel moveModelBean);

	/**
	 * Change the start and end dates of all the work orders and routing steps on the detail to the oldest date of the work load details.
	 * <p>
	 * This operation can be long.
	 * 
	 * @param detail
	 */
	public void alterWorkOrderToFitWorkLoad(MacOrderDetail detail);

	public List<WorkLoadDetail> getDetails(WorkLoad workLoad);
}
