package com.mac.custom.services;

import java.io.Serializable;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import com.mac.custom.bo.MacOrderDetail;
import com.mac.custom.services.customer.beans.DeclencheursEnCoursServicesBean;
import com.mac.custom.services.customer.interfaces.local.DeclencheursEnCoursServicesLocal;
import com.mac.custom.services.workLoad.beans.WorkLoadServicesBean;
import com.mac.custom.services.workLoad.interfaces.WorkLoadServices;
import com.netappsid.erp.server.bo.ExplodableDocument;
import com.netappsid.erp.server.bo.ExplodableItem;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.Loader;

public class ExplosionServicesInterceptor
{
	private final DeclencheursEnCoursServicesLocal declencheursEnCoursServicesLocal = new ServiceLocator<DeclencheursEnCoursServicesLocal>(
			DeclencheursEnCoursServicesBean.class).get();
	private final ServiceLocator<WorkLoadServices> workLoadServiceslocator = new ServiceLocator<WorkLoadServices>(WorkLoadServicesBean.class);
	private final ServiceLocator<Loader> loader = new ServiceLocator<Loader>(LoaderBean.class);
	
	@AroundInvoke
	public Object beanInterceptor(InvocationContext ctx) throws Exception
	{
		Object returnObject = ctx.proceed();

		if (ctx.getMethod().getName().equals("setNewDocumentStatus"))
		{
			ExplodableDocument explodableDocument = (ExplodableDocument) ctx.getParameters()[1];
			declencheursEnCoursServicesLocal.ajouterDeclencheurEnCours(explodableDocument.getId());
		}
		else if (ctx.getMethod().getName().equals("generateWorkOrderFromExplodableItem") && ctx.getParameters()[0] instanceof MacOrderDetail)
		{
			workLoadServiceslocator.get().alterWorkOrderToFitWorkLoad((MacOrderDetail)ctx.getParameters()[0]);
		}
		else if (ctx.getMethod().getName().equals("generateWorkOrderFromExplodableItem") && ctx.getParameters().length == 2 && ctx.getParameters()[0] instanceof Serializable && ctx.getParameters()[1] instanceof Serializable)
		{
			ExplodableItem explodableItem = loader.get().findById(ExplodableItem.class, null, (Serializable)ctx.getParameters()[0]);
			if (explodableItem instanceof MacOrderDetail)
			{
				workLoadServiceslocator.get().alterWorkOrderToFitWorkLoad((MacOrderDetail)explodableItem);
			}
		}

		return returnObject;
	}
}
