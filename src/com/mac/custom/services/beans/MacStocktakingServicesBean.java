package com.mac.custom.services.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.google.common.collect.Lists;
import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.message.SimpleValidationMessage;
import com.mac.custom.services.interfaces.local.MacStocktakingLocal;
import com.mac.custom.services.interfaces.remote.MacStocktakingRemote;
import com.netappsid.erp.client.utils.ERPServicesManager;
import com.netappsid.erp.server.bo.DocumentStatus;
import com.netappsid.erp.server.bo.Stocktaking;
import com.netappsid.erp.server.bo.StocktakingDetail;
import com.netappsid.resources.Translator;
import com.netappsid.services.interfaces.local.LoaderLocal;
import com.netappsid.services.interfaces.local.PersistenceLocal;

@Stateless
public class MacStocktakingServicesBean implements MacStocktakingLocal, MacStocktakingRemote
{
	@EJB
	private LoaderLocal loaderLocal;
	@EJB
	private PersistenceLocal persistenceLocal;

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean checkAndUpdateIncompleteStatus(Stocktaking stocktaking)
	{
		// if new and a variation is entered set status to incomplete
		if (stocktaking.getDocumentStatus().isNew())
		{
			boolean opened = false;
			for (StocktakingDetail sdetail : stocktaking.getDetails())
			{
				if (!sdetail.getMeasures().isEmpty())
				{
					opened = true;
					break;
				}
			}
			if (opened)
			{
				stocktaking.setDocumentStatus(ERPServicesManager.getLoaderServices().findFirstByField(DocumentStatus.class, DocumentStatus.class,
						DocumentStatus.PROPERTYNAME_INTERNALID, DocumentStatus.INCOMPLETE));
				return true;
			}
		}
		return false;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean checkAndUpdateCloseStatus(Serializable stocktakingId)
	{
		Stocktaking stocktaking = loaderLocal.findById(Stocktaking.class, null, stocktakingId);
		// if the status is not close but all is applied, close
		if (!stocktaking.getDocumentStatus().isClose() && !stocktaking.getDetails().isEmpty())
		{
			List<Boolean> results = loaderLocal.findByQuery("SELECT DISTINCT apply FROM StocktakingDetail sd WHERE sd.stocktaking = :stocktaking",
					Collections.<String, Object> singletonMap("stocktaking", stocktaking), false);
			// if all the details are applied
			if (results != null && results.size() == 1 && Boolean.TRUE.equals(results.get(0)))
			{
				stocktaking.setDocumentStatus(ERPServicesManager.getLoaderServices().findFirstByField(DocumentStatus.class, DocumentStatus.class,
						DocumentStatus.PROPERTYNAME_INTERNALID, DocumentStatus.CLOSE));
				persistenceLocal.save(stocktaking, false);
				return true;
			}
		}
		return false;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<ValidationMessage> validateConflicts(String languageCode, Stocktaking stocktaking)
	{
		List<StocktakingDetail> details = stocktaking.getDetails();
		if (details.isEmpty())
		{
			return Collections.emptyList();
		}

		List<ValidationMessage> messages = new ArrayList<ValidationMessage>();
		String conflictMessage = Translator.getString("StocktakingConflict", languageCode);
		// Pour eviter que la requete ait plus de 2000 parametres, split de la liste en paquet de 800 (pas plus de 999)
		List<List<StocktakingDetail>> partitionedStocktakingDetails = Lists.partition(details, 800);

		for (List<StocktakingDetail> partitionedStocktakingDetailList : partitionedStocktakingDetails)
		{
			// list all the opened stocktaking with a couple product/location from the provided stocktaking
			StringBuilder sql = new StringBuilder("SELECT DISTINCT sdesc.value FROM StocktakingDetail sd, StocktakingDescription sdesc "
					+ "WHERE sdesc.stocktaking = sd.stocktaking AND sdesc.languageCode = :langCode "
					+ "AND sd.stocktaking.documentStatus.internalId = :openDocStatus AND (");
			Map<String, Object> parameters = new HashMap<String, Object>();
			for (int cpt = 0; cpt < partitionedStocktakingDetailList.size(); ++cpt)
			{
				sql.append("(sd.inventoryProduct = :invProd").append(cpt);
				parameters.put("invProd" + cpt, partitionedStocktakingDetailList.get(cpt).getInventoryProduct());
				sql.append(" AND sd.location = :location").append(cpt).append(')');
				parameters.put("location" + cpt, partitionedStocktakingDetailList.get(cpt).getLocation());
				if (cpt < partitionedStocktakingDetailList.size() - 1)
				{
					sql.append(" OR ");
				}
			}
			sql.append(')');
			if (stocktaking.getId() != null)
			{
				sql.append(" AND sd.stocktaking.id <> :stocktakingId");
				parameters.put("stocktakingId", stocktaking.getId());
			}
			parameters.put("openDocStatus", DocumentStatus.INCOMPLETE);
			parameters.put("langCode", languageCode);

			List<String> results = loaderLocal.findByQuery(sql.toString(), parameters, false);

			if (results != null)
			{
				for (String stocktakingName : results)
				{
					messages.add(new SimpleValidationMessage(String.format(conflictMessage, stocktakingName), Severity.ERROR));
				}
			}
		}
		return messages;
	}
}
