package com.mac.custom.services.beans;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import com.mac.custom.bo.MacSetup;
import com.mac.custom.services.interfaces.local.AutomaticStockMissingResolutionServicesLocal;
import com.mac.custom.services.interfaces.remote.AutomaticStockMissingResolutionServicesRemote;
import com.netappsid.bo.model.Model;
import com.netappsid.erp.server.bo.InventoryAdjustmentType;
import com.netappsid.erp.server.bo.StockMissing;
import com.netappsid.erp.server.bo.User;
import com.netappsid.erp.server.enums.ResolveStockMissingEnum;
import com.netappsid.erp.server.services.interfaces.local.EMailServiceLocal;
import com.netappsid.erp.server.services.interfaces.local.StockMissingServicesLocal;
import com.netappsid.erp.server.services.resultholders.ResultHolder;
import com.netappsid.erp.server.services.utils.SetupUtils;
import com.netappsid.resources.Translator;
import com.netappsid.services.interfaces.local.LoaderLocal;

@Stateless
public class AutomaticStockMissingResolutionServicesBean implements AutomaticStockMissingResolutionServicesLocal, AutomaticStockMissingResolutionServicesRemote
{
	private static Logger logger = Logger.getLogger(AutomaticStockMissingResolutionServicesBean.class);
	private static String separator = "\t";

	@EJB
	private LoaderLocal loader;

	@EJB
	private StockMissingServicesLocal stockMissingServicesLocal;

	@EJB
	private EMailServiceLocal emailServiceLocal;

	@Override
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public void resolveStocksMissing()
	{
		MacSetup setup = (MacSetup) Model.getTarget(SetupUtils.INSTANCE.getMainSetup());

		InventoryAdjustmentType inventoryAdjustmentType = setup.getProductionInventoryAdjustmentType();
		User user = setup.getAutomaticServiceUser();
		Long numberToProcess = setup.getStockMissingServiceAutoAmountToResolve();

		if (user != null && inventoryAdjustmentType != null && numberToProcess != null)
		{
			try
			{
				List<Serializable> stockMissingIds = getUnresolvedStocksMissing(numberToProcess.intValue());
				List<String> warningMessages = new ArrayList<String>();
				String warningMessage = null;
				int resolutionWithWarnings = 0;
				ResultHolder result;

				for (Serializable id : stockMissingIds)
				{
					result = stockMissingServicesLocal.resolveStockMissing(id, ResolveStockMissingEnum.DEFINED_LOCATION, null, inventoryAdjustmentType,
							user.getId());

					if (result != null && result.hasWarnings())
					{

						StockMissing stockMissing = loader.findById(StockMissing.class, StockMissing.class, id);
						resolutionWithWarnings++;

						if (stockMissing != null && stockMissing.getInventoryProduct() != null && stockMissing.getLocation() != null)
						{
							for (ValidationMessage warning : (List<ValidationMessage>) result.getWarnings())
							{
								// Modif GNIC 03/09/2014 : modif du contenu et du formatage du message warning
								// Code article , Désignation Article ;Localisation ;Quantité en Stock, Sommes des quantités non résolues, Unité de Stock,
								// Message d’erreur
								// warningMessage = String.format(Translator.getString(warning.formattedText()), "(" + Translator.getString("product") + " : "
								// + stockMissing.getInventoryProduct().getCode() + ", " + Translator.getString("location") + " : "
								// + stockMissing.getLocation().getCode() + ")");

								warningMessage = stockMissing.getInventoryProduct().getCode()
										+ separator
										+ stockMissing.getInventoryProduct().getDescription("fr")
										+ separator
										+ stockMissing.getLocation().getCode()
										+ separator
										+ stockMissing.getInventoryProduct().getProductWarehouse(stockMissing.getLocation().getWarehouse())
												.getQuantityInStockToInventoryUnit().getValue().floatValue()
										+ separator
										+ stockMissing.getInventoryProduct().getProductWarehouse(stockMissing.getLocation().getWarehouse())
												.getQuantityInStockMissingToInventoryUnit().getValue().floatValue()
										+ separator
										+ stockMissing.getInventoryProduct().getProductWarehouse(stockMissing.getLocation().getWarehouse())
												.getQuantityInStockToInventoryUnit().getConvertedUnit().getDescription("fr")
										+ separator
										+ String.format(Translator.getString(warning.formattedText()), "(" + Translator.getString("product") + " : "
												+ stockMissing.getInventoryProduct().getCode() + ", " + Translator.getString("location") + " : "
												+ stockMissing.getLocation().getCode() + ")");
								if (!warningMessages.contains(warningMessage))
								{
									warningMessages.add(warningMessage);
								}
							}
						}
					}
				}
				if (setup.getStockMissingServiceAutoSendEmail())
				{
					sendMail(warningMessages, resolutionWithWarnings);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	private List<Serializable> getUnresolvedStocksMissing(int amountToRetrieve)
	{
		List<Serializable> stocksMissingIds = new ArrayList<Serializable>();
		final StringBuilder hqlQuery = new StringBuilder();
		String stockMissingClass = StockMissing.class.getSimpleName();

		hqlQuery.append("SELECT " + stockMissingClass + "." + StockMissing.PROPERTYNAME_ID).append("\n");
		hqlQuery.append("FROM " + stockMissingClass + " stockMissing").append("\n");
		hqlQuery.append("WHERE stockMissing." + StockMissing.PROPERTYNAME_RESOLVED + " = false").append("\n");

		stocksMissingIds = loader
				.findByQuery(Serializable.class, null, hqlQuery.toString(), new ArrayList<String>(), new ArrayList<Object>(), amountToRetrieve);

		return stocksMissingIds;
	}

	private void sendMail(List<String> warningMessages, int resolutionWithWarnings)
	{
		MacSetup setup = (MacSetup) Model.getTarget(SetupUtils.INSTANCE.getMainSetup());
		String emailSendFrom = setup.getStockMissingServiceAutoEmailFrom();
		String recipients = setup.getStockMissingServiceAutoEmailTo();
		int numberOfStockMissingProcesses = setup.getStockMissingServiceAutoAmountToResolve().intValue();

		if (emailSendFrom != null && !emailSendFrom.isEmpty() && recipients != null && !recipients.isEmpty())
		{
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			String title = "R\u00E9solution des stocks manquants - " + df.format(new Date());

			// message
			String message = "Bonjour,\n\nLe processus de r\u00E9solution des stocks manquants en automatique a rencontr\u00E9 des erreurs.\n";
			// Modif GNIC 03/09/2014 : suppression de la phrase ci-dessous
			// message += "Sur les " + numberOfStockMissingProcesses + " stocks manquants trait\u00E9s, " + resolutionWithWarnings
			// + " n'ont pas \u00E9t\u00E9 r\u00E9solu avec succ\u00e8s.\n\n";
			message += "Voici les messages retourn\u00E9s (uniques) : \n\n";

			message += "Code article" + separator + "D\u00E9signation Article" + separator + "Localisation" + separator + "Quantit\u00E9 en Stock" + separator
					+ "Sommes des quantit\u00E9s non r\u00E9solues" + separator + "Unit\u00E9 de Stock" + separator + "Message d'erreur" + "\n";
			for (String warningMessage : warningMessages)
			{
				message += warningMessage + "\n";
			}

			message += "\nCordialement\n";

			ValidationResult sendEmailResult = emailServiceLocal.sendAndValidate(emailSendFrom, Arrays.asList(recipients.split(";")), title, message, title,
					null);
			if (sendEmailResult.hasErrors())
			{
				logger.error(sendEmailResult.getMessagesText());
			}
		}
	}
}
