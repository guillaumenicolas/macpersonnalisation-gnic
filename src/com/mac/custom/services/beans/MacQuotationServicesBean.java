package com.mac.custom.services.beans;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.mac.custom.bo.MacQuotation;
import com.mac.custom.services.interfaces.local.MacQuotationServicesLocal;
import com.mac.custom.services.interfaces.remote.MacQuotationServicesRemote;
import com.netappsid.services.interfaces.local.LoaderLocal;

@Stateless
public class MacQuotationServicesBean implements MacQuotationServicesLocal, MacQuotationServicesRemote
{
	@EJB
	private LoaderLocal loader;

	@Override
	public void setSentToCommercial(List<String> quotationNumbers, boolean sent)
	{
		if (quotationNumbers != null && !quotationNumbers.isEmpty())
		{
			for (String quotationNumber : quotationNumbers)
			{
				MacQuotation macQuotation = loader.findFirstByField(MacQuotation.class, null, MacQuotation.PROPERTYNAME_NUMBER, quotationNumber);
				if (macQuotation != null)
				{
					macQuotation.setSentToCommercial(sent);
				}
			}
		}
	}
}
