
package com.mac.custom.services.custledgerentry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustLedgerEntry_List" type="{urn:microsoft-dynamics-schemas/page/custledgerentry}CustLedgerEntry_List"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "custLedgerEntryList"
})
@XmlRootElement(name = "UpdateMultiple_Result")
public class UpdateMultipleResult {

    @XmlElement(name = "CustLedgerEntry_List", required = true)
    protected CustLedgerEntryList custLedgerEntryList;

    /**
     * Gets the value of the custLedgerEntryList property.
     * 
     * @return
     *     possible object is
     *     {@link CustLedgerEntryList }
     *     
     */
    public CustLedgerEntryList getCustLedgerEntryList() {
        return custLedgerEntryList;
    }

    /**
     * Sets the value of the custLedgerEntryList property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustLedgerEntryList }
     *     
     */
    public void setCustLedgerEntryList(CustLedgerEntryList value) {
        this.custLedgerEntryList = value;
    }

}
