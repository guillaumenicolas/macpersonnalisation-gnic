
package com.mac.custom.services.custledgerentry;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustLedgerEntry_Fields.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CustLedgerEntry_Fields">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Posting_Date"/>
 *     &lt;enumeration value="Document_Type"/>
 *     &lt;enumeration value="Document_No"/>
 *     &lt;enumeration value="Customer_No"/>
 *     &lt;enumeration value="Description"/>
 *     &lt;enumeration value="Global_Dimension_1_Code"/>
 *     &lt;enumeration value="Global_Dimension_2_Code"/>
 *     &lt;enumeration value="IC_Partner_Code"/>
 *     &lt;enumeration value="Salesperson_Code"/>
 *     &lt;enumeration value="Currency_Code"/>
 *     &lt;enumeration value="Original_Amount"/>
 *     &lt;enumeration value="Original_Amt_LCY"/>
 *     &lt;enumeration value="Amount"/>
 *     &lt;enumeration value="Amount_LCY"/>
 *     &lt;enumeration value="Remaining_Amount"/>
 *     &lt;enumeration value="Remaining_Amt_LCY"/>
 *     &lt;enumeration value="Bal_Account_Type"/>
 *     &lt;enumeration value="Bal_Account_No"/>
 *     &lt;enumeration value="Due_Date"/>
 *     &lt;enumeration value="Pmt_Discount_Date"/>
 *     &lt;enumeration value="Pmt_Disc_Tolerance_Date"/>
 *     &lt;enumeration value="Original_Pmt_Disc_Possible"/>
 *     &lt;enumeration value="Remaining_Pmt_Disc_Possible"/>
 *     &lt;enumeration value="Max_Payment_Tolerance"/>
 *     &lt;enumeration value="Open"/>
 *     &lt;enumeration value="On_Hold"/>
 *     &lt;enumeration value="User_ID"/>
 *     &lt;enumeration value="Source_Code"/>
 *     &lt;enumeration value="Reason_Code"/>
 *     &lt;enumeration value="Reversed"/>
 *     &lt;enumeration value="Reversed_by_Entry_No"/>
 *     &lt;enumeration value="Reversed_Entry_No"/>
 *     &lt;enumeration value="Entry_No"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CustLedgerEntry_Fields")
@XmlEnum
public enum CustLedgerEntryFields {

    @XmlEnumValue("Posting_Date")
    POSTING_DATE("Posting_Date"),
    @XmlEnumValue("Document_Type")
    DOCUMENT_TYPE("Document_Type"),
    @XmlEnumValue("Document_No")
    DOCUMENT_NO("Document_No"),
    @XmlEnumValue("Customer_No")
    CUSTOMER_NO("Customer_No"),
    @XmlEnumValue("Description")
    DESCRIPTION("Description"),
    @XmlEnumValue("Global_Dimension_1_Code")
    GLOBAL_DIMENSION_1_CODE("Global_Dimension_1_Code"),
    @XmlEnumValue("Global_Dimension_2_Code")
    GLOBAL_DIMENSION_2_CODE("Global_Dimension_2_Code"),
    @XmlEnumValue("IC_Partner_Code")
    IC_PARTNER_CODE("IC_Partner_Code"),
    @XmlEnumValue("Salesperson_Code")
    SALESPERSON_CODE("Salesperson_Code"),
    @XmlEnumValue("Currency_Code")
    CURRENCY_CODE("Currency_Code"),
    @XmlEnumValue("Original_Amount")
    ORIGINAL_AMOUNT("Original_Amount"),
    @XmlEnumValue("Original_Amt_LCY")
    ORIGINAL_AMT_LCY("Original_Amt_LCY"),
    @XmlEnumValue("Amount")
    AMOUNT("Amount"),
    @XmlEnumValue("Amount_LCY")
    AMOUNT_LCY("Amount_LCY"),
    @XmlEnumValue("Remaining_Amount")
    REMAINING_AMOUNT("Remaining_Amount"),
    @XmlEnumValue("Remaining_Amt_LCY")
    REMAINING_AMT_LCY("Remaining_Amt_LCY"),
    @XmlEnumValue("Bal_Account_Type")
    BAL_ACCOUNT_TYPE("Bal_Account_Type"),
    @XmlEnumValue("Bal_Account_No")
    BAL_ACCOUNT_NO("Bal_Account_No"),
    @XmlEnumValue("Due_Date")
    DUE_DATE("Due_Date"),
    @XmlEnumValue("Pmt_Discount_Date")
    PMT_DISCOUNT_DATE("Pmt_Discount_Date"),
    @XmlEnumValue("Pmt_Disc_Tolerance_Date")
    PMT_DISC_TOLERANCE_DATE("Pmt_Disc_Tolerance_Date"),
    @XmlEnumValue("Original_Pmt_Disc_Possible")
    ORIGINAL_PMT_DISC_POSSIBLE("Original_Pmt_Disc_Possible"),
    @XmlEnumValue("Remaining_Pmt_Disc_Possible")
    REMAINING_PMT_DISC_POSSIBLE("Remaining_Pmt_Disc_Possible"),
    @XmlEnumValue("Max_Payment_Tolerance")
    MAX_PAYMENT_TOLERANCE("Max_Payment_Tolerance"),
    @XmlEnumValue("Open")
    OPEN("Open"),
    @XmlEnumValue("On_Hold")
    ON_HOLD("On_Hold"),
    @XmlEnumValue("User_ID")
    USER_ID("User_ID"),
    @XmlEnumValue("Source_Code")
    SOURCE_CODE("Source_Code"),
    @XmlEnumValue("Reason_Code")
    REASON_CODE("Reason_Code"),
    @XmlEnumValue("Reversed")
    REVERSED("Reversed"),
    @XmlEnumValue("Reversed_by_Entry_No")
    REVERSED_BY_ENTRY_NO("Reversed_by_Entry_No"),
    @XmlEnumValue("Reversed_Entry_No")
    REVERSED_ENTRY_NO("Reversed_Entry_No"),
    @XmlEnumValue("Entry_No")
    ENTRY_NO("Entry_No");
    private final String value;

    CustLedgerEntryFields(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CustLedgerEntryFields fromValue(String v) {
        for (CustLedgerEntryFields c: CustLedgerEntryFields.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
