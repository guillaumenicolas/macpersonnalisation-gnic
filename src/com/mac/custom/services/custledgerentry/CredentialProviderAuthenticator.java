package com.mac.custom.services.custledgerentry;

import java.net.Authenticator;
import java.net.PasswordAuthentication;

public class CredentialProviderAuthenticator extends Authenticator
{
	@Override
	public PasswordAuthentication getPasswordAuthentication()
	{
		return (new PasswordAuthentication("mac-groupe\\webnav", new char[] { 'w', 'e', 'b', 'n', 'a', 'v', '_', '3', '6', '0' }));
	}
}