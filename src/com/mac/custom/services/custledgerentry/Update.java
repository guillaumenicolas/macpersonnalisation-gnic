
package com.mac.custom.services.custledgerentry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustLedgerEntry" type="{urn:microsoft-dynamics-schemas/page/custledgerentry}CustLedgerEntry"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "custLedgerEntry"
})
@XmlRootElement(name = "Update")
public class Update {

    @XmlElement(name = "CustLedgerEntry", required = true)
    protected CustLedgerEntry custLedgerEntry;

    /**
     * Gets the value of the custLedgerEntry property.
     * 
     * @return
     *     possible object is
     *     {@link CustLedgerEntry }
     *     
     */
    public CustLedgerEntry getCustLedgerEntry() {
        return custLedgerEntry;
    }

    /**
     * Sets the value of the custLedgerEntry property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustLedgerEntry }
     *     
     */
    public void setCustLedgerEntry(CustLedgerEntry value) {
        this.custLedgerEntry = value;
    }

}
