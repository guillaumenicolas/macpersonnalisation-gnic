
package com.mac.custom.services.custledgerentry;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Document_Type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Document_Type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="_blank_"/>
 *     &lt;enumeration value="Payment"/>
 *     &lt;enumeration value="Invoice"/>
 *     &lt;enumeration value="Credit_Memo"/>
 *     &lt;enumeration value="Finance_Charge_Memo"/>
 *     &lt;enumeration value="Reminder"/>
 *     &lt;enumeration value="Refund"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Document_Type")
@XmlEnum
public enum DocumentType {

    @XmlEnumValue("_blank_")
    BLANK("_blank_"),
    @XmlEnumValue("Payment")
    PAYMENT("Payment"),
    @XmlEnumValue("Invoice")
    INVOICE("Invoice"),
    @XmlEnumValue("Credit_Memo")
    CREDIT_MEMO("Credit_Memo"),
    @XmlEnumValue("Finance_Charge_Memo")
    FINANCE_CHARGE_MEMO("Finance_Charge_Memo"),
    @XmlEnumValue("Reminder")
    REMINDER("Reminder"),
    @XmlEnumValue("Refund")
    REFUND("Refund");
    private final String value;

    DocumentType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DocumentType fromValue(String v) {
        for (DocumentType c: DocumentType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
