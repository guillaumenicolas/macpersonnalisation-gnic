
package com.mac.custom.services.custledgerentry;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustLedgerEntry_List complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustLedgerEntry_List">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustLedgerEntry" type="{urn:microsoft-dynamics-schemas/page/custledgerentry}CustLedgerEntry" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustLedgerEntry_List", propOrder = {
    "custLedgerEntry"
})
public class CustLedgerEntryList {

    @XmlElement(name = "CustLedgerEntry", required = true)
    protected List<CustLedgerEntry> custLedgerEntry;

    /**
     * Gets the value of the custLedgerEntry property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the custLedgerEntry property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustLedgerEntry().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustLedgerEntry }
     * 
     * 
     */
    public List<CustLedgerEntry> getCustLedgerEntry() {
        if (custLedgerEntry == null) {
            custLedgerEntry = new ArrayList<CustLedgerEntry>();
        }
        return this.custLedgerEntry;
    }

}
