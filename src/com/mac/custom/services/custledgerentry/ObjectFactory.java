
package com.mac.custom.services.custledgerentry;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.mac.custom.services.custledgerentry package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.mac.custom.services.custledgerentry
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CustLedgerEntry }
     * 
     */
    public CustLedgerEntry createCustLedgerEntry() {
        return new CustLedgerEntry();
    }

    /**
     * Create an instance of {@link GetRecIdFromKey }
     * 
     */
    public GetRecIdFromKey createGetRecIdFromKey() {
        return new GetRecIdFromKey();
    }

    /**
     * Create an instance of {@link Update }
     * 
     */
    public Update createUpdate() {
        return new Update();
    }

    /**
     * Create an instance of {@link CustLedgerEntryFilter }
     * 
     */
    public CustLedgerEntryFilter createCustLedgerEntryFilter() {
        return new CustLedgerEntryFilter();
    }

    /**
     * Create an instance of {@link ReadMultipleResult }
     * 
     */
    public ReadMultipleResult createReadMultipleResult() {
        return new ReadMultipleResult();
    }

    /**
     * Create an instance of {@link UpdateResult }
     * 
     */
    public UpdateResult createUpdateResult() {
        return new UpdateResult();
    }

    /**
     * Create an instance of {@link GetRecIdFromKeyResult }
     * 
     */
    public GetRecIdFromKeyResult createGetRecIdFromKeyResult() {
        return new GetRecIdFromKeyResult();
    }

    /**
     * Create an instance of {@link ReadByRecIdResult }
     * 
     */
    public ReadByRecIdResult createReadByRecIdResult() {
        return new ReadByRecIdResult();
    }

    /**
     * Create an instance of {@link ReadMultiple }
     * 
     */
    public ReadMultiple createReadMultiple() {
        return new ReadMultiple();
    }

    /**
     * Create an instance of {@link ReadByRecId }
     * 
     */
    public ReadByRecId createReadByRecId() {
        return new ReadByRecId();
    }

    /**
     * Create an instance of {@link UpdateMultiple }
     * 
     */
    public UpdateMultiple createUpdateMultiple() {
        return new UpdateMultiple();
    }

    /**
     * Create an instance of {@link Read }
     * 
     */
    public Read createRead() {
        return new Read();
    }

    /**
     * Create an instance of {@link CustLedgerEntryList }
     * 
     */
    public CustLedgerEntryList createCustLedgerEntryList() {
        return new CustLedgerEntryList();
    }

    /**
     * Create an instance of {@link ReadResult }
     * 
     */
    public ReadResult createReadResult() {
        return new ReadResult();
    }

    /**
     * Create an instance of {@link UpdateMultipleResult }
     * 
     */
    public UpdateMultipleResult createUpdateMultipleResult() {
        return new UpdateMultipleResult();
    }

    /**
     * Create an instance of {@link IsUpdated }
     * 
     */
    public IsUpdated createIsUpdated() {
        return new IsUpdated();
    }

    /**
     * Create an instance of {@link IsUpdatedResult }
     * 
     */
    public IsUpdatedResult createIsUpdatedResult() {
        return new IsUpdatedResult();
    }

}
