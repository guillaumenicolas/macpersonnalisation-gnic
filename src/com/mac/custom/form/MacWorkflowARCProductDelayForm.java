package com.mac.custom.form;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.SwingUtilities;

import com.mac.custom.bo.MacWorkflowARCProductDelay;
import com.netappsid.component.ForeignKeyPanel;
import com.netappsid.component.swing.NAIDComponentImpl;
import com.netappsid.erp.server.bo.Property;
import com.netappsid.framework.gui.formbuilder.AbstractForm;

public class MacWorkflowARCProductDelayForm extends AbstractForm
{
	private final PropertyChangeListener propertyChangeHandler = new PropertyChangeListener()
		{
			@Override
			public void propertyChange(PropertyChangeEvent evt)
			{
				changeProperty((Property) evt.getNewValue());
			}
		};
	private NAIDComponentImpl editProperty;

	@Override
	public void buildPanel()
	{
		super.buildPanel();

		editProperty = getComponent("editProperty");
	}

	private void changeProperty(Property property)
	{
		if (property == null)
		{
			/*
			 * we need to postpone the operation after the transaction for the Property field change in the UndoRedoManager is done or the UndoRedoManager will
			 * ignore our modification and the change won't be revertable
			 */
			SwingUtilities.invokeLater(new Runnable()
				{
					@Override
					public void run()
					{
						getModel(getModelName()).setValue(MacWorkflowARCProductDelay.PROPERTYNAME_PROPERTYVALUE, null);
					}
				});

			editProperty.setEditableWithoutAlteringButtons(true);
			editProperty.setActionsEnabled(true, ForeignKeyPanel.RESET_ACTION_NAME);
		}
		else
		{
			editProperty.setEditableWithoutAlteringButtons(false);
			editProperty.setActionsEnabled(false, ForeignKeyPanel.RESET_ACTION_NAME);
		}
	}

	@Override
	protected void afterBeanChanged()
	{
		super.afterBeanChanged();

		addChangeHandler();

		changeProperty(((MacWorkflowARCProductDelay) getCurrentBean()).getProperty());
	}

	@Override
	protected void beforeBeanChanged()
	{
		super.beforeBeanChanged();

		removeChangeHandler();
	}

	@Override
	public void dispose()
	{
		if (!isDisposed())
		{
			removeChangeHandler();
		}

		super.dispose();
	}

	private void addChangeHandler()
	{
		((MacWorkflowARCProductDelay) getCurrentBean()).addPropertyChangeListener(MacWorkflowARCProductDelay.PROPERTYNAME_PROPERTY, propertyChangeHandler);
	}

	private void removeChangeHandler()
	{
		((MacWorkflowARCProductDelay) getCurrentBean()).removePropertyChangeListener(MacWorkflowARCProductDelay.PROPERTYNAME_PROPERTY, propertyChangeHandler);
	}
}
