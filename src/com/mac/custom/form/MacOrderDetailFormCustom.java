package com.mac.custom.form;

import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JRadioButton;

import org.apache.commons.lang.StringUtils;

import resources.forms.classes.OrderForm;

import com.mac.custom.bo.MacOrder;
import com.mac.custom.bo.MacOrderDetail;
import com.mac.custom.utils.RareBirdUtils;
import com.netappsid.erp.client.utils.ERPSystemVariable;
import com.netappsid.erp.server.bo.User;
import com.netappsid.europe.naid.custom.form.OrderDetailEuroForm;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.framework.utils.SystemVariable;
import com.netappsid.security.auth.server.SecurityUser;
import com.netappsid.security.permission.client.SecurityPermissionManager;
import com.netappsid.security.permission.factory.SecurityPermissionManagerFactory;
import com.netappsid.security.permission.server.PermissionType;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.Loader;
import com.netappsid.utils.ComponentUtils;

public class MacOrderDetailFormCustom extends OrderDetailEuroForm
{
	public static final String SECURITYGROUP_WF_BE = "WF_BE";
	public static final String SECURITYGROUP_WF_ADMIN = "WF_ADMIN";
	public static final String SECURITYGROUP_ADMINISTRATORS = "ADMINISTRATORS";
	public static final String ORDERDETAIL = "orderDetail";

	private static final String M5PLABEL_ACCEPTE = "M5P Accept\u00e9 !";
	private static final String M5PLABEL_REJETE = "M5P Rejet\u00e9 !";
	private static final String M5PLABEL_NI_ACCEPTE_NI_REJETE = "M5P ni Accept\u00e9, ni Rejet\u00e9 !";

	private Component shippingDate;
	private JRadioButton optDateSpecific;
	private JRadioButton optDateASAP;
	private JRadioButton optDateToDetermine;
	private Component rareBirdAccepted;
	private Component rareBirdConfirmationNumber;
	private Component rareBirdDescription;
	private Component rareBirdDesignation;
	private Component rareBirdProcessStartDate;
	private Component rareBirdProcessedDate;
	private Component price;
	private Component modifyAddress;
	private Component searchAddress;
	private Component addAddress;

	private JLabel m5pAcceptedRejectedLabel;

	private final ServiceLocator<Loader> loaderLocator = new ServiceLocator<Loader>(LoaderBean.class);
	private final User user = SystemVariable.getVariable(ERPSystemVariable.USER);
	private final SecurityUser securityUser = loaderLocator.get().findById(SecurityUser.class, SecurityUser.class, user.getSecurityUser().getId());

	private final PropertyChangeListener rareBirdChangeHandler = new RareBirdChangeHandler();
	private final PropertyChangeListener rareBirdAcceptedChangeHandler = new RareBirdAcceptedChangeHandler();

	@Override
	public void buildPanel()
	{
		super.buildPanel();

		price = getComponent("price");
		shippingDate = getComponent("shippingDate");
		optDateSpecific = getComponent("optDateSpecific");
		optDateASAP = getComponent("optDateASAP");
		optDateToDetermine = getComponent("optDateToDetermine");

		applyShippingDatesSecurity();

		rareBirdAccepted = getComponent("rareBirdAccepted");
		rareBirdConfirmationNumber = getComponent("rareBirdConfirmationNumber");
		rareBirdDescription = getComponent("rareBirdDescription");
		rareBirdDesignation = getComponent("rareBirdDesignation");
		rareBirdProcessStartDate = getComponent("rareBirdProcessStartDate");
		rareBirdProcessedDate = getComponent("rareBirdProcessedDate");
		m5pAcceptedRejectedLabel = getComponent("m5pAcceptedRejectedLabel");
		modifyAddress = getComponent("modifyAddress");
		searchAddress = getComponent("searchAddress");
		addAddress = getComponent("addAddress");

		updateactionaddressshipping();
		updateWorkLoadActionsEnabledState();
		updateTauxDeRemisesEnabledState();
	}

	/**
	 * Modification OGA le 01/10/14 Ne pas donner la possibilit� de modifier une adresse de livraison dans la modification d un element si groupe user <>
	 * ADMINISTRATOR
	 */
	private void updateactionaddressshipping()
	{
		boolean isUserAdministrator = securityUser.isMember(SECURITYGROUP_ADMINISTRATORS);

		if (modifyAddress != null)
		{
			modifyAddress.setEnabled(isUserAdministrator);
		}

		if (searchAddress != null)
		{
			searchAddress.setEnabled(isUserAdministrator);
		}

		if (addAddress != null)
		{
			addAddress.setEnabled(isUserAdministrator);
		}
	}

	public static boolean isRareBirdModifiable(SecurityUser securityUser)
	{
		if (securityUser == null)
		{
			return true;
		}
		else
		{
			return securityUser.isMember(SECURITYGROUP_WF_BE) || securityUser.isMember(SECURITYGROUP_WF_ADMIN)
					|| securityUser.isMember(SECURITYGROUP_ADMINISTRATORS);
		}
	}

	public static boolean isTauxRemiseModifiable(SecurityUser securityUser)
	{
		if (securityUser == null)
		{
			return true;
		}
		else
		{
			return securityUser.isMember(SECURITYGROUP_ADMINISTRATORS);
		}
	}

	@Override
	protected void afterBeanChanged()
	{
		super.afterBeanChanged();

		disableDateControlsIfOrderIsExploded();

		MacOrderDetail orderDetail = getModelBean(ORDERDETAIL);

		if (orderDetail.isRareBird() && isRareBirdModifiable(securityUser))
		{
			enableRareBirdFields(true);
		}
		if (orderDetail.getRareBirdAccepted() == null)
		{
			m5pAcceptedRejectedLabel.setText(M5PLABEL_NI_ACCEPTE_NI_REJETE);
			m5pAcceptedRejectedLabel.setForeground(BLACK);
		}
		else if (orderDetail.getRareBirdAccepted() == true)
		{
			m5pAcceptedRejectedLabel.setText(M5PLABEL_ACCEPTE);
			m5pAcceptedRejectedLabel.setForeground(BLUE);
		}
		else if (orderDetail.getRareBirdAccepted() == false)
		{
			m5pAcceptedRejectedLabel.setText(M5PLABEL_REJETE);
			m5pAcceptedRejectedLabel.setForeground(RED);
		}

		updateWorkLoadActionsEnabledState();
	}

	private void updateWorkLoadActionsEnabledState()
	{
		MacOrderDetail orderDetail = getCurrentBean();
		if (orderDetail != null)
		{
			boolean workLoadModifiable = MacOrderFormCustom.isWorkLoadModifiable(orderDetail.getMacOrder());

			if (!workLoadModifiable)
			{
				setEnabled("productRevision", workLoadModifiable);
				setEnabled("quantity", workLoadModifiable);
				setEnabled("priceOverride", workLoadModifiable);
				setEnabled("priceMeasure", workLoadModifiable);
				setEnabled("discountRate", workLoadModifiable);
				setEnabled("applyDiscount", workLoadModifiable);
				setEnabled("date", workLoadModifiable);
				setEnabled("rareBird", workLoadModifiable);
				setEnabled("feePanel", workLoadModifiable);
			}
		}
	}

	private void updateTauxDeRemisesEnabledState()
	{
		MacOrderDetail orderDetail = getCurrentBean();
		if (orderDetail != null)
		{
			boolean state = isTauxRemiseModifiable(securityUser);
			setEnabled("discountRate", state);
		}
	}

	private void setEnabled(String componentName, boolean enabled)
	{
		Component component = getComponent(componentName);
		if (component != null)
		{
			ComponentUtils.setComponentHierarchyEnabled(component, enabled);
		}
	}

	/**
	 * Overridden to make sure the price is not editable when work loads aren't, even if the price has been overridden.
	 */
	@Override
	public void setPriceOverrided()
	{
		super.setPriceOverrided();

		if (price != null)
		{
			MacOrderDetail orderDetail = getCurrentBean();
			if (orderDetail != null && !MacOrderFormCustom.isWorkLoadModifiable(orderDetail.getMacOrder()))
			{
				ComponentUtils.setComponentHierarchyEnabled(price, false);
			}
		}
	}

	@Override
	public void initializeOrderDetailListeners()
	{
		super.initializeOrderDetailListeners();
		getModel(getModelName()).getModel(MacOrderDetail.PROPERTYNAME_RAREBIRD).addValueChangeListener(rareBirdChangeHandler);
		getModel(getModelName()).getModel(MacOrderDetail.PROPERTYNAME_RAREBIRDACCEPTED).addValueChangeListener(rareBirdAcceptedChangeHandler);
	}

	@Override
	public void removeOrderDetailListeners()
	{
		super.removeOrderDetailListeners();
		getModel(getModelName()).getModel(MacOrderDetail.PROPERTYNAME_RAREBIRD).removeValueChangeListener(rareBirdChangeHandler);
		getModel(getModelName()).getModel(MacOrderDetail.PROPERTYNAME_RAREBIRDACCEPTED).removeValueChangeListener(rareBirdAcceptedChangeHandler);
	}

	private void enableRareBirdFields(boolean enable)
	{
		MacOrderDetail orderDetail = getCurrentBean();
		if (orderDetail != null && !MacOrderFormCustom.isWorkLoadModifiable(orderDetail.getMacOrder()))
		{
			enable = false;
		}

		rareBirdAccepted.setEnabled(enable);
		rareBirdConfirmationNumber.setEnabled(enable);
		rareBirdDescription.setEnabled(enable);
		rareBirdDesignation.setEnabled(enable);
		rareBirdProcessStartDate.setEnabled(enable);
		rareBirdProcessedDate.setEnabled(enable);
	}

	/**
	 * Replicates the security from {@link OrderForm} for the 3 date radio buttons, and the shipping date.
	 */
	private void applyShippingDatesSecurity()
	{
		SecurityPermissionManager securityManager = new SecurityPermissionManagerFactory().newPermissionManager();

		if (optDateSpecific != null && !securityManager.isAllowed(MacOrderFormCustom.SECURITYKEY_OPTDATESPECIFIC, PermissionType.ENABLE))
		{
			optDateSpecific.setEnabled(false);
		}

		if (optDateASAP != null && !securityManager.isAllowed(MacOrderFormCustom.SECURITYKEY_OPTDATEASAP, PermissionType.ENABLE))
		{
			optDateASAP.setEnabled(false);
		}

		if (optDateToDetermine != null && !securityManager.isAllowed(MacOrderFormCustom.SECURITYKEY_OPTDATETODETERMINE, PermissionType.ENABLE))
		{
			optDateToDetermine.setEnabled(false);
		}

		if (shippingDate != null && !securityManager.isAllowed(MacOrderFormCustom.SECURITYKEY_SHIPPINGDATE, PermissionType.ENABLE))
		{
			shippingDate.setEnabled(false);
		}
	}

	private void disableDateControlsIfOrderIsExploded()
	{
		MacOrderDetail orderDetail = getModelBean(getModelName());
		MacOrder order = orderDetail.getMacOrder();

		if (order.isExploded())
		{
			if (optDateSpecific != null)
			{
				optDateSpecific.setEnabled(false);
			}
			if (optDateASAP != null)
			{
				optDateASAP.setEnabled(false);
			}
			if (optDateToDetermine != null)
			{
				optDateToDetermine.setEnabled(false);
			}
		}
	}

	private final class RareBirdChangeHandler implements PropertyChangeListener
	{
		@Override
		public void propertyChange(PropertyChangeEvent evt)
		{

			Boolean oldValue = (Boolean) evt.getOldValue();
			Boolean newValue = (Boolean) evt.getNewValue();

			if (evt.getOldValue() != null && oldValue != newValue)
			{
				MacOrderDetail orderDetail = (MacOrderDetail) getModel(getModelName()).getBean();
				// orderDetail.setRareBird(newValue);
				enableRareBirdFields(newValue);
				if (newValue == true)
				{
					if (orderDetail.getRareBirdProcessStartDate() == null)
					{
						orderDetail.setRareBirdProcessStartDate(new Date());
					}
				}

				if (orderDetail.isRareBird())
				{
					if (StringUtils.isEmpty(orderDetail.getRareBirdDesignation()))
					{
						orderDetail.setRareBirdDesignation(orderDetail.getLocalizedDescription());
					}

					if (StringUtils.isEmpty(orderDetail.getRareBirdDescription()))
					{
						orderDetail.setRareBirdDescription(RareBirdUtils.generateRareBirdDescription(orderDetail.getProperties()));
					}
				}

				firePropertyChange(MacOrderDetail.PROPERTYNAME_RAREBIRD, oldValue, newValue);
			}
		}
	}

	private final class RareBirdAcceptedChangeHandler implements PropertyChangeListener
	{
		@Override
		public void propertyChange(PropertyChangeEvent evt)
		{
			Boolean oldValue = (Boolean) evt.getOldValue();
			Boolean newValue = (Boolean) evt.getNewValue();

			MacOrderDetail orderDetail = (MacOrderDetail) getModel(getModelName()).getBean();
			if ((oldValue != null && oldValue == false) && (newValue == null))
			{
				m5pAcceptedRejectedLabel.setForeground(BLACK);
				m5pAcceptedRejectedLabel.setText(M5PLABEL_NI_ACCEPTE_NI_REJETE);
			}

			if (evt.getOldValue() == null && oldValue != newValue)
			{
				if (newValue != null && newValue == true)
				{
					orderDetail.setRareBirdProcessedDate(new Date());
					m5pAcceptedRejectedLabel.setForeground(BLUE);
					m5pAcceptedRejectedLabel.setText(M5PLABEL_ACCEPTE);
				}
			}
			else if (evt.getOldValue() != null && oldValue == true)
			{
				orderDetail.setRareBirdProcessedDate(null);
				m5pAcceptedRejectedLabel.setForeground(RED);
				m5pAcceptedRejectedLabel.setText(M5PLABEL_REJETE);
			}
			firePropertyChange(MacOrderDetail.PROPERTYNAME_RAREBIRD, oldValue, newValue);
		}
	}

}
