package com.mac.custom.form;

import com.jgoodies.binding.value.ValueModel;
import com.netappsid.annotations.DAO;
import com.netappsid.erp.server.bo.SaleInvoice;
import com.netappsid.erp.server.dao.SaleInvoiceDAO;
import com.netappsid.erp.server.model.form.SaleInvoiceFormModel;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;

@DAO(dao = SaleInvoiceDAO.class)
public class MacSaleInvoiceFormModel extends SaleInvoiceFormModel
{
	public MacSaleInvoiceFormModel(NAIDPresentationModel businessObjectModel)
	{
		this(businessObjectModel, businessObjectModel.getModel(SaleInvoice.PROPERTYNAME_LOCALE));
	}

	public MacSaleInvoiceFormModel(NAIDPresentationModel businessObjectModel, ValueModel localeValueModel)
	{
		super(businessObjectModel, localeValueModel);
	}
}
