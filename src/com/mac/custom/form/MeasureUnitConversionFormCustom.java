package com.mac.custom.form;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import resources.forms.classes.MeasureUnitConversionForm;

import com.netappsid.erp.server.bo.MeasureUnitConversion;

public class MeasureUnitConversionFormCustom extends MeasureUnitConversionForm
{
	private final PropertyChangeListener setUsedListener = new PropertyChangeListener()
		{
			@Override
			public void propertyChange(PropertyChangeEvent event)
			{
				MeasureUnitConversion measureUnitConversion = getCurrentBean();
				measureUnitConversion.setUsed(true);
			}
		};

	@Override
	public void buildPanel()
	{
		super.buildPanel();
		addListeners();
	}

	@Override
	protected void beforeBeanChanged()
	{
		super.beforeBeanChanged();
		removeListeners();
	}

	@Override
	protected void afterBeanChanged()
	{
		super.afterBeanChanged();
		addListeners();
	}

	private void addListeners()
	{
		// Adding a listener to force the "used" flag to be set.
		// Implementation note: we can't simply set the flag in buildPanel()/afterBeanChanged(), because of the "initfield" property set in the XML invoking
		// this form: it changes the "used" value back to false after these methods have been called. We also can't simply set the used flag after modifying the
		// list of MeasureUnitConversionDetails, because if we copy a MeasureUnitConversion (using the CopyDetail button), they are already set, except for the
		// "version" field. So the safest was to simply set the used flag when modifying that (mandatory) version field, it works when adding/copying a
		// MeasureUnitConversion, however it is not mandatory when modifying an existing MeasureUnitConversion. That side effect can be desirable anyway, since
		// all MeasureUnitConversions have been set to "true" (in the database) and all newly added ones will be true, it works... but if one day we really want
		// to modify an existing one, we'll be able to change the used flag directly in the database and then use the ERP to modify the object (since it won't
		// trigger the used flag).
		MeasureUnitConversion measureUnitConversion = getCurrentBean();
		measureUnitConversion.addPropertyChangeListener(MeasureUnitConversion.PROPERTYNAME_VERSION, setUsedListener);
	}

	private void removeListeners()
	{
		MeasureUnitConversion measureUnitConversion = getCurrentBean();
		measureUnitConversion.removePropertyChangeListener(MeasureUnitConversion.PROPERTYNAME_VERSION, setUsedListener);
	}

	@Override
	public void dispose()
	{
		super.dispose();
		if (!isDisposed())
		{
			removeListeners();
		}
	}
}
