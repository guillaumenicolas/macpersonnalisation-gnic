package com.mac.custom.form;

import resources.forms.classes.OrderDetailListForm;

public class MacOrderDetailListFormCustom extends OrderDetailListForm
{
	@Override
	public void buildPanel()
	{
		setListModelName("macOrderDetailList");
		super.buildPanel();
	}
}
