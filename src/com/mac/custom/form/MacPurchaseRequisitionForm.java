package com.mac.custom.form;

import java.awt.Component;
import java.beans.PropertyChangeEvent;

import resources.forms.classes.PurchaseRequisitionForm;

import com.netappsid.utils.ComponentUtils;

public class MacPurchaseRequisitionForm extends PurchaseRequisitionForm
{
	private Component modifySupplier;

	@Override
	public void buildPanel()
	{
		super.buildPanel();

		modifySupplier = getComponent("modifysupplier");
	}

	@Override
	protected void afterBeanChanged()
	{
		super.afterBeanChanged();

		if (modifySupplier != null)
		{
			ComponentUtils.setComponentHierarchyEnabled(modifySupplier, true);
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt)
	{
		super.propertyChange(evt);

		if (modifySupplier != null)
		{
			ComponentUtils.setComponentHierarchyEnabled(modifySupplier, true);
		}
	}
}
