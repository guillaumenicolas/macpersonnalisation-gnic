package com.mac.custom.form;

import com.netappsid.framework.gui.components.model.NAIDPresentationModel;
import com.netappsid.framework.gui.formbuilder.formmodel.FormModelFactory;

public class MacOrderDetailFormModelFactory extends FormModelFactory<MacOrderDetailFormModel>
{
	@Override
	public MacOrderDetailFormModel createFormModel(NAIDPresentationModel businessObjectModel)
	{
		MacOrderDetailFormModel macOrderDetailFormModel = new MacOrderDetailFormModel(businessObjectModel);
		return macOrderDetailFormModel;
	}
}