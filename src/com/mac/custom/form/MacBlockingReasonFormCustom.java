package com.mac.custom.form;

import java.awt.Component;

import resources.forms.classes.BlockingReasonForm;

import com.mac.custom.bo.MacBlockingReason;
import com.netappsid.erp.server.enums.BlockingReasonTypeEnum;
import com.netappsid.utils.ComponentUtils;

public class MacBlockingReasonFormCustom extends BlockingReasonForm
{
	private static final String BLOCKINGREASON = "blockingReason";

	@Override
	public void buildPanel()
	{
		super.buildPanel();
		initializeComponents();
	}

	@Override
	public void initializeBlockingReason()
	{
		super.initializeBlockingReason();
		initializeComponents();
	}

	@Override
	public void initializeUnblockingReason()
	{
		super.initializeUnblockingReason();
		initializeComponents();
	}

	@Override
	public void reasonChange()
	{
		super.reasonChange();
		initializeComponents();
	}

	public void initializeComponents()
	{
		MacBlockingReason reason = (MacBlockingReason) getModelBean(BLOCKINGREASON);

		if (reason != null && reason.getType() != null)
		{
			boolean isBlockingReason = reason.getType().equals(BlockingReasonTypeEnum.BLOCK);
			setComponentEnabled("excludeFromOrdersInProductionTotal", isBlockingReason);

			if (!isBlockingReason && reason.isExcludeFormOrdersInProductionTotal())
			{
				reason.setExcludeFormOrdersInProductionTotal(false);
			}
		}
	}

	private void setComponentEnabled(String componentName, boolean enabled)
	{
		if (componentName != null && !componentName.equals(""))
		{
			Component component = getComponent(componentName);
			if (component != null)
			{
				ComponentUtils.setComponentHierarchyEnabled(component, enabled);
			}
		}
	}
}
