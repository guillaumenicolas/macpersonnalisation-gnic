package com.mac.custom.form;

import com.mac.custom.bo.MaterialProductManagementAccounting;
import com.netappsid.erp.server.bo.Company;
import com.netappsid.framework.gui.formbuilder.AbstractForm;
import com.netappsid.utils.ComponentUtils;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class MaterialProductManagementAccountingForm extends AbstractForm implements ChangeListener
{
	private final PropertyChangeListener companyChangeHandler = new CompanyChangeHandler();
	private static final String COMPANY = "company";
	private static final String MATERIALPRODUCTMANAGEMENTACCOUNTING = "materialProductManagementAccounting";

	@Override
	public void buildPanel()
	{
		super.buildPanel();

		enableComponents(false);

		initializeMaterialProductManagementAccountingListeners();
	}

	@Override
	protected void afterBeanChanged()
	{
		super.afterBeanChanged();
		enableComponents(true);
		initializeMaterialProductManagementAccountingListeners();
	}

	@Override
	protected void beforeBeanChanged()
	{
		super.beforeBeanChanged();
		removeMaterialProductManagementAccountingListeners();
	}

	private void initializeMaterialProductManagementAccountingListeners()
	{
		getModel(getModelName()).getModel(MaterialProductManagementAccounting.PROPERTYNAME_COMPANY).addValueChangeListener(companyChangeHandler);
	}

	private void removeMaterialProductManagementAccountingListeners()
	{
		getModel(getModelName()).getModel(MaterialProductManagementAccounting.PROPERTYNAME_COMPANY).removeValueChangeListener(companyChangeHandler);
	}

	protected void enableComponents(boolean enable)
	{
		MaterialProductManagementAccounting materialProductManagementAccounting = getModelBean(MATERIALPRODUCTMANAGEMENTACCOUNTING);

		if (materialProductManagementAccounting != null)
		{
			boolean isSaved = materialProductManagementAccounting.getId() != null ? true : false;
			enableComponent(COMPANY, !isSaved || materialProductManagementAccounting.getCompany() == null);
		}
	}

	protected void enableComponent(String componentName, boolean enable)
	{
		Component component = getComponent(componentName);

		if (component != null)
		{
			ComponentUtils.setComponentHierarchyEnabled(component, enable);
		}
	}

	private final class CompanyChangeHandler implements PropertyChangeListener
	{
		@Override
		public void propertyChange(PropertyChangeEvent evt)
		{
			Company oldValue = (Company) evt.getOldValue();
			Company newValue = (Company) evt.getNewValue();

			if (evt.getOldValue() != null && oldValue != newValue)
			{
				MaterialProductManagementAccounting materialProductManagementAccounting = getModelBean(MATERIALPRODUCTMANAGEMENTACCOUNTING);
				materialProductManagementAccounting.setCostCodeDetailPurchase(null);
				materialProductManagementAccounting.setCostCodeDetailSale(null);
				materialProductManagementAccounting.setCostCodePurchase(null);
				materialProductManagementAccounting.setCostCodeSale(null);
				materialProductManagementAccounting.setProductCodeAnalyticPurchase(null);
				materialProductManagementAccounting.setProductCodeAnalyticSale(null);
			}
		}
	}

	@Override
	public void stateChanged(ChangeEvent e)
	{}
}
