package com.mac.custom.form;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.hibernate.Hibernate;

import com.jgoodies.binding.value.ValueModel;
import com.mac.custom.bo.MacQuotationDetail;
import com.netappsid.annotations.DAO;
import com.netappsid.binding.beans.CollectionValueModel;
import com.netappsid.bo.model.Model;
import com.netappsid.datatypes.MonetaryAmount;
import com.netappsid.erp.server.bo.Quotation;
import com.netappsid.erp.server.bo.QuotationDetail;
import com.netappsid.erp.server.dao.QuotationDAO;
import com.netappsid.erp.server.model.form.QuotationFormModel;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;
import com.netappsid.observable.ObservableByName;
import com.netappsid.observable.ObservableElementsCollectionHandler;

@DAO(dao = QuotationDAO.class)
public class MacQuotationFormModel extends QuotationFormModel
{
	private static final String PROPERTYNAME_DETAILS = "details";
	private final ObservableElementsCollectionHandler detailsPricePropertyChangeHandler;

	public MacQuotationFormModel(NAIDPresentationModel businessObjectModel)
	{
		this(businessObjectModel, businessObjectModel.getModel(Quotation.PROPERTYNAME_LOCALE));
	}

	public MacQuotationFormModel(NAIDPresentationModel businessObjectModel, ValueModel localeValueModel)
	{
		super(businessObjectModel, localeValueModel);

		CollectionValueModel details = businessObjectModel.getCollectionValueModel(PROPERTYNAME_DETAILS);

		detailsPricePropertyChangeHandler = new ObservableElementsCollectionHandler<ObservableByName>(QuotationDetail.PROPERTYNAME_PRICE,
				new OnPostPriceChangedPropertyChangeHandler());
		detailsPricePropertyChangeHandler.install(details);
	}

	@Override
	public void dispose()
	{
		CollectionValueModel details = getBusinessObjectModel().getCollectionValueModel(PROPERTYNAME_DETAILS);
		detailsPricePropertyChangeHandler.uninstall(details);
		super.dispose();
	}

	public static final class OnPostPriceChangedPropertyChangeHandler implements PropertyChangeListener
	{
		@Override
		public void propertyChange(PropertyChangeEvent evt)
		{
			Object source = evt.getSource();
			if (MacQuotationDetail.class.isAssignableFrom(Hibernate.getClass(source)))
			{
				MacQuotationDetail detail = (MacQuotationDetail) Model.getTarget(source);
				MonetaryAmount retailPrice = detail.getRetailPrice();
				MonetaryAmount price = detail.getPrice();
				detail.setPriceOverrided(!price.equals(retailPrice));
			}
		}
	}
}
