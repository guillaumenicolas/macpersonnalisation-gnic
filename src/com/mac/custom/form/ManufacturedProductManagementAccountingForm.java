package com.mac.custom.form;

import com.mac.custom.bo.ManufacturedProductManagementAccounting;
import com.netappsid.erp.server.bo.Company;
import com.netappsid.framework.gui.formbuilder.AbstractForm;
import com.netappsid.utils.ComponentUtils;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class ManufacturedProductManagementAccountingForm extends AbstractForm implements ChangeListener
{
	private final PropertyChangeListener companyChangeHandler = new CompanyChangeHandler();
	private static final String COMPANY = "company";
	private static final String MANUFACTUREDPRODUCTMANAGEMENTACCOUNTING = "manufacturedProductManagementAccounting";

	@Override
	public void buildPanel()
	{
		super.buildPanel();

		enableComponents(false);

		initializeManufacturedProductManagementAccountingListeners();
	}

	@Override
	protected void afterBeanChanged()
	{
		super.afterBeanChanged();
		enableComponents(true);
		initializeManufacturedProductManagementAccountingListeners();
	}

	@Override
	protected void beforeBeanChanged()
	{
		super.beforeBeanChanged();
		removeManufacturedProductManagementAccountingListeners();
	}

	private void initializeManufacturedProductManagementAccountingListeners()
	{
		getModel(getModelName()).getModel(ManufacturedProductManagementAccounting.PROPERTYNAME_COMPANY).addValueChangeListener(companyChangeHandler);
	}

	private void removeManufacturedProductManagementAccountingListeners()
	{
		getModel(getModelName()).getModel(ManufacturedProductManagementAccounting.PROPERTYNAME_COMPANY).removeValueChangeListener(companyChangeHandler);
	}

	protected void enableComponents(boolean enable)
	{
		ManufacturedProductManagementAccounting manufacturedProductManagementAccounting = getModelBean(MANUFACTUREDPRODUCTMANAGEMENTACCOUNTING);

		if (manufacturedProductManagementAccounting != null)
		{
			boolean isSaved = manufacturedProductManagementAccounting.getId() != null ? true : false;
			enableComponent(COMPANY, !isSaved || manufacturedProductManagementAccounting.getCompany() == null);
		}
	}

	protected void enableComponent(String componentName, boolean enable)
	{
		Component component = getComponent(componentName);

		if (component != null)
		{
			ComponentUtils.setComponentHierarchyEnabled(component, enable);
		}
	}

	private final class CompanyChangeHandler implements PropertyChangeListener
	{
		@Override
		public void propertyChange(PropertyChangeEvent evt)
		{
			Company oldValue = (Company) evt.getOldValue();
			Company newValue = (Company) evt.getNewValue();

			if (evt.getOldValue() != null && oldValue != newValue)
			{
				ManufacturedProductManagementAccounting manufacturedProductManagementAccounting = getModelBean(MANUFACTUREDPRODUCTMANAGEMENTACCOUNTING);
				manufacturedProductManagementAccounting.setCostCodeDetailPurchase(null);
				manufacturedProductManagementAccounting.setCostCodeDetailSale(null);
				manufacturedProductManagementAccounting.setCostCodePurchase(null);
				manufacturedProductManagementAccounting.setCostCodeSale(null);
				manufacturedProductManagementAccounting.setProductCodeAnalyticPurchase(null);
				manufacturedProductManagementAccounting.setProductCodeAnalyticSale(null);
			}
		}
	}

	@Override
	public void stateChanged(ChangeEvent e)
	{}
}
