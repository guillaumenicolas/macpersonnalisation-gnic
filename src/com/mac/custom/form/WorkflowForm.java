package com.mac.custom.form;

import java.util.List;

import com.mac.custom.bo.MacWorkflowMaster;
import com.netappsid.bo.EntityStatus;
import com.netappsid.framework.gui.formbuilder.AbstractForm;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.Loader;

public class WorkflowForm extends AbstractForm 
{
	private ServiceLocator<Loader> loaderServicesLocator = new ServiceLocator<Loader>(LoaderBean.class);
	
	@Override
	public void buildPanel() 
	{
		super.buildPanel();
		
		setDefaultEntity();
	}
	
	private void setDefaultEntity()
	{
		MacWorkflowMaster macWFMaster = getCurrentBean();

		List<MacWorkflowMaster> wfSetupList = loaderServicesLocator.get().findAll(MacWorkflowMaster.class, MacWorkflowMaster.class);

		if (!wfSetupList.isEmpty())
		{
			macWFMaster = wfSetupList.get(0);
		}
		
		setModelBean("wfMaster", macWFMaster);
		macWFMaster.setEntityStatus(EntityStatus.READY);
	}
}
