package com.mac.custom.form;

public class MacAccountingOrderListFormCustom extends MacOrderListFormCustom
{
	@Override
	public void buildPanel()
	{
		setListModelName("macAccountingOrderList");
		super.buildPanel();
		initializeCellStyleProvider("grid_orderDetails");
	}
}
