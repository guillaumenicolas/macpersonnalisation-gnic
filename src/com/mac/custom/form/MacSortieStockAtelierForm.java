package com.mac.custom.form;

import java.awt.Component;
import java.awt.Font;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.math.BigDecimal;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import com.mac.custom.bo.MacCompany;
import com.netappsid.component.swing.NAIDComponentImpl;
import com.netappsid.erp.client.gui.components.MeasureUnitField;
import com.netappsid.erp.server.bo.InventoryAdjustment;
import com.netappsid.erp.server.bo.InventoryProduct;
import com.netappsid.erp.server.bo.User;
import com.netappsid.erp.server.datatypes.GroupMeasureValue;
import com.netappsid.erp.server.model.MassInventoryAdjustment;
import com.netappsid.erp.server.services.beans.StockServicesBean;
import com.netappsid.erp.server.services.interfaces.remote.StockServicesRemote;
import com.netappsid.framework.gui.formbuilder.AbstractForm;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.framework.utils.SystemVariable;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.Loader;
import com.netappsid.utils.ComponentUtils;
import com.netappsid.utils.DateUtils;

public class MacSortieStockAtelierForm extends AbstractForm
{
	private MassInventoryAdjustment massInventoryAdjustment;
	private JLabel messageRetourSortieStock;
	private JButton validerSortie;
	private JComboBox unite;

	public String message;
	private ServiceLocator<StockServicesRemote> stockServicesServiceLocator;

	protected String getFollowedString(String prefix, String input)
	{
		return input;
	}

	@Override
	public void buildPanel()
	{

		super.buildPanel();

		validerSortie = getComponent("validerSortie");

		validerSortie.setBackground(RED);

		Font font = new Font("Arial", Font.BOLD, 20);
		validerSortie.setFont(font);
		messageRetourSortieStock = getComponent("messageRetourSortieStock");
		addActionListener(validerSortie, "sortieStock");

		// Initialisation du type d'ajustement par défaut et de la localisation
		this.massInventoryAdjustment = ((MassInventoryAdjustment) getModelBean(getModelName()));
		this.stockServicesServiceLocator = new ServiceLocator<StockServicesRemote>(StockServicesBean.class);
		setDefaultValues();

		MacCompany company = (MacCompany) massInventoryAdjustment.getUser().getCompany();

		if (massInventoryAdjustment.getLocation() == null)
		{
			massInventoryAdjustment.setLocation(((MacCompany) massInventoryAdjustment.getUser().getCompany()).getDefaultInventoryAdjustmentLocation());

		}
		if (massInventoryAdjustment.getInventoryAdjustmentType() == null)
		{
			massInventoryAdjustment.setInventoryAdjustmentType(((MacCompany) massInventoryAdjustment.getUser().getCompany())
					.getDefaultInventoryAdjustmentType());
		}
		// initializeMassInventoryAdjustmentListeners();
		initializeKeyboardFocusManager(massInventoryAdjustment);
	}

	public void sortieStock()
	{
		StringBuffer expandableMessage = new StringBuffer();
		String quantiteSaisie = massInventoryAdjustment.getQuantity().toString();
		massInventoryAdjustment.addInventoryAdjustment();
		for (InventoryAdjustment inventoryAdjustment : massInventoryAdjustment.getInventoryAdjustments())
		{
			InventoryAdjustment savedInventoryadjustment = removeStock(inventoryAdjustment);
			if (savedInventoryadjustment.getId() == null)
			{
				expandableMessage.append(getString("ThereIsNotEnoughAvailableStockForTheProduct"));
				expandableMessage.append(": ");
				expandableMessage.append(savedInventoryadjustment.getInventoryProduct().getCode());
				expandableMessage.append(" - ");
				expandableMessage.append(savedInventoryadjustment.getQuantity().toString());
				expandableMessage.append(" ");
				expandableMessage.append(getString("location"));
				expandableMessage.append(" : ");
				expandableMessage.append(savedInventoryadjustment.getLocation().getCode());
				expandableMessage.append("\n");
			}
		}

		String title = getString("StockReduction");

		if (expandableMessage.toString().length() > 0)
		{
			Font font = new Font("Arial", Font.BOLD, 20);
			messageRetourSortieStock.setFont(font);
			message = expandableMessage.toString();
			messageRetourSortieStock.setForeground(RED);

		}
		else
		{
			if (massInventoryAdjustment.getInventoryAdjustments().size() > 0)
			{
				Font font = new Font("Arial", Font.BOLD, 20);
				messageRetourSortieStock.setFont(font);
				message = ("Sortie de stock : " + massInventoryAdjustment.getInventoryAdjustments().get(0).getInventoryProduct().getCode() + " ("
						+ massInventoryAdjustment.getInventoryAdjustments().get(0).getInventoryProduct().getLocalizedDescription() + ") => " + quantiteSaisie);
				messageRetourSortieStock.setForeground(BLACK);
				massInventoryAdjustment.setBarCode(null);
				massInventoryAdjustment.setQuantity(null);
				massInventoryAdjustment.setInventoryProduct(null);

			}
		}

		messageRetourSortieStock.setText(message);

		massInventoryAdjustment.removeAllFromInventoryAdjustments();
	}

	private void initializeKeyboardFocusManager(MassInventoryAdjustment massInventoryAdjustment)
	{
		final MassInventoryAdjustment finalMassInventoryAdjustment = massInventoryAdjustment;
		final MacSortieStockAtelierForm finalMassInventoryAdjustmentForm = this;
		final ServiceLocator loaderLocator = new ServiceLocator(LoaderBean.class);

		KeyboardFocusManager.getCurrentKeyboardFocusManager().addPropertyChangeListener("focusOwner", new PropertyChangeListener()
			{
				final KeyListener keyListener = new KeyListener()
					{
						private String input = "";

						@Override
						public void keyPressed(KeyEvent e)
						{
							if (e.getKeyCode() == 116)
							{
								sortieStock();
							}
						}

						@Override
						public void keyReleased(KeyEvent e)
						{}

						@Override
						public void keyTyped(KeyEvent e)
						{

							if ((e.getSource() instanceof JTextField))
							{
								JTextField field = (JTextField) e.getSource();

								if ("measureUnitField.quantity".equals(field.getName()))
								{

									if (e.getKeyChar() == '\n')
									{
										BigDecimal quantite = finalMassInventoryAdjustment.getQuantity().getValue();
										InventoryProduct inventoryProduct = finalMassInventoryAdjustment.getInventoryProduct();
										if (quantite != null & inventoryProduct != null & !quantite.equals(BigDecimal.ZERO))
										{
											GroupMeasureValue quantity = finalMassInventoryAdjustment.getQuantity();
											if (quantity == null)
											{

												quantity = new GroupMeasureValue(inventoryProduct.getInventoryMeasureGroupMeasure(), BigDecimal.ZERO,
														inventoryProduct.getDefaultUnitConversion(), inventoryProduct.getDefaultConvertedUnit());

											}
											quantity.setValue(quantite);
											finalMassInventoryAdjustment.setQuantity(quantity);

										}
										SwingUtilities.invokeLater(new Runnable()
											{

												@Override
												public void run()
												{
													NAIDComponentImpl barcode = (NAIDComponentImpl) getComponent("barCode");

													JTextField barcodeField = (JTextField) barcode.getField();
													barcodeField.requestFocusInWindow();
												}
											});
									}
								}

								if ("barCode".equals(field.getParent().getParent().getName()))

								{
									String productUPCCode = "";

									if ((e.getKeyChar() != '\n') && (e.getKeyChar() != '\b') && (e.getKeyChar() != ''))
									{
										this.input += e.getKeyChar();
									}
									else if (e.getKeyChar() == '\n')
									{
										if (finalMassInventoryAdjustment.getBarCode() != null)
										{
											this.input = finalMassInventoryAdjustment.getBarCode().toUpperCase();
											messageRetourSortieStock.setText("");
										}

										productUPCCode = getFollowedString("NAIDUPC-", this.input);

										if (productUPCCode != null)
										{
											List inventoryProducts = ((Loader) loaderLocator.get()).findByField(InventoryProduct.class,
													MassInventoryAdjustment.class, "upcCode", productUPCCode);

											if (!inventoryProducts.isEmpty())
											{
												finalMassInventoryAdjustment.setInventoryProduct((InventoryProduct) inventoryProducts.get(0));
											}
											else
											{
												finalMassInventoryAdjustment.setInventoryProduct(null);
											}
										}

										finalMassInventoryAdjustment.setBarCode(null);

										SwingUtilities.invokeLater(new Runnable()
											{
												@Override
												public void run()
												{
													NAIDComponentImpl quantity = (NAIDComponentImpl) getComponent("quantity");

													MeasureUnitField quantityField = (MeasureUnitField) quantity.getField();

													quantityField.requestQuantityFocus();
												}
											});
										this.input = "";
									}
								}
							}
						}
					};

				@Override
				public void propertyChange(PropertyChangeEvent evt)
				{
					Component focusOwner = (Component) evt.getNewValue();
					Component oldFocusOwner = (Component) evt.getOldValue();

					if (oldFocusOwner != null)
					{
						oldFocusOwner.removePropertyChangeListener(this);
					}

					MacSortieStockAtelierForm form = ComponentUtils.getContainer(focusOwner, MacSortieStockAtelierForm.class);

					if ((form == null) && (focusOwner != null))
					{
						form = (MacSortieStockAtelierForm) ComponentUtils.getComponent((java.awt.Container) focusOwner, MacSortieStockAtelierForm.class);
					}

					if (finalMassInventoryAdjustmentForm == form)
					{
						if (oldFocusOwner != null)
						{
							oldFocusOwner.removeKeyListener(this.keyListener);
						}
						focusOwner.removeKeyListener(this.keyListener);
						focusOwner.addKeyListener(this.keyListener);
					}
				}
			});
	}

	private void setDefaultValues()
	{
		this.massInventoryAdjustment.setDate(DateUtils.getCurrentDateTime());
		User user = (User) SystemVariable.getVariable("user");
		this.massInventoryAdjustment.setUser(user);
	}

	protected InventoryAdjustment removeStock(InventoryAdjustment inventoryAdjustment)
	{

		return this.stockServicesServiceLocator.get().consumeStockFIFO(inventoryAdjustment);
	}
}
