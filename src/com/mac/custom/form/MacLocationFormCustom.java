package com.mac.custom.form;

import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.List;

import javax.swing.SwingUtilities;

import com.mac.custom.bo.MacLocation;
import com.netappsid.component.ExpandableDialogData;
import com.netappsid.component.OptionPane.MessageType;
import com.netappsid.erp.server.services.resultholders.ResultHolder;
import com.netappsid.framework.gui.formbuilder.AbstractForm;

public class MacLocationFormCustom extends AbstractForm
{
	private PropertyChangeListener storePCL;
	private List<Component> componentsToEnableForStore, componentsToDisableForStore;

	public PropertyChangeListener getStorePCL()
	{
		if (storePCL == null)
		{
			storePCL = new PropertyChangeListener()
				{
					private boolean inhabit;

					@Override
					public void propertyChange(final PropertyChangeEvent evt)
					{
						if (inhabit)
						{
							return;
						}

						inhabit = true;
						try
						{
							final MacLocation location = (MacLocation) evt.getSource();
							final boolean store = location.isStore();
							ResultHolder<Void> result = new ResultHolder<Void>();
							if (store)
							{
								if (location.isIncludeInMRPCalculation())
								{
									result.addError(getString("locationFormChangeStore.error.includeInMRPCalculation"));
								}
								if (location.isQuarantine())
								{
									result.addError(getString("locationFormChangeStore.error.quarantine"));
								}
							}
							else
							{
								if (!location.getSubLocations().isEmpty())
								{
									result.addError(getString("locationFormChangeStore.error.subLocations"));
								}
							}
							if (result.hasErrors())
							{
								ExpandableDialogData expandableMessages = new ExpandableDialogData(getString("locationFormChangeStore"),
										getString("locationFormChangeStoreWithErrors"), MessageType.ERROR,
										result.getAllMessagesText());
								getOptionPane().showExpandableMessages(expandableMessages);

								location.setStore(!store);
							}
							else
							{
								for (Component comp : componentsToEnableForStore)
								{
									comp.setEnabled(store);
								}
								for (Component comp : componentsToDisableForStore)
								{
									comp.setEnabled(!store);
								}
							}
						}
						finally
						{
							inhabit = false;
						}
					}
				};
		}

		return storePCL;
	}

	@Override
	public void buildPanel()
	{
		super.buildPanel();

		componentsToEnableForStore = Arrays.asList(getComponent("grid_subLocations"));
		componentsToDisableForStore = Arrays.asList(getComponent("quarantine"), getComponent("includeInMRPCalculation"));
		addStoreListener();
	}

	@Override
	protected void afterBeanChanged()
	{
		super.afterBeanChanged();

		addStoreListener();
	}

	@Override
	protected void beforeBeanChanged()
	{
		removeStoreListener();

		super.beforeBeanChanged();
	}

	@Override
	public void dispose()
	{
		if (!isDisposed())
		{
			removeStoreListener();

			super.dispose();
		}
	}

	private void removeStoreListener()
	{
		((MacLocation) getCurrentBean()).removePropertyChangeListener(MacLocation.PROPERTYNAME_STORE, getStorePCL());
	}

	private void addStoreListener()
	{
		((MacLocation) getCurrentBean()).addPropertyChangeListener(MacLocation.PROPERTYNAME_STORE, getStorePCL());

		SwingUtilities.invokeLater(new Runnable()
			{

				@Override
				public void run()
				{
					getStorePCL().propertyChange(new PropertyChangeEvent(getCurrentBean(), MacLocation.PROPERTYNAME_STORE, null, null));
				}
			});
	}
}
