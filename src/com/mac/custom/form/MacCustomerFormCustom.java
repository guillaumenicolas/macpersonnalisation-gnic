package com.mac.custom.form;

import java.awt.Color;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;

import com.jidesoft.swing.JideTabbedPane;
import com.mac.custom.bo.MacCommercialClass;
import com.mac.custom.bo.MacCustomer;
import com.mac.custom.bo.MacTerm;
import com.mac.custom.enums.TypeClientEnum;
import com.netappsid.action.Cancel;
import com.netappsid.action.Delete;
import com.netappsid.action.New;
import com.netappsid.action.adapter.ActionNotifyAdapter;
import com.netappsid.action.event.ActionNotifyEvent;
import com.netappsid.bo.model.ListModel;
import com.netappsid.erp.client.utils.ERPSystemVariable;
import com.netappsid.erp.server.bo.AddressType;
import com.netappsid.erp.server.bo.AddressType.Type;
import com.netappsid.erp.server.bo.CommercialEntityAddress;
import com.netappsid.erp.server.bo.Currency;
import com.netappsid.erp.server.bo.Customer;
import com.netappsid.erp.server.bo.PaymentMode;
import com.netappsid.erp.server.bo.PriceList;
import com.netappsid.erp.server.bo.SaleType;
import com.netappsid.erp.server.bo.User;
import com.netappsid.erp.server.listmodel.DocumentList;
import com.netappsid.europe.naid.custom.form.CustomerFormCustom;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.framework.utils.SystemVariable;
import com.netappsid.security.auth.server.SecurityUser;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.Loader;
import com.netappsid.utils.ComponentUtils;

public class MacCustomerFormCustom extends CustomerFormCustom
{
	private static final String CUSTOMER = "customer";
	private JRadioButton optCrmaValider;
	private JRadioButton optProspect;
	private JRadioButton optValidationFinance;
	private JRadioButton optClient;
	private JRadioButton optDivers;
	private Component afficherMontantFacturees;
	private final PropertyChangeListener typologieChangeHandler = new TypologieChangeHandler();
	private final PropertyChangeListener termChangeHandler = new TermChangeHandler();
	private final ServiceLocator<Loader> loaderLocator = new ServiceLocator<Loader>(LoaderBean.class);

	private final AddressesSelectionChangedHandler addressesSelectionChangedHandler = new AddressesSelectionChangedHandler();

	// Ajout OGA du 26/06/14 - Permet de r�cup�rer les onglets � coloriser
	private void initializeTabsColor()
	{
		JTabbedPane tabbedpane = (JTabbedPane) getComponent("detailPanel");
		if (tabbedpane != null)
		{
			JideTabbedPane parentTable = (JideTabbedPane) tabbedpane;

			// R�cup�ration des objets de order
			MacCustomer customer = getCurrentBean();

			// V�rification si onglet pi�ces jointes renseign�s auquel cas le
			// color� en ORANGE
			if (!customer.getAttachments().isEmpty())
			{
				paintTab(parentTable, "tab_attachment", Color.ORANGE);
			}
			else
			{
				paintTab(parentTable, "tab_attachment", null);
			}

			// V�rification si onglet notes renseign�s auquel cas le
			// color� en ORANGE
			if (!customer.getNotes().isEmpty())
			{
				paintTab(parentTable, "tab_notes", Color.ORANGE);
			}
			else
			{
				paintTab(parentTable, "tab_notes", null);
			}
		}
	}

	// Ajout OGA du 26/06/14 - Permet de colorer les onglets pr�c�demment s�lectionn�s
	private void paintTab(JideTabbedPane parentTable, String tabKey, Color color)
	{
		if (parentTable != null && tabKey != null)
		{
			int tabIndex = parentTable.indexOfTab(getString(tabKey));

			if (tabIndex != -1)
			{
				((JTabbedPane) getComponent("detailPanel")).setBackgroundAt(tabIndex, color);
			}
		}
	}

	@Override
	public void buildPanel()
	{
		super.buildPanel();

		New actionNew = getAction("new");
		Delete actionDelete = getAction("delete");
		Cancel actionCancel = getAction("cancel");

		afficherMontantFacturees = getComponent("afficherMontantFacturees");

		optCrmaValider = getComponent("optCrmaValider");
		optProspect = getComponent("optProspect");
		optValidationFinance = getComponent("optValidationFinance");
		optClient = getComponent("optClient");
		optDivers = getComponent("optDivers");

		ActionNotifyAdapter setDefaultValuesListener = new ActionNotifyAdapter()
			{
				@Override
				public void after(ActionNotifyEvent event)
				{
					setDefaultValues();
				}
			};

		if (actionNew != null)
		{
			actionNew.addActionNotifyListener(setDefaultValuesListener);
		}

		if (actionDelete != null)
		{
			actionDelete.addActionNotifyListener(setDefaultValuesListener);
		}

		if (actionCancel != null)
		{
			actionCancel.addActionNotifyListener(setDefaultValuesListener);
		}

		addActionListener(optCrmaValider, "initializeCrmaValider");
		addActionListener(optProspect, "initializeProspect");
		addActionListener(optValidationFinance, "initializeValidationFinance");
		addActionListener(optClient, "initializeClient");
		addActionListener(optDivers, "initializeDivers");

		setDefaultValues();

		initializeCustomerListeners();
	}

	@Override
	protected void afterBeanChanged()
	{
		super.afterBeanChanged();
		setTypeClientRadioButtons();
		toggleAfficherMontantFactureesButton();
		initializeCustomerListeners();
		// Ajout OGA du 01/07/14 - Fonction pour coloriser le ou les onglet(s)
		initializeTabsColor();
	}

	@Override
	protected void beforeBeanChanged()
	{
		super.beforeBeanChanged();
		removeCustomerListeners();
	}

	private void initializeCustomerListeners()
	{
		getModel(getModelName()).getModel(Customer.PROPERTYNAME_CUSTOMERCLASS).addValueChangeListener(typologieChangeHandler);
		getModel(getModelName()).getModel(Customer.PROPERTYNAME_TERM).addValueChangeListener(termChangeHandler);

		NAIDPresentationModel customerAddressesModel = getModel(getModelName() + '.' + MacCustomer.PROPERTYNAME_ADDRESSES);
		customerAddressesModel.getSelectionModel().addSelectionChangeListener(addressesSelectionChangedHandler);
	}

	private void removeCustomerListeners()
	{
		getModel(getModelName()).getModel(Customer.PROPERTYNAME_CUSTOMERCLASS).removeValueChangeListener(typologieChangeHandler);
		getModel(getModelName()).getModel(Customer.PROPERTYNAME_TERM).removeValueChangeListener(termChangeHandler);

		NAIDPresentationModel customerAddressesModel = getModel(getModelName() + '.' + MacCustomer.PROPERTYNAME_ADDRESSES);
		customerAddressesModel.getSelectionModel().removeSelectionChangeListener(addressesSelectionChangedHandler);
	}

	@Override
	protected void beanChanged()
	{
		super.beanChanged();
	}

	public void initializeCrmaValider()
	{
		MacCustomer customer = getModelBean(CUSTOMER);

		customer.setTypeClient(TypeClientEnum.CRMAVALIDER);

		getModel(CUSTOMER).setBean(customer);
		getModel(CUSTOMER).setDirty(true);
	}

	public void initializeProspect()
	{
		MacCustomer customer = getModelBean(CUSTOMER);

		customer.setTypeClient(TypeClientEnum.PROSPECT);

		getModel(CUSTOMER).setBean(customer);
		getModel(CUSTOMER).setDirty(true);
	}

	public void initializeValidationFinance()
	{
		MacCustomer customer = getModelBean(CUSTOMER);

		customer.setTypeClient(TypeClientEnum.VALIDATIONFINANCE);

		getModel(CUSTOMER).setBean(customer);
		getModel(CUSTOMER).setDirty(true);
	}

	public void initializeClient()
	{
		MacCustomer customer = getModelBean(CUSTOMER);

		customer.setTypeClient(TypeClientEnum.CLIENT);

		getModel(CUSTOMER).setBean(customer);
		getModel(CUSTOMER).setDirty(true);
	}

	public void initializeDivers()
	{
		MacCustomer customer = getModelBean(CUSTOMER);

		customer.setTypeClient(TypeClientEnum.DIVERS);

		getModel(CUSTOMER).setBean(customer);
		getModel(CUSTOMER).setDirty(true);
	}

	public void setTypeClientRadioButtons()
	{
		MacCustomer customer = (MacCustomer) getModelBean(CUSTOMER);

		if (customer != null)
		{
			if (customer.getId() != null)
			{
				if (customer.getTypeClient().equals(TypeClientEnum.CRMAVALIDER))
				{
					optCrmaValider.setSelected(true);
				}
				else if (customer.getTypeClient().equals(TypeClientEnum.PROSPECT))
				{
					optProspect.setSelected(true);
				}
				else if (customer.getTypeClient().equals(TypeClientEnum.VALIDATIONFINANCE))
				{
					optValidationFinance.setSelected(true);
				}
				else if (customer.getTypeClient().equals(TypeClientEnum.CLIENT))
				{
					optClient.setSelected(true);
				}
				else if (customer.getTypeClient().equals(TypeClientEnum.DIVERS))
				{
					optDivers.setSelected(true);
				}
			}
		}

		getModel(CUSTOMER).setDirty(false);
	}

	public void setDefaultSelected()
	{
		MacCustomer customer = (MacCustomer) getModelBean(CUSTOMER);

		optProspect.setSelected(true);
		customer.setTypeClient(TypeClientEnum.PROSPECT);
	}

	public void setDefaultValues()
	{
		MacCustomer customer = (MacCustomer) getModelBean(CUSTOMER);

		if (customer != null && customer.getId() == null && customer.isReadyForModifications())
		{
			optProspect.setSelected(true);

			customer.setTypeClient(TypeClientEnum.PROSPECT);

			// Valeur par défaut à OUI pour Né de BDC
			customer.setPONumberMandatory(true);
			// Valeur par défaut à OUI pour Né de BDC Unique
			customer.setUniquePONumber(true);
			// Valeur par défaut à OUI pour Controle Credit
			customer.setControlCredit(true);
			// Mettre date date du jour dans En Affaire Depuis
			// et Client Depuis
			customer.setInBusinessSince(new Date());
			customer.setRegisteredDate(new Date());
			// Positionner par défaut la Locale à Fr (France)
			Locale locale = new Locale("fr", "FR");
			customer.setLocale(locale);

			// Positionner par défaut la Devise EURO
			List<Currency> macCurrencyList = loaderLocator.get().findByField(Currency.class, Currency.class, Currency.PROPERTYNAME_CODE, "EUR");
			if (!macCurrencyList.isEmpty())
			{
				customer.setCurrency(macCurrencyList.get(0));
			}
			// Positionner par défaut le Terme à 300
			List<MacTerm> macTermList = loaderLocator.get().findByField(MacTerm.class, MacTerm.class, MacTerm.PROPERTYNAME_CODE, "300");
			if (!macTermList.isEmpty())
			{
				customer.setTerm(macTermList.get(0));
			}
			// Positionner par défaut le mode de paiement à LDR
			List<PaymentMode> macPaymentModeList = loaderLocator.get().findByField(PaymentMode.class, PaymentMode.class, PaymentMode.PROPERTYNAME_CODE, "LDR");
			if (!macPaymentModeList.isEmpty())
			{
				customer.setPaymentMode(macPaymentModeList.get(0));
			}
			// Positionner par defaut le tarif a PDTFABER
			List<PriceList> macPriceListList = loaderLocator.get().findByField(PriceList.class, PriceList.class, PaymentMode.PROPERTYNAME_CODE, "PDTFABER");
			if (!macPriceListList.isEmpty())
			{
				customer.setPriceList(macPriceListList.get(0));
			}

			// Positionner la société de l'utilisateur par défaut
			Object user = SystemVariable.getVariable(ERPSystemVariable.USER);
			if (user != null)
			{
				User currentUser = (User) user;
				customer.setCompany(currentUser.getCompany());
			}

			// Positionner par defaut Type de Ventes à DIFFUS
			List<SaleType> macSaleTypeList = loaderLocator.get().findByField(SaleType.class, SaleType.class, SaleType.PROPERTYNAME_CODE, "DIFF");
			if (!macSaleTypeList.isEmpty())
			{
				customer.setSaleType(macSaleTypeList.get(0));
			}

			// Valeur par défaut à 1 pour Delai Expedition
			customer.setShippingDelay((short) 1);

			customer.setDirty(false);
		}

		// tab_creditInfo_infos en lecture seule
		Component tabCreditInfoInfos = getComponent("tab_creditInfo_infos");
		if (tabCreditInfoInfos != null)
		{
			ComponentUtils.setComponentHierarchyEnabled(tabCreditInfoInfos, false);
		}
	}

	protected void toggleAfficherMontantFactureesButton()
	{
		MacCustomer customer = (MacCustomer) getModelBean(CUSTOMER);

		if (customer != null)
		{
			ComponentUtils.setComponentHierarchyEnabled(afficherMontantFacturees, (customer.getId() != null));
		}
	}

	private final class TypologieChangeHandler implements PropertyChangeListener
	{
		@Override
		public void propertyChange(PropertyChangeEvent evt)
		{
			MacCommercialClass oldValue = (MacCommercialClass) evt.getOldValue();
			MacCommercialClass newValue = (MacCommercialClass) evt.getNewValue();

			if (evt.getOldValue() != null && oldValue != newValue)
			{
				MacCustomer customer = (MacCustomer) getModel(getModelName()).getBean();
				customer.setActiviteClient(null);
				customer.setEnseigneClient(null);
			}
		}
	}

	private final class TermChangeHandler implements PropertyChangeListener
	{
		@Override
		public void propertyChange(PropertyChangeEvent evt)
		{
			MacTerm term = (MacTerm) evt.getNewValue();

			MacCustomer customer = (MacCustomer) getModel(getModelName()).getBean();
			if (customer.getPaymentMode() == null && term.getPaymentMode() != null)
			{
				customer.setPaymentMode(term.getPaymentMode());
			}
		}
	}

	private final class AddressesSelectionChangedHandler implements PropertyChangeListener
	{
		public static final String SECURITYGROUP_ADV = "ADV_CREATION_CLIENT";
		public static final String SECURITYGROUP_COMPTA_CLIENT = "COMPTA_CLT";

		private User user = null;
		private SecurityUser securityUser = null;

		private AddressesSelectionChangedHandler()
		{
			user = SystemVariable.getVariable(ERPSystemVariable.USER);
			securityUser = loaderLocator.get().findById(SecurityUser.class, SecurityUser.class, user.getSecurityUser().getId());
		}

		private void enableOrDisableAddressActions(boolean enable)
		{
			setComponentEnable("modifyaddress", enable);
			setComponentEnable("removeAddress", false); // remove address toujours a false
			// setComponentEnable("setToDefaultAddress", enable);
		}

		@Override
		public void propertyChange(PropertyChangeEvent evt)
		{
			List<CommercialEntityAddress> selectedAddress = getModel("customer.addresses").getSelectedItems();

			if (securityUser != null)
			{
				if (securityUser.isMember(SECURITYGROUP_ADV) || securityUser.isMember(SECURITYGROUP_COMPTA_CLIENT))
				{
					for (CommercialEntityAddress commercialEntityAddress : selectedAddress)
					{
						// pour chaque ligne d'adresse sélectionné
						// on vérifie selon le type de l'adresse si le user est bon.
						for (AddressType addressType : commercialEntityAddress.getTypes())
						{
							if (securityUser.isMember(SECURITYGROUP_ADV))
							{
								setComponentEnable("createAddress", true);
								if (addressType.getInternalId().equals(Type.SHIPPING.getInternalId()))
								{
									enableOrDisableAddressActions(true);
								}
								else
								{
									enableOrDisableAddressActions(false);
								}
							}
							if (securityUser.isMember(SECURITYGROUP_COMPTA_CLIENT))
							{
								setComponentEnable("createAddress", true);
								if (addressType.getInternalId().equals(Type.HEADQUARTERS.getInternalId())
										|| addressType.getInternalId().equals(Type.INVOICE.getInternalId()))
								{
									enableOrDisableAddressActions(true);
								}
								else
								{
									enableOrDisableAddressActions(false);
								}
							}
						}
					}
				}
			}
		}
	}

	private void setComponentEnable(String componentName, boolean enable)
	{
		if (componentName != null && !componentName.equals(""))
		{
			Component component = getComponent(componentName);
			if (component != null)
			{
				ComponentUtils.setComponentHierarchyEnabled(component, enable);
			}
		}
	}

	@Override
	protected void afterReinitializeListModelCriteria(ListModel listModel)
	{
		super.afterReinitializeListModelCriteria(listModel);

		setUserCriterion(listModel);
	}

	@Override
	protected void afterClearListModelCriteria(ListModel listModel)
	{
		super.afterClearListModelCriteria(listModel);

		setUserCriterion(listModel);
	}

	private void setUserCriterion(ListModel listModel)
	{
		if (DocumentList.class.isAssignableFrom(listModel.getClass()))
		{
			((DocumentList) listModel).setUser(null);
		}
	}
}
