package com.mac.custom.form;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;

import com.google.common.collect.Lists;
import com.netappsid.erp.client.gui.DefaultProductFormPanel;
import com.netappsid.erp.server.bo.CommercialEntity;
import com.netappsid.erp.server.bo.Company;
import com.netappsid.erp.server.bo.Document;
import com.netappsid.erp.server.bo.Product;
import com.netappsid.resources.Translator;

/**
 * A custom Mac class that extends {@link DefaultProductFormPanel}, so that it can have custom behavior such as having additional columns in its table.
 * 
 * @author ftaillefer
 * 
 * @param <T>
 *            The same type as in superclass {@link DefaultProductFormPanel}.
 */
public class MacDefaultProductFormPanel<T extends CommercialEntity> extends DefaultProductFormPanel<T>
{

	public MacDefaultProductFormPanel(T commercialEntity, Company company, Locale locale, List<Class<? extends Product>> productClasses, Document document)
	{
		super(commercialEntity, company, locale, productClasses, document);
	}

	@Override
	protected List<Object[]> getProducts(Locale locale, Serializable commercialEntityId, Serializable companyId, List<Class<? extends Product>> productClasses,
			Class<? extends Document> documentClass)
	{
		// Call a query with one extra field
		return productServicesServiceLocator.get().getProducts(locale, commercialEntityId, companyId, productClasses, documentClass,
				Lists.newArrayList("product.ancienCodeArticle"));
	}

	@Override
	protected List<Object> buildColumnNames()
	{
		List<Object> columnNames = super.buildColumnNames();
		columnNames.add(Translator.getString("ancienCodeArticle"));

		return columnNames;
	}

	@Override
	protected int getColumnCount()
	{
		// Adds +1 explicitly for the same reason the number is hard-coded in the superclass
		return super.getColumnCount() + 1;
	}

	private int getAncienCodeArticleColumn()
	{
		// We don't want to hard-code any indices, so ask superclass how many columns it has defined.
		// Then we know that value will correspond to the first index superclass doesn't use, which is our one column's index.
		return super.getColumnCount();
	}

	@Override
	protected Object[] buildTableModelDataRow(Object[] resultRow, int columnCount, boolean resultsHaveNomenclatureCode)
	{
		int ancienCodeArticleColumn = getAncienCodeArticleColumn();

		Object[] dataRow = super.buildTableModelDataRow(resultRow, columnCount, resultsHaveNomenclatureCode);
		// The nomenclature code is an extra column we have to skip, so it offsets the resultRow index by 1
		if (resultsHaveNomenclatureCode)
		{
			dataRow[ancienCodeArticleColumn] = resultRow[ancienCodeArticleColumn + 1];
		}
		else
		{
			dataRow[ancienCodeArticleColumn] = resultRow[ancienCodeArticleColumn];
		}
		return dataRow;
	}

	@Override
	protected int[] buildQuickFilterPaneColumns()
	{
		// We don't want to hard-code the indices, so ask superclass how many quick filter columns it has defined.
		// Then we know that value will correspond to the first empty index in the columns array
		int parentColumnCount = super.getQuickFilterPaneColumnCount();

		int[] columns = super.buildQuickFilterPaneColumns();
		columns[parentColumnCount] = getAncienCodeArticleColumn();

		return columns;
	}

	@Override
	protected int getQuickFilterPaneColumnCount()
	{
		// We're adding one column to quick filter pane
		return super.getQuickFilterPaneColumnCount() + 1;
	}

	@Override
	protected int[] buildQuickFilterFieldColumns()
	{
		// We don't want to hard-code the indices, so ask superclass how many quick filter columns it has defined.
		// Then we know that value will correspond to the first empty index in the columns array
		int parentColumnCount = super.getQuickFilterFieldColumnCount();

		int[] columns = super.buildQuickFilterFieldColumns();
		columns[parentColumnCount] = getAncienCodeArticleColumn();

		return columns;
	}

	@Override
	protected int getQuickFilterFieldColumnCount()
	{
		// We're adding one column to quick filter field
		return super.getQuickFilterFieldColumnCount() + 1;
	}

}
