package com.mac.custom.form;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.KeyStroke;

import org.xml.sax.helpers.AttributesImpl;

import com.mac.custom.bo.MacCustomer;
import com.mac.custom.bo.MacQuotation;
import com.mac.custom.bo.MacQuotationDetailSearch;
import com.netappsid.action.AbstractAction;
import com.netappsid.application.ApplicationInjector;
import com.netappsid.bo.BusinessObjectSelector;
import com.netappsid.component.FormDialog;
import com.netappsid.component.FormDialog.ButtonType;
import com.netappsid.component.swing.FormDialogSwingUtils;
import com.netappsid.dao.utils.boquery.BOQuery;
import com.netappsid.erp.client.utils.ERPSystemVariable;
import com.netappsid.erp.server.bo.User;
import com.netappsid.framework.gui.components.SearchPanel;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;
import com.netappsid.framework.gui.components.table.SearchTable;
import com.netappsid.framework.gui.formbuilder.AbstractForm;
import com.netappsid.framework.utils.ServicesManager;
import com.netappsid.framework.utils.SystemVariable;
import com.netappsid.utils.ComponentUtils;

public class MacQuotationDetailSearchForm extends AbstractForm implements BusinessObjectSelector
{
	private MacQuotationDetailSearch macQuotationDetailSearch;
	private SearchPanel searchPanel;
	private static final String MACQUOTATIONDETAILSEARCH = "macQuotationDetailSearch";
	private static final String MACQUOTATION = "quotation";
	private JButton refreshButton;

	@Override
	public void buildPanel()
	{
		super.buildPanel();
		macQuotationDetailSearch = (MacQuotationDetailSearch) getModelBean(getModelName());
		configureSearchPanel();
		configureButtons();
	}

	@Override
	public void setParentModel(NAIDPresentationModel parentModel)
	{
		super.setParentModel(parentModel);
		setDefaultValues();
	}

	public void setDefaultValues()
	{
		if (macQuotationDetailSearch != null)
		{
			Calendar calendar = Calendar.getInstance();
			macQuotationDetailSearch.setDateTo(calendar.getTime());
			calendar.add(Calendar.MONTH, -6);
			macQuotationDetailSearch.setDateFrom(calendar.getTime());

			User user = SystemVariable.getVariable(ERPSystemVariable.USER);
			macQuotationDetailSearch.setUser(user);

			MacQuotation quotation = (MacQuotation) getParentModel().getParentModel().getBean();
			MacCustomer customer = (MacCustomer) quotation.getCustomer();
			macQuotationDetailSearch.setMacCustomer(customer);
		}
	}

	private void configureButtons()
	{
		refreshButton = getComponent("refresh");
		AbstractAction refreshAction = new AbstractAction(new AttributesImpl())
			{
				@Override
				public void doAction(ActionEvent e)
				{
					FormDialog formDialog = FormDialogSwingUtils.getFormDialog(MacQuotationDetailSearchForm.this);

					BOQuery query = ((MacQuotationDetailSearch) getModelBean(MACQUOTATIONDETAILSEARCH)).getBOQuery(getBusinessObjectClass());
					searchPanel.setCriteria(query, formDialog.getBuildedCriterions());

				}
			};

		ApplicationInjector.INSTANCE.injectMembers(refreshAction);
		ApplicationInjector.INSTANCE.injectMembers(refreshButton);
		ApplicationInjector.INSTANCE.injectMembers(this);

		Icon icon = refreshButton.getIcon();
		String text = refreshButton.getText();

		refreshAction.activateDefaultProgressDialog();
		refreshButton.setAction(refreshAction);
		refreshButton.setIcon(icon);
		refreshButton.setText(text);
		ComponentUtils.addKeyPressAction(getIntelligentPanel(), KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0, false), refreshAction);

		JButton clearButton = getComponent("clear");

		clearButton.addActionListener(new ActionListener()
			{
				@Override
				public void actionPerformed(ActionEvent e)
				{
					((MacQuotationDetailSearch) getCurrentBean()).clearSearchParameters();
				}
			});
	}

	private void configureSearchPanel()
	{
		searchPanel = getComponent("grid_macQuotationDetails");

		// Ensure to add double-click support on the SearchTable
		// of the SearchPanel and not the SearchPanel itself
		SearchTable searchTable = searchPanel.getSearchTable();

		if (searchTable != null)
		{
			searchTable.addMouseListener(new MouseAdapter()
				{
					@Override
					public void mouseClicked(MouseEvent e)
					{
						if (e.getClickCount() >= 2)
						{
							FormDialog formDialog = FormDialogSwingUtils.getFormDialog(getIntelligentPanel());

							formDialog.setSelectedButton(ButtonType.OK);
							formDialog.setVisible(false);
						}
					}
				});
		}
	}

	@Override
	public Class<?> getBusinessObjectClass()
	{
		return searchPanel.getSearchTable().getBusinessObjectClass();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Object> getBusinessObjects()
	{
		List<Object> businessObjects = (List<Object>) ServicesManager.getLoaderServices().findByIdCollection(getBusinessObjectClass(),
				getParentModel().getParentModel().getBeanClass(), getBusinessObjectsID());

		return businessObjects;
	}

	@Override
	public List<Serializable> getBusinessObjectsID()
	{
		return searchPanel.getSearchTable().getBusinessObjectsID();
	}

}
