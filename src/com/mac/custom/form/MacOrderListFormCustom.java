package com.mac.custom.form;

import resources.forms.classes.OrderListForm;

import com.jidesoft.grid.CellStyleTable;
import com.mac.custom.table.renderer.MacOrderListTableCellRenderer;
import com.netappsid.framework.gui.components.SearchPanel;

public class MacOrderListFormCustom extends OrderListForm
{
	@Override
	public void buildPanel()
	{
		setListModelName("macOrderList");
		super.buildPanel();
		initializeCellStyleProvider("grid_orderDetails");

	}

	protected void initializeCellStyleProvider(String tableName)
	{
		SearchPanel searchPanel = (SearchPanel) getComponent(tableName);
		CellStyleTable frameworkTable = searchPanel.getSearchTable();
		frameworkTable.setCellStyleProvider(new MacOrderListTableCellRenderer(getClass().getSimpleName()));
	}
}
