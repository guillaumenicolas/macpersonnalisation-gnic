package com.mac.custom.form;

import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JRadioButton;

import com.mac.custom.bo.MacCommercialClass;
import com.mac.custom.bo.MacCustomer;
import com.mac.custom.bo.MacTerm;
import com.mac.custom.enums.TypeClientEnum;
import com.netappsid.action.Cancel;
import com.netappsid.action.Delete;
import com.netappsid.action.New;
import com.netappsid.action.adapter.ActionNotifyAdapter;
import com.netappsid.action.event.ActionNotifyEvent;
import com.netappsid.erp.server.bo.Customer;
import com.netappsid.europe.naid.custom.form.CustomerFormCustom;
import com.netappsid.utils.ComponentUtils;

public class PackagesFormCustom extends CustomerFormCustom
{
	private static final String CUSTOMER = "customer";
	private JRadioButton optCrmaValider;
	private JRadioButton optProspect;
	private JRadioButton optValidationFinance;
	private JRadioButton optClient;
	private JRadioButton optDivers;
	private Component afficherMontantFacturees;
	private final PropertyChangeListener typologieChangeHandler = new TypologieChangeHandler();
	private final PropertyChangeListener termChangeHandler = new TermChangeHandler();

	@Override
	public void buildPanel()
	{
		super.buildPanel();

		New actionNew = getAction("new");
		Delete actionDelete = getAction("delete");
		Cancel actionCancel = getAction("cancel");

		afficherMontantFacturees = getComponent("afficherMontantFacturees");

		optCrmaValider = getComponent("optCrmaValider");
		optProspect = getComponent("optProspect");
		optValidationFinance = getComponent("optValidationFinance");
		optClient = getComponent("optClient");
		optDivers = getComponent("optDivers");

		ActionNotifyAdapter setDefaultValuesListener = new ActionNotifyAdapter()
			{
				@Override
				public void after(ActionNotifyEvent event)
				{
					setDefaultValues();
				}
			};

		if (actionNew != null)
		{
			actionNew.addActionNotifyListener(setDefaultValuesListener);
		}

		if (actionDelete != null)
		{
			actionDelete.addActionNotifyListener(setDefaultValuesListener);
		}

		if (actionCancel != null)
		{
			actionCancel.addActionNotifyListener(setDefaultValuesListener);
		}

		addActionListener(optCrmaValider, "initializeCrmaValider");
		addActionListener(optProspect, "initializeProspect");
		addActionListener(optValidationFinance, "initializeValidationFinance");
		addActionListener(optClient, "initializeClient");
		addActionListener(optDivers, "initializeDivers");

		setDefaultValues();

		initializeCustomerListeners();
	}

	@Override
	protected void afterBeanChanged()
	{
		super.afterBeanChanged();
		setTypeClientRadioButtons();
		toggleAfficherMontantFactureesButton();
		initializeCustomerListeners();
	}

	@Override
	protected void beforeBeanChanged()
	{
		super.beforeBeanChanged();
		removeCustomerListeners();
	}

	private void initializeCustomerListeners()
	{
		getModel(getModelName()).getModel(Customer.PROPERTYNAME_CUSTOMERCLASS).addValueChangeListener(typologieChangeHandler);
		getModel(getModelName()).getModel(Customer.PROPERTYNAME_TERM).addValueChangeListener(termChangeHandler);
	}

	private void removeCustomerListeners()
	{
		getModel(getModelName()).getModel(Customer.PROPERTYNAME_CUSTOMERCLASS).removeValueChangeListener(typologieChangeHandler);
		getModel(getModelName()).getModel(Customer.PROPERTYNAME_TERM).removeValueChangeListener(termChangeHandler);
	}

	@Override
	protected void beanChanged()
	{
		super.beanChanged();
	}

	public void initializeCrmaValider()
	{
		MacCustomer customer = getModelBean(CUSTOMER);

		customer.setTypeClient(TypeClientEnum.CRMAVALIDER);

		getModel(CUSTOMER).setBean(customer);
		getModel(CUSTOMER).setDirty(true);
	}

	public void initializeProspect()
	{
		MacCustomer customer = getModelBean(CUSTOMER);

		customer.setTypeClient(TypeClientEnum.PROSPECT);

		getModel(CUSTOMER).setBean(customer);
		getModel(CUSTOMER).setDirty(true);
	}

	public void initializeValidationFinance()
	{
		MacCustomer customer = getModelBean(CUSTOMER);

		customer.setTypeClient(TypeClientEnum.VALIDATIONFINANCE);

		getModel(CUSTOMER).setBean(customer);
		getModel(CUSTOMER).setDirty(true);
	}

	public void initializeClient()
	{
		MacCustomer customer = getModelBean(CUSTOMER);

		customer.setTypeClient(TypeClientEnum.CLIENT);

		getModel(CUSTOMER).setBean(customer);
		getModel(CUSTOMER).setDirty(true);
	}

	public void initializeDivers()
	{
		MacCustomer customer = getModelBean(CUSTOMER);

		customer.setTypeClient(TypeClientEnum.DIVERS);

		getModel(CUSTOMER).setBean(customer);
		getModel(CUSTOMER).setDirty(true);
	}

	public void setTypeClientRadioButtons()
	{
		MacCustomer customer = (MacCustomer) getModelBean(CUSTOMER);

		if (customer != null)
		{
			if (customer.getId() != null)
			{
				if (customer.getTypeClient().equals(TypeClientEnum.CRMAVALIDER))
				{
					optCrmaValider.setSelected(true);
				}
				else if (customer.getTypeClient().equals(TypeClientEnum.PROSPECT))
				{
					optProspect.setSelected(true);
				}
				else if (customer.getTypeClient().equals(TypeClientEnum.VALIDATIONFINANCE))
				{
					optValidationFinance.setSelected(true);
				}
				else if (customer.getTypeClient().equals(TypeClientEnum.CLIENT))
				{
					optClient.setSelected(true);
				}
				else if (customer.getTypeClient().equals(TypeClientEnum.DIVERS))
				{
					optDivers.setSelected(true);
				}
			}
		}

		getModel(CUSTOMER).setDirty(false);
	}

	public void setDefaultSelected()
	{
		MacCustomer customer = (MacCustomer) getModelBean(CUSTOMER);

		optProspect.setSelected(true);
		customer.setTypeClient(TypeClientEnum.PROSPECT);
	}

	public void setDefaultValues()
	{
		MacCustomer customer = (MacCustomer) getModelBean(CUSTOMER);

		if (customer != null && customer.getId() == null && customer.isReadyForModifications())
		{
			optProspect.setSelected(true);
			customer.setTypeClient(TypeClientEnum.PROSPECT);

			customer.setDirty(false);
		}
	}

	protected void toggleAfficherMontantFactureesButton()
	{
		MacCustomer customer = (MacCustomer) getModelBean(CUSTOMER);

		if (customer != null)
		{
			ComponentUtils.setComponentHierarchyEnabled(afficherMontantFacturees, (customer.getId() != null));
		}
	}

	private final class TypologieChangeHandler implements PropertyChangeListener
	{
		@Override
		public void propertyChange(PropertyChangeEvent evt)
		{
			MacCommercialClass oldValue = (MacCommercialClass) evt.getOldValue();
			MacCommercialClass newValue = (MacCommercialClass) evt.getNewValue();

			if (evt.getOldValue() != null && oldValue != newValue)
			{
				MacCustomer customer = (MacCustomer) getModel(getModelName()).getBean();
				customer.setActiviteClient(null);
				customer.setEnseigneClient(null);
			}
		}
	}

	private final class TermChangeHandler implements PropertyChangeListener
	{
		@Override
		public void propertyChange(PropertyChangeEvent evt)
		{
			MacTerm term = (MacTerm) evt.getNewValue();

			MacCustomer customer = (MacCustomer) getModel(getModelName()).getBean();
			if (customer.getPaymentMode() == null && term.getPaymentMode() != null)
			{
				customer.setPaymentMode(term.getPaymentMode());
			}
		}
	}
}
