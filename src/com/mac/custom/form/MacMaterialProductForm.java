package com.mac.custom.form;

import java.awt.Color;

import javax.swing.JTabbedPane;

import resources.forms.classes.MaterialProductForm;

import com.jidesoft.swing.JideTabbedPane;
import com.netappsid.erp.server.bo.MaterialProduct;

public class MacMaterialProductForm extends MaterialProductForm
{
	// Ajout OGA du 24/07/14 - Permet de recuperer les onglets a colorer
	private void initializeTabsColor()
	{ 
		JTabbedPane tabbedpane = (JTabbedPane) getComponent("PanelGlobal");
		if (tabbedpane != null)
		{
			JideTabbedPane parentTable = (JideTabbedPane) tabbedpane;

			// R�cup�ration des objets de order
			MaterialProduct materialProduct = getCurrentBean();

			// V�rification si onglet pi�ces jointes renseign�s auquel cas le
			// color� en ORANGE
			if (!materialProduct.getAttachments().isEmpty())
			{
				paintTab(parentTable, "tab_attachment", Color.ORANGE);
			}
			else
			{
				paintTab(parentTable, "tab_attachment", null);
			}

			// V�rification si onglet notes renseign�s auquel cas le
			// color� en ORANGE
			if (!materialProduct.getNotes().isEmpty())
			{
				paintTab(parentTable, "tab_notes", Color.ORANGE);
			}
			else
			{
				paintTab(parentTable, "tab_notes", null);
			}
		}
	}

	// Ajout OGA du 24/07/14 - Permet de colorer les onglets precedemment selectionnes
	private void paintTab(JideTabbedPane parentTable, String tabKey, Color color)
	{
		if (parentTable != null && tabKey != null)
		{
			int tabIndex = parentTable.indexOfTab(getString(tabKey));

			if (tabIndex != -1)
			{
				parentTable.setBackgroundAt(tabIndex, color);
			}
		}
	}

	@Override
	protected void afterBeanChanged()
	{
		super.afterBeanChanged();

		// Ajout OGA du 24/07/14 - Fonction pour coloriser le ou les onglet(s)
		initializeTabsColor();

	}

}
