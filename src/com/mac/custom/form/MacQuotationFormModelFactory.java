package com.mac.custom.form;

import com.netappsid.framework.gui.components.model.NAIDPresentationModel;
import com.netappsid.framework.gui.formbuilder.formmodel.FormModelFactory;

public class MacQuotationFormModelFactory extends FormModelFactory<MacQuotationFormModel>
{
	@Override
	public MacQuotationFormModel createFormModel(NAIDPresentationModel businessObjectModel)
	{
		MacQuotationFormModel macQuotationFormModel = new MacQuotationFormModel(businessObjectModel);
		return macQuotationFormModel;
	}
}