package com.mac.custom.form;

import java.util.Date;

import javax.persistence.Transient;

import com.jgoodies.binding.value.ValueModel;
import com.mac.custom.services.order.beans.MacOrderDetailServicesBean;
import com.mac.custom.services.order.interfaces.remote.MacOrderDetailServicesRemote;
import com.netappsid.annotations.DAO;
import com.netappsid.erp.server.bo.Order;
import com.netappsid.erp.server.bo.OrderDetail;
import com.netappsid.erp.server.bo.dao.OrderDetailDAO;
import com.netappsid.erp.server.model.form.LocaleEditingFormModel;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.security.permission.server.AdminService;
import com.netappsid.security.permission.server.AdminServiceBean;

@DAO(dao = OrderDetailDAO.class)
public class MacOrderDetailFormModel extends LocaleEditingFormModel
{
	private final ServiceLocator<MacOrderDetailServicesRemote> macOrderDetailServiceLocator = new ServiceLocator<MacOrderDetailServicesRemote>(
			MacOrderDetailServicesBean.class);

	private Date expectedProductionDate;
	private Date explosionLaunchDate;
	private Date productionEndDate;
	private Date realShippingDate;
	private Date realDeliveryDate;
	private Date realBillingDate;

	public MacOrderDetailFormModel(NAIDPresentationModel businessObjectModel)
	{
		this(businessObjectModel, new ServiceLocator<AdminService>(AdminServiceBean.class));
	}

	public MacOrderDetailFormModel(NAIDPresentationModel businessObjectModel, ValueModel localeValueModel)
	{
		this(businessObjectModel, localeValueModel, new ServiceLocator<AdminService>(AdminServiceBean.class));
	}

	protected MacOrderDetailFormModel(NAIDPresentationModel businessObjectModel, ServiceLocator<AdminService> adminServicesSserviceLocator)
	{
		this(businessObjectModel, businessObjectModel.getModel(Order.PROPERTYNAME_LOCALE), adminServicesSserviceLocator);
	}

	public MacOrderDetailFormModel(NAIDPresentationModel businessObjectModel, ValueModel localeValueModel,
			ServiceLocator<AdminService> adminServicesSserviceLocator)
	{
		super(businessObjectModel, localeValueModel, adminServicesSserviceLocator);
	}

	public void setExpectedProductionDate(Date expectedProductionDate)
	{
		Date oldValue = getExpectedProductionDate();
		this.expectedProductionDate = expectedProductionDate == null ? null : new Date(expectedProductionDate.getTime());
		firePropertyChange(PROPERTYNAME_EXPECTEDPRODUCTIONDATE, oldValue, expectedProductionDate);
	}

	@Transient
	public Date getExpectedProductionDate()
	{
		return expectedProductionDate == null ? null : new Date(expectedProductionDate.getTime());
	}

	public void setExplosionLaunchDate(Date explosionLaunchDate)
	{
		Date oldValue = getExplosionLaunchDate();
		this.explosionLaunchDate = explosionLaunchDate == null ? null : new Date(explosionLaunchDate.getTime());
		firePropertyChange(PROPERTYNAME_EXPLOSIONLAUNCHDATE, oldValue, explosionLaunchDate);
	}

	@Transient
	public Date getExplosionLaunchDate()
	{
		return explosionLaunchDate == null ? null : new Date(explosionLaunchDate.getTime());
	}

	public void setProductionEndDate(Date productionEndDate)
	{
		Date oldValue = getProductionEndDate();
		this.productionEndDate = productionEndDate == null ? null : new Date(productionEndDate.getTime());
		firePropertyChange(PROPERTYNAME_PRODUCTIONENDDATE, oldValue, productionEndDate);
	}

	@Transient
	public Date getProductionEndDate()
	{
		return productionEndDate == null ? null : new Date(productionEndDate.getTime());
	}

	public void setRealShippingDate(Date realShippingDate)
	{
		Date oldValue = getRealShippingDate();
		this.realShippingDate = realShippingDate == null ? null : new Date(realShippingDate.getTime());
		firePropertyChange(PROPERTYNAME_REALSHIPPINGDATE, oldValue, realShippingDate);
	}

	@Transient
	public Date getRealShippingDate()
	{
		return realShippingDate == null ? null : new Date(realShippingDate.getTime());
	}

	public void setRealDeliveryDate(Date realDeliveryDate)
	{
		Date oldValue = getRealDeliveryDate();
		this.realDeliveryDate = realDeliveryDate == null ? null : new Date(realDeliveryDate.getTime());
		firePropertyChange(PROPERTYNAME_REALDELIVERYDATE, oldValue, realDeliveryDate);
	}

	@Transient
	public Date getRealDeliveryDate()
	{
		return realDeliveryDate == null ? null : new Date(realDeliveryDate.getTime());
	}

	public void setRealBillingDate(Date realBillingDate)
	{
		Date oldValue = getRealBillingDate();
		this.realBillingDate = realBillingDate == null ? null : new Date(realBillingDate.getTime());
		firePropertyChange(PROPERTYNAME_REALBILLINGDATE, oldValue, realBillingDate);
	}

	@Transient
	public Date getRealBillingDate()
	{
		return realBillingDate == null ? null : new Date(realBillingDate.getTime());
	}

	public void refreshDates()
	{
		OrderDetail orderDetail = (OrderDetail) getBean();
		if (orderDetail != null && orderDetail.getId() != null)
		{
			setRealBillingDate(macOrderDetailServiceLocator.get().getRealBillingDate(orderDetail.getId()));
			setRealDeliveryDate(macOrderDetailServiceLocator.get().getRealDeliveryDate(orderDetail.getId()));
			setRealShippingDate(macOrderDetailServiceLocator.get().getRealShippingDate(orderDetail.getId()));
			setProductionEndDate(macOrderDetailServiceLocator.get().getProductionEndDate(orderDetail.getId()));
			setExplosionLaunchDate(macOrderDetailServiceLocator.get().getExplosionLaunchDate(orderDetail.getId()));
			setExpectedProductionDate(macOrderDetailServiceLocator.get().getExpectedProductionDate(orderDetail.getId()));
		}
		else
		{
			setRealBillingDate(null);
			setRealDeliveryDate(null);
			setRealShippingDate(null);
			setProductionEndDate(null);
			setExplosionLaunchDate(null);
			setExpectedProductionDate(null);
		}
	}

	public static final String PROPERTYNAME_REALBILLINGDATE = "realBillingDate";
	public static final String PROPERTYNAME_REALDELIVERYDATE = "realDeliveryDate";
	public static final String PROPERTYNAME_REALSHIPPINGDATE = "realShippingDate";
	public static final String PROPERTYNAME_PRODUCTIONENDDATE = "productionEndDate";
	public static final String PROPERTYNAME_EXPLOSIONLAUNCHDATE = "explosionLaunchDate";
	public static final String PROPERTYNAME_EXPECTEDPRODUCTIONDATE = "expectedProductionDate";
}
