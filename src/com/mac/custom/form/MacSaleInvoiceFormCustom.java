package com.mac.custom.form;

import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import resources.forms.classes.SaleInvoiceForm;

import com.mac.custom.bo.MacSaleInvoice;
import com.mac.custom.enums.SaleTypeEnum;
import com.netappsid.bo.model.Model;
import com.netappsid.erp.server.bo.SaleType;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;
import com.netappsid.utils.ComponentUtils;

public class MacSaleInvoiceFormCustom extends SaleInvoiceForm implements ChangeListener
{
	private final PropertyChangeListener saleTypeChangeHandler;
	private static final String SERVICETYPE_COMPONENT_NAME = "serviceType";
	private static final String ORIGINALORDERNUMBER_COMPONENT_NAME = "originalOrderNumber";

	public MacSaleInvoiceFormCustom()
	{
		saleTypeChangeHandler = new SaleTypeChangeHandler(this);
	}

	@Override
	public void buildPanel()
	{
		super.buildPanel();
		enableComponents(false);
		initializeBeanListeners();
	}

	@Override
	protected void afterBeanChanged()
	{
		super.afterBeanChanged();
		MacSaleInvoice macSaleInvoice = Model.getTarget((MacSaleInvoice) getCurrentBean());
		enableComponents(macSaleInvoice.getSaleType());
		initializeBeanListeners();
	}

	@Override
	protected void beforeBeanChanged()
	{
		super.beforeBeanChanged();
		removeBeanListeners();
	}

	private void initializeBeanListeners()
	{
		NAIDPresentationModel model = getModel(getModelName());

		model.getModel(MacSaleInvoice.PROPERTYNAME_SALETYPE).addValueChangeListener(saleTypeChangeHandler);
	}

	private void removeBeanListeners()
	{
		NAIDPresentationModel model = getModel(getModelName());

		model.getModel(MacSaleInvoice.PROPERTYNAME_SALETYPE).removeValueChangeListener(saleTypeChangeHandler);
	}

	protected void enableComponents(boolean enable)
	{
		setComponentEnable(SERVICETYPE_COMPONENT_NAME, enable);
		setComponentEnable(ORIGINALORDERNUMBER_COMPONENT_NAME, enable);
	}

	private void setComponentEnable(String componentName, boolean enable)
	{
		if (componentName != null && !componentName.equals(""))
		{
			Component component = getComponent(componentName);
			if (component != null)
			{
				ComponentUtils.setComponentHierarchyEnabled(component, enable);
			}
		}
	}

	private final class SaleTypeChangeHandler implements PropertyChangeListener
	{
		private final MacSaleInvoiceFormCustom saleInvoiceForm;

		public SaleTypeChangeHandler(MacSaleInvoiceFormCustom saleInvoiceForm)
		{
			this.saleInvoiceForm = saleInvoiceForm;
		}

		@Override
		public void propertyChange(PropertyChangeEvent evt)
		{
			SaleType newValue = (SaleType) evt.getNewValue();
			enableComponents(newValue);
		}
	}

	private void enableComponents(SaleType saleType)
	{
		MacSaleInvoice saleInvoice = Model.getTarget(getCurrentBean());
		if (saleType != null && saleType.getCode() != null)
		{
			if (saleType.getUpperCode().equals(SaleTypeEnum.sav.getUpperCode()))
			{
				enableComponents(true);
			}
			else
			{
				enableComponents(false);
				saleInvoice.setServiceType(null);
				saleInvoice.setOriginalOrderNumber("");
			}
		}
		else
		{
			enableComponents(false);
			saleInvoice.setServiceType(null);
			saleInvoice.setOriginalOrderNumber("");
		}
	}

	@Override
	public void stateChanged(ChangeEvent e)
	{}
}
