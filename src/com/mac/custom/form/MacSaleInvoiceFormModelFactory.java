package com.mac.custom.form;

import com.netappsid.framework.gui.components.model.NAIDPresentationModel;
import com.netappsid.framework.gui.formbuilder.formmodel.FormModelFactory;

public class MacSaleInvoiceFormModelFactory extends FormModelFactory<MacSaleInvoiceFormModel>
{
	@Override
	public MacSaleInvoiceFormModel createFormModel(NAIDPresentationModel businessObjectModel)
	{
		MacSaleInvoiceFormModel macSaleInvoiceFormModel = new MacSaleInvoiceFormModel(businessObjectModel);
		return macSaleInvoiceFormModel;
	}
}