package com.mac.custom.form;

import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.mac.custom.bo.MacQuotationDetail;
import com.mac.custom.utils.RareBirdUtils;
import com.netappsid.erp.client.utils.ERPSystemVariable;
import com.netappsid.erp.server.bo.User;
import com.netappsid.europe.naid.custom.form.QuotationDetailEuroForm;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.framework.utils.SystemVariable;
import com.netappsid.security.auth.server.SecurityUser;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.Loader;
import com.netappsid.utils.ComponentUtils;

public class MacQuotationDetailFormCustom extends QuotationDetailEuroForm
{
	public static final String SECURITYGROUP_ADMINISTRATORS = "ADMINISTRATORS";

	private final ServiceLocator<Loader> loaderLocator = new ServiceLocator<Loader>(LoaderBean.class);

	private User user;
	private SecurityUser securityUser;

	private Component rareBirdAccepted;
	private Component rareBirdConfirmationNumber;
	private Component rareBirdDescription;
	private Component rareBirdDesignation;
	private Component rareBirdProcessStartDate;
	private Component rareBirdProcessedDate;
	private Component modifyAddress;
	private Component searchAddress;
	private Component addAddress;

	private final PropertyChangeListener rareBirdChangeHandler = new RareBirdChangeHandler();

	@Override
	public void buildPanel()
	{
		super.buildPanel();

		rareBirdAccepted = getComponent("rareBirdAccepted");
		rareBirdConfirmationNumber = getComponent("rareBirdConfirmationNumber");
		rareBirdDescription = getComponent("rareBirdDescription");
		rareBirdDesignation = getComponent("rareBirdDesignation");
		rareBirdProcessStartDate = getComponent("rareBirdProcessStartDate");
		rareBirdProcessedDate = getComponent("rareBirdProcessedDate");
		modifyAddress = getComponent("modifyAddress");
		searchAddress = getComponent("searchAddress");
		addAddress = getComponent("addAddress");

		updateTauxDeRemisesEnabledState();
		updateactionaddressshipping();
	}

	/**
	 * Modification OGA le 01/10/14 Ne pas donner la possibilit� de modifier une adresse de livraison dans la modification d un element si groupe user <>
	 * ADMINISTRATOR
	 */
	private void updateactionaddressshipping()
	{
		boolean isUserAdministrator = securityUser.isMember(SECURITYGROUP_ADMINISTRATORS);

		if (modifyAddress != null)
		{
			modifyAddress.setEnabled(isUserAdministrator);
		}

		if (searchAddress != null)
		{
			searchAddress.setEnabled(isUserAdministrator);
		}

		if (addAddress != null)
		{
			addAddress.setEnabled(isUserAdministrator);
		}
	}

	private SecurityUser getSecurityUser()
	{
		if (user == null)
		{
			user = SystemVariable.getVariable(ERPSystemVariable.USER);
			securityUser = loaderLocator.get().findById(SecurityUser.class, SecurityUser.class, user.getSecurityUser().getId());
		}
		return securityUser;
	}

	@Override
	protected void beforeBeanChanged()
	{
		super.beforeBeanChanged();

		removeListeners();
	}

	@Override
	protected void afterBeanChanged()
	{
		super.afterBeanChanged();

		initializeListeners();
		updateRareBirdFields();
	}

	private void updateRareBirdFields()
	{
		MacQuotationDetail quotationDetail = getModelBean(getModelName());

		if (quotationDetail.isRareBird() && MacOrderDetailFormCustom.isRareBirdModifiable(getSecurityUser()))
		{
			enableRareBirdFields(true);
		}
	}

	private void initializeListeners()
	{
		NAIDPresentationModel model = getModel(getModelName());

		model.getModel(MacQuotationDetail.PROPERTYNAME_RAREBIRD).addValueChangeListener(rareBirdChangeHandler);
	}

	private void removeListeners()
	{
		NAIDPresentationModel model = getModel(getModelName());

		model.getModel(MacQuotationDetail.PROPERTYNAME_RAREBIRD).removeValueChangeListener(rareBirdChangeHandler);
	}

	private void enableRareBirdFields(boolean enable)
	{
		rareBirdAccepted.setEnabled(enable);
		rareBirdConfirmationNumber.setEnabled(enable);
		rareBirdDescription.setEnabled(enable);
		rareBirdDesignation.setEnabled(enable);
		rareBirdProcessStartDate.setEnabled(enable);
		rareBirdProcessedDate.setEnabled(enable);
	}

	private final class RareBirdChangeHandler implements PropertyChangeListener
	{
		@Override
		public void propertyChange(PropertyChangeEvent event)
		{
			MacQuotationDetail quotationDetail = getCurrentBean();
			enableRareBirdFields(quotationDetail.isRareBird());

			if (quotationDetail.isRareBird() && quotationDetail.getRareBirdProcessStartDate() == null)
			{
				quotationDetail.setRareBirdProcessStartDate(new Date());
			}

			if (quotationDetail.isRareBird())
			{
				if (StringUtils.isEmpty(quotationDetail.getRareBirdDesignation()))
				{
					quotationDetail.setRareBirdDesignation(quotationDetail.getLocalizedDescription());
				}

				if (StringUtils.isEmpty(quotationDetail.getRareBirdDescription()))
				{
					quotationDetail.setRareBirdDescription(RareBirdUtils.generateRareBirdDescription(quotationDetail.getProperties()));
				}
			}
		}
	}

	private void updateTauxDeRemisesEnabledState()
	{
		MacQuotationDetail quotationDetail = getCurrentBean();
		if (quotationDetail != null)
		{
			boolean state = isTauxRemiseModifiable(getSecurityUser());
			setEnabled("discountRate", state);
		}
	}

	public static boolean isTauxRemiseModifiable(SecurityUser securityUser)
	{
		if (securityUser == null)
		{
			return true;
		}
		else
		{
			return securityUser.isMember(SECURITYGROUP_ADMINISTRATORS);
		}
	}

	private void setEnabled(String componentName, boolean enabled)
	{
		Component component = getComponent(componentName);
		if (component != null)
		{
			ComponentUtils.setComponentHierarchyEnabled(component, enabled);
		}
	}
}
