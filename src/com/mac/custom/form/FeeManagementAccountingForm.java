package com.mac.custom.form;

import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.mac.custom.bo.FeeManagementAccounting;
import com.netappsid.erp.server.bo.Company;
import com.netappsid.framework.gui.formbuilder.AbstractForm;
import com.netappsid.utils.ComponentUtils;

public class FeeManagementAccountingForm extends AbstractForm implements ChangeListener
{
	private final PropertyChangeListener companyChangeHandler = new CompanyChangeHandler();
	private static final String COMPANY = "company";
	private static final String FEEMANAGEMENTACCOUNTING = "feeManagementAccounting";

	@Override
	public void buildPanel()
	{
		super.buildPanel();

		enableComponents(false);

		initializeFeeManagementAccountingListeners();
	}

	@Override
	protected void afterBeanChanged()
	{
		super.afterBeanChanged();
		enableComponents(true);
		initializeFeeManagementAccountingListeners();
	}

	@Override
	protected void beforeBeanChanged()
	{
		super.beforeBeanChanged();
		removeFeeManagementAccountingListeners();
	}

	private void initializeFeeManagementAccountingListeners()
	{
		getModel(getModelName()).getModel(FeeManagementAccounting.PROPERTYNAME_COMPANY).addValueChangeListener(companyChangeHandler);
	}

	private void removeFeeManagementAccountingListeners()
	{
		getModel(getModelName()).getModel(FeeManagementAccounting.PROPERTYNAME_COMPANY).removeValueChangeListener(companyChangeHandler);
	}

	protected void enableComponents(boolean enable)
	{
		FeeManagementAccounting feeManagementAccounting = getModelBean(FEEMANAGEMENTACCOUNTING);

		if (feeManagementAccounting != null)
		{
			boolean isSaved = feeManagementAccounting.getId() != null ? true : false;
			enableComponent(COMPANY, !isSaved || feeManagementAccounting.getCompany() == null);
		}
	}

	protected void enableComponent(String componentName, boolean enable)
	{
		Component component = getComponent(componentName);

		if (component != null)
		{
			ComponentUtils.setComponentHierarchyEnabled(component, enable);
		}
	}

	private final class CompanyChangeHandler implements PropertyChangeListener
	{
		@Override
		public void propertyChange(PropertyChangeEvent evt)
		{
			Company oldValue = (Company) evt.getOldValue();
			Company newValue = (Company) evt.getNewValue();

			if (evt.getOldValue() != null && oldValue != newValue)
			{
				FeeManagementAccounting feeManagementAccounting = getModelBean(FEEMANAGEMENTACCOUNTING);
				feeManagementAccounting.setCostCodeDetailPurchase(null);
				feeManagementAccounting.setCostCodeDetailSale(null);
				feeManagementAccounting.setCostCodePurchase(null);
				feeManagementAccounting.setCostCodeSale(null);
				feeManagementAccounting.setFeeCodeAnalyticPurchase(null);
				feeManagementAccounting.setFeeCodeAnalyticSale(null);
			}
		}
	}

	@Override
	public void stateChanged(ChangeEvent e)
	{}
}
