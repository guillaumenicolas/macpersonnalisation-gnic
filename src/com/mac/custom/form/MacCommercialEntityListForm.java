package com.mac.custom.form;

import com.netappsid.erp.client.gui.AbstractERPListForm;

public class MacCommercialEntityListForm extends AbstractERPListForm
{
	@Override
	public void buildPanel()
	{
		setListModelName("macCommercialEntityList");
		super.buildPanel();
	}
}