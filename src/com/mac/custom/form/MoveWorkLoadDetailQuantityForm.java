package com.mac.custom.form;

import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JCheckBox;

import com.google.common.base.Objects;
import com.mac.custom.bo.model.WorkLoadDetailQuantityMoveModel;
import com.netappsid.component.swing.NAIDComponentImpl;
import com.netappsid.framework.gui.formbuilder.AbstractForm;
import com.netappsid.utils.ComponentUtils;

public class MoveWorkLoadDetailQuantityForm extends AbstractForm
{
	public static final String WORKLOADDETAILQUANTITYMOVEMODEL = "workLoadDetailQuantityMoveModel";

	public static final String COMPONENT_SHIPPINGDATETABLE = "shippingDateTable";
	public static final String COMPONENT_WORKLOADDETAILTABLE = "workLoadDetailTable";
	public static final String COMPONENT_SHIPPINGDATE = "orderShippingDate";
	public static final String COMPONENT_AUTOSELECTSHIPPINGDATE = "autoSelectShippingDate";

	private final PropertyChangeListener moveShippingDateChangeHandler = new MoveShippingDateChangeHandler();
	private final PropertyChangeListener moveWorkLoadDetailChangeHandler = new MoveWorkLoadDetailChangeHandler();
	private final PropertyChangeListener autoSelectShippingDateChangeHandler = new AutoSelectShippingDateChangeHandler();

	private JCheckBox chkAutoSelectShippingDate;

	@Override
	public void buildPanel()
	{
		setModelName(WORKLOADDETAILQUANTITYMOVEMODEL);
		super.buildPanel();

		initializeComponents();
	}

	@Override
	protected void beforeBeanChanged()
	{
		super.beforeBeanChanged();
		removeListeners();
	}

	@Override
	protected void afterBeanChanged()
	{
		super.afterBeanChanged();
		initializeListeners();
		initializeComponents();
	}

	private void initializeComponents()
	{
		NAIDComponentImpl autoSelectShippingDateComponent = getComponent(COMPONENT_AUTOSELECTSHIPPINGDATE);
		chkAutoSelectShippingDate = (JCheckBox) ComponentUtils.getComponent(autoSelectShippingDateComponent, JCheckBox.class);

		enabledComponent(COMPONENT_SHIPPINGDATETABLE, false);
	}

	private void initializeListeners()
	{
		getModel(getModelName()).getModel(WorkLoadDetailQuantityMoveModel.PROPERTYNAME_MOVEORDERSHIPPINGDATE).addValueChangeListener(
				moveShippingDateChangeHandler);
		getModel(getModelName()).getModel(WorkLoadDetailQuantityMoveModel.PROPERTYNAME_MOVEWORKLOADDETAIL).addValueChangeListener(
				moveWorkLoadDetailChangeHandler);
		getModel(getModelName()).getModel(WorkLoadDetailQuantityMoveModel.PROPERTYNAME_AUTOSELECTSHIPPINGDATE).addValueChangeListener(
				autoSelectShippingDateChangeHandler);
	}

	private void removeListeners()
	{
		getModel(getModelName()).getModel(WorkLoadDetailQuantityMoveModel.PROPERTYNAME_MOVEORDERSHIPPINGDATE).removeValueChangeListener(
				moveShippingDateChangeHandler);
		getModel(getModelName()).getModel(WorkLoadDetailQuantityMoveModel.PROPERTYNAME_MOVEWORKLOADDETAIL).removeValueChangeListener(
				moveWorkLoadDetailChangeHandler);
		getModel(getModelName()).getModel(WorkLoadDetailQuantityMoveModel.PROPERTYNAME_AUTOSELECTSHIPPINGDATE).removeValueChangeListener(
				autoSelectShippingDateChangeHandler);
	}

	private final class AutoSelectShippingDateChangeHandler implements PropertyChangeListener
	{
		@Override
		public void propertyChange(PropertyChangeEvent evt)
		{
			final boolean newValue = (Boolean) evt.getNewValue();
			final boolean oldValue = (Boolean) evt.getOldValue();

			if (!Objects.equal(oldValue, newValue))
			{
				enabledComponent(COMPONENT_SHIPPINGDATE, !newValue);
			}
		}
	}

	private final class MoveShippingDateChangeHandler implements PropertyChangeListener
	{
		@Override
		public void propertyChange(PropertyChangeEvent evt)
		{
			final boolean newValue = (Boolean) evt.getNewValue();
			final boolean oldValue = (Boolean) evt.getOldValue();

			if (!Objects.equal(oldValue, newValue))
			{
				enabledComponent(COMPONENT_SHIPPINGDATETABLE, newValue);
				enabledComponent(COMPONENT_SHIPPINGDATE, newValue && !chkAutoSelectShippingDate.isSelected());
			}
		}
	}

	private final class MoveWorkLoadDetailChangeHandler implements PropertyChangeListener
	{
		@Override
		public void propertyChange(PropertyChangeEvent evt)
		{
			final boolean newValue = (Boolean) evt.getNewValue();
			final boolean oldValue = (Boolean) evt.getOldValue();

			if (!Objects.equal(oldValue, newValue))
			{
				enabledComponent(COMPONENT_WORKLOADDETAILTABLE, newValue);
			}
		}
	}

	protected void enabledComponent(String componentName, boolean enabled)
	{
		Component component = getComponent(componentName);
		if (component != null)
		{
			ComponentUtils.setComponentHierarchyEnabled(component, enabled);
		}
	}
}
