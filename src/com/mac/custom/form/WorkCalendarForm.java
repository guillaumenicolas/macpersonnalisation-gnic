package com.mac.custom.form;

import com.jidesoft.grid.CellStyleTable;
import com.mac.custom.listmodel.WorkCalendar;
import com.mac.custom.table.renderer.WorkLoadDetailCellRenderer;
import com.netappsid.action.AbstractBoundAction;
import com.netappsid.action.adapter.ActionNotifyAdapter;
import com.netappsid.action.event.ActionNotifyEvent;
import com.netappsid.component.OptionPane.MessageType;
import com.netappsid.erp.client.gui.AbstractERPListForm;
import com.netappsid.erp.server.services.beans.ResourceEfficiencyServicesBean;
import com.netappsid.erp.server.services.interfaces.ResourceEfficiencyServices;
import com.netappsid.framework.gui.components.TablePanel;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.undoredo.UndoRedoCollectionValueModel;

public class WorkCalendarForm extends AbstractERPListForm
{
	private TablePanel workLoadTablePanel;

	private AbstractBoundAction moveWorkLoadDetailQuantity;
	private AbstractBoundAction modifyDetail;
	private AbstractBoundAction clearWorkCalendarDetailLine;

	private final ServiceLocator<ResourceEfficiencyServices> resourceEfficiencyServicesLocator = new ServiceLocator<ResourceEfficiencyServices>(
			ResourceEfficiencyServicesBean.class);

	@Override
	public void buildPanel()
	{
		setListModelName("workCalendar");
		super.buildPanel();

		workLoadTablePanel = getComponent("grid_workCalendarDetails");

		moveWorkLoadDetailQuantity = getAction("moveWorkLoadDetailQuantity");
		modifyDetail = getAction("modifyDetail");
		clearWorkCalendarDetailLine = getAction("clearWorkCalendarDetailLine");

		AutoRefreshHandler autoRefreshHandler = new AutoRefreshHandler(this);
		moveWorkLoadDetailQuantity.addActionNotifyListener(autoRefreshHandler);
		modifyDetail.addActionNotifyListener(autoRefreshHandler);
		clearWorkCalendarDetailLine.addActionNotifyListener(autoRefreshHandler);

		initializeCellStyleProvider("grid_workCalendarDetailLoad");

		getRefreshAction().addActionNotifyListener(new RefreshWithSameSelectionHandler(this));

		UndoRedoCollectionValueModel undoRedoModel = (UndoRedoCollectionValueModel) getPresentationModel("workCalendar").getCollectionValueModel(
				"workCalendarDetails");
		undoRedoModel.disableCollectionTracking();
		undoRedoModel = (UndoRedoCollectionValueModel) getPresentationModel("workCalendar").getCollectionValueModel("workCalendarDetails.workLoad.details");
		undoRedoModel.disableCollectionTracking();
	}

	private void initializeCellStyleProvider(String tableName)
	{
		TablePanel tablePanel = ((TablePanel) getComponent(tableName));
		if (tablePanel != null)
		{
			CellStyleTable frameworkTable = (CellStyleTable) tablePanel.getScrollPane().getViewport().getComponent(0);
			frameworkTable.setCellStyleProvider(new WorkLoadDetailCellRenderer());
		}
	}

	/**
	 * Handler used to refresh the form after moving a work load detail.
	 */
	private static class AutoRefreshHandler extends ActionNotifyAdapter
	{
		private final WorkCalendarForm form;

		public AutoRefreshHandler(WorkCalendarForm workCalendarForm)
		{
			this.form = workCalendarForm;
		}

		@Override
		public void after(ActionNotifyEvent event)
		{
			form.getRefresh().doClick();
		}
	}

	/**
	 * Refreshes the form while keeping the same selection in the work calendar detail grid. Note that it's possible that after refreshing the grid, the
	 * returned data might be slightly different... in which case a wrong line could be selected in the grid since it is based on index, not on the actual data.
	 * This is a rare situation though, and it has not been judged a big problem.
	 */
	private static class RefreshWithSameSelectionHandler extends ActionNotifyAdapter
	{
		private final WorkCalendarForm form;
		private int lastWorkLoadTableSelectedRow;

		public RefreshWithSameSelectionHandler(WorkCalendarForm workCalendarForm)
		{
			this.form = workCalendarForm;
		}

		@Override
		public boolean before(ActionNotifyEvent event)
		{
			WorkCalendar workCalendar = form.getCurrentBean();

			if (workCalendar.getDateFrom() == null || workCalendar.getDateTo() == null)
			{
				form.getOptionPane().show(form.getFormComponent(), form.getString("searchDatesAreMandatory"), form.getString("Refresh"), MessageType.WARNING);
				return false;
			}
			else
			{
				lastWorkLoadTableSelectedRow = form.workLoadTablePanel.getTable().getSelectedRow();
				return true;
			}
		}

		@Override
		public void after(ActionNotifyEvent event)
		{
			if (lastWorkLoadTableSelectedRow >= 0)
			{
				form.workLoadTablePanel.getTable().getSelectionModel().setSelectionInterval(lastWorkLoadTableSelectedRow, lastWorkLoadTableSelectedRow);
			}
		}
	}
}
