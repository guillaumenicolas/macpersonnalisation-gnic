package com.mac.custom.form;

import java.awt.Component;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Set;

import resources.forms.classes.PurchaseInvoiceForm;

import com.google.common.collect.Sets;
import com.mac.custom.services.receiving.bean.MacReceivingServicesBean;
import com.mac.custom.services.receiving.interfaces.MacReceivingServices;
import com.netappsid.action.adapter.ActionNotifyAdapter;
import com.netappsid.action.event.ActionNotifyEvent;
import com.netappsid.erp.client.action.generalledgertransaction.SavePurchaseInvoice;
import com.netappsid.erp.client.action.purchaseinvoice.SaveAndReloadPurchaseInvoice;
import com.netappsid.erp.server.bo.DocumentStatus;
import com.netappsid.erp.server.bo.PurchaseInvoice;
import com.netappsid.erp.server.bo.PurchaseInvoiceDetail;
import com.netappsid.erp.server.bo.Receiving;
import com.netappsid.erp.server.bo.User;
import com.netappsid.erp.server.services.beans.DocumentStatusServicesBean;
import com.netappsid.erp.server.services.interfaces.DocumentStatusServices;
import com.netappsid.erp.server.services.utils.SetupUtils;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.utils.ComponentUtils;

public class MacPurchaseInvoiceForm extends PurchaseInvoiceForm
{
	private final ServiceLocator<DocumentStatusServices> documentStatusServicesLocator = new ServiceLocator<DocumentStatusServices>(
			DocumentStatusServicesBean.class);
	private final ServiceLocator<MacReceivingServices> macReceivingServicesLocator = new ServiceLocator<MacReceivingServices>(MacReceivingServicesBean.class);

	private final String SAVE_AND_RELOAD_PURCHASEINVOICE = "saveAndReloadPurchaseInvoice";
	private final String SAVE_PURCHASEINVOICE = "savePurchaseInvoice";
	private Component modifySupplier;

	@Override
	public void buildPanel()
	{
		super.buildPanel();

		ActionNotifyAdapter saveReceivingNotifier = new ActionNotifyAdapter()
			{
				@Override
				public void after(ActionNotifyEvent event)
				{
					closeReceivings();
				}
			};

		SaveAndReloadPurchaseInvoice saveAndReloadPurchaseInvoice = getAction(SAVE_AND_RELOAD_PURCHASEINVOICE);
		saveAndReloadPurchaseInvoice.addActionNotifyListener(saveReceivingNotifier);

		SavePurchaseInvoice savePurchaseInvoice = getAction(SAVE_PURCHASEINVOICE);
		savePurchaseInvoice.addActionNotifyListener(saveReceivingNotifier);

		modifySupplier = getComponent("modifysupplier");
	}

	private void closeReceivings()
	{
		PurchaseInvoice purchaseInvoice = getModelBean(getModelName());

		Set<Serializable> receivingIds = Sets.newHashSet();
		for (PurchaseInvoiceDetail purchaseInvoiceDetail : purchaseInvoice.getDetails())
		{
			if (purchaseInvoiceDetail.getReceivingDetail() != null)
			{
				receivingIds.add(purchaseInvoiceDetail.getReceivingDetail().getReceiving().getId());
			}
		}

		// performance note: could be improved by doing a single server call instead of a loop, but the number of receivings should always be small anyway
		for (Serializable receivingId : receivingIds)
		{
			if (macReceivingServicesLocator.get().isReceivingInvoiced(receivingId))
			{
				User user = SetupUtils.INSTANCE.getMainSetup().getAutomaticServiceUser();
				documentStatusServicesLocator.get().setDocumentStatus(Arrays.asList(receivingId), Receiving.class, DocumentStatus.CLOSE,
						user != null ? user.getId() : null);
			}
		}
	}

	@Override
	protected void afterBeanChanged()
	{
		super.afterBeanChanged();

		if (modifySupplier != null)
		{
			ComponentUtils.setComponentHierarchyEnabled(modifySupplier, true);
		}
	}
}
