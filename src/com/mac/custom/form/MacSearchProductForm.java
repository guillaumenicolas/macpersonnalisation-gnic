package com.mac.custom.form;

import java.util.List;
import java.util.Locale;

import com.netappsid.erp.client.gui.DefaultProductFormPanel;
import com.netappsid.erp.client.gui.ProductSupplierFormPanel;
import com.netappsid.erp.client.gui.SearchProductForm;
import com.netappsid.erp.client.utils.ConfigurationSearchForm;
import com.netappsid.erp.server.bo.CommercialEntity;
import com.netappsid.erp.server.bo.Company;
import com.netappsid.erp.server.bo.Document;
import com.netappsid.erp.server.bo.Product;

/**
 * A custom Mac class that extends {@link SearchProductForm}, so that its {@link DefaultProductFormPanel} can have custom mac behavior.
 * 
 * @author ftaillefer
 * 
 * @param <T>
 */
public class MacSearchProductForm<T extends CommercialEntity> extends SearchProductForm<T>
{

	public MacSearchProductForm(List<ConfigurationSearchForm> configurationSearchForms, T commercialEntity, Company company, boolean includeDefaultSearchForm,
			Locale locale, List<Class<? extends Product>> productClasses, Document document)
	{
		super(configurationSearchForms, commercialEntity, company, includeDefaultSearchForm, locale, productClasses, document);
	}

	@Override
	protected DefaultProductFormPanel<CommercialEntity> buildDefaultProductFormPanel(T commercialEntity, Company company, Locale locale,
			List<Class<? extends Product>> productClasses, Document document)
	{
		// Create and return the custom mac version
		return new MacDefaultProductFormPanel<CommercialEntity>(commercialEntity, company, locale, productClasses, document);
	}

	@Override
	protected ProductSupplierFormPanel<CommercialEntity> buildProductSupplierFormPanel(T commercialEntity, Company company, Locale locale,
			List<Class<? extends Product>> productClasses, Document document)
	{
		// Create and return the custom mac version
		return new MacProductSupplierFormPanel<CommercialEntity>(commercialEntity, company, locale, productClasses, document);
	};
}
