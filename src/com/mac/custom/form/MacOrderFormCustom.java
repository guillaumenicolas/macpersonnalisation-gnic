package com.mac.custom.form;

import java.awt.Color;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import com.jidesoft.swing.JideTabbedPane;
import com.mac.custom.bo.MacOrder;
import com.mac.custom.bo.MacOrderDetail;
import com.mac.custom.enums.SaleTypeEnum;
import com.mac.custom.enums.TypeClientEnum;
import com.netappsid.action.AbstractBoundAction;
import com.netappsid.action.adapter.ActionNotifyAdapter;
import com.netappsid.action.event.ActionNotifyEvent;
import com.netappsid.bo.model.Model;
import com.netappsid.datatypes.MonetaryAmount;
import com.netappsid.erp.server.bo.OrderDetail;
import com.netappsid.erp.server.bo.SaleTransactionOrigin;
import com.netappsid.erp.server.bo.SaleType;
import com.netappsid.erp.server.model.form.OrderFormModel;
import com.netappsid.europe.naid.custom.form.OrderFormEuro;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.security.permission.client.SecurityPermissionManager;
import com.netappsid.security.permission.factory.SecurityPermissionManagerFactory;
import com.netappsid.security.permission.server.PermissionType;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.Loader;
import com.netappsid.utils.ComponentUtils;

public class MacOrderFormCustom extends OrderFormEuro
{
	/*
	 * Patch time! The security module currently only supports security forms that are present in modules.xml, not sub-forms... however we need to apply the
	 * order's security to orderDetail for the date components. Solution? The order's security keys (based on the XML IDs) are hard-coded here, and are used to
	 * replicate the security to orderDetail.
	 */
	public static final String SECURITYKEY_OPTDATESPECIFIC = "orders:order:7239cbf976de4007:220856a1982044f4:ac78d98a8f54787:237094b6cf14787:5c34db99f5945aa:366eeb0bd8004d29";
	public static final String SECURITYKEY_OPTDATEASAP = "orders:order:7239cbf976de4007:220856a1982044f4:ac78d98a8f54787:237094b6cf14787:5c34db99f5945aa:74ca3603ab7c462b";
	public static final String SECURITYKEY_OPTDATETODETERMINE = "orders:order:7239cbf976de4007:220856a1982044f4:ac78d98a8f54787:237094b6cf14787:5c34db99f5945aa:bdec5bd761f24bb0";
	public static final String SECURITYKEY_SHIPPINGDATE = "orders:order:7239cbf976de4007:220856a1982044f4:ac78d98a8f54787:237094b6cf14787:18dc7f51da73412f:ac5e38429bc34877";

	// security key required to add conditional security on the address panels
	public static final String SECURITYKEY_ADDRESSSELECTIONSECURITYCOMPONENT = "orders:order:7239cbf976de4007:220856a1982044f4:ac78d98a8f54787:3ff4831d7fe24903:ace55930eb2e43a8";

	private final ServiceLocator<Loader> loaderLocator = new ServiceLocator<Loader>(LoaderBean.class);

	private final PropertyChangeListener customerChangeHandler = new CustomerChangeHandler(this);

	private final SaleTypeChangeHandler saleTypeChangeHandler;
	private final DesiredDateChangeHandler desiredDateChangeHandler;
	private final DateModeChangeHandler dateModeChangeHandler;
	private final BlockProductionChangeHandler blockProductionChangeHandler;

	private final SecurityPermissionManager securityManager;

	private Component desiredDate;
	private Component saleType;
	private Component invoiceAddressPanel, shippingAddressPanel;
	private Component invoiceAddressSearchAction, shippingAddressSearchAction;

	private static final String SERVICETYPE_COMPONENT_NAME = "serviceType";
	private static final String ORIGINALORDERNUMBER_COMPONENT_NAME = "originalOrderNumber";

	public MacOrderFormCustom()
	{
		saleTypeChangeHandler = new SaleTypeChangeHandler(this);
		desiredDateChangeHandler = new DesiredDateChangeHandler(this);
		dateModeChangeHandler = new DateModeChangeHandler(this);
		blockProductionChangeHandler = new BlockProductionChangeHandler(this);
		securityManager = new SecurityPermissionManagerFactory().newPermissionManager();
	}

	@Override
	public void buildPanel()
	{
		super.buildPanel();

		this.desiredDate = getComponent("desiredDate");
		this.saleType = getComponent("saleType");
		this.invoiceAddressPanel = getComponent("panelAdrInvoice");
		this.shippingAddressPanel = getComponent("panelAdrShipping");
		this.invoiceAddressSearchAction = getComponent("searchInvoiceAddress");
		this.shippingAddressSearchAction = getComponent("searchShippingAddress");

		MacOrder order = getCurrentBean();
		order.setShipASAP(true);
		getOptDateASAP().setSelected(true);
		dateASAPSelected();

		enableComponents(false);

		updateWorkLoadActionsEnabledState();

		initializeBeanListeners();
		initializeComponentListeners();
	}

	// Ajout OGA du 26/06/14 - Permet de récupérer les onglets à colorer
	private void initializeTabsColor()
	{
		if (getDetailTab() != null)
		{
			JideTabbedPane parentTable = (JideTabbedPane) getDetailTab();

			// Récupération des objets de order
			MacOrder order = getCurrentBean();

			// Vérification si onglet pièces jointes renseignés auquel cas le
			// coloré en ORANGE
			if (!order.getAttachments().isEmpty())
			{
				paintTab(parentTable, "tab_attachment", Color.ORANGE);
			}
			else
			{
				paintTab(parentTable, "tab_attachment", null);
			}

			// Vérification si onglet notes renseignés auquel cas le
			// coloré en ORANGE
			if (!order.getNotes().isEmpty())
			{
				paintTab(parentTable, "tab_notes", Color.ORANGE);
			}
			else
			{
				paintTab(parentTable, "tab_notes", null);
			}
		}
	}

	// Ajout OGA du 26/06/14 - Permet de colorer les onglets précédemment sélectionnés
	private void paintTab(JideTabbedPane parentTable, String tabKey, Color color)
	{
		if (parentTable != null && tabKey != null)
		{
			int tabIndex = parentTable.indexOfTab(getString(tabKey));

			if (tabIndex != -1)
			{
				getDetailTab().setBackgroundAt(tabIndex, color);
			}
		}
	}

	@Override
	protected void afterBeanChanged()
	{
		super.afterBeanChanged();
		initializeBeanListeners();

		MacOrder order = getCurrentBean();

		MacOrderFormModel macOrderFormModel = (MacOrderFormModel) getModelBean("orderFormModel");
		macOrderFormModel.setRealBillingDate(null);
		macOrderFormModel.setRealDeliveryDate(null);
		macOrderFormModel.setRealShippingDate(null);
		macOrderFormModel.setProductionEndDate(null);
		macOrderFormModel.setExplosionLaunchDate(null);
		macOrderFormModel.setExpectedProductionDate(null);

		if (order.getId() != null)
		{
			ComponentUtils.setComponentHierarchyEnabled(getComponent("date"), false);
		}
		else
		{
			order.setShipASAP(true);
		}

		// Blocage des actions Ajout/Modification de l'adresse facturation
		disableInvoiceAddressesModification();

		// active / desactive les champs du SAV
		enableComponents(order.getSaleType(), false);

		// Ajout OGA du 26/06/14 - Fonction pour coloriser le ou les onglet(s)
		initializeTabsColor();

		updateAddressTabsEnabledState();
		updateWorkLoadActionsEnabledState();
	}

	private void updateWorkLoadActionsEnabledState()
	{
		MacOrder order = getCurrentBean();
		boolean workLoadModifiable = isWorkLoadModifiable(order);

		if (!workLoadModifiable)
		{
			getOptDateSpecific().setEnabled(workLoadModifiable);
			getOptDateASAP().setEnabled(workLoadModifiable);
			getOptDateToDetermine().setEnabled(workLoadModifiable);

			setComponentEnable("desiredDate", workLoadModifiable);
			setComponentEnable("shippingDate", workLoadModifiable);
			setComponentEnable("saleType", workLoadModifiable);
			setComponentEnable("clearOrderWorkLoads", workLoadModifiable);
			setComponentEnable("route", workLoadModifiable);
			setComponentEnable("addTransactionDetail", workLoadModifiable);
			setComponentEnable("importProfile", workLoadModifiable);
			setComponentEnable("saleOrderDetailMassEdit", workLoadModifiable);
			setComponentEnable("removeOrderDetail", workLoadModifiable);
			setComponentEnable("copyTransactionDetail", workLoadModifiable);
			setComponentEnable("importQuotationDetail", workLoadModifiable);
			setComponentEnable("importOrderDetail", workLoadModifiable);
			setComponentEnable("updateTransactionDetailVersion", workLoadModifiable);
			setComponentEnable("cancelDetail", workLoadModifiable);
			setComponentEnable("massModifyDetailDiscountRate", workLoadModifiable);
			setComponentEnable("massMove", workLoadModifiable);
			setComponentEnable("regeneratePropertyValues", workLoadModifiable);
			setComponentEnable("modifyR4Discount", workLoadModifiable);
			setComponentEnable("customerDiscountGroup", workLoadModifiable);
			setComponentEnable("discountRate", workLoadModifiable);
			setComponentEnable("modifySaleTransactionDiscountRate", workLoadModifiable);
			setComponentEnable("modifyTransactionAvailableDiscount", workLoadModifiable);
			setComponentEnable("modifyTransactionDetailDiscount", workLoadModifiable);
			setComponentEnable("feePanel", workLoadModifiable);
			setComponentEnable("fixedPrice", workLoadModifiable);
			setComponentEnable("grid_orderFees", workLoadModifiable);
		}
	}

	public static boolean isWorkLoadModifiable(MacOrder order)
	{
		return order != null && order.getOriginalDocumentStatus() != null && !order.getOriginalDocumentStatus().isAllowWorkLoadModifications() ? false : true;
	}

	private void updateAddressTabsEnabledState()
	{
		MacOrder order = getCurrentBean();

		if (order != null && order.getCustomer() != null)
		{
			boolean allowed = securityManager.isAllowed(SECURITYKEY_ADDRESSSELECTIONSECURITYCOMPONENT, PermissionType.ENABLE);
			boolean enabled = allowed || TypeClientEnum.DIVERS.equals(order.getMacCustomer().getTypeClient());

			if (invoiceAddressPanel != null)
			{
				ComponentUtils.setComponentHierarchyEnabled(invoiceAddressPanel, enabled);
			}
			if (shippingAddressPanel != null)
			{
				ComponentUtils.setComponentHierarchyEnabled(shippingAddressPanel, enabled);
			}
			ComponentUtils.setComponentHierarchyEnabled(invoiceAddressSearchAction, true);
			ComponentUtils.setComponentHierarchyEnabled(shippingAddressSearchAction, true);
		}
	}

	private void initializeBeanListeners()
	{
		NAIDPresentationModel model = getModel(getModelName());

		model.getModel(MacOrder.PROPERTYNAME_SALETYPE).addValueChangeListener(saleTypeChangeHandler);
		model.getModel(MacOrder.PROPERTYNAME_DESIREDDATE).addValueChangeListener(desiredDateChangeHandler);
		model.getModel(MacOrder.PROPERTYNAME_SHIPASAP).addValueChangeListener(dateModeChangeHandler);
		model.getModel(MacOrder.PROPERTYNAME_SHIPTOCONFIRM).addValueChangeListener(dateModeChangeHandler);
		model.getModel(MacOrder.PROPERTYNAME_BLOCKPRODUCTION).addValueChangeListener(blockProductionChangeHandler);
		model.getModel(MacOrder.PROPERTYNAME_CUSTOMER).addValueChangeListener(customerChangeHandler);
	}

	private void removeBeanListeners()
	{
		NAIDPresentationModel model = getModel(getModelName());

		model.getModel(MacOrder.PROPERTYNAME_SALETYPE).removeValueChangeListener(saleTypeChangeHandler);
		model.getModel(MacOrder.PROPERTYNAME_DESIREDDATE).removeValueChangeListener(desiredDateChangeHandler);
		model.getModel(MacOrder.PROPERTYNAME_SHIPASAP).removeValueChangeListener(dateModeChangeHandler);
		model.getModel(MacOrder.PROPERTYNAME_SHIPTOCONFIRM).removeValueChangeListener(dateModeChangeHandler);
		model.getModel(MacOrder.PROPERTYNAME_BLOCKPRODUCTION).removeValueChangeListener(blockProductionChangeHandler);
		model.getModel(MacOrder.PROPERTYNAME_CUSTOMER).removeValueChangeListener(customerChangeHandler);
	}

	private void initializeComponentListeners()
	{
		((AbstractBoundAction) getAction("addTransactionDetail")).addActionNotifyListener(new AddTransactionDetailListener());
	}

	@Override
	protected void beforeBeanChanged()
	{
		super.beforeBeanChanged();
		removeBeanListeners();
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		MacOrder order = getCurrentBean();

		if (order != null && order.getId() == null && order.isReadyForModifications())
		{
			// Positionner par defaut Type de Ventes à DIFFUS
			List<SaleType> macSaleTypeList = loaderLocator.get().findByField(SaleType.class, SaleType.class, SaleType.PROPERTYNAME_CODE, "DIFF");
			if (!macSaleTypeList.isEmpty())
			{
				order.setSaleType(macSaleTypeList.get(0));
			}

			// Positionner par defaut Origine Transaction à FAX
			List<SaleTransactionOrigin> saleTransactionOrigin = loaderLocator.get().findByField(SaleTransactionOrigin.class, SaleTransactionOrigin.class,
					SaleTransactionOrigin.PROPERTYNAME_CODE, "FAX");
			if (!saleTransactionOrigin.isEmpty())
			{
				order.setSaleTransactionOrigin(saleTransactionOrigin.get(0));
			}

			// Initialiser le champ Tout Afficher à 1 (cocher par defaut)
			OrderFormModel orderFormModel = getModelBean("orderFormModel");
			orderFormModel.setShowAll(true);

			order.setDirty(false);
		}
	}

	// Blocage des actions Ajout/Modification de l'adresse facturation
	private void disableInvoiceAddressesModification()
	{
		setComponentEnable("addJobSiteAddress", false);
		setComponentEnable("modifyInvoiceAddress", false);
	}

	@Override
	protected void validateUpdateAllShippingDate(PropertyChangeEvent evt)
	{
		MacOrder order = getCurrentBean();

		// when the order's shipping date is modified in ASAP mode, it doesn't
		// necessarily mean we want to reciprocate the shipping date to the
		// details, since
		// it wouldn't make sense in some contexts... hence why the following
		// flag has been created
		if (!order.isResetingShippingDate())
		{
			if (!order.getDetails().isEmpty() && order.isReadyForModifications())
			{
				order.updateAccessoriesShippingDate();
				order.updateDetailShippingDate(order.getShippingDate(), order.isShipASAP(), order.isShipToConfirm());
			}
		}
	}

	/**
	 * Overridden to avoid prompting a dialog, instead we always want to apply the change to the order details.
	 */
	@Override
	public void dateSpecificSelected()
	{
		MacOrder order = getCurrentBean();
		order.setResetingShippingDate(true);
		super.dateSpecificSelected();
		order.setResetingShippingDate(false);
	}

	/**
	 * Overridden to avoid prompting a dialog, instead we always want to apply the change to the order details.
	 */
	@Override
	public void dateASAPSelected()
	{
		MacOrder order = getCurrentBean();
		order.setResetingShippingDate(true);
		super.dateASAPSelected();
		order.setResetingShippingDate(false);
	}

	/**
	 * Overridden to avoid prompting a dialog, instead we always want to apply the change to the order details.
	 */
	@Override
	public void dateToDetermineSelected()
	{
		MacOrder order = getCurrentBean();
		order.setResetingShippingDate(true);
		super.dateToDetermineSelected();
		order.setResetingShippingDate(false);
	}

	/**
	 * Overridden to avoid prompting a dialog, instead we always want to apply the change to the order details.
	 */
	@Override
	protected void promptUpdateOrderDetailsDates()
	{
		MacOrder order = getCurrentBean();
		order.updateDetailShippingDate(order.getShippingDate(), order.isShipASAP(), order.isShipToConfirm());
	}

	private final class SaleTypeChangeHandler implements PropertyChangeListener
	{
		private final MacOrderFormCustom orderForm;

		public SaleTypeChangeHandler(MacOrderFormCustom orderForm)
		{
			this.orderForm = orderForm;
		}

		@Override
		public void propertyChange(PropertyChangeEvent event)
		{
			MacOrder order = orderForm.getCurrentBean();
			SaleType oldValue = (SaleType) event.getOldValue();
			SaleType newValue = (SaleType) event.getNewValue();

			enableComponents(newValue, true);
			if (order.getSaleType() != null)
			{
				SaleTypeEnum saleTypeEnum = SaleTypeEnum.getSaleTypeEnum(order.getSaleType());
				if (saleTypeEnum != null)
				{
					switch (saleTypeEnum)
					{
						case chantier:
							orderForm.dateToDetermineSelected();
							break;
						case diffu:
							orderForm.dateASAPSelected();
							break;
					}
				}
			}
		}
	}

	private static class DesiredDateChangeHandler implements PropertyChangeListener
	{
		private final MacOrderFormCustom orderForm;

		public DesiredDateChangeHandler(MacOrderFormCustom orderForm)
		{
			this.orderForm = orderForm;
		}

		@Override
		public void propertyChange(PropertyChangeEvent event)
		{
			MacOrder order = orderForm.getCurrentBean();

			// work load details will have to be recalculated with the new
			// desired date (ASAP mode only)
			for (OrderDetail orderDetail : order.getDetails())
			{
				MacOrderDetail macOrderDetail = Model.getTarget(orderDetail);
				if (macOrderDetail.isShipASAP())
				{
					macOrderDetail.resetWorkLoadDetails();
				}
			}
		}
	}

	private static class DateModeChangeHandler implements PropertyChangeListener
	{
		private final MacOrderFormCustom orderForm;

		public DateModeChangeHandler(MacOrderFormCustom orderForm)
		{
			this.orderForm = orderForm;
		}

		@Override
		public void propertyChange(PropertyChangeEvent event)
		{
			MacOrder order = orderForm.getCurrentBean();

			// replicate the order's date mode to the order details
			for (OrderDetail orderDetail : order.getDetails())
			{
				orderDetail.setShipASAP(order.isShipASAP());
				orderDetail.setShipToConfirm(order.isShipToConfirm());
			}
		}
	}

	private static class BlockProductionChangeHandler implements PropertyChangeListener
	{
		private final MacOrderFormCustom orderForm;

		public BlockProductionChangeHandler(MacOrderFormCustom orderForm)
		{
			this.orderForm = orderForm;
		}

		@Override
		public void propertyChange(PropertyChangeEvent event)
		{
			MacOrder order = orderForm.getCurrentBean();

			// when an order is blocked, we clear the order details that are in
			// ASAP mode (specific dated details are always kept)
			if (order.isBlockProductionEffective() && !order.isExploded())
			{
				for (OrderDetail orderDetail : order.getDetails())
				{
					MacOrderDetail macOrderDetail = Model.getTarget(orderDetail);
					if (macOrderDetail.isShipASAP())
					{
						macOrderDetail.resetWorkLoadDetails();
					}
				}
			}
		}
	}

	protected static final class CustomerChangeHandler implements PropertyChangeListener
	{
		private final MacOrderFormCustom orderForm;

		public CustomerChangeHandler(MacOrderFormCustom orderForm)
		{
			this.orderForm = orderForm;
		}

		@Override
		public void propertyChange(PropertyChangeEvent evt)
		{
			MacOrder order = orderForm.getCurrentBean();
			if (evt.getNewValue() != null && order.isReadyForModifications())
			{
				// Blocage des actions Ajout/Modification de l'adresse
				// facturation (creation commande)
				orderForm.disableInvoiceAddressesModification();

				// When the client changes we refresh the conditional security
				// of the address tabs
				orderForm.updateAddressTabsEnabledState();
			}
		}
	}

	private void setComponentEnable(String componentName, boolean enable)
	{
		if (componentName != null && !componentName.equals(""))
		{
			Component component = getComponent(componentName);
			if (component != null)
			{
				ComponentUtils.setComponentHierarchyEnabled(component, enable);
			}
		}
	}

	protected void enableComponents(boolean enable)
	{
		setComponentEnable(SERVICETYPE_COMPONENT_NAME, enable);
		setComponentEnable(ORIGINALORDERNUMBER_COMPONENT_NAME, enable);
	}

	private void enableComponents(SaleType saleType, boolean doSetValues)
	{
		MacOrder order = getCurrentBean();
		if (saleType != null && saleType.getCode() != null)
		{
			if (saleType.getUpperCode().equals(SaleTypeEnum.sav.getUpperCode()))
			{
				enableComponents(true);
			}
			else
			{
				enableComponents(false);
				if (doSetValues)
				{
					order.setServiceType(null);
					order.setOriginalOrderNumber(null);
				}
			}
		}
		else
		{
			enableComponents(false);
			if (doSetValues)
			{
				order.setServiceType(null);
				order.setOriginalOrderNumber(null);
			}
		}
	}

	private class AddTransactionDetailListener extends ActionNotifyAdapter
	{
		@Override
		public void after(ActionNotifyEvent event)
		{
			MacOrder order = event.getBean();

			for (OrderDetail orderDetail : order.getDetails())
			{
				// we initialize the R4 amount to 0.00 with its currency, in order to have a valid editor in the table afterwards (doesn't support null)
				MacOrderDetail macOrderDetail = Model.getTarget(orderDetail);
				if (macOrderDetail.getMontantR4() == null)
				{
					macOrderDetail.setMontantR4(new MonetaryAmount(order.getCurrency().getJavaCurrencyInstance()));
				}
			}
		}
	}
}
