package com.mac.custom.form;

import java.awt.Color;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Date;
import java.util.List;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.jidesoft.swing.JideTabbedPane;
import com.mac.custom.bo.MacQuotation;
import com.mac.custom.bo.MacQuotationDetail;
import com.mac.custom.enums.SaleTypeEnum;
import com.mac.custom.enums.TypeClientEnum;
import com.netappsid.action.AbstractBoundAction;
import com.netappsid.action.adapter.ActionNotifyAdapter;
import com.netappsid.action.event.ActionNotifyEvent;
import com.netappsid.bo.model.Model;
import com.netappsid.datatypes.MonetaryAmount;
import com.netappsid.erp.server.bo.QuotationDetail;
import com.netappsid.erp.server.bo.SaleTransactionOrigin;
import com.netappsid.erp.server.bo.SaleType;
import com.netappsid.europe.naid.custom.form.QuotationFormEuro;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.security.permission.client.SecurityPermissionManager;
import com.netappsid.security.permission.factory.SecurityPermissionManagerFactory;
import com.netappsid.security.permission.server.PermissionType;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.Loader;
import com.netappsid.utils.ComponentUtils;

public class MacQuotationFormCustom extends QuotationFormEuro implements ChangeListener
{
	private static final String QUOTATION = "quotation";
	private final ServiceLocator<Loader> loaderLocator = new ServiceLocator<Loader>(LoaderBean.class);

	private final PropertyChangeListener saleTypeChangeHandler;
	private static final String SALETYPE_COMPONENT_NAME = "saleType";
	private static final String SERVICETYPE_COMPONENT_NAME = "serviceType";
	private static final String ORIGINALORDERNUMBER_COMPONENT_NAME = "originalOrderNumber";

	private Component invoiceAddressSearchAction, shippingAddressSearchAction;

	public static final String SECURITYKEY_ADDRESSSELECTIONSECURITYCOMPONENT = "orders:quotation:732dcc4d60194141:680b9bf813a640e6:4df4c81b10794f7c:3ff4831d7fe24903:ace55930eb2e43a8";

	private final SecurityPermissionManager securityManager;
	private Component invoiceAddressPanel;
	private Component shippingAddressPanel;
	private final PropertyChangeListener customerChangeHandler = new CustomerChangeHandler(this);

	public MacQuotationFormCustom()
	{
		saleTypeChangeHandler = new SaleTypeChangeHandler(this);
		securityManager = new SecurityPermissionManagerFactory().newPermissionManager();
	}

	// Ajout OGA du 01/07/14 - Permet de récupérer les onglets à coloriser
	private void initializeTabsColor()
	{
		if (getDetailTab() != null)
		{
			JideTabbedPane parentTable = (JideTabbedPane) getDetailTab();

			// Récupération des objets de quotation
			MacQuotation quotation = getCurrentBean();

			// Vérification si onglet pièces jointes renseignés auquel cas le
			// coloré en ORANGE
			if (!quotation.getAttachments().isEmpty())
			{
				paintTab(parentTable, "tab_attachment", Color.ORANGE);
			}
			else
			{
				paintTab(parentTable, "tab_attachment", null);
			}
		}
	}

	// Ajout OGA du 01/07/14 - Permet de colorer les onglets précédemment
	// sélectionnés
	private void paintTab(JideTabbedPane parentTable, String tabKey, Color color)
	{
		if (parentTable != null && tabKey != null)
		{
			int tabIndex = parentTable.indexOfTab(getString(tabKey));

			if (tabIndex != -1)
			{
				getDetailTab().setBackgroundAt(tabIndex, color);
			}
		}
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		MacQuotation quotation = (MacQuotation) getModelBean(QUOTATION);

		if (quotation != null && quotation.getId() == null && quotation.isReadyForModifications())
		{
			// Positionner par defaut Type de Ventes à DIFFUS
			List<SaleType> macSaleTypeList = loaderLocator.get().findByField(SaleType.class, SaleType.class, SaleType.PROPERTYNAME_CODE,
					SaleTypeEnum.diffu.getUpperCode());
			if (macSaleTypeList != null && !macSaleTypeList.isEmpty())
			{
				quotation.setSaleType(macSaleTypeList.get(0));
			}

			// Positionner par defaut Origine Transaction à FAX
			List<SaleTransactionOrigin> saleTransactionOrigin = loaderLocator.get().findByField(SaleTransactionOrigin.class, SaleTransactionOrigin.class,
					SaleTransactionOrigin.PROPERTYNAME_CODE, "FAX");
			if (saleTransactionOrigin != null && !saleTransactionOrigin.isEmpty())
			{
				quotation.setSaleTransactionOrigin(saleTransactionOrigin.get(0));
			}

			// Mettre date date du jour dans date desire OGA le 31/01/2014
			quotation.setDesiredDate(new Date());

			quotation.setDirty(false);
		}
	}

	@Override
	public void buildPanel()
	{
		super.buildPanel();
		enableComponents(false);
		initializeBeanListeners();
		initializeComponentListeners();

		this.invoiceAddressPanel = getComponent("panelAdrInvoice");
		this.shippingAddressPanel = getComponent("panelAdrShipping");
		this.invoiceAddressSearchAction = getComponent("searchInvoiceAddress");
		this.shippingAddressSearchAction = getComponent("searchShippingAddress");
	}

	@Override
	protected void afterBeanChanged()
	{
		super.afterBeanChanged();
		enableComponents(((MacQuotation) getCurrentBean()).getSaleType());
		initializeBeanListeners();

		// Ajout OGA du 01/07/14 - Fonction pour coloriser le ou les onglet(s)
		initializeTabsColor();

		updateAddressTabsEnabledState();
	}

	private void updateAddressTabsEnabledState()
	{
		MacQuotation quotation = getCurrentBean();

		if (quotation != null && quotation.getCustomer() != null)
		{
			boolean allowed = securityManager.isAllowed(SECURITYKEY_ADDRESSSELECTIONSECURITYCOMPONENT, PermissionType.ENABLE);
			boolean enabled = allowed || TypeClientEnum.DIVERS.equals(quotation.getMacCustomer().getTypeClient());

			if (invoiceAddressPanel != null)
			{
				ComponentUtils.setComponentHierarchyEnabled(invoiceAddressPanel, enabled);
			}
			if (shippingAddressPanel != null)
			{
				ComponentUtils.setComponentHierarchyEnabled(shippingAddressPanel, enabled);
			}
			ComponentUtils.setComponentHierarchyEnabled(invoiceAddressSearchAction, true);
			ComponentUtils.setComponentHierarchyEnabled(shippingAddressSearchAction, true);
		}
	}

	@Override
	protected void beforeBeanChanged()
	{
		super.beforeBeanChanged();
		removeBeanListeners();
	}

	private void initializeBeanListeners()
	{
		NAIDPresentationModel model = getModel(getModelName());

		model.getModel(MacQuotation.PROPERTYNAME_SALETYPE).addValueChangeListener(saleTypeChangeHandler);
		model.getModel(MacQuotation.PROPERTYNAME_CUSTOMER).addValueChangeListener(customerChangeHandler);
	}

	private void removeBeanListeners()
	{
		NAIDPresentationModel model = getModel(getModelName());

		model.getModel(MacQuotation.PROPERTYNAME_SALETYPE).removeValueChangeListener(saleTypeChangeHandler);
		model.getModel(MacQuotation.PROPERTYNAME_CUSTOMER).removeValueChangeListener(customerChangeHandler);
	}

	private void initializeComponentListeners()
	{
		((AbstractBoundAction) getAction("addTransactionDetail")).addActionNotifyListener(new AddTransactionDetailListener());
	}

	protected void enableComponents(boolean enable)
	{
		setComponentEnable(SERVICETYPE_COMPONENT_NAME, enable);
		setComponentEnable(ORIGINALORDERNUMBER_COMPONENT_NAME, enable);
	}

	private void setComponentEnable(String componentName, boolean enable)
	{
		if (componentName != null && !componentName.equals(""))
		{
			Component component = getComponent(componentName);
			if (component != null)
			{
				ComponentUtils.setComponentHierarchyEnabled(component, enable);
			}
		}
	}

	private final class SaleTypeChangeHandler implements PropertyChangeListener
	{
		private final MacQuotationFormCustom quotationForm;

		public SaleTypeChangeHandler(MacQuotationFormCustom quotationForm)
		{
			this.quotationForm = quotationForm;
		}

		@Override
		public void propertyChange(PropertyChangeEvent evt)
		{
			SaleType oldValue = (SaleType) evt.getOldValue();
			SaleType newValue = (SaleType) evt.getNewValue();
			enableComponents(newValue);
		}
	}

	private void enableComponents(SaleType saleType)
	{
		MacQuotation quotation = getCurrentBean();
		if (saleType != null && saleType.getCode() != null)
		{
			if (saleType.getUpperCode().equals(SaleTypeEnum.sav.getUpperCode()))
			{
				enableComponents(true);
			}
			else
			{
				enableComponents(false);
				quotation.setServiceType(null);
				quotation.setOriginalOrderNumber("");
			}
		}
		else
		{
			enableComponents(false);
			quotation.setServiceType(null);
			quotation.setOriginalOrderNumber("");
		}
	}

	@Override
	public void stateChanged(ChangeEvent e)
	{
		System.out.println("STATE CHANGED = " + e.getSource());
	}

	protected static final class CustomerChangeHandler implements PropertyChangeListener
	{
		private final MacQuotationFormCustom quotationForm;

		public CustomerChangeHandler(MacQuotationFormCustom quotationForm)
		{
			this.quotationForm = quotationForm;
		}

		@Override
		public void propertyChange(PropertyChangeEvent evt)
		{
			MacQuotation quotation = quotationForm.getCurrentBean();
			if (evt.getNewValue() != null && quotation.isReadyForModifications())
			{
				// When the client changes we refresh the conditional security
				// of the address tabs
				quotationForm.updateAddressTabsEnabledState();
			}
		}
	}

	private class AddTransactionDetailListener extends ActionNotifyAdapter
	{
		@Override
		public void after(ActionNotifyEvent event)
		{
			MacQuotation quotation = event.getBean();

			for (QuotationDetail quotationDetail : quotation.getDetails())
			{
				// we initialize the R4 amount to 0.00 with its currency, in order to have a valid editor in the table afterwards (doesn't support null)
				MacQuotationDetail macQuotationDetail = Model.getTarget(quotationDetail);
				if (macQuotationDetail.getMontantR4() == null)
				{
					macQuotationDetail.setMontantR4(new MonetaryAmount(quotation.getCurrency().getJavaCurrencyInstance()));
				}
			}
		}
	}
}
