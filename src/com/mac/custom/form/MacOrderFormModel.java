package com.mac.custom.form;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Date;

import javax.persistence.Transient;

import org.hibernate.Hibernate;

import com.jgoodies.binding.value.ValueModel;
import com.mac.custom.bo.MacOrderDetail;
import com.mac.custom.services.order.beans.MacOrderServicesBean;
import com.mac.custom.services.order.interfaces.MacOrderServices;
import com.netappsid.annotations.DAO;
import com.netappsid.binding.beans.CollectionValueModel;
import com.netappsid.bo.model.Model;
import com.netappsid.datatypes.MonetaryAmount;
import com.netappsid.erp.server.bo.Order;
import com.netappsid.erp.server.bo.OrderDetail;
import com.netappsid.erp.server.bo.dao.OrderDAO;
import com.netappsid.erp.server.model.form.OrderFormModel;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.observable.ObservableByName;
import com.netappsid.observable.ObservableElementsCollectionHandler;
import com.netappsid.security.permission.server.AdminService;
import com.netappsid.security.permission.server.AdminServiceBean;

@DAO(dao = OrderDAO.class)
public class MacOrderFormModel extends OrderFormModel
{
	private final ServiceLocator<MacOrderServices> macOrderServiceLocator = new ServiceLocator<MacOrderServices>(MacOrderServicesBean.class);

	private static final String PROPERTYNAME_DETAILS = "details";
	private final ObservableElementsCollectionHandler detailsPricePropertyChangeHandler;

	private Date expectedProductionDate;
	private Date explosionLaunchDate;
	private Date productionEndDate;
	private Date realShippingDate;
	private Date realDeliveryDate;
	private Date realBillingDate;

	public MacOrderFormModel(NAIDPresentationModel businessObjectModel)
	{
		this(businessObjectModel, new ServiceLocator<AdminService>(AdminServiceBean.class));
	}

	public MacOrderFormModel(NAIDPresentationModel businessObjectModel, ValueModel localeValueModel)
	{
		this(businessObjectModel, localeValueModel, new ServiceLocator<AdminService>(AdminServiceBean.class));
	}

	protected MacOrderFormModel(NAIDPresentationModel businessObjectModel, ServiceLocator<AdminService> adminServicesSserviceLocator)
	{
		this(businessObjectModel, businessObjectModel.getModel(Order.PROPERTYNAME_LOCALE), adminServicesSserviceLocator);
	}

	public MacOrderFormModel(NAIDPresentationModel businessObjectModel, ValueModel localeValueModel, ServiceLocator<AdminService> adminServicesSserviceLocator)
	{
		super(businessObjectModel, localeValueModel, adminServicesSserviceLocator);

		CollectionValueModel details = businessObjectModel.getCollectionValueModel(PROPERTYNAME_DETAILS);

		detailsPricePropertyChangeHandler = new ObservableElementsCollectionHandler<ObservableByName>(OrderDetail.PROPERTYNAME_PRICE,
				new OnPostPriceChangedPropertyChangeHandler());
		detailsPricePropertyChangeHandler.install(details);
	}

	@Override
	public void dispose()
	{
		CollectionValueModel details = getBusinessObjectModel().getCollectionValueModel(PROPERTYNAME_DETAILS);
		detailsPricePropertyChangeHandler.uninstall(details);
		super.dispose();
	}

	public void setExpectedProductionDate(Date expectedProductionDate)
	{
		Date oldValue = getExpectedProductionDate();
		this.expectedProductionDate = expectedProductionDate == null ? null : new Date(expectedProductionDate.getTime());
		firePropertyChange(PROPERTYNAME_EXPECTEDPRODUCTIONDATE, oldValue, expectedProductionDate);
	}

	@Transient
	public Date getExpectedProductionDate()
	{
		return expectedProductionDate == null ? null : new Date(expectedProductionDate.getTime());
	}

	public void setExplosionLaunchDate(Date explosionLaunchDate)
	{
		Date oldValue = getExplosionLaunchDate();
		this.explosionLaunchDate = explosionLaunchDate == null ? null : new Date(explosionLaunchDate.getTime());
		firePropertyChange(PROPERTYNAME_EXPLOSIONLAUNCHDATE, oldValue, explosionLaunchDate);
	}

	@Transient
	public Date getExplosionLaunchDate()
	{
		return explosionLaunchDate == null ? null : new Date(explosionLaunchDate.getTime());
	}

	public void setProductionEndDate(Date productionEndDate)
	{
		Date oldValue = getProductionEndDate();
		this.productionEndDate = productionEndDate == null ? null : new Date(productionEndDate.getTime());
		firePropertyChange(PROPERTYNAME_PRODUCTIONENDDATE, oldValue, productionEndDate);
	}

	@Transient
	public Date getProductionEndDate()
	{
		return productionEndDate == null ? null : new Date(productionEndDate.getTime());
	}

	public void setRealShippingDate(Date realShippingDate)
	{
		Date oldValue = getRealShippingDate();
		this.realShippingDate = realShippingDate == null ? null : new Date(realShippingDate.getTime());
		firePropertyChange(PROPERTYNAME_REALSHIPPINGDATE, oldValue, realShippingDate);
	}

	@Transient
	public Date getRealShippingDate()
	{
		return realShippingDate == null ? null : new Date(realShippingDate.getTime());
	}

	public void setRealDeliveryDate(Date realDeliveryDate)
	{
		Date oldValue = getRealDeliveryDate();
		this.realDeliveryDate = realDeliveryDate == null ? null : new Date(realDeliveryDate.getTime());
		firePropertyChange(PROPERTYNAME_REALDELIVERYDATE, oldValue, realDeliveryDate);
	}

	@Transient
	public Date getRealDeliveryDate()
	{
		return realDeliveryDate == null ? null : new Date(realDeliveryDate.getTime());
	}

	public void setRealBillingDate(Date realBillingDate)
	{
		Date oldValue = getRealBillingDate();
		this.realBillingDate = realBillingDate == null ? null : new Date(realBillingDate.getTime());
		firePropertyChange(PROPERTYNAME_REALBILLINGDATE, oldValue, realBillingDate);
	}

	@Transient
	public Date getRealBillingDate()
	{
		return realBillingDate == null ? null : new Date(realBillingDate.getTime());
	}

	public void refreshDates()
	{
		Order order = (Order) getBean();
		if (order != null && order.getId() != null)
		{
			setRealBillingDate(macOrderServiceLocator.get().getRealBillingDate(order.getId()));
			setRealDeliveryDate(macOrderServiceLocator.get().getRealDeliveryDate(order.getId()));
			setRealShippingDate(macOrderServiceLocator.get().getRealShippingDate(order.getId()));
			setProductionEndDate(macOrderServiceLocator.get().getProductionEndDate(order.getId()));
			setExplosionLaunchDate(macOrderServiceLocator.get().getExplosionLaunchDate(order.getId()));
			setExpectedProductionDate(macOrderServiceLocator.get().getExpectedProductionDate(order.getId()));
		}
		else
		{
			setRealBillingDate(null);
			setRealDeliveryDate(null);
			setRealShippingDate(null);
			setProductionEndDate(null);
			setExplosionLaunchDate(null);
			setExpectedProductionDate(null);
		}
	}

	public static final class OnPostPriceChangedPropertyChangeHandler implements PropertyChangeListener
	{
		@Override
		public void propertyChange(PropertyChangeEvent evt)
		{
			Object source = evt.getSource();
			if (MacOrderDetail.class.isAssignableFrom(Hibernate.getClass(source)))
			{
				MacOrderDetail detail = (MacOrderDetail) Model.getTarget(source);
				MonetaryAmount retailPrice = detail.getRetailPrice();
				MonetaryAmount price = detail.getPrice();
				detail.setPriceOverrided(!price.equals(retailPrice));
			}
		}
	}

	public static final String PROPERTYNAME_REALBILLINGDATE = "realBillingDate";
	public static final String PROPERTYNAME_REALDELIVERYDATE = "realDeliveryDate";
	public static final String PROPERTYNAME_REALSHIPPINGDATE = "realShippingDate";
	public static final String PROPERTYNAME_PRODUCTIONENDDATE = "productionEndDate";
	public static final String PROPERTYNAME_EXPLOSIONLAUNCHDATE = "explosionLaunchDate";
	public static final String PROPERTYNAME_EXPECTEDPRODUCTIONDATE = "expectedProductionDate";
}
