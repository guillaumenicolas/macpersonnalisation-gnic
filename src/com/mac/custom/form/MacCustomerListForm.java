package com.mac.custom.form;

import com.jidesoft.grid.CellStyleTable;
import com.mac.custom.table.renderer.MacCustomerListTableCellRenderer;
import com.netappsid.erp.client.gui.AbstractERPListForm;
import com.netappsid.framework.gui.components.SearchPanel;

public class MacCustomerListForm extends AbstractERPListForm
{
	@Override
	public void buildPanel()
	{
		setListModelName("macCommercialEntityList");
		super.buildPanel();
		initializeCellStyleProvider("grid_customers");

	}

	protected void initializeCellStyleProvider(String tableName)
	{
		SearchPanel searchPanel = (SearchPanel) getComponent(tableName);
		CellStyleTable frameworkTable = searchPanel.getSearchTable();
		frameworkTable.setCellStyleProvider(new MacCustomerListTableCellRenderer(getClass().getSimpleName()));
	}
}