package com.mac.custom.form;

import com.netappsid.framework.gui.components.model.NAIDPresentationModel;
import com.netappsid.framework.gui.formbuilder.formmodel.FormModelFactory;

public class MacOrderFormModelFactory extends FormModelFactory<MacOrderFormModel>
{
	@Override
	public MacOrderFormModel createFormModel(NAIDPresentationModel businessObjectModel)
	{
		MacOrderFormModel macOrderFormModel = new MacOrderFormModel(businessObjectModel);
		return macOrderFormModel;
	}
}