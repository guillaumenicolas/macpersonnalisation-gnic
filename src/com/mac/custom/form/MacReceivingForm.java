package com.mac.custom.form;

import java.awt.Component;
import java.util.Arrays;

import resources.forms.classes.ReceivingForm;

import com.netappsid.action.adapter.ActionNotifyAdapter;
import com.netappsid.action.event.ActionNotifyEvent;
import com.netappsid.erp.client.action.generalledgertransaction.SaveReceiving;
import com.netappsid.erp.client.action.receiving.SaveAndReloadReceiving;
import com.netappsid.erp.server.bo.DocumentStatus;
import com.netappsid.erp.server.bo.PurchaseOrder;
import com.netappsid.erp.server.bo.Receiving;
import com.netappsid.erp.server.bo.User;
import com.netappsid.erp.server.services.beans.DocumentStatusServicesBean;
import com.netappsid.erp.server.services.interfaces.DocumentStatusServices;
import com.netappsid.erp.server.services.utils.SetupUtils;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.utils.ComponentUtils;

public class MacReceivingForm extends ReceivingForm
{
	private final DocumentStatusServices documentStatusServicesLocator = new ServiceLocator<DocumentStatusServices>(DocumentStatusServicesBean.class).get();
	private final String SAVE_AND_RELOAD_RECEIVING = "saveAndReloadReceiving";
	private final String SAVE_RECEIVING = "saveReceiving";
	private static final String EDITSUPPLIER_COMPONENT_NAME = "modifysupplier";

	@Override
	public void buildPanel()
	{
		super.buildPanel();

		ActionNotifyAdapter saveReceivingNotifier = new ActionNotifyAdapter()
			{
				@Override
				public void after(ActionNotifyEvent event)
				{
					closePurchaseOrder();
				}
			};

		SaveAndReloadReceiving saveAndReloadReceiving = getAction(SAVE_AND_RELOAD_RECEIVING);
		saveAndReloadReceiving.addActionNotifyListener(saveReceivingNotifier);

		SaveReceiving saveReceiving = getAction(SAVE_RECEIVING);
		saveReceiving.addActionNotifyListener(saveReceivingNotifier);

		enableEditSupplierComponent();

	}

	private void setComponentEnable(String componentName, boolean enable)
	{
		if (componentName != null && !componentName.equals(""))
		{
			Component component = getComponent(componentName);
			if (component != null)
			{
				ComponentUtils.setComponentHierarchyEnabled(component, enable);
			}
		}
	}

	@Override
	protected void afterBeanChanged()
	{
		super.afterBeanChanged();
		// Activer ou desactiver icone modification fiche fournisseur - OGA le 11/07/14
		enableEditSupplierComponent();
	}

	@Override
	// Activer ou desactiver icone modification fiche fournisseur - OGA le 11/07/14
	// Fonction ex�cuter par d�faut dans le code source il faut donc la prendre en compte
	public void activateComponentControl(boolean control)
	{
		super.activateComponentControl(control);

		enableEditSupplierComponent();
	}

	private void enableEditSupplierComponent()
	{
		Receiving receiving = getCurrentBean();
		setComponentEnable(EDITSUPPLIER_COMPONENT_NAME, receiving != null && receiving.getId() != null);
	}

	private void closePurchaseOrder()
	{
		Receiving receiving = getModelBean(getModelName());
		PurchaseOrder purchaseOrder = receiving.getPurchaseOrder();
		if (purchaseOrder != null && !purchaseOrder.getDocumentStatus().isClose() && purchaseOrder.isReceived())
		{
			User user = SetupUtils.INSTANCE.getMainSetup().getAutomaticServiceUser();
			documentStatusServicesLocator.setDocumentStatus(Arrays.asList(purchaseOrder.getId()), PurchaseOrder.class, DocumentStatus.CLOSE,
					user != null ? user.getId() : null);
		}

	}
}