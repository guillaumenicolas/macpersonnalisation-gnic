package com.mac.custom.form;

import java.util.Calendar;

import com.mac.custom.bo.MacCustomer;
import com.mac.custom.factory.EncoursClientFactory;
import com.mac.custom.services.customer.beans.MacCustomerServicesBean;
import com.mac.custom.services.customer.interfaces.remote.MacCustomerServicesRemote;
import com.netappsid.datatypes.MonetaryAmount;
import com.netappsid.erp.server.model.form.CustomerFormModel;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;
import com.netappsid.framework.utils.ServiceLocator;

public class MacCustomerFormModel extends CustomerFormModel
{
	private static final String PROPERTYNAME_TOTAL_MONTANTFACTURE_ANNEENCOURS = "totalMontantFactureAnneEnCours";
	private static final String PROPERTYNAME_TOTAL_MONTANTFACTURE_ANNEPRECEDENTE = "totalMontantFactureAnnePrecedente";
	private static final String PROPERTYNAME_TOTAL_MONTANTFACTURE_ILYADEUXANS = "totalMontantFactureIlyaDeuxAns";
	// Ajoute le 03/12/13 par OGA/PBY
	// Champs calcules dans module information credit de la fiche client

	private MonetaryAmount totalMontantFactureAnneEnCours;
	private MonetaryAmount totalMontantFactureAnnePrecedente;
	private MonetaryAmount totalMontantFactureIlyaDeuxAns;
	// Ajoute le 03/12/13 par OGA/PBY
	// Champs calcules dans module information credit de la fiche client

	private final ServiceLocator<MacCustomerServicesRemote> customerServiceLocator = new ServiceLocator<MacCustomerServicesRemote>(
			MacCustomerServicesBean.class);

	public MacCustomerFormModel(NAIDPresentationModel businessObjectModel)
	{
		super(businessObjectModel);
	}

	public MonetaryAmount getTotalMontantFactureAnneEnCours()
	{
		return totalMontantFactureAnneEnCours;
	}

	public void setTotalMontantFactureAnneEnCours(MonetaryAmount totalMontantFactureAnneEnCours)
	{
		MonetaryAmount oldValue = getTotalMontantFactureAnneEnCours();
		this.totalMontantFactureAnneEnCours = totalMontantFactureAnneEnCours;

		firePropertyChange(PROPERTYNAME_TOTAL_MONTANTFACTURE_ANNEENCOURS, oldValue, totalMontantFactureAnneEnCours, false);
	}

	public MonetaryAmount getTotalMontantFactureAnnePrecedente()
	{
		return totalMontantFactureAnnePrecedente;
	}

	public void setTotalMontantFactureAnnePrecedente(MonetaryAmount totalMontantFactureAnnePrecedente)
	{
		MonetaryAmount oldValue = getTotalMontantFactureAnnePrecedente();
		this.totalMontantFactureAnnePrecedente = totalMontantFactureAnnePrecedente;

		firePropertyChange(PROPERTYNAME_TOTAL_MONTANTFACTURE_ANNEPRECEDENTE, oldValue, totalMontantFactureAnnePrecedente, false);
	}

	public MonetaryAmount getTotalMontantFactureIlyaDeuxAns()
	{
		return totalMontantFactureIlyaDeuxAns;
	}

	public void setTotalMontantFactureIlyaDeuxAns(MonetaryAmount totalMontantFactureIlyaDeuxAns)
	{
		MonetaryAmount oldValue = getTotalMontantFactureIlyaDeuxAns();
		this.totalMontantFactureIlyaDeuxAns = totalMontantFactureIlyaDeuxAns;

		firePropertyChange(PROPERTYNAME_TOTAL_MONTANTFACTURE_ILYADEUXANS, oldValue, totalMontantFactureIlyaDeuxAns, false);
	}

	@Override
	protected void formModelBeanChanged(Object oldValue, Object newValue)
	{
		super.formModelBeanChanged(oldValue, newValue);

		// Ensure to reset the content of all 'total' fields with null values since they are loaded on demmand.

		setTotalMontantFactureAnneEnCours(null);
		setTotalMontantFactureAnnePrecedente(null);
		setTotalMontantFactureIlyaDeuxAns(null);

	}

	private void updateEncours()
	{
		MacCustomer customer = (MacCustomer) getBean();

		if (customer != null && customer.getId() != null)
		{
			EncoursClientFactory encoursClientFactory = new EncoursClientFactory();
			customer.setEncoursClient(encoursClientFactory.create(customer));
		}
	}

	@Override
	public void fetchTotals()
	{
		updateEncours();
	}

	/**
	 * Fetches total of this customer's invoices for current year.
	 */
	public void fetchTotalMontantFactureAnneEnCours()
	{
		MacCustomer customer = (MacCustomer) getBean();
		if (customer != null)
		{
			int currentYear = Calendar.getInstance().get(Calendar.YEAR);
			setTotalMontantFactureAnneEnCours(customerServiceLocator.get().getTotalMontantFactureesParAnnee(customer, currentYear));
		}
	}

	/**
	 * Fetches total of this customer's invoices for last year.
	 */
	public void fetchTotalMontantFactureAnnePrecedente()
	{
		MacCustomer customer = (MacCustomer) getBean();
		if (customer != null)
		{
			int lastYear = Calendar.getInstance().get(Calendar.YEAR) - 1;
			setTotalMontantFactureAnnePrecedente(customerServiceLocator.get().getTotalMontantFactureesParAnnee(customer, lastYear));
		}
	}

	/**
	 * Fetches total of this customer's invoices for last last year.
	 */
	public void fetchTotalMontantFactureIlyaDeuxAns()
	{
		MacCustomer customer = (MacCustomer) getBean();
		if (customer != null)
		{
			int lastLastYear = Calendar.getInstance().get(Calendar.YEAR) - 2;
			setTotalMontantFactureIlyaDeuxAns(customerServiceLocator.get().getTotalMontantFactureesParAnnee(customer, lastLastYear));
		}
	}
}
