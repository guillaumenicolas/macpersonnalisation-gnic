package com.mac.custom.form;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;

import com.google.common.collect.Lists;
import com.netappsid.erp.client.gui.ProductSupplierFormPanel;
import com.netappsid.erp.server.bo.CommercialEntity;
import com.netappsid.erp.server.bo.Company;
import com.netappsid.erp.server.bo.Document;
import com.netappsid.erp.server.bo.Product;
import com.netappsid.framework.gui.components.model.table.ColumnDescriptor;
import com.netappsid.resources.Translator;

/**
 * A custom Mac class that extends {@link ProductSupplierFormPanel}, so that it can have custom behavior such as having additional columns in its table.
 * 
 * @author ftaillefer
 * 
 * @param <T>
 *            The same type as in superclass {@link ProductSupplierFormPanel}.
 */
public class MacProductSupplierFormPanel<T extends CommercialEntity> extends ProductSupplierFormPanel<T>
{

	public MacProductSupplierFormPanel(T commercialEntity, Company company, Locale locale, List<Class<? extends Product>> productClasses, Document document)
	{
		super(commercialEntity, company, locale, productClasses, document);
	}

	@Override
	protected List<Object[]> getProductSuppliers(Locale locale, Serializable commercialEntityId, Serializable companyId, Class<? extends Document> documentClass)
	{
		// Call a query with one extra field
		return productServicesServiceLocator.get().getProductSuppliers(locale, commercialEntityId, companyId, documentClass,
				Lists.newArrayList("product.ancienCodeArticle"));
	}

	@Override
	protected List<ColumnDescriptor> buildColumnDescriptors()
	{
		List<ColumnDescriptor> columnDescriptors = super.buildColumnDescriptors();

		columnDescriptors.add(new ColumnDescriptor(Translator.getString("ancienCodeArticle"), String.class));
		return columnDescriptors;
	}

	@Override
	protected List<Integer> getQuickFilterPaneColumnIndexes()
	{
		List<Integer> indexes = super.getQuickFilterPaneColumnIndexes();
		indexes.add(getColumnDescriptorIndex(Translator.getString("ancienCodeArticle")));

		return indexes;
	}

	@Override
	protected List<Integer> getQuickFilterFieldColumnIndexes()
	{
		List<Integer> indexes = super.getQuickFilterFieldColumnIndexes();
		indexes.add(getColumnDescriptorIndex(Translator.getString("ancienCodeArticle")));

		return indexes;
	}

}
