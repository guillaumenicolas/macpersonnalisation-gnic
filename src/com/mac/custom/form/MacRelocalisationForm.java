package com.mac.custom.form;

import java.awt.Component;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.mac.custom.bo.model.MacRelocalisationFormModel;
import com.netappsid.component.swing.ForeignKeyPanelImpl;
import com.netappsid.component.swing.NAIDComponentImpl;
import com.netappsid.erp.client.gui.components.MeasureUnitField;
import com.netappsid.erp.server.bo.Company;
import com.netappsid.erp.server.bo.InventoryProduct;
import com.netappsid.erp.server.bo.Relocation;
import com.netappsid.erp.server.bo.RelocationDetail;
import com.netappsid.erp.server.bo.SaleTransactionDetail;
import com.netappsid.erp.server.bo.User;
import com.netappsid.erp.server.bo.Warehouse;
import com.netappsid.erp.server.datatypes.GroupMeasureValue;
import com.netappsid.erp.server.model.MassInventoryAdjustment;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;
import com.netappsid.framework.gui.formbuilder.AbstractForm;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.Loader;
import com.netappsid.utils.ComponentUtils;

public class MacRelocalisationForm extends AbstractForm implements ChangeListener
{
	private MacRelocalisationFormModel macRelocalisationModel;
	private RelocationDetail relocationDetail;
	private User user;
	private Company company;
	private Warehouse warehouse;
	private Date relocationDate;
	private String barCode;
	private Relocation relocation;
	private NAIDPresentationModel relocalisationFormModel;

	@Override
	public void buildPanel()
	{

		super.buildPanel();
		initializePresentationModel();
		initializeRelocation();
		initializeKeyboardFocusManager();
	}

	public void setInventoryProduct(List inventoryProducts)
	{
		macRelocalisationModel.setInventoryProduct((InventoryProduct) inventoryProducts.get(0));
	}

	public String getBarCode()
	{
		if (macRelocalisationModel.getInventoryProduct() != null)
		{
			return macRelocalisationModel.getInventoryProduct().getCode();
		}
		else
			return null;
	}

	private void initializeKeyboardFocusManager()
	{
		final ServiceLocator loaderLocator = new ServiceLocator(LoaderBean.class);
		final MacRelocalisationForm finalMacRelocalisationForm = this;

		KeyboardFocusManager.getCurrentKeyboardFocusManager().addPropertyChangeListener("focusOwner", new PropertyChangeListener()
			{
				final KeyListener keyListener = new KeyListener()
					{
						private String input = "";
						private SaleTransactionDetail saleInvoiceDetail;

						@Override
						public void keyPressed(KeyEvent e)
						{}

						@Override
						public void keyReleased(KeyEvent e)
						{}

						@Override
						public void keyTyped(KeyEvent e)
						{

							if ((e.getSource() instanceof JTextField))
							{
								JTextField field = (JTextField) e.getSource();

								if ("quantity".equals(field.getName()))
								{
									if (e.getKeyChar() == '\n')
									{
										BigDecimal quantite = macRelocalisationModel.getQuantity().getValue();
										InventoryProduct inventoryProduct = macRelocalisationModel.getInventoryProduct();
										if (quantite != null & inventoryProduct != null & !quantite.equals(BigDecimal.ZERO))
										{
											GroupMeasureValue quantity = new GroupMeasureValue(inventoryProduct.getInventoryMeasure(), BigDecimal.ZERO);
											// finalMassInventoryAdjustment.setInventoryProduct(inventoryProduct);
											quantity.setValue(quantite);
											macRelocalisationModel.setQuantity(quantity);
										}
										SwingUtilities.invokeLater(new Runnable()
											{

												@Override
												public void run()
												{
													NAIDComponentImpl relocationDetailFrom = (NAIDComponentImpl) getComponent("relocationDetailFrom");

													ForeignKeyPanelImpl relocationDetailFromField = (ForeignKeyPanelImpl) relocationDetailFrom.getField();
													relocationDetailFromField.requestFocusInWindow();

													// ((JButton) getComponent("addAdjustment")).doClick(); } }); }
												}
											});
									}
								}

								if ("barcode".equals(field.getParent().getParent().getName()))

								{
									String productUPCCode = "";

									if ((e.getKeyChar() != '\n') && (e.getKeyChar() != '\b') && (e.getKeyChar() != ''))
									{
										this.input += e.getKeyChar();
									}
									else if (e.getKeyChar() == '\n')
									{
										if (macRelocalisationModel.getBarcode() != null)
										{
											this.input = macRelocalisationModel.getBarcode().toUpperCase();

											productUPCCode = getFollowedString("NAIDUPC-", this.input);

											if (productUPCCode != null)
											{
												List inventoryProducts = ((Loader) loaderLocator.get()).findByField(InventoryProduct.class,
														MassInventoryAdjustment.class, "upcCode", productUPCCode);

												if (!inventoryProducts.isEmpty())
												{
													setInventoryProduct(inventoryProducts);
													macRelocalisationModel.setQuantity(null);
												}
												else
												{
													macRelocalisationModel.setInventoryProduct(null);

												}
											}

											macRelocalisationModel.setBarcode(null);

											SwingUtilities.invokeLater(new Runnable()
												{
													@Override
													public void run()
													{
														NAIDComponentImpl quantity = (NAIDComponentImpl) getComponent("quantity");
														MeasureUnitField quantityField = (MeasureUnitField) quantity.getField();

														quantityField.requestQuantityFocus();
													}
												});
											this.input = "";
										}
									}
								}
							}
						}

					};

				@Override
				public void propertyChange(PropertyChangeEvent evt)
				{
					Component focusOwner = (Component) evt.getNewValue();
					Component oldFocusOwner = (Component) evt.getOldValue();

					if (oldFocusOwner != null)
					{
						oldFocusOwner.removePropertyChangeListener(this);
					}

					MacRelocalisationForm form = ComponentUtils.getContainer(focusOwner, MacRelocalisationForm.class);

					if ((form == null) && (focusOwner != null))
					{
						form = (MacRelocalisationForm) ComponentUtils.getComponent((java.awt.Container) focusOwner, MacRelocalisationForm.class);
					}

					if (finalMacRelocalisationForm == form)
					{
						if (oldFocusOwner != null)
						{
							oldFocusOwner.removeKeyListener(this.keyListener);
						}
						focusOwner.removeKeyListener(this.keyListener);
						focusOwner.addKeyListener(this.keyListener);
					}
				}
			});
	}

	public void initializeRelocation()
	{
		this.macRelocalisationModel = (MacRelocalisationFormModel) this.relocalisationFormModel.getBean();
	}

	public void initializePresentationModel()
	{
		this.relocalisationFormModel = getPresentationModel("relocalisationFormModel");

	}

	protected String getFollowedString(String prefix, String input)
	{
		return input;
	}

	@Override
	public void stateChanged(ChangeEvent e)
	{
		// TODO Auto-generated method stub

	}
}