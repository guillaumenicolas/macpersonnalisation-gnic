package com.mac.custom.form;

import resources.forms.classes.WorkOrderPrintingForm;

import com.mac.custom.listmodel.MacWorkOrderRoutingStepList;
import com.netappsid.erp.server.listmodel.WorkOrderRoutingStepList;

public class MacWorkOrderPrintingFormCustom extends WorkOrderPrintingForm
{
	@Override
	protected WorkOrderRoutingStepList createWorkOrderRoutingStepList()
	{
		return new MacWorkOrderRoutingStepList();
	}
}
