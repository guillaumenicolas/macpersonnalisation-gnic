package com.mac.custom.form;

import java.awt.Component;

import resources.forms.classes.StocktakingForm;

import com.mac.custom.services.beans.MacStocktakingServicesBean;
import com.mac.custom.services.interfaces.MacStocktakingServices;
import com.netappsid.erp.server.bo.Stocktaking;
import com.netappsid.framework.utils.ServiceLocator;

public class MacStocktakingFormCustom extends StocktakingForm
{
	private final ServiceLocator<MacStocktakingServices> stocktakingServicesServiceLocator = new ServiceLocator<MacStocktakingServices>(
			MacStocktakingServicesBean.class);

	@Override
	public void afterAction_ApplyStocktaking()
	{
		stocktakingServicesServiceLocator.get().checkAndUpdateCloseStatus(((Stocktaking) getCurrentBean()).getId());

		super.afterAction_ApplyStocktaking();
	}

	@Override
	public void buildPanel()
	{
		super.buildPanel();

		activateDeleteAndSave();
	}

	@Override
	protected void afterBeanChanged()
	{
		super.afterBeanChanged();

		activateDeleteAndSave();
	}

	public void activateDeleteAndSave()
	{
		Stocktaking stocktaking = getModelBean(getModelName());
		boolean enable;
		if (stocktaking != null && stocktaking.getDocumentStatus() != null && stocktaking.getDocumentStatus().isClose())
		{
			enable = false;
		}
		else
		{
			enable = true;
		}
		stocktaking.setModelReadOnly(!enable);
		safeEnable(enable, "grid_ObjectCriteria", "grid_StocktakingDetailsByTag", "grid_StocktakingDetails", "grid_StocktakingMeasures",
				"action_AddStocktakingDetailMeasure", "action_AddStocktakingDetail", "action_AddStocktakingCriterion");
	}

	private void safeEnable(boolean enable, String... componentsName)
	{
		for (String componentName : componentsName)
		{
			Component component = getComponent(componentName);
			if (component != null)
			{
				component.setEnabled(enable);
			}
		}
	}
}
