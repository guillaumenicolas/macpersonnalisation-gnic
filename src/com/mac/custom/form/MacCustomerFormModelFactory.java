package com.mac.custom.form;

import com.netappsid.erp.server.model.form.CustomerFormModelFactory;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;

public class MacCustomerFormModelFactory extends CustomerFormModelFactory
{
	@Override
	public MacCustomerFormModel createFormModel(NAIDPresentationModel businessObjectModel)
	{
		MacCustomerFormModel customerFormModel = new MacCustomerFormModel(businessObjectModel);
		return customerFormModel;
	}
}
