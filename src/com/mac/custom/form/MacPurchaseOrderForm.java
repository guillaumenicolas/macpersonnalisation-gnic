package com.mac.custom.form;

import java.awt.Color;
import java.util.List;
import java.awt.Component;

import resources.forms.classes.PurchaseOrderForm;

import com.jidesoft.swing.JideTabbedPane;
import com.mac.custom.bo.MacPurchaseOrder;
import com.netappsid.erp.server.bo.SaleTransactionOrigin;
import com.netappsid.erp.server.bo.SaleType;
import com.netappsid.erp.server.model.form.OrderFormModel;
import com.netappsid.erp.server.model.form.PurchaseOrderFormModel;
import com.netappsid.utils.ComponentUtils;

public class MacPurchaseOrderForm extends PurchaseOrderForm
{
	private Component modifySupplier;

	// Ajout OGA du 24/07/14 - Permet de recuperer les onglets a colorer
	private void initializeTabsColor()
	{
		if (getDetailTab() != null)
		{
			JideTabbedPane parentTable = (JideTabbedPane) getDetailTab();

			// Recuperation des objets de order
			MacPurchaseOrder purchaseOrder = getCurrentBean();

			// Verification si onglet pieces jointes renseignes auquel cas le
			// colore en ORANGE
			if (!purchaseOrder.getAttachments().isEmpty())
			{
				paintTab(parentTable, "tab_attachment", Color.ORANGE);
			}
			else
			{
				paintTab(parentTable, "tab_attachment", null);
			}

			// Verification si onglet notes renseignees auquel cas le
			// colore en ORANGE
			if (!purchaseOrder.getNotes().isEmpty())
			{
				paintTab(parentTable, "tab_notes", Color.ORANGE);
			}
			else
			{
				paintTab(parentTable, "tab_notes", null);
			}
		}
	}

	// Ajout OGA du 24/07/14 - Permet de colorer les onglets precedemment selectionnes
	private void paintTab(JideTabbedPane parentTable, String tabKey, Color color)
	{
		if (parentTable != null && tabKey != null)
		{
			int tabIndex = parentTable.indexOfTab(getString(tabKey));

			if (tabIndex != -1)
			{
				getDetailTab().setBackgroundAt(tabIndex, color);
			}
		}
	}

	@Override
	protected void afterBeanChanged()
	{
		super.afterBeanChanged();

		// Ajout OGA du 24/07/14 - Fonction pour coloriser le ou les onglet(s)
		initializeTabsColor();

		if (modifySupplier != null)
		{
			ComponentUtils.setComponentHierarchyEnabled(modifySupplier, true);
		}
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		MacPurchaseOrder purchaseOrder = getCurrentBean();

		if (purchaseOrder != null && purchaseOrder.getId() == null && purchaseOrder.isReadyForModifications())
		{
			// Initialiser le champ Tout Afficher à 1 (cocher par defaut)
			PurchaseOrderFormModel purchaseorderFormModel = getModelBean("purchaseOrderFormModel");
			purchaseorderFormModel.setShowAll(true);

			purchaseOrder.setDirty(false);
		}
	}
	@Override
	public void buildPanel()
	{
		super.buildPanel();

		modifySupplier = getComponent("modifysupplier");
	}
}
