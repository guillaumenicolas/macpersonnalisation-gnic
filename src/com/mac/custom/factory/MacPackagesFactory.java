package com.mac.custom.factory;

import com.netappsid.erp.server.factory.PackagesFactory;
import com.netappsid.erp.server.factory.initializer.ItemToShipInitializer;

public class MacPackagesFactory extends PackagesFactory
{
	public MacPackagesFactory(ItemToShipInitializer itemToShipInitializer)
	{
		super(itemToShipInitializer);
	}

}
