package com.mac.custom.factory;

import java.io.Serializable;
import java.util.Date;

import com.mac.custom.bo.WorkLoad;
import com.netappsid.erp.server.bo.Resource;
import com.netappsid.factory.BOFactory;
import com.netappsid.factory.utils.FactoryUtils;

public class WorkLoadFactory extends BOFactory implements Serializable
{
	@Override
	public WorkLoad create()
	{
		return (WorkLoad) FactoryUtils.getBOEntity(WorkLoad.class);
	}

	public WorkLoad create(Date date, Resource resource)
	{
		WorkLoad workLoad = create();
		workLoad.setDate(date);
		workLoad.setResource(resource);

		return workLoad;
	}
}
