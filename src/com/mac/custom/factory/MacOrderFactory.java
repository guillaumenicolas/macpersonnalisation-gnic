package com.mac.custom.factory;

import com.mac.custom.bo.MacOrder;
import com.netappsid.erp.server.bo.Quotation;
import com.netappsid.erp.server.bo.User;
import com.netappsid.europe.naid.custom.factory.OrderFactoryEuro;

public class MacOrderFactory extends OrderFactoryEuro
{
	@Override
	public MacOrder createFromQuotation(Quotation quotation, User user)
	{
		MacOrder order = (MacOrder) super.createFromQuotation(quotation, user);
		order.setShipASAP(true);
		return order;
	}
}
