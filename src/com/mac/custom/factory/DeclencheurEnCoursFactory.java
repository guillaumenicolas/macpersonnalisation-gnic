package com.mac.custom.factory;

import java.io.Serializable;

import com.mac.custom.bo.DeclencheurEnCours;
import com.netappsid.factory.BOFactory;
import com.netappsid.factory.utils.FactoryUtils;

public class DeclencheurEnCoursFactory extends BOFactory
{
	@Override
	public DeclencheurEnCours create()
	{
		return (DeclencheurEnCours) FactoryUtils.getBOEntity(DeclencheurEnCours.class);
	}

	public DeclencheurEnCours create(String entityClassName, Serializable entityId)
	{
		DeclencheurEnCours declencheurEnCours = new DeclencheurEnCours();
		declencheurEnCours.setEntityClassName(entityClassName);
		declencheurEnCours.setEntityId(entityId);
		return declencheurEnCours;
	}
}
