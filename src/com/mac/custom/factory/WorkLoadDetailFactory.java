package com.mac.custom.factory;

import com.mac.custom.bo.WorkLoadDetail;
import com.netappsid.factory.BOFactory;
import com.netappsid.factory.utils.FactoryUtils;

public class WorkLoadDetailFactory extends BOFactory
{
	@Override
	public WorkLoadDetail create()
	{
		return (WorkLoadDetail) FactoryUtils.getBOEntity(WorkLoadDetail.class);
	}
}
