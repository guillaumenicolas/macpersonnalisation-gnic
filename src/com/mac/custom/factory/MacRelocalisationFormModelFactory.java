package com.mac.custom.factory;

import com.mac.custom.bo.model.MacRelocalisationFormModel;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;
import com.netappsid.framework.gui.formbuilder.formmodel.FormModelFactory;

public class MacRelocalisationFormModelFactory<T extends MacRelocalisationFormModel> extends FormModelFactory<MacRelocalisationFormModel>
{
	@Override
	public MacRelocalisationFormModel createFormModel(NAIDPresentationModel businessObjectModel)
	{
		return new MacRelocalisationFormModel(businessObjectModel);
	}

}
