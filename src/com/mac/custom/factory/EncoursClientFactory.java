package com.mac.custom.factory;

import java.io.Serializable;

import com.mac.custom.bo.MacCustomer;
import com.mac.custom.bo.model.EncoursClient;
import com.mac.custom.services.customer.beans.MacCustomerServicesBean;
import com.mac.custom.services.customer.interfaces.MacCustomerServices;
import com.netappsid.datatypes.MonetaryAmount;
import com.netappsid.framework.utils.ServiceLocator;

public class EncoursClientFactory implements Serializable
{
	private final ServiceLocator<MacCustomerServices> customerServiceLocator = new ServiceLocator<MacCustomerServices>(MacCustomerServicesBean.class);

	public EncoursClient create(MacCustomer macCustomer)
	{
		return create(macCustomer, null);
	}

	public EncoursClient create(MacCustomer macCustomer, Serializable orderToIgnoreId)
	{
		EncoursClient encoursClient = new EncoursClient();

		encoursClient.setTotalInvoicesNotPaid(getMacCustomerService().getTotalInvoicesNotPaid(macCustomer));
		encoursClient.setTotalShippedNotInvoiced(getMacCustomerService().getTotalShippedNotInvoiced(macCustomer));
		encoursClient.setTotalBlockedOrdersNotProductionNotShipped(getMacCustomerService().getTotalAmountOfOrdersByCriteria(macCustomer, false, true, false,
				false, orderToIgnoreId));
		encoursClient.setTotalNotBlockedOrdersNotProductionNotShipped(getMacCustomerService().getTotalAmountOfOrdersByCriteria(macCustomer, false, false,
				false, false, orderToIgnoreId));
		encoursClient.setTotalBlockedOrdersProductionNotShipped(getMacCustomerService().getTotalAmountOfOrdersByCriteria(macCustomer, false, true, true, false,
				orderToIgnoreId));
		encoursClient.setTotalNotBlockedOrdersProductionNotShipped(getMacCustomerService().getTotalAmountOfOrdersByCriteria(macCustomer, false, false, true,
				false, orderToIgnoreId));
		encoursClient.setTotalOrdersNewIncomplete(getMacCustomerService()
				.getTotalAmountOfOrdersByCriteria(macCustomer, true, null, null, null, orderToIgnoreId));

		encoursClient.setEncoursAccepte(calculateEncoursAccepte(macCustomer));
		encoursClient.setEncoursFinancier(calculateEncoursFinancier(macCustomer, encoursClient));
		encoursClient.setEncoursNonFacture(calculateEncoursNonFacture(macCustomer, encoursClient));
		encoursClient.setEncoursTotal(calculateEncoursTotal(macCustomer, encoursClient));
		encoursClient.setDepassement(calculateDepassement(macCustomer, encoursClient));

		return encoursClient;
	}

	private MonetaryAmount calculateEncoursAccepte(MacCustomer macCustomer)
	{
		MonetaryAmount encoursAccepte = new MonetaryAmount(macCustomer.getCurrency().getCode());

		if (macCustomer.getMacRiskAmount() != null)
		{
			encoursAccepte = encoursAccepte.add(macCustomer.getMacRiskAmount());
		}
		if (macCustomer.getSfacRiskAmount() != null)
		{
			encoursAccepte = encoursAccepte.add(macCustomer.getSfacRiskAmount());
		}

		return encoursAccepte;
	}

	private MonetaryAmount calculateEncoursFinancier(MacCustomer macCustomer, EncoursClient encoursClient)
	{
		MonetaryAmount encoursFinancier = new MonetaryAmount(macCustomer.getCurrency().getCode());
		if (macCustomer.getLimitInvoicesNotPaid() != null)
		{
			encoursFinancier = encoursFinancier.add(macCustomer.getLimitInvoicesNotPaid());
		}
		encoursFinancier = encoursFinancier.add(encoursClient.getTotalInvoicesNotPaid());

		return encoursFinancier;
	}

	private MonetaryAmount calculateEncoursNonFacture(MacCustomer macCustomer, EncoursClient encoursClient)
	{
		MonetaryAmount encoursNonFacture = new MonetaryAmount(macCustomer.getCurrency().getCode());

		encoursNonFacture = encoursNonFacture.add(encoursClient.getTotalBlockedOrdersProductionNotShipped());
		encoursNonFacture = encoursNonFacture.add(encoursClient.getTotalNotBlockedOrdersNotProductionNotShipped());
		encoursNonFacture = encoursNonFacture.add(encoursClient.getTotalNotBlockedOrdersProductionNotShipped());
		encoursNonFacture = encoursNonFacture.add(encoursClient.getTotalShippedNotInvoiced());

		return encoursNonFacture;
	}

	private MonetaryAmount calculateEncoursTotal(MacCustomer macCustomer, EncoursClient encoursClient)
	{
		MonetaryAmount encoursTotal = new MonetaryAmount(macCustomer.getCurrency().getCode());

		encoursTotal = encoursTotal.add(encoursClient.getEncoursFinancier());
		encoursTotal = encoursTotal.add(encoursClient.getEncoursNonFacture());

		return encoursTotal;
	}

	private MonetaryAmount calculateDepassement(MacCustomer macCustomer, EncoursClient encoursClient)
	{
		MonetaryAmount depassement = new MonetaryAmount(macCustomer.getCurrency().getCode());

		depassement = depassement.add(encoursClient.getEncoursTotal());
		depassement = depassement.subtract(encoursClient.getEncoursAccepte());

		return depassement;
	}

	private MacCustomerServices getMacCustomerService()
	{
		return customerServiceLocator.get();
	}
}
