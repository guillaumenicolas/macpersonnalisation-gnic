package com.mac.custom.factory;

import java.util.Date;
import java.util.List;

import com.mac.custom.bo.MacOrder;
import com.mac.custom.bo.MacSaleInvoice;
import com.netappsid.bo.model.Model;
import com.netappsid.erp.server.bo.AccountingPeriod;
import com.netappsid.erp.server.bo.Deposit;
import com.netappsid.erp.server.bo.SaleInvoice;
import com.netappsid.erp.server.bo.Shipping;
import com.netappsid.erp.server.bo.ShippingDetail;
import com.netappsid.erp.server.bo.User;
import com.netappsid.erp.server.factory.SaleInvoiceFactory;
import com.netappsid.erp.server.services.interfaces.local.DocumentStatusServicesLocal;

public class MacSaleInvoiceFactory extends SaleInvoiceFactory
{
	@Override
	public SaleInvoice create(Shipping shipping, User user, Date invoiceDate, AccountingPeriod accountingPeriod, Deposit deposit,
			List<ShippingDetail> shippingDetailsNotInvoiced, boolean generateFeeAndDiscount, DocumentStatusServicesLocal documentStatusServices)
	{
		MacSaleInvoice macSaleInvoice = (MacSaleInvoice) super.create(shipping, user, invoiceDate, accountingPeriod, deposit, shippingDetailsNotInvoiced,
				generateFeeAndDiscount, documentStatusServices);
		MacOrder macOrder = (MacOrder) Model.getTarget(shipping.getOrder());
		macSaleInvoice.setServiceType(macOrder.getServiceType());
		macSaleInvoice.setOriginalOrderNumber(macOrder.getOriginalOrderNumber());
		return macSaleInvoice;
	}
}
