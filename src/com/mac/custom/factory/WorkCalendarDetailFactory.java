package com.mac.custom.factory;

import java.io.Serializable;

import com.mac.custom.bo.WorkCalendarDetail;
import com.mac.custom.bo.WorkLoad;
import com.netappsid.erp.server.bo.ResourceEfficiency;

public class WorkCalendarDetailFactory implements Serializable
{
	public WorkCalendarDetail create(WorkLoad workLoad, ResourceEfficiency resourceEfficiency)
	{
		WorkCalendarDetail workCalendarDetail = new WorkCalendarDetail();
		workCalendarDetail.setWorkLoad(workLoad);
		workCalendarDetail.setResourceEfficiency(resourceEfficiency);

		return workCalendarDetail;
	}
}
