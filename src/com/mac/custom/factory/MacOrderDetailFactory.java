package com.mac.custom.factory;

import com.netappsid.erp.server.bo.OrderDetail;
import com.netappsid.erp.server.bo.TransactionDetail;
import com.netappsid.europe.naid.custom.factory.OrderDetailFactoryEuro;
import java.util.List;

public class MacOrderDetailFactory extends OrderDetailFactoryEuro
{
	@Override
	public OrderDetail create(TransactionDetail originalDetail)
	{
		OrderDetail detail = super.create(originalDetail);

		return detail;
	}

	@Override
	public OrderDetail clone(TransactionDetail detail, List<String> fieldExclude, boolean keepDiscount)
	{
		OrderDetail cloneDetail = super.clone(detail, fieldExclude, keepDiscount);

		return cloneDetail;
	}

}
