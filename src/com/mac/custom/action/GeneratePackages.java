package com.mac.custom.action;

import java.awt.event.ActionEvent;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.print.PrintServiceLookup;

import net.sf.jasperreports.engine.JasperPrint;

import org.hibernate.Hibernate;
import org.xml.sax.Attributes;

import com.jgoodies.validation.ValidationResult;
import com.mac.custom.bo.MacOrder;
import com.mac.custom.bo.model.AutomaticPackages;
import com.mac.custom.bo.model.AutomaticPackagesItemToShipWrapper;
import com.netappsid.action.AbstractBoundAction;
import com.netappsid.bo.Entity;
import com.netappsid.bo.model.Model;
import com.netappsid.component.ExpandableDialogData;
import com.netappsid.component.OptionPane;
import com.netappsid.component.OptionPane.MessageType;
import com.netappsid.erp.client.action.Print;
import com.netappsid.erp.client.utils.ERPSystemVariable;
import com.netappsid.erp.client.utils.Icon;
import com.netappsid.erp.server.bo.DocumentStatus;
import com.netappsid.erp.server.bo.ItemToShip;
import com.netappsid.erp.server.bo.OrderDetailToShip;
import com.netappsid.erp.server.bo.Packages;
import com.netappsid.erp.server.bo.ReportDetail;
import com.netappsid.erp.server.bo.ReportType;
import com.netappsid.erp.server.bo.User;
import com.netappsid.erp.server.datatypes.GroupMeasureValue;
import com.netappsid.erp.server.factory.ItemToShipFactory;
import com.netappsid.erp.server.factory.PackagesFactory;
import com.netappsid.erp.server.factory.initializer.ItemToShipInitializer;
import com.netappsid.erp.server.services.beans.ReportingServicesBean;
import com.netappsid.erp.server.services.interfaces.remote.ReportingServicesRemote;
import com.netappsid.erp.server.services.resultholders.PrintResult;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.framework.utils.SystemVariable;
import com.netappsid.resources.Translator;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.beans.PersistenceBean;
import com.netappsid.services.interfaces.Loader;
import com.netappsid.services.interfaces.Persistence;

public class GeneratePackages extends AbstractBoundAction
{
	private final ServiceLocator<Persistence> persistenceServiceLocator = new ServiceLocator<Persistence>(PersistenceBean.class);
	private final ServiceLocator<Loader> loaderServicesServiceLocator = new ServiceLocator<Loader>(LoaderBean.class);
	private final ServiceLocator<ReportingServicesRemote> reportServicesLocator = new ServiceLocator<ReportingServicesRemote>(ReportingServicesBean.class);
	private static final String PRINT_LABEL = Translator.getString("Print");

	public GeneratePackages(Attributes attributes)
	{
		super(attributes);
		setForm("automaticPackages");
		putValue(PROPERTY_ICON, Icon.ITEMTOSHIP.getIcon(PROPERTY_DEFAULT_ICON_SIZE));
		putValue(PROPERTY_TOOLTIP_TEXT, Translator.getString("AutomaticPackagesGeneration"));
	}

	@Override
	public void doAction(ActionEvent e)
	{
		AutomaticPackages automaticPackages = (AutomaticPackages) getModel().getBean();

		if (automaticPackages != null)
		{
			ValidationResult result = automaticPackages.validate();
			if (!result.equals(ValidationResult.EMPTY))
			{
				getOptionPane().show(getDocumentPane(), result.getMessagesText(), getLocalizedTitle(), OptionPane.MessageType.ERROR);
			}
			else
			{
				processPackages(automaticPackages);

				// create a new AutomaticPackages with the same weight group measure
				AutomaticPackages newAutomaticPackages = new AutomaticPackages();
				newAutomaticPackages.setWeight(new GroupMeasureValue(automaticPackages.getWeightGroupMeasure()));
				getModel().setBean(newAutomaticPackages);
			}
		}
	}

	public void processPackages(AutomaticPackages automaticPackages)
	{
		MacOrder order = automaticPackages.getOrder();
		List<Packages> oldPackages = getPackagesListFromOrder(order);
		generatePackages(automaticPackages);
		MacOrder savedMacOrder = persistenceServiceLocator.get().save(order, true);
		List<Packages> newPackages = getPackagesListFromOrder(savedMacOrder);

		String title = Translator.getString("AutomaticPackagesGeneration");
		String message = Translator.getString("PackagesGenerated");
		String expandedMessage = "";
		MessageType messageType = MessageType.INFORMATION;

		ExpandableDialogData expandableDialogData = new ExpandableDialogData(title, message, messageType, expandedMessage);
		getOptionPane().showExpandableMessages(expandableDialogData);
		printPackages(getPackagesListDiff(oldPackages, newPackages));
	}

	private List<Packages> getPackagesListFromOrder(MacOrder order)
	{
		List<Packages> packages = new ArrayList<Packages>();

		for (ItemToShip itemToShip : order.getItemsToShip())
		{
			if (itemToShip instanceof Packages)
			{
				packages.add((Packages) itemToShip);
			}
		}
		return packages;
	}

	private List<Packages> getPackagesListDiff(List<Packages> oldPackages, List<Packages> newPackages)
	{
		List<Packages> packages = new ArrayList<Packages>();
		for (Packages newPackage : newPackages)
		{
			boolean foundPackage = false;
			for (Packages oldPackage : oldPackages)
			{
				if (oldPackage.hasSameID(newPackage))
				{
					foundPackage = true;
					break;
				}
			}
			if (!foundPackage)
			{
				packages.add(newPackage);
			}
		}

		return packages;
	}

	public void generatePackages(AutomaticPackages automaticPackages)
	{
		ItemToShipInitializer itemToShipInitializer = new ItemToShipInitializer();
		PackagesFactory packagesFactory = new PackagesFactory(itemToShipInitializer);

		Long numberPackages = automaticPackages.getNumberPackages();
		GroupMeasureValue weight = automaticPackages.getWeight();
		MacOrder order = automaticPackages.getOrder();

		if (numberPackages > 0)
		{
			Packages packages;
			BigDecimal divideWeight = weight.getValue().divide(new BigDecimal(numberPackages), RoundingMode.HALF_UP);
			Packages lastPackage = null;

			// Accumuler les items � placer dans des colis
			List<AutomaticPackagesItemToShipWrapper> itemToShipWrappers = automaticPackages.getItemToShipWrappers();
			List<ItemToShip> itemToShips = new ArrayList<ItemToShip>();
			ItemToShipFactory factory = null;
			for (AutomaticPackagesItemToShipWrapper itemToShipWrapper : itemToShipWrappers)
			{
				GroupMeasureValue quantityToShip = itemToShipWrapper.getQuantityToShip();
				if (!quantityToShip.isGreaterThanZero())
				{
					continue;
				}
				List<ItemToShip> wrapperItemsToShip = itemToShipWrapper.getItemsToShip();
				for (int i = 0; i < wrapperItemsToShip.size(); ++i)
				{
					ItemToShip itemToShip = wrapperItemsToShip.get(i);
					if (OrderDetailToShip.class.isAssignableFrom(Hibernate.getClass(itemToShip)))
					{
						OrderDetailToShip detailToShip = (OrderDetailToShip) Model.getTarget(itemToShip);
						if (quantityToShip.compareTo(itemToShip.getQuantityToShip()) < 0)
						{
							// L'item a une trop grande quantit�, il faut s�parer l'ItemToShip en deux
							if (factory == null)
							{
								factory = new ItemToShipFactory();
							}

							OrderDetailToShip copy = (OrderDetailToShip) factory.copy(detailToShip);
							copy.setOrderDetail(detailToShip.getOrderDetail());
							copy.setCode(detailToShip.getCode());
							copy.setShippingDate(detailToShip.getShippingDate());
							copy.setProductRevision(detailToShip.getProductRevision());
							copy.setCommercialEntity(detailToShip.getCommercialEntity());
							wrapperItemsToShip.add(copy);
							order.addToItemsToShip(copy);

							copy.setQuantity(itemToShip.getQuantityToShip().substract(quantityToShip));
							detailToShip.setQuantity(quantityToShip);
						}
						itemToShips.add(itemToShip);
						quantityToShip = quantityToShip.substract(itemToShip.getQuantityToShip());
						if (!quantityToShip.isGreaterThanZero())
						{
							break;
						}
					}
				}
			}

			/*
			 * 3 Cas possible pour la generation de colis
			 * 
			 * (1) Autant de colis que d'itemToShip, donc il y aura 1 ITS dans chacun des colis
			 * 
			 * (2) Plus de colis que d'itemToShip, donc il y aura des colis vides quand tous les ITS sont dans leur colis
			 * 
			 * (3) Moins de colis que d'ItemToShip, donc les ITS auront chacun leur colis jusqu'a ce qu'il n'y ait plus de colis disponible, le restant des ITS
			 * seront alors ajoutés au dernier colis
			 */

			for (int i = 0; i < itemToShips.size(); i++)
			{
				ItemToShip its = itemToShips.get(i);
				if (numberPackages <= i)
				{
					lastPackage.addToItemsToShip(its);
				}
				else
				{
					packages = packagesFactory.create(its, order);
					packages.setWeight(new GroupMeasureValue(weight, divideWeight));
					packages.addToItemsToShip(its);
					lastPackage = packages;
				}
			}

			// Generate empty packages
			for (int i = itemToShips.size(); i < numberPackages; i++)
			{
				packages = packagesFactory.create();
				packages.setOrder(order);
				packages.setWeight(new GroupMeasureValue(weight, divideWeight));
			}
		}
	}

	private void printPackages(List<Packages> packagesList)
	{
		if (!packagesList.isEmpty())
		{
			Print print = new Print();
			PrintResult allPrintResult = new PrintResult();

			final ReportType reportType = loaderServicesServiceLocator.get()
					.findByField(ReportType.class, ReportType.class, DocumentStatus.PROPERTYNAME_INTERNALID, ReportType.PACKAGE_ID).get(0);

			if (reportType.getDefaultReport() != null)
			{
				for (Packages packages : packagesList)
				{
					Serializable userId = ((User) SystemVariable.getVariable(ERPSystemVariable.USER)).getId();
					List<PrintResult> printResults = new ArrayList<PrintResult>();
					for (ReportDetail reportDetail : reportType.getDefaultReport().getDetails())
					{
						HashMap<String, Object> parameters = new HashMap<String, Object>();
						parameters.put(Entity.PROPERTYNAME_ID, packages.getId().toString());
						printResults.add(reportServicesLocator.get().print(reportDetail.getId(), parameters, userId));
					}

					for (PrintResult printResult : printResults)
					{
						for (JasperPrint jasperPrint : printResult.getResults())
						{
							print.processPrint(jasperPrint, PrintServiceLookup.lookupDefaultPrintService().getName(), null, 1);
						}
						allPrintResult.addAllFrom(printResult);
					}
				}
				showMessages(allPrintResult, packagesList.size(), packagesList.size());
			}
			else
			{
				allPrintResult.addError(Translator.getString("NODEFAULTREPORT"));
				showMessages(allPrintResult, 0, packagesList.size());
			}
		}
	}

	protected void showMessages(final PrintResult printResult, final Integer passed, final Integer total)
	{
		MessageType messageType;
		final String message = String.format(getString("HaveBeenSuccessfullySent"), passed, total);
		String messageText = null;
		if (printResult.hasErrors())
		{
			messageType = MessageType.ERROR;
			messageText = printResult.getErrorMessagesText(Translator.getDictionnary());
		}
		else if (printResult.hasWarnings())
		{
			messageType = MessageType.WARNING;
			messageText = printResult.getWarningMessagesText(Translator.getDictionnary());
		}
		else
		{
			messageType = MessageType.INFORMATION;
			messageText = getString("NoErrorOrWarningToSignal");
		}

		ExpandableDialogData expandableDialogData = new ExpandableDialogData(PRINT_LABEL, message, messageType, messageText);
		getOptionPane().showExpandableMessages(expandableDialogData);
	}
}
