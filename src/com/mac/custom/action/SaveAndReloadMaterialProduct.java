package com.mac.custom.action;

import org.xml.sax.Attributes;

import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.message.SimpleValidationMessage;
import com.mac.custom.bo.MacMaterialProduct;
import com.netappsid.bo.model.Model;
import com.netappsid.component.AsyncCallback;
import com.netappsid.component.OptionPane;
import com.netappsid.component.OptionPane.OptionResult;
import com.netappsid.erp.client.action.product.SaveAndReloadInventoryProduct;
import com.netappsid.resources.Translator;

public class SaveAndReloadMaterialProduct extends SaveAndReloadInventoryProduct
{
	public SaveAndReloadMaterialProduct(Attributes attributes)
	{
		super(attributes);
	}

	@Override
	protected ValidationResult validateModelBeforeSave()
	{
		final ValidationResult result = super.validateModelBeforeSave();

		if (result.equals(ValidationResult.EMPTY))
		{
			final MacMaterialProduct materialProduct = (MacMaterialProduct) Model.getTarget(getModel().getBean());
			if (materialProduct != null && materialProduct.getId() != null)
			{
				final MacMaterialProduct originalManufacturedProduct = getLoaderServiceLocator().get().findById(MacMaterialProduct.class, null,
						materialProduct.getId());
				if (originalManufacturedProduct != null && !originalManufacturedProduct.getCode().equals(materialProduct.getCode()))
				{
					// the code has been modified, warn the user
					getOptionPane().show(getDocumentPane(), Translator.getString("codeModifiedWarning"), Translator.getString("Warning"),
							com.netappsid.component.OptionPane.MessageType.QUESTION, OptionPane.Option.YES_NO, new AsyncCallback<OptionPane.OptionResult>()
								{
									@Override
									public void execute(OptionPane.OptionResult optionResult)
									{
										if (optionResult != OptionResult.YES)
										{
											result.add(new SimpleValidationMessage(getString("codeModifiedError"), Severity.ERROR));
										}
									}
								});
				}
			}
		}

		return result;
	}
}
