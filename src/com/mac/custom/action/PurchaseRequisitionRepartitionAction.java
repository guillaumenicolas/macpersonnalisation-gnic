package com.mac.custom.action;

import java.awt.event.ActionEvent;
import java.io.Serializable;
import java.util.List;

import javax.swing.JOptionPane;

import org.xml.sax.Attributes;

import com.mac.custom.bo.PurchaseRequisitionRepartition;
import com.mac.custom.services.purchaseRequisition.beans.MacPurchaseRequisitionServicesBean;
import com.mac.custom.services.purchaseRequisition.interfaces.remote.MacPurchaseRequisitionServicesRemote;
import com.netappsid.component.FormDialog.ButtonType;
import com.netappsid.erp.client.action.AbstractLockAction;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.gui.utils.WindowUtils.WindowPosition;
import com.netappsid.resources.Translator;

public class PurchaseRequisitionRepartitionAction extends AbstractLockAction
{
	public PurchaseRequisitionRepartitionAction(Attributes attributes)
	{
		super(attributes);
		putValue(PROPERTY_TOOLTIP_TEXT, "R\u00E9partition des offres d'achats");
		setTranslatorText("R\u00E9partition des offres d'achats");
	}

	@Override
	public void doLockAction(ActionEvent e)
	{
		final List<Serializable> businessObjectsID = getBusinessObjectSelector().getBusinessObjectsID();

		if (businessObjectsID.isEmpty())
		{
			JOptionPane.showMessageDialog(null, Translator.getString("NoLineSelected"), Translator.getString("ReverseTransaction"), 1);
		}
		else
		{
			MacPurchaseRequisitionServicesRemote purchaseRequisitionServicesRemote;
			try
			{
				ServiceLocator<MacPurchaseRequisitionServicesRemote> locator = new ServiceLocator<MacPurchaseRequisitionServicesRemote>(
						MacPurchaseRequisitionServicesBean.class);

				purchaseRequisitionServicesRemote = locator.get();
				PurchaseRequisitionRepartition purchaseRequisitionRepartition = purchaseRequisitionServicesRemote
						.generatePurchaesRequisitionRepartition(businessObjectsID);

				com.netappsid.component.FormDialog formDialog = getComponentFactory().newFormDialog(getDocumentPane(), "purchaseRequisitionRepartition",
						this.getClass().getClassLoader(), ButtonType.OK, ButtonType.CANCEL);

				NAIDPresentationModel foundModel = formDialog.getDocumentPane().getPresentationModel("purchaseRequisitionRepartition");
				foundModel.setBean(purchaseRequisitionRepartition);
				formDialog.setModal(true);
				formDialog.setPosition(WindowPosition.CENTER);
				formDialog.setTitle("R\u00E9partition des offres d'achats");
				formDialog.setModel(foundModel);
				formDialog.setVisible(true);
				formDialog.dispose();

				if (formDialog.getSelectedButton() == ButtonType.OK)
				{
					purchaseRequisitionServicesRemote.distributePurchaseRequisition(purchaseRequisitionRepartition);
				}

			}
			catch (Exception except)
			{
				except.printStackTrace();
			}
		}
	}
}
