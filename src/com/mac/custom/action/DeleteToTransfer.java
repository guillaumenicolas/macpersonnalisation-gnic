package com.mac.custom.action;

import org.xml.sax.Attributes;

import com.netappsid.action.Delete;
import com.netappsid.bo.model.Model;
import com.netappsid.erp.server.bo.Document;
import com.netappsid.event.DirtyStateEvent;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;

public class DeleteToTransfer extends Delete
{
	private boolean disabledDirtyStateChange = false;

	public DeleteToTransfer(Attributes attributes)
	{
		super(attributes);
	}

	@Override
	public void dirtyStateChanged(DirtyStateEvent event)
	{
		NAIDPresentationModel model = getModel();
		Object bean = (model == null) ? null : model.getBean();

		if (event.getSource() == model && !disabledDirtyStateChange)
		{
			Document document = ((Document) bean);
			if (bean instanceof Model && ((Model) bean).isModelReadOnly())
			{
				setEnabled(false);
			}
			else if (event.isDirty())
			{
				setEnabled(false);
			}
			else if (document.getDocumentStatus() != null && document.getDocumentStatus().isToTransfer())
			{
				setEnabled(false);
			}
			else if (event.getId() != null)
			{
				setEnabled(true);
			}
			else
			{
				setEnabled(false);
			}
		}
	}

}
