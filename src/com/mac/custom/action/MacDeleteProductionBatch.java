package com.mac.custom.action;

import org.xml.sax.Attributes;

import com.netappsid.action.Delete;
import com.netappsid.erp.server.bo.ProductionBatch;
import com.netappsid.event.DirtyStateEvent;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;

public class MacDeleteProductionBatch extends Delete
{

	public MacDeleteProductionBatch(Attributes attributes)
	{
		super(attributes);
	}

	@Override
	public void dirtyStateChanged(DirtyStateEvent event)
	{
		super.dirtyStateChanged(event);
		NAIDPresentationModel model = getModel();
		if (model != null)
		{
			ProductionBatch productionBatch = (ProductionBatch) model.getBean();
			if (productionBatch.isConfirm())
			{
				setEnabled(false);
			}
		}
	}
}