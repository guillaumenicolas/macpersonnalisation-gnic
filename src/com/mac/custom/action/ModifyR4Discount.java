package com.mac.custom.action;

import java.awt.event.ActionEvent;
import java.util.List;

import javax.inject.Inject;

import org.xml.sax.Attributes;

import com.netappsid.action.AbstractNotifyingBoundAction;
import com.netappsid.action.DefaultModifyAction;
import com.netappsid.action.event.ActionNotifyEvent;
import com.netappsid.bo.model.Model;
import com.netappsid.erp.server.bo.Discount;
import com.netappsid.erp.server.bo.DiscountGroupDetail;
import com.netappsid.erp.server.bo.SaleOrderDetail;
import com.netappsid.erp.server.bo.SaleTransaction;
import com.netappsid.erp.server.bo.SaleTransactionDetail;
import com.netappsid.erp.server.bo.SaleTransactionDetailDiscount;
import com.netappsid.erp.server.bo.SaleTransactionDiscount;
import com.netappsid.erp.server.bo.TransactionAvailableDiscount;
import com.netappsid.erp.server.factory.TransactionDiscountFactory;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;
import com.netappsid.framework.gui.components.model.NAIDPresentationModelFactory;
import com.netappsid.resources.icons.FrameworkIconFactory;
import com.netappsid.utils.StringUtils;

public class ModifyR4Discount extends AbstractNotifyingBoundAction
{
	private NAIDPresentationModelFactory naidPresentationModelFactory;
	private final Attributes attributes;
	private final String R4DISCOUNTCODE = "R4";

	public ModifyR4Discount(Attributes attributes)
	{
		super(attributes);
		this.attributes = attributes;
		putValue(PROPERTY_ICON, FrameworkIconFactory.getInstance().getIcon("24x24/r4Discount.png"));
		setToolTipText("ManageR4Discount");
	}

	@Override
	public void doAction(ActionEvent e)
	{
		NAIDPresentationModel model = getModel();
		if (!model.getBusinessObjects().isEmpty())
		{
			final List<Object> selectedItems = model.getBusinessObjects();
			final SaleOrderDetail orderDetail = (SaleOrderDetail) selectedItems.get(0);
			final Boolean r4SaleTransactionDetailDiscountExist = isR4SaleTransactionDetailDiscountExist(orderDetail);
			final TransactionAvailableDiscount r4TransactionAvailableDiscount = getR4TransactionAvailableDiscount(orderDetail);
			final SaleTransactionDetailDiscount selectedR4Discount = getR4SaleTransactionDetailDiscount(orderDetail, r4TransactionAvailableDiscount);

			if (r4TransactionAvailableDiscount != null && selectedR4Discount != null)
			{
				DefaultModifyAction modifySaleTransactionDiscountRate = new DefaultModifyAction(attributes)
					{
						@Override
						public void fireBeforeCommit(ActionNotifyEvent event)
						{
							super.fireBeforeCommit(event);

							for (Object orderDetail : selectedItems)
							{
								if (orderDetail instanceof SaleOrderDetail)
								{
									SaleTransactionDetailDiscount discount = getR4SaleTransactionDetailDiscount((SaleOrderDetail) orderDetail,
											r4TransactionAvailableDiscount);
									discount.setValue(selectedR4Discount.getValue());
									discount.setOverwriteRate(selectedR4Discount.isOverwriteRate());
									((SaleTransactionDetail) discount.getTransactionDetail()).calculateTotal();
									discount.getTransactionDetail().getTransaction().calculateTotal();
								}
							}
						}

						@Override
						public void fireBeforeRollback(ActionNotifyEvent event)
						{
							super.fireBeforeRollback(event);

							if (!r4SaleTransactionDetailDiscountExist)
							{
								rollBackSaleTransactionDetailDiscount(orderDetail, r4TransactionAvailableDiscount);
							}
						}
					};

				String entityName = StringUtils.uncapitalizeFirstLetter(selectedR4Discount.getClass().getSimpleName());
				modifySaleTransactionDiscountRate.setModel(getNaidPresentationModelFactory().newInstance(entityName, selectedR4Discount,
						getIntelligentPanel().getUndoRedoManager()));
				modifySaleTransactionDiscountRate.setIntelligentPanel(getIntelligentPanel());
				modifySaleTransactionDiscountRate.setDocumentPane(getDocumentPane());
				modifySaleTransactionDiscountRate.setComponentFactory(getComponentFactory());
				modifySaleTransactionDiscountRate.setNaidPresentationModelFactory(getNaidPresentationModelFactory());
				modifySaleTransactionDiscountRate.doAction(null);
			}
		}
	}

	private void rollBackSaleTransactionDetailDiscount(SaleOrderDetail orderDetail, TransactionAvailableDiscount transactionAvailableDiscount)
	{
		orderDetail.removeDiscounts(transactionAvailableDiscount);
		orderDetail.getTransaction().calculateTotal();
	}

	private Boolean isR4SaleTransactionDetailDiscountExist(SaleOrderDetail orderDetail)
	{
		for (SaleTransactionDetailDiscount saleTransactionDetailDiscount : orderDetail.getDiscounts())
		{
			if (saleTransactionDetailDiscount.getDiscountCode().equalsIgnoreCase(R4DISCOUNTCODE))
			{
				return true;
			}
		}
		return false;
	}

	private SaleTransactionDetailDiscount getR4SaleTransactionDetailDiscount(SaleOrderDetail orderDetail,
			TransactionAvailableDiscount transactionAvailableDiscount)
	{
		for (SaleTransactionDetailDiscount saleTransactionDetailDiscount : orderDetail.getDiscounts())
		{
			if (saleTransactionDetailDiscount.getDiscountCode().equalsIgnoreCase(R4DISCOUNTCODE))
			{
				return saleTransactionDetailDiscount;
			}
		}

		if (transactionAvailableDiscount.getTransactionDiscount() == null)
		{
			transactionAvailableDiscount.setTransactionDiscount(getSaleTransactionDiscount(orderDetail, transactionAvailableDiscount));
		}
		orderDetail.addDiscounts(transactionAvailableDiscount);
		orderDetail.getTransaction().calculateTotal();

		return getR4SaleTransactionDetailDiscount(orderDetail, transactionAvailableDiscount);
	}

	private SaleTransactionDiscount getSaleTransactionDiscount(SaleOrderDetail orderDetail, TransactionAvailableDiscount transactionAvailableDiscount)
	{
		int sequence = orderDetail.getTransaction().getAvailableDiscounts().indexOf(transactionAvailableDiscount);
		DiscountGroupDetail discountGroupDetail = transactionAvailableDiscount.getDiscountGroupDetail();
		Discount discount = Model.getTarget(discountGroupDetail.getDiscount());

		SaleTransactionDiscount saleTransactionDiscount = new TransactionDiscountFactory().createSaleTransactionDiscount(discount,
				transactionAvailableDiscount, sequence);

		((SaleTransaction) orderDetail.getTransaction()).addToDiscounts(saleTransactionDiscount);
		return saleTransactionDiscount;
	}

	private TransactionAvailableDiscount getR4TransactionAvailableDiscount(SaleOrderDetail orderDetail)
	{
		for (TransactionAvailableDiscount transactionAvailableDiscount : orderDetail.getTransaction().getAvailableDiscounts())
		{
			if (transactionAvailableDiscount.getDiscountCode().equalsIgnoreCase(R4DISCOUNTCODE))
			{
				return transactionAvailableDiscount;
			}
		}
		return null;
	}

	protected NAIDPresentationModelFactory getNaidPresentationModelFactory()
	{
		return naidPresentationModelFactory;
	}

	@Inject
	public void setNaidPresentationModelFactory(NAIDPresentationModelFactory naidPresentationModelFactory)
	{
		this.naidPresentationModelFactory = naidPresentationModelFactory;
	}
}
