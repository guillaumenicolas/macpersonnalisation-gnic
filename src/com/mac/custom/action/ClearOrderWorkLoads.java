package com.mac.custom.action;

import java.awt.event.ActionEvent;
import java.util.List;

import org.xml.sax.Attributes;

import com.mac.custom.bo.MacOrder;
import com.mac.custom.bo.MacOrderDetail;
import com.mac.custom.form.MacOrderFormCustom;
import com.netappsid.action.AbstractBoundAction;
import com.netappsid.bo.model.Model;
import com.netappsid.erp.server.bo.OrderDetail;
import com.netappsid.resources.icons.FrameworkIconFactory;

/**
 * Resets the work load details for each detail of the orders, if the order status allows it. This is done client-side, therefore saving is required afterwards.
 * 
 * @author plefebvre
 */
public class ClearOrderWorkLoads extends AbstractBoundAction
{
	public ClearOrderWorkLoads(Attributes attributes)
	{
		super(attributes);
		putValue(PROPERTY_ICON, FrameworkIconFactory.getInstance().getIcon("24x24/changeneededdate.png"));
		setToolTipText("clearOrderWorkLoads");
	}

	@Override
	public void doAction(ActionEvent event)
	{
		List<Object> entities = getBusinessObjectSelector().getBusinessObjects();
		for (Object entity : entities)
		{
			MacOrder order = (MacOrder) Model.getTarget(entity);

			if (MacOrderFormCustom.isWorkLoadModifiable(order))
			{
				for (OrderDetail orderDetail : order.getDetails())
				{
					MacOrderDetail macOrderDetail = Model.getTarget(orderDetail);
					macOrderDetail.resetWorkLoadDetails();
				}
			}
		}
	}
}
