package com.mac.custom.action;

import java.awt.event.ActionEvent;

import org.xml.sax.Attributes;

import com.mac.custom.services.crm.bean.ImportClientCRMServiceBean;
import com.mac.custom.services.crm.remote.ImportClientCRMServiceRemote;
import com.netappsid.action.AbstractAction;
import com.netappsid.framework.utils.ServiceLocator;

public class ImportationClient extends AbstractAction
{
	// Attributs
	private final ServiceLocator<ImportClientCRMServiceRemote> serviceLocator;

	// Constructeurs
	public ImportationClient(Attributes atts)
	{
		super(atts);
		serviceLocator = new ServiceLocator<ImportClientCRMServiceRemote>(ImportClientCRMServiceBean.class);
	}

	@Override
	public void doAction(ActionEvent arg0)
	{
		System.out.println("-----D�but interface importation client");

		try
		{
			serviceLocator.get().methodeImportation();
		}
		catch (Exception e)
		{
			System.out.println("Catch lancement bouton" + e.getMessage());
		}

		System.out.println("-----Fin interface importation client");
	}
}
