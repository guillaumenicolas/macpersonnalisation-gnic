package com.mac.custom.action;

import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;

import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationResult;
import com.netappsid.action.AbstractBoundAction;
import com.netappsid.bo.BusinessObjectSelector;
import com.netappsid.bo.model.Identifiable;
import com.netappsid.component.AsyncCallback;
import com.netappsid.component.ExpandableDialogData;
import com.netappsid.component.FormDialog;
import com.netappsid.component.FormDialog.ButtonType;
import com.netappsid.component.OptionPane;
import com.netappsid.component.OptionPane.MessageType;
import com.netappsid.component.OptionPane.OptionResult;
import com.netappsid.erp.server.bo.ConfigurationRevision;
import com.netappsid.erp.server.bo.ConfiguratorRevision;
import com.netappsid.erp.server.bo.Product;
import com.netappsid.erp.server.bo.PurchaseTransaction;
import com.netappsid.erp.server.bo.PurchaseTransactionDetail;
import com.netappsid.erp.server.bo.SaleTransaction;
import com.netappsid.erp.server.bo.Transaction;
import com.netappsid.erp.server.bo.TransactionDetail;
import com.netappsid.erp.server.enums.ExplodableItemStatus;
import com.netappsid.erp.server.services.beans.DiscountServicesBean;
import com.netappsid.erp.server.services.interfaces.DiscountServices;
import com.netappsid.erp.server.services.resultholders.ResultHolder;
import com.netappsid.exceptions.NAIDValidationException;
import com.netappsid.framework.gui.components.SearchForm;
import com.netappsid.framework.gui.formbuilder.formmodel.FormModel;
import com.netappsid.framework.gui.utils.OptionPaneMessageDialogData;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.gui.utils.WindowUtils.WindowPosition;
import com.netappsid.resources.icons.Icon;

public class MacImportTransactionDetail extends AbstractBoundAction
{
	private static final Logger logger = Logger.getLogger(MacImportTransactionDetail.class);
	private static final String SCALEUP = "scaleup";
	private final ServiceLocator<DiscountServices> discountServices = new ServiceLocator<DiscountServices>(DiscountServicesBean.class);

	private String searchForm;
	private boolean scaleUp = true;

	public MacImportTransactionDetail(Attributes attributes)
	{
		super(attributes);
		putValue(PROPERTY_ICON, Icon.IMPORT.getIcon(PROPERTY_DEFAULT_ICON_SIZE));

		if (getToolTipText() == null)
		{
			setToolTipText("ImportDetail");
		}

		if (getTitle() == null)
		{
			setTitle("DetailImportation");
		}

		searchForm = attributes.getValue(SEARCHFORM);

		if (attributes.getValue(SCALEUP) != null)
		{
			scaleUp = Boolean.parseBoolean(attributes.getValue(SCALEUP));
		}
	}

	@Override
	public void doAction(ActionEvent e)
	{
		final Transaction transaction;

		Object bean = getParentModel().getBean();

		// Use the businessObjectModel when a FormModel is present
		if (bean instanceof FormModel)
		{
			transaction = (Transaction) ((FormModel) bean).getBusinessObjectModel().getBean();
		}
		else
		{
			transaction = (Transaction) bean;
		}

		if (transaction != null)
		{
			Object[] importedDetails = null;
			if (searchForm != null)
			{
				importedDetails = getImportedDetailsFromSearchForm(transaction);
			}
			else if (getForm() != null)
			{
				importedDetails = getImportedDetailsFromForm(transaction);
			}

			if (importedDetails != null)
			{
				final ResultHolder<List<TransactionDetail>> result = importDetails(transaction, importedDetails);
				if (!result.isEmpty())
				{
					ExpandableDialogData data = new ExpandableDialogData(getString("CopyDetail"), getString("SomeDetailsCouldNotBeCopied"),
							MessageType.INFORMATION, result.getAllMessagesText());
					getOptionPane().showExpandableMessages(data);
				}

				// Only one list of details will be returned
				if (scaleUp && !result.getResults().isEmpty())
				{
					scaleUpNewDetails(result.getResults().get(0));
				}
			}
		}
	}

	public Object[] getImportedDetailsFromSearchForm(Transaction transaction)
	{
		Object[] importedDetails = null;
		final SearchForm search = new SearchForm(transaction.getClass(), searchForm, false);
		if (getWidth() != null && getHeight() != null)
		{
			search.setSize(getWidth(), getHeight());
		}
		search.setPosition(WindowPosition.CENTER);
		search.setTitle(getLocalizedTitle());
		search.setModal(true);
		search.setVisible(true);

		if (search.hasExitedWithOK())
		{
			if (search.getValues().size() == 0)
			{
				importedDetails = new Object[] { search.getValues() };
			}
			else
			{
				importedDetails = search.getValues().toArray();
			}
		}

		search.dispose();

		return importedDetails;
	}

	public Object[] getImportedDetailsFromForm(Transaction transaction)
	{
		Object[] importedDetails = null;
		if (getDocumentPane() != null)
		{
			FormDialog dialog = getComponentFactory().newFormDialog(getDocumentPane(), getForm(), getModel().getBeanClass().getClassLoader(), ButtonType.OK,
					ButtonType.CANCEL);

			if (getWidth() != null && getHeight() != null)
			{
				dialog.setSize(getWidth(), getHeight());
			}

			dialog.setTitle((String) getValue(PROPERTY_TITLE));
			dialog.setParentModel(getModel());
			dialog.setPosition(WindowPosition.CENTER);
			dialog.showInactive();
			dialog.setModal(true);
			dialog.setVisible(true);

			if (dialog.getSelectedButton() == ButtonType.OK)
			{
				BusinessObjectSelector businessObjectSelector = dialog.getBusinessObjectSelector();

				if (businessObjectSelector != null && Identifiable.class.isAssignableFrom(businessObjectSelector.getBusinessObjectClass())
						&& businessObjectSelector.getBusinessObjects() != null)
				{
					importedDetails = businessObjectSelector.getBusinessObjects().toArray();
				}
			}

			dialog.dispose();
		}

		return importedDetails;
	}

	protected void scaleUpNewDetails(final List<TransactionDetail> newDetails)
	{
		// Ask the user if he want to perform updates if there is some update that can be performed
		if (canScaleUp(newDetails))
		{
			String messageKey = newDetails.size() > 1 ? "DoYouWantToUpdateTheConfigurationOfTheImportedElements"
					: "DoYouWantToUpdateTheConfigurationOfTheImportedElement";
			OptionPaneMessageDialogData data = new OptionPaneMessageDialogData(null, getString("Updating"), getString(messageKey),
					OptionPane.MessageType.INFORMATION);

			AsyncCallback<OptionPane.OptionResult> callBack = new AsyncCallback<OptionPane.OptionResult>()
				{

					@Override
					public void execute(OptionResult result)
					{
						if (result == OptionPane.OptionResult.YES)
						{
							// Perform the updates if required and inform the user of the update results
							proceedWithUpdate(newDetails);
						}
					}

				};

			showOptionPaneConfirmDialog(data, OptionPane.Option.YES_NO_CANCEL, callBack);
		}
	}

	/**
	 * @param newDetails
	 *            : details just created
	 * @param defaultConfiguration
	 * @return true if at least one detail need to be updated to the defaultConfiguration
	 */
	protected boolean canScaleUp(List<TransactionDetail> newDetails)
	{
		boolean result = false;
		// Ask the user if he want to update the detail to the last configuration available
		// To do so, we must first check if there is at least one detail that has a config that is not up-to-date
		for (TransactionDetail detail : newDetails)
		{
			ConfigurationRevision configurationRevision = detail.getConfigurationRevision();
			ConfiguratorRevision configuratorRevision = detail.getConfiguratorRevision();

			ConfigurationRevision defaultRevision = (configuratorRevision == null) ? null : configurationRevision.getConfiguration().getDefaultRevision();

			if (configurationRevision != null && !configurationRevision.getVersion().equals(defaultRevision.getVersion()) || configuratorRevision != null
					&& !configuratorRevision.getVersion().equals(defaultRevision.getConfiguratorRevision().getVersion()))
			{
				result = true;
				break;
			}
		}
		return result;
	}

	protected ResultHolder<List<TransactionDetail>> importDetails(Transaction transaction, Object[] beans)
	{
		ResultHolder<List<TransactionDetail>> resultHolder = new ResultHolder<List<TransactionDetail>>();
		List<TransactionDetail> newDetails = new ArrayList<TransactionDetail>();
		List<Product> productsNeedingConfirm = new ArrayList<Product>();
		List<TransactionDetail> needconfirmDetails = new ArrayList<TransactionDetail>();

		for (Object bean : beans)
		{
			TransactionDetail detail = (TransactionDetail) bean;
			if (detail != null)
			{
				ResultHolder<TransactionDetail> result = copyTransactionDetail(transaction, detail);

				// Add all messages that could have been logged
				resultHolder.addAllFrom(result.getValidationResult());

				if (!result.getResults().isEmpty())
				{
					TransactionDetail newDetail = result.getResults().get(0);
					if (newDetail instanceof PurchaseTransactionDetail && transaction instanceof PurchaseTransaction)
					{
						if (((PurchaseTransactionDetail) newDetail).isNeedConfirmation(((PurchaseTransaction) transaction).getSupplier()))
						{
							if (newDetail.getProduct() != null)
							{
								productsNeedingConfirm.add(newDetail.getProduct());
								needconfirmDetails.add(newDetail);
							}
						}
					}

					newDetail.setExplodableItemStatus(ExplodableItemStatus.NEW);

					if (isValidForTransaction(newDetail))
					{
						updateNewDetailBeforeImport(detail, newDetail);

						if (transaction instanceof SaleTransaction)
						{
							if (newDetail.getProduct() != null)
							{
								SaleTransaction saleTransaction = (SaleTransaction) transaction;
								newDetail.setDiscountRate(getProductDiscountRate(newDetail.getProduct(), saleTransaction));
							}
						}

						newDetail.setTransaction(transaction);
						newDetail.calculateTotal();
						newDetails.add(newDetail);
					}
				}
			}
		}

		if (beans.length != newDetails.size())
		{
			String message = "";
			if (transaction instanceof SaleTransaction)
			{
				message = getString("SomeProductsCouldNoBeImportedBecauseTheyAreNotAvailableForSale");
			}
			else if (transaction instanceof PurchaseTransaction)
			{
				message = getString("SomeProductsCouldNoBeImportedBecauseTheyAreNotAvailableForPurchase");
			}

			resultHolder.addWarning(message);
		}

		if (transaction instanceof PurchaseTransaction && !productsNeedingConfirm.isEmpty())
		{
			final String productsCodes = ((PurchaseTransaction) transaction).returnAllProductsCode(productsNeedingConfirm);
			final String supplierCode = ((PurchaseTransaction) transaction).getSupplier().getCode();
			final String message = String.format(getString("TheProductForTheSupplierNeedsConfirmationConfirmOrderRequisition"), productsCodes, supplierCode);
			final String title = getString("TitleNeedConfirmation");

			OptionPaneMessageDialogData data = new OptionPaneMessageDialogData(getDocumentPane(), title, message, OptionPane.MessageType.INFORMATION);
			final int optionResult = showJOptionPaneConfirmDialog(data, JOptionPane.YES_NO_OPTION);
			if (optionResult != JOptionPane.YES_OPTION)
			{
				newDetails.removeAll(needconfirmDetails);
			}
		}
		transaction.addManyDetails(newDetails);
		resultHolder.addToResults(newDetails);
		return resultHolder;
	}

	protected BigDecimal getProductDiscountRate(Product product, SaleTransaction saleTransaction)
	{
		return discountServices.get().getRate(product.getBanner(), saleTransaction.getCustomer(), saleTransaction.getCompany());
	}

	private void proceedWithUpdate(List<TransactionDetail> newDetails)
	{
		ValidationResult finalResult = new ValidationResult();
		for (TransactionDetail detail : newDetails)
		{
			ConfigurationRevision defaultRevision = detail.getConfigurationRevision().getConfiguration().getDefaultRevision();
			ValidationResult validationResult = detail.updateConfigurationVersion(defaultRevision, defaultRevision.getConfiguratorRevision());

			// Null ValidationResult means this item does not need to be updated... just continue
			if (validationResult != null)
			{
				finalResult.addAllFrom(validationResult);
			}
		}

		// No message so everything went right... give a successfull message
		Severity severity = finalResult.getSeverity();
		if (severity == null || severity.compareTo(Severity.OK) == 0)
		{
			OptionPaneMessageDialogData data = new OptionPaneMessageDialogData(null, getString("Update"), getString("ImportedDetailsUpdatedSuccesfully"),
					OptionPane.MessageType.INFORMATION);
			showJOptionPaneMessageDialog(data);
		}
		else if (severity.compareTo(Severity.WARNING) == 0 || severity.compareTo(Severity.ERROR) == 0)
		{
			final String messageKey = severity.compareTo(Severity.WARNING) == 0 ? "ImportedDetailsUpdatedWithWarnings" : "ImportedDetailsUpdatedWithErrors";
			ExpandableDialogData data = new ExpandableDialogData(getString("Update"), getString(messageKey), MessageType.ERROR, finalResult.getMessagesText());
			getOptionPane().showExpandableMessages(data);
		}

		return;
	}

	protected ResultHolder<TransactionDetail> copyTransactionDetail(Transaction transaction, TransactionDetail detail)
	{
		ResultHolder<TransactionDetail> result = new ResultHolder<TransactionDetail>();
		ValidationResult validationResult = new ValidationResult();
		TransactionDetail newDetail = null;
		try
		{
			newDetail = transaction.copyDetail(detail, validationResult, false);
			result.addAllFrom(validationResult);
			result.addToResults(newDetail);
		}
		catch (NAIDValidationException validationException)
		{
			result.addError(validationException.getMessage());
		}

		return result;
	}

	/**
	 * @param detail
	 * @return true if the detail is valid to add into the transaction Used to extend with a custom
	 */
	protected boolean isValidForTransaction(TransactionDetail detail)
	{
		return detail.getProduct() != null;
	}

	/**
	 * @param detailImported
	 *            : The detail imported
	 * @param newDetail
	 *            : The new detail created from the detailImported Used to extend with a custom
	 */
	protected void updateNewDetailBeforeImport(TransactionDetail detailImported, TransactionDetail newDetail)
	{}

	public String getSearchForm()
	{
		return searchForm;
	}

	public void setSearchForm(String searchForm)
	{
		this.searchForm = searchForm;
	}

	public boolean isScaleUp()
	{
		return scaleUp;
	}

	public void setScaleUp(boolean scaleUp)
	{
		this.scaleUp = scaleUp;
	}
}