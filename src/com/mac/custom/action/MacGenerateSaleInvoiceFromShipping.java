package com.mac.custom.action;

import org.xml.sax.Attributes;

import com.mac.custom.utils.DepositUtils;
import com.netappsid.erp.client.action.shipping.GenerateSaleInvoiceFromShipping;
import com.netappsid.erp.server.bo.Deposit;

public class MacGenerateSaleInvoiceFromShipping extends GenerateSaleInvoiceFromShipping
{
	private final DepositUtils depositUtils = new DepositUtils();

	public MacGenerateSaleInvoiceFromShipping(Attributes attributes)
	{
		super(attributes);
	}

	@Override
	protected Deposit getDeposit()
	{
		return depositUtils.getDefaultDeposit();
	}
}
