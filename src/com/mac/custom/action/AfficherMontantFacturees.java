package com.mac.custom.action;

import java.awt.event.ActionEvent;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;

import com.mac.custom.form.MacCustomerFormModel;
import com.netappsid.action.AbstractBoundAction;
import com.netappsid.action.event.ActionNotifyEvent;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;
import com.netappsid.resources.icons.Icon;

public class AfficherMontantFacturees extends AbstractBoundAction
{
	private static Logger logger = Logger.getLogger(AfficherMontantFacturees.class);
	
	public AfficherMontantFacturees(Attributes attributes)
	{
		super(attributes);
		putValue(PROPERTY_ICON, Icon.REFRESH.getIcon(PROPERTY_DEFAULT_ICON_SIZE));
		setText("Refresh");
		setToolTipText("Refresh");
		setEnabled(false);
	}

	@Override
	public void doAction(ActionEvent e)
	{
		if (getModel() != null && getModel().getBean() != null)
		{
			NAIDPresentationModel model = getDocumentPane().getPresentationModel("customerFormModel");
	
			if (model != null)
			{
				if (fireBefore(new ActionNotifyEvent(this, getModel(), model.getBean())))
				{
					MacCustomerFormModel customerFormModel = (MacCustomerFormModel) model.getBean();
	
					customerFormModel.fetchTotalMontantFactureAnneEnCours();
					customerFormModel.fetchTotalMontantFactureAnnePrecedente();
					customerFormModel.fetchTotalMontantFactureIlyaDeuxAns();
				}
	
				fireAfter(new ActionNotifyEvent(this, getModel(), model.getBean()));
			}
			else
			{
				logger.error(getString("ModelIsNotSpecify"));
			}
		}	
	}
}
