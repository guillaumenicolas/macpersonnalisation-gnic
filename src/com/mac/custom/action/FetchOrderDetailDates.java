package com.mac.custom.action;

import java.awt.event.ActionEvent;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;

import com.mac.custom.form.MacOrderDetailFormModel;
import com.netappsid.action.AbstractBoundAction;
import com.netappsid.action.event.ActionNotifyEvent;
import com.netappsid.erp.client.action.customer.FetchCurrentTotals;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;
import com.netappsid.resources.icons.Icon;

public class FetchOrderDetailDates extends AbstractBoundAction
{
	private static Logger logger = Logger.getLogger(FetchCurrentTotals.class);

	public FetchOrderDetailDates(Attributes attributes)
	{
		super(attributes);
		putValue(PROPERTY_ICON, Icon.REFRESH.getIcon(PROPERTY_DEFAULT_ICON_SIZE));
		setText("Refresh");
		setToolTipText("Refresh");
	}

	@Override
	public void doAction(ActionEvent e)
	{
		if (getModel() != null && getModel().getBean() != null)
		{
			NAIDPresentationModel model = getDocumentPane().getPresentationModel("macOrderDetailFormModel");

			if (model != null)
			{
				if (fireBefore(new ActionNotifyEvent(this, getModel(), model.getBean())))
				{
					MacOrderDetailFormModel macOrderDetailFormModel = (MacOrderDetailFormModel) model.getBean();

					macOrderDetailFormModel.refreshDates();
				}
				fireAfter(new ActionNotifyEvent(this, getModel(), model.getBean()));
			}
			else
			{
				logger.error(getString("ModelIsNotSpecify"));
			}
		}
	}
}
