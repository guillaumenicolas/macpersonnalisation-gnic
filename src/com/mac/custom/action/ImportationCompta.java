package com.mac.custom.action;

import java.awt.event.ActionEvent;

import org.xml.sax.Attributes;

import com.mac.custom.exception.ExceptionInterface;
import com.mac.custom.services.compta.bean.ImportComptaServiceBean;
import com.mac.custom.services.compta.remote.ImportComptaServiceRemote;
import com.netappsid.action.AbstractAction;
import com.netappsid.framework.utils.ServiceLocator;

public class ImportationCompta extends AbstractAction
{
	private final ServiceLocator<ImportComptaServiceRemote> serviceLocator;

	public ImportationCompta(Attributes atts)
	{
		super(atts);

		serviceLocator = new ServiceLocator<ImportComptaServiceRemote>(ImportComptaServiceBean.class);
	}

	@Override
	public void doAction(ActionEvent arg0)
	{
		try
		{
			serviceLocator.get().importationCompta();
		}
		catch (ExceptionInterface e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
