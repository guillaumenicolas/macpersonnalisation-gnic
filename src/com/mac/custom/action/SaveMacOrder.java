package com.mac.custom.action;

import java.util.Arrays;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;

import com.mac.custom.bo.MacOrder;
import com.mac.custom.bo.WorkLoadDetail;
import com.mac.custom.services.order.beans.MacOrderServicesBean;
import com.mac.custom.services.order.interfaces.remote.MacOrderServicesRemote;
import com.netappsid.component.ExpandableDialogData;
import com.netappsid.component.OptionPane.MessageType;
import com.netappsid.erp.server.services.resultholders.ResultHolder;
import com.netappsid.europe.naid.custom.actions.SaveOrderEuro;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.Loader;

public class SaveMacOrder extends SaveOrderEuro
{
	private static final Logger LOGGER = Logger.getLogger(SaveMacOrder.class);

	private final ServiceLocator<MacOrderServicesRemote> macOrderServicesLocator = new ServiceLocator<MacOrderServicesRemote>(MacOrderServicesBean.class);
	private final ServiceLocator<Loader> loaderBeanServiceLocator = new ServiceLocator<Loader>(LoaderBean.class);

	boolean firstSave = true;

	public SaveMacOrder(Attributes attributes)
	{
		super(attributes);
	}

	/**
	 * Methods overridden to generate the shipping dates (order and details). Since saving the order can automatically trigger a re-explosion service, and since
	 * modifying certain settings can remove the shipping dates in ASAP mode (even after an order detail has been exploded), we do updates here. The following
	 * operations will be performed: saving the order, updating the shipping dates via a service call (possibly creating {@link WorkLoadDetail} objects),
	 * reloading the modified order, re-exploding the order (if necessary). For performance reasons we don't want the order to load the associations twice,
	 * hence why the {@link #firstSave} flag has been created; it modifies the behavior of {@link #getDAOClass(Object)}, which will initially return
	 * <code>null</code> and then later on will return <code>MacOrder</code>.
	 */
	@Override
	public void setSavedObject(Object savedObject)
	{
		if (savedObject instanceof MacOrder)
		{
			MacOrder order = (MacOrder) savedObject;
			firstSave = false;

			// we surround with try/catch in case generateOrderShippingDates crashes... we still need to re-load the form regardless of what happened!
			ResultHolder<Void> result = new ResultHolder<Void>();
			try
			{
				result.addAllFrom(macOrderServicesLocator.get().generateOrderShippingDates(Arrays.asList(order.getId())));
			}
			catch (Exception e)
			{
				LOGGER.error(e.getMessage(), e);
				result.addError(getString("errorWhileGeneratingShippingDate"));
			}

			savedObject = loaderBeanServiceLocator.get().findByField(MacOrder.class, MacOrder.class, MacOrder.PROPERTYNAME_ID, order.getId()).get(0);
			super.setSavedObject(savedObject);
			firstSave = true;

			if (result.hasMessages())
			{
				ExpandableDialogData expandableMessages = new ExpandableDialogData(getString("Save"), getString("unableToDistributeOrderDetailWorkLoad"),
						MessageType.WARNING, result.getAllMessagesText());
				getOptionPane().showExpandableMessages(expandableMessages);
			}
		}
		else
		{
			super.setSavedObject(savedObject);
		}
	}

	@Override
	protected Class<?> getDAOClass(Object object) throws ClassNotFoundException
	{
		if (firstSave)
		{
			return null;
		}
		else
		{
			return super.getDAOClass(object);
		}
	}
}
