package com.mac.custom.action;

import java.awt.event.ActionEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.xml.sax.Attributes;

import com.google.common.collect.Lists;
import com.netappsid.action.AbstractBoundAction;
import com.netappsid.erp.server.services.beans.StockMissingServicesBean;
import com.netappsid.erp.server.services.interfaces.StockMissingServices;
import com.netappsid.framework.utils.ServiceLocator;

public class InvalidateStockMissing extends AbstractBoundAction
{
	private final ServiceLocator<StockMissingServices> stockMissingServicesLocator = new ServiceLocator<StockMissingServices>(StockMissingServicesBean.class);

	public InvalidateStockMissing(Attributes attributes)
	{
		super(attributes);
		setToolTipText(getString("invalidateStockMissing"));
	}

	@Override
	public void doAction(ActionEvent event)
	{
		List<Serializable> stockMissingIds = getBusinessObjectSelector().getBusinessObjectsID();

		if (stockMissingIds.isEmpty())
		{
			showNoLineSelectedMessage();
		}
		else if (JOptionPane.showConfirmDialog(getIntelligentPanel(), getString("confirmInvalidateStockMissing"), getString("TitleNeedConfirmation"),
				JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE) == JOptionPane.YES_OPTION)
		{
			for (List<Serializable> stockMissingIdsPartition : Lists.partition(stockMissingIds, 1000))
			{
				stockMissingServicesLocator.get().invalidateStockMissings(new ArrayList<Serializable>(stockMissingIdsPartition));
			}
		}
	}
}
