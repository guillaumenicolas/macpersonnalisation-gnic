package com.mac.custom.action;

import java.awt.event.ActionEvent;

import org.xml.sax.Attributes;

import com.mac.custom.services.beans.AutomaticStockMissingResolutionServicesBean;
import com.mac.custom.services.interfaces.remote.AutomaticStockMissingResolutionServicesRemote;
import com.netappsid.action.AbstractBoundAction;
import com.netappsid.framework.utils.ServiceLocator;

public class TestAutomaticStockMissingResolutionServicesBean extends AbstractBoundAction
{
	private final ServiceLocator<AutomaticStockMissingResolutionServicesRemote> serviceLocator;

	public TestAutomaticStockMissingResolutionServicesBean(Attributes atts)
	{
		super(atts);
		serviceLocator = new ServiceLocator<AutomaticStockMissingResolutionServicesRemote>(AutomaticStockMissingResolutionServicesBean.class);
	}

	@Override
	public void doAction(ActionEvent arg0)
	{
		try
		{
			serviceLocator.get().resolveStocksMissing();
		}
		catch (Exception e)

		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
