package com.mac.custom.action;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.measure.converters.ConversionException;
import javax.swing.JFileChooser;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.xml.sax.Attributes;

import com.jgoodies.validation.ValidationResult;
import com.mac.custom.action.ExportInventoryToExcel.InventoryExcelColumnEnum;
import com.mac.custom.bo.MacStocktakingDetail;
import com.mac.custom.dao.MacStocktakingDAO;
import com.netappsid.action.AbstractBoundAction;
import com.netappsid.component.ExpandableDialogData;
import com.netappsid.component.OptionPane.MessageType;
import com.netappsid.erp.server.bo.GroupMeasure;
import com.netappsid.erp.server.bo.InventoryProduct;
import com.netappsid.erp.server.bo.Location;
import com.netappsid.erp.server.bo.Stocktaking;
import com.netappsid.erp.server.bo.StocktakingDetail;
import com.netappsid.erp.server.bo.StocktakingMeasure;
import com.netappsid.erp.server.datatypes.GroupMeasureValue;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.framework.utils.filechooser.filter.GenericFilter;
import com.netappsid.resources.icons.Icon;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.beans.PersistenceBean;
import com.netappsid.services.interfaces.Loader;
import com.netappsid.services.interfaces.Persistence;

public class ImportInventory extends AbstractBoundAction
{
	private static final String DEFAULTFILEPATH = "import\\";
	private static final int INVENTORY_LOCATION = InventoryExcelColumnEnum.LOCATION_CODE.getIndex();
	private static final int INVENTORY_MATERIALCODE = InventoryExcelColumnEnum.MATERIAL_CODE.getIndex();
	private static final int INVENTORY_QUANTITYINSTOCK = InventoryExcelColumnEnum.QUANTITY_IN_STOCK.getIndex();
	private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(ImportInventory.class);

	private JFileChooser chooser;
	private final ServiceLocator<Loader> loaderServiceLocator = new ServiceLocator<Loader>(LoaderBean.class);
	private final ServiceLocator<Persistence> persistenceServiceLocator = new ServiceLocator<Persistence>(PersistenceBean.class);

	public ImportInventory(Attributes attributes)
	{
		super(attributes);
		putValue(PROPERTY_ICON, Icon.IMPORT.getIcon(PROPERTY_DEFAULT_ICON_SIZE));
		setToolTipText("Import");
	}

	@Override
	public void doAction(ActionEvent arg0)
	{
		JFileChooser chooser = getChooser();

		if (chooser.showDialog(null, getString("ImportEntries")) == JFileChooser.APPROVE_OPTION)
		{
			final String filePath = chooser.getSelectedFile().getAbsolutePath();

			try
			{
				// map of product, location, qty
				Map<String, Map<String, BigDecimal>> lines = loadFile(filePath);
				List<String> convertionExceptions = new ArrayList<String>();
				// list of products found in the details list, this is used for the rule to add location if the product is in the list
				Map<String, InventoryProduct> foundProducts = new HashMap<String, InventoryProduct>();

				Stocktaking stocktaking = ((Stocktaking) getModel().getParentModel().getBean());

				for (StocktakingDetail currentDetail : stocktaking.getDetails())
				{
					String productCode = currentDetail.getInventoryProduct().getCode();
					Map<String, BigDecimal> locationsMap = lines.get(productCode);
					if (locationsMap != null)
					{
						foundProducts.put(productCode, currentDetail.getInventoryProduct());

						String locationCode = currentDetail.getLocation().getCode();
						BigDecimal quantityInStock = locationsMap.remove(locationCode);
						if (locationsMap.isEmpty())
						{
							lines.remove(productCode);
							// this is just to save space, we don't care about this
							foundProducts.remove(productCode);
						}
						// import in the stocktaking
						if (!importSingleLocation(currentDetail, stocktaking, productCode, quantityInStock, convertionExceptions))
						{
							return;
						}
					}
				}

				StringBuilder message = new StringBuilder();

				for (Entry<String, Map<String, BigDecimal>> product : lines.entrySet())
				{
					String productCode = product.getKey();
					// faster to remove for future use
					InventoryProduct inventoryProduct = foundProducts.remove(productCode);
					if (inventoryProduct != null)
					{
						for (Entry<String, BigDecimal> location : product.getValue().entrySet())
						{
							MacStocktakingDetail detail = new MacStocktakingDetail();
							Location locationBean = loaderServiceLocator.get().findFirstByField(Location.class, Location.class, Location.PROPERTYNAME_CODE,
									location.getKey());
							if (locationBean == null)
							{
								message.append("La localisation '").append(location.getKey()).append("' utilis�e par le produit '").append(productCode)
										.append("' n'existe pas\n");
							}
							else
							{
								// add a link to the location in the product, the product is saved if the import is successful
								boolean saveProduct = false;
								if (inventoryProduct.getProductLocation(locationBean) == null)
								{
									saveProduct = true;

									inventoryProduct.getProductWarehouse(locationBean.getWarehouse()).addToLocation(locationBean, true);
								}

								detail.setInventoryProduct(inventoryProduct);
								detail.setLocation(locationBean);
								// import the new detail
								if (importSingleLocation(detail, stocktaking, productCode, location.getValue(), convertionExceptions))
								{
									detail.setStocktaking(stocktaking);
									stocktaking.addToDetails(detail);
									if (saveProduct)
									{
										// force the save of the product
										inventoryProduct = persistenceServiceLocator.get().save(inventoryProduct, MacStocktakingDAO.class);
									}
								}
								else
								{
									// some cleanup
									detail.setInventoryProduct(null);
									detail.setLocation(null);
									return;
								}
							}
						}
					}
					else
					{
						message.append("Le formulaire ne contient pas d'entr\u00e9e pour le produit '").append(productCode).append("'\n");
					}
				}

				for (StocktakingDetail currentDetail : stocktaking.getDetails())
				{
					if (!currentDetail.getNewQuantity().isValid())
					{
						importSingleLocation(currentDetail, stocktaking, currentDetail.getInventoryProduct().getCode(), BigDecimal.ZERO, convertionExceptions);

					}
				}

				for (String l : convertionExceptions)
				{
					String[] split = l.split("\t");
					message.append("Impossible de convertir l'unit\u00e9 '").append(split[1]).append("' pour le produit '").append(split[0]).append("'\n");
				}

				if (message.length() != 0)
				{
					message.setLength(message.length() - 1);

					ExpandableDialogData expandableDialogData = new ExpandableDialogData("Probl\u00e8mes d'importation",
							"Des erreurs ont emp\u00each\u00e9 l'importation de certaines lignes du fichier.", MessageType.WARNING, message.toString());
					getOptionPane().showExpandableMessages(expandableDialogData);
				}
			}
			catch (IOException e)
			{
				LOGGER.error("Can't read excel file with path " + filePath, e);
			}
		}
	}

	private boolean importSingleLocation(StocktakingDetail currentDetail, Stocktaking stocktaking, String productCode, BigDecimal quantityInStock,
			List<String> convertionExceptions)
	{
		if (quantityInStock != null)
		{
			GroupMeasure inventoryMeasureGroupMeasure = currentDetail.getInventoryProduct().getInventoryMeasureGroupMeasure();
			GroupMeasureValue quantity = new GroupMeasureValue(inventoryMeasureGroupMeasure, quantityInStock, currentDetail.getInventoryProduct()
					.getDefaultUnitConversion(), inventoryMeasureGroupMeasure.getMeasureUnitMultiplier());

			if (quantity.getConversion() == null && currentDetail.getInventoryProduct().getDefaultUnitConversion() != null)
			{
				quantity.setConversion(currentDetail.getInventoryProduct().getDefaultUnitConversion());
			}

			StocktakingMeasure stocktakingMeasure = new StocktakingMeasure();
			stocktakingMeasure.setQuantity(quantity);
			stocktakingMeasure.setStocktakingDetail(currentDetail);

			if (!currentDetail.getStockQuantity().isValid())
			{
				GroupMeasureValue stockQuantity = new GroupMeasureValue(quantity, BigDecimal.ZERO);
				currentDetail.setStockQuantity(stockQuantity);
			}

			if (!currentDetail.getStockMissingQuantity().isValid())
			{
				GroupMeasureValue stockMissingQuantity = new GroupMeasureValue(quantity, BigDecimal.ZERO);
				currentDetail.setStockMissingQuantity(stockMissingQuantity);
			}

			ValidationResult result = stocktakingMeasure.validate();
			if (!result.equals(ValidationResult.EMPTY))
			{
				getOptionPane().show(getDocumentPane(), result.getMessagesText(), getString("stocktakingMeasure"), MessageType.ERROR);
				return false;
			}

			try
			{
				// replace all measures with the one from the file
				currentDetail.removeAllFromMeasures();
				currentDetail.addToMeasures(stocktakingMeasure);
				stocktaking.setStocktakingMeasure(null);
			}
			catch (ConversionException e)
			{
				currentDetail.removeFromMeasures(stocktakingMeasure);
				convertionExceptions.add(productCode + "\t" + inventoryMeasureGroupMeasure.toString());
			}
		}
		return true;
	}

	protected JFileChooser getChooser()
	{
		if (chooser == null)
		{
			chooser = new JFileChooser();
			GenericFilter fileFilter = new GenericFilter();
			fileFilter.setExtensions(Arrays.asList("xls"));
			fileFilter.setDescription("Excel file (*.xls)");
			chooser.setFileFilter(fileFilter);
			chooser.setAcceptAllFileFilterUsed(false);
			chooser.setCurrentDirectory(new File(DEFAULTFILEPATH));
		}

		return chooser;
	}

	private static String getCellValue(Row row, int column)
	{
		if (row == null)
		{
			return null;
		}

		Cell cell = row.getCell(column);
		String returnValue = "";

		FormulaEvaluator evaluator = row.getSheet().getWorkbook().getCreationHelper().createFormulaEvaluator();

		int typeEvaluation = evaluator.evaluateFormulaCell(cell);

		if (cell != null)
		{
			int cellType;

			if (typeEvaluation == -1)
			{
				cellType = cell.getCellType();
			}
			else
			{
				cellType = typeEvaluation;
			}
			switch (cellType)
			{
				case Cell.CELL_TYPE_STRING:
					returnValue = cell.getRichStringCellValue().getString();
					break;
				case Cell.CELL_TYPE_NUMERIC:
					if (DateUtil.isCellDateFormatted(cell))
					{
						returnValue = cell.getDateCellValue().toString();
					}
					else
					{
						Double doubleValue = cell.getNumericCellValue();

						/*
						 * Try an integer division. If I don't have a remainder, then I can interpret the value as an integer and eliminate the ".0" in the
						 * string value I return.
						 */
						if (doubleValue.intValue() == doubleValue)
						{
							returnValue = ((Integer) doubleValue.intValue()).toString();
						}
						else
						{
							returnValue = doubleValue.toString();
						}
					}
					break;
				case Cell.CELL_TYPE_BOOLEAN:
					returnValue = ((Boolean) cell.getBooleanCellValue()).toString();
					break;
				default:
					break;
			}
		}

		return returnValue.trim();
	}

	private static BigDecimal getSafeDecimal(String rate)
	{
		BigDecimal result = null;
		if (rate != null && !rate.isEmpty())
		{
			if (rate.contains(","))
			{
				rate = rate.replace(",", ".");
			}
			result = new BigDecimal(rate);
		}
		else
		{
			result = BigDecimal.ZERO;
		}

		return result;
	}

	private static Map<String, Map<String, BigDecimal>> loadFile(String filePath) throws IOException
	{
		FileInputStream fis = new FileInputStream(filePath);
		try
		{
			Workbook workbook = new HSSFWorkbook(fis);

			Sheet sheet = workbook.getSheetAt(0);

			Map<String, Map<String, BigDecimal>> lines = new HashMap<String, Map<String, BigDecimal>>(sheet.getLastRowNum());

			for (int i = 1; i <= sheet.getLastRowNum(); ++i)
			{
				Row row = sheet.getRow(i);

				final String code = getCellValue(row, INVENTORY_MATERIALCODE);
				final String location = getCellValue(row, INVENTORY_LOCATION);
				final BigDecimal qty = getSafeDecimal(getCellValue(row, INVENTORY_QUANTITYINSTOCK));

				Map<String, BigDecimal> productMap = lines.get(code);
				if (productMap == null)
				{
					lines.put(code, productMap = new HashMap<String, BigDecimal>());
				}
				productMap.put(location, qty);
			}

			return lines;
		}
		finally
		{
			fis.close();
		}
	}
}
