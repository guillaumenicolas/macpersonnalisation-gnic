package com.mac.custom.action;

import org.xml.sax.Attributes;

import com.netappsid.erp.client.action.purchaseinvoice.SaveAndReloadPurchaseInvoice;
import com.netappsid.erp.server.bo.PurchaseInvoice;
import com.netappsid.event.DirtyStateEvent;

public class MacSaveAndReloadPurchaseInvoice extends SaveAndReloadPurchaseInvoice
{
	private boolean disabledDirtyStateChange = false;

	public MacSaveAndReloadPurchaseInvoice(Attributes attributes)
	{
		super(attributes);
	}

	@Override
	public void dirtyStateChanged(DirtyStateEvent event)
	{
		if (!disabledDirtyStateChange && (event.getSource() == getModel()))
		{
			if (getModel() != null && getModel().getBean() != null)
			{
				PurchaseInvoice purchaseInvoice = (PurchaseInvoice) getModel().getBean();
				if (purchaseInvoice.isModelReadOnly() || (purchaseInvoice.getDocumentStatus() != null && purchaseInvoice.getDocumentStatus().isToTransfer()))
				{
					setEnabled(false);
				}
				else
				{
					setEnabled(event.isDirty());
				}
			}
		}
	}
}