package com.mac.custom.action;

import java.awt.event.ActionEvent;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;

import com.mac.custom.services.report.beans.MacReportServicesBean;
import com.mac.custom.services.report.interfaces.MacReportServices;
import com.netappsid.action.AbstractBoundAction;
import com.netappsid.framework.gui.components.ProgressDialog;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.resources.icons.Icon;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.Loader;

import foxtrot.Task;

public class CreateTodaysQuotationPDF extends AbstractBoundAction
{
	private final ServiceLocator<MacReportServices> macReportServiceLocator = new ServiceLocator<MacReportServices>(MacReportServicesBean.class);
	private final ServiceLocator<Loader> loaderLocator = new ServiceLocator<Loader>(LoaderBean.class);

	private static Logger logger = Logger.getLogger(CreateTodaysQuotationPDF.class);

	public CreateTodaysQuotationPDF(Attributes attributes)
	{
		super(attributes);
		putValue(PROPERTY_ICON, Icon.REFRESH.getIcon(PROPERTY_DEFAULT_ICON_SIZE));
		setToolTipText("Envoi des devis du jour aux commerciaux");
	}

	@Override
	public void doAction(ActionEvent e)
	{
		try
		{
			Task task = new Task()
				{
					@Override
					public Object run()
					{
						try
						{
							macReportServiceLocator.get().runTodaysQuotationPDF();
						}
						catch (Exception e)
						{
							e.printStackTrace();
						}
						return null;
					}
				};

			ProgressDialog dialog = new ProgressDialog();

			try
			{
				dialog.showProgress(task, "Impression des Devis du jour en cours...");
			}
			catch (Exception e1)
			{
				e1.printStackTrace();
			}
			finally
			{
				dialog.dispose();
			}
		}
		catch (Exception ex)
		{
			logger.error(ex, ex);
		}
	}
}
