package com.mac.custom.action;

import java.awt.event.ActionEvent;
import java.io.Serializable;
import java.util.List;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.xml.sax.Attributes;

import com.jgoodies.validation.ValidationResult;
import com.mac.custom.services.order.beans.MacOrderServicesBean;
import com.mac.custom.services.order.interfaces.MacOrderServices;
import com.netappsid.component.ExpandableDialogData;
import com.netappsid.component.OptionPane.MessageType;
import com.netappsid.erp.client.action.order.BlockOrderShipping;
import com.netappsid.erp.server.bo.BlockingReason;
import com.netappsid.erp.server.bo.User;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.framework.utils.SystemVariable;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.remote.LoaderRemote;

public class MacBlockOrderShipping extends BlockOrderShipping
{
	private final ServiceLocator<MacOrderServices> macOrderServicesLocator = new ServiceLocator<MacOrderServices>(MacOrderServicesBean.class);
	private final ServiceLocator<LoaderRemote> loaderRemoteLocator = new ServiceLocator<LoaderRemote>(LoaderBean.class);
	private final String BLOCAGE_FINANCE = "BF";

	public MacBlockOrderShipping(Attributes attributes)
	{
		super(attributes);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void doLockAction(ActionEvent arg0)
	{
		List<Serializable> selectedOrdersIds = getBusinessObjectSelector().getBusinessObjectsID();

		if (selectedOrdersIds != null && !selectedOrdersIds.isEmpty())
		{
			User user = SystemVariable.getVariable("user");

			// ===================================================
			// modif PBY : 24/07/2014 : enlever le choix de la raison de blocage par la fenêtre de sélection
			// et remplacer par le choix automatique du blocage finance
			//
			// BlockingReason blockingReason = getBlockingUnblockingReason(BlockingReasonTypeEnum.BLOCK.name().toLowerCase());
			//
			BlockingReason blockingReason = loaderRemoteLocator.get().findFirstByField(BlockingReason.class, BlockingReason.class,
					BlockingReason.PROPERTYNAME_CODE, BLOCAGE_FINANCE);
			// ===================================================
			ValidationResult result = macOrderServicesLocator.get().blockOrderShipping(selectedOrdersIds, blockingReason.getId(), user.getId());
			if (result.hasMessages())
			{
				ExpandableDialogData expandableMessages = new ExpandableDialogData("BLOCAGE de commandes en EXPEDITION",
						"Le blocage s'est effectu\u00E9 avec succ\u00E8s. Voir d\u00E9tail ci-dessous", MessageType.WARNING, result.getMessagesText());
				getOptionPane().showExpandableMessages(expandableMessages);
			}
		}
	}
}
