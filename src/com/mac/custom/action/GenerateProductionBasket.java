package com.mac.custom.action;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.xml.sax.Attributes;

import com.mac.custom.bo.MacOrderDetail;
import com.mac.custom.dao.GenerateProductionBasketDAO;
import com.netappsid.erp.server.bo.User;
import com.netappsid.erp.server.bo.WorkOrderRoutingStep;
import com.netappsid.erp.server.services.resultholders.ProductionResult;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.services.beans.PersistenceBean;
import com.netappsid.services.interfaces.Persistence;

public class GenerateProductionBasket extends com.netappsid.erp.client.action.production.GenerateProductionBasket
{
	private final ServiceLocator<Persistence> persistenceServiceLocator = new ServiceLocator<Persistence>(PersistenceBean.class);

	public GenerateProductionBasket(Attributes attributes)
	{
		super(attributes);
	}

	@Override
	public ProductionResult generateProductionBasket(WorkOrderRoutingStep workOrderRoutingStep, User user, Date date)
	{
		ProductionResult productionResult = super.generateProductionBasket(workOrderRoutingStep, user, date);

		if (productionResult != null && productionResult.getProductionBasket() != null
				&& productionResult.getProductionBasket().getWorkOrderRoutingStep() != null
				&& productionResult.getProductionBasket().getWorkOrderRoutingStep().getWorkOrder() != null
				&& productionResult.getProductionBasket().getWorkOrderRoutingStep().getWorkOrder().getExplodableItem() != null)
		{
			MacOrderDetail orderDetail = (MacOrderDetail) productionResult.getProductionBasket().getWorkOrderRoutingStep().getWorkOrder().getExplodableItem();

			if (hasNotInBatchWors(orderDetail.getOrderId()))
			{
				WorkOrderRoutingStep wors = getLoaderServicesLocator().get().findById(WorkOrderRoutingStep.class, GenerateProductionBasketDAO.class,
						productionResult.getProductionBasket().getWorkOrderRoutingStep().getId());
				if (wors != null && wors.getProductionBatch() != null && !wors.getProductionBatch().isClosed())
				{
					wors.getProductionBatch().setClosed(true);
					persistenceServiceLocator.get().save(wors.getProductionBatch(), false);
				}
			}
		}

		return productionResult;
	}

	private boolean hasNotInBatchWors(Serializable orderId)
	{
		StringBuilder commentQuery = new StringBuilder();
		commentQuery.append("SELECT COUNT(*)").append("\n");
		commentQuery.append("FROM WorkOrderRoutingStep LEFT JOIN").append("\n");
		commentQuery.append("ProductionBatch ON ProductionBatch.id = WorkOrderRoutingStep.productionBatch_id LEFT JOIN").append("\n");
		commentQuery.append("ProductionBasket ON ProductionBasket.workOrderRoutingStep_id = WorkOrderRoutingStep.id LEFT JOIN").append("\n");
		commentQuery.append("WorkOrder ON WorkOrder.id = WorkOrderRoutingStep.workOrder_id LEFT JOIN").append("\n");
		commentQuery.append("OrderDetail ON OrderDetail.id = Workorder.explodableItem_id LEFT JOIN").append("\n");
		commentQuery.append("DocumentStatus ON DocumentStatus.id = WorkOrder.documentStatus_id").append("\n");
		commentQuery
				.append("WHERE ProductionBatch.id IS NOT NULL AND DocumentStatus.internalId NOT IN(3,6) AND ProductionBasket.id IS NULL AND OrderDetail.saleTransaction_id = :orderId")
				.append("\n");

		List<String> parametersName = new ArrayList<String>();
		List<Object> parametersValue = new ArrayList<Object>();

		parametersName.add("orderId");
		parametersValue.add(orderId.toString());

		List list = getLoaderServicesLocator().get().findByNativeQuery(commentQuery.toString(), parametersName, parametersValue, -1);

		return !list.isEmpty() && (Integer) ((Object[]) list.get(0))[0] == 0;
	}
}
