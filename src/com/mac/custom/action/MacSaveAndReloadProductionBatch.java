package com.mac.custom.action;

import org.xml.sax.Attributes;

import com.netappsid.action.SaveAndReload;
import com.netappsid.erp.server.bo.ProductionBatch;
import com.netappsid.event.DirtyStateEvent;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;

public class MacSaveAndReloadProductionBatch extends SaveAndReload
{

	public MacSaveAndReloadProductionBatch(Attributes attributes)
	{
		super(attributes);
	}

	@Override
	public void dirtyStateChanged(DirtyStateEvent event)
	{
		super.dirtyStateChanged(event);
		NAIDPresentationModel model = getModel();
		if (model != null)
		{
			ProductionBatch productionBatch = (ProductionBatch) model.getBean();
			if (productionBatch.isConfirm())
			{
				setEnabled(false);
			}
		}
	}
}