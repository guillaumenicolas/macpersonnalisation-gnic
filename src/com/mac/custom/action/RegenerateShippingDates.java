package com.mac.custom.action;

import java.awt.event.ActionEvent;
import java.io.Serializable;
import java.util.List;

import org.xml.sax.Attributes;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import com.mac.custom.services.order.beans.MacOrderServicesBean;
import com.mac.custom.services.order.interfaces.MacOrderServices;
import com.netappsid.component.ExpandableDialogData;
import com.netappsid.component.OptionPane.MessageType;
import com.netappsid.erp.client.action.AbstractLockAction;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.resources.icons.FrameworkIconFactory;

public class RegenerateShippingDates extends AbstractLockAction
{
	private final ServiceLocator<MacOrderServices> macOrderServicesLocator = new ServiceLocator<MacOrderServices>(MacOrderServicesBean.class);

	public RegenerateShippingDates(Attributes attributes)
	{
		super(attributes);
		putValue(PROPERTY_ICON, FrameworkIconFactory.getInstance().getIcon("24x24/changeneededdate.png"));
		setToolTipText("regenerateShippingDates");
	}

	@Override
	public void doLockAction(ActionEvent event)
	{
		List<Serializable> orderIds = getBusinessObjectSelector().getBusinessObjectsID();
		if (orderIds.isEmpty())
		{
			showNoLineSelectedMessage();
		}
		else
		{
			// clear the work load details before recalculating the shipping dates
			orderIds = macOrderServicesLocator.get().clearWorkLoadDetails(orderIds);

			if (!orderIds.isEmpty())
			{
				ValidationResult result = macOrderServicesLocator.get().generateOrderShippingDates(orderIds);
				if (result.hasMessages())
				{
					ValidationResult translatedValidationResult = translateValidationResult(result);
					ExpandableDialogData expandableMessages = new ExpandableDialogData(getString("regenerateShippingDates"),
							getString("unableToDistributeOrderDetailWorkLoad"), MessageType.WARNING, translatedValidationResult.getMessagesText());
					getOptionPane().showExpandableMessages(expandableMessages);
				}
			}
		}
	}

	private ValidationResult translateValidationResult(ValidationResult validationResult)
	{
		ValidationResult translatedValidationResult = new ValidationResult();

		for (ValidationMessage validationMessage : validationResult.getErrors())
		{
			translatedValidationResult.addError(getString(validationMessage.formattedText()));
		}

		for (ValidationMessage validationMessage : validationResult.getWarnings())
		{
			translatedValidationResult.addWarning(getString(validationMessage.formattedText()));
		}

		return translatedValidationResult;
	}

}
