package com.mac.custom.action;

import org.xml.sax.Attributes;

import com.netappsid.erp.client.action.generalledgertransaction.SavePurchaseInvoice;
import com.netappsid.erp.server.bo.PurchaseInvoice;
import com.netappsid.event.DirtyStateEvent;

public class MacSavePurchaseInvoice extends SavePurchaseInvoice
{
	private boolean disabledDirtyStateChange = false;

	public MacSavePurchaseInvoice(Attributes attributes)
	{
		super(attributes);
	}

	@Override
	public void dirtyStateChanged(DirtyStateEvent event)
	{
		if (!disabledDirtyStateChange && (event.getSource() == getModel()))
		{
			if (getModel() != null && getModel().getBean() != null)
			{
				PurchaseInvoice purchaseInvoice = (PurchaseInvoice) getModel().getBean();
				if (purchaseInvoice.isModelReadOnly() || (purchaseInvoice.getDocumentStatus() != null && purchaseInvoice.getDocumentStatus().isToTransfer()))
				{
					setEnabled(false);
				}
				else
				{
					setEnabled(event.isDirty());
				}
			}
		}
	}
}
