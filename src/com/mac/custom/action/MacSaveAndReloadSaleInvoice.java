package com.mac.custom.action;

import org.xml.sax.Attributes;

import com.netappsid.erp.client.action.saleinvoice.SaveAndReloadSaleInvoice;
import com.netappsid.erp.server.bo.SaleInvoice;
import com.netappsid.event.DirtyStateEvent;

public class MacSaveAndReloadSaleInvoice extends SaveAndReloadSaleInvoice
{
	private boolean disabledDirtyStateChange = false;

	public MacSaveAndReloadSaleInvoice(Attributes attributes)
	{
		super(attributes);
	}

	@Override
	public void dirtyStateChanged(DirtyStateEvent event)
	{
		if (!disabledDirtyStateChange && (event.getSource() == getModel()))
		{
			if (getModel() != null && getModel().getBean() != null)
			{
				SaleInvoice saleInvoice = (SaleInvoice) getModel().getBean();
				if (saleInvoice.isModelReadOnly() || (saleInvoice.getDocumentStatus() != null && saleInvoice.getDocumentStatus().isToTransfer()))
				{
					setEnabled(false);
				}
				else
				{
					setEnabled(event.isDirty());
				}
			}
		}
	}
}