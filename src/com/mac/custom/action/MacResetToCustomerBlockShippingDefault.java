package com.mac.custom.action;

import java.awt.event.ActionEvent;
import java.io.Serializable;
import java.util.List;

import org.xml.sax.Attributes;

import com.jgoodies.validation.ValidationResult;
import com.mac.custom.services.order.beans.MacOrderServicesBean;
import com.mac.custom.services.order.interfaces.MacOrderServices;
import com.netappsid.component.ExpandableDialogData;
import com.netappsid.component.OptionPane.MessageType;
import com.netappsid.erp.client.action.order.ResetToCustomerBlockShippingDefault;
import com.netappsid.erp.server.bo.User;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.framework.utils.SystemVariable;

public class MacResetToCustomerBlockShippingDefault extends ResetToCustomerBlockShippingDefault
{
	private final ServiceLocator<MacOrderServices> macOrderServicesLocator = new ServiceLocator<MacOrderServices>(MacOrderServicesBean.class);

	public MacResetToCustomerBlockShippingDefault(Attributes attributes)
	{
		super(attributes);
	}

	@Override
	public void doLockAction(ActionEvent arg0)
	{
		// PBYTODO Bouton MacResetToCustomerBlockShippingDefault dans Liste Commande
		List<Serializable> selectedOrdersIds = getBusinessObjectSelector().getBusinessObjectsID();

		if (selectedOrdersIds != null && !selectedOrdersIds.isEmpty())
		{
			User user = SystemVariable.getVariable("user");

			ValidationResult result = macOrderServicesLocator.get().resetToCustomerBlockShipping(selectedOrdersIds, user.getId());
			if (result.hasMessages())
			{
				ExpandableDialogData expandableMessages = new ExpandableDialogData("Application du BLOCAGE/DEBLOCAGE Client sur les commandes en EXPEDITION",
						"Le blocage/deblocage s'est effectu\u00E9 avec succ\u00E8s. Voir d\u00E9tail ci-dessous", MessageType.WARNING, result.getMessagesText());
				getOptionPane().showExpandableMessages(expandableMessages);
			}
		}
	}
}
