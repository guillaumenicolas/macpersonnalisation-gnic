package com.mac.custom.action;

import java.util.Arrays;

import org.xml.sax.Attributes;

import com.netappsid.action.ModifyDetail;
import com.netappsid.action.event.ActionNotifyEvent;
import com.netappsid.action.interfaces.ModifyActionNotifyListenerAdpater;
import com.netappsid.erp.server.bo.ResourceEfficiency;
import com.netappsid.erp.server.services.beans.ResourceEfficiencyServicesBean;
import com.netappsid.erp.server.services.interfaces.ResourceEfficiencyServices;
import com.netappsid.framework.utils.ServiceLocator;

/**
 * Extends {@link ModifyDetail} to automatically save a {@link ResourceEfficiency} once the edition has finished successfully (save will not be executed if the
 * action is canceled).
 * 
 * @author plefebvre
 */
public class ModifyAndSaveResourceEfficiency extends ModifyDetail
{
	public ModifyAndSaveResourceEfficiency(Attributes attributes)
	{
		super(attributes);

		addModifyActionNotifyListener(new AutoSaveHandler());
	}

	/**
	 * Handler used to automatically save a {@link ResourceEfficiency} after it has been modified.
	 */
	private static class AutoSaveHandler extends ModifyActionNotifyListenerAdpater
	{
		@Override
		public void afterCommit(ActionNotifyEvent event)
		{
			new ServiceLocator<ResourceEfficiencyServices>(ResourceEfficiencyServicesBean.class).get().saveAndDeleteResourceEfficiencies(
					Arrays.asList((ResourceEfficiency) event.getBean()));
		}
	}
}
