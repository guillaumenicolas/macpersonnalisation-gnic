package com.mac.custom.action;

import java.util.List;
import java.util.Locale;

import org.xml.sax.Attributes;

import com.mac.custom.utils.MacSearchProductFormUtils;
import com.netappsid.erp.client.action.transaction.AddTransactionDetail;
import com.netappsid.erp.client.gui.components.ProductSearchInfo;
import com.netappsid.erp.server.bo.Product;
import com.netappsid.erp.server.bo.Transaction;

/**
 * Extends {@link AddTransactionDetail} so that some custom Mac data can be included in the displayed form.
 * 
 * @author ftaillefer
 * 
 */
public class MacAddTransactionDetail extends AddTransactionDetail
{

	public MacAddTransactionDetail(Attributes attributes)
	{
		super(attributes);
	}

	@Override
	protected List<ProductSearchInfo> showProductSearchForm(Transaction transaction, List<ProductSearchInfo> productsSearchInfo, boolean includeCustomSearch,
			boolean includeDefaultSearch, Locale locale, List<Class<? extends Product>> productClasses)
	{
		return MacSearchProductFormUtils.showProductSearchForm(transaction, productsSearchInfo, includeCustomSearch, true, locale, productClasses);
	}

}
