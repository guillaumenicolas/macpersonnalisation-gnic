package com.mac.custom.action;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JFileChooser;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.xml.sax.Attributes;

import com.jgoodies.validation.ValidationResult;
import com.netappsid.action.AbstractBoundAction;
import com.netappsid.component.ExpandableDialogData;
import com.netappsid.component.OptionPane.MessageType;
import com.netappsid.erp.server.bo.Location;
import com.netappsid.erp.server.bo.MaterialProduct;
import com.netappsid.erp.server.bo.ProductWarehouse;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.framework.utils.filechooser.filter.GenericFilter;
import com.netappsid.resources.icons.Icon;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.beans.PersistenceBean;
import com.netappsid.services.interfaces.Loader;
import com.netappsid.services.interfaces.Persistence;
import com.netappsid.tuples.Pair;
import com.netappsid.tuples.Tuples;

/**
 * @author gcarignan
 * 
 *         This class assume that : The warehouse code is FA and FA only; Every product and location already exist in the application; The excel file format
 *         won't change (product_code, warehouse_code and location_code are the first 3 columns).
 * 
 */
public class ImportProductLocation extends AbstractBoundAction
{
	private static final String DEFAULTFILEPATH = "import\\";
	private static final int INVENTORY_LOCATION = 2;
	private static final int INVENTORY_MATERIALCODE = 0;
	private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(ImportInventory.class);

	private JFileChooser chooser;

	public ImportProductLocation(Attributes attributes)
	{
		super(attributes);
		putValue(PROPERTY_ICON, Icon.IMPORT.getIcon(PROPERTY_DEFAULT_ICON_SIZE));
		setToolTipText("Importation de localisation de produit");
	}

	@Override
	public void doAction(ActionEvent event)
	{
		JFileChooser chooser = getChooser();
		ServiceLocator<Loader> loaderServiceLocator = new ServiceLocator<Loader>(LoaderBean.class);
		ServiceLocator<Persistence> persistenceServiceLocator = new ServiceLocator<Persistence>(PersistenceBean.class);

		if (chooser.showDialog(null, getString("ImportEntries")) == JFileChooser.APPROVE_OPTION)
		{
			final String filePath = chooser.getSelectedFile().getAbsolutePath();

			try
			{
				List<Pair<String, String>> list = loadFile(filePath);

				int count = 1;
				ValidationResult validationResult = new ValidationResult();

				for (Pair<String, String> pair : list)
				{
					count++;
					MaterialProduct materialProduct = loaderServiceLocator.get().findFirstByField(MaterialProduct.class, MaterialProduct.class,
							MaterialProduct.PROPERTYNAME_CODE, pair.get1());
					if (materialProduct != null)
					{
						Location location = loaderServiceLocator.get()
								.findFirstByField(Location.class, Location.class, Location.PROPERTYNAME_CODE, pair.get2());

						if (location != null)
						{
							boolean locationExist = false;
							for (ProductWarehouse productWarehouse : materialProduct.getProductWarehouses())
							{
								if (productWarehouse.getProductLocation(location) != null)
								{
									locationExist = true;
									break;
								}
							}
							if (!locationExist)
							{
								materialProduct.getDefaultProductWarehouse().addToLocation(location, true);
								persistenceServiceLocator.get().save(materialProduct, true);
							}
						}
						else
						{
							validationResult.addError("La localisation " + pair.get2() + " n'existe pas dans la base - Ligne " + count);
						}
					}
					else
					{
						validationResult.addError("Le produit " + pair.get1() + " n'existe pas dans la base - Ligne " + count);
					}
				}

				if (validationResult.hasErrors())
				{
					ExpandableDialogData expandableDialogData = new ExpandableDialogData("Probl\u00E8mes d'importation des localisations de produit",
							"Des erreurs ont \u00E9t\u00E9 rencontr\u00E9es lors de l'importation de certaines lignes du fichier.", MessageType.WARNING,
							validationResult.getMessagesText());
					getOptionPane().showExpandableMessages(expandableDialogData);
				}
			}
			catch (IOException e)
			{
				LOGGER.error("Can't read excel file with path " + filePath, e);
			}
		}
	}

	protected JFileChooser getChooser()
	{
		if (chooser == null)
		{
			chooser = new JFileChooser();
			GenericFilter fileFilter = new GenericFilter();
			fileFilter.setExtensions(Arrays.asList("xls"));
			fileFilter.setDescription("Excel file (*.xls)");
			chooser.setFileFilter(fileFilter);
			chooser.setAcceptAllFileFilterUsed(false);
			chooser.setCurrentDirectory(new File(DEFAULTFILEPATH));
		}

		return chooser;
	}

	private static String getCellValue(Row row, int column)
	{
		if (row == null)
		{
			return null;
		}

		Cell cell = row.getCell(column);
		String returnValue = "";

		FormulaEvaluator evaluator = row.getSheet().getWorkbook().getCreationHelper().createFormulaEvaluator();

		int typeEvaluation = evaluator.evaluateFormulaCell(cell);

		if (cell != null)
		{
			int cellType;

			if (typeEvaluation == -1)
			{
				cellType = cell.getCellType();
			}
			else
			{
				cellType = typeEvaluation;
			}
			switch (cellType)
			{
				case Cell.CELL_TYPE_STRING:
					returnValue = cell.getRichStringCellValue().getString();
					break;
				case Cell.CELL_TYPE_NUMERIC:
					if (DateUtil.isCellDateFormatted(cell))
					{
						returnValue = cell.getDateCellValue().toString();
					}
					else
					{
						Double doubleValue = cell.getNumericCellValue();

						/*
						 * Try an integer division. If I don't have a remainder, then I can interpret the value as an integer and eliminate the ".0" in the
						 * string value I return.
						 */
						if (doubleValue.intValue() == doubleValue)
						{
							returnValue = ((Integer) doubleValue.intValue()).toString();
						}
						else
						{
							returnValue = doubleValue.toString();
						}
					}
					break;
				case Cell.CELL_TYPE_BOOLEAN:
					returnValue = ((Boolean) cell.getBooleanCellValue()).toString();
					break;
				default:
					break;
			}
		}

		return returnValue.trim();
	}

	private static List<Pair<String, String>> loadFile(String filePath) throws IOException
	{
		FileInputStream fis = new FileInputStream(filePath);
		try
		{
			Workbook workbook = new HSSFWorkbook(fis);
			Sheet sheet = workbook.getSheetAt(0);
			List<Pair<String, String>> list = new ArrayList<Pair<String, String>>();

			for (int i = 1; i <= sheet.getLastRowNum(); ++i)
			{
				Row row = sheet.getRow(i);
				Pair<String, String> pair = Tuples.pair(getCellValue(row, INVENTORY_MATERIALCODE), getCellValue(row, INVENTORY_LOCATION));
				list.add(pair);
			}

			return list;
		}
		finally
		{
			fis.close();
		}
	}

}
