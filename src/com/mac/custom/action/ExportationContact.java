package com.mac.custom.action;

import java.awt.event.ActionEvent;

import org.xml.sax.Attributes;

import com.mac.custom.exception.ExceptionInterface;
import com.mac.custom.services.crm.bean.ExportContactCRMServiceBean;
import com.mac.custom.services.crm.remote.ExportContactCRMServiceRemote;
import com.netappsid.action.AbstractAction;
import com.netappsid.framework.utils.ServiceLocator;

public class ExportationContact extends AbstractAction
{
	private final ServiceLocator<ExportContactCRMServiceRemote> serviceLocator;

	public ExportationContact(Attributes atts)
	{
		super(atts);
		serviceLocator = new ServiceLocator<ExportContactCRMServiceRemote>(ExportContactCRMServiceBean.class);
	}

	@Override
	public void doAction(ActionEvent arg0)
	{
		try
		{
			serviceLocator.get().methodeExportation();
		}
		catch (ExceptionInterface e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
