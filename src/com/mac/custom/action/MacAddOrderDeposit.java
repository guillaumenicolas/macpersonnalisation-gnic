package com.mac.custom.action;

import org.xml.sax.Attributes;

import com.mac.custom.utils.DepositUtils;
import com.netappsid.component.FormDialog;
import com.netappsid.erp.client.action.transaction.saletransaction.order.AddOrderDeposit;
import com.netappsid.erp.server.bo.Deposit;
import com.netappsid.erp.server.bo.Order;
import com.netappsid.erp.server.bo.Receipt;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;

public class MacAddOrderDeposit extends AddOrderDeposit
{
	private final DepositUtils depositUtils = new DepositUtils();

	public MacAddOrderDeposit(Attributes attributes)
	{
		super(attributes);
	}

	@Override
	public void showDepositForm(FormDialog dialog, NAIDPresentationModel foundModel, Order order, Receipt receipt)
	{
		Deposit defaultDeposit = depositUtils.getDefaultDeposit();
		if (defaultDeposit != null)
		{
			receipt.setDeposit(defaultDeposit);
		}
		super.showDepositForm(dialog, foundModel, order, receipt);
	}
}
