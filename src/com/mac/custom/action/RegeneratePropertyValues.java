package com.mac.custom.action;

import java.awt.event.ActionEvent;
import java.util.List;

import org.xml.sax.Attributes;

import com.mac.custom.bo.MacOrderDetail;
import com.netappsid.action.AbstractBoundAction;
import com.netappsid.action.event.ActionNotifyEvent;
import com.netappsid.component.OptionPane.MessageType;
import com.netappsid.erp.client.utils.ConfigurableHelperFactory;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;
import com.netappsid.resources.icons.FrameworkIconFactory;

public class RegeneratePropertyValues extends AbstractBoundAction
{
	public RegeneratePropertyValues(Attributes attributes)
	{
		super(attributes);
		putValue(PROPERTY_ICON, FrameworkIconFactory.getInstance().getIcon("24x24/productCodeAnalytic.png"));
		setToolTipText(getString("regeneratePropertyValues"));
	}

	@Override
	public void doAction(ActionEvent e)
	{

		NAIDPresentationModel model = getDocumentPane().getPresentationModel("order");

		if (model != null)
		{
			if (fireBefore(new ActionNotifyEvent(this, getModel(), model.getBean())))
			{
				List<Object> businessObjects = getBusinessObjectSelector().getBusinessObjects();

				for (Object object : businessObjects)
				{
					MacOrderDetail macOrderDetail = (MacOrderDetail) object;
					ConfigurableHelperFactory.INSTANCE.generateConfigurablePropertyValue(macOrderDetail, null);
					macOrderDetail.setDirty(true);
				}

				getOptionPane().show(getDocumentPane(), getString("saveMandatoryRegeneratePropetyValues"), getString("Save"), MessageType.WARNING);
			}
			fireAfter(new ActionNotifyEvent(this, getModel(), model.getBean()));
		}

	}
}
