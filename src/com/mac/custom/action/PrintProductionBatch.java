package com.mac.custom.action;

import java.awt.event.ActionEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;

import com.netappsid.erp.client.action.Printing;
import com.netappsid.erp.server.bo.ProductionBatch;
import com.netappsid.erp.server.services.beans.ProductionBatchServicesBean;
import com.netappsid.erp.server.services.interfaces.ProductionBatchServices;
import com.netappsid.framework.utils.ServiceLocator;

public class PrintProductionBatch extends Printing
{
	protected final ServiceLocator<ProductionBatchServices> productionBatchServicesLocator;

	public PrintProductionBatch(Attributes attributes)
	{
		this(attributes, new ServiceLocator<ProductionBatchServices>(ProductionBatchServicesBean.class));
	}

	protected PrintProductionBatch(Attributes attributes, ServiceLocator<ProductionBatchServices> productionBatchServicesLocator)
	{
		super(attributes);
		this.productionBatchServicesLocator = productionBatchServicesLocator;
	}

	@Override
	public void doAction(ActionEvent evt)
	{
		super.doAction(evt);
		confirmProductionBatches();
	}

	protected void confirmProductionBatches()
	{
		List<Serializable> ids = new ArrayList<Serializable>();
		if (getModel().getSelectedItems().size() > 0)
		{
			for (Object bean : getModel().getSelectedItems())
			{
				ids.add(((ProductionBatch) bean).getId());
			}
		}
		else
		{
			ids.add(((ProductionBatch) getModel().getBean()).getId());
		}

		productionBatchServicesLocator.get().confirmProductionBatch(ids);
	}
}
