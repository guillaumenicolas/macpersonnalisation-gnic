package com.mac.custom.action;

import java.awt.event.ActionEvent;
import java.io.Serializable;
import java.util.List;

import org.xml.sax.Attributes;

import com.jgoodies.validation.ValidationResult;
import com.mac.custom.services.order.beans.MacOrderServicesBean;
import com.mac.custom.services.order.interfaces.MacOrderServices;
import com.netappsid.component.ExpandableDialogData;
import com.netappsid.component.OptionPane.MessageType;
import com.netappsid.erp.client.action.order.ResetToCustomerBlockProductionDefault;
import com.netappsid.erp.server.bo.User;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.framework.utils.SystemVariable;

public class MacResetToCustomerBlockProductionDefault extends ResetToCustomerBlockProductionDefault
{
	private final ServiceLocator<MacOrderServices> macOrderServicesLocator = new ServiceLocator<MacOrderServices>(MacOrderServicesBean.class);

	public MacResetToCustomerBlockProductionDefault(Attributes attributes)
	{
		super(attributes);
	}

	@Override
	public void doLockAction(ActionEvent arg0)
	{
		// PBYTODO Bouton MacResetToCustomerBlockProductionDefault dans Liste Commande
		List<Serializable> selectedOrdersIds = getBusinessObjectSelector().getBusinessObjectsID();

		if (selectedOrdersIds != null && !selectedOrdersIds.isEmpty())
		{
			User user = SystemVariable.getVariable("user");

			ValidationResult result = macOrderServicesLocator.get().resetToCustomerBlockProduction(selectedOrdersIds, user.getId());
			if (result.hasMessages())
			{
				ExpandableDialogData expandableMessages = new ExpandableDialogData("Application du BLOCAGE/DEBLOCAGE Client sur les commandes en PRODUCTION",
						"Le blocage/deblocage s'est effectu\u00E9 avec succ\u00E8s. Voir d\u00E9tail ci-dessous", MessageType.WARNING, result.getMessagesText());
				getOptionPane().showExpandableMessages(expandableMessages);
			}
		}
	}
}
