package com.mac.custom.action;

import java.awt.event.ActionEvent;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;

import com.mac.custom.bo.MacOrder;
import com.mac.custom.bo.model.AutomaticPackages;
import com.mac.custom.bo.predicate.ProductionClosedItemToShipNotPlannedNotShippedNotCanceledNotPackagesPredicate;
import com.netappsid.component.ExpandableDialogData;
import com.netappsid.component.FormDialog;
import com.netappsid.component.FormDialog.ButtonType;
import com.netappsid.component.OptionPane.MessageType;
import com.netappsid.erp.client.action.AbstractLockAction;
import com.netappsid.erp.client.utils.Icon;
import com.netappsid.erp.server.bo.ItemToShip;
import com.netappsid.erp.server.bo.Packages;
import com.netappsid.erp.server.datatypes.GroupMeasureValue;
import com.netappsid.erp.server.factory.PackagesFactory;
import com.netappsid.erp.server.factory.initializer.ItemToShipInitializer;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.gui.utils.WindowUtils.WindowPosition;
import com.netappsid.resources.Translator;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.beans.PersistenceBean;
import com.netappsid.services.interfaces.Loader;
import com.netappsid.services.interfaces.Persistence;

public class GenerateAutomaticPackages extends AbstractLockAction
{
	private final ServiceLocator<Loader> loaderServicesLocator = new ServiceLocator<Loader>(LoaderBean.class);
	private final ServiceLocator<Persistence> persistenceServiceLocator = new ServiceLocator<Persistence>(PersistenceBean.class);

	public GenerateAutomaticPackages(Attributes attributes)
	{
		super(attributes);
		setForm("automaticPackagesDialog");
		putValue(PROPERTY_ICON, Icon.ITEMTOSHIP.getIcon(PROPERTY_DEFAULT_ICON_SIZE));
		putValue(PROPERTY_TOOLTIP_TEXT, Translator.getString("AutomaticPackagesGeneration"));
	}

	@Override
	public void doLockAction(ActionEvent e)
	{
		List<Serializable> macOrderIds = getBusinessObjectSelector().getBusinessObjectsID();

		if (!macOrderIds.isEmpty() && macOrderIds.size() == 1)
		{
			MacOrder macOrder = loaderServicesLocator.get().findById(MacOrder.class, MacOrder.class, macOrderIds.get(0));

			FormDialog formDialog = getComponentFactory().newFormDialog(getDocumentPane(), getForm(), getClass().getClassLoader(), ButtonType.OK,
					ButtonType.CANCEL);
			AutomaticPackages automaticPackages = new AutomaticPackages();

			automaticPackages.setOrder(macOrder);
			NAIDPresentationModel foundModel = formDialog.getDocumentPane().getPresentationModel("automaticPackages");
			foundModel.setBean(automaticPackages);
			formDialog.setTitle(Translator.getString("AutomaticPackagesGeneration"));
			formDialog.setModal(true);
			formDialog.setPosition(WindowPosition.CENTER);
			formDialog.setModel(foundModel);
			formDialog.setVisible(true);

			if (formDialog.getSelectedButton() == ButtonType.OK)
			{
				processPackages(automaticPackages);
			}
		}
	}

	public void processPackages(AutomaticPackages automaticPackages)
	{
		MacOrder generateMacOrder = generatePackages(automaticPackages.getOrder(), automaticPackages.getNumberPackages(), automaticPackages.getWeight());
		persistenceServiceLocator.get().save(generateMacOrder, false);
		String title = Translator.getString("AutomaticPackagesGeneration");
		String message = Translator.getString("PackagesGenerated");
		String expandedMessage = "";
		MessageType messageType = MessageType.INFORMATION;

		ExpandableDialogData expandableDialogData = new ExpandableDialogData(title, message, messageType, expandedMessage);
		getOptionPane().showExpandableMessages(expandableDialogData);
	}

	public MacOrder generatePackages(MacOrder macOrder, Long numberOfPackage, GroupMeasureValue weigth)
	{
		ItemToShipInitializer itemToShipInitializer = new ItemToShipInitializer();
		PackagesFactory packagesFactory = new PackagesFactory(itemToShipInitializer);
		List<ItemToShip> itemToShips = new ArrayList<ItemToShip>();

		macOrder.setItemToShipPredicate(new ProductionClosedItemToShipNotPlannedNotShippedNotCanceledNotPackagesPredicate());

		for (ItemToShip itemToShip : macOrder.getItemsToShipNotPlannedNotShippedAndNotCanceled())
		{
			if (itemToShips.size() < numberOfPackage)
			{
				itemToShips.add(itemToShip);
			}
			else
			{
				break;
			}
		}

		List<Packages> packagesList = new ArrayList<Packages>();
		Packages packages;
		BigDecimal divideWeight = weigth.getValue().divide(new BigDecimal(numberOfPackage), RoundingMode.HALF_UP);
		for (ItemToShip itemToShip : itemToShips)
		{
			packages = packagesFactory.create(itemToShip, macOrder);
			packages.setWeight(new GroupMeasureValue(weigth, divideWeight));
			packages.addToItemsToShip(itemToShip);
			packagesList.add(packages);
		}

		return macOrder;
	}
}