package com.mac.custom.action;

import java.awt.event.ActionEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.xml.sax.Attributes;

import com.mac.custom.bo.WorkCalendarDetail;
import com.mac.custom.services.workCalendar.beans.WorkCalendarServicesBean;
import com.mac.custom.services.workCalendar.interfaces.WorkCalendarServices;
import com.netappsid.action.AbstractBoundAction;
import com.netappsid.action.event.ActionNotifyEvent;
import com.netappsid.erp.server.bo.ResourceEfficiency;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.resources.icons.Icon;

/**
 * Clears all user-entered data on the selected {@link WorkCalendarDetail} objects.
 * 
 * @author plefebvre
 */
public class ClearWorkCalendarDetailLine extends AbstractBoundAction
{
	private final ServiceLocator<WorkCalendarServices> workCalendarServicesLocator = new ServiceLocator<WorkCalendarServices>(WorkCalendarServicesBean.class);

	public ClearWorkCalendarDetailLine(Attributes attributes)
	{
		super(attributes);

		putValue(PROPERTY_ICON, Icon.CANCEL.getIcon(PROPERTY_DEFAULT_ICON_SIZE));
		setToolTipText(getString("clearWorkCalendarDetailLine"));
	}

	@Override
	public void doAction(ActionEvent event)
	{
		List<WorkCalendarDetail> workCalendarDetails = getModel().getSelectedItems();

		if (!workCalendarDetails.isEmpty())
		{
			if (JOptionPane.showConfirmDialog(null, getString("confirmClearLineAutomaticSave"), getString("clearWorkCalendarDetailLine"),
					JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE) == JOptionPane.YES_OPTION)
			{
				if (fireBefore(new ActionNotifyEvent(this, getModel(), workCalendarDetails)))
				{
					List<Serializable> resourceEfficiencyIds = new ArrayList<Serializable>();

					for (WorkCalendarDetail workCalendarDetail : workCalendarDetails)
					{
						ResourceEfficiency resourceEfficiency = workCalendarDetail.getResourceEfficiency();
						// some resource efficiencies might not be persisted, in which case we can simply ignore them
						if (resourceEfficiency.getId() != null)
						{
							resourceEfficiencyIds.add(resourceEfficiency.getId());
						}
					}

					workCalendarServicesLocator.get().deleteResourceEfficiencies(resourceEfficiencyIds);

					fireAfter(new ActionNotifyEvent(this, getModel(), workCalendarDetails));
				}
			}
		}
	}
}
