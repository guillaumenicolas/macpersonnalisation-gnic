package com.mac.custom.action;

import java.awt.event.ActionEvent;
import java.util.List;

import org.xml.sax.Attributes;

import com.mac.custom.bo.model.ProductionRestocking;
import com.mac.custom.services.workOrder.beans.MacWorkOrderServicesBean;
import com.mac.custom.services.workOrder.interfaces.MacWorkOrderServices;
import com.netappsid.action.AbstractBoundAction;
import com.netappsid.component.FormDialog;
import com.netappsid.component.FormDialog.ButtonType;
import com.netappsid.erp.server.bo.Relocation;
import com.netappsid.erp.server.bo.RelocationDetail;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.gui.utils.WindowUtils.WindowPosition;
import com.netappsid.resources.icons.FrameworkIconFactory;

public class ProductionRestockingAction extends AbstractBoundAction
{
	private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(ProductionRestockingAction.class);
	private final ServiceLocator<MacWorkOrderServices> macWorkOrderServicesLocator = new ServiceLocator<MacWorkOrderServices>(MacWorkOrderServicesBean.class);

	public ProductionRestockingAction(Attributes attributes)
	{
		super(attributes);
		setForm("productionRestocking");
		putValue(PROPERTY_TITLE, PROPERTY_TITLE);
		putValue(PROPERTY_ICON, com.netappsid.erp.client.utils.Icon.WORKORDER.getIcon(PROPERTY_DEFAULT_ICON_SIZE));
		putValue(PROPERTY_ICON, FrameworkIconFactory.getInstance().getIcon("24x24/ProductionRestocking.png"));

		putValue(PROPERTY_TOOLTIP_TEXT, getString("productionRestocking"));
	}

	@Override
	public void doAction(ActionEvent e)
	{
		FormDialog dialog = null;
		dialog = getComponentFactory().newFormDialog(getDocumentPane(), getForm(), this.getClass().getClassLoader(), ButtonType.OK, ButtonType.CANCEL);
		NAIDPresentationModel foundModel = getDialogPresentationModel(dialog);
		dialog.setParentModel(getModel());
		dialog.setPosition(WindowPosition.CENTER);
		dialog.setTitle(getLocalizedTitle());
		dialog.setModal(true);
		dialog.setVisible(true);

		if (dialog.getSelectedButton() == ButtonType.OK)
		{
			final ProductionRestocking productionRestocking = (ProductionRestocking) foundModel.getBean();

			if (productionRestocking.getDateFrom() != null && productionRestocking.getDateTo() != null)
			{
				List<RelocationDetail> relocationDetails = macWorkOrderServicesLocator.get().getStockToTransfertFromWorkOrderDetail(
						productionRestocking.getDateFrom(), productionRestocking.getDateTo(),
						productionRestocking.getProductionLocation().getId(),
						productionRestocking.getStorageLocation().getId());
				Relocation relocation = (Relocation) getParentModel().getBean();
				relocation.addManyToDetails(relocationDetails);
			}
		}
	}

}
