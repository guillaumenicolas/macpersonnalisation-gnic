package com.mac.custom.action;

import java.awt.event.ActionEvent;
import java.util.List;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;

import com.mac.custom.bo.CustomerLedgerEntry;
import com.mac.custom.bo.MacCustomer;
import com.mac.custom.services.customer.beans.MacCustomerServicesBean;
import com.mac.custom.services.customer.interfaces.MacCustomerServices;
import com.netappsid.action.AbstractBoundAction;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.resources.icons.Icon;

public class FetchCustomerLedgerEntry extends AbstractBoundAction
{
	private final ServiceLocator<MacCustomerServices> macCustomerServiceLocator = new ServiceLocator<MacCustomerServices>(MacCustomerServicesBean.class);

	private static Logger logger = Logger.getLogger(FetchCustomerLedgerEntry.class);

	public FetchCustomerLedgerEntry(Attributes attributes)
	{
		super(attributes);
		putValue(PROPERTY_ICON, Icon.REFRESH.getIcon(PROPERTY_DEFAULT_ICON_SIZE));
		setText("Refresh");
		setToolTipText("Refresh");
	}

	@Override
	public void doAction(ActionEvent e)
	{
		try
		{
			if (getModel() != null && getModel().getBean() != null)
			{
				NAIDPresentationModel model = getDocumentPane().getPresentationModel("customerFormModel");

				MacCustomer customer = (MacCustomer) getModel().getBean();
				if (model != null && customer.getCode() != null)
				{
					List<CustomerLedgerEntry> list = macCustomerServiceLocator.get().fetchCustomerLegderEntry(customer.getCode(),
							customer.getCurrency().getCode());
					customer.setEntries(list);
				}
			}
		}
		catch (Exception ex)
		{
			logger.error(ex, ex);
		}

	}
}