package com.mac.custom.action;

import java.awt.event.ActionEvent;
import java.util.List;

import org.xml.sax.Attributes;

import com.mac.custom.bo.MacOrder;
import com.mac.custom.bo.WorkLoadDetail;
import com.mac.custom.bo.model.WorkLoadDetailQuantityMoveModel;
import com.mac.custom.dao.MacOrderMoveWorkLoadDetailDAO;
import com.mac.custom.services.workLoad.beans.WorkLoadServicesBean;
import com.mac.custom.services.workLoad.interfaces.WorkLoadServices;
import com.netappsid.action.AbstractBoundAction;
import com.netappsid.action.event.ActionNotifyEvent;
import com.netappsid.component.ExpandableDialogData;
import com.netappsid.component.FormDialog;
import com.netappsid.component.FormDialog.ButtonType;
import com.netappsid.component.OptionPane.MessageType;
import com.netappsid.erp.server.model.ShippingDateModel;
import com.netappsid.erp.server.services.resultholders.ResultHolder;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.gui.utils.WindowUtils.WindowPosition;
import com.netappsid.resources.icons.FrameworkIconFactory;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.Loader;

/**
 * Moves quantity from {@link WorkLoadDetail}s to another day. See
 * {@link WorkLoadServices#moveWorkLoadDetailQuantity(List, java.util.Date, com.netappsid.erp.server.datatypes.GroupMeasureValue)} for specific details.
 * 
 * @author plefebvre
 */
public class MoveWorkLoadDetailQuantity extends AbstractBoundAction
{
	private final ServiceLocator<WorkLoadServices> workLoadServicesLocator = new ServiceLocator<WorkLoadServices>(WorkLoadServicesBean.class);
	private final ServiceLocator<Loader> loaderServiceLocator = new ServiceLocator<Loader>(LoaderBean.class);

	public MoveWorkLoadDetailQuantity(Attributes attributes)
	{
		super(attributes);
		putValue(PROPERTY_ICON, FrameworkIconFactory.getInstance().getIcon("24x24/schedule.png"));
		setToolTipText(getString("moveWorkLoadOrShippingDate"));
		setTitle(getString("moveWorkLoadOrShippingDate"));
	}

	@Override
	public void doAction(ActionEvent event)
	{
		List<WorkLoadDetail> workLoadDetails = getBusinessObjectSelector().getBusinessObjects();
		if (workLoadDetails.isEmpty())
		{
			showNoLineSelectedMessage();
		}
		else if (fireBefore(new ActionNotifyEvent(this, getModel(), getModel().getBean())))
		{
			WorkLoadDetailQuantityMoveModel moveModelBean = initializeModel(workLoadDetails);

			FormDialog dialog = getComponentFactory().newFormDialog(getDocumentPane(), "moveWorkLoadDetailQuantity", this.getClass().getClassLoader(),
					ButtonType.OK, ButtonType.CANCEL);
			NAIDPresentationModel movePresentationModel = dialog.getDocumentPane().getPresentationModel("workLoadDetailQuantityMoveModel");
			movePresentationModel.setBean(moveModelBean);

			dialog.setTitle(getLocalizedTitle());
			dialog.setModel(movePresentationModel);
			dialog.setModal(true);
			dialog.setPosition(WindowPosition.CENTER);
			dialog.setVisible(true);

			if (dialog.getSelectedButton() == ButtonType.OK)
			{
				ResultHolder<Void> result = new ResultHolder<Void>();
				result.addAllFrom(workLoadServicesLocator.get().moveWorkLoadDetailQuantity(getBusinessObjectSelector().getBusinessObjectsID(), moveModelBean));

				MessageType messageType = MessageType.INFORMATION;
				String message = getString("moveWorkLoadDetailSuccess");

				if (result.hasErrors())
				{
					messageType = MessageType.ERROR;
					message = getString("moveWorkLoadDetailSuccessWithErrors");
				}
				else if (result.hasWarnings())
				{
					messageType = MessageType.WARNING;
					message = getString("moveWorkLoadDetailSuccessWithWarning");
				}
				if (result.hasMessages())
				{
					ExpandableDialogData expandableMessages = new ExpandableDialogData(getString("moveWorkLoadOrShippingDateResults"), message, messageType,
							result.getAllMessagesText());
					getOptionPane().showExpandableMessages(expandableMessages);
				}

				fireAfter(new ActionNotifyEvent(this, getModel(), getModel().getBean()));
			}
			dialog.dispose();
		}
	}

	protected WorkLoadDetailQuantityMoveModel initializeModel(List<WorkLoadDetail> workLoadDetails)
	{
		// prepare model bean with the date and biggest quantity (if the quantity is too big for a detail, the full quantity of that detail will be moved)
		WorkLoadDetailQuantityMoveModel moveModelBean = new WorkLoadDetailQuantityMoveModel();

		moveModelBean.setDate(workLoadDetails.get(0).getWorkLoad().getDate());

		if (workLoadDetails.size() == 1)
		{
			MacOrder macOrder = loaderServiceLocator.get().findById(MacOrder.class, MacOrderMoveWorkLoadDetailDAO.class,
					workLoadDetails.get(0).getOrderDetail().getSaleTransaction().getId());

			moveModelBean.setMoveOrderShippingDate(false);
			moveModelBean.setOrderShippingDate(macOrder.getShippingDate());

			// set shipping date model
			ShippingDateModel shippingDateModel = new ShippingDateModel();
			shippingDateModel.setAddress(macOrder.getShippingAddress());
			shippingDateModel.setRoute(macOrder.getRoute(), false);
			shippingDateModel.setDate(macOrder.getShippingDate(), false);
			shippingDateModel.setSchedules();
			moveModelBean.setShippingDateModel(shippingDateModel);
		}

		// quantity
		for (WorkLoadDetail workLoadDetail : workLoadDetails)
		{
			if (moveModelBean.getQuantityValue() == null || workLoadDetail.getQuantityToProduce().isValid()
					&& moveModelBean.getQuantity().compareTo(workLoadDetail.getQuantityToProduce()) < 0)
			{
				moveModelBean.setQuantity(workLoadDetail.getQuantityToProduce());
			}
		}

		return moveModelBean;
	}
}
