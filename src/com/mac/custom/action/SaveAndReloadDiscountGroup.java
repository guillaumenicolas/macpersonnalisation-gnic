package com.mac.custom.action;

import java.awt.event.ActionEvent;
import java.io.Serializable;

import org.xml.sax.Attributes;

import com.mac.custom.bo.MacSetup;
import com.mac.custom.dao.MacDiscountGroupDAO;
import com.netappsid.action.SaveAndReload;
import com.netappsid.bo.model.Model;
import com.netappsid.component.OptionPane;
import com.netappsid.erp.client.utils.ERPSystemVariable;
import com.netappsid.erp.server.bo.DiscountGroup;
import com.netappsid.erp.server.services.utils.SetupUtils;
import com.netappsid.europe.naid.custom.bo.UserEuro;
import com.netappsid.framework.gui.utils.OptionPaneMessageDialogData;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.framework.utils.SystemVariable;
import com.netappsid.security.auth.server.SecurityGroup;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.remote.LoaderRemote;

public class SaveAndReloadDiscountGroup extends SaveAndReload
{
	private final String DISCOUNT_GROUP_FOR_CUSTOM = "referentiel";

	private final ServiceLocator<LoaderRemote> loader = new ServiceLocator<LoaderRemote>(LoaderBean.class);

	public SaveAndReloadDiscountGroup(Attributes attributes)
	{
		super(attributes);
	}

	@Override
	public void doAction(ActionEvent event)
	{
		DiscountGroup discountGroup = (DiscountGroup) getModel().getBean();

		if (!isUserAlowesToSave(discountGroup))
		{
			OptionPaneMessageDialogData data = new OptionPaneMessageDialogData(getDocumentPane(), getString("discountGroup"),
					getString("userNotAllowedToSaveDiscountGroup"), OptionPane.MessageType.WARNING);
			showJOptionPaneMessageDialog(data);
		}
		else
		{
			super.doAction(event);
		}
	}

	/**
	 * Retourne l'instance du groupe de remise qui est dans la base
	 * 
	 * @param id
	 *            ID du groupe de remise
	 * @return Instance qui est dans la base
	 */
	private DiscountGroup getBeanInDB(Serializable id)
	{
		return loader.get().findFirstByField(DiscountGroup.class, MacDiscountGroupDAO.class, DiscountGroup.PROPERTYNAME_ID, id);
	}

	/**
	 * Retourne true si l'utilisateur est autorise a sauvegarder le groupe d'escompte
	 * 
	 * @param discountGroup
	 *            Groupe d'escompte
	 * @return true ou false
	 */
	private boolean isUserAlowesToSave(DiscountGroup discountGroup)
	{
		UserEuro user = SystemVariable.getVariable(ERPSystemVariable.USER);

		boolean discountGroupForAdmin = false;

		if (discountGroup.getCustom().get(DISCOUNT_GROUP_FOR_CUSTOM) != null && discountGroup.getCustom().get(DISCOUNT_GROUP_FOR_CUSTOM).getValue() != null)
		{
			discountGroupForAdmin = new Boolean(discountGroup.getCustom().get(DISCOUNT_GROUP_FOR_CUSTOM).getValue());
		}

		if (discountGroupForAdmin && user != null)
		{
			return isMemberOf(user);

		}
		else if (discountGroup.getId() != null)
		{
			DiscountGroup oldDiscountGroup = getBeanInDB(discountGroup.getId());

			if (oldDiscountGroup != null && user != null)
			{
				if (oldDiscountGroup.getCustom().get(DISCOUNT_GROUP_FOR_CUSTOM) != null
						&& oldDiscountGroup.getCustom().get(DISCOUNT_GROUP_FOR_CUSTOM).getValue() != null)
				{
					discountGroupForAdmin = new Boolean(oldDiscountGroup.getCustom().get(DISCOUNT_GROUP_FOR_CUSTOM).getValue());
				}

				if (discountGroupForAdmin && user != null)
				{
					return isMemberOf(user);
				}
			}
			return true;
		}

		return true;
	}

	/**
	 * Est-ce que l'utilisateur est membre du groupe de securite autorise
	 * 
	 * @param user
	 *            Utilisateur
	 * @return Groupe de securite
	 */
	private boolean isMemberOf(UserEuro user)
	{
		MacSetup setup = (MacSetup) Model.getTarget(SetupUtils.INSTANCE.getMainSetup());

		SecurityGroup securityGroup = (SecurityGroup) setup.getDiscountGroupAdmin();

		if (securityGroup != null)
		{
			return user.getSecurityUser().isMember(securityGroup);
		}

		return false;
	}
}
