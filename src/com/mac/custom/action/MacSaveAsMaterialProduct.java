package com.mac.custom.action;

import java.math.BigDecimal;

import org.xml.sax.Attributes;

import com.netappsid.datatypes.MonetaryAmount;
import com.netappsid.erp.client.action.product.SaveAsInventoryProduct;
import com.netappsid.erp.server.bo.InventoryProduct;
import com.netappsid.erp.server.bo.MaterialProduct;
import com.netappsid.erp.server.bo.Product;

public class MacSaveAsMaterialProduct extends SaveAsInventoryProduct
{
	public MacSaveAsMaterialProduct(Attributes attributes)
	{
		super(attributes);
	}

	@Override
	protected InventoryProduct cloneProduct(final InventoryProduct originalProduct)
	{
		InventoryProduct cloneBean = null;

		if (originalProduct != null)
		{
			cloneBean = (InventoryProduct) originalProduct.clone(Product.PROPERTYNAME_ID, InventoryProduct.PROPERTYNAME_QUICKLIBRARIES,
					MaterialProduct.PROPERTYNAME_COSTFOLLOWDEFAULTSUPPLIER, MaterialProduct.PROPERTYNAME_DEFAULTSUPPLIER,
					MaterialProduct.PROPERTYNAME_SUPPLIERS, MaterialProduct.PROPERTYNAME_PRODUCTCOSTTYPE, MaterialProduct.PROPERTYNAME_WEIGHTEDAVERAGECOST,
					MaterialProduct.PROPERTYNAME_WEIGHTEDAVERAGECOSTMEASURE, MaterialProduct.PROPERTYNAME_WEIGHTEDAVERAGECOSTDATE,
					MaterialProduct.PROPERTYNAME_WEIGHTEDAVERAGECOSTMEASUREGROUPMEASURE, MaterialProduct.PROPERTYNAME_LASTPURCHASEPRICE,
					MaterialProduct.PROPERTYNAME_LASTPURCHASEPRICEMEASURE, MaterialProduct.PROPERTYNAME_LASTPURCHASEPRICEMEASUREGROUPMEASURE,
					MaterialProduct.PROPERTYNAME_LASTPURCHASEPRICEDATE, MaterialProduct.PROPERTYNAME_TOBUY,
					MaterialProduct.PROPERTYNAME_COSTFOLLOWDEFAULTSUPPLIER, MaterialProduct.PROPERTYNAME_INCLUDEINPRICELIST,
					// mandatory field with no default value, keep it
					// MaterialProduct.PROPERTYNAME_COSTCALCULATIONTYPEENUM,
					// don't clone it but set to 0 later, this is mandatory
					MaterialProduct.PROPERTYNAME_COST);
			// force 0, cost is mandatory
			cloneBean.setCost(new MonetaryAmount(BigDecimal.ZERO, originalProduct.getCost().getCurrency()));

			initializeProductLocationStock(cloneBean);
		}

		return cloneBean;
	}
}
