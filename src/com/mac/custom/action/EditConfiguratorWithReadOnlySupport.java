package com.mac.custom.action;

import java.util.List;

import org.xml.sax.Attributes;

import com.mac.custom.bo.MacOrder;
import com.mac.custom.bo.MacOrderDetail;
import com.mac.custom.form.MacOrderFormCustom;
import com.netappsid.bo.model.Model;
import com.netappsid.erp.client.action.EditConfigurator;

/**
 * Extends {@link EditConfigurator} to open the configurator, but will open in read only mode if the status requires it.
 * 
 * @author plefebvre
 */
public class EditConfiguratorWithReadOnlySupport extends EditConfigurator
{
	public EditConfiguratorWithReadOnlySupport(Attributes attributes)
	{
		super(attributes);
	}

	@Override
	protected boolean isForceReadOnly()
	{
		List<Object> details = getBusinessObjectSelector().getBusinessObjects();
		for (Object detail : details)
		{
			MacOrderDetail orderDetail = (MacOrderDetail) Model.getTarget(detail);
			MacOrder order = orderDetail.getMacOrder();

			if (!MacOrderFormCustom.isWorkLoadModifiable(order))
			{
				return true;
			}
		}

		return false;
	}
}
