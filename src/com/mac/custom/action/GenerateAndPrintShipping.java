package com.mac.custom.action;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.sf.jasperreports.engine.JasperPrint;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;

import com.mac.custom.services.order.beans.MacShippingServicesBean;
import com.mac.custom.services.order.interfaces.remote.MacShippingServicesRemote;
import com.mac.custom.services.report.beans.MacReportServicesBean;
import com.mac.custom.services.report.interfaces.MacReportServices;
import com.mac.custom.utils.Utils;
import com.netappsid.erp.client.action.Printing;
import com.netappsid.erp.client.action.PrintingMultipleEntities;
import com.netappsid.erp.client.action.shipping.GenerateShipping;
import com.netappsid.erp.client.utils.ERPSystemVariable;
import com.netappsid.erp.server.bo.Carrier;
import com.netappsid.erp.server.bo.Deliverable;
import com.netappsid.erp.server.bo.Order;
import com.netappsid.erp.server.bo.Setup;
import com.netappsid.erp.server.bo.Shipping;
import com.netappsid.erp.server.bo.User;
import com.netappsid.erp.server.services.beans.ShippingServicesBean;
import com.netappsid.erp.server.services.interfaces.remote.ShippingServicesRemote;
import com.netappsid.erp.server.services.resultholders.ResultHolder;
import com.netappsid.framework.gui.components.SearchForm;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.framework.utils.SystemVariable;
import com.netappsid.gui.utils.WindowUtils.WindowPosition;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.beans.PersistenceBean;
import com.netappsid.services.interfaces.Loader;
import com.netappsid.services.interfaces.Persistence;

public class GenerateAndPrintShipping extends GenerateShipping
{
	private static final Logger LOGGER = Logger.getLogger(GenerateAndPrintShipping.class);

	private final ServiceLocator<Loader> loaderServiceLocator = new ServiceLocator<Loader>(LoaderBean.class);
	private final ServiceLocator<MacShippingServicesRemote> macShippingServicesLocator;
	private final User user;
	// Ajout GNIC 11/12/2013 : Génération du fichier Calberson pour l'impression des étiquettes
	private final MacReportServices macReportServicesLocator = new ServiceLocator<MacReportServices>(MacReportServicesBean.class).get();

	// Fin Ajout GNIC 11/12/2013

	private final ServiceLocator<ShippingServicesRemote> shippingServicesLocator = new ServiceLocator<ShippingServicesRemote>(ShippingServicesBean.class);
	private final ServiceLocator<Persistence> persistenceServicesServicesLocator = new ServiceLocator<Persistence>(PersistenceBean.class);

	public GenerateAndPrintShipping(Attributes attributes)
	{
		this(attributes, new ServiceLocator<MacShippingServicesRemote>(MacShippingServicesBean.class), new ServiceLocator<ShippingServicesRemote>(
				ShippingServicesBean.class), SystemVariable.<User> getVariable(ERPSystemVariable.USER));
	}

	protected GenerateAndPrintShipping(Attributes attributes, ServiceLocator<MacShippingServicesRemote> macShippingServicesLocator,
			ServiceLocator<ShippingServicesRemote> shippingServicesLocator, User user)
	{
		super(attributes, shippingServicesLocator, user);
		this.user = user;
		this.macShippingServicesLocator = macShippingServicesLocator;
	}

	@Override
	public void doLockAction(ActionEvent e)
	{
		final Map<Order, List<Deliverable>> deliverablesByOrder = new HashMap<Order, List<Deliverable>>();
		Class<?> orderClass = null;

		// We'll use these variables to know whether all orders have a carrier in common, and which one
		Carrier commonCarrier = null;
		boolean foundOrder = false;

		// If at least one deliverable is selected, iterate over all selected deliverables. We want to obtain the DeliverableDocument each of them belongs to
		// and group the deliverables by said orders.
		if (!getModel().getBusinessObjects().isEmpty())
		{
			for (Object selectedItem : getModel().getBusinessObjects())
			{
				Deliverable deliverable = (Deliverable) selectedItem;

				// Look for an order that has that document number
				List<Order> orderList = loaderServiceLocator.get().findByField(Order.class, Order.class, Order.PROPERTYNAME_NUMBER,
						deliverable.getDocumentNumber());

				// If we found an order, add it to the list of orders
				if (orderList != null && !orderList.isEmpty())
				{
					Order order = orderList.get(0);
					orderClass = order.getClass();
					if (!deliverablesByOrder.containsKey(order))
					{
						deliverablesByOrder.put(order, new ArrayList<Deliverable>());
					}
					List<Deliverable> orderDeliverables = deliverablesByOrder.get(order);
					orderDeliverables.add(deliverable);

					// If this is the first order we find, then take its carrier as our common carrier
					if (!foundOrder)
					{
						commonCarrier = order.getCarrier();
					}
					// If this is the second order or later, set common carrier to null if they don't match
					else
					{
						if (commonCarrier != null && !commonCarrier.equals(order.getCarrier()))
						{
							commonCarrier = null;
						}
					}

					foundOrder = true;
				}
			}
		}

		if (deliverablesByOrder.isEmpty())
		{
			showEmptyBusinessObjectIdsMessage();
		}
		else
		{
			// If we have at least an order, then we can go on.

			// Ask the user to select a Carrier.
			Carrier selectedCarrier = promptCarrier(commonCarrier);

			// Interpret no selection as a cancel
			if (selectedCarrier != null)
			{
				// Pass only ids to server
				Map<Serializable, List<Serializable>> itemToShipIdsByDocumentIds = new HashMap<Serializable, List<Serializable>>();
				for (Entry<Order, List<Deliverable>> entry : deliverablesByOrder.entrySet())
				{
					Serializable orderId = entry.getKey().getId();
					List<Serializable> deliverableList = new ArrayList<Serializable>();
					itemToShipIdsByDocumentIds.put(orderId, deliverableList);
					for (Deliverable deliverable : entry.getValue())
					{
						deliverableList.add(deliverable.getItemToShipId());
					}
				}

				// Change the orders' carrier to the provided one and save the change to the orders
				List<Order> orders = new ArrayList<Order>();
				orders.addAll(deliverablesByOrder.keySet());
				for (Order order : orders)
				{
					order.setCarrier(selectedCarrier);
				}
				persistenceServicesServicesLocator.get().save(orders, null);

				ResultHolder<Shipping> shippingResult = shippingServicesLocator.get().generateShippingFromItemsToShip(itemToShipIdsByDocumentIds, orderClass,
						user, true);

				// Load the shipping associations (we actually need to reload the shippings because they come from a different hibernate context)
				List<Serializable> shippingIds = new ArrayList<Serializable>();
				for (Shipping shipping : shippingResult.getResults())
				{
					shippingIds.add(shipping.getId());
				}
				shippingResult.getResults().clear();
				shippingResult.getResults().addAll(loaderServiceLocator.get().findByIdCollection(Shipping.class, Shipping.class, shippingIds));

				if (!shippingResult.hasErrors())
				{
					Printing print = new PrintingMultipleEntities(shippingResult.getResults(), getIntelligentPanel());
					print.doAction(null);
				}
				showResultMessage(shippingResult);

				try
				{
					if (!shippingResult.getResults().isEmpty())
					{
						String fileTempPath = null;
						String fileSavePath = null;
						String inParameter = null;
						String fieldDelimiter = null;
						JasperPrint jasperPrint = null;
						ArrayList arrayList = new ArrayList();
						Setup setup = loaderServiceLocator.get().findAll(Setup.class, Setup.class).get(0);
						File fileCible = null;
						List<File> arrayFile = new ArrayList<File>();

						Integer counter = 0;

						for (Shipping shipping : shippingResult.getResults())
						{

							// On ne traite que les BL Calberson
							if (shipping.getCarrier().getCode().startsWith("CAL"))
							{
								counter = counter + 1;
								inParameter = "NumeroBL:" + shipping.getNumber();

								if (counter == 1)
								{
									arrayList = macReportServicesLocator.exportRapport("shipping", inParameter);
									fileCible = new File((String) arrayList.get(3));

								}
								else
								{
									arrayList.clear();
									arrayList = macReportServicesLocator.exportRapport("shipping", inParameter);
								}
								jasperPrint = (JasperPrint) arrayList.get(0);
								fieldDelimiter = (String) arrayList.get(2);
								fileTempPath = arrayList.get(1) + "tmp\\" + shipping.getNumber() + ".txt";
								com.mac.custom.utils.ImpressionJasper.genereCSV(jasperPrint, fileTempPath, fieldDelimiter);
								fileSavePath = arrayList.get(1) + "save\\" + shipping.getNumber() + ".txt";

								File fileTemp = new File(fileTempPath);
								File fileSave = new File(fileSavePath);
								Utils.CopierFichier(fileTemp, fileSave);
								arrayFile.add(fileTemp);

							}

						}
						Utils.appendFichier(arrayFile, fileCible);
					}

				}
				catch (Exception e1)
				{

					LOGGER.error(e1, e1);
				} // Ajout GNIC 11/12/2013
			}
		}

	}

	/**
	 * Asks the user to selected a {@link Carrier} to ship with.
	 * 
	 * @param commonCarrier
	 *            A carrier that all orders have in common. It's simply displayed to the user. If they don't all share the same carrier, this should be null.
	 * @return
	 */
	private Carrier promptCarrier(Carrier commonCarrier)
	{
		Carrier carrier = null;

		// Ask the user to select a Carrier.
		SearchForm search = new SearchForm(getParentModel().getBeanClass(), Carrier.class);
		search.setPosition(WindowPosition.CENTER);

		// If all orders have the same carrier, help user out by putting it in the title
		if (commonCarrier != null)
		{
			search.setTitle(search.getTitle() + " (" + getString("commonCarrier") + " " + commonCarrier.getCode() + ")");
		}

		search.setModal(true);
		search.setVisible(true);

		// If a carrier was selected...
		if (search.hasExitedWithOK())
		{
			carrier = (Carrier) search.getValue();
		}

		return carrier;
	}
}
