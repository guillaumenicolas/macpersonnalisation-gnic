package com.mac.custom.action;

import java.awt.event.ActionEvent;

import org.xml.sax.Attributes;

import com.mac.custom.exception.ExceptionInterface;
import com.mac.custom.services.crm.bean.ImportContactCRMServiceBean;
import com.mac.custom.services.crm.remote.ImportContactCRMServiceRemote;
import com.netappsid.action.AbstractAction;
import com.netappsid.framework.utils.ServiceLocator;

public class ImportationContact extends AbstractAction
{
	private final ServiceLocator<ImportContactCRMServiceRemote> serviceLocator;

	public ImportationContact(Attributes atts)
	{
		super(atts);
		serviceLocator = new ServiceLocator<ImportContactCRMServiceRemote>(ImportContactCRMServiceBean.class);
	}

	@Override
	public void doAction(ActionEvent arg0)
	{
		try
		{
			serviceLocator.get().methodeImportation();
		}
		catch (ExceptionInterface e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
