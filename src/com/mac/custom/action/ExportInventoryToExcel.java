package com.mac.custom.action;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.xml.sax.Attributes;

import com.google.common.collect.Lists;
import com.netappsid.datatypes.MonetaryAmount;
import com.netappsid.erp.client.action.ExportElementsToXls;
import com.netappsid.erp.server.bo.InventoryAdjustmentType;
import com.netappsid.erp.server.bo.Location;
import com.netappsid.erp.server.bo.Stocktaking;
import com.netappsid.erp.server.bo.StocktakingDetail;
import com.netappsid.erp.server.bo.Supplier;
import com.netappsid.erp.server.bo.Warehouse;
import com.netappsid.erp.server.datatypes.GroupMeasureValue;
import com.netappsid.erp.server.services.utils.SetupUtils;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.Loader;

/**
 * Exports selected (as in {@link StocktakingDetail#isSelected()}) StocktakingDetails to an Excel file. This file matches what {@link ImportInventory} expects.<br/>
 * 
 * 
 * @author ftaillefer
 * 
 */
public class ExportInventoryToExcel extends ExportElementsToXls<StocktakingDetail>
{
	private final ServiceLocator<Loader> loaderLocator = new ServiceLocator<Loader>(LoaderBean.class);

	/**
	 * Defines the columns in the Excel file. This is not the same as columns in the stocktaking form.
	 * 
	 * @author ftaillefer
	 * 
	 */
	private final InventoryAdjustmentType adjustmentType = SetupUtils.INSTANCE.getMainSetup().getStocktakingInventoryAdjustmentType();

	public static enum InventoryExcelColumnEnum
	{
		// The order in which those are declared is important. Their ordinal is expected to match their index in the excel file.
		MATERIAL_CODE("inventoryExportMaterialCode"), WAREHOUSE_CODE("inventoryExportWarehouseCode"), LOCATION_CODE("inventoryExportLocationCode"), ADJUSTMENT_TYPE(
				"inventoryExportAdjustmentType"), QUANTITY_IN_STOCK("inventoryExportQuantityInStock"), QUANTITE_INVENTORIE("inventoryExportQuantiteInventorie"), INVENTORY_DATE(
				"inventoryExportInventoryDate"), DEFAULT_SUPPLIER("inventoryExportDefaultSupplier"), FAMILLE_ACHAT("inventoryExportFamilleAchat"), SOUS_FAMILLE_ACHAT(
				"inventoryExportSousFamilleAchat"), UNITE_STOCK_ARTICLE("inventoryExportUniteStock"), DPA_UNITE_STOCK("inventoryExportDPA"), VALORISATION_THEORIQUE(
				"inventoryExportValorisationTheorique"), VALORISATION_INVENTORIE("inventoryExportValorisationInventorie"), ECART_QUANTITE(
				"inventoryExportEcartQuantite"), ECART_VALORISATION("inventoryExportEcartValorisation")

		;

		private String titleKey;

		private InventoryExcelColumnEnum(String titleKey)
		{
			this.titleKey = titleKey;
		}

		/**
		 * Returns this column's title key, to be used for translations.
		 * 
		 * @return
		 */
		public String getTitleKey()
		{
			return titleKey;
		}

		/**
		 * Returns this column's index.
		 * 
		 * @return
		 */
		public int getIndex()
		{
			return ordinal();
		}
	}

	private CellStyle regularCellStyle;

	public ExportInventoryToExcel(Attributes attributes)
	{
		super(attributes);
	}

	@Override
	protected List<? extends StocktakingDetail> getElementsToExport()
	{
		List<StocktakingDetail> toExport = Lists.newArrayList();

		Stocktaking stocktaking = (Stocktaking) getModel().getParentModel().getBean();
		List<StocktakingDetail> filteredDetails = stocktaking.getFiltredStocktakingDetails();
		for (StocktakingDetail currentDetail : filteredDetails)
		{
			if (currentDetail.isSelected())
			{
				toExport.add(currentDetail);
			}
		}

		return toExport;
	}

	@Override
	protected void onBeginExport(Workbook workbook)
	{
		// We want to use the same CellStyle for every regular cell, but we don't need to create a new CellStyle every row - create one once, now
		regularCellStyle = buildRegularCellStyle(workbook);
	}

	@Override
	protected String getSheetTitle()
	{
		return getString("inventoryExportSheetTitle");
	}

	@Override
	protected int addHeader(Sheet detailSheet, int row)
	{
		Row headerRow = detailSheet.createRow(row++);
		CellStyle cellStyle = buildHeaderCellStyle(detailSheet.getWorkbook());

		for (InventoryExcelColumnEnum currentColumn : InventoryExcelColumnEnum.values())
		{
			Cell currentCell = headerRow.createCell(currentColumn.getIndex());
			currentCell.setCellValue(getString(currentColumn.getTitleKey()));
			currentCell.setCellStyle(cellStyle);
		}
		headerRow.setHeight((short) (headerRow.getHeight() * 4));

		return row;
	}

	/**
	 * Builds and returns the {@link CellStyle} to use for the header row.
	 * 
	 * @param workbook
	 * @return
	 */
	private CellStyle buildHeaderCellStyle(Workbook workbook)
	{
		Font headerFont = workbook.createFont();
		headerFont.setItalic(true);
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setFontHeightInPoints((short) 9);

		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		headerCellStyle.setFillForegroundColor(HSSFColor.LIME.index);
		headerCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		headerCellStyle.setFont(headerFont);
		headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerCellStyle.setBorderTop(CellStyle.BORDER_THIN);
		headerCellStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerCellStyle.setBorderRight(CellStyle.BORDER_THIN);

		return headerCellStyle;
	}

	/**
	 * Builds and returns the {@link CellStyle} to use for non-header rows.
	 * 
	 * @param workbook
	 * @return
	 */
	private CellStyle buildRegularCellStyle(Workbook workbook)
	{
		CellStyle cellStyle = workbook.createCellStyle();
		cellStyle.setBorderBottom(CellStyle.BORDER_THIN);
		cellStyle.setBorderTop(CellStyle.BORDER_THIN);
		cellStyle.setBorderLeft(CellStyle.BORDER_THIN);
		cellStyle.setBorderRight(CellStyle.BORDER_THIN);

		Font font = workbook.createFont();
		font.setFontHeightInPoints((short) 11);

		cellStyle.setFont(font);

		return cellStyle;
	}

	@Override
	protected int exportElement(Sheet detailSheet, int row, StocktakingDetail element)
	{
		Row elementRow = detailSheet.createRow(row++);

		// Column A - Material code
		Cell materialCell = elementRow.createCell(InventoryExcelColumnEnum.MATERIAL_CODE.getIndex());
		materialCell.setCellValue(element.getInventoryProduct().getCode());
		materialCell.setCellStyle(regularCellStyle);

		// Column B - Warehouse code
		Cell warehouseCell = elementRow.createCell(InventoryExcelColumnEnum.WAREHOUSE_CODE.getIndex());
		Location location = element.getLocation();
		Warehouse warehouse = location == null ? null : location.getWarehouse();
		warehouseCell.setCellValue(warehouse == null ? null : warehouse.getCode());
		warehouseCell.setCellStyle(regularCellStyle);

		// Column C - Location code
		Cell locationCell = elementRow.createCell(InventoryExcelColumnEnum.LOCATION_CODE.getIndex());
		locationCell.setCellValue(location == null ? null : location.getCode());
		locationCell.setCellStyle(regularCellStyle);

		// Column D - Adjustment type
		Cell adjustmentTypeCell = elementRow.createCell(InventoryExcelColumnEnum.ADJUSTMENT_TYPE.getIndex());
		adjustmentTypeCell.setCellValue(adjustmentType == null ? null : adjustmentType.getCode());
		adjustmentTypeCell.setCellStyle(regularCellStyle);

		// Column E - Quantity in stock
		Cell quantityCell = elementRow.createCell(InventoryExcelColumnEnum.QUANTITY_IN_STOCK.getIndex());
		GroupMeasureValue stockQuantity = element.getStockQuantity();

		BigDecimal stockQuantityValue = stockQuantity == null ? null : stockQuantity.getValue();
		Double stockQuantityDouble = stockQuantityValue == null ? null : stockQuantityValue.setScale(2, RoundingMode.HALF_UP).doubleValue();
		// Can't set put null Double into a cell, it would cause an autoboxing NPE
		if (stockQuantityDouble != null)
		{
			quantityCell.setCellValue(stockQuantityDouble);
		}
		quantityCell.setCellStyle(regularCellStyle);

		// quantite inventorie
		Double qteInventorie = 0d;
		if (element.getNewQuantity().getValue() != null)
		{
			qteInventorie = element.getNewQuantity().getValue().setScale(2, RoundingMode.HALF_UP).doubleValue();
		}
		Cell quantiteInventorie = elementRow.createCell(InventoryExcelColumnEnum.QUANTITE_INVENTORIE.getIndex());
		if (qteInventorie != null)
		{
			quantiteInventorie.setCellValue(qteInventorie);
		}
		quantiteInventorie.setCellStyle(regularCellStyle);

		// Column F - Inventory date
		Cell inventoryDateCell = elementRow.createCell(InventoryExcelColumnEnum.INVENTORY_DATE.getIndex());
		Date date = element.getStocktaking().getDate();
		inventoryDateCell.setCellValue(date == null ? null : new SimpleDateFormat("yyyy-MM-dd").format(date));
		inventoryDateCell.setCellStyle(regularCellStyle);

		// Column G - fournisseur par defaut
		Cell defaultSupplier = elementRow.createCell(InventoryExcelColumnEnum.DEFAULT_SUPPLIER.getIndex());
		Supplier supplier;
		try
		{
			supplier = element.getInventoryProduct().getDefaultSupplier().getSupplier();
		}
		catch (Exception e)
		{
			// pas de suplier par defaut donc null
			supplier = null;
		}

		defaultSupplier.setCellValue(supplier == null ? null : supplier.getName());
		defaultSupplier.setCellStyle(regularCellStyle);

		// Column H Famille Achat
		Cell familleAchat = elementRow.createCell(InventoryExcelColumnEnum.FAMILLE_ACHAT.getIndex());
		String famAchat = "";
		try
		{
			famAchat = element.getInventoryProduct().getGroupingModels().get(0).getDescription("fr");
		}
		catch (Exception e)
		{
			// pas de famille
		}
		familleAchat.setCellValue(famAchat == null ? null : famAchat);
		familleAchat.setCellStyle(regularCellStyle);

		// Column I Sous Famille Achat
		Cell sousFamilleAchat = elementRow.createCell(InventoryExcelColumnEnum.SOUS_FAMILLE_ACHAT.getIndex());
		String sousFamAchat = "";
		try
		{
			sousFamAchat = element.getInventoryProduct().getGroupingModels().get(1).getDescription("fr");
		}
		catch (Exception e)
		{
			// pas de sous famille
		}
		sousFamilleAchat.setCellValue(sousFamAchat == null ? null : sousFamAchat);
		sousFamilleAchat.setCellStyle(regularCellStyle);

		// Column J Unite de stock de l'article
		Cell uniteStockArticle = elementRow.createCell(InventoryExcelColumnEnum.UNITE_STOCK_ARTICLE.getIndex());
		uniteStockArticle.setCellValue(element.getInventoryProduct().getInventoryMeasure().toString() == null ? null : element.getInventoryProduct()
				.getInventoryMeasure().toString());
		uniteStockArticle.setCellStyle(regularCellStyle);

		// formule magique de conversion d'unite : dpa en unite de stock
		Double dpaUniteDeStockDouble = null;
		MonetaryAmount dpaUniteDeStock = null;
		try
		{
			if (stockQuantity != null)
			{
				dpaUniteDeStock = element
						.getInventoryProduct()
						.getLastPurchasePrice()
						.multiply(
								new GroupMeasureValue(stockQuantity, BigDecimal.ONE).to(element.getInventoryProduct().getLastPurchasePriceMeasure()).getValue());
			}
		}
		catch (Exception e)
		{}

		if (dpaUniteDeStock != null)
		{
			dpaUniteDeStockDouble = dpaUniteDeStock.getValue().setScale(2, RoundingMode.HALF_UP).doubleValue();
		}

		GroupMeasureValue stockQtyInCostMeasure = null;

		try
		{
			stockQtyInCostMeasure = element.getStockQuantity().to(element.getInventoryProduct().getLastPurchasePriceMeasure());
		}
		catch (Exception e)
		{
			stockQtyInCostMeasure = null;
		}
		BigDecimal stockValueInCostMeasure = null;
		if (stockQtyInCostMeasure != null)
		{
			stockValueInCostMeasure = stockQtyInCostMeasure.getValue().multiply(element.getInventoryProduct().getLastPurchasePrice().getValue());
		}
		// column K DPA en unite de stock
		Cell dpaEnUniteStock = elementRow.createCell(InventoryExcelColumnEnum.DPA_UNITE_STOCK.getIndex());
		dpaEnUniteStock.setCellValue(dpaUniteDeStockDouble == null ? null : dpaUniteDeStockDouble.toString());
		dpaEnUniteStock.setCellStyle(regularCellStyle);

		// column L - Valorisation th�orique=quantit� th�orique * DPA en unit� de stock
		Cell valorisationTheorique = elementRow.createCell(InventoryExcelColumnEnum.VALORISATION_THEORIQUE.getIndex());

		if (stockQuantityDouble != null)
		{
			if (dpaUniteDeStockDouble != null)
			{
				valorisationTheorique.setCellValue(dpaUniteDeStockDouble * stockQuantityDouble);
			}

		}
		valorisationTheorique.setCellStyle(regularCellStyle);

		// column M - - Valorisation inventori� = Quantit� inventori� * DPA en unit� de stock
		Cell valorisationInventorie = elementRow.createCell(InventoryExcelColumnEnum.VALORISATION_INVENTORIE.getIndex());

		if (qteInventorie != null)
		{
			if (dpaUniteDeStockDouble != null)
			{
				valorisationInventorie.setCellValue(dpaUniteDeStockDouble * qteInventorie);
			}
		}
		valorisationInventorie.setCellStyle(regularCellStyle);

		// column N - - Ecart Quantit�s (Th�orique-Inventori�) = colonne variation de 360
		Cell ecartQuantite = elementRow.createCell(InventoryExcelColumnEnum.ECART_QUANTITE.getIndex());
		if (stockQuantityDouble != null)
		{
			if (qteInventorie != null)
			{
				ecartQuantite.setCellValue(stockQuantityDouble - qteInventorie);
			}
		}
		ecartQuantite.setCellStyle(regularCellStyle);

		// column O - - Ecart valorisation (Th�orique-Inventori�) = Valorisation th�orique � valorisation inventori�
		Cell ecartValorisation = elementRow.createCell(InventoryExcelColumnEnum.ECART_VALORISATION.getIndex());

		if (dpaUniteDeStockDouble != null)
		{
			if (stockQuantityDouble != null)
			{
				if (qteInventorie != null)
				{
					ecartValorisation.setCellValue((dpaUniteDeStockDouble * stockQuantityDouble) - (dpaUniteDeStockDouble * qteInventorie));
				}
			}
		}
		ecartValorisation.setCellStyle(regularCellStyle);
		return row;
	}

	@Override
	protected int getColumnCount()
	{
		return InventoryExcelColumnEnum.values().length;
	}

	@Override
	protected void showNothingToExportMessage()
	{
		// Exported rows are those that are selected, so we can use the generic no line selected message here
		showNoLineSelectedMessage();
	}

}
