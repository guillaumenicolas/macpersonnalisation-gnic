package com.mac.custom.action;

import java.awt.event.ActionEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.print.PrintServiceLookup;

import net.sf.jasperreports.engine.JasperPrint;

import org.xml.sax.Attributes;

import com.netappsid.action.SaveAndReload;
import com.netappsid.bo.Entity;
import com.netappsid.component.ExpandableDialogData;
import com.netappsid.component.OptionPane.MessageType;
import com.netappsid.erp.client.action.Print;
import com.netappsid.erp.client.utils.ERPSystemVariable;
import com.netappsid.erp.server.bo.DocumentStatus;
import com.netappsid.erp.server.bo.ReportDetail;
import com.netappsid.erp.server.bo.ReportType;
import com.netappsid.erp.server.bo.User;
import com.netappsid.erp.server.services.beans.ReportingServicesBean;
import com.netappsid.erp.server.services.interfaces.remote.ReportingServicesRemote;
import com.netappsid.erp.server.services.resultholders.PrintResult;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.framework.utils.SystemVariable;
import com.netappsid.resources.Translator;

public class SaveAndReloadPackage extends SaveAndReload
{
	private static final String PRINT_LABEL = Translator.getString("Print");

	private final ServiceLocator<ReportingServicesRemote> reportServicesLocator = new ServiceLocator<ReportingServicesRemote>(ReportingServicesBean.class);

	public SaveAndReloadPackage(Attributes attributes)
	{
		super(attributes);
	}

	@Override
	public void doAction(ActionEvent event)
	{
		super.doAction(event);
		print();
	}

	protected void print()
	{
		Print print = new Print();
		PrintResult allPrintResult = new PrintResult();

		Long reportTypeID = ReportType.getReportIDFromRole("package");
		ReportType reportType = getLoaderServiceLocator().get()
				.findByField(ReportType.class, ReportType.class, DocumentStatus.PROPERTYNAME_INTERNALID, reportTypeID).get(0);

		if (reportType.getDefaultReport() != null)
		{
			Serializable userId = ((User) SystemVariable.getVariable(ERPSystemVariable.USER)).getId();
			List<PrintResult> printResults = new ArrayList<PrintResult>();
			for (ReportDetail reportDetail : reportType.getDefaultReport().getDetails())
			{
				HashMap<String, Object> parameters = new HashMap<String, Object>();
				parameters.put(Entity.PROPERTYNAME_ID, ((Entity<?>) getModel().getBean()).getId().toString());
				printResults.add(reportServicesLocator.get().print(reportDetail.getId(), parameters, userId));
			}

			for (PrintResult printResult : printResults)
			{
				for (JasperPrint jasperPrint : printResult.getResults())
				{
					print.processPrint(jasperPrint, PrintServiceLookup.lookupDefaultPrintService().getName(), null, 1);
				}
				allPrintResult.addAllFrom(printResult);
			}
			showMessages(allPrintResult, 1, 1);
		}
		else
		{
			allPrintResult.addError(Translator.getString("NODEFAULTREPORT"));
			showMessages(allPrintResult, 0, 1);
		}

	}

	protected void showMessages(final PrintResult printResult, final Integer passed, final Integer total)
	{
		MessageType messageType;
		final String message = String.format(getString("HaveBeenSuccessfullySent"), passed, total);
		String messageText = null;
		if (printResult.hasErrors())
		{
			messageType = MessageType.ERROR;
			messageText = printResult.getErrorMessagesText(Translator.getDictionnary());
		}
		else if (printResult.hasWarnings())
		{
			messageType = MessageType.WARNING;
			messageText = printResult.getWarningMessagesText(Translator.getDictionnary());
		}
		else
		{
			messageType = MessageType.INFORMATION;
			messageText = getString("NoErrorOrWarningToSignal");
		}

		ExpandableDialogData expandableDialogData = new ExpandableDialogData(PRINT_LABEL, message, messageType, messageText);
		getOptionPane().showExpandableMessages(expandableDialogData);
	}
}
