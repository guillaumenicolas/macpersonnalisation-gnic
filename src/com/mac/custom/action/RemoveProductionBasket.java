package com.mac.custom.action;

import java.awt.event.ActionEvent;

import org.xml.sax.Attributes;

import com.netappsid.component.ExpandableDialogData;
import com.netappsid.component.OptionPane.MessageType;
import com.netappsid.erp.server.bo.ProductionBasket;
import com.netappsid.erp.server.bo.WorkOrderRoutingStep;
import com.netappsid.erp.server.services.beans.ProductionServicesBean;
import com.netappsid.erp.server.services.interfaces.remote.ProductionServicesRemote;
import com.netappsid.erp.server.services.resultholders.ProductionResult;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.beans.PersistenceBean;
import com.netappsid.services.interfaces.Loader;
import com.netappsid.services.interfaces.Persistence;

public class RemoveProductionBasket extends com.netappsid.erp.client.action.production.RemoveProductionBasket
{
	private final ServiceLocator<Persistence> persistenceServiceLocator = new ServiceLocator<Persistence>(PersistenceBean.class);
	private final ServiceLocator<ProductionServicesRemote> productionServiceLocator = new ServiceLocator<ProductionServicesRemote>(ProductionServicesBean.class);
	private final ServiceLocator<Loader> loaderServicesLocator = new ServiceLocator<Loader>(LoaderBean.class);

	public RemoveProductionBasket(Attributes attributes)
	{
		super(attributes);
	}

	@Override
	public void doAction(ActionEvent e)
	{
		if (getBusinessObjectSelector() != null && getBusinessObjectSelector().getBusinessObjectsID() != null
				&& !getBusinessObjectSelector().getBusinessObjectsID().isEmpty())
		{
			ProductionBasket productionBasket = loaderServicesLocator.get().findById(ProductionBasket.class,
					getBusinessObjectSelector().getBusinessObjectsID().get(0));
			ProductionResult productionResult = productionServiceLocator.get().deleteProductionBasketsById(getBusinessObjectSelector().getBusinessObjectsID());

			String title;
			String message;
			MessageType messageType;

			if (productionResult.hasErrors())
			{
				title = getString("Delete");
				message = getString("ErrorWhileDeleting");
				messageType = MessageType.ERROR;
			}
			else
			{
				title = getString("Delete");
				message = getString("TheFollowingProductionBasketsHaveBeenRemoved");

				if (productionResult.hasWarnings())
				{
					messageType = MessageType.WARNING;
				}
				else
				{
					messageType = MessageType.INFORMATION;
				}

				WorkOrderRoutingStep wors = loaderServicesLocator.get()
						.findById(WorkOrderRoutingStep.class, productionBasket.getWorkOrderRoutingStep().getId());
				if (wors.getProductionBatch() != null)
				{
					wors.getProductionBatch().setClosed(false);
					persistenceServiceLocator.get().save(wors.getProductionBatch(), false);
				}
			}

			ExpandableDialogData expandableDialogData = new ExpandableDialogData(title, message, messageType, productionResult.getAllMessagesText());
			getOptionPane().showExpandableMessages(expandableDialogData);
		}
		else
		{
			getOptionPane().show(getDocumentPane(), getString("NoProductionBasketSelected"), getString("Delete"), MessageType.ERROR);
		}
	}
}
