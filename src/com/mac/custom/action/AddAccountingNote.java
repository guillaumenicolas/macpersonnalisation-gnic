package com.mac.custom.action;

import java.awt.event.ActionEvent;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;

import com.mac.custom.bo.MacOrder;
import com.netappsid.action.AbstractBoundAction;
import com.netappsid.component.FormDialog;
import com.netappsid.component.FormDialog.ButtonType;
import com.netappsid.component.OptionPane.MessageType;
import com.netappsid.erp.server.bo.Note;
import com.netappsid.erp.server.bo.TypeNote;
import com.netappsid.erp.server.bo.User;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;
import com.netappsid.framework.gui.utils.OptionPaneMessageDialogData;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.framework.utils.SystemVariable;
import com.netappsid.gui.utils.WindowUtils.WindowPosition;
import com.netappsid.resources.Translator;
import com.netappsid.resources.icons.Icon;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.Loader;

public class AddAccountingNote extends AbstractBoundAction
{
	private final ServiceLocator<Loader> loaderLocator = new ServiceLocator<Loader>(LoaderBean.class);
	private final String TYPE_NOTE_COMPTA = "COMPTA";

	private static Logger logger = Logger.getLogger(AddAccountingNote.class);

	public AddAccountingNote(Attributes attributes)
	{
		super(attributes);
		putValue(PROPERTY_ICON, Icon.REFRESH.getIcon(PROPERTY_DEFAULT_ICON_SIZE));
		setToolTipText("Ajout d'une Note Service Comptabilité");
	}

	@Override
	public void doAction(ActionEvent e)
	{
		try
		{
			boolean isNewNote = false;

			List<Serializable> macOrderListIDSelection = getBusinessObjectSelector().getBusinessObjectsID();
			if (macOrderListIDSelection != null && !macOrderListIDSelection.isEmpty())
			{
				if (macOrderListIDSelection.size() == 1)
				{
					MacOrder firstOrderSelected = loaderLocator.get().findAndLoadAssociationsById(MacOrder.class, macOrderListIDSelection.get(0));
					List<Note> noteList = firstOrderSelected.getNotes();
					Note firstNoteCompta = null;
					for (Note note : noteList)
					{
						if (note.getTypeNote().getCode().equals(TYPE_NOTE_COMPTA))
						{
							firstNoteCompta = note;
							break;
						}
					}
					if (firstNoteCompta == null)
					{
						isNewNote = true;
						firstNoteCompta = new Note();
						TypeNote typeNoteCompta = loaderLocator.get().findFirstByField(TypeNote.class, TypeNote.class, TypeNote.PROPERTYNAME_CODE,
								TYPE_NOTE_COMPTA);
						if (typeNoteCompta == null)
						{
							getOptionPane().show(
									new OptionPaneMessageDialogData(getDocumentPane(), "Erreur", Translator.getString("typeNoteComptaInexistant"),
											MessageType.ERROR));
							return;
						}
						firstNoteCompta.setTypeNote(typeNoteCompta);
						firstNoteCompta.setDate(new Date());
						firstNoteCompta.setUser((User) SystemVariable.getVariable("user"));
					}

					FormDialog formDialog = getComponentFactory().newFormDialog(getDocumentPane(), getForm(), getClass().getClassLoader(), ButtonType.OK,
							ButtonType.CANCEL);
					NAIDPresentationModel foundModel = formDialog.getDocumentPane().getPresentationModel("note");
					foundModel.setBean(firstNoteCompta);
					formDialog.setTitle(Translator.getString("note"));
					formDialog.setModal(true);
					formDialog.setPosition(WindowPosition.CENTER);
					formDialog.setModel(foundModel);

					formDialog.setVisible(true);

					if (formDialog.getSelectedButton() == ButtonType.OK)
					{
						if (isNewNote)
						{
							firstOrderSelected.getNotes().add(firstNoteCompta);
						}
						firstOrderSelected.saveObjects();
					}
				}
				else
				{
					getOptionPane().show(
							new OptionPaneMessageDialogData(getDocumentPane(), "Selection", Translator.getString("justOneLineSelectionForThisAction"),
									MessageType.WARNING));
				}
			}
		}
		catch (Exception e1)
		{
			e1.printStackTrace();
		}
	}
}
