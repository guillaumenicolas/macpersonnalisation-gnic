package com.mac.custom.action;

import java.awt.event.ActionEvent;

import org.xml.sax.Attributes;

import com.mac.custom.services.workflow.bean.MacWorkflowServicesBean;
import com.mac.custom.services.workflow.interfaces.remote.MacWorkflowServicesRemote;
import com.netappsid.action.AbstractBoundAction;
import com.netappsid.erp.server.bo.SaleOrder;
import com.netappsid.framework.utils.ServiceLocator;

public class TestARCAction extends AbstractBoundAction
{
	private final ServiceLocator<MacWorkflowServicesRemote> serviceLocator;

	public TestARCAction(Attributes atts)
	{
		super(atts);
		serviceLocator = new ServiceLocator<MacWorkflowServicesRemote>(MacWorkflowServicesBean.class);
	}

	@Override
	public void doAction(ActionEvent arg0)
	{
		SaleOrder saleOrder = (SaleOrder)getModel().getBean();
		try
		{
			serviceLocator.get().generateARCFile(saleOrder.getId());
		}
		catch (Exception e)
		
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
