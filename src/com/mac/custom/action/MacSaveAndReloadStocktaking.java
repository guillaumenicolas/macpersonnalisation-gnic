package com.mac.custom.action;

import java.util.Locale;

import org.xml.sax.Attributes;

import com.jgoodies.validation.ValidationResult;
import com.mac.custom.services.beans.MacStocktakingServicesBean;
import com.mac.custom.services.interfaces.MacStocktakingServices;
import com.netappsid.action.SaveAndReload;
import com.netappsid.erp.server.bo.Stocktaking;
import com.netappsid.framework.utils.ServiceLocator;

/**
 * Validate fon conflict with other stocktakings before save.
 * 
 */
public class MacSaveAndReloadStocktaking extends SaveAndReload
{
	private final ServiceLocator<MacStocktakingServices> stocktakingServicesServiceLocator = new ServiceLocator<MacStocktakingServices>(
			MacStocktakingServicesBean.class);

	public MacSaveAndReloadStocktaking(Attributes attributes)
	{
		super(attributes);
	}

	@Override
	protected ValidationResult validateModelBeforeSave()
	{
		ValidationResult validateModelBeforeSave = super.validateModelBeforeSave();

		validateModelBeforeSave.addAll(stocktakingServicesServiceLocator.get().validateConflicts(Locale.getDefault().getLanguage(), (Stocktaking) getModel().getBean()));

		return validateModelBeforeSave;
	}
}
