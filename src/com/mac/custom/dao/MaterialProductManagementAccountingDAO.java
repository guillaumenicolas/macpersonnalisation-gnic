package com.mac.custom.dao;

// Imports
import com.mac.custom.bo.CostCode;
import com.mac.custom.bo.CostCodeDetail;
import com.mac.custom.bo.MaterialProductManagementAccounting;
import com.mac.custom.bo.ProductCodeAnalytic;
import com.netappsid.erp.server.bo.Company;
import com.netappsid.erp.server.dao.ERPDAO;

public class MaterialProductManagementAccountingDAO<T extends MaterialProductManagementAccounting> extends ERPDAO<T>
{
	@Override
	public void loadAssociations(Object object)
	{
		logLoadAssociationsCall(object);

		if (object instanceof Company)
		{
			loadCompanyAssociations((Company) object);
		}
		else if (object instanceof CostCodeDetail)
		{
			loadCostCodeDetailAssociations((CostCodeDetail) object);
		}
		else if (object instanceof ProductCodeAnalytic)
		{
			loadProductCodeAnalyticAssociations((ProductCodeAnalytic) object);
		}
		else if (object instanceof CostCode)
		{
			loadCostCodeAssociations((CostCode) object);
		}
	}

	private void loadCompanyAssociations(Company company)
	{
		if (company != null)
		{
			initializeProxy(company);
			initializeProxy(company.getDescription());
		}
	}

	private void loadCostCodeDetailAssociations(CostCodeDetail object)
	{
		if (object != null)
		{
			initializeProxy(object);
			initializeProxy(object.getDescription());

			if (object.getCompany() != null)
			{
				initializeProxy(object.getCompany());
				initializeProxy(object.getCompany().getDescription());
			}
		}
	}

	private void loadCostCodeAssociations(CostCode costCode)
	{
		if (costCode != null)
		{
			initializeProxy(costCode);
			initializeProxy(costCode.getDescription());

			if (costCode.getCompany() != null)
			{
				initializeProxy(costCode.getCompany());
				initializeProxy(costCode.getCompany().getDescription());
			}
		}
	}

	private void loadProductCodeAnalyticAssociations(ProductCodeAnalytic object)
	{
		if (object != null)
		{
			initializeProxy(object);
			initializeProxy(object.getDescription());

			if (object.getCompany() != null)
			{
				initializeProxy(object.getCompany());
				initializeProxy(object.getCompany().getDescription());
			}
		}
	}
}
