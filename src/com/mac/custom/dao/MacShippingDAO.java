package com.mac.custom.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;

import com.mac.custom.bo.MacPackages;
import com.mac.custom.bo.MacShippingLocation;
import com.mac.custom.bo.MacWarehouse;
import com.mac.custom.services.customer.beans.DeclencheursEnCoursServicesBean;
import com.mac.custom.services.customer.interfaces.local.DeclencheursEnCoursServicesLocal;
import com.mac.custom.services.order.beans.MacOrderServicesBean;
import com.mac.custom.services.order.interfaces.MacOrderServices;
import com.netappsid.bo.EntityStatus;
import com.netappsid.bo.model.Model;
import com.netappsid.dao.DAOInitializeVisitor;
import com.netappsid.erp.server.bo.CommercialEntity;
import com.netappsid.erp.server.bo.Customer;
import com.netappsid.erp.server.bo.DocumentStatus;
import com.netappsid.erp.server.bo.InventoryProduct;
import com.netappsid.erp.server.bo.ItemToShip;
import com.netappsid.erp.server.bo.Order;
import com.netappsid.erp.server.bo.Shipping;
import com.netappsid.erp.server.bo.ShippingDetail;
import com.netappsid.erp.server.bo.Stock;
import com.netappsid.erp.server.bo.User;
import com.netappsid.erp.server.bo.Warehouse;
import com.netappsid.erp.server.dao.ShippingDAO;
import com.netappsid.erp.server.datatypes.GroupMeasureValue;
import com.netappsid.erp.server.interfaces.DocumentStatusHistory;
import com.netappsid.erp.server.services.beans.DocumentStatusServicesBean;
import com.netappsid.erp.server.services.interfaces.DocumentStatusServices;
import com.netappsid.erp.server.services.utils.SetupUtils;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.Loader;

public class MacShippingDAO<T extends Shipping> extends ShippingDAO<T>
{
	private final Loader loader = new ServiceLocator<Loader>(LoaderBean.class).get();
	private final DeclencheursEnCoursServicesLocal declencheursEnCoursServicesLocal = new ServiceLocator<DeclencheursEnCoursServicesLocal>(
			DeclencheursEnCoursServicesBean.class).get();

	private final DocumentStatusServices documentStatusServicesLocator = new ServiceLocator<DocumentStatusServices>(DocumentStatusServicesBean.class).get();

	private final MacOrderServices macOrderServicesLocator = new ServiceLocator<MacOrderServices>(MacOrderServicesBean.class).get();

	public MacShippingDAO()
	{
		addDAOInitializeVisitor(MacWarehouse.class, new DAOInitializeVisitor<MacWarehouse>()
			{
				@Override
				public void initialize(MacWarehouse macWarehouse)
				{
					if (macWarehouse != null)
					{
						for (MacShippingLocation location : macWarehouse.getShippingLocations())
						{
							initializeProxy(location);
						}
					}
				}
			});
	}

	@Override
	public T save(T shipping)
	{
		CommercialEntity commercialEntity = Model.getTarget(shipping.getCommercialEntity());

		if (Customer.class.isAssignableFrom(commercialEntity.getClass()))
		{
			declencheursEnCoursServicesLocal.ajouterDeclencheurEnCours(commercialEntity.getClass().getCanonicalName(), commercialEntity.getId());
		}

		T savedShipping = super.save(shipping);

		if (savedShipping.getOrder() != null/* && !DocumentStatus.CLOSE.equals(savedShipping.getOrder().getDocumentStatus().getInternalId()) */)
		{
			// We need to determine whether the order is all shipped at this point.
			// However, while the Shipping has been saved, it has not been committed or flushed yet.
			// We could simply flush it here, but this will take locks sooner than necessary.
			// Instead, we'll call a method that tells the service to manually include the Shipping in its check.
			if (macOrderServicesLocator.isOrderAllShippedAfterShipping(savedShipping.getOrder().getId(), savedShipping.getId()))
			{
				User user = SetupUtils.INSTANCE.getMainSetup().getAutomaticServiceUser();
				List<Order> orders = loader.findByIdCollection(Order.class, null, Arrays.asList(savedShipping.getOrder().getId()));
				for (Order order : orders)
				{
					if (!order.getDocumentStatus().isClose() && user != null)
					{
						order.setEntityStatus(EntityStatus.READY);
						order.getNewStatusHistory().setUser(user);
					}
				}

				documentStatusServicesLocator.setDocumentStatus((List<DocumentStatusHistory>) (Object) orders, DocumentStatus.CLOSE,
						user != null ? user.getId() : null);
			}

			assignShippingToPackages(savedShipping);
		}

		return savedShipping;
	}

	public void assignShippingToPackages(Shipping shipping)
	{
		if (shipping.getOrder() != null)
		{
			for (ItemToShip itemToShip : shipping.getOrder().getItemsToShip())
			{
				if (MacPackages.class.isAssignableFrom(Hibernate.getClass(itemToShip)))
				{
					MacPackages macPackages = (MacPackages) Model.getTarget(itemToShip);
					if (macPackages.getShipping() == null)
					{
						macPackages.setShipping(shipping);
					}
				}
			}
		}
	}

	@Override
	public void delete(T shipping)
	{
		CommercialEntity commercialEntity = Model.getTarget(shipping.getCommercialEntity());
		if (Customer.class.isAssignableFrom(commercialEntity.getClass()))
		{
			declencheursEnCoursServicesLocal.ajouterDeclencheurEnCours(commercialEntity.getClass().getCanonicalName(), shipping.getCommercialEntity().getId());
		}
		super.delete(shipping);
	}

	/**
	 * Changes the superclass behavior so that, if the provided ItemToShip's Warehouse has specific shipping Locations defined, the returned Stocks will only be
	 * from those Locations. If no shipping locations are defined, the superclass behavior is maintained.
	 */
	@Override
	protected List<Stock> getAvailableStocks(final Shipping shipping, final ShippingDetail shippingDetail, final ItemToShip itemToShip,
			final InventoryProduct inventoryProduct, final Serializable productRevisionId)
	{
		final Map<Serializable, Integer> locations = getShippingLocations(itemToShip);

		if (locations.isEmpty())
		{
			return super.getAvailableStocks(shipping, shippingDetail, itemToShip, inventoryProduct, productRevisionId);
		}

		// list all stocks in shipping location
		List<Stock> availableStocks = new ArrayList<Stock>(stockServicesLocal.getAvailableStocks(inventoryProduct.getId(), productRevisionId,
				itemToShip.getProperties(), itemToShip.getWarehouse().getId(), (List<Serializable>) new ArrayList<Serializable>(locations.keySet())));
		// sort for location sequence than stock date
		Collections.sort(availableStocks, new Comparator<Stock>()
			{
				@Override
				public int compare(Stock o1, Stock o2)
				{
					int c = locations.get(o1.getLocation().getId()) - locations.get(o2.getLocation().getId());
					if (c == 0)
					{
						return o1.getDate().compareTo(o2.getDate());
					}
					return 0;
				}
			});

		return availableStocks;
	}

	private Map<Serializable, Integer> getShippingLocations(final ItemToShip itemToShip)
	{
		List<MacShippingLocation> shippingLocations = Model.<Warehouse, MacWarehouse> getTarget(itemToShip.getWarehouse()).getShippingLocations();

		// map with locationid + sequence
		final Map<Serializable, Integer> locations = new HashMap<Serializable, Integer>();
		for (MacShippingLocation sl : shippingLocations)
		{
			locations.put(sl.getLocation().getId(), sl.getSequence());
		}
		return locations;
	}

	/**
	 * Changes the superclass behavior so that, if the provided ItemToShip's Warehouse has specific shipping Locations defined, the returned quantity will be
	 * calculated only from those Locations. If no shipping locations are defined, the superclass behavior is maintained.
	 */
	@Override
	protected GroupMeasureValue getTotalAvailableStockQuantity(InventoryProduct inventoryProduct, Serializable productRevisionId, ItemToShip itemToShip,
			List<Serializable> locationsId)
	{

		final Map<Serializable, Integer> locations = getShippingLocations(itemToShip);

		if (locations.isEmpty())
		{
			return super.getTotalAvailableStockQuantity(inventoryProduct, productRevisionId, itemToShip, locationsId);
		}

		return stockServicesLocal.getTotalAvailableStockQuantity(inventoryProduct.getId(), productRevisionId, itemToShip.getProperties(), itemToShip
				.getWarehouse().getId(), (List<Serializable>) new ArrayList<Serializable>(locations.keySet()));

	}
}
