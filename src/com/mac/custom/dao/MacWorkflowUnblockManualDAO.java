package com.mac.custom.dao;

import com.mac.custom.bo.MacWorkflowUnblockManual;
import com.mac.custom.bo.MacWorkflowUnblockAuto;
import com.netappsid.dao.GenericDAO;

public class MacWorkflowUnblockManualDAO<T extends MacWorkflowUnblockManual> extends GenericDAO<T> 
{
	@Override
	public void loadAssociations(Object object)
	{
		logLoadAssociationsCall(object);
		
		if (object instanceof MacWorkflowUnblockManual)
		{
			loadMacWfUnblockManualAssociations((MacWorkflowUnblockManual) object);
		}
	}

	private void loadMacWfUnblockManualAssociations(MacWorkflowUnblockManual macWorkflowUnblockManual) 
	{
		initializeProxy(macWorkflowUnblockManual);
	}
}
