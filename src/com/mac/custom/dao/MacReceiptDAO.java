package com.mac.custom.dao;

import com.mac.custom.services.customer.beans.DeclencheursEnCoursServicesBean;
import com.mac.custom.services.customer.interfaces.local.DeclencheursEnCoursServicesLocal;
import com.netappsid.bo.model.Model;
import com.netappsid.erp.server.bo.Receipt;
import com.netappsid.erp.server.dao.ReceiptDAO;
import com.netappsid.framework.utils.ServiceLocator;

public class MacReceiptDAO<T extends Receipt> extends ReceiptDAO<T>
{
	private final DeclencheursEnCoursServicesLocal declencheursEnCoursServicesLocal = new ServiceLocator<DeclencheursEnCoursServicesLocal>(
			DeclencheursEnCoursServicesBean.class).get();

	@Override
	public T save(T receipt)
	{
		declencheursEnCoursServicesLocal.ajouterDeclencheurEnCours(Model.getTarget(receipt.getCustomer()).getClass().getCanonicalName(), receipt.getCustomer()
				.getId());

		T savedOrder = super.save(receipt);

		return savedOrder;
	}

	@Override
	public void delete(T receipt)
	{
		declencheursEnCoursServicesLocal.ajouterDeclencheurEnCours(Model.getTarget(receipt.getCustomer()).getClass().getCanonicalName(), receipt.getCustomer()
				.getId());
		super.delete(receipt);
	}
}
