package com.mac.custom.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.hibernate.Hibernate;

import com.mac.custom.bo.MacPurchaseOrder;
import com.netappsid.bo.model.Model;
import com.netappsid.erp.server.bo.Address;
import com.netappsid.erp.server.bo.AddressCommunication;
import com.netappsid.erp.server.bo.AddressRoute;
import com.netappsid.erp.server.bo.Carrier;
import com.netappsid.erp.server.bo.City;
import com.netappsid.erp.server.bo.Communication;
import com.netappsid.erp.server.bo.Country;
import com.netappsid.erp.server.bo.District;
import com.netappsid.erp.server.bo.DocumentStatus;
import com.netappsid.erp.server.bo.PhoneCommunication;
import com.netappsid.erp.server.bo.PhoneCommunicationType;
import com.netappsid.erp.server.bo.PurchaseOrder;
import com.netappsid.erp.server.bo.PurchaseOrderDetail;
import com.netappsid.erp.server.bo.PurchaseRequisition;
import com.netappsid.erp.server.bo.Region;
import com.netappsid.erp.server.bo.Route;
import com.netappsid.erp.server.bo.State;
import com.netappsid.erp.server.bo.TaxGroup;
import com.netappsid.erp.server.bo.User;
import com.netappsid.erp.server.dao.PurchaseOrderDAO;
import com.netappsid.erp.server.services.beans.DocumentStatusServicesBean;
import com.netappsid.erp.server.services.interfaces.DocumentStatusServices;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.framework.utils.SystemVariable;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.Loader;

public class MacPurchaseOrderDAO extends PurchaseOrderDAO<MacPurchaseOrder>
{
	private final DocumentStatusServices documentStatusServicesLocator = new ServiceLocator<DocumentStatusServices>(DocumentStatusServicesBean.class).get();
	private final Loader loader = new ServiceLocator<Loader>(LoaderBean.class).get();

	@Override
	public void loadAssociations(Object object)
	{
		super.loadAssociations(object);
		object = Model.getTarget(object);

		if (MacPurchaseOrder.class.isAssignableFrom(Hibernate.getClass(object)))
		{
			loadMacPurchaseOrderAssociations((MacPurchaseOrder) object);
		}
		else if (Address.class.isAssignableFrom(Hibernate.getClass(object)))
		{
			loadAddressAssociations((Address) object);
		}
	}

	private void loadMacPurchaseOrderAssociations(MacPurchaseOrder purchaseOrder)
	{
		if (purchaseOrder != null)
		{
			initializeProxy(purchaseOrder);
			loadAddressAssociations(purchaseOrder.getShippingAddress());
		}
	}

	private void loadAddressAssociations(Address address)
	{
		if (address != null)
		{
			initializeProxy(address);
			City city = address.getCity();

			if (city != null)
			{
				initializeProxy(city);
				initializeProxy(city.getName());

				Region region = city.getRegion();
				if (region != null)
				{
					initializeProxy(region);
					initializeProxy(region.getName());

					State state = region.getState();
					if (state != null)
					{
						initializeProxy(state);
						initializeProxy(state.getName());

						TaxGroup taxGroup = state.getTaxGroup();
						if (taxGroup != null)
						{
							initializeProxy(taxGroup);
							initializeProxy(taxGroup.getDescription());
						}

						Country country = state.getCountry();

						if (country != null)
						{
							initializeProxy(country);
							initializeProxy(country.getName());
						}
					}

					for (Region subRegion : region.getSubRegions())
					{
						initializeProxy(subRegion);
						initializeProxy(subRegion.getName());
					}
				}

				for (District district : city.getDistricts())
				{
					initializeProxy(district);
					initializeProxy(district.getName());
				}
			}

			for (AddressCommunication addressCommunication : address.getCommunications())
			{
				loadCommunicationAssociations(addressCommunication.getCommunication());
			}
			for (AddressRoute addressRoute : address.getAddressRoutes())
			{
				initializeProxy(addressRoute);
				loadRouteAssociations(addressRoute.getRoute());
				loadCarrierAssociations(addressRoute.getCarrier());
			}
		}
	}

	private void loadCommunicationAssociations(Communication communication)
	{
		if (communication != null)
		{
			initializeProxy(communication);
			initializeProxy(communication.getDescription());

			Object target = Model.getTarget(communication);
			if (target instanceof PhoneCommunication)
			{
				PhoneCommunicationType phoneCommunicationType = ((PhoneCommunication) target).getPhoneCommunicationType();
				if (phoneCommunicationType != null)
				{
					initializeProxy(phoneCommunicationType);
					initializeProxy(phoneCommunicationType.getDescription());
				}
			}
		}
	}

	private void loadRouteAssociations(Route route)
	{
		if (route != null)
		{
			initializeProxy(route);
			initializeProxy(route.getDescription());
			loadCarrierAssociations(route.getDefaultCarrier());
		}
	}

	private void loadCarrierAssociations(Carrier carrier)
	{
		if (carrier != null)
		{
			initializeProxy(carrier);
			initializeProxy(carrier.getDescription());
		}
	}

	@Override
	public MacPurchaseOrder save(MacPurchaseOrder object)
	{
		// list of purchase requisition we need to set at NEW if the link is broken
		List<Serializable> purchaseRequisitionsToOpen = new ArrayList<Serializable>();
		if (object.getId() != null)
		{
			final PurchaseOrder originalPurchaseOrder = loader.findById(PurchaseOrder.class, null, object.getId());
			for (PurchaseOrderDetail detail : object.getDetails())
			{
				// if the order has no requisition, check if it had one
				if (detail.getPurchaseRequisition() == null)
				{
					PurchaseOrderDetail originalPurchaseOrderDetail = null;
					for (PurchaseOrderDetail originalDetail : originalPurchaseOrder.getDetails())
					{
						if (detail.hasSameID(originalDetail))
						{
							originalPurchaseOrderDetail = originalDetail;
							break;
						}
					}
					// if the order had a requisition and that requisition is close, reopen it
					if (originalPurchaseOrderDetail != null && originalPurchaseOrderDetail.getPurchaseRequisition() != null
							&& originalPurchaseOrderDetail.getPurchaseRequisition().getDocumentStatus().isClose())
					{
						purchaseRequisitionsToOpen.add(originalPurchaseOrderDetail.getPurchaseRequisition().getId());
					}
				}
			}
		}

		MacPurchaseOrder savedPurchaseOrder = super.save(object);
		PurchaseRequisition purchaseRequisition;
		User user = SystemVariable.getVariable("user");
		if (!purchaseRequisitionsToOpen.isEmpty())
		{
			documentStatusServicesLocator.setDocumentStatus(purchaseRequisitionsToOpen, PurchaseRequisition.class, DocumentStatus.NEW,
					user != null ? user.getId() : null);
		}
		for (PurchaseOrderDetail purchaseOrderDetail : savedPurchaseOrder.getDetails())
		{
			purchaseRequisition = purchaseOrderDetail.getPurchaseRequisition();
			if (purchaseRequisition != null && !purchaseRequisition.getDocumentStatus().isClose())
			{
				documentStatusServicesLocator.setDocumentStatus(Arrays.asList(purchaseRequisition.getId()), PurchaseRequisition.class, DocumentStatus.CLOSE,
						user != null ? user.getId() : null);
			}
		}

		return savedPurchaseOrder;
	}
}
