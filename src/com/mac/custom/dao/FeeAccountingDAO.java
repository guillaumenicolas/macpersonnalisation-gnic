package com.mac.custom.dao;

import com.mac.custom.bo.CostCode;
import com.mac.custom.bo.CostCodeDetail;
import com.mac.custom.bo.FeeAccounting;
import com.mac.custom.bo.FeeCodeAnalytic;
import com.mac.custom.bo.FeeManagementAccounting;
import com.netappsid.dao.GenericDAO;
import com.netappsid.erp.server.bo.Company;
import com.netappsid.erp.server.bo.Fee;

public class FeeAccountingDAO<T extends FeeAccounting> extends GenericDAO<T>
{
	@Override
	public void loadAssociations(Object object)
	{
		logLoadAssociationsCall(object);

		if (object instanceof FeeAccounting)
		{
			loadFeeAccountingAssociations((FeeAccounting) object);
		}
		else if (object instanceof Fee)
		{
			loadFeeAssociations((Fee) object);
		}
	}

	private void loadFeeAssociations(Fee fee)
	{
		if (fee != null)
		{
			initializeProxy(fee);
			initializeProxy(fee.getDescription());
		}
	}

	private void loadFeeAccountingAssociations(FeeAccounting feeAccounting)
	{
		if (feeAccounting != null)
		{
			initializeProxy(feeAccounting);
			initializeProxy(feeAccounting.getFee().getDescription());
			initializeProxy(feeAccounting.getFeeManagementAccountings());

			if (feeAccounting.getFeeManagementAccountings() != null)
			{
				for (FeeManagementAccounting feeManagementAccounting : feeAccounting.getFeeManagementAccountings())
				{
					initializeProxy(feeManagementAccounting);
					loadCompanyAssociations(feeManagementAccounting.getCompany());
					loadCostCodeAssociations(feeManagementAccounting.getCostCodeSale());
					loadCostCodeAssociations(feeManagementAccounting.getCostCodePurchase());
					loadCostCodeDetailAssociations(feeManagementAccounting.getCostCodeDetailSale());
					loadCostCodeDetailAssociations(feeManagementAccounting.getCostCodeDetailPurchase());
					loadFeeCodeAnalyticAssociations(feeManagementAccounting.getFeeCodeAnalyticSale());
					loadFeeCodeAnalyticAssociations(feeManagementAccounting.getFeeCodeAnalyticPurchase());
				}
			}
		}
	}

	private void loadCompanyAssociations(Company company)
	{
		if (company != null)
		{
			initializeProxy(company);
			initializeProxy(company.getDescription());
		}
	}

	private void loadCostCodeDetailAssociations(CostCodeDetail costCodeDetail)
	{
		if (costCodeDetail != null)
		{
			initializeProxy(costCodeDetail);
			initializeProxy(costCodeDetail.getDescription());

			if (costCodeDetail.getCompany() != null)
			{
				initializeProxy(costCodeDetail.getCompany());
				initializeProxy(costCodeDetail.getCompany().getDescription());
			}
		}
	}

	private void loadCostCodeAssociations(CostCode costCode)
	{
		if (costCode != null)
		{
			initializeProxy(costCode);
			initializeProxy(costCode.getDescription());

			if (costCode.getCompany() != null)
			{
				initializeProxy(costCode.getCompany());
				initializeProxy(costCode.getCompany().getDescription());
			}
		}
	}

	private void loadFeeCodeAnalyticAssociations(FeeCodeAnalytic feeCodeAnalytic)
	{
		if (feeCodeAnalytic != null)
		{
			initializeProxy(feeCodeAnalytic);
			initializeProxy(feeCodeAnalytic.getDescription());

			if (feeCodeAnalytic.getCompany() != null)
			{
				initializeProxy(feeCodeAnalytic.getCompany());
				initializeProxy(feeCodeAnalytic.getCompany().getDescription());
			}
		}
	}
}
