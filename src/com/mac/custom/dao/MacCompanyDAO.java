package com.mac.custom.dao;

import com.mac.custom.bo.MacCompany;
import com.netappsid.bo.model.Model;
import com.netappsid.erp.server.bo.AvailableMeasureUnit;
import com.netappsid.erp.server.bo.InventoryAdjustmentType;
import com.netappsid.erp.server.bo.Location;
import com.netappsid.erp.server.bo.WorkingStation;
import com.netappsid.erp.server.dao.CompanyDAO;
import com.netappsid.erp.server.dao.utils.GroupMeasureDAOUtils;

public class MacCompanyDAO<T extends MacCompany> extends CompanyDAO<T>
{
	@Override
	public void loadAssociations(Object object)
	{
		super.loadAssociations(object);
		object = Model.getTarget(object);

		if (object instanceof MacCompany)
		{
			loadMacCompanyAssociations((MacCompany) object);
		}
		else if (object instanceof WorkingStation)
		{
			loadWorkingStationAssociations((WorkingStation) object);
		}
	}

	private void loadMacCompanyAssociations(MacCompany macCompany)
	{
		if (macCompany != null)
		{
			initializeProxy(macCompany);
			loadAvailableMeasureUnitAssociations(macCompany.getWorkUnitAvailableMeasureUnit());
			loadWorkingStationAssociations(macCompany.getDefaultMaterialProductWorkingStation());
			loadInventoryAdjustmentTypeAssociations(macCompany.getDefaultInventoryAdjustmentType());
			loadLocationAssociations(macCompany.getDefaultInventoryAdjustmentLocation());
		}
	}

	private void loadWorkingStationAssociations(WorkingStation defaultMaterialProductWorkingStation)
	{
		if (defaultMaterialProductWorkingStation != null)
		{
			initializeProxy(defaultMaterialProductWorkingStation);
			initializeProxy(defaultMaterialProductWorkingStation.getDescription());
		}
	}

	private void loadAvailableMeasureUnitAssociations(AvailableMeasureUnit availableMeasureUnit)
	{
		if (availableMeasureUnit != null)
		{
			initializeProxy(availableMeasureUnit);
			GroupMeasureDAOUtils.loadGroupMeasureAssociations(availableMeasureUnit.getGroupMeasure());
		}
	}

	private void loadInventoryAdjustmentTypeAssociations(InventoryAdjustmentType defaultInventoryAdjustmentType)
	{
		if (defaultInventoryAdjustmentType != null)
		{
			initializeProxy(defaultInventoryAdjustmentType);
			initializeProxy(defaultInventoryAdjustmentType.getDescription());
		}
	}

	private void loadLocationAssociations(Location defaultInventoryAdjustmentLocation)
	{
		if (defaultInventoryAdjustmentLocation != null)
		{
			initializeProxy(defaultInventoryAdjustmentLocation);
			initializeProxy(defaultInventoryAdjustmentLocation.getDescription());
		}
	}
}
