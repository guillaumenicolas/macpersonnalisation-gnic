package com.mac.custom.dao;

import com.mac.custom.bo.MacWorkflowARC;
import com.mac.custom.bo.MacWorkflowARCProductDelay;
import com.netappsid.dao.GenericDAO;
import com.netappsid.erp.server.bo.ManufacturedProduct;
import com.netappsid.erp.server.bo.Property;
import com.netappsid.erp.server.bo.PropertyValue;
import com.netappsid.erp.server.dao.utils.PropertyAndAttributeDaoUtils;

public class MacWorkflowARCProductDelayDAO<T extends MacWorkflowARCProductDelay> extends GenericDAO<T>
{
	@Override
	public void loadAssociations(Object object)
	{
		logLoadAssociationsCall(object);

		if (object instanceof MacWorkflowARCProductDelay)
		{
			loadMacWfARCPDAssociations((MacWorkflowARCProductDelay) object);
		}
		else if (object instanceof Property)
		{
			PropertyAndAttributeDaoUtils.initializedPropertyAssociations((Property) object);
		}
		else if (object instanceof ManufacturedProduct)
		{
			loadManufacturedProduct((ManufacturedProduct) object);
		}
		else if (object instanceof PropertyValue)
		{
			PropertyAndAttributeDaoUtils.initializePropertyValueAssociations((PropertyValue<?>) object);
		}
	}

	private void loadManufacturedProduct(ManufacturedProduct product)
	{
		if (product != null)
		{
			initializeProxy(product);
			initializeProxy(product.getDescription());
		}
	}

	private void loadMacWfARCPDAssociations(MacWorkflowARCProductDelay macWorkflowArcProductDelay)
	{
		if (macWorkflowArcProductDelay != null)
		{
			initializeProxy(macWorkflowArcProductDelay);
			initializeMacWorkflowARC(macWorkflowArcProductDelay.getMacWorkflowARC());
		}
	}

	private void initializeMacWorkflowARC(MacWorkflowARC macWorkflowARC)
	{
		if (macWorkflowARC != null)
		{
			initializeProxy(macWorkflowARC);
		}
	}
}
