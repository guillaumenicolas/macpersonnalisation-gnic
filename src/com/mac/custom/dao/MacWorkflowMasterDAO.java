package com.mac.custom.dao;

import com.mac.custom.bo.MacWorkflowARC;
import com.mac.custom.bo.MacWorkflowARCProductDelay;
import com.mac.custom.bo.MacWorkflowBlockAuto;
import com.mac.custom.bo.MacWorkflowClientValidation;
import com.mac.custom.bo.MacWorkflowFinanceValidation;
import com.mac.custom.bo.MacWorkflowM5P;
import com.mac.custom.bo.MacWorkflowMaster;
import com.mac.custom.bo.MacWorkflowUnblockAuto;
import com.mac.custom.bo.MacWorkflowUnblockManual;
import com.netappsid.dao.GenericDAO;
import com.netappsid.erp.server.bo.AbstractShift;
import com.netappsid.erp.server.bo.BlockingReason;
import com.netappsid.erp.server.bo.Day;
import com.netappsid.erp.server.bo.DocumentStatus;
import com.netappsid.erp.server.bo.IntervalShift;
import com.netappsid.erp.server.bo.ManufacturedProduct;
import com.netappsid.erp.server.bo.Shift;
import com.netappsid.erp.server.bo.WorkShift;
import com.netappsid.erp.server.bo.WorkShiftDetail;
import com.netappsid.erp.server.bo.WorkShiftException;
import com.netappsid.erp.server.dao.utils.GroupMeasureDAOUtils;
import com.netappsid.erp.server.dao.utils.PropertyAndAttributeDaoUtils;

public class MacWorkflowMasterDAO<T extends MacWorkflowMaster> extends GenericDAO<T>
{
	@Override
	public void loadAssociations(Object object)
	{
		logLoadAssociationsCall(object);

		if (object instanceof MacWorkflowMaster)
		{
			loadMacWfMasterAssociations((MacWorkflowMaster) object);
		}

		if (object instanceof DocumentStatus)
		{
			loadDocumentStatusAssociations((DocumentStatus) object);
		}

		if (object instanceof WorkShift)
		{
			loadWorkShift((WorkShift) object);
		}

		if (object instanceof BlockingReason)
		{
			loadBlockingReasonAssociations((BlockingReason) object);
		}
	}

	private void loadMacWfMasterAssociations(MacWorkflowMaster macWFMaster)
	{
		if (macWFMaster != null)
		{
			if (macWFMaster.getSysAdmin() != null)
			{
				initializeProxy(macWFMaster.getSysAdmin());
				initializeProxy(macWFMaster.getSysAdmin().getSecurityUser());
			}

			if (macWFMaster.getUserWF() != null)
			{
				initializeProxy(macWFMaster.getUserWF());
				initializeProxy(macWFMaster.getUserWF().getSecurityUser());
			}
		}

		loadMacWfARCAssociations(macWFMaster.getWfARC());

		if (macWFMaster.getWfM5P() != null)
		{
			loadMacWfM5PAssociations(macWFMaster.getWfM5P());
		}

		if (macWFMaster.getWfUnblockAuto() != null)
		{
			loadMacWfUnblockAutoAssociations(macWFMaster.getWfUnblockAuto());
		}

		if (macWFMaster.getWfUnblockManual() != null)
		{
			loadMacWfUnblockManualAssociations(macWFMaster.getWfUnblockManual());
		}

		if (macWFMaster.getWfBlockAuto() != null)
		{
			loadMacWfBlockAutoAssociations(macWFMaster.getWfBlockAuto());
		}

		if (macWFMaster.getWfFinanceValidation() != null)
		{
			loadMacWfFinanceValidationAssociations(macWFMaster.getWfFinanceValidation());
		}

		if (macWFMaster.getWfClientValidation() != null)
		{
			loadMacWfClientValidationAssociations(macWFMaster.getWfClientValidation());
		}
	}

	private void loadMacWfFinanceValidationAssociations(MacWorkflowFinanceValidation macWorkflowFinanceValidation)
	{
		if (macWorkflowFinanceValidation.getEmailCompta() != null)
		{
			initializeProxy(macWorkflowFinanceValidation.getEmailCompta());
		}

		if (macWorkflowFinanceValidation.getEmailOrigin() != null)
		{
			initializeProxy(macWorkflowFinanceValidation.getEmailOrigin());
		}
	}

	private void loadMacWfClientValidationAssociations(MacWorkflowClientValidation macWorkflowClientValidation)
	{
		if (macWorkflowClientValidation.getReport() != null)
		{
			initializeProxy(macWorkflowClientValidation.getReport());
			initializeProxy(macWorkflowClientValidation.getReport().getLocalizedDescription());
		}

		if (macWorkflowClientValidation.getEmailOrigin() != null)
		{
			initializeProxy(macWorkflowClientValidation.getEmailOrigin());
		}
	}

	private void loadMacWfBlockAutoAssociations(MacWorkflowBlockAuto WfBlockAuto)
	{
		if (WfBlockAuto.getBlockingReason() != null)
		{
			loadBlockingReasonAssociations(WfBlockAuto.getBlockingReason());
		}

		if (WfBlockAuto.getBlockingReasonProforma() != null)
		{
			loadBlockingReasonAssociations(WfBlockAuto.getBlockingReasonProforma());
		}

		if (WfBlockAuto.getBlockedStatus() != null)
		{
			loadDocumentStatusAssociations(WfBlockAuto.getBlockedStatus());
		}

		if (WfBlockAuto.getInitialStatus() != null)
		{
			loadDocumentStatusAssociations(WfBlockAuto.getInitialStatus());
		}

		if (WfBlockAuto.getEmailOrigin() != null)
		{
			initializeProxy(WfBlockAuto.getEmailOrigin());
		}
	}

	private void loadMacWfUnblockManualAssociations(MacWorkflowUnblockManual WfUnblockManual)
	{
		if (WfUnblockManual.getUnblockedStatus() != null)
		{
			loadDocumentStatusAssociations(WfUnblockManual.getUnblockedStatus());
		}
	}

	private void loadMacWfUnblockAutoAssociations(MacWorkflowUnblockAuto WfUnblockAuto)
	{
		if (WfUnblockAuto.getUnblockingReason() != null)
		{
			loadBlockingReasonAssociations(WfUnblockAuto.getUnblockingReason());
		}

		if (WfUnblockAuto.getUnblockedStatus() != null)
		{
			loadDocumentStatusAssociations(WfUnblockAuto.getUnblockedStatus());
		}
	}

	private void loadMacWfM5PAssociations(MacWorkflowM5P wfM5P)
	{
		if (wfM5P.getM5pStatus() != null)
		{
			loadDocumentStatusAssociations(wfM5P.getM5pStatus());
		}

		if (wfM5P.getBlockedStatus() != null)
		{
			loadDocumentStatusAssociations(wfM5P.getBlockedStatus());
		}

		if (wfM5P.getUnblockedStatus() != null)
		{
			loadDocumentStatusAssociations(wfM5P.getUnblockedStatus());
		}

		if (wfM5P.getRefusedStatus() != null)
		{
			loadDocumentStatusAssociations(wfM5P.getRefusedStatus());
		}

		if (wfM5P.getBlockingReason() != null)
		{
			loadBlockingReasonAssociations(wfM5P.getBlockingReason());
		}

		if (wfM5P.getUnblockingReason() != null)
		{
			loadBlockingReasonAssociations(wfM5P.getUnblockingReason());
		}
	}

	private void loadBlockingReasonAssociations(BlockingReason blockingReason)
	{
		initializeProxy(blockingReason);
		initializeProxy(blockingReason.getDescription());
	}

	private void loadMacWfARCAssociations(MacWorkflowARC wfARC)
	{
		if (wfARC != null)
		{
			initializeProxy(wfARC);
			loadDocumentStatusAssociations(wfARC.getArcSentStatus());
			loadDocumentStatusAssociations(wfARC.getInitialDelayStatus());
			for (MacWorkflowARCProductDelay dp : wfARC.getApprovalProductDelays())
			{
				loadMacWorkflowARCProductDelayAssociations(dp);
			}
			loadWorkShift(wfARC.getWorkShift());
		}
	}

	private void loadWorkShift(WorkShift workShift)
	{
		if (workShift != null)
		{
			initializeProxy(workShift);
			initializeProxy(workShift.getDescription());

			for (WorkShiftDetail workShiftDetail : workShift.getDetails())
			{
				loadWorkShiftDetailAssociations(workShiftDetail);
			}

			for (WorkShiftException workShiftException : workShift.getExceptions())
			{
				loadWorkShiftExceptionAssociations(workShiftException);
			}
		}
	}

	private void loadWorkShiftDetailAssociations(WorkShiftDetail workShiftDetail)
	{
		if (workShiftDetail != null)
		{
			initializeProxy(workShiftDetail);

			for (Day day : workShiftDetail.getDays())
			{
				loadDayAssociations(day);
			}
		}
	}

	private void loadDayAssociations(Day day)
	{
		if (day != null)
		{
			initializeProxy(day);
			for (AbstractShift shift : day.getShifts())
			{
				loadAbstractShiftAssociations(shift);
			}
		}
	}

	private void loadWorkShiftExceptionAssociations(WorkShiftException workShiftException)
	{
		if (workShiftException != null)
		{
			initializeProxy(workShiftException);

			for (AbstractShift shift : workShiftException.getShifts())
			{
				loadAbstractShiftAssociations(shift);
			}
		}
	}

	private void loadAbstractShiftAssociations(AbstractShift abstractShift)
	{
		if (abstractShift != null)
		{
			if (abstractShift instanceof Shift)
			{
				loadShiftAssociations((Shift) abstractShift);
			}
			else if (abstractShift instanceof IntervalShift)
			{
				loadIntervalShiftAssociations((IntervalShift) abstractShift);
			}
		}
	}

	private void loadIntervalShiftAssociations(IntervalShift intervalShift)
	{
		if (intervalShift != null)
		{
			initializeProxy(intervalShift);
		}
	}

	private void loadShiftAssociations(Shift shift)
	{
		if (shift != null)
		{
			initializeProxy(shift);
			GroupMeasureDAOUtils.loadGroupMeasureAssociations(shift.getTimeMeasure().getGroupMeasure());
			GroupMeasureDAOUtils.loadMeasureUnitMultiplierAssociations(shift.getTimeMeasureConvertedUnit());
		}
	}

	private void loadMacWorkflowARCProductDelayAssociations(MacWorkflowARCProductDelay macWorkflowARCProductDelay)
	{
		if (macWorkflowARCProductDelay != null)
		{
			initializeProxy(macWorkflowARCProductDelay);
			loadManufacturedProduct(macWorkflowARCProductDelay.getProduct());
			PropertyAndAttributeDaoUtils.initializedPropertyAssociations(macWorkflowARCProductDelay.getProperty());
			PropertyAndAttributeDaoUtils.initializePropertyValueAssociations(macWorkflowARCProductDelay.getPropertyValue());
		}
	}

	private void loadManufacturedProduct(ManufacturedProduct product)
	{
		if (product != null)
		{
			initializeProxy(product);
			initializeProxy(product.getDescription());
		}
	}

	private void loadDocumentStatusAssociations(DocumentStatus documentStatus)
	{
		if (documentStatus != null)
		{
			initializeProxy(documentStatus);
			initializeProxy(documentStatus.getDescription());
		}
	}
}
