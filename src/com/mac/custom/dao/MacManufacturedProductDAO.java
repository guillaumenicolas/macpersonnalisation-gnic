package com.mac.custom.dao;

import com.mac.custom.bo.MacManufacturedProduct;
import com.mac.custom.bo.ManufacturedProductManagementAccounting;
import com.netappsid.erp.server.bo.InventoryProduct;
import com.netappsid.erp.server.bo.MeasureUnitConversion;
import com.netappsid.erp.server.bo.ProductLocation;
import com.netappsid.erp.server.bo.ProductLocationStock;
import com.netappsid.erp.server.bo.ProductLocationStockMissing;
import com.netappsid.erp.server.bo.ProductWarehouse;
import com.netappsid.erp.server.dao.utils.GroupMeasureDAOUtils;
import com.netappsid.europe.naid.custom.dao.ManufacturedProductEuroDAO;

public class MacManufacturedProductDAO<T extends MacManufacturedProduct> extends ManufacturedProductEuroDAO<T>
{
	@Override
	public void loadAssociations(Object object)
	{
		super.loadAssociations(object);

		if (object instanceof MacManufacturedProduct)
		{
			loadMacManufacturedProductAssociations((MacManufacturedProduct) object);
		}
		else if (object instanceof ProductWarehouse)
		{
			loadProductWarehouseAssociations((ProductWarehouse) object);
		}
	}

	private void loadProductWarehouseAssociations(ProductWarehouse productWarehouse)
	{
		if (productWarehouse != null)
		{
			InventoryProduct inventoryProduct = productWarehouse.getInventoryProduct();
			GroupMeasureDAOUtils.loadGroupMeasureAssociations(inventoryProduct.getInventoryMeasureGroupMeasure());

			if (inventoryProduct.getDefaultConvertedUnit() != null)
			{
				GroupMeasureDAOUtils.loadGroupMeasureAssociations(inventoryProduct.getDefaultConvertedUnit().getGroupMeasure());
			}

			GroupMeasureDAOUtils.loadMeasureUnitConversionAssociations(inventoryProduct.getDefaultUnitConversion());

			for (MeasureUnitConversion measureUnitConversion : inventoryProduct.getUnitConversions())
			{
				GroupMeasureDAOUtils.loadMeasureUnitConversionAssociations(measureUnitConversion);
			}

			initializeProxy(productWarehouse);

			for (ProductLocation productLocation : productWarehouse.getProductLocations())
			{
				loadProductLocationStockAssociations(productLocation.getProductLocationStock());
				loadProductLocationStockMissingAssociations(productLocation.getProductLocationStockMissing());
			}
		}
	}

	private void loadProductLocationStockAssociations(ProductLocationStock productLocationStock)
	{
		if (productLocationStock != null)
		{
			initializeProxy(productLocationStock);
			GroupMeasureDAOUtils.loadGroupMeasureAssociations(productLocationStock.getQuantityGroupMeasure());
		}
	}

	private void loadProductLocationStockMissingAssociations(ProductLocationStockMissing productLocationStockMissing)
	{
		if (productLocationStockMissing != null)
		{
			initializeProxy(productLocationStockMissing);
			GroupMeasureDAOUtils.loadGroupMeasureAssociations(productLocationStockMissing.getQuantityGroupMeasure());
		}
	}

	private void loadMacManufacturedProductAssociations(MacManufacturedProduct macManufacturedProduct)
	{
		if (macManufacturedProduct != null)
		{
			initializeProxy(macManufacturedProduct);

			for (ManufacturedProductManagementAccounting managementAccounting : macManufacturedProduct.getManagementAccountings())
			{
				loadManagementAccountingAssociation(managementAccounting);
			}
		}
	}

	private void loadManagementAccountingAssociation(ManufacturedProductManagementAccounting managementAccounting)
	{
		if (managementAccounting != null)
		{
			initializeProxy(managementAccounting);

			if (managementAccounting.getCompany() != null)
			{
				initializeProxy(managementAccounting.getCompany());
				initializeProxy(managementAccounting.getCompany().getDescription());
			}

			if (managementAccounting.getCostCodeSale() != null)
			{
				initializeProxy(managementAccounting.getCostCodeSale());
				initializeProxy(managementAccounting.getCostCodeSale().getDescription());
			}

			if (managementAccounting.getCostCodeDetailSale() != null)
			{
				initializeProxy(managementAccounting.getCostCodeDetailSale());
				initializeProxy(managementAccounting.getCostCodeDetailSale().getDescription());
			}

			if (managementAccounting.getProductCodeAnalyticSale() != null)
			{
				initializeProxy(managementAccounting.getProductCodeAnalyticSale());
				initializeProxy(managementAccounting.getProductCodeAnalyticSale().getDescription());
			}

			if (managementAccounting.getCostCodePurchase() != null)
			{
				initializeProxy(managementAccounting.getCostCodePurchase());
				initializeProxy(managementAccounting.getCostCodePurchase().getDescription());
			}

			if (managementAccounting.getCostCodeDetailPurchase() != null)
			{
				initializeProxy(managementAccounting.getCostCodeDetailPurchase());
				initializeProxy(managementAccounting.getCostCodeDetailPurchase().getDescription());
			}

			if (managementAccounting.getProductCodeAnalyticPurchase() != null)
			{
				initializeProxy(managementAccounting.getProductCodeAnalyticPurchase());
				initializeProxy(managementAccounting.getProductCodeAnalyticPurchase().getDescription());
			}
		}
	}
}
