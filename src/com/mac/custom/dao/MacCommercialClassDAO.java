package com.mac.custom.dao;

import com.mac.custom.bo.ActiviteClient;
import com.mac.custom.bo.EnseigneClient;
import com.mac.custom.bo.MacCommercialClass;
import com.netappsid.erp.server.dao.CommercialClassDAO;

public class MacCommercialClassDAO<T extends MacCommercialClass> extends CommercialClassDAO<T>
{
	@Override
	public void loadAssociations(Object object)
	{
		super.loadAssociations(object);

		if (object instanceof MacCommercialClass)
		{
			loadMacCommercialClassAssociations((MacCommercialClass) object);
		}
		else if (object instanceof ActiviteClient)
		{
			loadActiviteClientAssociations((ActiviteClient) object);
		}
		else if (object instanceof EnseigneClient)
		{
			loadEnseigneClientAssociations((EnseigneClient) object);
		}
	}

	private void loadMacCommercialClassAssociations(MacCommercialClass macCommercialClass)
	{
		if (macCommercialClass != null)
		{
			initializeProxy(macCommercialClass);
			initializeProxy(macCommercialClass.getDescription());

			for (ActiviteClient activiteClient : macCommercialClass.getActivitesClient())
			{
				loadActiviteClientAssociations(activiteClient);
			}

			for (EnseigneClient enseigneClient : macCommercialClass.getEnseignesClient())
			{
				loadEnseigneClientAssociations(enseigneClient);
			}
		}
	}

	private void loadEnseigneClientAssociations(EnseigneClient enseigneClient)
	{
		if (enseigneClient != null)
		{
			initializeProxy(enseigneClient);
			initializeProxy(enseigneClient.getDescription());
		}
	}

	private void loadActiviteClientAssociations(ActiviteClient activiteClient)
	{
		if (activiteClient != null)
		{
			initializeProxy(activiteClient);
			initializeProxy(activiteClient.getDescription());
		}
	}
}
