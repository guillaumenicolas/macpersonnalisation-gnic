package com.mac.custom.dao;

import com.mac.custom.bo.MacQuotation;
import com.netappsid.erp.server.bo.Company;
import com.netappsid.erp.server.bo.Configuration;
import com.netappsid.erp.server.bo.ConfigurationRevision;
import com.netappsid.erp.server.bo.FabricationProduct;
import com.netappsid.erp.server.bo.InventoryProduct;
import com.netappsid.erp.server.bo.Location;
import com.netappsid.erp.server.bo.ManufacturedProduct;
import com.netappsid.erp.server.bo.Product;
import com.netappsid.erp.server.bo.ProductCategory;
import com.netappsid.erp.server.bo.ProductCostType;
import com.netappsid.erp.server.bo.ProductFamily;
import com.netappsid.erp.server.bo.ProductLocation;
import com.netappsid.erp.server.bo.ProductNoteByDocument;
import com.netappsid.erp.server.bo.ProductQuickLibrary;
import com.netappsid.erp.server.bo.ProductRevision;
import com.netappsid.erp.server.bo.ProductSupplier;
import com.netappsid.erp.server.bo.ProductSupplierCompanyDiscount;
import com.netappsid.erp.server.bo.ProductSupplierPrice;
import com.netappsid.erp.server.bo.ProductSupplierPriceRange;
import com.netappsid.erp.server.bo.ProductWarehouse;
import com.netappsid.erp.server.bo.QuotationDetail;
import com.netappsid.erp.server.bo.Tax;
import com.netappsid.erp.server.bo.Warehouse;
import com.netappsid.erp.server.dao.ERPDAO;
import com.netappsid.erp.server.dao.ProductQuickLibraryDAO;

public class MacQuotationDetailSearchDAO<T extends QuotationDetail> extends ERPDAO<T>
{

	@Override
	public void loadAssociations(Object object)
	{
		logLoadAssociationsCall(object);

		if (object instanceof Product)
		{
			loadProductAssociations((Product) object);
		}
		else if (object instanceof MacQuotation)
		{
			MacQuotationDAO<MacQuotation> macQuotationDAO = new MacQuotationDAO<MacQuotation>();
			macQuotationDAO.loadAssociations(object);
		}
		else if (object instanceof ProductQuickLibrary)
		{
			ProductQuickLibraryDAO<ProductQuickLibrary> productQuickLibraryDAO = new ProductQuickLibraryDAO<ProductQuickLibrary>();
			productQuickLibraryDAO.loadAssociations(object);
		}
	}

	private void loadProductAssociations(Product product)
	{
		if (product != null)
		{
			initializeProxy(product);
			initializeProxy(product.getDescription());

			loadProductCategoryAssociations(product.getProductCategory());
			loadProductFamilyAssociations(product.getProductFamily());

			for (ProductSupplier productSupplier : product.getSuppliers())
			{
				for (ProductSupplierPrice productSupplierPrice : productSupplier.getPrices())
				{
					loadProductSupplierPriceAssociations(productSupplierPrice);
				}

				for (ProductSupplierCompanyDiscount companyDiscount : productSupplier.getCompanyDiscounts())
				{
					locaCompanyAssociations(companyDiscount.getCompany());
				}
			}

			ProductSupplier productSupplier = product.getDefaultSupplier();

			if (productSupplier != null)
			{
				for (ProductSupplierPrice productSupplierPrice : productSupplier.getPrices())
				{
					loadProductSupplierPriceAssociations(productSupplierPrice);
				}

				for (ProductSupplierCompanyDiscount companyDiscount : productSupplier.getCompanyDiscounts())
				{
					locaCompanyAssociations(companyDiscount.getCompany());
				}
			}

			for (ProductNoteByDocument productNote : product.getDocumentNotes())
			{
				if (productNote != null)
				{
					initializeProxy(productNote);
					initializeProxy(productNote.getDescription());
					initializeProxy(productNote.getNote());
				}
			}

			for (Tax tax : product.getTaxes())
			{
				initializeProxy(tax);
				initializeProxy(tax.getDescription());
			}

			if (product instanceof FabricationProduct)
			{
				loadFabricationProductAssociations((FabricationProduct) product);
			}
		}
	}

	private void loadProductFamilyAssociations(ProductFamily productFamily)
	{
		if (productFamily != null)
		{
			initializeProxy(productFamily);
			initializeProxy(productFamily.getDescription());
		}
	}

	private void locaCompanyAssociations(Company company)
	{
		if (company != null)
		{
			initializeProxy(company);
			initializeProxy(company.getDescription());
		}
	}

	private void loadProductCategoryAssociations(ProductCategory category)
	{
		if (category != null)
		{
			initializeProxy(category);
			initializeProxy(category.getDescription());
		}
	}

	private void loadProductSupplierPriceAssociations(ProductSupplierPrice productSupplierPrice)
	{
		if (productSupplierPrice != null)
		{
			initializeProxy(productSupplierPrice);

			for (ProductSupplierPriceRange productSupplierPriceRange : productSupplierPrice.getPriceRange())
			{
				initializeProxy(productSupplierPriceRange);
			}
		}
	}

	private void loadFabricationProductAssociations(FabricationProduct fabricationProduct)
	{
		if (fabricationProduct != null)
		{
			for (ProductRevision productRevision : fabricationProduct.getRevisions())
			{
				loadProductRevisionAssociations(productRevision);
			}

			if (fabricationProduct instanceof InventoryProduct)
			{
				loadInventoryProductAssociations((InventoryProduct) fabricationProduct);
			}
		}
	}

	private void loadInventoryProductAssociations(InventoryProduct inventoryProduct)
	{
		ProductCostType costType = inventoryProduct.getProductCostType();
		if (costType != null)
		{
			initializeProxy(costType);
			initializeProxy(costType.getDescription());
		}

		for (ProductWarehouse productWarehouse : inventoryProduct.getProductWarehouses())
		{
			loadWarehouseAssociations(productWarehouse.getWarehouse());
			for (ProductLocation productLocation : productWarehouse.getProductLocations())
			{
				loadLocationAssociations(productLocation.getLocation());
			}
		}

		if (inventoryProduct instanceof ManufacturedProduct)
		{
			loadManufacturedProductAssociations((ManufacturedProduct) inventoryProduct);
		}

	}

	private void loadLocationAssociations(Location location)
	{
		if (location != null)
		{
			initializeProxy(location);
			initializeProxy(location.getDescription());
		}
	}

	private void loadWarehouseAssociations(Warehouse warehouse)
	{
		if (warehouse != null)
		{
			initializeProxy(warehouse);
			initializeProxy(warehouse.getDescription());
		}
	}

	private void loadManufacturedProductAssociations(ManufacturedProduct manufacturedProduct)
	{
		Configuration configuration = manufacturedProduct.getConfiguration();
		if (configuration != null)
		{
			initializeProxy(configuration);
			initializeProxy(configuration.getDescription());
		}
	}

	private void loadProductRevisionAssociations(ProductRevision productRevision)
	{
		if (productRevision != null)
		{
			initializeProxy(productRevision);
			initializeProxy(productRevision.getDescription());

			ProductRevision replacedBy = productRevision.getReplacedBy();
			if (replacedBy != null)
			{
				initializeProxy(replacedBy);
				initializeProxy(replacedBy.getDescription());
			}

			for (ConfigurationRevision configurationRevision : productRevision.getRevisions())
			{
				Configuration configuration = configurationRevision.getConfiguration();

				if (configuration != null)
				{
					initializeProxy(configuration);
					initializeProxy(configuration.getDescription());
				}
			}

			FabricationProduct fabricationProduct = productRevision.getFabricationProduct();
			if (fabricationProduct != null)
			{
				initializeProxy(fabricationProduct);
				initializeProxy(fabricationProduct.getDescription());
				initializeProxy(fabricationProduct.getRevisions());
			}
		}
	}
}
