package com.mac.custom.dao;

import com.netappsid.bo.model.Model;
import com.netappsid.erp.server.bo.CommercialEntityDefaultReport;
import com.netappsid.erp.server.bo.CommercialEntityReportContact;
import com.netappsid.erp.server.bo.Communication;
import com.netappsid.erp.server.bo.Customer;
import com.netappsid.erp.server.bo.Report;
import com.netappsid.erp.server.bo.SaleOrder;
import com.netappsid.erp.server.dao.ERPDAO;

public class WorkflowARCUtilsDAO<T extends Object> extends ERPDAO<T>
{
	@Override
	public void loadAssociations(Object object)
	{
		logLoadAssociationsCall(object);

		object = Model.getTarget(object);

		if (object instanceof SaleOrder)
		{
			loadSaleOrderAssociations((SaleOrder) object);
		}
	}

	private void loadSaleOrderAssociations(SaleOrder saleOrder)
	{
		if (saleOrder != null)
		{
			initializeProxy(saleOrder);
			loadCustomerAssociations(saleOrder.getCustomer());
		}
	}

	private void loadCustomerAssociations(Customer customer)
	{
		if (customer != null)
		{
			initializeProxy(customer);
			for (CommercialEntityDefaultReport commercialEntityDefaultReport : customer.getDefaultReports())
			{
				loadCommercialEntityDefaultReportAssociations(commercialEntityDefaultReport);
			}
		}
	}

	private void loadCommercialEntityDefaultReportAssociations(CommercialEntityDefaultReport commercialEntityDefaultReport)
	{
		if (commercialEntityDefaultReport != null)
		{
			initializeProxy(commercialEntityDefaultReport);
			loadReportAssociations(commercialEntityDefaultReport.getReport());

			for (CommercialEntityReportContact commercialEntityReportContact : commercialEntityDefaultReport.getContacts())
			{
				loadCommercialEntityReportContactAssociations(commercialEntityReportContact);
			}
		}
	}

	private void loadCommercialEntityReportContactAssociations(CommercialEntityReportContact commercialEntityReportContact)
	{
		if (commercialEntityReportContact != null)
		{
			initializeProxy(commercialEntityReportContact);
			loadCommunicationAssociations(commercialEntityReportContact.getCommunication());
		}
	}

	private void loadCommunicationAssociations(Communication communication)
	{
		if (communication != null)
		{
			initializeProxy(communication);
		}
	}

	private void loadReportAssociations(Report report)
	{
		if (report != null)
		{
			initializeProxy(report);
		}
	}
}
