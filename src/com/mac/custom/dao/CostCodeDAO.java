package com.mac.custom.dao;

// Imports
import com.mac.custom.bo.CostCode;
import com.mac.custom.bo.base.BaseCostCode;
import com.netappsid.erp.server.dao.ERPDAO;

public class CostCodeDAO<T extends CostCode> extends ERPDAO<T>
{
	@Override
	public void loadAssociations(Object object)
	{
		logLoadAssociationsCall(object);

		if (object instanceof CostCode)
		{
			loadCostCodeAssociations((BaseCostCode) object);
		}
	}

	private void loadCostCodeAssociations(BaseCostCode costCode)
	{
		if (costCode != null)
		{
			initializeProxy(costCode);
			initializeProxy(costCode.getDescription());

			if (costCode.getCompany() != null)
			{
				initializeProxy(costCode.getCompany());
				initializeProxy(costCode.getCompany().getDescription());
			}
		}
	}

}
