package com.mac.custom.dao;

import com.mac.custom.bo.MacOrder;
import com.netappsid.dao.GenericDAO;
import com.netappsid.erp.server.bo.Address;
import com.netappsid.erp.server.bo.AddressRoute;
import com.netappsid.erp.server.bo.Route;

public class MacOrderMoveWorkLoadDetailDAO<T extends MacOrder> extends GenericDAO<T>
{
	@Override
	public void loadAssociations(Object object)
	{
		logLoadAssociationsCall(object);

		if (object instanceof MacOrder)
		{
			loadMacOrderAssociations((MacOrder) object);
		}
	}

	private void loadMacOrderAssociations(MacOrder macOrder)
	{
		if (macOrder != null)
		{
			initializeProxy(macOrder);

			loadRouteAssociations(macOrder.getRoute());

			Address macShippingAddress = macOrder.getShippingAddress();
			if (macShippingAddress != null)
			{
				loadAddressAssociations(macShippingAddress);
			}
		}
	}

	protected void loadRouteAssociations(Route route)
	{
		if (route != null)
		{
			initializeProxy(route);
		}
	}

	protected void loadAddressAssociations(Address address)
	{
		if (address != null)
		{
			initializeProxy(address);

			for (AddressRoute addressRoute : address.getAddressRoutes())
			{
				initializeProxy(addressRoute);
				loadRouteAssociations(addressRoute.getRoute());
			}
		}
	}
}
