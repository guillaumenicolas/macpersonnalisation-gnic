package com.mac.custom.dao;

// Imports
import com.mac.custom.bo.CostCodeDetail;
import com.mac.custom.bo.base.BaseCostCodeDetail;
import com.netappsid.erp.server.dao.ERPDAO;

public class CostCodeDetailDAO<T extends CostCodeDetail> extends ERPDAO<T>
{
	@Override
	public void loadAssociations(Object object)
	{
		logLoadAssociationsCall(object);

		if (object instanceof CostCodeDetail)
		{
			loadCostCodeDetailAssociations((BaseCostCodeDetail) object);
		}
	}

	private void loadCostCodeDetailAssociations(BaseCostCodeDetail costCodeDetail)
	{
		if (costCodeDetail != null)
		{
			initializeProxy(costCodeDetail);
			initializeProxy(costCodeDetail.getDescription());

			if (costCodeDetail.getCompany() != null)
			{
				initializeProxy(costCodeDetail.getCompany());
				initializeProxy(costCodeDetail.getCompany().getDescription());
			}
		}
	}
}
