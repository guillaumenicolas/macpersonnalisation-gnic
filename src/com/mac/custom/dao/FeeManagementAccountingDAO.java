package com.mac.custom.dao;

import com.mac.custom.bo.CostCode;
import com.mac.custom.bo.CostCodeDetail;
import com.mac.custom.bo.FeeCodeAnalytic;
import com.mac.custom.bo.FeeManagementAccounting;
import com.netappsid.erp.server.bo.Company;
import com.netappsid.erp.server.dao.ERPDAO;

public class FeeManagementAccountingDAO<T extends FeeManagementAccounting> extends ERPDAO<T>
{
	@Override
	public void loadAssociations(Object object)
	{
		logLoadAssociationsCall(object);

		if (object instanceof Company)
		{
			loadCompanyAssociations((Company) object);
		}
		else if (object instanceof CostCodeDetail)
		{
			loadCostCodeDetailAssociations((CostCodeDetail) object);
		}
		else if (object instanceof FeeCodeAnalytic)
		{
			loadFeeCodeAnalyticAssociations((FeeCodeAnalytic) object);
		}
		else if (object instanceof CostCode)
		{
			loadCostCodeAssociations((CostCode) object);
		}
	}

	private void loadCompanyAssociations(Company company)
	{
		if (company != null)
		{
			initializeProxy(company);
			initializeProxy(company.getDescription());
		}
	}

	private void loadCostCodeDetailAssociations(CostCodeDetail costDetail)
	{
		if (costDetail != null)
		{
			initializeProxy(costDetail);
			initializeProxy(costDetail.getDescription());

			if (costDetail.getCompany() != null)
			{
				initializeProxy(costDetail.getCompany());
				initializeProxy(costDetail.getCompany().getDescription());
			}
		}
	}

	private void loadCostCodeAssociations(CostCode costCode)
	{
		if (costCode != null)
		{
			initializeProxy(costCode);
			initializeProxy(costCode.getDescription());

			if (costCode.getCompany() != null)
			{
				initializeProxy(costCode.getCompany());
				initializeProxy(costCode.getCompany().getDescription());
			}
		}
	}

	private void loadFeeCodeAnalyticAssociations(FeeCodeAnalytic feeCodeAnalytic)
	{
		if (feeCodeAnalytic != null)
		{
			initializeProxy(feeCodeAnalytic);
			initializeProxy(feeCodeAnalytic.getDescription());

			if (feeCodeAnalytic.getCompany() != null)
			{
				initializeProxy(feeCodeAnalytic.getCompany());
				initializeProxy(feeCodeAnalytic.getCompany().getDescription());
			}
		}
	}
}