package com.mac.custom.dao;

import com.mac.custom.bo.MacWorkflowFinanceValidation;
import com.netappsid.dao.GenericDAO;

public class MacWorkflowFinanceValidationDAO<T extends MacWorkflowFinanceValidation> extends GenericDAO<T> 
{
	@Override
	public void loadAssociations(Object object)
	{
		logLoadAssociationsCall(object);
		
		if (object instanceof MacWorkflowFinanceValidation)
		{
			loadMacWfFinanceValidationAssociations((MacWorkflowFinanceValidation) object);
		}
	}

	private void loadMacWfFinanceValidationAssociations(MacWorkflowFinanceValidation macWorkflowFinanceValidation) 
	{
		initializeProxy(macWorkflowFinanceValidation);
	}
}
