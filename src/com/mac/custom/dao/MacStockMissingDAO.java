package com.mac.custom.dao;

import com.netappsid.bo.model.Model;
import com.netappsid.dao.DAOInitializeVisitor;
import com.netappsid.erp.server.bo.ProductWarehouse;
import com.netappsid.erp.server.bo.StockMissing;
import com.netappsid.erp.server.bo.Warehouse;
import com.netappsid.erp.server.dao.StockMissingDAO;

public class MacStockMissingDAO<T extends StockMissing> extends StockMissingDAO<T>
{
	private DAOInitializeVisitor daoInitializeVisitor;

	@Override
	public void loadAssociations(Object object)
	{
		super.loadAssociations(object);
		object = Model.getTarget(object);

		if (object instanceof StockMissing)
		{
			loadQuantiteStock((StockMissing) object);
		}
	}

	private void loadQuantiteStock(StockMissing stockMissing)
	{
		Warehouse warehouse = stockMissing.getLocation().getWarehouse();
		ProductWarehouse productWarehouse = stockMissing.getInventoryProduct().getProductWarehouse(stockMissing.getLocation().getWarehouse());
		initializeProxy(productWarehouse);
		initializeProxy(productWarehouse.getQuantityInStockToInventoryUnit());
		initializeProxy(productWarehouse.getQuantityInStockMissingToInventoryUnit());
	}
}
