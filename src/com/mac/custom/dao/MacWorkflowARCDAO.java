package com.mac.custom.dao;

import com.mac.custom.bo.MacWorkflowARC;
import com.mac.custom.bo.MacWorkflowARCProductDelay;
import com.netappsid.dao.GenericDAO;

public class MacWorkflowARCDAO<T extends MacWorkflowARC> extends GenericDAO<T>
{
	@Override
	public void loadAssociations(Object object)
	{
		logLoadAssociationsCall(object);
		
		if (object instanceof MacWorkflowARC)
		{
			loadMacWfARCAssociations((MacWorkflowARC) object);
		}
	}

	private void loadMacWfARCAssociations(MacWorkflowARC macWorkflowArc)
	{
		if (macWorkflowArc != null)
		{
			initializeProxy(macWorkflowArc);
			for (MacWorkflowARCProductDelay dp : macWorkflowArc.getApprovalProductDelays())
			{
				loadMacWorkflowARCProductDelayAssociations(dp);
			}
		}
	}

	private void loadMacWorkflowARCProductDelayAssociations(MacWorkflowARCProductDelay macWorkflowARCProductDelay)
	{
		if (macWorkflowARCProductDelay != null)
		{
			initializeProxy(macWorkflowARCProductDelay);
		}
	}
}
