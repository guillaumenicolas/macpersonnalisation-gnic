package com.mac.custom.dao;

import com.mac.custom.bo.MacManufacturedProduct;
import com.mac.custom.bo.MacMaterialProduct;
import com.netappsid.dao.DAOInitializeVisitor;
import com.netappsid.erp.server.bo.InventoryProduct;
import com.netappsid.erp.server.bo.PurchaseInvoice;
import com.netappsid.erp.server.dao.PurchaseInvoiceDAO;
import com.netappsid.europe.naid.custom.bo.CustomNomenclature;

public class MacPurchaseInvoiceDAO<T extends PurchaseInvoice> extends PurchaseInvoiceDAO<T>
{
	public MacPurchaseInvoiceDAO()
	{
		addDAOInitializeVisitor(InventoryProduct.class, new DAOInitializeVisitor<InventoryProduct>()
			{
				@Override
				public void initialize(InventoryProduct macInventoryProduct)
				{
					if (macInventoryProduct instanceof MacManufacturedProduct)
					{
						loadCustomNomenclature(((MacManufacturedProduct) macInventoryProduct).getCustomNomenclature());
					}
					else if (macInventoryProduct instanceof MacMaterialProduct)
					{
						loadCustomNomenclature(((MacMaterialProduct) macInventoryProduct).getCustomNomenclature());
					}
				}

				private void loadCustomNomenclature(CustomNomenclature customNomenclature)
				{
					if (customNomenclature != null)
					{
						initializeProxy(customNomenclature);
						initializeProxy(customNomenclature.getDescription());
					}
				}
			});
	}
}