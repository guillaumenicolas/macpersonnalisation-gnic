package com.mac.custom.dao;

import com.mac.custom.action.GenerateProductionBasket;
import com.netappsid.dao.DAOInitializeVisitor;
import com.netappsid.erp.server.bo.ProductionBatch;
import com.netappsid.erp.server.bo.WorkOrderRoutingStep;
import com.netappsid.erp.server.dao.EntityDescriptionDAO;

/**
 * DAO used by {@link GenerateProductionBasket}.<br>
 * This loads a {@link ProductionBatch} in a {@link WorkOrderRoutingStep} for specific usage in
 * {@link GenerateProductionBasket#generateProductionBasket(WorkOrderRoutingStep, com.netappsid.erp.server.bo.User, java.util.Date)}.
 * 
 * @author plcharette
 * 
 */
public class GenerateProductionBasketDAO extends EntityDescriptionDAO<WorkOrderRoutingStep>
{
	public GenerateProductionBasketDAO()
	{
		addDAOInitializeVisitor(WorkOrderRoutingStep.class, new DAOInitializeVisitor<WorkOrderRoutingStep>()
			{
				@Override
				public void initialize(WorkOrderRoutingStep workOrderRoutingStep)
				{
					initializeProxy(workOrderRoutingStep.getProductionBatch());
				}
			});
	}
}
