package com.mac.custom.dao;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.mac.custom.bo.ActiviteClient;
import com.mac.custom.bo.EnseigneClient;
import com.mac.custom.bo.MacCommercialClass;
import com.mac.custom.bo.MacCustomer;
import com.mac.custom.bo.MacRegionCommerciale;
import com.mac.custom.bo.MacTerm;
import com.mac.custom.bo.MacWorkflowMaster;
import com.mac.custom.enums.TypeClientEnum;
import com.mac.custom.services.customer.beans.DeclencheursEnCoursServicesBean;
import com.mac.custom.services.customer.interfaces.local.DeclencheursEnCoursServicesLocal;
import com.mac.custom.services.workflow.bean.MacWorkflowServicesBean;
import com.mac.custom.services.workflow.interfaces.local.MacWorkflowServicesLocal;
import com.netappsid.bo.model.Model;
import com.netappsid.bonitasoft.services.enums.BonitaParameters;
import com.netappsid.bonitasoft.services.services.BonitaServicesBean;
import com.netappsid.bonitasoft.services.services.interfaces.BonitaServices;
import com.netappsid.erp.server.bo.Customer;
import com.netappsid.erp.server.bo.PaymentMode;
import com.netappsid.erp.server.bo.Setup;
import com.netappsid.erp.server.bo.User;
import com.netappsid.europe.naid.custom.dao.CustomerEuroDAO;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.Loader;

public class MacCustomerDAO<T extends MacCustomer> extends CustomerEuroDAO<T>
{
	private static final Logger logger = Logger.getLogger(MacCustomerDAO.class);

	private final DeclencheursEnCoursServicesLocal declencheursEnCoursServicesLocal = new ServiceLocator<DeclencheursEnCoursServicesLocal>(
			DeclencheursEnCoursServicesBean.class).get();

	private final MacWorkflowServicesLocal macWfServicesLocator = new ServiceLocator<MacWorkflowServicesLocal>(MacWorkflowServicesBean.class).get();
	private final ServiceLocator<BonitaServices> wfServicesLocator = new ServiceLocator<BonitaServices>(BonitaServicesBean.class);

	private final ServiceLocator<Loader> loaderServicesLocator = new ServiceLocator<Loader>(LoaderBean.class);

	private MacWorkflowMaster macWFMaster;

	@Override
	public void loadAssociations(Object object)
	{
		super.loadAssociations(object);

		if (object instanceof MacCustomer)
		{
			loadMacCustomerAssociations((MacCustomer) object);
		}
		else if (object instanceof MacTerm)
		{
			loadTermAssociations((MacTerm) object);
		}
	}

	private void loadMacCustomerAssociations(MacCustomer macCustomer)
	{
		if (macCustomer != null)
		{
			initializeProxy(macCustomer);
			loadMacCommercialClassAssociations((MacCommercialClass) Model.getTarget(macCustomer.getCommercialClass()));
			MacCommercialClass macCommercialClass = (MacCommercialClass) Model.getTarget(macCustomer.getCustomerClass());
			loadMacCommercialClassAssociations(macCommercialClass);
			loadMacRegionCommercialeAssociations(macCustomer.getMacRegionCommerciale());
			loadActiviteClientAssociations(macCustomer.getActiviteClient());
			loadEnseigneClientAssociations(macCustomer.getEnseigneClient());
			loadEnCoursAAssociations(macCustomer.getEnCoursA());
			loadAnalysteCreditAssociations(macCustomer.getAnalysteCredit());
			loadPaymentModeAssociations(macCustomer.getPaymentMode());
		}
	}

	private void loadTermAssociations(MacTerm term)
	{
		if (term != null)
		{
			initializeProxy(term);
			initializeProxy(term.getDescription());
			loadPaymentModeAssociations(term.getPaymentMode());
		}
	}

	private void loadMacCommercialClassAssociations(MacCommercialClass macCommercialClass)
	{
		if (macCommercialClass != null)
		{
			initializeProxy(macCommercialClass);
			initializeProxy(macCommercialClass.getDescription());
			for (ActiviteClient activiteClient : macCommercialClass.getActivitesClient())
			{
				loadActiviteClientAssociations(activiteClient);
			}
			for (EnseigneClient enseigneClient : macCommercialClass.getEnseignesClient())
			{
				loadEnseigneClientAssociations(enseigneClient);
			}
		}
	}

	private void loadActiviteClientAssociations(ActiviteClient activiteClient)
	{
		if (activiteClient != null)
		{
			initializeProxy(activiteClient);
			initializeProxy(activiteClient.getDescription());
		}
	}

	private void loadEnseigneClientAssociations(EnseigneClient enseigneClient)
	{
		if (enseigneClient != null)
		{
			initializeProxy(enseigneClient);
			initializeProxy(enseigneClient.getDescription());
		}
	}

	private void loadMacRegionCommercialeAssociations(MacRegionCommerciale macRegionCommerciale)
	{
		if (macRegionCommerciale != null)
		{
			initializeProxy(macRegionCommerciale);
			initializeProxy(macRegionCommerciale.getDescription());
		}
	}

	private void loadEnCoursAAssociations(Customer enCoursA)
	{
		if (enCoursA != null)
		{
			initializeProxy(enCoursA);
		}
	}

	private void loadAnalysteCreditAssociations(User analysetCredit)
	{
		if (analysetCredit != null)
		{
			initializeProxy(analysetCredit);
			initializeProxy(analysetCredit.getCode());
		}
	}

	private void loadPaymentModeAssociations(PaymentMode paymentMode)
	{
		if (paymentMode != null)
		{
			initializeProxy(paymentMode);
			initializeProxy(paymentMode.getDescription());
		}
	}

	@Override
	public T save(T customer)
	{
		declencheursEnCoursServicesLocal.ajouterDeclencheurEnCours(Model.getTarget(customer).getClass().getCanonicalName(), customer.getId());

		TypeClientEnum oldTypeClient = getOldTypeClient(customer);

		// Clear the cache before saving to ensure Hibernate doesn't use old entities (otherwise sub-entities might not be merged properly)
		getEntityManager().clear();

		T savedCustomer = super.save(customer);

		// Validation finance
		if (getMacWFMaster() != null && getMacWFMaster().getWfFinanceValidation() != null && getMacWFMaster().getWfFinanceValidation().isActive())
		{
			if (oldTypeClient != TypeClientEnum.VALIDATIONFINANCE && ((MacCustomer) savedCustomer).getTypeClient() == TypeClientEnum.VALIDATIONFINANCE)
			{
				initiateFinanceValidationWorkflow(savedCustomer);
			}
		}

		// Valiadtion client
		if (getMacWFMaster() != null && getMacWFMaster().getWfClientValidation() != null && getMacWFMaster().getWfClientValidation().isActive())
		{
			if (oldTypeClient != TypeClientEnum.CLIENT && ((MacCustomer) savedCustomer).getTypeClient() == TypeClientEnum.CLIENT)
			{
				initiateClientValidationWorkflow(savedCustomer);
			}
		}

		// Deblocage automatique des commandes (si WF active)
		if (getMacWFMaster() != null && getMacWFMaster().getWfUnblockAuto() != null && getMacWFMaster().getWfUnblockAuto().isActive())
		{
			unblockOrders(savedCustomer);
		}

		return savedCustomer;
	}

	@Override
	public void delete(T customer)
	{
		declencheursEnCoursServicesLocal.ajouterDeclencheurEnCours(Model.getTarget(customer).getClass().getCanonicalName(), customer.getId());
		super.delete(customer);
	}

	private void unblockOrders(MacCustomer macCustomer)
	{
		try
		{
			if (macCustomer.getSfacRiskAmount() == null)
			{
				throw new Exception("Aucun risque sfac de renseigné");
			}

			macWfServicesLocator.unblockOrder(macCustomer.getId());
		}
		catch (Exception e)
		{
			logger.error("Erreur lors du deblocage automatique des commandes: " + e.getLocalizedMessage());
		}
	}

	/**
	 * Retourne le type de client du client persiste dans ls BD
	 * 
	 * @param customer
	 *            Client
	 * @return Type de client du client persiste dans ls BD
	 */
	private TypeClientEnum getOldTypeClient(Customer customer)
	{
		MacCustomer macCustomer = loaderServicesLocator.get().findById(MacCustomer.class, null, customer.getId());

		if (macCustomer != null)
		{
			return macCustomer.getTypeClient();
		}

		return null;
	}

	/**
	 * Initialisation du workflow validation finance
	 * 
	 * @param customer
	 *            Client qui passe a "validation finance"
	 * @return true
	 */
	private boolean initiateFinanceValidationWorkflow(MacCustomer customer)
	{
		Setup setup = loaderServicesLocator.get().findAll(Setup.class, null).get(0);

		Map<String, Object> params = new HashMap<String, Object>();

		// The name of the bean (the object)
		params.put(BonitaParameters.ENTITY_CLASSNAME.toString(), MacCustomer.class.getName());
		// The name of the form i.e. the name of the XML without .xml
		params.put(BonitaParameters.ENTITY_FORMNAME.toString(), "customer");
		// The uuid of the entity
		params.put(BonitaParameters.ENTITY_ID.toString(), customer.getId().toString());
		// The refenrece label
		params.put(BonitaParameters.ENTITY_REFERENCE.toString(), "Client " + customer.getCode());

		params.put("emailOrigin360", getMacWFMaster().getWfFinanceValidation().getEmailOrigin());
		params.put("emailCompta360", getMacWFMaster().getWfFinanceValidation().getEmailCompta());
		params.put("smtpServer360", setup.getMailSmtpHost());
		params.put("serverPort360", setup.getMailSmtpPort());
		params.put("sysadmin360", getMacWFMaster().getSysAdmin().getCode());

		try
		{
			wfServicesLocator.get().initiateProcess(com.mac.custom.utils.Utils.getLoginName(), "wfMACFinanceValidation", params);
		}
		catch (Exception e)
		{
			logger.error("Probleme lors de l'appel du WF validation finance: " + e.getLocalizedMessage());
		}

		return true;
	}

	/**
	 * Initialisation du workflow validation client
	 * 
	 * @param customer
	 *            Client qui passe a "validation client"
	 * @return true
	 */
	private boolean initiateClientValidationWorkflow(MacCustomer customer)
	{
		Setup setup = loaderServicesLocator.get().findAll(Setup.class, null).get(0);

		Map<String, Object> params = new HashMap<String, Object>();

		// The name of the bean (the object)
		params.put(BonitaParameters.ENTITY_CLASSNAME.toString(), MacCustomer.class.getName());
		// The name of the form i.e. the name of the XML without .xml
		params.put(BonitaParameters.ENTITY_FORMNAME.toString(), "customer");
		// The uuid of the entity
		params.put(BonitaParameters.ENTITY_ID.toString(), customer.getId().toString());
		// The refenrece label
		params.put(BonitaParameters.ENTITY_REFERENCE.toString(), "Client " + customer.getCode());

		params.put("condPaiement360", customer.getTerm() != null && customer.getTerm().getLocalizedDescription() != null ? customer.getTerm()
				.getLocalizedDescription() : "");
		params.put("modePaiement360", customer.getPaymentMode() != null && customer.getPaymentMode().getLocalizedDescription() != null ? customer
				.getPaymentMode().getLocalizedDescription() : "");
		params.put("emailOrigin360", getMacWFMaster().getWfFinanceValidation().getEmailOrigin());
		params.put("smtpServer360", setup.getMailSmtpHost());
		params.put("serverPort360", setup.getMailSmtpPort());
		params.put("sysadmin360", getMacWFMaster().getSysAdmin().getCode());

		try
		{
			wfServicesLocator.get().initiateProcess(com.mac.custom.utils.Utils.getLoginName(), "wfMACClientValidation", params);
		}
		catch (Exception e)
		{
			logger.error("Probleme lors de l'appel du WF validation client: " + e.getLocalizedMessage());
		}

		return true;
	}

	public MacWorkflowMaster getMacWFMaster()
	{
		if (macWFMaster == null)
		{
			macWFMaster = com.mac.custom.workflow.WorkflowUtils.getWFMaster();
		}
		return macWFMaster;
	}
}
