package com.mac.custom.dao;

import com.mac.custom.bo.MacOrderDetail;
import com.mac.custom.bo.WorkLoad;
import com.mac.custom.bo.WorkLoadDetail;
import com.netappsid.dao.GenericDAO;

public class MacOrderDetailWorkLoadDAO<T extends MacOrderDetail> extends GenericDAO<T>
{
	@Override
	public void loadAssociations(Object object)
	{
		logLoadAssociationsCall(object);

		if (object instanceof MacOrderDetail)
		{
			loadMacOrderDetailWorkLoadAssociations((MacOrderDetail) object);
		}
	}

	private void loadMacOrderDetailWorkLoadAssociations(MacOrderDetail macOrderDetail)
	{
		for (WorkLoadDetail workLoadDetail : macOrderDetail.getWorkLoadDetails())
		{
			loadWorkLoadDetailAssociations(workLoadDetail);
		}
	}

	private void loadWorkLoadDetailAssociations(WorkLoadDetail workLoadDetail)
	{
		if (workLoadDetail != null)
		{
			initializeProxy(workLoadDetail);
			loadWorkLoadAssociations(workLoadDetail.getWorkLoad());
		}
	}

	private void loadWorkLoadAssociations(WorkLoad workLoad)
	{
		if (workLoad != null)
		{
			initializeProxy(workLoad);
		}
	}
}
