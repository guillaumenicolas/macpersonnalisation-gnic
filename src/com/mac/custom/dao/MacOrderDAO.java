package com.mac.custom.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ObjectUtils;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.Multimaps;
import com.mac.custom.bo.MacCompany;
import com.mac.custom.bo.MacCustomer;
import com.mac.custom.bo.MacDocumentStatus;
import com.mac.custom.bo.MacOrder;
import com.mac.custom.bo.MacOrderDetail;
import com.mac.custom.bo.MacWorkflowARC;
import com.mac.custom.bo.MacWorkflowARCProductDelay;
import com.mac.custom.bo.MacWorkflowMaster;
import com.mac.custom.bo.WorkLoadDetail;
import com.mac.custom.bo.model.EncoursClient;
import com.mac.custom.factory.EncoursClientFactory;
import com.mac.custom.services.customer.beans.DeclencheursEnCoursServicesBean;
import com.mac.custom.services.customer.interfaces.local.DeclencheursEnCoursServicesLocal;
import com.mac.custom.utils.MacWorkShiftUtils;
import com.mac.custom.workflow.WorkflowUtils;
import com.netappsid.bo.model.Model;
import com.netappsid.bonitasoft.services.enums.BonitaParameters;
import com.netappsid.bonitasoft.services.services.BonitaServicesBean;
import com.netappsid.bonitasoft.services.services.interfaces.BonitaServices;
import com.netappsid.dao.DAOInitializeVisitor;
import com.netappsid.datatypes.MonetaryAmount;
import com.netappsid.erp.server.bo.BlockingHistory;
import com.netappsid.erp.server.bo.BlockingReason;
import com.netappsid.erp.server.bo.Communication;
import com.netappsid.erp.server.bo.Customer;
import com.netappsid.erp.server.bo.DocumentStatus;
import com.netappsid.erp.server.bo.ItemToShip;
import com.netappsid.erp.server.bo.Order;
import com.netappsid.erp.server.bo.OrderDetail;
import com.netappsid.erp.server.bo.Packages;
import com.netappsid.erp.server.bo.PaymentMode;
import com.netappsid.erp.server.bo.PropertyValue;
import com.netappsid.erp.server.bo.Quotation;
import com.netappsid.erp.server.bo.ServiceType;
import com.netappsid.erp.server.bo.Setup;
import com.netappsid.erp.server.bo.StatusHistory;
import com.netappsid.erp.server.bo.User;
import com.netappsid.erp.server.bo.WorkShift;
import com.netappsid.erp.server.enums.BlockingReasonTypeEnum;
import com.netappsid.erp.server.enums.ShiftTypeEnum;
import com.netappsid.europe.naid.custom.bo.OrderDetailEuro;
import com.netappsid.europe.naid.custom.dao.OrderEuroDAO;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.Loader;

public class MacOrderDAO<T extends MacOrder> extends OrderEuroDAO<T>
{
	private final DeclencheursEnCoursServicesLocal declencheursEnCoursServicesLocal = new ServiceLocator<DeclencheursEnCoursServicesLocal>(
			DeclencheursEnCoursServicesBean.class).get();

	private static final Logger logger = Logger.getLogger(MacOrderDAO.class);

	private final ServiceLocator<BonitaServices> wfServicesLocator = new ServiceLocator<BonitaServices>(BonitaServicesBean.class);
	private final ServiceLocator<Loader> loaderServicesLocator = new ServiceLocator<Loader>(LoaderBean.class);
	private final MacWorkShiftUtils workShiftUtils = new MacWorkShiftUtils();

	private MacWorkflowMaster macWFMaster;

	public MacOrderDAO()
	{
		addDAOInitializeVisitor(MacOrderDetail.class, new DAOInitializeVisitor<MacOrderDetail>()
			{
				@Override
				public void initialize(MacOrderDetail orderDetail)
				{
					for (WorkLoadDetail workLoadDetail : orderDetail.getWorkLoadDetails())
					{
						loadWorkLoadDetailAssociations(workLoadDetail);
					}
				}
			});
		addDAOInitializeVisitor(MacOrderDetail.class, new DAOInitializeVisitor<MacOrderDetail>()
			{
				@Override
				public void initialize(MacOrderDetail orderDetail)
				{
					for (WorkLoadDetail workLoadDetail : orderDetail.getWorkLoadDetails())
					{
						loadWorkLoadDetailAssociations(workLoadDetail);
					}
				}
			});
		addDAOInitializeVisitor(Quotation.class, new DAOInitializeVisitor<Quotation>()
			{
				@Override
				public void initialize(Quotation quotation)
				{
					for (StatusHistory statusHistory : quotation.getStatusHistories())
					{
						initializeProxy(statusHistory);
						initializeProxy(statusHistory.getUser());
						loadDocumentStatusAssociations(statusHistory.getDocumentStatus());
						loadDocumentStatusAssociations(statusHistory.getStatusFrom());
					}
				}
			});
		addDAOInitializeVisitor(DocumentStatus.class, new DAOInitializeVisitor<DocumentStatus>()
			{
				@Override
				public void initialize(DocumentStatus documentStatus)
				{
					loadDocumentStatusAssociations(documentStatus);
				}
			});
	}

	@Override
	public void loadAssociations(Object object)
	{
		super.loadAssociations(object);
		object = Model.getTarget(object);

		if (object instanceof MacOrder)
		{
			loadMacOrderAssociations((MacOrder) object);
		}
		else if (object instanceof MacCustomer)
		{
			loadMacCustomerAssociations((MacCustomer) object);
		}
	}

	private void loadWorkLoadDetailAssociations(WorkLoadDetail workLoadDetail)
	{
		if (workLoadDetail != null)
		{
			initializeProxy(workLoadDetail);
		}
	}

	private void loadMacOrderAssociations(MacOrder macOrder)
	{
		if (macOrder != null)
		{
			initializeProxy(macOrder);
			loadPaymentModeAssociations(macOrder.getPaymentMode());
			loadServiceTypeAssociations(macOrder.getServiceType());
			loadMacCompanyAssociations((MacCompany) Model.getTarget(macOrder.getCompany()));
		}
	}

	private void loadMacCompanyAssociations(MacCompany macCompany)
	{
		if (macCompany != null)
		{
			initializeProxy(macCompany);
			loadAvailableMeasureUnitAssociations(macCompany.getWorkUnitAvailableMeasureUnit());
		}
	}

	private void loadMacCustomerAssociations(MacCustomer macCustomer)
	{
		if (macCustomer != null)
		{
			loadPaymentModeAssociations(macCustomer.getPaymentMode());
			loadUserAssociations(macCustomer.getBlockingProductionUser());
		}
	}

	private void loadPaymentModeAssociations(PaymentMode paymentMode)
	{
		if (paymentMode != null)
		{
			initializeProxy(paymentMode);
			initializeProxy(paymentMode.getDescription());
		}
	}

	private void loadServiceTypeAssociations(ServiceType serviceType)
	{
		if (serviceType != null)
		{
			initializeProxy(serviceType);
			initializeProxy(serviceType.getDescription());
		}
	}

	@Override
	public void delete(T order)
	{
		// Work with the target rather than the proxy to prevent a crash that happened only when running embedded,
		// presumably due to some bug or another in mob. See ERP-3863.
		order = Model.getTarget(order);
		declencheursEnCoursServicesLocal.ajouterDeclencheurEnCours(Model.getTarget(order.getCustomer()).getClass().getCanonicalName(), order.getCustomer()
				.getId());
		super.delete(order);
	}

	protected void blockOrderFinance(MacOrder macOrder)
	{
		if (!macOrder.isBlockProductionEffective())
		{
			macOrder.blockUnblockProduction(getMacWFMaster().getWfBlockAuto().getBlockingReason(), getMacWFMaster().getUserWF());
		}

		if (!macOrder.isBlockShippingEffective())
		{
			macOrder.blockUnblockShipping(getMacWFMaster().getWfBlockAuto().getBlockingReason(), getMacWFMaster().getUserWF());
		}
	}

	protected void blockOrderProforma(MacOrder macOrder)
	{
		if (!macOrder.isBlockProductionEffective())
		{
			macOrder.blockUnblockProduction(getMacWFMaster().getWfBlockAuto().getBlockingReasonProforma(), getMacWFMaster().getUserWF());
		}

		if (!macOrder.isBlockShippingEffective())
		{
			macOrder.blockUnblockShipping(getMacWFMaster().getWfBlockAuto().getBlockingReasonProforma(), getMacWFMaster().getUserWF());
		}
	}

	@Override
	public T save(T order)
	{
		Customer customer = order.getCustomer();
		declencheursEnCoursServicesLocal.ajouterDeclencheurEnCours(customer.getClass().getCanonicalName(), customer.getId());

		DocumentStatus oldStatus = getLastStatus(order);
		MonetaryAmount oldTotal = getLastTotal(order);

		// sets the first communicated shipping date if the status requires it
		if (order.getFirstCommunicatedShippingDate() == null && order.getMacDocumentStatus().isSaveFirstCommunicatedShippingDateRequired())
		{
			order.setFirstCommunicatedShippingDate(order.getShippingDate());
		}

		// If the order was cancelled, cancel each order detail
		if (order.getDocumentStatus().isCancel())
		{
			for (OrderDetail detail : order.getDetails())
			{
				if (!detail.isCancel())
				{
					detail.setCancel(true);
				}
			}
		}

		// Make sure that the MacPackages are stores first, otherwise hibernate sees them as Packages and cannot save.
		for (ItemToShip itemToShip : order.getItemsToShip())
		{
			if (Packages.class.isAssignableFrom(Hibernate.getClass(itemToShip)))
			{
				if (itemToShip.getId() == null)
				{
					getEntityManager().persist(itemToShip);
				}
			}
		}

		T savedOrder = super.save(order);
		DocumentStatus status = order.getDocumentStatus();
		MacDocumentStatus macStatus = MacDocumentStatus.class.isAssignableFrom(Hibernate.getClass(status)) ? (MacDocumentStatus) Model.getTarget(status) : null;
		MonetaryAmount total = order.getTotal();

		if (macStatus.isValidateBalance() && (!macStatus.hasSameID(oldStatus) || oldTotal == null || oldTotal.compareTo(total) != 0))
		{
			if (isBlockedFinance(order))
			{
				boolean orderUnblocked = false;
				// Si le client a un blocage, la commande doit rester bloqu?e
				if (!customer.isBlockProduction() && !customer.isBlockShipping()/* PAS de blocage niveau client */)
				{
					if (!savedOrder.isProformaEffective())
					{
						boolean customerCreditValid = orderValidvsLimit(order);
						if (customerCreditValid)
						{
							// Cr?dit valide, d?bloquer la commande (raison Finance)
							if (getMacWFMaster() != null && getMacWFMaster().getWfUnblockAuto() != null
									&& getMacWFMaster().getWfUnblockAuto().isActive() == true)
							{
								orderUnblocked = true;
								savedOrder.resetToCustomerBlockProductionDefault(getMacWFMaster().getUserWF());
								savedOrder.resetToCustomerBlockShippingDefault(getMacWFMaster().getUserWF());
							}
						}
					}
				}

				if (!orderUnblocked)
				{
					// Commande non d?bloqu?e, revenir au statut initial
					if (oldStatus != null)
					{
						savedOrder.setDocumentStatus(oldStatus);
					}
				}
			}
			else
			{
				// Ne pas tenter de bloquer automatiquement si la commande est d?bloqu?e manuellement
				boolean isUnblocked = false;
				List<BlockingHistory> blockingHistories = order.getBlockingHistories();
				for (BlockingHistory blockingHistory : blockingHistories)
				{
					if (blockingHistory.getReasonType() == BlockingReasonTypeEnum.UNBLOCK)
					{
						isUnblocked = true;
						break;
					}

				}
				if (!isUnblocked)
				{
					if (savedOrder.isProformaEffective())
					{
						// Blocage (raison Proforma) et revenir au statut initial
						if (getMacWFMaster() != null && getMacWFMaster().getWfBlockAuto() != null && getMacWFMaster().getWfBlockAuto().isActive() == true)
						{
							blockOrderProforma(savedOrder);
							initiateBlockAutoWorkflow(savedOrder);
						}
						if (oldStatus != null)
						{
							savedOrder.setDocumentStatus(oldStatus);
						}
					}
					else
					{
						if (!macStatus.hasSameID(oldStatus) || total.compareTo(oldTotal) > 0)
						{
							boolean customerCreditValid = orderValidvsLimit(order);
							if (!customerCreditValid)
							{
								// Cr?dit invalide, blocage (raison Finance) et revenir au statut initial
								if (getMacWFMaster() != null && getMacWFMaster().getWfBlockAuto() != null
										&& getMacWFMaster().getWfBlockAuto().isActive() == true)
								{
									blockOrderFinance(savedOrder);
									initiateBlockAutoWorkflow(savedOrder);
								}
								if (oldStatus != null)
								{
									savedOrder.setDocumentStatus(oldStatus);
								}
							}
						}
					}
				}
			}
		}

		if (order.getId() != null)
		{
			// La commande a ete debloqu�e production)
			if (orderHasBeenUnblockedProduction(savedOrder))
			{
				initiateManualProductionUnblock(savedOrder);
			}
			// La commande a ete debloqu�e exp�dition)
			if (orderHasBeenUnblockedShipping(savedOrder))
			{
				initiateManualShippingUnblock(savedOrder);
			}
		}

		if (!(savedOrder.isBlockProductionEffective() || savedOrder.isBlockShippingEffective()))
		{
			initiateARCWorkflow(savedOrder, oldStatus);
		}

		initiateM5PWorkflow(savedOrder);

		return savedOrder;
	}

	private boolean isBlockedFinance(T order)
	{
		if (getMacWFMaster() != null && getMacWFMaster().getWfBlockAuto() != null)
		{
			BlockingReason prodBlockingReason = order.getBlockingProductionReason();
			BlockingReason shipBlockingReason = order.getBlockingShippingReason();
			BlockingReason financeBlockingReason = getMacWFMaster().getWfBlockAuto().getBlockingReason();
			return (prodBlockingReason != null && prodBlockingReason.hasSameID(financeBlockingReason))
					|| (shipBlockingReason != null && shipBlockingReason.hasSameID(financeBlockingReason));
		}
		return false;
	}

	private boolean orderValidvsLimit(T order)
	{
		boolean result = false;

		try
		{
			MacCustomer customer = (MacCustomer) Model.getTarget(order.getCustomer());

			// Ignore current order in overflow calculation to make sure an old total is not taken into account for an
			// order which has changed counters without changing from being included of not in the total
			EncoursClientFactory encoursFactory = new EncoursClientFactory();
			EncoursClient encoursWithoutOrder = encoursFactory.create(customer, order.getId());
			MonetaryAmount newOverflow = encoursWithoutOrder.getDepassement().add(order.getTotal());

			MonetaryAmount risk = getTotalRisk(customer);

			if (risk != null)
			{
				if (newOverflow.compareTo(new MonetaryAmount(BigDecimal.ZERO, newOverflow.getCurrency())) < 0)
				{
					return true;
				}
			}
		}
		catch (Exception e)
		{
			logger.error("Erreur lors de la validation de la limite credit: " + e.getLocalizedMessage());
		}

		return result;
	}

	private DocumentStatus getLastStatus(Order order)
	{
		return (DocumentStatus) findOrderField(order.getId(), MacOrder.PROPERTYNAME_DOCUMENTSTATUS);
	}

	private MonetaryAmount getLastTotal(Order order)
	{
		return (MonetaryAmount) findOrderField(order.getId(), MacOrder.PROPERTYNAME_TOTAL);
	}

	private Object findOrderField(Serializable orderId, String orderProperty)
	{
		Object object = null;
		if (orderId != null && orderProperty != null)
		{
			final StringBuilder hqlQuery = new StringBuilder();
			hqlQuery.append("SELECT orders." + orderProperty).append("\n");
			hqlQuery.append("FROM Order orders").append("\n");
			hqlQuery.append("WHERE orders." + MacOrder.PROPERTYNAME_ID + " = :orderId ");

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("orderId", orderId.toString());

			object = loaderServicesLocator.get().findByQuery(hqlQuery.toString(), parameters, true);
		}

		return object;
	}

	private Object findOrderDetailField(Serializable macOrderDetailId, String orderDetailProperty)
	{
		Object object = null;
		if (macOrderDetailId != null && orderDetailProperty != null)
		{
			final StringBuilder hqlQuery = new StringBuilder();
			hqlQuery.append("SELECT macOrderDetail." + orderDetailProperty).append("\n");
			hqlQuery.append("FROM MacOrderDetail macOrderDetail").append("\n");
			hqlQuery.append("WHERE macOrderDetail." + MacOrderDetail.PROPERTYNAME_ID + " = :macOrderDetailId ");

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("macOrderDetailId", macOrderDetailId.toString());

			object = loaderServicesLocator.get().findByQuery(hqlQuery.toString(), parameters, true);
		}

		return object;
	}

	private boolean orderHasBeenUnblockedProduction(Order order)
	{
		if (!order.isBlockProductionEffective())
		{
			Boolean productionBlocked = (Boolean) findOrderField(order.getId(), MacOrder.PROPERTYNAME_BLOCKPRODUCTION);
			return productionBlocked == null ? order.getCustomer().isBlockProduction() : productionBlocked.booleanValue();
		}
		return false;
	}

	private boolean orderHasBeenUnblockedShipping(Order order)
	{
		if (!order.isBlockShippingEffective())
		{
			Boolean shippingBlocked = (Boolean) findOrderField(order.getId(), MacOrder.PROPERTYNAME_BLOCKSHIPPING);
			return shippingBlocked == null ? order.getCustomer().isBlockShipping() : shippingBlocked.booleanValue();
		}
		return false;
	}

	private String getUserEmailAddresses(User user)
	{
		ArrayList<String> list = new ArrayList<String>();

		String result = "";

		for (Communication communication : user.getCommunications())
		{
			if (communication.getType().equalsIgnoreCase(WorkflowUtils.EMAIL))
			{
				list.add(communication.getValue());
			}
		}

		for (int cmp = 0; cmp < list.size(); cmp++)
		{
			result = result + list.get(cmp);

			if (cmp != list.size() - 1)
			{
				result = result + ",";
			}
		}

		return result;
	}

	/**
	 * Retourne le montant total du risque pour un client donn�
	 * 
	 * @param macCustomer
	 *            Client
	 * @return Montant total du risque
	 * @throws Exception
	 *             Exception lors du calcul
	 */
	private MonetaryAmount getTotalRisk(MacCustomer macCustomer) throws Exception
	{
		if (macCustomer.getSfacRiskAmount() == null)
		{
			throw new Exception("Aucun risque sfac de renseign\u00E9");
		}

		return macCustomer.getEncoursClient().getEncoursAccepte();
	}

	private boolean initiateBlockAutoWorkflow(Order order)
	{
		// Desactive temporairement a la demande de Michel
		/*
		 * if(!order.getDocumentStatus().hasSameID(getMacWFMaster().getWfBlockAuto().getInitialStatus())) { return false; }
		 */

		Setup setup = loaderServicesLocator.get().findAll(Setup.class, null).get(0);

		MacCustomer customer = (MacCustomer) Model.getTarget(order.getCustomer());

		Map<String, Object> params = new HashMap<String, Object>();
		// The name of the bean (the object)
		params.put(BonitaParameters.ENTITY_CLASSNAME.toString(), Order.class.getName());
		// The name of the form i.e. the name of the XML without .xml
		params.put(BonitaParameters.ENTITY_FORMNAME.toString(), "order");
		// The uuid of the entity
		params.put(BonitaParameters.ENTITY_ID.toString(), order.getId().toString());
		// The refenrece label
		params.put(BonitaParameters.ENTITY_REFERENCE.toString(), "Commande " + order.getDocumentNumber());

		if (customer.getAnalysteCredit() != null)
		{
			params.put("analysteFinancierEmails360", getUserEmailAddresses(customer.getAnalysteCredit()));
		}

		params.put("emailOrigin360", getMacWFMaster().getWfARC().getEmailOrigin());
		params.put("smtpServer360", setup.getMailSmtpHost());
		params.put("serverPort360", setup.getMailSmtpPort());
		params.put("documentNumber360", "Commande " + order.getNumber());

		try
		{
			wfServicesLocator.get().initiateProcess(com.mac.custom.utils.Utils.getLoginName(), "wfMACBlockAuto", params);
		}
		catch (Exception e)
		{
			logger.error("Probleme lors de l'appel du WF blocage automatique: " + e.getLocalizedMessage());
		}

		return true;
	}

	private boolean initiateM5PWorkflow(Order order)
	{
		if (getMacWFMaster() == null || getMacWFMaster().getWfM5P() == null || getMacWFMaster().getWfM5P().isActive() == false)
		{
			return false;
		}

		Setup setup = loaderServicesLocator.get().findAll(Setup.class, null).get(0);

		for (OrderDetail detail : order.getDetails())
		{
			OrderDetailEuro detailEuro = (OrderDetailEuro) detail;

			// C'est un M5P
			if (isM5POrderDetail(detailEuro))
			{
				detailEuro.setRareBirdProcessStartDate(new Date());
				Map<String, Object> params = new HashMap<String, Object>();
				// The name of the bean (the object)
				params.put(BonitaParameters.ENTITY_CLASSNAME.toString(), Order.class.getName());
				// The name of the form i.e. the name of the XML without .xml
				params.put(BonitaParameters.ENTITY_FORMNAME.toString(), "order");
				// The uuid of the entity
				params.put(BonitaParameters.ENTITY_ID.toString(), order.getId().toString());
				// The refenrece label
				params.put(BonitaParameters.ENTITY_REFERENCE.toString(), "Commande " + order.getDocumentNumber());

				params.put("documentNumber360", "Commande " + order.getNumber());
				params.put("emailAddressBE", getMacWFMaster().getWfM5P().getEmailAddressBE());
				params.put("smtpServer360", setup.getMailSmtpHost());
				params.put("serverPort360", setup.getMailSmtpPort());
				params.put("transactionDetailId360", detailEuro.getId().toString());
				try
				{
					wfServicesLocator.get().initiateProcess(com.mac.custom.utils.Utils.getLoginName(), "wfMACM5P", params);
				}
				catch (Exception e)
				{
					logger.error("Probleme lors de l'appel du WF M5p: " + e.getLocalizedMessage());
				}
			}
		}

		return true;
	}

	private boolean isM5POrderDetail(OrderDetailEuro orderDetailEuro)
	{
		boolean isM5P = false;

		if (orderDetailEuro.isRareBird() && (orderDetailEuro.getRareBirdAccepted() == null || !orderDetailEuro.getRareBirdAccepted()))
		{
			// nouveau detail de commande
			if (orderDetailEuro.getId() == null)
			{
				isM5P = true;
			}
			else
			{
				Boolean oldM5P = (Boolean) findOrderDetailField(orderDetailEuro.getId(), MacOrderDetail.PROPERTYNAME_RAREBIRD);

				// detail de commande existant, anciennement pas M5P
				if (oldM5P == null || (oldM5P != null && !oldM5P.booleanValue()))
				{
					isM5P = true;
				}
			}
		}

		return isM5P;
	}

	private boolean initiateARCWorkflow(Order order, DocumentStatus oldStatus)
	{

		if (getMacWFMaster() == null || getMacWFMaster().getWfARC() == null || getMacWFMaster().getWfARC().isActive() == false)
		{
			return false;
		}

		if (oldStatus == null)
		{
			if (order.getDocumentStatus() != null && !order.getDocumentStatus().hasSameID(getMacWFMaster().getWfARC().getInitialDelayStatus()))
			{
				return false;
			}

		}

		else if (oldStatus.hasSameID(order.getDocumentStatus()) || !order.getDocumentStatus().hasSameID(getMacWFMaster().getWfARC().getInitialDelayStatus()))
		{
			return false;
		}

		Setup setup = loaderServicesLocator.get().findAll(Setup.class, null).get(0);

		Map<String, Object> params = new HashMap<String, Object>();

		// The name of the bean (the object)
		params.put(BonitaParameters.ENTITY_CLASSNAME.toString(), Order.class.getName());
		// The name of the form i.e. the name of the XML without .xml
		params.put(BonitaParameters.ENTITY_FORMNAME.toString(), "order");
		// The uuid of the entity
		params.put(BonitaParameters.ENTITY_ID.toString(), order.getId().toString());
		// The refenrece label
		params.put(BonitaParameters.ENTITY_REFERENCE.toString(), "Commande " + order.getDocumentNumber());

		params.put("arcOrigin360", getMacWFMaster().getWfARC().getEmailOrigin());
		params.put("documentNumber360", "Commande " + order.getNumber());
		params.put("smtpServer360", setup.getMailSmtpHost());
		params.put("serverPort360", setup.getMailSmtpPort());
		params.put("sysadmin360", getMacWFMaster().getSysAdmin().getCode());
		// delay end date
		params.put("arcDelayEndDate360", getARCApprovalDelay(order));

		try
		{
			wfServicesLocator.get().initiateProcess(com.mac.custom.utils.Utils.getLoginName(), "wfMACARC", params);
		}
		catch (Exception e)
		{
			logger.error("Probleme lors de l'appel du WF ARC: " + e.getLocalizedMessage());
		}
		return true;
	}

	private Date getARCApprovalDelay(Order order)
	{
		// default to 0
		Date endDate = new Date();
		MacWorkflowARC wfARC = getMacWFMaster().getWfARC();
		if (wfARC != null)
		{
			int delayInHours = 0;
			// group by product but mutiple lines can have the same product
			final ImmutableListMultimap<String, MacWorkflowARCProductDelay> delaysByProduct = Multimaps.index(wfARC.getApprovalProductDelays(),
					new Function<MacWorkflowARCProductDelay, String>()
						{
							@Override
							public String apply(MacWorkflowARCProductDelay delay)
							{
								return delay.getProduct().getUpperCode();
							}
						});

			for (OrderDetail detail : order.getDetails())
			{
				if (detail != null && detail.getProduct() != null)
				{
					for (MacWorkflowARCProductDelay delay : delaysByProduct.get(detail.getProduct().getUpperCode()))
					{
						// check if the property in the delay fit any property on the detail
						for (PropertyValue<?> pv : detail.getProperties())
						{
							if (delay.getProperty().getUpperCode().equals(pv.getProperty().getUpperCode()))
							{
								if (ObjectUtils.equals(delay.getPropertyValue().getValue(), pv.getValue()))
								{
									// it fits
									delayInHours = Math.max(delayInHours, delay.getApprovalDelay());
								}
							}
						}
					}
				}
			}
			// use the default in the setting screen
			if (delayInHours == 0)
			{
				delayInHours = wfARC.getApprovalDelay();
			}

			// apply delay to calendar
			WorkShift workShift = wfARC.getWorkShift();
			if (workShift != null)
			{
				if (workShift.getShiftType() != ShiftTypeEnum.intervalShift)
				{
					throw new IllegalStateException("The ARC worklof calendar is not using interval shifts.");
				}
				endDate = workShiftUtils.applyWorkShiftWorkedHoursInterval(endDate, delayInHours * 3600000l, workShift);
			}
		}
		return endDate;
	}

	private boolean initiateManualShippingUnblock(Order order)
	{
		if (getMacWFMaster() == null || getMacWFMaster().getWfUnblockManual() == null || getMacWFMaster().getWfUnblockManual().isActive() == false)
		{
			return false;
		}

		Map<String, Object> params = new HashMap<String, Object>();
		// The name of the bean (the object)
		params.put(BonitaParameters.ENTITY_CLASSNAME.toString(), Order.class.getName());
		// The name of the form i.e. the name of the XML without .xml
		params.put(BonitaParameters.ENTITY_FORMNAME.toString(), "order");
		// The uuid of the entity
		params.put(BonitaParameters.ENTITY_ID.toString(), order.getId().toString());
		// The refenrece label
		params.put(BonitaParameters.ENTITY_REFERENCE.toString(), "Commande " + order.getDocumentNumber());

		try
		{
			wfServicesLocator.get().initiateProcess(com.mac.custom.utils.Utils.getLoginName(), "wfMACUnblockManual", params);
		}
		catch (Exception e)
		{
			logger.error("Probleme lors de l'appel du WF deblocage manuel: " + e.getLocalizedMessage());
		}

		return true;
	}

	private boolean initiateManualProductionUnblock(Order order)
	{
		if (getMacWFMaster() == null || getMacWFMaster().getWfUnblockManual() == null || getMacWFMaster().getWfUnblockManual().isActive() == false)
		{
			return false;
		}

		Map<String, Object> params = new HashMap<String, Object>();
		// The name of the bean (the object)
		params.put(BonitaParameters.ENTITY_CLASSNAME.toString(), Order.class.getName());
		// The name of the form i.e. the name of the XML without .xml
		params.put(BonitaParameters.ENTITY_FORMNAME.toString(), "order");
		// The uuid of the entity
		params.put(BonitaParameters.ENTITY_ID.toString(), order.getId().toString());
		// The reference label
		params.put(BonitaParameters.ENTITY_REFERENCE.toString(), "Commande " + order.getDocumentNumber());

		try
		{
			wfServicesLocator.get().initiateProcess(com.mac.custom.utils.Utils.getLoginName(), "wfMACUnblockManual", params);
		}
		catch (Exception e)
		{
			logger.error("Probleme lors de l'appel du WF deblocage manuel: " + e.getLocalizedMessage());
		}

		return true;
	}

	@Override
	protected void loadDocumentStatusAssociations(DocumentStatus documentStatus)
	{
		if (documentStatus != null)
		{
			initializeProxy(documentStatus.getDescription());
			initializeProxy(documentStatus.getCustom());
		}
	}

	public MacWorkflowMaster getMacWFMaster()
	{
		if (macWFMaster == null)
		{
			macWFMaster = com.mac.custom.workflow.WorkflowUtils.getWFMaster();
		}
		return macWFMaster;
	}
}
