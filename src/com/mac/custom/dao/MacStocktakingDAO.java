package com.mac.custom.dao;

import com.mac.custom.services.beans.MacStocktakingServicesBean;
import com.mac.custom.services.interfaces.MacStocktakingServices;
import com.netappsid.dao.DAOInitializeVisitor;
import com.netappsid.erp.server.bo.GroupingModel;
import com.netappsid.erp.server.bo.InventoryProduct;
import com.netappsid.erp.server.bo.ProductLocation;
import com.netappsid.erp.server.bo.ProductSupplier;
import com.netappsid.erp.server.bo.ProductWarehouse;
import com.netappsid.erp.server.bo.Stocktaking;
import com.netappsid.erp.server.dao.StocktakingDAO;
import com.netappsid.erp.server.dao.utils.GroupMeasureDAOUtils;
import com.netappsid.framework.utils.ServiceLocator;

public class MacStocktakingDAO extends StocktakingDAO<Stocktaking>
{
	private final MacStocktakingServices stocktakingServices = new ServiceLocator<MacStocktakingServices>(MacStocktakingServicesBean.class).get();

	public MacStocktakingDAO()
	{
		addDAOInitializeVisitor(InventoryProduct.class, new DAOInitializeVisitor<InventoryProduct>()
			{
				@Override
				public void initialize(InventoryProduct inventoryProduct)
				{
					for (ProductSupplier supplier : inventoryProduct.getSuppliers())
					{
						if (supplier != null)
						{
							initializeProxy(supplier);
							initializeProxy(supplier.getSupplier());
						}
					}

					for (GroupingModel gm : inventoryProduct.getGroupingModels())
					{
						if (gm != null)
						{
							initializeProxy(gm);
							initializeProxy(gm.getDescription());
						}
					}

					for (ProductWarehouse productWarehouse : inventoryProduct.getProductWarehouses())
					{
						if (productWarehouse != null)
						{
							initializeProxy(productWarehouse);
							initializeProxy(productWarehouse.getWarehouse());
							for (ProductLocation pl : productWarehouse.getProductLocations())
							{
								loadProductLocationAssociations(pl);
							}
						}
					}
					for (ProductLocation pl : inventoryProduct.getProductLocations())
					{
						loadProductLocationAssociations(pl);
					}

							GroupMeasureDAOUtils.loadGroupMeasureAssociations(inventoryProduct.getLastPurchasePriceMeasureGroupMeasure());
				}

				private void loadProductLocationAssociations(ProductLocation productLocation)
				{
					if (productLocation != null)
					{
						initializeProxy(productLocation);
						initializeProxy(productLocation.getLocation());
					}
				}
			});
	}

	@Override
	public Stocktaking save(Stocktaking object)
	{
		stocktakingServices.checkAndUpdateIncompleteStatus(object);

		return super.save(object);
	}
}
