package com.mac.custom.dao;

import com.mac.custom.bo.MacCustomer;
import com.mac.custom.bo.MacOrder;
import com.mac.custom.bo.MacOrderDetail;
import com.mac.custom.bo.WorkLoad;
import com.mac.custom.bo.WorkLoadDetail;
import com.netappsid.bo.model.Model;
import com.netappsid.dao.GenericDAO;
import com.netappsid.erp.server.bo.Employee;
import com.netappsid.erp.server.bo.Location;
import com.netappsid.erp.server.bo.Machine;
import com.netappsid.erp.server.bo.OrderDetail;
import com.netappsid.erp.server.bo.Person;
import com.netappsid.erp.server.bo.Product;
import com.netappsid.erp.server.bo.Resource;
import com.netappsid.erp.server.bo.User;
import com.netappsid.erp.server.bo.WorkingStation;
import com.netappsid.erp.server.dao.utils.GroupMeasureDAOUtils;

public class WorkLoadDAO<T extends WorkLoad> extends GenericDAO<T>
{
	@Override
	public void loadAssociations(Object object)
	{
		logLoadAssociationsCall(object);

		if (object instanceof WorkLoad)
		{
			loadWorkLoadAssociations((WorkLoad) object);
			for (WorkLoadDetail workLoadDetail : ((WorkLoad) object).getDetails())
			{
				loadWorkLoadDetailAssociations(workLoadDetail);
			}
		}
		else if (object instanceof WorkLoadDetail)
		{
			loadWorkLoadDetailAssociations((WorkLoadDetail) object);
			loadWorkLoadAssociations(((WorkLoadDetail) object).getWorkLoad());
		}
	}

	private void loadWorkLoadAssociations(WorkLoad workLoad)
	{
		if (workLoad != null)
		{
			initializeProxy(workLoad);

			Resource resource = Model.getTarget(workLoad.getResource());
			loadResourceAssociations(resource);
		}
	}

	private void loadWorkLoadDetailAssociations(WorkLoadDetail workLoadDetail)
	{
		if (workLoadDetail != null)
		{
			initializeProxy(workLoadDetail);

			MacOrderDetail macOrderDetail = Model.getTarget(workLoadDetail.getOrderDetail());
			loadMacOrderDetailAssociations(macOrderDetail);

			GroupMeasureDAOUtils.loadGroupMeasureAssociations(workLoadDetail.getQuantityGroupMeasure());
			GroupMeasureDAOUtils.loadMeasureUnitMultiplierAssociations(workLoadDetail.getQuantityConvertedUnit());
			GroupMeasureDAOUtils.loadGroupMeasureAssociations(workLoadDetail.getQuantityProducedGroupMeasure());
			GroupMeasureDAOUtils.loadMeasureUnitMultiplierAssociations(workLoadDetail.getQuantityProducedConvertedUnit());
		}
	}

	private void loadMacOrderDetailAssociations(MacOrderDetail macOrderDetail)
	{
		if (macOrderDetail != null)
		{
			initializeProxy(macOrderDetail);
			initializeProxy(macOrderDetail.getProperties());

			GroupMeasureDAOUtils.loadGroupMeasureAssociations(macOrderDetail.getQuantityGroupMeasure());

			loadProductAssociations(macOrderDetail.getProduct());
			initializeProxy(macOrderDetail.getConfigurationRevision());
			initializeProxy(macOrderDetail.getConfiguratorRevision());
			initializeProxy(macOrderDetail.getProductNomenclature());

			loadSaleTransactionAssociation(macOrderDetail.getMacOrder());
		}
	}

	private void loadSaleTransactionAssociation(MacOrder macOrder)
	{
		if (macOrder != null)
		{
			initializeProxy(macOrder);
			MacCustomer macCustomer = Model.getTarget(macOrder.getCustomer());

			for (OrderDetail detail : macOrder.getDetails())
			{
				MacOrderDetail macOrderDetail = Model.getTarget(detail);
				loadLightMacOrderDetailAssociations(macOrderDetail);
			}

			loadMacCustomerAssociations(macCustomer);
		}
	}

	private void loadLightMacOrderDetailAssociations(MacOrderDetail macOrderDetail)
	{
		if (macOrderDetail != null)
		{
			initializeProxy(macOrderDetail);

			for (WorkLoadDetail workLoadDetail : macOrderDetail.getWorkLoadDetails())
			{
				loadLightWorkLoadDetailAssociations(workLoadDetail);
			}
		}
	}

	private void loadLightWorkLoadDetailAssociations(WorkLoadDetail workLoadDetail)
	{
		if (workLoadDetail != null)
		{
			initializeProxy(workLoadDetail);
			GroupMeasureDAOUtils.loadGroupMeasureAssociations(workLoadDetail.getQuantityGroupMeasure());
			GroupMeasureDAOUtils.loadMeasureUnitMultiplierAssociations(workLoadDetail.getQuantityConvertedUnit());
			GroupMeasureDAOUtils.loadGroupMeasureAssociations(workLoadDetail.getQuantityProducedGroupMeasure());
			GroupMeasureDAOUtils.loadMeasureUnitMultiplierAssociations(workLoadDetail.getQuantityProducedConvertedUnit());
		}
	}

	private void loadMacCustomerAssociations(MacCustomer macCustomer)
	{
		if (macCustomer != null)
		{
			initializeProxy(macCustomer);
		}
	}

	private void loadProductAssociations(Product product)
	{
		if (product != null)
		{
			initializeProxy(product);
			initializeProxy(product.getDescription());
		}
	}

	private void loadResourceAssociations(Resource resource)
	{
		if (resource != null)
		{
			initializeProxy(resource);
			initializeProxy(resource.getName());

			if (resource instanceof Machine)
			{
				loadMachineAssociations((Machine) resource);
			}
			else if (resource instanceof WorkingStation)
			{
				loadWorkingStationAssociations(((WorkingStation) resource));
			}
			else
			{
				Person person = ((Person) resource);
				if (person instanceof User)
				{
					initializeProxy((User) person);
				}
			}
		}
	}

	private void loadMachineAssociations(Machine machine)
	{
		if (machine != null)
		{
			initializeProxy(machine);
			initializeProxy(machine.getDescription());
		}
	}

	private void loadWorkingStationAssociations(WorkingStation workingStation)
	{
		if (workingStation != null)
		{
			initializeProxy(workingStation);
			initializeProxy(workingStation.getDescription());

			for (Employee employee : workingStation.getEmployees())
			{
				initializeProxy(employee);
			}

			for (Machine machine : workingStation.getMachines())
			{
				initializeProxy(machine);
			}

			for (Location location : workingStation.getLocations())
			{
				initializeProxy(location);
				initializeProxy(location.getWarehouse());
				initializeProxy(location.getWarehouse().getDescription());
			}
		}
	}

}
