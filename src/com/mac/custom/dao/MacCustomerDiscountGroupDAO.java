package com.mac.custom.dao;

import java.util.Map;

import com.netappsid.dao.DAOInitializeVisitor;
import com.netappsid.erp.server.bo.CustomerDiscountGroup;
import com.netappsid.erp.server.bo.extension.DiscountGroupCustom;
import com.netappsid.erp.server.dao.CustomerDiscountGroupDAO;

public class MacCustomerDiscountGroupDAO<T extends CustomerDiscountGroup> extends CustomerDiscountGroupDAO<T>
{
	public MacCustomerDiscountGroupDAO()
	{
		addDAOInitializeVisitor(CustomerDiscountGroup.class, new DAOInitializeVisitor<CustomerDiscountGroup>()
			{
				@Override
				public void initialize(CustomerDiscountGroup discountGroup)
				{
					loadCustomerDiscountGroupAssociations(discountGroup);
				}

				private void loadCustomerDiscountGroupAssociations(CustomerDiscountGroup discountGroup)
				{
					loadCustomAssociations(discountGroup.getCustom());
				}

				private void loadCustomAssociations(Map<String, DiscountGroupCustom> custom)
				{
					if (custom != null)
					{
						initializeProxy(custom);
					}
				}
			});
	}
}