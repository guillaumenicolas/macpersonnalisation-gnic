package com.mac.custom.dao;

// Imports
import com.mac.custom.bo.FeeCodeAnalytic;
import com.mac.custom.bo.base.BaseFeeCodeAnalytic;
import com.netappsid.erp.server.dao.ERPDAO;

public class FeeCodeAnalyticDAO<T extends FeeCodeAnalytic> extends ERPDAO<T>
{
	@Override
	public void loadAssociations(Object object)
	{
		logLoadAssociationsCall(object);

		if (object instanceof FeeCodeAnalytic)
		{
			loadCostCodeAssociations((BaseFeeCodeAnalytic) object);
		}
	}

	private void loadCostCodeAssociations(BaseFeeCodeAnalytic feeCodeAnalytic)
	{
		if (feeCodeAnalytic != null)
		{
			initializeProxy(feeCodeAnalytic);
			initializeProxy(feeCodeAnalytic.getDescription());

			if (feeCodeAnalytic.getCompany() != null)
			{
				initializeProxy(feeCodeAnalytic.getCompany());
				initializeProxy(feeCodeAnalytic.getCompany().getDescription());
			}
		}
	}

}
