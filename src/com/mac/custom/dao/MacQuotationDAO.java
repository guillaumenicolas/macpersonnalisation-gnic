package com.mac.custom.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.mac.custom.bo.MacCustomer;
import com.mac.custom.bo.MacQuotation;
import com.mac.custom.bo.MacQuotationDetail;
import com.mac.custom.bo.MacWorkflowMaster;
import com.netappsid.bonitasoft.services.enums.BonitaParameters;
import com.netappsid.bonitasoft.services.services.BonitaServicesBean;
import com.netappsid.bonitasoft.services.services.interfaces.BonitaServices;
import com.netappsid.erp.server.bo.Order;
import com.netappsid.erp.server.bo.PaymentMode;
import com.netappsid.erp.server.bo.Quotation;
import com.netappsid.erp.server.bo.QuotationDetail;
import com.netappsid.erp.server.bo.ServiceType;
import com.netappsid.erp.server.bo.Setup;
import com.netappsid.europe.naid.custom.bo.QuotationDetailEuro;
import com.netappsid.europe.naid.custom.dao.QuotationEuroDAO;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.Loader;

public class MacQuotationDAO<T extends MacQuotation> extends QuotationEuroDAO<T>
{
	private static final Logger logger = Logger.getLogger(MacQuotationDAO.class);

	private final ServiceLocator<BonitaServices> wfServicesLocator = new ServiceLocator<BonitaServices>(BonitaServicesBean.class);
	private final ServiceLocator<Loader> loaderServicesLocator = new ServiceLocator<Loader>(LoaderBean.class);

	private MacWorkflowMaster macWFMaster;

	@Override
	public void loadAssociations(Object object)
	{
		super.loadAssociations(object);

		if (object instanceof MacQuotation)
		{
			loadMacQuotationAssociations((MacQuotation) object);
		}

		if (object instanceof MacCustomer)
		{
			loadMacCustomerAssociations((MacCustomer) object);

		}
	}

	private void loadMacQuotationAssociations(MacQuotation macQuotation)
	{
		if (macQuotation != null)
		{
			initializeProxy(macQuotation);
			loadPaymentModeAssociations(macQuotation.getPaymentMode());
			loadServiceTypeAssociations(macQuotation.getServiceType());
		}
	}

	private void loadMacCustomerAssociations(MacCustomer macCustomer)
	{
		if (macCustomer != null)
		{
			initializeProxy(macCustomer);
			loadPaymentModeAssociations(macCustomer.getPaymentMode());
		}
	}

	private void loadPaymentModeAssociations(PaymentMode paymentMode)
	{
		if (paymentMode != null)
		{
			initializeProxy(paymentMode);
			initializeProxy(paymentMode.getDescription());
		}
	}

	private void loadServiceTypeAssociations(ServiceType serviceType)
	{
		if (serviceType != null)
		{
			initializeProxy(serviceType);
			initializeProxy(serviceType.getDescription());
		}
	}

	@Override
	public T save(T quotation)
	{
		T savedQuotation = super.save(quotation);

		initiateM5PWorkflow(savedQuotation);

		return savedQuotation;
	}

	private Object findQuotationDetailField(Serializable macQuotationDetailId, String quotationDetailProperty)
	{
		Object object = null;
		if (macQuotationDetailId != null && quotationDetailProperty != null)
		{
			final StringBuilder hqlQuery = new StringBuilder();
			hqlQuery.append("SELECT macQuotationDetail." + quotationDetailProperty).append("\n");
			hqlQuery.append("FROM MacQuotationDetail macQuotationDetail").append("\n");
			hqlQuery.append("WHERE macQuotationDetail." + MacQuotationDetail.PROPERTYNAME_ID + " = :macQuotationDetailId ");

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("macQuotationDetailId", macQuotationDetailId.toString());

			object = loaderServicesLocator.get().findByQuery(hqlQuery.toString(), parameters, true);
		}

		return object;
	}

	private boolean initiateM5PWorkflow(Quotation quotation)
	{
		if (getMacWFMaster() == null || getMacWFMaster().getWfM5P() == null || getMacWFMaster().getWfM5P().isActive() == false)
		{
			return false;
		}

		Setup setup = loaderServicesLocator.get().findAll(Setup.class, Setup.class).get(0);

		for (QuotationDetail detail : quotation.getDetails())
		{
			QuotationDetailEuro detailEuro = (QuotationDetailEuro) detail;

			// C'est un M5P
			if (isM5POrderDetail(detailEuro))
			{
				detailEuro.setRareBirdProcessStartDate(new Date());

				Map<String, Object> params = new HashMap<String, Object>();

				// The name of the bean (the object)
				params.put(BonitaParameters.ENTITY_CLASSNAME.toString(), Order.class.getName());

				// The name of the form i.e. the name of the XML without .xml
				params.put(BonitaParameters.ENTITY_FORMNAME.toString(), "order");

				// The uuid of the entity
				params.put(BonitaParameters.ENTITY_ID.toString(), quotation.getId().toString());

				// The reference label
				params.put(BonitaParameters.ENTITY_REFERENCE.toString(), "Devis " + quotation.getDocumentNumber());

				params.put("documentNumber360", "Devis " + quotation.getNumber());
				params.put("emailAddressBE", getMacWFMaster().getWfM5P().getEmailAddressBE());
				params.put("smtpServer360", setup.getMailSmtpHost());
				params.put("serverPort360", setup.getMailSmtpPort());
				params.put("transactionDetailId360", detailEuro.getId().toString());

				try
				{
					wfServicesLocator.get().initiateProcess(com.mac.custom.utils.Utils.getLoginName(), "wfMACM5P", params);
				}
				catch (Exception e)
				{
					logger.error("Probleme lors de l'appel du WF M5p: " + e.getLocalizedMessage());
				}
			}
		}

		return true;
	}

	private boolean isM5POrderDetail(QuotationDetailEuro quotationDetailEuro)
	{
		boolean isM5P = false;

		if (quotationDetailEuro.isRareBird() && (quotationDetailEuro.getRareBirdAccepted() == null || !quotationDetailEuro.getRareBirdAccepted()))
		{
			// nouveau detail de devis
			if (quotationDetailEuro.getId() == null)
			{
				isM5P = true;
			}
			else
			{
				Boolean oldM5P = (Boolean) findQuotationDetailField(quotationDetailEuro.getId(), MacQuotationDetail.PROPERTYNAME_RAREBIRD);

				// detail de devis existant, anciennement pas M5P
				if (oldM5P == null || (oldM5P != null && !oldM5P.booleanValue()))
				{
					isM5P = true;
				}
			}
		}

		return isM5P;
	}

	public MacWorkflowMaster getMacWFMaster()
	{
		if (macWFMaster == null)
		{
			macWFMaster = com.mac.custom.workflow.WorkflowUtils.getWFMaster();
		}
		return macWFMaster;
	}
}
