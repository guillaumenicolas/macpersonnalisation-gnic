package com.mac.custom.dao;

import com.mac.custom.bo.MacCompany;
import com.netappsid.bo.model.Model;
import com.netappsid.dao.DAOInitializeVisitor;
import com.netappsid.erp.server.bo.User;
import com.netappsid.europe.naid.custom.bo.UserEuro;
import com.netappsid.europe.naid.custom.dao.UserEuroDAO;

public class MacUserDAO<T extends UserEuro> extends UserEuroDAO<T>
{
	public MacUserDAO()
	{
		addDAOInitializeVisitor(User.class, new DAOInitializeVisitor<User>()
			{
				@Override
				public void initialize(User user)
				{
					MacCompany company = (MacCompany) Model.getTarget(user.getCompany());

					initializeProxy(company.getDefaultInventoryAdjustmentType());
					if (company.getDefaultInventoryAdjustmentType() != null)
					{
						initializeProxy(company.getDefaultInventoryAdjustmentType().getDescription());
					}
				}
			});
	}
}
