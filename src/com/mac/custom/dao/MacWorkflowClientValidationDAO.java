package com.mac.custom.dao;

import com.mac.custom.bo.MacWorkflowClientValidation;
import com.netappsid.dao.GenericDAO;

public class MacWorkflowClientValidationDAO<T extends MacWorkflowClientValidation> extends GenericDAO<T> 
{
	@Override
	public void loadAssociations(Object object)
	{
		logLoadAssociationsCall(object);
		
		if (object instanceof MacWorkflowClientValidation)
		{
			loadMacWfClientValidationAssociations((MacWorkflowClientValidation) object);
		}
	}

	private void loadMacWfClientValidationAssociations(MacWorkflowClientValidation macWorkflowClientValidation) 
	{
		initializeProxy(macWorkflowClientValidation);
	}
}
