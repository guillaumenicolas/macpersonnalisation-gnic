package com.mac.custom.dao;

import com.mac.custom.bo.MacWorkflowBlockAuto;
import com.netappsid.dao.GenericDAO;

public class MacWorkflowBlockAutoDAO<T extends MacWorkflowBlockAuto> extends GenericDAO<T> 
{
	@Override
	public void loadAssociations(Object object)
	{
		logLoadAssociationsCall(object);
		
		if (object instanceof MacWorkflowBlockAuto)
		{
			loadMacWfBlockAutoAssociations((MacWorkflowBlockAuto) object);
		}
	}

	private void loadMacWfBlockAutoAssociations(MacWorkflowBlockAuto macWorkflowBlockAuto) 
	{
		initializeProxy(macWorkflowBlockAuto);
	}
}
