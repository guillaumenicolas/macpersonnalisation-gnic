package com.mac.custom.dao;

import com.netappsid.erp.server.bo.DocumentStatus;
import com.netappsid.erp.server.bo.Production;
import com.netappsid.erp.server.bo.WorkOrderRoutingStep;
import com.netappsid.erp.server.dao.ProductionDAO;
import com.netappsid.erp.server.services.beans.DocumentStatusServicesBean;
import com.netappsid.erp.server.services.interfaces.DocumentStatusServices;
import com.netappsid.framework.utils.ServiceLocator;

public class MacProductionDAO extends ProductionDAO
{
	private final DocumentStatusServices documentStatusServicesLocator = new ServiceLocator<DocumentStatusServices>(DocumentStatusServicesBean.class).get();

	@Override
	public Object save(Object object)
	{
		Production production = (Production) object;
		WorkOrderRoutingStep workOrderRoutingStep = production.getWorkOrderRoutingStep();
		if (workOrderRoutingStep.isLastWorkOrderRoutingStep() && workOrderRoutingStep.isProduce())
		{
			DocumentStatus closeStatus = documentStatusServicesLocator.getInternalDocumentStatus(DocumentStatus.CLOSE);
			production.getWorkOrderRoutingStep().getWorkOrder().setDocumentStatus(closeStatus);
		}
		return super.save(production);
	}
}
