package com.mac.custom.dao;

// Imports
import java.util.List;

import org.hibernate.criterion.Criterion;

import com.mac.custom.bo.ActiviteClient;
import com.mac.custom.bo.EnseigneClient;
import com.mac.custom.bo.MacCustomer;
import com.mac.custom.bo.MacRegionCommerciale;
import com.mac.custom.listmodel.base.BaseMacCommercialEntityList;
import com.netappsid.erp.server.bo.Address;
import com.netappsid.erp.server.bo.City;
import com.netappsid.erp.server.bo.CommercialClass;
import com.netappsid.erp.server.bo.Country;
import com.netappsid.erp.server.dao.ERPDAO;
import com.netappsid.exceptions.InfrastructureException;
import com.netappsid.resources.Translator;

public class MacCommercialEntityListDAO<T extends BaseMacCommercialEntityList> extends ERPDAO<T>
{
	@Override
	public BaseMacCommercialEntityList save(BaseMacCommercialEntityList order)
	{
		throw new IllegalStateException(Translator.getString("IllegalOperation"));
	}

	@Override
	public void delete(BaseMacCommercialEntityList object)
	{
		throw new IllegalStateException(Translator.getString("IllegalOperation"));
	}

	@Override
	public List<T> findAll()
	{
		throw new IllegalStateException(Translator.getString("IllegalOperation"));
	}

	@Override
	public List<T> findByCriteria(List<Criterion> criterionList, List<String> subcriterionList, List<Integer> subcriterionJoinSpecList)
	{
		throw new IllegalStateException(Translator.getString("IllegalOperation"));
	}

	@Override
	public List<T> findByCriteria(List<Criterion> criterionList, List<String> subcriterionList, List<Integer> subcriterionJoinSpecList, int maxResults)
	{
		throw new IllegalStateException(Translator.getString("IllegalOperation"));
	}

	@Override
	public List<T> findByExample(BaseMacCommercialEntityList exampleItem) throws InfrastructureException
	{
		throw new IllegalStateException(Translator.getString("IllegalOperation"));
	}

	@Override
	public List<T> findByField(Object fieldName, Object fieldValue)
	{
		throw new IllegalStateException(Translator.getString("IllegalOperation"));
	}

	@Override
	public List<T> findByQuery(String query, List<String> parameterNames, List<Object> parameterValues, int maxResults)
	{
		throw new IllegalStateException(Translator.getString("IllegalOperation"));
	}

	@Override
	public void loadAssociations(Object object)
	{
		logLoadAssociationsCall(object);

		if (object instanceof MacCustomer)
		{
			loadMacCustomerAssociations((MacCustomer) object);
		}
		if (object instanceof Address)
		{
			loadMacAddressAssociations((Address) object);
		}
		if (object instanceof EnseigneClient)
		{
			loadEnseigneClientAssociations((EnseigneClient) object);
		}
		if (object instanceof ActiviteClient)
		{
			loadActiviteClientAssociations((ActiviteClient) object);
		}
		if (object instanceof CommercialClass)
		{
			loadTypologieAssociations((CommercialClass) object);
		}
		if (object instanceof MacRegionCommerciale)
		{
			loadMacRegionCommercialeAssociations((MacRegionCommerciale) object);
		}
		if (object instanceof City)
		{
			loadCityAssociations((City) object);
		}
		if (object instanceof Country)
		{
			loadCountryAssociations((Country) object);
		}
	}

	private void loadMacRegionCommercialeAssociations(MacRegionCommerciale macRegionCommerciale)
	{
		if (macRegionCommerciale != null)
		{
			initializeProxy(macRegionCommerciale);
			initializeProxy(macRegionCommerciale.getDescription());
		}
	}

	private void loadMacCustomerAssociations(MacCustomer macCustomer)
	{
		if (macCustomer != null)
		{
			initializeProxy(macCustomer);
		}
	}

	private void loadCityAssociations(City city)
	{
		if (city != null)
		{
			initializeProxy(city);
			initializeProxy(city.getName());
		}
	}

	private void loadCountryAssociations(Country country)
	{
		if (country != null)
		{
			initializeProxy(country);
			initializeProxy(country.getName());
		}
	}

	private void loadEnseigneClientAssociations(EnseigneClient enseigneClient)
	{
		if (enseigneClient != null)
		{
			initializeProxy(enseigneClient);
			initializeProxy(enseigneClient.getDescription());
		}
	}

	private void loadTypologieAssociations(CommercialClass commercialClass)
	{
		if (commercialClass != null)
		{
			initializeProxy(commercialClass);
			initializeProxy(commercialClass.getDescription());
		}
	}

	private void loadActiviteClientAssociations(ActiviteClient activiteClient)
	{
		if (activiteClient != null)
		{
			initializeProxy(activiteClient);
			initializeProxy(activiteClient.getDescription());
		}
	}

	private void loadMacAddressAssociations(Address address)
	{
		if (address != null)
		{
			initializeProxy(address);
		}
	}

}