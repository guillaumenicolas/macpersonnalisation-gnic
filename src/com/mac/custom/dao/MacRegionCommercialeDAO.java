package com.mac.custom.dao;

import com.mac.custom.bo.MacRegionCommerciale;
import com.netappsid.dao.GenericDAO;

public class MacRegionCommercialeDAO <T extends MacRegionCommerciale> extends GenericDAO<T>
{
	@Override
	public void loadAssociations(Object object)
	{
		logLoadAssociationsCall(object);

		if (object instanceof MacRegionCommerciale)
		{
			loadMacRegionCommercialeAssociations((MacRegionCommerciale) object);
		}
	}

	private void loadMacRegionCommercialeAssociations(MacRegionCommerciale macRegionCommerciale)
	{
		if (macRegionCommerciale != null)
		{
			initializeProxy(macRegionCommerciale);
			initializeProxy(macRegionCommerciale.getDescription());
		}
	}
}
