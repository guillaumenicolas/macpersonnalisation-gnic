package com.mac.custom.dao;

import com.mac.custom.bo.MacWorkflowM5P;
import com.netappsid.dao.GenericDAO;

public class MacWorkflowM5PDAO<T extends MacWorkflowM5P> extends GenericDAO<T> 
{
	@Override
	public void loadAssociations(Object object)
	{
		logLoadAssociationsCall(object);
		
		if (object instanceof MacWorkflowM5P)
		{
			loadMacWfARCAssociations((MacWorkflowM5P) object);
		}
	}

	private void loadMacWfARCAssociations(MacWorkflowM5P macWorkflowM5P) 
	{
		initializeProxy(macWorkflowM5P);
	}
}
