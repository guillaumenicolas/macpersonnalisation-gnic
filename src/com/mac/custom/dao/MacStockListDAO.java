package com.mac.custom.dao;

import com.mac.custom.listmodel.MacStockList;
import com.netappsid.bo.model.Model;
import com.netappsid.erp.server.bo.Location;
import com.netappsid.erp.server.dao.StockListDAO;

public class MacStockListDAO extends StockListDAO<MacStockList>
{
	@Override
	public void loadAssociations(Object object)
	{
		super.loadAssociations(object);
		object = Model.getTarget(object);

		if (object instanceof Location)
		{
			loadLocationAssociations((Location) object);
		}
	}

	private void loadLocationAssociations(Location object)
	{
		if (object != null)
		{
			initializeProxy(object);
			for (Location location : object.getSubLocations())
			{
				loadLocationAssociations(location);
			}
		}
	}
}
