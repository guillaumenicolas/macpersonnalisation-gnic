package com.mac.custom.dao;

import com.mac.custom.bo.MacLocation;
import com.mac.custom.bo.MacShippingLocation;
import com.mac.custom.bo.MacWarehouse;
import com.netappsid.dao.DAOInitializeVisitor;
import com.netappsid.erp.server.dao.WarehouseDAO;

public class MacWarehouseDAO extends WarehouseDAO<MacWarehouse>
{
	public MacWarehouseDAO()
	{
		addDAOInitializeVisitor(MacWarehouse.class, new DAOInitializeVisitor<MacWarehouse>()
			{
				@Override
				public void initialize(MacWarehouse toInitialize)
				{
					for (MacShippingLocation loc : toInitialize.getShippingLocations())
					{
						initializeProxy(loc);
						MacLocation location = loc.getLocation();
						loadMacLocationAssociations(location);
					}
				}
			});
	}

	@Override
	public void loadAssociations(Object object)
	{
		super.loadAssociations(object);

		if (object instanceof MacLocation)
		{
			loadMacLocationAssociations((MacLocation) object);
		}
	}

	private void loadMacLocationAssociations(MacLocation macLocation)
	{
		if (macLocation != null)
		{
			initializeProxy(macLocation);
			initializeProxy(macLocation.getDescription());
			initializeProxy(macLocation.getSubLocations());
		}
	}
}
