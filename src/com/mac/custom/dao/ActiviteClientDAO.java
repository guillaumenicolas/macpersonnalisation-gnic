package com.mac.custom.dao;

// Imports
import com.mac.custom.bo.ActiviteClient;
import com.mac.custom.bo.base.BaseActiviteClient;
import com.netappsid.dao.GenericDAO;

public class ActiviteClientDAO<T extends ActiviteClient> extends GenericDAO<T>
{
	@Override
	public void loadAssociations(Object object)
	{
		logLoadAssociationsCall(object);

		if (object instanceof ActiviteClient)
		{
			loadActiviteClientAssociations((BaseActiviteClient) object);
		}
	}

	private void loadActiviteClientAssociations(BaseActiviteClient activiteclient)
	{
		if (activiteclient != null)
		{
			initializeProxy(activiteclient);
			initializeProxy(activiteclient.getDescription());
		}
	}

}
