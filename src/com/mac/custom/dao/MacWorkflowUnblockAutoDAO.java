package com.mac.custom.dao;

import com.mac.custom.bo.MacWorkflowUnblockAuto;
import com.netappsid.dao.GenericDAO;

public class MacWorkflowUnblockAutoDAO<T extends MacWorkflowUnblockAuto> extends GenericDAO<T> 
{
	@Override
	public void loadAssociations(Object object)
	{
		logLoadAssociationsCall(object);
		
		if (object instanceof MacWorkflowUnblockAuto)
		{
			loadMacWfUnblockAutoAssociations((MacWorkflowUnblockAuto) object);
		}
	}

	private void loadMacWfUnblockAutoAssociations(MacWorkflowUnblockAuto macWorkflowUnblockAuto) 
	{
		initializeProxy(macWorkflowUnblockAuto);
	}
}
