package com.mac.custom.dao;

import java.util.Map;

import com.netappsid.dao.DAOInitializeVisitor;
import com.netappsid.erp.server.bo.DiscountGroup;
import com.netappsid.erp.server.bo.extension.DiscountGroupCustom;
import com.netappsid.erp.server.dao.DiscountGroupDAO;

public class MacDiscountGroupDAO<T extends DiscountGroup> extends DiscountGroupDAO<T>
{
	public MacDiscountGroupDAO()
	{
		addDAOInitializeVisitor(DiscountGroup.class, new DAOInitializeVisitor<DiscountGroup>()
			{
				@Override
				public void initialize(DiscountGroup discountGroup)
				{
					loadDiscountGroupAssociations(discountGroup);
				}

				private void loadDiscountGroupAssociations(DiscountGroup discountGroup)
				{
					loadCustomAssociations(discountGroup.getCustom());
				}

				private void loadCustomAssociations(Map<String, DiscountGroupCustom> custom)
				{
					if (custom != null)
					{
						initializeProxy(custom);
					}
				}
			});
	}
}