package com.mac.custom.dao;

// Imports
import com.mac.custom.bo.ProductCodeAnalytic;
import com.mac.custom.bo.base.BaseProductCodeAnalytic;
import com.netappsid.erp.server.dao.ERPDAO;

public class ProductCodeAnalyticDAO<T extends ProductCodeAnalytic> extends ERPDAO<T>
{
	@Override
	public void loadAssociations(Object object)
	{
		logLoadAssociationsCall(object);

		if (object instanceof ProductCodeAnalytic)
		{
			loadCostCodeAssociations((BaseProductCodeAnalytic) object);
		}
	}

	private void loadCostCodeAssociations(BaseProductCodeAnalytic productCodeAnalytic)
	{
		if (productCodeAnalytic != null)
		{
			initializeProxy(productCodeAnalytic);
			initializeProxy(productCodeAnalytic.getDescription());

			if (productCodeAnalytic.getCompany() != null)
			{
				initializeProxy(productCodeAnalytic.getCompany());
				initializeProxy(productCodeAnalytic.getCompany().getDescription());
			}
		}
	}

}
