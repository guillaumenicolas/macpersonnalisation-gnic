package com.mac.custom.dao;

import com.mac.custom.bo.MacTerm;
import com.netappsid.erp.server.bo.PaymentMode;
import com.netappsid.erp.server.dao.TermDAO;

public class MacTermDAO<T extends MacTerm> extends TermDAO<T>
{
	@Override
	public void loadAssociations(Object object)
	{
		super.loadAssociations(object);

		if (object instanceof MacTerm)
		{
			loadMacTermAssociations((MacTerm) object);
		}
	}

	private void loadMacTermAssociations(MacTerm macTerm)
	{
		if (macTerm != null)
		{
			initializeProxy(macTerm);
			loadPaymentModeAssociations(macTerm.getPaymentMode());
		}
	}

	private void loadPaymentModeAssociations(PaymentMode paymentMode)
	{
		if (paymentMode != null)
		{
			initializeProxy(paymentMode);
			initializeProxy(paymentMode.getDescription());
		}
	}
}
