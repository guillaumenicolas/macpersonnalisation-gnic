package com.mac.custom.dao;

import com.mac.custom.bo.PurchaseRequisitionRepartition;
import com.netappsid.dao.GenericDAO;
import com.netappsid.erp.server.bo.PurchaseRequisition;
import com.netappsid.erp.server.bo.Supplier;
import com.netappsid.erp.server.dao.utils.GroupMeasureDAOUtils;

public class PurchaseRequisitionRepartitionDAO extends GenericDAO<PurchaseRequisitionRepartition>
{
	@Override
	public void loadAssociations(Object object)
	{
		if (object instanceof PurchaseRequisition)
		{
			loadPurchaseRequisitionAssociations(((PurchaseRequisition) object));
		}
		else if (object instanceof Supplier)
		{
			loadSupplierAssociations(((Supplier) object));
		}
	}

	private void loadPurchaseRequisitionAssociations(PurchaseRequisition purchaseRequisition)
	{
		initializeProxy(purchaseRequisition);
		initializeProxy(purchaseRequisition.getProduct());
		initializeProxy(purchaseRequisition.getProduct().getDescription());
		GroupMeasureDAOUtils.loadGroupMeasureAssociations(purchaseRequisition.getQuantity().getGroupMeasure());
	}

	private void loadSupplierAssociations(Supplier supplier)
	{
		initializeProxy(supplier);
		initializeProxy(supplier.getName());
	}
}