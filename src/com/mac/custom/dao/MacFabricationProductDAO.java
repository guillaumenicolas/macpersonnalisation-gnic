package com.mac.custom.dao;

import org.apache.log4j.Logger;

import com.mac.custom.bo.MacManufacturedProduct;
import com.mac.custom.bo.MacMaterialProduct;
import com.mac.custom.bo.ManufacturedProductManagementAccounting;
import com.mac.custom.bo.MaterialProductManagementAccounting;
import com.mac.custom.bo.base.BaseProductManagementAccounting;
import com.netappsid.erp.server.bo.FabricationProduct;

public class MacFabricationProductDAO<T extends FabricationProduct> extends com.netappsid.erp.server.dao.FabricationProductDAO<T>
{
	private static final Logger logger = Logger.getLogger(MacFabricationProductDAO.class);

	@Override
	public void loadAssociations(Object object)
	{
		super.loadAssociations(object);

		if (object instanceof MacMaterialProduct)
		{
			loadMacMaterialProductAssociations((MacMaterialProduct) object);
		}
		else if (object instanceof MacManufacturedProduct)
		{
			loadMacManufactedProductAssociations((MacManufacturedProduct) object);
		}
	}

	private void loadMacMaterialProductAssociations(MacMaterialProduct macMaterialProduct)
	{
		if (macMaterialProduct != null)
		{
			initializeProxy(macMaterialProduct);

			for (MaterialProductManagementAccounting managementAccounting : macMaterialProduct.getManagementAccountings())
			{
				loadManagementAccountingAssociation(managementAccounting);
			}
		}
	}

	private void loadMacManufactedProductAssociations(MacManufacturedProduct macManufacturedProduct)
	{
		if (macManufacturedProduct != null)
		{
			initializeProxy(macManufacturedProduct);

			for (ManufacturedProductManagementAccounting managementAccounting : macManufacturedProduct.getManagementAccountings())
			{
				loadManagementAccountingAssociation(managementAccounting);
			}
		}
	}

	private void loadManagementAccountingAssociation(BaseProductManagementAccounting managementAccounting)
	{
		if (managementAccounting != null)
		{
			initializeProxy(managementAccounting);

			if (managementAccounting.getCompany() != null)
			{
				initializeProxy(managementAccounting.getCompany());
				initializeProxy(managementAccounting.getCompany().getDescription());
			}

			if (managementAccounting.getCostCodeSale() != null)
			{
				initializeProxy(managementAccounting.getCostCodeSale());
				initializeProxy(managementAccounting.getCostCodeSale().getDescription());
			}

			if (managementAccounting.getCostCodeDetailSale() != null)
			{
				initializeProxy(managementAccounting.getCostCodeDetailSale());
				initializeProxy(managementAccounting.getCostCodeDetailSale().getDescription());
			}

			if (managementAccounting.getProductCodeAnalyticSale() != null)
			{
				initializeProxy(managementAccounting.getProductCodeAnalyticSale());
				initializeProxy(managementAccounting.getProductCodeAnalyticSale().getDescription());
			}

			if (managementAccounting.getCostCodePurchase() != null)
			{
				initializeProxy(managementAccounting.getCostCodePurchase());
				initializeProxy(managementAccounting.getCostCodePurchase().getDescription());
			}

			if (managementAccounting.getCostCodeDetailPurchase() != null)
			{
				initializeProxy(managementAccounting.getCostCodeDetailPurchase());
				initializeProxy(managementAccounting.getCostCodeDetailPurchase().getDescription());
			}

			if (managementAccounting.getProductCodeAnalyticPurchase() != null)
			{
				initializeProxy(managementAccounting.getProductCodeAnalyticPurchase());
				initializeProxy(managementAccounting.getProductCodeAnalyticPurchase().getDescription());
			}
		}
	}
}
