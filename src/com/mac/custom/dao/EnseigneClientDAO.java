package com.mac.custom.dao;

// Imports
import com.mac.custom.bo.EnseigneClient;
import com.netappsid.dao.GenericDAO;

public class EnseigneClientDAO<T extends EnseigneClient> extends GenericDAO<T>
{
	@Override
	public void loadAssociations(Object object)
	{
		logLoadAssociationsCall(object);

		if (object instanceof EnseigneClient)
		{
			loadEnseigneAssociations((EnseigneClient) object);
		}
	}

	private void loadEnseigneAssociations(EnseigneClient enseigneclient)
	{
		if (enseigneclient != null)
		{
			initializeProxy(enseigneclient);
			initializeProxy(enseigneclient.getDescription());
		}
	}

}
