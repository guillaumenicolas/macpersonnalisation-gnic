package com.mac.custom.dao;

import com.mac.custom.listmodel.WorkCalendar;
import com.netappsid.dao.GenericDAO;

public class WorkCalendarDAO<T extends WorkCalendar> extends GenericDAO<T>
{
	@Override
	public void loadAssociations(Object object)
	{
		logLoadAssociationsCall(object);
	}

}
