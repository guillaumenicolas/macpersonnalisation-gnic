package com.mac.custom.listmodel;

import java.util.ArrayList;
import java.util.List;

import com.mac.custom.bo.MacProductionBatch;
import com.netappsid.bo.model.Model;
import com.netappsid.erp.server.bo.ProductionBatch;
import com.netappsid.erp.server.listmodel.ProductionBatchList;

public class MacProductionBatchList extends ProductionBatchList
{
	@Override
	public Class<?> getListType()
	{
		return MacProductionBatch.class;
	}

	@Override
	public void afterRefresh()
	{
		super.afterRefresh();

		removeAllFromProductionBatches();

		List<ProductionBatch> productionBatches = new ArrayList<ProductionBatch>();

		for (Model model : getItems())
		{
			productionBatches.add(new MacProductionBatch((ProductionBatch) model));
		}

		addManyToProductionBatches(productionBatches);
	}
}
