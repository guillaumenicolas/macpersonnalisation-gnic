package com.mac.custom.listmodel;

// Imports
import javax.persistence.Table;

import org.hibernate.criterion.Restrictions;

import com.mac.custom.bo.MacQuotation;
import com.netappsid.annotations.DAO;
import com.netappsid.application.ApplicationInjector;
import com.netappsid.erp.server.dao.OrderListDAO;
import com.netappsid.europe.naid.custom.listmodel.QuotationListEuro;
import com.netappsid.framework.security.SecurityHelper;

@Table(name = "MacOrderList")
@DAO(dao = OrderListDAO.class)
public class MacQuotationList extends QuotationListEuro
{
	private Boolean devisEnvoyeCommerciaux;

	public MacQuotationList()
	{
		this(ApplicationInjector.INSTANCE.getInstance(SecurityHelper.class));
		setDevisEnvoyeCommerciaux(null);
	}

	public MacQuotationList(SecurityHelper securityHelper)
	{
		super(securityHelper);
	}

	// Gestion du critere de recherche "Mode Date Expedition specifique"
	public void setDevisEnvoyeCommerciaux(Boolean devisEnvoyeCommerciaux)
	{
		Boolean oldValue = getDevisEnvoyeCommerciaux();
		this.devisEnvoyeCommerciaux = devisEnvoyeCommerciaux;
		firePropertyChange(PROPERTYNAME_DEVISENVOYECOMMERCIAUX, oldValue, devisEnvoyeCommerciaux);
	}

	public Boolean getDevisEnvoyeCommerciaux()
	{
		return devisEnvoyeCommerciaux;
	}

	@Override
	public void buildCriteria()
	{
		super.buildCriteria();

		if (getDevisEnvoyeCommerciaux() != null)
		{
			getCriteria().add(Restrictions.eq(MacQuotation.PROPERTYNAME_SENTTOCOMMERCIAL, getDevisEnvoyeCommerciaux()));
			getSubcriteria().add(null);
			getJoinsSpec().add(null);
		}
	}

	@Override
	public void clear()
	{
		super.clear();
		setDevisEnvoyeCommerciaux(null);
	}

	@Override
	public Class<?> getListType()
	{
		return MacQuotation.class;
	}

	public static final String PROPERTYNAME_DEVISENVOYECOMMERCIAUX = "devisEnvoyeCommerciaux";
}
