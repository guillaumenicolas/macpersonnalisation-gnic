package com.mac.custom.listmodel;

import java.util.Date;

import com.netappsid.erp.server.bo.ProductionPlanning;
import com.netappsid.erp.server.listmodel.WorkOrderRoutingStepList;

public class MacProductionPlanning extends ProductionPlanning
{
	@Override
	protected WorkOrderRoutingStepList createWorkOrderRoutingStepList()
	{
		return new MacWorkOrderRoutingStepList();
	}

	public MacWorkOrderRoutingStepList getMacWorkOrderRoutingStepList()
	{
		return (MacWorkOrderRoutingStepList) super.getWorkOrderRoutingStepList();
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		setWorkOrderRoutingStepList(new MacWorkOrderRoutingStepList());
		getWorkOrderRoutingStepList().setStartDateFrom(new Date());
		getWorkOrderRoutingStepList().setEndDateTo(new Date());
	}
}
