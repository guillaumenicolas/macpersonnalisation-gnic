package com.mac.custom.listmodel;

import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.mac.custom.bo.MacCustomer;
import com.mac.custom.dao.MacCommercialEntityListDAO;
import com.mac.custom.listmodel.base.BaseMacCommercialEntityList;
import com.netappsid.annotations.DAO;
import com.netappsid.erp.server.bo.Address;
import com.netappsid.erp.server.bo.AddressType;
import com.netappsid.erp.server.bo.AddressType.Type;
import com.netappsid.erp.server.bo.City;
import com.netappsid.erp.server.bo.CommercialEntity;
import com.netappsid.erp.server.bo.CommercialEntityAddress;
import com.netappsid.erp.server.bo.CommercialEntityDefaultAddress;
import com.netappsid.erp.server.bo.Country;
import com.netappsid.erp.server.bo.Customer;
import com.netappsid.erp.server.bo.Region;
import com.netappsid.erp.server.bo.State;
import com.netappsid.europe.naid.custom.bo.SupplierEuro;

@DAO(dao = MacCommercialEntityListDAO.class)
public class MacCommercialEntityList extends BaseMacCommercialEntityList
{

	@Override
	public void buildCriteria()
	{
		super.buildCriteria();

		getCriteria().add(Restrictions.eq(CommercialEntity.PROPERTYNAME_ACTIVE, isActive()));
		getSubcriteria().add(null);
		getJoinsSpec().add(null);

		if (getCode() != null)
		{
			getCriteria().add(Restrictions.ilike(CommercialEntity.PROPERTYNAME_CODE, getCode()));
			getSubcriteria().add(null);
			getJoinsSpec().add(null);
		}
		if (getSiren() != null)
		{
			if (getClass().isAssignableFrom(MacCommercialEntityList.class))
			{
				getCriteria().add(Restrictions.ilike(SupplierEuro.PROPERTYNAME_SIREN, getSiren()));
			}
			else if (getClass().isAssignableFrom(MacCustomerList.class))
			{
				getCriteria().add(Restrictions.ilike(MacCustomer.PROPERTYNAME_SIREN, getSiren()));
			}
			getSubcriteria().add(null);
			getJoinsSpec().add(null);
		}

		if (getHeadQuarterAddressZipCode() != null)
		{
			getCriteria().add(Restrictions.ilike(Address.PROPERTYNAME_ZIPCODE, getHeadQuarterAddressZipCode()));
			getSubcriteria().add(
					CommercialEntity.PROPERTYNAME_DEFAULTADDRESSES + "." + CommercialEntityDefaultAddress.PROPERTYNAME_COMMERCIALENTITYADDRESS + "."
							+ CommercialEntityAddress.PROPERTYNAME_ADDRESS);
			getJoinsSpec().add(CriteriaSpecification.LEFT_JOIN);

			getCriteria().add(Restrictions.eq(AddressType.PROPERTYNAME_INTERNALID, Type.HEADQUARTERS.getInternalId()));
			getSubcriteria().add(Customer.PROPERTYNAME_DEFAULTADDRESSES + "." + CommercialEntityDefaultAddress.PROPERTYNAME_ADDRESSTYPE);
			getJoinsSpec().add(CriteriaSpecification.INNER_JOIN);
		}

		if (getHeadQuarterAddressCity() != null)
		{
			getCriteria().add(Restrictions.eq(Address.PROPERTYNAME_CITY, getHeadQuarterAddressCity()));
			getSubcriteria().add(
					CommercialEntity.PROPERTYNAME_DEFAULTADDRESSES + "." + CommercialEntityDefaultAddress.PROPERTYNAME_COMMERCIALENTITYADDRESS + "."
							+ CommercialEntityAddress.PROPERTYNAME_ADDRESS);
			getJoinsSpec().add(CriteriaSpecification.LEFT_JOIN);

			getCriteria().add(Restrictions.eq(AddressType.PROPERTYNAME_INTERNALID, Type.HEADQUARTERS.getInternalId()));
			getSubcriteria().add(Customer.PROPERTYNAME_DEFAULTADDRESSES + "." + CommercialEntityDefaultAddress.PROPERTYNAME_ADDRESSTYPE);
			getJoinsSpec().add(CriteriaSpecification.INNER_JOIN);
		}

		if (getPaysEtranger() != null)
		{
			Criterion criterePaysEtranger = null;
			if (getPaysEtranger())
			{
				criterePaysEtranger = Restrictions.and(
						Restrictions.ne(Address.PROPERTYNAME_CITY + "." + City.PROPERTYNAME_REGION + "." + Region.PROPERTYNAME_STATE + "."
								+ State.PROPERTYNAME_COUNTRY + "." + Country.PROPERTYNAME_CODE, "FR"),
						Restrictions.ne(Address.PROPERTYNAME_CITY + "." + City.PROPERTYNAME_REGION + "." + Region.PROPERTYNAME_STATE + "."
								+ State.PROPERTYNAME_COUNTRY + "." + Country.PROPERTYNAME_CODE, "WW"));
			}
			else
			{
				criterePaysEtranger = Restrictions.or(
						Restrictions.eq(Address.PROPERTYNAME_CITY + "." + City.PROPERTYNAME_REGION + "." + Region.PROPERTYNAME_STATE + "."
								+ State.PROPERTYNAME_COUNTRY + "." + Country.PROPERTYNAME_CODE, "FR"),
						Restrictions.eq(Address.PROPERTYNAME_CITY + "." + City.PROPERTYNAME_REGION + "." + Region.PROPERTYNAME_STATE + "."
								+ State.PROPERTYNAME_COUNTRY + "." + Country.PROPERTYNAME_CODE, "WW"));
			}
			getCriteria().add(criterePaysEtranger);
			getSubcriteria().add(
					CommercialEntity.PROPERTYNAME_DEFAULTADDRESSES + "." + CommercialEntityDefaultAddress.PROPERTYNAME_COMMERCIALENTITYADDRESS + "."
							+ CommercialEntityAddress.PROPERTYNAME_ADDRESS);
			getJoinsSpec().add(CriteriaSpecification.LEFT_JOIN);

			getCriteria().add(Restrictions.eq(AddressType.PROPERTYNAME_INTERNALID, Type.HEADQUARTERS.getInternalId()));
			getSubcriteria().add(Customer.PROPERTYNAME_DEFAULTADDRESSES + "." + CommercialEntityDefaultAddress.PROPERTYNAME_ADDRESSTYPE);
			getJoinsSpec().add(CriteriaSpecification.INNER_JOIN);
		}

		if (getHeadQuarterAddressCountry() != null)
		{
			Criterion criterePays = Restrictions.eq(Address.PROPERTYNAME_CITY + "." + City.PROPERTYNAME_REGION + "." + Region.PROPERTYNAME_STATE + "."
					+ State.PROPERTYNAME_COUNTRY, getHeadQuarterAddressCountry());

			getCriteria().add(criterePays);
			getSubcriteria().add(
					CommercialEntity.PROPERTYNAME_DEFAULTADDRESSES + "." + CommercialEntityDefaultAddress.PROPERTYNAME_COMMERCIALENTITYADDRESS + "."
							+ CommercialEntityAddress.PROPERTYNAME_ADDRESS);
			getJoinsSpec().add(CriteriaSpecification.LEFT_JOIN);

			getCriteria().add(Restrictions.eq(AddressType.PROPERTYNAME_INTERNALID, Type.HEADQUARTERS.getInternalId()));
			getSubcriteria().add(Customer.PROPERTYNAME_DEFAULTADDRESSES + "." + CommercialEntityDefaultAddress.PROPERTYNAME_ADDRESSTYPE);
			getJoinsSpec().add(CriteriaSpecification.INNER_JOIN);
		}

		if (getHeadQuarterPhone() != null && !getHeadQuarterPhone().isEmpty())
		{
			getCriteria().add(Restrictions.ilike(Address.PROPERTYNAME_PHONE, getHeadQuarterPhone()));
			getSubcriteria().add(
					CommercialEntity.PROPERTYNAME_DEFAULTADDRESSES + "." + CommercialEntityDefaultAddress.PROPERTYNAME_COMMERCIALENTITYADDRESS + "."
							+ CommercialEntityAddress.PROPERTYNAME_ADDRESS);
			getJoinsSpec().add(CriteriaSpecification.LEFT_JOIN);

			getCriteria().add(Restrictions.eq(AddressType.PROPERTYNAME_INTERNALID, Type.HEADQUARTERS.getInternalId()));
			getSubcriteria().add(Customer.PROPERTYNAME_DEFAULTADDRESSES + "." + CommercialEntityDefaultAddress.PROPERTYNAME_ADDRESSTYPE);
			getJoinsSpec().add(CriteriaSpecification.INNER_JOIN);
		}
		if (getHeadQuarterFax() != null && !getHeadQuarterFax().isEmpty())
		{
			getCriteria().add(Restrictions.ilike(Address.PROPERTYNAME_PHONEOTHER, getHeadQuarterFax()));
			getSubcriteria().add(
					CommercialEntity.PROPERTYNAME_DEFAULTADDRESSES + "." + CommercialEntityDefaultAddress.PROPERTYNAME_COMMERCIALENTITYADDRESS + "."
							+ CommercialEntityAddress.PROPERTYNAME_ADDRESS);
			getJoinsSpec().add(CriteriaSpecification.LEFT_JOIN);

			getCriteria().add(Restrictions.eq(AddressType.PROPERTYNAME_INTERNALID, Type.HEADQUARTERS.getInternalId()));
			getSubcriteria().add(Customer.PROPERTYNAME_DEFAULTADDRESSES + "." + CommercialEntityDefaultAddress.PROPERTYNAME_ADDRESSTYPE);
			getJoinsSpec().add(CriteriaSpecification.INNER_JOIN);
		}
	}

	@Override
	public void clear()
	{
		super.clear();

		setActive(true);
		setCode(null);
		setHeadQuarterAddressZipCode(null);
		setHeadQuarterAddressCity(null);
		setHeadQuarterAddressCountry(null);
		setHeadQuarterPhone(null);
		setHeadQuarterFax(null);
		setSiren(null);
		setTypologie(null);
		setPaysEtranger(null);
	}

	@Override
	public Class<?> getListType()
	{
		return SupplierEuro.class;
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
		setActive(true);
	}
}
