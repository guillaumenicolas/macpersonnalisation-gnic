package com.mac.custom.listmodel;

import com.mac.custom.bo.MacReceivingDetail;
import com.netappsid.erp.server.listmodel.ReceivingDetailList;

public class MacReceivingDetailList extends ReceivingDetailList
{
	@Override
	public Class<?> getListType()
	{
		return MacReceivingDetail.class;
	}

}
