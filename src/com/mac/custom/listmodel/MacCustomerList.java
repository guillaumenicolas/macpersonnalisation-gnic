package com.mac.custom.listmodel;

import org.hibernate.criterion.Restrictions;

import com.mac.custom.bo.MacCustomer;
import com.mac.custom.listmodel.base.BaseMacCustomerList;
import com.netappsid.erp.server.bo.CustomerSalesRep;
import com.netappsid.europe.naid.custom.bo.CustomerSalesRepEuro;
import com.netappsid.europe.naid.custom.bo.SalesRepEuro;

public class MacCustomerList extends BaseMacCustomerList
{
	@Override
	public void buildCriteria()
	{
		super.buildCriteria();

		if (getSalesRep() != null)
		{
			getCriteria().add(Restrictions.eq(MacCustomer.PROPERTYNAME_SALESREPS + "." + CustomerSalesRep.PROPERTYNAME_SALESREP, getSalesRep()));
			getSubcriteria().add(null);
			getJoinsSpec().add(null);
		}
		if (getSalesRepManager() != null)
		{
			getCriteria().add(
					Restrictions.eq(MacCustomer.PROPERTYNAME_SALESREPS + "." + CustomerSalesRep.PROPERTYNAME_SALESREP + "."
							+ SalesRepEuro.PROPERTYNAME_SALESMANAGER, getSalesRepManager()));
			getSubcriteria().add(null);
			getJoinsSpec().add(null);
		}
		if (getRegionCommerciale() != null)
		{
			getCriteria().add(Restrictions.eq(MacCustomer.PROPERTYNAME_MACREGIONCOMMERCIALE, getRegionCommerciale()));
			getSubcriteria().add(null);
			getJoinsSpec().add(null);
		}
		if (getAdv() != null)
		{
			getCriteria().add(Restrictions.eq(MacCustomer.PROPERTYNAME_SALESREPS + "." + CustomerSalesRepEuro.PROPERTYNAME_ADVUSER, getAdv()));
			getSubcriteria().add(null);
			getJoinsSpec().add(null);
		}
		if (getActivite() != null)
		{
			getCriteria().add(Restrictions.eq(MacCustomer.PROPERTYNAME_ACTIVITECLIENT, getActivite()));
			getSubcriteria().add(null);
			getJoinsSpec().add(null);
		}
		if (getEnseigne() != null)
		{
			getCriteria().add(Restrictions.eq(MacCustomer.PROPERTYNAME_ENSEIGNECLIENT, getEnseigne()));
			getSubcriteria().add(null);
			getJoinsSpec().add(null);
		}
		if (getTypologie() != null)
		{
			getCriteria().add(Restrictions.eq(MacCustomer.PROPERTYNAME_CUSTOMERCLASS, getTypologie()));
			getSubcriteria().add(null);
			getJoinsSpec().add(null);
		}

		if (getBlockProductionEffective() != null)
		{
			getCriteria().add(Restrictions.eq(MacCustomer.PROPERTYNAME_BLOCKPRODUCTION, getBlockProductionEffective()));
			getSubcriteria().add(null);
			getJoinsSpec().add(null);
		}
		if (getBlockShippingEffective() != null)
		{
			getCriteria().add(Restrictions.eq(MacCustomer.PROPERTYNAME_BLOCKSHIPPING, getBlockShippingEffective()));
			getSubcriteria().add(null);
			getJoinsSpec().add(null);
		}
		if (getBlockSalesEffective() != null)
		{
			getCriteria().add(Restrictions.eq(MacCustomer.PROPERTYNAME_BLOCKSALE, getBlockSalesEffective()));
			getSubcriteria().add(null);
			getJoinsSpec().add(null);
		}

	}

	@Override
	public void clear()
	{
		super.clear();
		setSalesRep(null);
		setSalesRepManager(null);
		setRegionCommerciale(null);
		setAdv(null);
		setActivite(null);
		setEnseigne(null);
	}

	@Override
	public Class<?> getListType()
	{
		return MacCustomer.class;
	}

}
