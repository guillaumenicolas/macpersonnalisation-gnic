package com.mac.custom.listmodel;

import com.mac.custom.listmodel.base.BaseMacSupplierList;
import com.netappsid.europe.naid.custom.bo.SupplierEuro;

public class MacSupplierList extends BaseMacSupplierList
{

	@Override
	public Class<?> getListType()
	{
		return SupplierEuro.class;
	}

}
