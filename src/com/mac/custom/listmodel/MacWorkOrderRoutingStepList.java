package com.mac.custom.listmodel;

import java.util.Collections;

import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Criterion;

import com.netappsid.dao.utils.boquery.SearchQueryInExpression;
import com.netappsid.erp.server.bo.DocumentStatus;
import com.netappsid.erp.server.bo.WorkOrder;
import com.netappsid.erp.server.bo.WorkOrderRoutingStep;
import com.netappsid.erp.server.listmodel.WorkOrderRoutingStepList;
import com.netappsid.query.SearchQuery;

public class MacWorkOrderRoutingStepList extends WorkOrderRoutingStepList
{
	private Boolean explodedOrderOnly;

	public void setExplodedOrderOnly(Boolean explodedOrderOnly)
	{
		Boolean oldValue = getExplodedOrderOnly();
		this.explodedOrderOnly = explodedOrderOnly;
		firePropertyChange(PROPERTYNAME_EXPLODEDORDERONLY, oldValue, explodedOrderOnly);
	}

	public Boolean getExplodedOrderOnly()
	{
		return explodedOrderOnly;
	}

	@Override
	public void buildCriteria()
	{
		super.buildCriteria();

		if (getExplodedOrderOnly() != null)
		{
			SearchQuery searchQuery = new SearchQuery(getNotFullyExplodedOrderIdsQuery(), Collections.<String, Object> emptyMap(), !getExplodedOrderOnly(),
					"workOrder.explodableDocument.id");
			Criterion inQuery = new SearchQueryInExpression(searchQuery);
			getCriteria().add(inQuery);
			getSubcriteria().add(WorkOrderRoutingStep.PROPERTYNAME_WORKORDER + "." + WorkOrder.PROPERTYNAME_EXPLODABLEDOCUMENT);
			getJoinsSpec().add(CriteriaSpecification.INNER_JOIN);
		}
	}

	public String getNotFullyExplodedOrderIdsQuery()
	{
		// @formatter:off
		return "select distinct orders.id " + "from Order orders " + "inner join orders.documentStatus documentStatus "
				+ "where documentStatus.internalId is null " + "or orders.documentStatus.internalId <> " + DocumentStatus.EXPLODE;
		// @formatter:on
	}

	@Override
	public void clear()
	{
		super.clear();
		setExplodedOrderOnly(null);
	}

	public static final String PROPERTYNAME_EXPLODEDORDERONLY = "explodedOrderOnly";
}
