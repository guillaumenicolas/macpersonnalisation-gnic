package com.mac.custom.listmodel;

// Imports
import javax.persistence.Table;

import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

import com.mac.custom.bo.MacCustomer;
import com.netappsid.annotations.DAO;
import com.netappsid.erp.server.bo.User;
import com.netappsid.erp.server.dao.OrderListDAO;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.Loader;

@Table(name = "MacOrderList")
@DAO(dao = OrderListDAO.class)
public class MacAccountingOrderList extends MacOrderList
{
	private final ServiceLocator<Loader> loaderServiceLocator = new ServiceLocator<Loader>(LoaderBean.class);

	private User analysteCredit;

	// Gestion du critere de recherche "Mode Date Expedition specifique"
	public void setAnalysteCredit(User analysteCredit)
	{
		User oldValue = getAnalysteCredit();
		this.analysteCredit = analysteCredit;
		firePropertyChange(PROPERTYNAME_ANALYSTECREDIT, oldValue, analysteCredit);
	}

	public User getAnalysteCredit()
	{
		return analysteCredit;
	}

	@Override
	public void buildCriteria()
	{
		super.buildCriteria();

		if (getAnalysteCredit() != null)
		{
			getCriteria().add(Restrictions.eq(MacCustomer.PROPERTYNAME_ANALYSTECREDIT, getAnalysteCredit()));
			getSubcriteria().add(MacAccountingOrderList.PROPERTYNAME_CUSTOMER);
			getJoinsSpec().add(CriteriaSpecification.INNER_JOIN);
		}

	}

	@Override
	public void clear()
	{
		super.clear();
		setAnalysteCredit(null);
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
		setAnalysteCredit(getUser());
		setUser(null);
		setBlockProductionEffective(true);
		setBlockShippingEffective(true);
	}

	public static final String PROPERTYNAME_ANALYSTECREDIT = "analysteCredit";

}
