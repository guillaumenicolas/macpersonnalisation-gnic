package com.mac.custom.listmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.CollectionTypeInfo;

import com.mac.custom.bo.MacCustomer;
import com.mac.custom.bo.MacOrder;
import com.mac.custom.bo.WorkCalendarDetail;
import com.mac.custom.dao.WorkCalendarDAO;
import com.mac.custom.services.workCalendar.beans.WorkCalendarServicesBean;
import com.mac.custom.services.workCalendar.interfaces.WorkCalendarServices;
import com.netappsid.annotations.DAO;
import com.netappsid.bo.model.Model;
import com.netappsid.erp.client.model.ERPListModel;
import com.netappsid.erp.server.bo.Resource;
import com.netappsid.erp.server.bo.WorkShift;
import com.netappsid.erp.server.services.resultholders.ResultHolder;
import com.netappsid.framework.utils.NaturalKeyValidator;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.observable.ObservableCollections;
import com.netappsid.transactional.TransactionalCollections;
import com.netappsid.transactional.TransactionalList;

@DAO(dao = WorkCalendarDAO.class)
public class WorkCalendar extends ERPListModel
{
	// attributes
	private Date dateFrom;
	private Date dateTo;
	private MacCustomer macCustomer;
	private Resource resource;
	private WorkShift workShift;
	private final List<WorkCalendarDetail> workCalendarDetails = ObservableCollections.newObservableArrayList();
	private MacOrder macOrder;
	private final ServiceLocator<WorkCalendarServices> workCalendarServicesLocator = new ServiceLocator<WorkCalendarServices>(WorkCalendarServicesBean.class);

	public void setDateFrom(Date dateFrom)
	{
		Date oldValue = getDateFrom();
		this.dateFrom = dateFrom == null ? null : new Date(dateFrom.getTime());
		firePropertyChange(PROPERTYNAME_DATEFROM, oldValue, dateFrom);
	}

	@Column(name = "dateFrom")
	@Temporal(TemporalType.DATE)
	@AccessType(value = "field")
	public Date getDateFrom()
	{
		return dateFrom == null ? null : new Date(dateFrom.getTime());
	}

	public void setDateTo(Date dateTo)
	{
		Date oldValue = getDateTo();
		this.dateTo = dateTo == null ? null : new Date(dateTo.getTime());
		firePropertyChange(PROPERTYNAME_DATETO, oldValue, dateTo);
	}

	@Column(name = "dateTo")
	@Temporal(TemporalType.DATE)
	@AccessType(value = "field")
	public Date getDateTo()
	{
		return dateTo == null ? null : new Date(dateTo.getTime());
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public MacCustomer getMacCustomer()
	{
		return macCustomer;
	}

	public void setMacCustomer(MacCustomer macCustomer)
	{
		MacCustomer oldValue = this.macCustomer;
		this.macCustomer = macCustomer;
		firePropertyChange(PROPERTYNAME_MACCUSTOMER, oldValue, this.macCustomer);
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public Resource getResource()
	{
		return resource;
	}

	public void setResource(Resource resource)
	{
		Resource oldValue = this.resource;
		this.resource = resource;
		firePropertyChange(PROPERTYNAME_RESOURCE, oldValue, this.resource);
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public WorkShift getWorkShift()
	{
		return workShift;
	}

	public void setWorkShift(WorkShift workShift)
	{
		WorkShift oldValue = this.workShift;
		this.workShift = workShift;
		firePropertyChange(PROPERTYNAME_WORKSHIFT, oldValue, this.workShift);
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public MacOrder getMacOrder()
	{
		return macOrder;
	}

	public void setMacOrder(MacOrder macOrder)
	{
		MacOrder oldValue = this.macOrder;
		this.macOrder = macOrder;
		firePropertyChange(PROPERTYNAME_MACORDER, oldValue, this.macOrder);
	}

	@OneToMany(fetch = FetchType.LAZY)
	@CollectionTypeInfo(name = "com.netappsid.hibernate.LazyLoadBagType")
	@AccessType(value = "field")
	public List<WorkCalendarDetail> getWorkCalendarDetails()
	{
		return workCalendarDetails;
	}

	protected void onWorkCalendarDetailsCollectionChange(TransactionalList<WorkCalendarDetail> workCalendarDetails)
	{
		TransactionalCollections.end(workCalendarDetails);
	}

	public void addToWorkCalendarDetails(WorkCalendarDetail workCalendarDetail)
	{
		final TransactionalList<WorkCalendarDetail> transactional = TransactionalCollections.begin(this.workCalendarDetails);
		NaturalKeyValidator.isAddedItemValid(transactional, workCalendarDetail);
		transactional.add(workCalendarDetail);
		onWorkCalendarDetailsCollectionChange(transactional);
	}

	public void addToWorkCalendarDetails(WorkCalendarDetail workCalendarDetail, int index)
	{
		final TransactionalList<WorkCalendarDetail> transactional = TransactionalCollections.begin(this.workCalendarDetails);
		NaturalKeyValidator.isAddedItemValid(transactional, workCalendarDetail);
		transactional.add(index, workCalendarDetail);
		onWorkCalendarDetailsCollectionChange(transactional);
	}

	public void addManyToWorkCalendarDetails(Collection<WorkCalendarDetail> workCalendarDetails)
	{
		final TransactionalList<WorkCalendarDetail> transactional = TransactionalCollections.begin(this.workCalendarDetails);
		for (WorkCalendarDetail workCalendarDetail : workCalendarDetails)
		{
			NaturalKeyValidator.isAddedItemValid(transactional, workCalendarDetail);
		}

		transactional.addAll(workCalendarDetails);
		onWorkCalendarDetailsCollectionChange(transactional);
	}

	public void removeFromWorkCalendarDetails(WorkCalendarDetail workCalendarDetail)
	{
		final TransactionalList<WorkCalendarDetail> transactional = TransactionalCollections.begin(this.workCalendarDetails);
		transactional.remove(workCalendarDetail);
		onWorkCalendarDetailsCollectionChange(transactional);
	}

	public void removeManyFromWorkCalendarDetails(Collection<WorkCalendarDetail> workCalendarDetails)
	{
		final TransactionalList<WorkCalendarDetail> transactional = TransactionalCollections.begin(this.workCalendarDetails);
		transactional.removeAll(workCalendarDetails);
		onWorkCalendarDetailsCollectionChange(transactional);
	}

	public void removeAllFromWorkCalendarDetails()
	{
		final TransactionalList<WorkCalendarDetail> transactional = TransactionalCollections.begin(this.workCalendarDetails);
		transactional.clear();
		onWorkCalendarDetailsCollectionChange(transactional);
	}

	public void replaceWorkCalendarDetails(WorkCalendarDetail oldWorkCalendarDetail, WorkCalendarDetail newWorkCalendarDetail)
	{
		final TransactionalList<WorkCalendarDetail> transactional = TransactionalCollections.begin(this.workCalendarDetails);
		final int indexOf = transactional.indexOf(oldWorkCalendarDetail);

		if (indexOf != -1)
		{
			NaturalKeyValidator.isAddedItemValid(transactional, newWorkCalendarDetail);
			transactional.set(indexOf, newWorkCalendarDetail);
		}
		onWorkCalendarDetailsCollectionChange(transactional);
	}

	public WorkCalendarDetail setToWorkCalendarDetails(int index, WorkCalendarDetail workCalendarDetail)
	{
		final TransactionalList<WorkCalendarDetail> transactional = TransactionalCollections.begin(this.workCalendarDetails);
		NaturalKeyValidator.isAddedItemValid(transactional, workCalendarDetail);

		WorkCalendarDetail removed = transactional.set(index, workCalendarDetail);

		onWorkCalendarDetailsCollectionChange(transactional);
		return removed;
	}

	@Override
	public void buildCriteria()
	{
		super.buildCriteria();
	}

	@Override
	public void clear()
	{
		super.clear();

		setDateFrom(null);
		setDateTo(null);
		setResource(null);
		setMacCustomer(null);
		setMacOrder(null);
		setWorkShift(null);
	}

	@Override
	public Class<?> getListType()
	{
		return WorkCalendarDetail.class;
	}

	@Override
	public List<Model> retrieveItems()
	{
		Serializable resourceId = null;
		Serializable workShiftId = null;
		Serializable macOrderId = null;
		Serializable macCustomerId = null;

		if (getResource() != null)
		{
			resourceId = getResource().getId();
		}
		if (getWorkShift() != null)
		{
			workShiftId = getWorkShift().getId();
		}
		if (getMacOrder() != null)
		{
			macOrderId = getMacOrder().getId();
		}
		if (getMacCustomer() != null)
		{
			macCustomerId = getMacCustomer().getId();
		}

		ResultHolder<WorkCalendarDetail> workCalendarDetailResults = workCalendarServicesLocator.get().find(getDateFrom(), getDateTo(), workShiftId,
				resourceId, macOrderId, macCustomerId);

		List<Model> items = new ArrayList<Model>();
		items.addAll(workCalendarDetailResults.getResults());

		return items;
	}

	@Override
	public boolean beforeRefresh()
	{
		super.beforeRefresh();

		removeAllFromWorkCalendarDetails();

		return true;
	}

	@Override
	public void afterRefresh()
	{
		super.afterRefresh();

		if (getItems() != null)
		{
			removeAllFromWorkCalendarDetails();

			List<WorkCalendarDetail> workCalendarDetails = new ArrayList<WorkCalendarDetail>();

			for (Model model : getItems())
			{
				workCalendarDetails.add((WorkCalendarDetail) model);
			}

			addManyToWorkCalendarDetails(workCalendarDetails);
		}
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();

		setDateFrom(new Date());
		setDateTo(new Date());
	}

	public static final String PROPERTYNAME_DATEFROM = "dateFrom";
	public static final String PROPERTYNAME_DATETO = "dateTo";
	public static final String PROPERTYNAME_MACCUSTOMER = "macCustomer";
	public static final String PROPERTYNAME_RESOURCE = "resource";
	public static final String PROPERTYNAME_WORKSHIFT = "workShift";
	public static final String PROPERTYNAME_WORKLOADS = "workCalendarDetails";
	public static final String PROPERTYNAME_MACORDER = "macOrder";
}
