package com.mac.custom.listmodel;

import java.util.ArrayList;

import org.hibernate.criterion.Restrictions;

import com.mac.custom.bo.MacLocation;
import com.mac.custom.dao.MacStockListDAO;
import com.netappsid.annotations.DAO;
import com.netappsid.erp.server.bo.Location;
import com.netappsid.erp.server.bo.Stock;
import com.netappsid.erp.server.listmodel.StockList;

@DAO(dao = MacStockListDAO.class)
public class MacStockList extends StockList
{
	@Override
	protected void buildLocationCriteria()
	{
		MacLocation location = (MacLocation) getLocation();
		if (location.isStore())
		{
			// take the location and all its sublocations
			ArrayList<Location> locations = new ArrayList<Location>(location.getSubLocations());
			locations.add(location);

			getCriteria().add(Restrictions.in(Stock.PROPERTYNAME_LOCATION, locations));
			getSubcriteria().add(null);
			getJoinsSpec().add(null);
		}
		else
		{
			super.buildLocationCriteria();
		}
	}
}
