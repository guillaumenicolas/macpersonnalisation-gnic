package com.mac.custom.listmodel;

// Imports
import javax.persistence.Column;
import javax.persistence.Table;

import org.hibernate.annotations.AccessType;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;

import com.mac.custom.bo.MacCustomer;
import com.mac.custom.bo.MacOrder;
import com.netappsid.annotations.DAO;
import com.netappsid.application.ApplicationInjector;
import com.netappsid.erp.server.dao.OrderListDAO;
import com.netappsid.europe.naid.custom.listmodel.OrderListEuro;
import com.netappsid.framework.security.SecurityHelper;

@Table(name = "MacOrderList")
@DAO(dao = OrderListDAO.class)
public class MacOrderList extends OrderListEuro
{
	private Boolean modeDateExpSpec;
	private Boolean blockProductionEffective;
	private Boolean blockShippingEffective;
	private Boolean proforma;

	public MacOrderList()
	{
		this(ApplicationInjector.INSTANCE.getInstance(SecurityHelper.class));
		setModeDateExpSpec(null);
	}

	public MacOrderList(SecurityHelper securityHelper)
	{
		super(securityHelper);
	}

	// Gestion du critere de recherche "Mode Date Expedition specifique"
	public void setModeDateExpSpec(Boolean modeDateExpSpec)
	{
		Boolean oldValue = getModeDateExpSpec();
		this.modeDateExpSpec = modeDateExpSpec;
		firePropertyChange(PROPERTYNAME_MODEDATEEXPSPEC, oldValue, modeDateExpSpec);
	}

	public Boolean getModeDateExpSpec()
	{
		return modeDateExpSpec;
	}

	public void setBlockProductionEffective(Boolean blockProductionEffective)
	{
		Boolean oldValue = getBlockProductionEffective();
		this.blockProductionEffective = blockProductionEffective;
		firePropertyChange(PROPERTYNAME_BLOCKPRODUCTIONEFFECTIVE, oldValue, blockProductionEffective);
	}

	@Column(name = "blockProductionEffective")
	@AccessType(value = "field")
	public Boolean getBlockProductionEffective()
	{
		return blockProductionEffective;
	}

	public void setBlockShippingEffective(Boolean blockShippingEffective)
	{
		Boolean oldValue = getBlockShippingEffective();
		this.blockShippingEffective = blockShippingEffective;
		firePropertyChange(PROPERTYNAME_BLOCKSHIPPINGEFFECTIVE, oldValue, blockShippingEffective);
	}

	@Column(name = "blockShippingEffective")
	@AccessType(value = "field")
	public Boolean getBlockShippingEffective()
	{
		return blockShippingEffective;
	}

	public void setProforma(Boolean proforma)
	{
		Boolean oldValue = getProforma();
		this.proforma = proforma;
		firePropertyChange(PROPERTYNAME_PROFORMA, oldValue, proforma);
	}

	public Boolean getProforma()
	{
		return proforma;
	}

	@Override
	public void buildCriteria()
	{
		super.buildCriteria();

		if (getModeDateExpSpec() != null)
		{
			if (getModeDateExpSpec())
			{
				getCriteria().add(
						Restrictions.and(Restrictions.eq(MacOrder.PROPERTYNAME_SHIPASAP, false), Restrictions.eq(MacOrder.PROPERTYNAME_SHIPTOCONFIRM, false)));
				getSubcriteria().add(null);
				getJoinsSpec().add(null);
			}
			else
			{
				getCriteria().add(
						Restrictions.or(Restrictions.eq(MacOrder.PROPERTYNAME_SHIPASAP, true), Restrictions.eq(MacOrder.PROPERTYNAME_SHIPTOCONFIRM, true)));
				getSubcriteria().add(null);
				getJoinsSpec().add(null);
			}

		}

		if (getBlockProductionEffective() != null)
		{
			SimpleExpression orderEqual = Restrictions.eq(MacOrder.PROPERTYNAME_BLOCKPRODUCTION, getBlockProductionEffective());
			Criterion customerFallbackEqual = Restrictions.and(Restrictions.isNull(MacOrder.PROPERTYNAME_BLOCKPRODUCTION),
					Restrictions.eq(MacOrder.PROPERTYNAME_CUSTOMER + "." + MacCustomer.PROPERTYNAME_BLOCKPRODUCTION, getBlockProductionEffective()));
			getCriteria().add(Restrictions.or(orderEqual, customerFallbackEqual));
			getSubcriteria().add(null);
			getJoinsSpec().add(null);
		}

		if (getBlockShippingEffective() != null)
		{
			SimpleExpression orderEqual = Restrictions.eq(MacOrder.PROPERTYNAME_BLOCKSHIPPING, getBlockShippingEffective());
			Criterion customerFallbackEqual = Restrictions.and(Restrictions.isNull(MacOrder.PROPERTYNAME_BLOCKSHIPPING),
					Restrictions.eq(MacOrder.PROPERTYNAME_CUSTOMER + "." + MacCustomer.PROPERTYNAME_BLOCKSHIPPING, getBlockShippingEffective()));
			getCriteria().add(Restrictions.or(orderEqual, customerFallbackEqual));
			getSubcriteria().add(null);
			getJoinsSpec().add(null);
		}

		if (getProforma() != null)
		{
			getCriteria().add(Restrictions.eq(MacOrder.PROPERTYNAME_PROFORMA, getProforma()));
			getSubcriteria().add(null);
			getJoinsSpec().add(null);
		}

	}

	@Override
	public void clear()
	{
		super.clear();
		setModeDateExpSpec(null);
		setBlockProductionEffective(null);
		setBlockShippingEffective(null);
		setProforma(null);
	}

	@Override
	public Class<?> getListType()
	{
		return MacOrder.class;
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
		setExcludeClosedAndCanceled(true);
	}

	public static final String PROPERTYNAME_MODEDATEEXPSPEC = "modeDateExpSpec";
	public static final String PROPERTYNAME_BLOCKPRODUCTIONEFFECTIVE = "blockProductionEffective";
	public static final String PROPERTYNAME_BLOCKSHIPPINGEFFECTIVE = "blockShippingEffective";
	public static final String PROPERTYNAME_PROFORMA = "proforma";
}
