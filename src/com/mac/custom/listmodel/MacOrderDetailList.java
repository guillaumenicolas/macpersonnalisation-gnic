package com.mac.custom.listmodel;

import javax.persistence.Column;

import org.hibernate.annotations.AccessType;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;

import com.mac.custom.bo.MacCustomer;
import com.mac.custom.bo.MacOrder;
import com.mac.custom.bo.MacOrderDetail;
import com.netappsid.erp.server.listmodel.OrderDetailList;

public class MacOrderDetailList extends OrderDetailList
{
	private Boolean rareBird;
	private Boolean blockProductionEffective;
	private Boolean blockShippingEffective;

	public void setRareBird(Boolean rareBird)
	{
		Boolean oldValue = getRareBird();
		this.rareBird = rareBird;
		firePropertyChange(PROPERTYNAME_RAREBIRD, oldValue, rareBird);
	}

	@Column(name = "rareBird")
	@AccessType(value = "field")
	public Boolean getRareBird()
	{
		return rareBird;
	}

	public void setBlockProductionEffective(Boolean blockProductionEffective)
	{
		Boolean oldValue = getBlockProductionEffective();
		this.blockProductionEffective = blockProductionEffective;
		firePropertyChange(PROPERTYNAME_BLOCKPRODUCTIONEFFECTIVE, oldValue, blockProductionEffective);
	}

	@Column(name = "blockProductionEffective")
	@AccessType(value = "field")
	public Boolean getBlockProductionEffective()
	{
		return blockProductionEffective;
	}

	public void setBlockShippingEffective(Boolean blockShippingEffective)
	{
		Boolean oldValue = getBlockShippingEffective();
		this.blockShippingEffective = blockShippingEffective;
		firePropertyChange(PROPERTYNAME_BLOCKSHIPPINGEFFECTIVE, oldValue, blockShippingEffective);
	}

	@Column(name = "blockShippingEffective")
	@AccessType(value = "field")
	public Boolean getBlockShippingEffective()
	{
		return blockShippingEffective;
	}

	@Override
	public void buildCriteria()
	{
		super.buildCriteria();

		if (getRareBird() != null)
		{
			getCriteria().add(Restrictions.eq(MacOrderDetail.PROPERTYNAME_RAREBIRD, getRareBird()));
			getSubcriteria().add(null);
			getJoinsSpec().add(null);
		}

		if (getBlockProductionEffective() != null)
		{
			SimpleExpression orderEqual = Restrictions.eq(MacOrder.PROPERTYNAME_BLOCKPRODUCTION, getBlockProductionEffective());
			Criterion customerFallbackEqual = Restrictions.and(Restrictions.isNull(MacOrder.PROPERTYNAME_BLOCKPRODUCTION),
					Restrictions.eq(MacOrder.PROPERTYNAME_CUSTOMER + "." + MacCustomer.PROPERTYNAME_BLOCKPRODUCTION, getBlockProductionEffective()));
			getCriteria().add(Restrictions.or(orderEqual, customerFallbackEqual));
			getSubcriteria().add(MacOrderDetail.PROPERTYNAME_SALETRANSACTION);
			getJoinsSpec().add(CriteriaSpecification.INNER_JOIN);
		}

		if (getBlockShippingEffective() != null)
		{
			SimpleExpression orderEqual = Restrictions.eq(MacOrder.PROPERTYNAME_BLOCKSHIPPING, getBlockShippingEffective());
			Criterion customerFallbackEqual = Restrictions.and(Restrictions.isNull(MacOrder.PROPERTYNAME_BLOCKSHIPPING),
					Restrictions.eq(MacOrder.PROPERTYNAME_CUSTOMER + "." + MacCustomer.PROPERTYNAME_BLOCKSHIPPING, getBlockShippingEffective()));
			getCriteria().add(Restrictions.or(orderEqual, customerFallbackEqual));
			getSubcriteria().add(MacOrderDetail.PROPERTYNAME_SALETRANSACTION);
			getJoinsSpec().add(CriteriaSpecification.INNER_JOIN);
		}
	}

	@Override
	public void clear()
	{
		super.clear();
		setRareBird(null);
		setBlockProductionEffective(null);
		setBlockShippingEffective(null);
	}

	@Override
	public Class<?> getListType()
	{
		return MacOrderDetail.class;
	}

	@Override
	public void setDefaultValues()
	{
		super.setDefaultValues();
		setExcludeClosedAndCanceled(true);
	}

	public static final String PROPERTYNAME_RAREBIRD = "rareBird";
	public static final String PROPERTYNAME_BLOCKPRODUCTIONEFFECTIVE = "blockProductionEffective";
	public static final String PROPERTYNAME_BLOCKSHIPPINGEFFECTIVE = "blockShippingEffective";
}
