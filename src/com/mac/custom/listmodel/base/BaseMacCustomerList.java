package com.mac.custom.listmodel.base;

import java.io.Serializable;

import com.mac.custom.bo.ActiviteClient;
import com.mac.custom.bo.EnseigneClient;
import com.mac.custom.bo.MacRegionCommerciale;
import com.mac.custom.listmodel.MacCommercialEntityList;
import com.netappsid.europe.naid.custom.bo.SalesRepEuro;
import com.netappsid.europe.naid.custom.bo.UserEuro;

public abstract class BaseMacCustomerList extends MacCommercialEntityList implements Serializable
{
	private MacRegionCommerciale regionCommerciale;
	private SalesRepEuro salesRepManager;
	private SalesRepEuro salesRep;
	private UserEuro adv;
	private ActiviteClient activite;
	private EnseigneClient enseigne;
	private Boolean blockProductionEffective;
	private Boolean blockShippingEffective;
	private Boolean blockSalesEffective;

	public SalesRepEuro getSalesRep()
	{
		return salesRep;
	}

	public void setSalesRep(SalesRepEuro salesRep)
	{
		SalesRepEuro oldValue = getSalesRep();
		this.salesRep = salesRep;
		firePropertyChange(PROPERTYNAME_SALESREP, oldValue, salesRep);
	}

	public UserEuro getAdv()
	{
		return adv;
	}

	public void setAdv(UserEuro adv)
	{
		UserEuro oldValue = getAdv();
		this.adv = adv;
		firePropertyChange(PROPERTYNAME_ADV, oldValue, adv);
	}

	public MacRegionCommerciale getRegionCommerciale()
	{
		return regionCommerciale;
	}

	public SalesRepEuro getSalesRepManager()
	{
		return salesRepManager;
	}

	public void setRegionCommerciale(MacRegionCommerciale regionCommerciale)
	{
		MacRegionCommerciale oldValue = getRegionCommerciale();
		this.regionCommerciale = regionCommerciale;
		firePropertyChange(PROPERTYNAME_REGIONCOMMERCIALE, oldValue, regionCommerciale);
	}

	public void setSalesRepManager(SalesRepEuro salesRepManager)
	{
		SalesRepEuro oldValue = getSalesRepManager();
		this.salesRepManager = salesRepManager;
		firePropertyChange(PROPERTYNAME_SALESREPMANAGER, oldValue, salesRepManager);
	}

	public ActiviteClient getActivite()
	{
		return activite;
	}

	public EnseigneClient getEnseigne()
	{
		return enseigne;
	}

	public void setActivite(ActiviteClient activite)
	{
		ActiviteClient oldValue = getActivite();
		this.activite = activite;
		firePropertyChange(PROPERTYNAME_ACTIVITE, oldValue, activite);
	}

	public void setEnseigne(EnseigneClient enseigne)
	{
		EnseigneClient oldValue = getEnseigne();
		this.enseigne = enseigne;
		firePropertyChange(PROPERTYNAME_ENSEIGNE, oldValue, enseigne);
	}

	public Boolean getBlockProductionEffective()
	{
		return blockProductionEffective;
	}

	public Boolean getBlockShippingEffective()
	{
		return blockShippingEffective;
	}

	public Boolean getBlockSalesEffective()
	{
		return blockSalesEffective;
	}

	public void setBlockProductionEffective(Boolean blockProductionEffective)
	{
		Boolean oldValue = getBlockProductionEffective();
		this.blockProductionEffective = blockProductionEffective;
		firePropertyChange(PROPERTYNAME_BLOCKPRODUCTION_EFFECTIVE, oldValue, blockProductionEffective);
	}

	public void setBlockShippingEffective(Boolean blockShippingEffective)
	{
		Boolean oldValue = getBlockShippingEffective();
		this.blockShippingEffective = blockShippingEffective;
		firePropertyChange(PROPERTYNAME_BLOCKSHIPPING_EFFECTIVE, oldValue, blockShippingEffective);
	}

	public void setBlockSalesEffective(Boolean blockSalesEffective)
	{
		Boolean oldValue = getBlockSalesEffective();
		this.blockSalesEffective = blockSalesEffective;
		firePropertyChange(PROPERTYNAME_BLOCKSALES_EFFECTIVE, oldValue, blockSalesEffective);
	}

	public static final String PROPERTYNAME_SALESREP = "salesRep";
	public static final String PROPERTYNAME_SALESREPMANAGER = "salesRepManager";
	public static final String PROPERTYNAME_REGIONCOMMERCIALE = "regionCommerciale";
	public static final String PROPERTYNAME_ADV = "adv";
	public static final String PROPERTYNAME_ACTIVITE = "activite";
	public static final String PROPERTYNAME_ENSEIGNE = "enseigne";
	public static final String PROPERTYNAME_BLOCKPRODUCTION_EFFECTIVE = "blockProductionEffective";
	public static final String PROPERTYNAME_BLOCKSHIPPING_EFFECTIVE = "blockShippingEffective";
	public static final String PROPERTYNAME_BLOCKSALES_EFFECTIVE = "blockSalesEffective";

}