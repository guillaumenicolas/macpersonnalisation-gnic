package com.mac.custom.listmodel.base;

import java.io.Serializable;

import com.netappsid.erp.client.model.ERPListModel;
import com.netappsid.erp.server.bo.City;
import com.netappsid.erp.server.bo.CommercialClass;
import com.netappsid.erp.server.bo.Country;

public abstract class BaseMacCommercialEntityList extends ERPListModel implements Serializable
{
	private boolean active;
	private String code;
	private String headQuarterAddressZipCode;
	private City headQuarterAddressCity;
	private Country headQuarterAddressCountry;
	private Boolean paysEtranger;
	private String headQuarterPhone;
	private String headQuarterFax;
	private String siren;
	private CommercialClass typologie;

	public String getCode()
	{
		return code;
	}

	public void setCode(String code)
	{
		String oldValue = getCode();
		this.code = code;
		firePropertyChange(PROPERTYNAME_CODE, oldValue, code);
	}

	public String getHeadQuarterAddressZipCode()
	{
		return headQuarterAddressZipCode;
	}

	public void setHeadQuarterAddressZipCode(String invoicingAddressZipCode)
	{
		String oldValue = getHeadQuarterAddressZipCode();
		this.headQuarterAddressZipCode = invoicingAddressZipCode;
		firePropertyChange(PROPERTYNAME_HEADQUARTER_ADDRESSZIPCODE, oldValue, invoicingAddressZipCode);
	}

	public Country getHeadQuarterAddressCountry()
	{
		return headQuarterAddressCountry;
	}

	public void setHeadQuarterAddressCountry(Country addressCountry)
	{
		Country oldValue = getHeadQuarterAddressCountry();
		this.headQuarterAddressCountry = addressCountry;
		firePropertyChange(PROPERTYNAME_HEADQUARTER_ADDRESSCOUNTRY, oldValue, addressCountry);
	}

	public City getHeadQuarterAddressCity()
	{
		return headQuarterAddressCity;
	}

	public void setHeadQuarterAddressCity(City addressCity)
	{
		City oldValue = getHeadQuarterAddressCity();
		this.headQuarterAddressCity = addressCity;
		firePropertyChange(PROPERTYNAME_HEADQUARTER_ADDRESSCITY, oldValue, addressCity);
	}

	public String getHeadQuarterPhone()
	{
		return headQuarterPhone;
	}

	public String getHeadQuarterFax()
	{
		return headQuarterFax;
	}

	public String getSiren()
	{
		return siren;
	}

	public void setSiren(String siren)
	{
		String oldValue = getSiren();
		this.siren = siren;
		firePropertyChange(PROPERTYNAME_SIREN, oldValue, siren);
	}

	public void setHeadQuarterPhone(String phone)
	{
		String oldValue = getHeadQuarterPhone();
		this.headQuarterPhone = phone;
		firePropertyChange(PROPERTYNAME_HEADQUARTER_PHONE, oldValue, phone);
	}

	public void setHeadQuarterFax(String fax)
	{
		String oldValue = getHeadQuarterFax();
		this.headQuarterFax = fax;
		firePropertyChange(PROPERTYNAME_HEADQUARTER_FAX, oldValue, fax);
	}

	public boolean isActive()
	{
		return active;
	}

	public void setActive(boolean active)
	{
		boolean oldValue = isActive();
		this.active = active;
		firePropertyChange(PROPERTYNAME_ACTIVE, oldValue, active);
	}

	public CommercialClass getTypologie()
	{
		return typologie;
	}

	public void setTypologie(CommercialClass typologie)
	{
		CommercialClass oldValue = getTypologie();
		this.typologie = typologie;
		firePropertyChange(PROPERTYNAME_TYPOLOGIE, oldValue, typologie);
	}

	public Boolean getPaysEtranger()
	{
		return paysEtranger;
	}

	public void setPaysEtranger(Boolean paysEtranger)
	{

		Boolean oldValue = getPaysEtranger();
		this.paysEtranger = paysEtranger;
		firePropertyChange(PROPERTYNAME_PAYS_ETRANGER, oldValue, paysEtranger);
	}

	public static final String PROPERTYNAME_ACTIVE = "active";
	public static final String PROPERTYNAME_CODE = "code";
	public static final String PROPERTYNAME_HEADQUARTER_ADDRESSZIPCODE = "headQuarterAddressZipCode";
	public static final String PROPERTYNAME_HEADQUARTER_ADDRESSCITY = "headQuarterAddressCity";
	public static final String PROPERTYNAME_HEADQUARTER_ADDRESSCOUNTRY = "headQuarterAddressCountry";
	public static final String PROPERTYNAME_HEADQUARTER_PHONE = "headQuarterPhone";
	public static final String PROPERTYNAME_HEADQUARTER_FAX = "headQuarterFax";
	public static final String PROPERTYNAME_SIREN = "siren";
	public static final String PROPERTYNAME_TYPOLOGIE = "typologie";
	public static final String PROPERTYNAME_PAYS_ETRANGER = "paysEtranger";
}