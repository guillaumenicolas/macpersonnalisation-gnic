package com.mac.custom.table.renderer;

import java.awt.Color;

import javax.swing.table.TableModel;

import com.jidesoft.grid.CellStyle;
import com.jidesoft.grid.CellStyleProvider;
import com.mac.custom.bo.WorkLoadDetail;
import com.netappsid.framework.gui.components.model.table.BeanTableModelUtils;
import com.netappsid.framework.gui.components.table.NAIDTableConstant;

public class WorkLoadDetailCellRenderer implements CellStyleProvider
{
	private final CellStyle producedCellStyle = new CellStyle();

	public WorkLoadDetailCellRenderer()
	{
		initializeProducedCellStyle();
	}

	@Override
	public CellStyle getCellStyleAt(TableModel model, int rowIndex, int columnIndex)
	{
		final WorkLoadDetail workLoadDetail = BeanTableModelUtils.getBean(model, rowIndex);

		if (workLoadDetail != null)
		{
			if (workLoadDetail.isProduced())
			{
				return producedCellStyle;
			}
		}
		return null;
	}

	private void initializeProducedCellStyle()
	{
		producedCellStyle.setBackground(Color.GREEN);
		producedCellStyle.setForeground(NAIDTableConstant.EVENROWFOREGROUND);
	}
}