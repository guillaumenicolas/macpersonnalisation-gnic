package com.mac.custom.table.renderer;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;

import org.apache.log4j.Logger;

import com.netappsid.erp.server.bo.AddressType;
import com.netappsid.erp.server.bo.CommercialEntityAddress;
import com.netappsid.framework.gui.components.model.table.BeanTableModelUtils;

public class CommercialEntityAddressTypesCellRenderer extends DefaultTableCellRenderer
{

	private static final Logger logger = Logger.getLogger(CommercialEntityAddressTypesCellRenderer.class);

	public CommercialEntityAddressTypesCellRenderer()
	{
		setBorder(null);
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
	{
		super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		Component component = this;

		TableModel model = table.getModel();
		CommercialEntityAddress commercialEntityAddress = BeanTableModelUtils.getBean(model, row);

		if (commercialEntityAddress != null)
		{
			try
			{
				String addressTypeList = "";
				int index = 0;
				for (AddressType addressType : commercialEntityAddress.getTypes())
				{
					addressTypeList += addressType.getLocalizedDescription();
					index++;
					if (index < commercialEntityAddress.getTypes().size())
					{
						addressTypeList += ", ";
					}
				}
				setText(addressTypeList);
			}
			catch (Exception exc)
			{
				logger.debug(exc, exc);
			}
		}
		if (isSelected || table.getSelectedRow() == row)
		{
			component.setBackground(table.getSelectionBackground());
			component.setForeground(table.getSelectionForeground());
		}
		else
		{
			component.setBackground(table.getBackground());
			component.setForeground(table.getForeground());
		}

		return component;
	}
}
