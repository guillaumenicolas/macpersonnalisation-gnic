package com.mac.custom.table.renderer;

import java.awt.Color;
import java.util.HashMap;

import javax.swing.table.TableModel;

import com.jidesoft.grid.CellStyle;
import com.jidesoft.grid.CellStyleProvider;
import com.netappsid.framework.gui.components.table.NAIDTableConstant;

public class MacOrderListTableCellRenderer implements CellStyleProvider
{
	private final String formName;
	private final static HashMap<String, Integer> mapPositions = new HashMap<String, Integer>();

	private final CellStyle defaultCellStyleBlockProductionEffective = new CellStyle();
	private final CellStyle defaultCellStyleBlockShippingEffective = new CellStyle();
	private final CellStyle defaultCellStyleUnblocked = new CellStyle();

	public MacOrderListTableCellRenderer(String formName)
	{
		// initialise les positions des champs dans la liste originale.
		mapPositions.put("MacOrderListFormCustom_POSITION_CHAMP_BLOQUEE_PROD", new Integer(11));
		mapPositions.put("MacOrderListFormCustom_POSITION_CHAMP_BLOQUEE_EXPED", new Integer(12));
		mapPositions.put("MacOrderListFormCustom_POSITION_CHAMP_DEBLOQUEE", new Integer(13));
		mapPositions.put("MacAccountingOrderListFormCustom_POSITION_CHAMP_BLOQUEE_PROD", new Integer(9));
		mapPositions.put("MacAccountingOrderListFormCustom_POSITION_CHAMP_BLOQUEE_EXPED", new Integer(10));
		mapPositions.put("MacAccountingOrderListFormCustom_POSITION_CHAMP_DEBLOQUEE", new Integer(11));

		// récupère le nom du formulaire sur lequel travaille la cellule
		this.formName = formName;
		// initialize les couleurs (style) de la cellule
		initializeProducedCellStyle();
	}

	private int getPosition(String bloquedProdOrExpedPosition)
	{
		Integer position = mapPositions.get(formName + "_" + bloquedProdOrExpedPosition);
		if (position == null)
			return 1;
		else
			return position;
	}

	@Override
	public CellStyle getCellStyleAt(TableModel model, int rowIndex, int columnIndex)
	{

		if (model.getValueAt(rowIndex, getPosition("POSITION_CHAMP_BLOQUEE_PROD")).equals(Boolean.TRUE))
		{
			if (formName.equals("MacAccountingOrderListFormCustom"))
				return null;
			else
				return defaultCellStyleBlockProductionEffective;
		}
		else if (model.getValueAt(rowIndex, getPosition("POSITION_CHAMP_BLOQUEE_EXPED")).equals(Boolean.TRUE))
		{
			return defaultCellStyleBlockShippingEffective;
		}
		else if (model.getValueAt(rowIndex, getPosition("POSITION_CHAMP_DEBLOQUEE")).equals(Boolean.TRUE))
		{
			return defaultCellStyleUnblocked;
		}
		else
			return null;
	}

	private void initializeProducedCellStyle()
	{
		defaultCellStyleBlockProductionEffective.setBackground(Color.RED);
		defaultCellStyleBlockProductionEffective.setForeground(Color.WHITE);
		defaultCellStyleBlockShippingEffective.setBackground(Color.ORANGE);
		defaultCellStyleBlockShippingEffective.setForeground(NAIDTableConstant.EVENROWFOREGROUND);
		defaultCellStyleUnblocked.setBackground(Color.GREEN);
		defaultCellStyleUnblocked.setForeground(NAIDTableConstant.EVENROWFOREGROUND);
	}
}
