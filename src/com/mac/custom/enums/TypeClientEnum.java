package com.mac.custom.enums;

import com.netappsid.resources.Translator;

public enum TypeClientEnum
{
	CRMAVALIDER
	{
		@Override
		public String toString()
		{
			return Translator.getString("crmaValider");
		}
	},
	PROSPECT
	{
		@Override
		public String toString()
		{
			return Translator.getString("prospect");
		}
	},
	VALIDATIONFINANCE
	{
		@Override
		public String toString()
		{
			return Translator.getString("validationFinance");
		}
	},
	CLIENT
	{
		@Override
		public String toString()
		{
			return Translator.getString("client");
		}
	},
	DIVERS
	{
		@Override
		public String toString()
		{
			return Translator.getString("divers");
		}
	}
}
