package com.mac.custom.enums;

import com.netappsid.erp.server.bo.SaleType;

public enum SaleTypeEnum
{
	chantier
	{
		@Override
		public String getUpperCode()
		{
			return "CHAN";
		}
	},
	diffu
	{
		@Override
		public String getUpperCode()
		{
			return "DIFF";
		}
	},
	sav
	{
		@Override
		public String getUpperCode()
		{
			return "SAV";
		}
	};

	public abstract String getUpperCode();

	public static SaleTypeEnum getSaleTypeEnum(SaleType saleType)
	{
		for (SaleTypeEnum saleTypeEnum : SaleTypeEnum.values())
		{
			if (saleTypeEnum.getUpperCode().equals(saleType.getUpperCode()))
			{
				return saleTypeEnum;
			}
		}
		return null;
	}
}
