package com.mac.custom.gabarits;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mac.custom.exception.ExceptionInterface;

@Stateless
public class ExportBDGabaritGeneriqueServiceBean implements ExportBDGabaritGeneriqueServiceLocal
{
	private static Logger logger = Logger.getLogger(ExportBDGabaritGeneriqueServiceBean.class);

	// Dans quel contexte on est: local ou remote
	private static String IMPORT_EXPORT_CONTEXT = "importExportContext";

	@Resource(mappedName = "java:/INTCOMPTADS")
	private DataSource importDS;

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void suppressionEntrees(String nomTable) throws ExceptionInterface
	{
		String context = System.getProperty(IMPORT_EXPORT_CONTEXT);

		Connection connection = getConnection();

		if (connection == null)
		{
			throw new ExceptionInterface("Aucune connection de sp�cifi�", logger);
		}

		if (nomTable == null || nomTable.equals(""))
		{
			throw new ExceptionInterface("Nom de la table invalide", logger);
		}

		StringBuffer sql = new StringBuffer();

		sql.append("Delete from ");
		sql.append(nomTable);
		sql.append(" where codeinte = 2");

		PreparedStatement preparedStatement;

		try
		{
			preparedStatement = connection.prepareStatement(sql.toString());

			preparedStatement.executeUpdate();

			if (context != null && context.equalsIgnoreCase("local"))
			{
				connection.commit();
			}
		}
		catch (Exception e)
		{
			throw new ExceptionInterface("Erreur lors de la mise � jou de la table d'interface", logger);
		}
		finally
		{
			try
			{
				connection.close();
				connection = null;
			}
			catch (SQLException e1)
			{
				throw new ExceptionInterface("Impossible de terminer la connexion: " + e1.getLocalizedMessage(), logger);
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateChampsInterface(Map<String, String> clesColonnes, String nomTable, String nomColonne, int code, String messageErreur)
			throws ExceptionInterface
	{
		String context = System.getProperty(IMPORT_EXPORT_CONTEXT);

		Connection connection = getConnection();

		if (connection == null)
		{
			throw new ExceptionInterface("Aucune connection de sp�cifi�", logger);
		}

		if (clesColonnes == null || clesColonnes.size() == 0)
		{
			throw new ExceptionInterface("Map de cles/colonnes invalide", logger);
		}

		if (nomTable == null || nomTable.equals(""))
		{
			throw new ExceptionInterface("Nom de la table invalide", logger);
		}

		int nbParametres = 0;
		boolean contientErreur = false;

		StringBuffer sql = new StringBuffer();

		sql.append("Update ");
		sql.append(nomTable);
		sql.append(" set ");
		sql.append(nomColonne);
		sql.append("= ?");

		if (messageErreur != null && !messageErreur.equals(""))
		{
			contientErreur = true;
			sql.append(", erreur = ?");
		}

		sql.append(" where ");

		for (String cle : clesColonnes.keySet())
		{
			nbParametres++;

			sql.append(cle + "= ?");

			if (nbParametres < clesColonnes.size())
			{
				sql.append(" and ");
			}
		}

		PreparedStatement preparedStatement;

		try
		{
			preparedStatement = connection.prepareStatement(sql.toString());

			preparedStatement.setInt(1, code);

			if (contientErreur)
			{
				preparedStatement.setString(2, messageErreur);
			}

			int compteur = 2;

			if (contientErreur)
			{
				compteur = 3;
			}

			for (String valeur : clesColonnes.values())
			{
				preparedStatement.setString(compteur, valeur);
				compteur++;
			}

			preparedStatement.executeUpdate();

			if (context != null && context.equalsIgnoreCase("local"))
			{
				connection.commit();
			}
		}
		catch (Exception e)
		{
			throw new ExceptionInterface("Erreur lors de la mise � jou de la table d'interface", logger);
		}
		finally
		{
			try
			{
				connection.close();
				connection = null;
			}
			catch (SQLException e1)
			{
				throw new ExceptionInterface("Impossible de terminer la connexion: " + e1.getLocalizedMessage(), logger);
			}
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean encoreResultats(String nomTable, String nomColonne)
	{
		Connection connection = null;

		try
		{
			connection = getConnection();

			String query = "SELECT COUNT(*) as results FROM " + nomTable + " where " + nomColonne + "= 0";

			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(query);

			while (rs.next())
			{
				int results = rs.getInt("results");

				if (results > 0)
				{
					connection.close();

					return true;
				}
			}
		}
		catch (Exception ex)
		{
			logger.error("Impossible d'executer encoreResultats: " + ex.getMessage());
		}
		finally
		{
			try
			{
				if (connection != null)
				{
					connection.close();
				}

			}
			catch (SQLException e)
			{
				logger.error("Impossible de terminer la connexion dans encoreResultats: " + e.getMessage());
			}
		}

		return false;
	}

	/**
	 * Retourne la connexion JDBC � la BD.
	 * 
	 * @return Retourne la connextion � la BD
	 */
	private Connection getConnection() throws ExceptionInterface
	{
		String context = System.getProperty(IMPORT_EXPORT_CONTEXT);

		Connection connection = null;

		try
		{
			connection = null;

			if (context == null || context.equalsIgnoreCase("remote"))
			{
				connection = importDS.getConnection();
			}
			// Temporaire en attendant un correctif de 360
			else
			{
				DriverManager.registerDriver((Driver) Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance());
				connection = DriverManager.getConnection("jdbc:jtds:sqlserver://127.0.0.1:1433/BC360MACExemples", "sa", "victor");
				connection.setAutoCommit(false);
			}

			return connection;
		}
		catch (Exception e)
		{
			logger.error("Impossible d'obtenir une connexion � la DB: " + e.getMessage());

			try
			{
				if (connection != null)
				{
					connection.close();
					connection = null;
				}
			}
			catch (Exception e1)
			{
				logger.error("Impossible de terminer la connexion: " + e1.getLocalizedMessage());
			}
		}

		return null;
	}

}
