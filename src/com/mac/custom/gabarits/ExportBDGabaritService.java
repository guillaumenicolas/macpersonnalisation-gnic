package com.mac.custom.gabarits;

import com.mac.custom.exception.ExceptionInterface;

public interface ExportBDGabaritService
{
	/**
	 * Methode sans param�tre appel�e � partir des services c�dul�s
	 */
	public void methodeExportation() throws ExceptionInterface;
}
