package com.mac.custom.gabarits;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.mac.custom.exception.ExceptionInterface;
import com.netappsid.services.interfaces.local.LoaderLocal;
import com.netappsid.services.interfaces.local.PersistenceLocal;

@Stateless
public class ImportBDGabaritServiceBean implements ImportBDGabaritServiceLocal, ImportBDGabaritServiceRemote
{
	private static Logger logger = Logger.getLogger(ImportBDGabaritServiceBean.class);

	private static String IMPORT_EXPORT_CONTEXT = "importExportContext";

	private static String NOM_TABLE = "maTable";
	private static String NOM_COLONNE = "codeinte";

	private static int MAX_RESULTATS = 100;

	@EJB(mappedName = "erp/ImportBDGabaritGeneriqueServiceBean/local")
	public ImportBDGabaritGeneriqueServiceLocal importBDServiceLocal;

	@EJB(mappedName = "erp/LoaderBean/local")
	public LoaderLocal loader;

	@EJB(mappedName = "erp/PersistenceBean/local")
	public PersistenceLocal persistence;

	@Resource(mappedName = "java:/INTCOMPTADS")
	private DataSource importDS;

	@Override
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public void methodeImportation() throws ExceptionInterface
	{
		while (importBDServiceLocal.encoreResultats(NOM_TABLE, NOM_COLONNE))
		{
			logger.debug("D�but de l'importation [NOM DE L'IMPORTATION]");

			importationBatch();

			logger.debug("FIN de l'importation [NOM DE L'IMPORTATION]");
		}
	}

	@TransactionAttribute(TransactionAttributeType.NEVER)
	private void importationBatch() throws ExceptionInterface
	{
		Connection connection = getConnection();

		if (connection == null)
		{
			throw new ExceptionInterface("Aucune connection de sp�cifi�", logger);
		}

		// ----------------------

		// Ici on va chercher les valeurs par d�faut et on valide que tout est d�fini

		// /---------------------

		ResultSet rowset = null;

		try
		{
			Statement statement = connection.createStatement();
			statement.setMaxRows(MAX_RESULTATS);

			rowset = statement.executeQuery(getRequete());
		}
		catch (Exception e)
		{
			try
			{
				connection.close();
				connection = null;
			}
			catch (SQLException e1)
			{
				throw new ExceptionInterface("Impossible de terminer la connexion: " + e1.getLocalizedMessage(), logger);
			}

			throw new ExceptionInterface("Impossible d'ex�cuter la requ�te: " + e.getLocalizedMessage(), logger);
		}

		try
		{
			while (rowset.next())
			{
				Map<String, String> clesColonnes = getClesColonnes(rowset);

				try
				{
					if (clesColonnes != null)
					{
						// Modification du statut
						importBDServiceLocal.updateChampsInterface(clesColonnes, NOM_TABLE, NOM_COLONNE, 1, "");

						traitementDeLaLigne(rowset);

						// Modification du statut
						importBDServiceLocal.updateChampsInterface(clesColonnes, NOM_TABLE, NOM_COLONNE, 2, "");
					}
					else
					{
						throw new ExceptionInterface("Cles/colonnes non specifies pour la table " + NOM_TABLE, logger);
					}

				}
				catch (ExceptionInterface ei)
				{
					// Modification du statut
					importBDServiceLocal.updateChampsInterface(clesColonnes, NOM_TABLE, NOM_COLONNE, 2, ei.getMessage());
				}

			}
		}
		catch (SQLException e)
		{
			try
			{
				connection.close();
				connection = null;
			}
			catch (SQLException e1)
			{
				throw new ExceptionInterface("Impossible de terminer la connexion: " + e1.getLocalizedMessage(), logger);
			}
		}

	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void traitementDeLaLigne(ResultSet resultset) throws ExceptionInterface, SQLException
	{
		// ----------------------

		// Ici on va chercher les valeurs de la requete
		// String valeur = resultset.getString(1);

		// ----------------------

		// Creation de l'objet

		// Sauvegarde de l,objet
		// persistence.save(OBJECTASAUVEGARDER, null);
	}

	private Map<String, String> getClesColonnes(ResultSet resultset)
	{
		Map<String, String> parametres = new HashMap<String, String>();

		try
		{
			parametres.put("NOMDELACOLONNE", resultset.getString(1));
			parametres.put("NOMDELACOLONNE", resultset.getString(2));
			// ...
		}
		catch (Exception e)
		{
			return null;
		}

		return parametres;
	}

	private String getRequete()
	{
		return "Select....";
	}

	/**
	 * Retourne la connexion JDBC � la BD.
	 * 
	 * @return Retourne la connextion � la BD
	 */
	private Connection getConnection() throws ExceptionInterface
	{
		String context = System.getProperty(IMPORT_EXPORT_CONTEXT);

		Connection connection = null;

		try
		{
			connection = null;

			if (context == null || context.equalsIgnoreCase("remote"))
			{
				connection = importDS.getConnection();
			}
			// Temporaire en attendant un correctif de 360
			else
			{
				DriverManager.registerDriver((Driver) Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance());
				connection = DriverManager.getConnection("jdbc:jtds:sqlserver://127.0.0.1:1433/BC360MACExemples", "sa", "victor");
				connection.setAutoCommit(false);
			}

			return connection;
		}
		catch (Exception e)
		{
			logger.error("Impossible d'obtenir une connexion � la DB: " + e.getMessage());

			try
			{
				if (connection != null)
				{
					connection.close();
					connection = null;
				}
			}
			catch (Exception e1)
			{
				logger.error("Impossible de terminer la connexion: " + e1.getLocalizedMessage());
			}
		}

		return null;
	}
}
