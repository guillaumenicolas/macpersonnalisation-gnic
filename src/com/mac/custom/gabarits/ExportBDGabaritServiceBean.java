package com.mac.custom.gabarits;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import com.mac.custom.exception.ExceptionInterface;
import com.netappsid.erp.server.bo.Contact;
import com.netappsid.services.interfaces.local.LoaderLocal;
import com.netappsid.services.interfaces.local.PersistenceLocal;

@Stateless
public class ExportBDGabaritServiceBean implements ExportBDGabaritServiceLocal, ExportBDGabaritServiceRemote
{
	private static Logger logger = Logger.getLogger(ExportBDGabaritServiceBean.class);

	private static String IMPORT_EXPORT_CONTEXT = "importExportContext";

	private String context = System.getProperty(IMPORT_EXPORT_CONTEXT);

	@EJB(mappedName = "erp/ExportBDGabaritGeneriqueServiceBean/local")
	public ExportBDGabaritGeneriqueServiceLocal exportBDServiceLocal;

	@EJB(mappedName = "erp/LoaderBean/local")
	public LoaderLocal loader;

	@EJB(mappedName = "erp/PersistenceBean/local")
	public PersistenceLocal persistence;

	@Resource(mappedName = "java:/INTCOMPTADS")
	private DataSource importDS;

	@Override
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public void methodeExportation() throws ExceptionInterface
	{
		logger.debug("D�but de l'exportation [NOM DE L'IMPORTATION] ");

		exportationBatch();

		logger.debug("FIN de l'exportation [NOM DE L'IMPORTATION]");

	}

	@TransactionAttribute(TransactionAttributeType.NEVER)
	private void exportationBatch() throws ExceptionInterface
	{
		Connection connection = getConnection();

		if (connection == null)
		{
			throw new ExceptionInterface("Aucune connection de sp�cifi�", logger);
		}

		try
		{
			// ----------------------

			// Ici on va chercher les valeurs par d�faut et on valide que tout est d�fini

			// /---------------------

			//

			// On r�cup�re la liste des Objets � exporter

			List<Contact> listContacts = getListeObjetsAExporter();

			final StringBuffer sql = new StringBuffer();

			sql.append("INSERT INTO CONTACTS_EXPORT VALUES (");
			sql.append("CONT_EXPO_SOCI");
			sql.append(", CONT_EXPO_DTCR");
			// sql.append(", CONT_EXPO_CODE_INTE");
			// sql.append(", CONT_EXPO_ERREUR");
			sql.append(", CONT_EXPO_CODE_CRM");
			// sql.append(", CONT_EXPO_CODE_ERP");
			// sql.append(", CONT_EXPO_CIVILITE");
			sql.append(", CONT_EXPO_NOM");
			sql.append(", CONT_EXPO_PRENOM");
			// sql.append(", CONT_EXPO_FONCTION");
			// sql.append(", CONT_EXPO_TEL");
			// sql.append(", CONT_EXPO_PORTABLE");
			// sql.append(", CONT_EXPO_FAX");
			// sql.append(", CONT_EXPO_EMAIL");
			// sql.append(", CONT_EXPO_CODE_COMPTE_PROSPECT");
			// sql.append(", CONT_EXPO_CODE_COMPTE_CLIENT");
			// sql.append(", CONT_EXPO_ACCE_ESPA_PRO");
			// sql.append(", CONT_EXPO_ACCE_COMPLET_CCS");
			// sql.append(", CONT_EXPO_ACCE_GRAND_PUBLIQUE");
			// sql.append(", CONT_EXPO_NFPPS");

			sql.append(") (");

			sql.append("?,?,?,?,?");

			PreparedStatement statement = connection.prepareStatement(sql.toString());

			for (Contact contact360 : listContacts)
			{

				traitementDeLaLigne(statement, contact360);
				if (context != null && context.equalsIgnoreCase("local"))
				{
					connection.commit();
				}

			}

		}

		catch (SQLException e)
		{
			try
			{
				connection.close();
				connection = null;
			}
			catch (SQLException e1)
			{
				throw new ExceptionInterface("Impossible de terminer la connexion: " + e1.getLocalizedMessage(), logger);
			}
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void traitementDeLaLigne(PreparedStatement statement, Contact contact) throws ExceptionInterface, SQLException
	{
		// ----------------------

		statement.setString(1, "FA");
		statement.setString(2, "01/01/2013");
		statement.setString(3, contact.getCode());
		statement.setString(4, contact.getLastName());
		statement.setString(5, contact.getFirstName());

		statement.executeUpdate();

	}

	private List<Contact> getListeObjetsAExporter()
	{
		// calcul de la date du jour - 10J
		Calendar calendrier = Calendar.getInstance();
		calendrier.add(Calendar.DAY_OF_MONTH, -10);

		// Recherche des clients
		List<Criterion> criteria = new ArrayList<Criterion>();
		List<String> subcriteria = new ArrayList<String>();
		List<Integer> joinSpec = new ArrayList<Integer>();

		criteria.add(Restrictions.ilike(Contact.PROPERTYNAME_LASTNAME, "N", MatchMode.START));
		subcriteria.add(null);
		joinSpec.add(null);

		List<Contact> contacts = loader.findByCriteria(Contact.class, Contact.class, criteria, subcriteria, joinSpec);

		// listContacts = loader.findByField(Contact.class, Contact.class, Contact.PROPERTYNAME_MODIFDATE, calendrier.getTime());

		return contacts;
	}

	/**
	 * Retourne la connexion JDBC � la BD.
	 * 
	 * @return Retourne la connextion � la BD
	 */
	private Connection getConnection() throws ExceptionInterface
	{

		Connection connection = null;

		try
		{
			connection = null;

			if (context == null || context.equalsIgnoreCase("remote"))
			{
				connection = importDS.getConnection();
			}
			// Temporaire en attendant un correctif de 360
			else
			{
				DriverManager.registerDriver((Driver) Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance());
				connection = DriverManager.getConnection("jdbc:jtds:sqlserver://s8-crm-t02:1433/CRM_INTERFACE", "ADMIN_CRM", "octal");
				connection.setAutoCommit(false);
			}

			return connection;
		}
		catch (Exception e)
		{
			logger.error("Impossible d'obtenir une connexion � la DB: " + e.getMessage());

			try
			{
				if (connection != null)
				{
					connection.close();
					connection = null;
				}
			}
			catch (Exception e1)
			{
				logger.error("Impossible de terminer la connexion: " + e1.getLocalizedMessage());
			}
		}

		return null;
	}
}
