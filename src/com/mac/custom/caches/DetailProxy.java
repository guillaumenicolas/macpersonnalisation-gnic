package com.mac.custom.caches;

import java.util.ArrayList;
import java.util.List;

import com.netappsid.erp.server.bo.TransactionDetail;

public class DetailProxy
{
	private TransactionDetail detail;
	List<ConfiguratorInformation> configuratorProxies;

	public DetailProxy(TransactionDetail detail)
	{
		this.detail = detail;
	}

	public List<ConfiguratorInformation> getConfigurator()
	{
		if (configuratorProxies == null)
		{
			Object configuratorInformation = detail.getConfiguratorCache().getValue("ConfiguratorCache");

			if (configuratorInformation != null && configuratorInformation instanceof List<?>)
			{
				configuratorProxies = (List<ConfiguratorInformation>) configuratorInformation;
			}
			else
			{
				configuratorProxies = new ArrayList<ConfiguratorInformation>();
			}
		}

		return configuratorProxies;
	}
}
