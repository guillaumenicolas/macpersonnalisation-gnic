package com.mac.custom.caches;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.netappsid.configurator.IConfigurable;
import com.netappsid.configurator.IConfigurator;
import com.netappsid.configurator.cache.ConfiguratorValueStrategy;
import com.netappsid.utils.ReflectionUtils;

public class ConfiguratorCache implements ConfiguratorValueStrategy
{
	private static final Logger logger = Logger.getLogger(ConfiguratorCache.class);

	@Override
	public Serializable getValue(IConfigurator configurator)
	{

		List<ConfiguratorInformation> configuratorInformationList = new ArrayList<ConfiguratorInformation>();

		try
		{
			final List<IConfigurable> configurables = configurator.getConfigurables();

			for (IConfigurable item : configurables)
			{
				String elementDescription = callMethodString(item, "getElementDescription");
				Double basePrice = callMethodDouble(item, "getBasePrice");
				Map<String, Double> prices = callMethodDoubleMap(item, "getPrices");
				Map<String, String> pricesDescriptions = callMethodStringMap(item, "getPriceDescriptions");

				ConfiguratorInformation configuratorInformation = new ConfiguratorInformation();

				configuratorInformation.setElementDescription(elementDescription);
				configuratorInformation.setBasePrice(basePrice);
				configuratorInformation.setPrices(prices);
				configuratorInformation.setPricesDescriptions(pricesDescriptions);

				configuratorInformationList.add(configuratorInformation);
			}

		}
		catch (Exception e)
		{
			logger.warn(e);
		}

		return (Serializable) configuratorInformationList;
	}

	private Map<String, String> callMethodStringMap(IConfigurable item, String methodName)
	{
		Object value = null;
		Map<String, String> result = null;

		value = callMethod(item, methodName);

		if (value instanceof Map<?, ?>)
		{
			result = (Map<String, String>) value;
		}

		return result;
	}

	private Map<String, Double> callMethodDoubleMap(IConfigurable item, String methodName)
	{
		Object value = null;
		Map<String, Double> result = null;

		value = callMethod(item, methodName);

		if (value instanceof Map<?, ?>)
		{
			result = (Map<String, Double>) value;
		}

		return result;
	}

	private String callMethodString(IConfigurable item, String methodName)
	{
		Object value = null;
		String result = null;

		value = callMethod(item, methodName);

		if (value instanceof String)
		{
			result = (String) value;
		}

		return result;
	}

	private Double callMethodDouble(IConfigurable item, String methodName)
	{
		Object value = null;
		Double result = null;

		value = callMethod(item, methodName);

		if (value instanceof Double)
		{
			result = (Double) value;
		}

		return result;
	}

	private Object callMethod(IConfigurable item, String methodName)
	{
		Method method = ReflectionUtils.getDeclaredMethod(methodName, item.getObject().getClass(), false);
		try
		{
			return method.invoke(item.getObject());
		}
		catch (IllegalArgumentException e)
		{
			throw new IllegalStateException("De mauvais arguments ont été passés à la méthode " + methodName + " de l'objet " + item.getClass()
					+ " lors de la création de la ConfiguratorCache", e);
		}
		catch (IllegalAccessException e)
		{
			throw new IllegalStateException("La methode " + methodName + " n'est pas accessible (private) sur l'objet " + item.getClass()
					+ " lors de la création de la ConfiguratorCache", e);
		}
		catch (InvocationTargetException e)
		{
			throw new IllegalStateException("Erreur lors de l'exécution de la " + methodName + " sur l'objet " + item.getClass()
					+ " lors de la création de la ConfiguratorCache", e);
		}
	}
}
