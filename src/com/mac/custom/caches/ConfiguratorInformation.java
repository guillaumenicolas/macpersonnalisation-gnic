package com.mac.custom.caches;

import java.io.Serializable;
import java.util.Map;

public class ConfiguratorInformation implements Serializable
{
	private static final long serialVersionUID = 5420190264836469419L;

	private String elementDescription;
	private Double basePrice;
	private Map<String, Double> prices;
	private Map<String, String> pricesDescriptions;

	public void setElementDescription(String elementDescription)
	{
		this.elementDescription = elementDescription;
	}

	public void setBasePrice(Double basePrice)
	{
		this.basePrice = basePrice;
	}

	public void setPrices(Map<String, Double> prices)
	{
		this.prices = prices;
	}

	public void setPricesDescriptions(Map<String, String> pricesDescriptions)
	{
		this.pricesDescriptions = pricesDescriptions;
	}

	public String getElementDescription()
	{
		return elementDescription;
	}

	public Map<String, Double> getPrices()
	{
		return prices;
	}

	public Double getBasePrice()
	{
		return basePrice;
	}

	public Map<String, String> getPricesDescriptions()
	{
		return pricesDescriptions;
	}

}
