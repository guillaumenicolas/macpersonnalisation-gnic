package com.mac.custom.exception;

import org.apache.log4j.Logger;

@SuppressWarnings("serial")
public class ExceptionInterface extends Exception
{
	private Exception exception = null;

	public ExceptionInterface(Exception exception, Logger logger)
	{
		this.exception = exception;

		if (logger != null)
		{
			logger.error(exception.getMessage());
		}
	}

	public ExceptionInterface(String exception, Logger logger)
	{
		this.exception = new Exception(exception);

		if (logger != null)
		{
			logger.error(exception);
		}
	}

	public Exception getException()
	{
		return exception;
	}

	public void setException(Exception exception)
	{
		this.exception = exception;
	}
}
