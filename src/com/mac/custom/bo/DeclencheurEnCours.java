// generated class, do not edit

package com.mac.custom.bo;

// Imports 
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.mac.custom.dao.DeclencheurEnCoursDAO;
import com.netappsid.annotations.DAO;
import com.netappsid.dao.PersistenceListener;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "standard")
@DAO(dao = DeclencheurEnCoursDAO.class)
@EntityListeners(value = { PersistenceListener.class })
@Table(name = "DeclencheurEnCours", schema = "MacSchema")
public class DeclencheurEnCours extends com.netappsid.bo.Entity implements Serializable
{
	private Serializable id;
	private String entityClassName;
	private Serializable entityId;

	@Override
	@Id
	@GeneratedValue(generator = "naid-uuid")
	@GenericGenerator(name = "naid-uuid", strategy = "com.netappsid.framework.utils.UUIDGenerator")
	@Type(type = "com.netappsid.datatypes.UUIDUserType")
	@AccessType(value = "field")
	public Serializable getId()
	{
		return id;
	}

	@Override
	public void setId(Serializable id)
	{
		this.id = id;
	}

	public void setEntityClassName(String entityClassName)
	{
		String oldValue = getEntityClassName();
		String newValue = entityClassName;
		if (newValue != null && newValue.length() == 0)
		{
			newValue = null;
		}
		this.entityClassName = newValue;
		firePropertyChange(PROPERTYNAME_ENTITYCLASSNAME, oldValue, newValue);
	}

	@Column(name = "entityClassName")
	@org.hibernate.validator.NotNull
	@com.netappsid.annotations.NotNull
	@com.netappsid.annotations.Length(max = 255)
	@AccessType(value = "field")
	public String getEntityClassName()
	{
		return entityClassName;
	}

	public void setEntityId(Serializable entityId)
	{
		Serializable oldValue = getEntityId();
		this.entityId = entityId;
		firePropertyChange(PROPERTYNAME_ENTITYID, oldValue, entityId);
	}

	@Type(type = "com.netappsid.datatypes.UUIDUserType")
	@Column(name = "entityId")
	@AccessType(value = "field")
	public Serializable getEntityId()
	{
		return entityId;
	}

	public static final String PROPERTYNAME_ENTITYCLASSNAME = "entityClassName";
	public static final String PROPERTYNAME_ENTITYID = "entityId";
}
