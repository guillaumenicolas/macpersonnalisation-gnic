package com.mac.custom.bo.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

import com.jgoodies.binding.value.ValueModel;
import com.mac.custom.bo.MacCompany;
import com.netappsid.annotations.DAO;
import com.netappsid.erp.server.bo.Company;
import com.netappsid.erp.server.bo.InventoryProduct;
import com.netappsid.erp.server.bo.MeasureUnitConversion;
import com.netappsid.erp.server.bo.MeasureUnitMultiplier;
import com.netappsid.erp.server.bo.Relocation;
import com.netappsid.erp.server.bo.RelocationDetail;
import com.netappsid.erp.server.bo.RelocationDetailFrom;
import com.netappsid.erp.server.bo.RelocationDetailTo;
import com.netappsid.erp.server.bo.User;
import com.netappsid.erp.server.bo.Warehouse;
import com.netappsid.erp.server.dao.RelocationDetailDAO;
import com.netappsid.erp.server.datatypes.GroupMeasureValue;
import com.netappsid.erp.server.model.form.LocaleEditingFormModel;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;
import com.netappsid.framework.utils.SystemVariable;

@DAO(dao = RelocationDetailDAO.class)
public class MacRelocalisationFormModel extends LocaleEditingFormModel
{
	private String barcode;
	private Warehouse warehouse;
	private List<Warehouse> warehouses;
	private User user;
	private Company company;
	private GroupMeasureValue quantity;
	private com.netappsid.erp.server.bo.GroupMeasure quantityGroupMeasure;
	private BigDecimal quantityValue;
	private MeasureUnitConversion quantityConversion;
	private MeasureUnitMultiplier quantityConvertedUnit;
	private BigDecimal quantityConvertedValue;
	private Relocation relocation;
	private RelocationDetail relocationDetail;
	private Date date;
	private RelocationDetailFrom relocationDetailFrom;
	private InventoryProduct inventoryProduct;
	public static final String PROPERTYNAME_BARCODE = "barCode";
	public static final String PROPERTYNAME_WAREHOUSE = "warehouse";
	public static final String PROPERTYNAME_USER = "user";
	public static final String PROPERTYNAME_QUANTITY = "quantity";
	public static final String PROPERTYNAME_QUANTITYGROUPMEASURE = "quantityGroupMeasure";
	public static final String PROPERTYNAME_QUANTITYVALUE = "quantityValue";
	public static final String PROPERTYNAME_QUANTITYCONVERSION = "quantityConversion";
	public static final String PROPERTYNAME_QUANTITYCONVERTEDUNIT = "quantityConvertedUnit";
	public static final String PROPERTYNAME_QUANTITYCONVERTEDVALUE = "quantityConvertedValue";
	public static final String PROPERTYNAME_RELOCATION = "relocation";
	public static final String PROPERTYNAME_RELOCATIONDETAIL = "relocationDetail";
	public static final String PROPERTYNAME_RELOCATIONDETAILFROM = "relocationDetailFrom";
	public static final String PROPERTYNAME_INVENTORYPRODUCT = "inventoryProduct";

	@Override
	protected void formModelBeanChanged(Object oldValue, Object newValue)
	{
		super.formModelBeanChanged(oldValue, newValue);

	}

	public MacRelocalisationFormModel(NAIDPresentationModel businessObjectModel, ValueModel localeValueModel)
	{
		super(businessObjectModel, localeValueModel);
	}

	public MacRelocalisationFormModel(NAIDPresentationModel businessObjectModel)
	{
		// TODO Auto-generated constructor stub
		super(businessObjectModel, businessObjectModel.getModel("locale"));
		setRelocationDetail((RelocationDetail) businessObjectModel.getBean());
		setRelocation(new Relocation());
		relocation.addToDetails(relocationDetail);
		setRelocationDetailFrom(new RelocationDetailFrom());
		relocationDetailFrom.setRelocationDetail(relocationDetail);

		setInventoryProduct(null);
		setBarcode(null);
		setUser((User) SystemVariable.getVariable("user"));
		setCompany(getUser().getCompany());
		relocationDetail.addToRelocationDetailsTo(new RelocationDetailTo());
		relocationDetail.getRelocationDetailsTo().get(0).setLocation(((MacCompany) company).getDefaultInventoryAdjustmentLocation());
	}

	private void setRelocationDetail(RelocationDetail relocationDetail)
	{
		RelocationDetail oldValue = this.relocationDetail;
		this.relocationDetail = relocationDetail;

	}

	public List<Warehouse> getWarehouses()
	{
		return this.warehouses;
	}

	public InventoryProduct getInventoryProduct()
	{
		return this.inventoryProduct;
	}

	public void setInventoryProduct(InventoryProduct inventoryProduct)
	{
		InventoryProduct oldValue = this.inventoryProduct;
		this.inventoryProduct = inventoryProduct;
		this.relocationDetail.setInventoryProduct(inventoryProduct);
		firePropertyChange("inventoryProduct", oldValue, this.inventoryProduct);
	}

	public void setBarcode(String barcode)
	{
		String oldValue = getBarcode();
		String newValue = barcode;
		if ((newValue != null) && (newValue.length() == 0))
		{
			newValue = null;
		}
		this.barcode = newValue;
		firePropertyChange("barcode", oldValue, newValue);
	}

	public String getBarcode()
	{
		return this.barcode;
	}

	public Company getCompany()
	{
		return this.company;
	}

	public void setCompany(Company company)
	{
		Company oldValue = getCompany();
		Company newValue = company;

		this.company = newValue;
		this.relocation.setCompany(newValue);
		setWarehouse(this.company.getDefaultWarehouse());
		firePropertyChange("company", oldValue, newValue);
	}

	public Date getDate()
	{
		return this.date;
	}

	public void setDate(Date date)
	{
		Date oldValue = getDate();
		Date newValue = date;

		this.date = newValue;
		this.relocation.setDate(newValue);
		firePropertyChange("date", oldValue, newValue);
	}

	public Warehouse getWarehouse()
	{
		return this.warehouse;
	}

	public void setWarehouse(Warehouse warehouse)
	{
		Warehouse oldValue = this.warehouse;
		this.warehouse = warehouse;
		this.relocation.setWarehouse(warehouse);
		firePropertyChange("warehouse", oldValue, this.warehouse);
	}

	public Relocation getRelocation()
	{
		return this.relocation;
	}

	public void setRelocation(Relocation relocation)
	{
		Relocation oldValue = this.relocation;
		this.relocation = relocation;
	}

	public RelocationDetailFrom getRelocationDetailFrom()
	{
		return this.relocationDetailFrom;
	}

	public void setRelocationDetailFrom(RelocationDetailFrom relocationDetailFrom)
	{
		RelocationDetailFrom oldValue = this.relocationDetailFrom;
		this.relocationDetailFrom = relocationDetailFrom;
		this.relocationDetail.setRelocationDetailFrom(relocationDetailFrom);
		firePropertyChange("relocationDetailFrom", oldValue, this.relocationDetailFrom);
	}

	public void setQuantity(GroupMeasureValue quantity)
	{
		GroupMeasureValue oldValue = this.quantity;
		this.quantity = quantity;
		if (quantity != null)
		{
			setQuantityGroupMeasure(quantity.getGroupMeasure());
			setQuantityValue(quantity.getValue());
			setQuantityConvertedUnit(quantity.getConvertedUnit());
			setQuantityConversion(quantity.getConversion());
			setQuantityConvertedValue(quantity.getConvertedValue());
		}
		else
		{
			setQuantityGroupMeasure(null);
			setQuantityConvertedUnit(null);
			setQuantityConvertedValue(null);
			setQuantityConversion(null);
			setQuantityValue(null);
		}
		this.relocationDetail.setQuantity(quantity);
		if (quantity != null)
		{
			relocationDetail.getRelocationDetailsTo().get(0).setQuantity(this.quantity);
		}
		;
		firePropertyChange("quantity", oldValue, quantity);
	}

	public GroupMeasureValue getQuantity()
	{
		if (this.quantity == null)
		{
			this.quantity = new GroupMeasureValue();
			this.quantity.initialize(getQuantityGroupMeasure(), getQuantityConvertedUnit(), getQuantityConvertedValue(), getQuantityConversion(),
					getQuantityValue(), true);
		}

		return this.quantity;
	}

	private void setQuantityGroupMeasure(com.netappsid.erp.server.bo.GroupMeasure quantityGroupMeasure)
	{
		com.netappsid.erp.server.bo.GroupMeasure oldValue = getQuantityGroupMeasure();
		this.quantityGroupMeasure = quantityGroupMeasure;

		firePropertyChange("quantityGroupMeasure", oldValue, quantityGroupMeasure);
	}

	public com.netappsid.erp.server.bo.GroupMeasure getQuantityGroupMeasure()
	{
		return this.quantityGroupMeasure;
	}

	private void setQuantityValue(BigDecimal quantityValue)
	{
		BigDecimal oldValue = getQuantityValue();
		this.quantityValue = quantityValue;
		if (this.quantityValue != null)
		{
			this.quantityValue = this.quantityValue.setScale(10, RoundingMode.HALF_UP);
		}
		firePropertyChange("quantityValue", oldValue, quantityValue);
	}

	public BigDecimal getQuantityValue()
	{
		return this.quantityValue;
	}

	private void setQuantityConversion(MeasureUnitConversion quantityConversion)
	{
		MeasureUnitConversion oldValue = getQuantityConversion();
		this.quantityConversion = quantityConversion;
		firePropertyChange("quantityConversion", oldValue, quantityConversion);
	}

	public MeasureUnitConversion getQuantityConversion()
	{
		return this.quantityConversion;
	}

	private void setQuantityConvertedUnit(MeasureUnitMultiplier quantityConvertedUnit)
	{
		MeasureUnitMultiplier oldValue = getQuantityConvertedUnit();
		this.quantityConvertedUnit = quantityConvertedUnit;
		firePropertyChange("quantityConvertedUnit", oldValue, quantityConvertedUnit);
	}

	public MeasureUnitMultiplier getQuantityConvertedUnit()
	{
		return this.quantityConvertedUnit;
	}

	private void setQuantityConvertedValue(BigDecimal quantityConvertedValue)
	{
		BigDecimal oldValue = getQuantityConvertedValue();
		this.quantityConvertedValue = quantityConvertedValue;
		if (this.quantityConvertedValue != null)
		{
			this.quantityConvertedValue = this.quantityConvertedValue.setScale(10, RoundingMode.HALF_UP);
		}
		firePropertyChange("quantityConvertedValue", oldValue, quantityConvertedValue);
	}

	public BigDecimal getQuantityConvertedValue()
	{
		return this.quantityConvertedValue;
	}

	public User getUser()
	{
		return this.user;
	}

	public void setUser(User user)
	{
		User oldValue = this.user;
		this.user = user;
		this.relocation.setUser(user);
		firePropertyChange("user", oldValue, this.user);
	}

}
