package com.mac.custom.bo.model;

import java.util.Date;

import com.netappsid.annotations.DAO;
import com.netappsid.bo.model.Model;
import com.netappsid.erp.server.bo.Location;
import com.netappsid.erp.server.dao.RelocationDetailDAO;

@DAO(dao = RelocationDetailDAO.class)
public class ProductionRestocking extends Model
{
	private Date dateFrom;
	private Date dateTo;
	private Location storageLocation;
	private Location productionLocation;

	public void setDateFrom(Date dateFrom)
	{
		Date oldValue = getDateFrom();
		this.dateFrom = dateFrom == null ? null : new Date(dateFrom.getTime());
		firePropertyChange(PROPERTYNAME_DATEFROM, oldValue, dateFrom);
	}

	@com.netappsid.annotations.NotNull
	public Date getDateFrom()
	{
		return dateFrom == null ? null : new Date(dateFrom.getTime());
	}

	public void setDateTo(Date dateTo)
	{
		Date oldValue = getDateTo();
		this.dateTo = dateTo == null ? null : new Date(dateTo.getTime());
		firePropertyChange(PROPERTYNAME_DATETO, oldValue, dateTo);
	}

	@com.netappsid.annotations.NotNull
	public Date getDateTo()
	{
		return dateTo == null ? null : new Date(dateTo.getTime());
	}

	@com.netappsid.annotations.NotNull
	public Location getStorageLocation()
	{
		return storageLocation;
	}

	public void setStorageLocation(Location storageLocation)
	{
		Location oldValue = this.storageLocation;
		this.storageLocation = storageLocation;
		firePropertyChange(PROPERTYNAME_STORAGELOCATION, oldValue, this.storageLocation);
	}

	@com.netappsid.annotations.NotNull
	public Location getProductionLocation()
	{
		return productionLocation;
	}

	public void setProductionLocation(Location productionLocation)
	{
		Location oldValue = this.productionLocation;
		this.productionLocation = productionLocation;
		firePropertyChange(PROPERTYNAME_PRODUCTIONLOCATION, oldValue, this.productionLocation);
	}

	public static final String PROPERTYNAME_PRODUCTIONLOCATION = "productionLocation";
	public static final String PROPERTYNAME_STORAGELOCATION = "storageLocation";
	public static final String PROPERTYNAME_DATEFROM = "dateFrom";
	public static final String PROPERTYNAME_DATETO = "dateTo";
}
