package com.mac.custom.bo.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

import javax.persistence.Transient;

import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.message.SimpleValidationMessage;
import com.jidesoft.combobox.DateModel;
import com.mac.custom.bo.WorkLoadDetail;
import com.mac.custom.utils.PostponedReasonsUtils;
import com.netappsid.annotations.NotNull;
import com.netappsid.annotations.Scale;
import com.netappsid.annotations.Validate;
import com.netappsid.bo.model.Model;
import com.netappsid.erp.server.bo.GroupMeasure;
import com.netappsid.erp.server.bo.MeasureUnitConversion;
import com.netappsid.erp.server.bo.MeasureUnitMultiplier;
import com.netappsid.erp.server.datatypes.GroupMeasureValue;
import com.netappsid.erp.server.model.ShippingDateModel;
import com.netappsid.europe.naid.custom.bo.PostponedReason;
import com.netappsid.observable.ObservableCollections;
import com.netappsid.resources.Translator;

/**
 * Model used to move a quantity from a {@link WorkLoadDetail}, it simply contains a date and a quantity.
 * 
 * @author plefebvre
 */
public class WorkLoadDetailQuantityMoveModel extends Model
{
	private Date date;
	private GroupMeasureValue quantity;
	private GroupMeasure quantityGroupMeasure;
	private BigDecimal quantityValue;
	private MeasureUnitConversion quantityConversion;
	private MeasureUnitMultiplier quantityConvertedUnit;
	private BigDecimal quantityConvertedValue;
	private Date orderShippingDate;
	private PostponedReason postponedReason;
	private boolean moveOrderShippingDate;
	private boolean moveWorkLoadDetail = true;
	private boolean autoSelectShippingDate = true;

	private List<PostponedReason> availablePostponedReasons;
	private ShippingDateModel shippingDateModel = new ShippingDateModel();

	@NotNull
	public Date getDate()
	{
		return date;
	}

	public void setDate(Date date)
	{
		Date oldValue = getDate();
		this.date = date;
		firePropertyChange(PROPERTYNAME_DATE, oldValue, date, false);
	}

	public void setQuantity(GroupMeasureValue quantity)
	{
		GroupMeasureValue oldValue = this.quantity;
		this.quantity = quantity;
		if (quantity != null)
		{
			setQuantityGroupMeasure(quantity.getGroupMeasure());
			setQuantityValue(quantity.getValue());
			setQuantityConvertedUnit(quantity.getConvertedUnit());
			setQuantityConversion(quantity.getConversion());
			setQuantityConvertedValue(quantity.getConvertedValue());
		}
		else
		{
			setQuantityGroupMeasure(null);
			setQuantityConvertedUnit(null);
			setQuantityConvertedValue(null);
			setQuantityConversion(null);
			setQuantityValue(null);
		}
		firePropertyChange(PROPERTYNAME_QUANTITY, oldValue, quantity);
	}

	@com.netappsid.erp.annotations.GroupMeasure(showQuantity = true)
	@Scale(decimal = 10)
	@Validate
	public GroupMeasureValue getQuantity()
	{
		if (quantity == null)
		{
			quantity = new GroupMeasureValue();
			quantity.initialize(getQuantityGroupMeasure(), getQuantityConvertedUnit(), getQuantityConvertedValue(), getQuantityConversion(),
					getQuantityValue(), true);
		}

		return quantity;
	}

	private void setQuantityGroupMeasure(GroupMeasure quantityGroupMeasure)
	{
		GroupMeasure oldValue = getQuantityGroupMeasure();
		this.quantityGroupMeasure = quantityGroupMeasure;
		firePropertyChange(PROPERTYNAME_QUANTITYGROUPMEASURE, oldValue, quantityGroupMeasure);
	}

	public GroupMeasure getQuantityGroupMeasure()
	{
		return quantityGroupMeasure;
	}

	private void setQuantityValue(BigDecimal quantityValue)
	{
		BigDecimal oldValue = getQuantityValue();
		this.quantityValue = quantityValue;
		if (this.quantityValue != null)
		{
			this.quantityValue = this.quantityValue.setScale(10, RoundingMode.HALF_UP);
		}
		firePropertyChange(PROPERTYNAME_QUANTITYVALUE, oldValue, quantityValue);
	}

	public BigDecimal getQuantityValue()
	{
		return quantityValue;
	}

	private void setQuantityConversion(MeasureUnitConversion quantityConversion)
	{
		MeasureUnitConversion oldValue = getQuantityConversion();
		this.quantityConversion = quantityConversion;
		firePropertyChange(PROPERTYNAME_QUANTITYCONVERSION, oldValue, quantityConversion);
	}

	public MeasureUnitConversion getQuantityConversion()
	{
		return quantityConversion;
	}

	private void setQuantityConvertedUnit(MeasureUnitMultiplier quantityConvertedUnit)
	{
		MeasureUnitMultiplier oldValue = getQuantityConvertedUnit();
		this.quantityConvertedUnit = quantityConvertedUnit;
		firePropertyChange(PROPERTYNAME_QUANTITYCONVERTEDUNIT, oldValue, quantityConvertedUnit);
	}

	public MeasureUnitMultiplier getQuantityConvertedUnit()
	{
		return quantityConvertedUnit;
	}

	private void setQuantityConvertedValue(BigDecimal quantityConvertedValue)
	{
		BigDecimal oldValue = getQuantityConvertedValue();
		this.quantityConvertedValue = quantityConvertedValue;
		if (this.quantityConvertedValue != null)
		{
			this.quantityConvertedValue = this.quantityConvertedValue.setScale(10, RoundingMode.HALF_UP);
		}
		firePropertyChange(PROPERTYNAME_QUANTITYCONVERTEDVALUE, oldValue, quantityConvertedValue);
	}

	public BigDecimal getQuantityConvertedValue()
	{
		return quantityConvertedValue;
	}

	public Date getOrderShippingDate()
	{
		return orderShippingDate;
	}

	public void setOrderShippingDate(Date orderShippingDate)
	{
		Date oldValue = getOrderShippingDate();
		this.orderShippingDate = orderShippingDate;
		firePropertyChange(PROPERTYNAME_ORDERSHIPPINGDATE, oldValue, orderShippingDate);
	}

	public PostponedReason getPostponedReason()
	{
		return postponedReason;
	}

	public void setPostponedReason(PostponedReason postponedReason)
	{
		PostponedReason oldValue = getPostponedReason();
		this.postponedReason = postponedReason;
		firePropertyChange(PROPERTYNAME_POSTPONEDREASON, oldValue, postponedReason);
	}

	public boolean isMoveOrderShippingDate()
	{
		return moveOrderShippingDate;
	}

	public void setMoveOrderShippingDate(boolean moveOrderShippingDate)
	{
		boolean oldValue = isMoveOrderShippingDate();
		this.moveOrderShippingDate = moveOrderShippingDate;
		firePropertyChange(PROPERTYNAME_MOVEORDERSHIPPINGDATE, oldValue, moveOrderShippingDate);
	}

	/**
	 * Returns all postponed reasons except for the automatic date one, which is a system-defined one. The list is being sorted prior to being returned.
	 * 
	 * @return
	 */
	@Transient
	public List<PostponedReason> getAvailablePostponedReasons()
	{
		if (availablePostponedReasons == null)
		{
			availablePostponedReasons = ObservableCollections.newObservableArrayList();

			PostponedReasonsUtils postponedReasonsUtils = new PostponedReasonsUtils();
			availablePostponedReasons.addAll(postponedReasonsUtils.getAvailablePostponedReasons());
		}

		return availablePostponedReasons;
	}

	@Transient
	public DateModel getShippingDateModel()
	{
		return shippingDateModel;
	}

	public void setShippingDateModel(ShippingDateModel shippingDateModel)
	{
		this.shippingDateModel = shippingDateModel;
	}

	public boolean isMoveWorkLoadDetail()
	{
		return moveWorkLoadDetail;
	}

	public void setMoveWorkLoadDetail(boolean moveWorkLoadDetail)
	{
		boolean oldValue = isMoveWorkLoadDetail();
		this.moveWorkLoadDetail = moveWorkLoadDetail;
		firePropertyChange(PROPERTYNAME_MOVEWORKLOADDETAIL, oldValue, moveWorkLoadDetail);
	}

	public boolean isAutoSelectShippingDate()
	{
		return autoSelectShippingDate;
	}

	public void setAutoSelectShippingDate(boolean autoSelectShippingDate)
	{
		boolean oldValue = isAutoSelectShippingDate();
		this.autoSelectShippingDate = autoSelectShippingDate;
		firePropertyChange(PROPERTYNAME_AUTOSELECTSHIPPINGDATE, oldValue, autoSelectShippingDate);
	}

	@Override
	public ValidationResult validate()
	{
		ValidationResult result = super.validate();

		if (isMoveOrderShippingDate() && getPostponedReason() == null)
		{
			result.add(new SimpleValidationMessage(Translator.getString("postponedReason") + Translator.getString("IsMandatory"), Severity.ERROR));
		}

		if (!isMoveOrderShippingDate() && !isMoveWorkLoadDetail())
		{
			result.add(new SimpleValidationMessage(Translator.getString("noMoveWorkLoadOrShippingDateSelected"), Severity.ERROR));
		}

		if (getQuantity() != null && getQuantity().isValid() && !getQuantity().isGreaterThanZero())
		{
			result.add(new SimpleValidationMessage(Translator.getString("cannotMoveQuantityMustBePositive"), Severity.ERROR));
		}

		if (getDate() != null && getOrderShippingDate() != null && getDate().after(getOrderShippingDate()) && isMoveWorkLoadDetail()
				&& isMoveOrderShippingDate() && !isAutoSelectShippingDate())
		{
			result.add(new SimpleValidationMessage(Translator.getString("dateCannotBeAfterShippingDate"), Severity.ERROR));
		}

		if (isMoveOrderShippingDate() && !isAutoSelectShippingDate() && getOrderShippingDate() == null)
		{
			result.add(new SimpleValidationMessage(Translator.getString("shippingDate") + Translator.getString("IsMandatory"), Severity.ERROR));
		}

		return result;
	}

	public static final String PROPERTYNAME_DATE = "date";
	public static final String PROPERTYNAME_QUANTITY = "quantity";
	public static final String PROPERTYNAME_QUANTITYGROUPMEASURE = "quantityGroupMeasure";
	public static final String PROPERTYNAME_QUANTITYVALUE = "quantityValue";
	public static final String PROPERTYNAME_QUANTITYCONVERSION = "quantityConversion";
	public static final String PROPERTYNAME_QUANTITYCONVERTEDUNIT = "quantityConvertedUnit";
	public static final String PROPERTYNAME_QUANTITYCONVERTEDVALUE = "quantityConvertedValue";
	public static final String PROPERTYNAME_ORDERSHIPPINGDATE = "orderShippingDate";
	public static final String PROPERTYNAME_POSTPONEDREASON = "postponedReason";
	public static final String PROPERTYNAME_MOVEORDERSHIPPINGDATE = "moveOrderShippingDate";
	public static final String PROPERTYNAME_MOVEWORKLOADDETAIL = "moveWorkLoadDetail";
	public static final String PROPERTYNAME_AUTOSELECTSHIPPINGDATE = "autoSelectShippingDate";
}
