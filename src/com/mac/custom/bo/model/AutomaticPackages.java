package com.mac.custom.bo.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.Cascade;

import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.message.SimpleValidationMessage;
import com.mac.custom.bo.MacOrder;
import com.mac.custom.bo.MacOrderDetail;
import com.mac.custom.dao.MacOrderDAO;
import com.netappsid.annotations.DAO;
import com.netappsid.annotations.Scale;
import com.netappsid.annotations.Validate;
import com.netappsid.bo.model.Model;
import com.netappsid.erp.server.bo.GroupMeasure;
import com.netappsid.erp.server.bo.ItemToShip;
import com.netappsid.erp.server.bo.MeasureUnitConversion;
import com.netappsid.erp.server.bo.MeasureUnitMultiplier;
import com.netappsid.erp.server.bo.OrderDetailToShip;
import com.netappsid.erp.server.bo.Packages;
import com.netappsid.erp.server.datatypes.GroupMeasureValue;
import com.netappsid.observable.ObservableCollections;
import com.netappsid.validation.Validable;

@DAO(dao = MacOrderDAO.class)
public class AutomaticPackages extends Model implements Validable
{
	private GroupMeasureValue weight;
	private GroupMeasure weightGroupMeasure;
	private BigDecimal weightValue;
	private MeasureUnitConversion weightConversion;
	private MeasureUnitMultiplier weightConvertedUnit;
	private BigDecimal weightConvertedValue;
	private Long numberPackages;
	private MacOrder order;
	private Long validIts;
	private final List<AutomaticPackagesItemToShipWrapper> itemToShipWrappers = ObservableCollections.newObservableArrayList();

	public void setWeight(GroupMeasureValue weight)
	{
		GroupMeasureValue oldValue = this.weight;
		this.weight = weight;
		if (weight != null)
		{
			setWeightGroupMeasure(weight.getGroupMeasure());
			setWeightValue(weight.getValue());
			setWeightConvertedUnit(weight.getConvertedUnit());
			setWeightConversion(weight.getConversion());
			setWeightConvertedValue(weight.getConvertedValue());
		}
		else
		{
			setWeightGroupMeasure(null);
			setWeightConvertedUnit(null);
			setWeightConvertedValue(null);
			setWeightConversion(null);
			setWeightValue(null);
		}
		firePropertyChange(PROPERTYNAME_WEIGHT, oldValue, weight);
	}

	@com.netappsid.erp.annotations.GroupMeasure(showQuantity = true)
	@Scale(decimal = 10)
	@Validate
	@Transient
	public GroupMeasureValue getWeight()
	{
		if (weight == null)
		{
			weight = new GroupMeasureValue();
			weight.initialize(getWeightGroupMeasure(), getWeightConvertedUnit(), getWeightConvertedValue(), getWeightConversion(), getWeightValue(), true);
		}

		return weight;
	}

	private void setWeightGroupMeasure(GroupMeasure weightGroupMeasure)
	{
		GroupMeasure oldValue = getWeightGroupMeasure();
		this.weightGroupMeasure = weightGroupMeasure;
		firePropertyChange(PROPERTYNAME_WEIGHTGROUPMEASURE, oldValue, weightGroupMeasure);
	}

	@ManyToOne()
	@Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
	@org.hibernate.validator.NotNull
	@AccessType(value = "field")
	public GroupMeasure getWeightGroupMeasure()
	{
		return weightGroupMeasure;
	}

	private void setWeightValue(BigDecimal weightValue)
	{
		BigDecimal oldValue = getWeightValue();
		this.weightValue = weightValue;
		if (this.weightValue != null)
		{
			this.weightValue = this.weightValue.setScale(10, RoundingMode.HALF_UP);
		}
		firePropertyChange(PROPERTYNAME_WEIGHTVALUE, oldValue, weightValue);
	}

	@AccessType(value = "field")
	@org.hibernate.validator.NotNull
	public BigDecimal getWeightValue()
	{
		return weightValue;
	}

	private void setWeightConversion(MeasureUnitConversion weightConversion)
	{
		MeasureUnitConversion oldValue = getWeightConversion();
		this.weightConversion = weightConversion;
		firePropertyChange(PROPERTYNAME_WEIGHTCONVERSION, oldValue, weightConversion);
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public MeasureUnitConversion getWeightConversion()
	{
		return weightConversion;
	}

	private void setWeightConvertedUnit(MeasureUnitMultiplier weightConvertedUnit)
	{
		MeasureUnitMultiplier oldValue = getWeightConvertedUnit();
		this.weightConvertedUnit = weightConvertedUnit;
		firePropertyChange(PROPERTYNAME_WEIGHTCONVERTEDUNIT, oldValue, weightConvertedUnit);
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public MeasureUnitMultiplier getWeightConvertedUnit()
	{
		return weightConvertedUnit;
	}

	private void setWeightConvertedValue(BigDecimal weightConvertedValue)
	{
		BigDecimal oldValue = getWeightConvertedValue();
		this.weightConvertedValue = weightConvertedValue;
		if (this.weightConvertedValue != null)
		{
			this.weightConvertedValue = this.weightConvertedValue.setScale(10, RoundingMode.HALF_UP);
		}
		firePropertyChange(PROPERTYNAME_WEIGHTCONVERTEDVALUE, oldValue, weightConvertedValue);
	}

	@AccessType(value = "field")
	public BigDecimal getWeightConvertedValue()
	{
		return weightConvertedValue;
	}

	public void setNumberPackages(Long numberPackages)
	{
		Long oldValue = getNumberPackages();
		this.numberPackages = numberPackages;
		firePropertyChange(PROPERTYNAME_NUMBERPACKAGES, oldValue, numberPackages);
	}

	@AccessType(value = "field")
	@org.hibernate.validator.NotNull
	public Long getNumberPackages()
	{
		return numberPackages;
	}

	@com.netappsid.annotations.NotNull
	public MacOrder getOrder()
	{
		return order;
	}

	public void setOrder(MacOrder order)
	{
		MacOrder oldValue = this.order;
		this.order = order;

		refreshItemToShipWrappers();
		firePropertyChange(PROPERTYNAME_ORDER, oldValue, order);
	}

	public void setValidIts(Long validIts)
	{
		Long oldValue = getNumberPackages();
		this.validIts = validIts;
		firePropertyChange(PROPERTYNAME_VALIDITS, oldValue, validIts);
	}

	@AccessType(value = "field")
	public Long getValidIts()
	{
		return validIts;
	}

	@Override
	public ValidationResult validate()
	{
		ValidationResult result = super.validate();

		if (!getWeight().hasValuePresent())
		{
			result.add(new SimpleValidationMessage("Le poids est requis.", Severity.ERROR));
		}
		if (getNumberPackages() == null)
		{
			result.add(new SimpleValidationMessage("Le nombre de colis est requis.", Severity.ERROR));
		}

		return result;
	}

	@Transient
	public List<AutomaticPackagesItemToShipWrapper> getItemToShipWrappers()
	{
		return itemToShipWrappers;
	}

	@Transient
	private void refreshItemToShipWrappers()
	{
		itemToShipWrappers.clear();

		if (this.order != null)
		{
			Long validIts = 0l;

			List<ItemToShip> itemsToShip = this.order.getItemsToShipNotPlannedNotShippedNotCanceledAndMaterial();
			for (ItemToShip itemToShip : itemsToShip)
			{
				if (!Packages.class.isAssignableFrom(itemToShip.getClass()) && itemToShip.getPackages() == null)
				{
					if (OrderDetailToShip.class.isAssignableFrom(itemToShip.getClass()))
					{
						OrderDetailToShip orderDetailToShip = (OrderDetailToShip) Model.getTarget(itemToShip);
						MacOrderDetail orderDetail = (MacOrderDetail) Model.getTarget(orderDetailToShip.getOrderDetail());
						boolean found = false;
						for (AutomaticPackagesItemToShipWrapper wrapper : itemToShipWrappers)
						{

							if (wrapper.getOrderDetail().hasSameID(orderDetail) && wrapper.getInventoryProduct() != null
									&& wrapper.getInventoryProduct().hasSameID(orderDetailToShip.getInventoryProduct()))
							{
								wrapper.addItemToShip(itemToShip);
								found = true;
							}
						}
						if (!found)
						{
							itemToShipWrappers.add(new AutomaticPackagesItemToShipWrapper(itemToShip));
						}
						validIts++;
					}
				}
			}
			setValidIts(validIts);
			setNumberPackages(null); // On veux maintenant laisser le choix à l'utilisateur de saisir son nombre de colis
		}
		else
		{
			setValidIts(null);
			setNumberPackages(null);
		}
	}

	public static final String PROPERTYNAME_VALIDITS = "validIts";
	public static final String PROPERTYNAME_ORDER = "order";
	public static final String PROPERTYNAME_NUMBERPACKAGES = "numberPackages";
	public static final String PROPERTYNAME_WEIGHT = "weight"; //$NON-NLS-1$
	public static final String PROPERTYNAME_WEIGHTGROUPMEASURE = "weightGroupMeasure"; //$NON-NLS-1$
	public static final String PROPERTYNAME_WEIGHTVALUE = "weightValue"; //$NON-NLS-1$
	public static final String PROPERTYNAME_WEIGHTCONVERSION = "weightConversion"; //$NON-NLS-1$
	public static final String PROPERTYNAME_WEIGHTCONVERTEDUNIT = "weightConvertedUnit"; //$NON-NLS-1$
	public static final String PROPERTYNAME_WEIGHTCONVERTEDVALUE = "weightConvertedValue"; //$NON-NLS-1$
}
