package com.mac.custom.bo.model;

import com.netappsid.bo.model.Model;
import com.netappsid.datatypes.MonetaryAmount;

public class EncoursClient extends Model
{
	private MonetaryAmount totalBlockedOrdersNotProductionNotShipped;
	private MonetaryAmount totalNotBlockedOrdersNotProductionNotShipped;
	private MonetaryAmount totalBlockedOrdersProductionNotShipped;
	private MonetaryAmount totalNotBlockedOrdersProductionNotShipped;
	private MonetaryAmount totalShippedNotInvoiced;
	private MonetaryAmount totalInvoicesNotPaid;
	private MonetaryAmount totalOrdersNewIncomplete;
	private MonetaryAmount encoursNonFacture;
	private MonetaryAmount encoursFinancier;
	private MonetaryAmount encoursTotal;
	private MonetaryAmount encoursAccepte;
	private MonetaryAmount depassement;

	public MonetaryAmount getTotalBlockedOrdersNotProductionNotShipped()
	{
		return totalBlockedOrdersNotProductionNotShipped;
	}

	public void setTotalBlockedOrdersNotProductionNotShipped(MonetaryAmount totalBlockedOrdersNotProductionNotShipped)
	{
		MonetaryAmount oldValue = getTotalBlockedOrdersNotProductionNotShipped();
		this.totalBlockedOrdersNotProductionNotShipped = totalBlockedOrdersNotProductionNotShipped;
		firePropertyChange(PROPERTYNAME_TOTAL_BLOCKED_ORDERS_NOTPRODUCTION_NOTSHIPPED, oldValue, totalBlockedOrdersNotProductionNotShipped, false);
	}

	public MonetaryAmount getTotalNotBlockedOrdersNotProductionNotShipped()
	{
		return totalNotBlockedOrdersNotProductionNotShipped;
	}

	public void setTotalNotBlockedOrdersNotProductionNotShipped(MonetaryAmount totalNotBlockedOrdersNotProductionNotShipped)
	{
		MonetaryAmount oldValue = getTotalNotBlockedOrdersNotProductionNotShipped();
		this.totalNotBlockedOrdersNotProductionNotShipped = totalNotBlockedOrdersNotProductionNotShipped;
		firePropertyChange(PROPERTYNAME_TOTAL_NOTBLOCKED_ORDERS_NOTPRODUCTION_NOTSHIPPED, oldValue, totalNotBlockedOrdersNotProductionNotShipped, false);
	}

	public MonetaryAmount getTotalBlockedOrdersProductionNotShipped()
	{
		return totalBlockedOrdersProductionNotShipped;
	}

	public void setTotalBlockedOrdersProductionNotShipped(MonetaryAmount totalBlockedOrdersProductionNotShipped)
	{
		MonetaryAmount oldValue = getTotalBlockedOrdersProductionNotShipped();
		this.totalBlockedOrdersProductionNotShipped = totalBlockedOrdersProductionNotShipped;
		firePropertyChange(PROPERTYNAME_TOTAL_BLOCKED_ORDERS_PRODUCTION_NOTSHIPPED, oldValue, totalBlockedOrdersProductionNotShipped, false);
	}

	public MonetaryAmount getTotalNotBlockedOrdersProductionNotShipped()
	{
		return totalNotBlockedOrdersProductionNotShipped;
	}

	public void setTotalNotBlockedOrdersProductionNotShipped(MonetaryAmount totalNotBlockedOrdersProductionNotShipped)
	{
		MonetaryAmount oldValue = getTotalNotBlockedOrdersProductionNotShipped();
		this.totalNotBlockedOrdersProductionNotShipped = totalNotBlockedOrdersProductionNotShipped;
		firePropertyChange(PROPERTYNAME_TOTAL_NOTBLOCKED_ORDERS_PRODUCTION_NOTSHIPPED, oldValue, totalNotBlockedOrdersProductionNotShipped, false);
	}

	public MonetaryAmount getTotalShippedNotInvoiced()
	{
		return totalShippedNotInvoiced;
	}

	public void setTotalShippedNotInvoiced(MonetaryAmount totalShippedNotInvoiced)
	{
		MonetaryAmount oldValue = getTotalShippedNotInvoiced();
		this.totalShippedNotInvoiced = totalShippedNotInvoiced;
		firePropertyChange(PROPERTYNAME_TOTAL_SHIPPED_NOTINVOICED, oldValue, totalShippedNotInvoiced, false);
	}

	public MonetaryAmount getTotalInvoicesNotPaid()
	{
		return totalInvoicesNotPaid;
	}

	public void setTotalInvoicesNotPaid(MonetaryAmount totalInvoicesNotPaid)
	{
		MonetaryAmount oldValue = getTotalInvoicesNotPaid();
		this.totalInvoicesNotPaid = totalInvoicesNotPaid;
		firePropertyChange(PROPERTYNAME_TOTAL_INVOICES_NOTPAID, oldValue, totalInvoicesNotPaid, false);
	}

	public MonetaryAmount getTotalOrdersNewIncomplete()
	{
		return totalOrdersNewIncomplete;
	}

	public void setTotalOrdersNewIncomplete(MonetaryAmount totalOrdersNewIncomplete)
	{
		MonetaryAmount oldValue = getTotalOrdersNewIncomplete();
		this.totalOrdersNewIncomplete = totalOrdersNewIncomplete;
		firePropertyChange(PROPERTYNAME_TOTAL_ORDERS_NEW_INCOMPLETE, oldValue, totalOrdersNewIncomplete, false);
	}

	/**
	 * Methode get de l encours financier calcule a partir de l encours financier et de l encours de fabrication Ajoute par OGA/PBY le 03/12/2013
	 */
	public MonetaryAmount getEncoursNonFacture()
	{
		return encoursNonFacture;
	}

	/**
	 * Methode set de l encours financier calcule a partir de l'encours financier et de l'encours de fabrication Ajoute par OGA/PBY le 03/12/2013
	 */
	public void setEncoursNonFacture(MonetaryAmount encoursNonFacture)
	{
		MonetaryAmount oldValue = getEncoursNonFacture();
		this.encoursNonFacture = encoursNonFacture;
		firePropertyChange(PROPERTYNAME_ENCOURS_NONFACTURE, oldValue, encoursNonFacture, false);
	}

	/**
	 * Methode get de l encours financier calcule a partir de l encours financier et de l encours de fabrication Ajoute par OGA/PBY le 03/12/2013
	 */
	public MonetaryAmount getEncoursFinancier()
	{
		return encoursFinancier;
	}

	/**
	 * Methode set de l encours financier calcule a partir de l'encours financier et de l'encours de fabrication Ajoute par OGA/PBY le 03/12/2013
	 */
	public void setEncoursFinancier(MonetaryAmount encoursFinancier)
	{
		MonetaryAmount oldValue = getEncoursFinancier();
		this.encoursFinancier = encoursFinancier;
		firePropertyChange(PROPERTYNAME_ENCOURS_FINANCIER, oldValue, encoursFinancier, false);
	}

	/**
	 * Methode get de l encours total calcule a partir de l encours financier et de l encours de fabrication Ajoute par OGA/PBY le 03/12/2013
	 */
	public MonetaryAmount getEncoursTotal()
	{
		return encoursTotal;
	}

	/**
	 * Methode set de l encours total calcule a partir de l'encours financier et de l'encours de fabrication Ajoute par OGA/PBY le 03/12/2013
	 */
	public void setEncoursTotal(MonetaryAmount encoursTotal)
	{
		MonetaryAmount oldValue = getEncoursTotal();
		this.encoursTotal = encoursTotal;
		firePropertyChange(PROPERTYNAME_ENCOURS_TOTAL, oldValue, encoursTotal, false);
	}

	/**
	 * Methode get de l encours accepte calcule a partir du montant du risque dans assurance credit et du montant du risque dans assurance societe Ajoute par
	 * OGA/PBY le 03/12/2013
	 */
	public MonetaryAmount getEncoursAccepte()
	{
		return encoursAccepte;
	}

	/**
	 * Methode set de l encours accepte calcule a partir du montant du risque dans assurance credit et du montant du risque dans assurance societe Ajoute par
	 * OGA/PBY le 03/12/2013
	 */
	public void setEncoursAccepte(MonetaryAmount encoursAccepte)
	{
		MonetaryAmount oldValue = getEncoursAccepte();
		this.encoursAccepte = encoursAccepte;
		firePropertyChange(PROPERTYNAME_ENCOURS_ACCEPTE, oldValue, encoursAccepte, false);
	}

	/**
	 * Methode get du depassement calcul : difference entre encours total et encours accepte Ajoute par OGA/PBY le 03/12/2013
	 */
	public MonetaryAmount getDepassement()
	{
		return depassement;
	}

	/**
	 * Methode get du depassement calcul : difference entre encours total et encours accepte Ajoute par OGA/PBY le 03/12/2013
	 */
	public void setDepassement(MonetaryAmount depassement)
	{
		MonetaryAmount oldValue = getDepassement();
		this.depassement = depassement;
		firePropertyChange(PROPERTYNAME_DEPASSEMENT, oldValue, depassement, false);
	}

	private static final String PROPERTYNAME_TOTAL_BLOCKED_ORDERS_NOTPRODUCTION_NOTSHIPPED = "totalBlockedOrdersNotProductionNotShipped";
	private static final String PROPERTYNAME_TOTAL_NOTBLOCKED_ORDERS_NOTPRODUCTION_NOTSHIPPED = "totalNotBlockedOrdersNotProductionNotShipped";
	private static final String PROPERTYNAME_TOTAL_BLOCKED_ORDERS_PRODUCTION_NOTSHIPPED = "totalBlockedOrdersProductionNotShipped";
	private static final String PROPERTYNAME_TOTAL_NOTBLOCKED_ORDERS_PRODUCTION_NOTSHIPPED = "totalNotBlockedOrdersProductionNotShipped";
	private static final String PROPERTYNAME_TOTAL_SHIPPED_NOTINVOICED = "totalShippedNotInvoiced";
	private static final String PROPERTYNAME_TOTAL_INVOICES_NOTPAID = "totalInvoicesNotPaid";
	private static final String PROPERTYNAME_TOTAL_ORDERS_NEW_INCOMPLETE = "totalOrdersNewIncomplete";
	private static final String PROPERTYNAME_ENCOURS_FINANCIER = "encoursFinancier";
	private static final String PROPERTYNAME_ENCOURS_NONFACTURE = "encoursNonFacture";
	private static final String PROPERTYNAME_ENCOURS_TOTAL = "encoursTotal";
	private static final String PROPERTYNAME_ENCOURS_ACCEPTE = "encoursAccepte";
	private static final String PROPERTYNAME_DEPASSEMENT = "depassement";
}
