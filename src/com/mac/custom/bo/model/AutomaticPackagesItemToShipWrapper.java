package com.mac.custom.bo.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Transient;

import com.mac.custom.bo.MacOrderDetail;
import com.netappsid.bo.model.Model;
import com.netappsid.erp.server.bo.InventoryProduct;
import com.netappsid.erp.server.bo.ItemToShip;
import com.netappsid.erp.server.bo.OrderDetailToShip;
import com.netappsid.erp.server.datatypes.GroupMeasureValue;

public class AutomaticPackagesItemToShipWrapper extends Model
{
	private final List<ItemToShip> itemsToShip = new ArrayList<ItemToShip>();
	private final MacOrderDetail orderDetail;
	private transient GroupMeasureValue quantity = null;
	private transient GroupMeasureValue quantityShipped = null;
	private transient GroupMeasureValue quantityToShip = null;

	AutomaticPackagesItemToShipWrapper(ItemToShip itemToShip)
	{
		addItemToShip(itemToShip);
		ItemToShip firstItemToShip = itemsToShip.get(0);
		if (firstItemToShip instanceof OrderDetailToShip)
		{
			OrderDetailToShip orderDetailToShip = (OrderDetailToShip) firstItemToShip;
			this.orderDetail = (MacOrderDetail) Model.getTarget(orderDetailToShip.getOrderDetail());
		}
		else
		{
			this.orderDetail = null;
		}
	}

	@Transient
	public List<ItemToShip> getItemsToShip()
	{
		return itemsToShip;
	}

	public void addItemToShip(ItemToShip itemToShip)
	{
		itemsToShip.add(Model.getTarget(itemToShip));
		// Recalculate quantities
		GroupMeasureValue quantity = getQuantity();
		if (quantity == null)
		{
			setQuantity(GroupMeasureValue.getSpecificValueGroupMeasureValue(itemToShip.getQuantity(), itemToShip.getQuantity().getValue()));
		}
		else
		{
			setQuantity(quantity.add(itemToShip.getQuantity()));
		}

		GroupMeasureValue quantityShipped = getQuantityShipped();
		if (quantityShipped == null)
		{
			setQuantityShipped(GroupMeasureValue.getSpecificValueGroupMeasureValue(itemToShip.getQuantityShipped(), itemToShip.getQuantityShipped().getValue()));
		}
		else
		{
			setQuantityShipped(quantityShipped.add(itemToShip.getQuantityShipped()));
		}

		quantity = getQuantity();
		quantityShipped = getQuantityShipped();
		setQuantityToShip(quantity.substract(quantityShipped));
	}

	@Transient
	public InventoryProduct getInventoryProduct()
	{
		return itemsToShip.get(0).getInventoryProduct();
	}

	@Transient
	public MacOrderDetail getOrderDetail()
	{
		return orderDetail;
	}

	@Transient
	public GroupMeasureValue getQuantity()
	{
		return quantity;
	}

	public void setQuantity(GroupMeasureValue quantity)
	{
		GroupMeasureValue oldQuantity = this.quantity;
		this.quantity = quantity;
		firePropertyChange(PROPERTYNAME_QUANTITY, oldQuantity, quantity);
	}

	@Transient
	public GroupMeasureValue getQuantityShipped()
	{
		return quantityShipped;
	}

	public void setQuantityShipped(GroupMeasureValue quantityShipped)
	{
		GroupMeasureValue oldQuantityShipped = this.quantityShipped;
		this.quantityShipped = quantityShipped;
		firePropertyChange(PROPERTYNAME_QUANTITYSHIPPED, oldQuantityShipped, quantityShipped);
	}

	@Transient
	public GroupMeasureValue getQuantityToShip()
	{
		return quantityToShip;
	}

	public void setQuantityToShip(GroupMeasureValue quantityToShip)
	{
		// Validate entry and make sure it does not exceed boundaries
		if (quantityToShip.compareTo(GroupMeasureValue.getZeroGroupMeasureValue(quantityToShip)) < 0)
		{
			quantityToShip = GroupMeasureValue.getZeroGroupMeasureValue(quantityToShip);
		}
		GroupMeasureValue maxValue = getQuantity().substract(getQuantityShipped());
		if (quantityToShip.compareTo(maxValue) > 0)
		{
			quantityToShip = maxValue;
		}

		GroupMeasureValue oldQuantityToShip = this.quantityToShip;
		this.quantityToShip = quantityToShip;
		firePropertyChange(PROPERTYNAME_QUANTITYTOSHIP, oldQuantityToShip, quantityToShip);
	}

	public static final String PROPERTYNAME_QUANTITY = "quantity";
	public static final String PROPERTYNAME_QUANTITYSHIPPED = "quantityShipped";
	public static final String PROPERTYNAME_QUANTITYTOSHIP = "quantityToShip";
	public static final String PROPERTYNAME_ITEMSTOSHIP = "itemsToShip";
	public static final String PROPERTYNAME_INVENTORYPRODUCT = "inventoryProduct";
	public static final String PROPERTYNAME_ORDERDETAIL = "orderDetail";
}
