package com.mac.custom.bo.localization;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.mac.custom.bo.MacRegionCommerciale;
import com.netappsid.bo.model.LocalizedString;

@Entity
@Table(name = "MacRegionCommercialeDescription", schema = "MacSchema", uniqueConstraints = { @UniqueConstraint(columnNames = { "macRegionCommerciale_id", "languageCode" }) })//$NON-NLS-1$ //$NON-NLS-2$
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "standard")
public class MacRegionCommercialeDescription extends LocalizedString
{
	private Serializable id;
	private String languageCode;
	private String value;
	private MacRegionCommerciale macRegionCommerciale;

	/**
	 * Default constructor used by Hibernate to construct the object
	 */
	public MacRegionCommercialeDescription()
	{}

	/**
	 * This constructor is used by the BO to set the data
	 */
	public MacRegionCommercialeDescription(String languageCode, String value)
	{
		this.languageCode = languageCode;
		this.value = value;
	}

	@Id
	@GeneratedValue(generator = "naid-uuid")
	@GenericGenerator(name = "naid-uuid", strategy = "com.netappsid.framework.utils.UUIDGenerator")
	@Type(type = "com.netappsid.datatypes.UUIDUserType")
	public Serializable getId()
	{
		return id;
	}

	public void setId(Serializable id)
	{
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public MacRegionCommerciale getMacRegionCommerciale()
	{
		return macRegionCommerciale;
	}

	public void setMacRegionCommerciale(MacRegionCommerciale macRegionCommerciale)
	{
		this.macRegionCommerciale = macRegionCommerciale;
	}

	public String getLanguageCode()
	{
		return languageCode;
	}

	public void setLanguageCode(String languageCode)
	{
		this.languageCode = languageCode;
	}

	public String getValue()
	{
		return value;
	}

	public void setValue(String value)
	{
		this.value = value;
	}

	public static final String PROPERTYNAME_MACREGIONCOMMERCIALE = "macRegionCommerciale"; //$NON-NLS-1$
}
