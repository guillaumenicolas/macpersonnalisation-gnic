package com.mac.custom.bo;

import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.message.SimpleValidationMessage;
import com.mac.custom.bo.base.BaseMacSaleInvoice;
import com.mac.custom.enums.SaleTypeEnum;
import com.netappsid.resources.Translator;

@javax.persistence.Entity
@Table(name = "MacSaleInvoice", schema = "MacSchema")
@PrimaryKeyJoinColumn(name = "id")
public class MacSaleInvoice extends BaseMacSaleInvoice
{
	@Override
	public ValidationResult validate() 
	{
		ValidationResult vr = super.validate();
		
		if (getSaleType()!= null && getSaleType().getCode() != null && getSaleType().getUpperCode().equals(SaleTypeEnum.sav.getUpperCode())) {
			if (getServiceType() == null || getServiceType().getCode() == null || getServiceType().getCode().equals("")) {
				vr.add(new SimpleValidationMessage(Translator.getString("cannotSaveWhenSAVServiceTypeNotCompleted", Severity.ERROR)));
			}
			if (getOriginalOrderNumber() == null || getOriginalOrderNumber().equals("")) {
				vr.add(new SimpleValidationMessage(Translator.getString("cannotSaveWhenSAVOriginalOrderNumberNotCompleted", Severity.ERROR)));
			}
		}
		return vr;
	}
}