package com.mac.custom.bo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.CollectionTypeInfo;
import org.hibernate.annotations.Index;

import com.netappsid.annotations.Scale;
import com.netappsid.dao.PersistenceListener;
import com.netappsid.erp.server.bo.Product;
import com.netappsid.erp.server.bo.PurchaseRequisition;
import com.netappsid.framework.utils.NaturalKeyValidator;
import com.netappsid.observable.ObservableCollections;
import com.netappsid.transactional.TransactionalCollections;
import com.netappsid.transactional.TransactionalList;

@MappedSuperclass
@EntityListeners(value = { PersistenceListener.class })
@Table(name = "PurchaseRequisitionProduct")
public class PurchaseRequisitionProduct extends com.netappsid.bo.model.Model implements Serializable
{
	// attributes
	private BigDecimal percentage;
	private final List<PurchaseRequisitionSupplier> suppliers = ObservableCollections.newObservableArrayList();
	private final List<PurchaseRequisition> purchaseRequisitions = ObservableCollections.newObservableArrayList();
	private Product product;

	// Operations

	public void setPercentage(BigDecimal percentage)
	{
		BigDecimal oldValue = getPercentage();
		this.percentage = percentage;
		firePropertyChange(PROPERTYNAME_PERCENTAGE, oldValue, percentage);
	}

	@Column(name = "percentage", precision = 32, scale = 10)//$NON-NLS-2$
	@Scale(decimal = 10)
	@AccessType(value = "field")
	public BigDecimal getPercentage()
	{
		return percentage;
	}

	@Transient
	@OneToMany(mappedBy = "purchaseRequisitionProduct", fetch = FetchType.LAZY)
	@Index(name = "_suppliers_x")
	@CollectionTypeInfo(name = "com.netappsid.hibernate.LazyLoadBagType")
	@AccessType(value = "field")
	public List<PurchaseRequisitionSupplier> getSuppliers()
	{
		return suppliers;
	}

	protected void onSuppliersCollectionChange(TransactionalList<PurchaseRequisitionSupplier> suppliers)
	{
		TransactionalCollections.end(suppliers);
	}

	public void addToSuppliers(PurchaseRequisitionSupplier purchaseRequisitionSupplier)
	{
		final TransactionalList<PurchaseRequisitionSupplier> transactional = TransactionalCollections.begin(this.suppliers);
		purchaseRequisitionSupplier.setPurchaseRequisitionProduct(this);
		NaturalKeyValidator.isAddedItemValid(transactional, purchaseRequisitionSupplier);
		transactional.add(purchaseRequisitionSupplier);
		onSuppliersCollectionChange(transactional);
	}

	public void addToSuppliers(PurchaseRequisitionSupplier purchaseRequisitionSupplier, int index)
	{
		final TransactionalList<PurchaseRequisitionSupplier> transactional = TransactionalCollections.begin(this.suppliers);
		purchaseRequisitionSupplier.setPurchaseRequisitionProduct(this);
		NaturalKeyValidator.isAddedItemValid(transactional, purchaseRequisitionSupplier);
		transactional.add(index, purchaseRequisitionSupplier);
		onSuppliersCollectionChange(transactional);
	}

	public void addManyToSuppliers(Collection<PurchaseRequisitionSupplier> suppliers)
	{
		final TransactionalList<PurchaseRequisitionSupplier> transactional = TransactionalCollections.begin(this.suppliers);
		for (PurchaseRequisitionSupplier purchaseRequisitionSupplier : suppliers)
		{
			purchaseRequisitionSupplier.setPurchaseRequisitionProduct(this);
		}
		for (PurchaseRequisitionSupplier purchaseRequisitionSupplier : suppliers)
		{
			NaturalKeyValidator.isAddedItemValid(transactional, purchaseRequisitionSupplier);
		}

		transactional.addAll(suppliers);
		onSuppliersCollectionChange(transactional);
	}

	public void removeFromSuppliers(PurchaseRequisitionSupplier purchaseRequisitionSupplier)
	{
		final TransactionalList<PurchaseRequisitionSupplier> transactional = TransactionalCollections.begin(this.suppliers);
		if (transactional.remove(purchaseRequisitionSupplier))
		{
			purchaseRequisitionSupplier.setPurchaseRequisitionProduct(null);
		}
		onSuppliersCollectionChange(transactional);
	}

	public void removeManyFromSuppliers(Collection<PurchaseRequisitionSupplier> suppliers)
	{
		final TransactionalList<PurchaseRequisitionSupplier> transactional = TransactionalCollections.begin(this.suppliers);
		for (PurchaseRequisitionSupplier purchaseRequisitionSupplier : suppliers)
		{
			if (transactional.contains(purchaseRequisitionSupplier))
			{
				purchaseRequisitionSupplier.setPurchaseRequisitionProduct(null);
			}
		}

		transactional.removeAll(suppliers);
		onSuppliersCollectionChange(transactional);
	}

	public void removeAllFromSuppliers()
	{
		final TransactionalList<PurchaseRequisitionSupplier> transactional = TransactionalCollections.begin(this.suppliers);
		for (PurchaseRequisitionSupplier purchaseRequisitionSupplier : suppliers)
		{
			purchaseRequisitionSupplier.setPurchaseRequisitionProduct(null);
		}

		transactional.clear();
		onSuppliersCollectionChange(transactional);
	}

	public void replaceSuppliers(PurchaseRequisitionSupplier oldPurchaseRequisitionSupplier, PurchaseRequisitionSupplier newPurchaseRequisitionSupplier)
	{
		final TransactionalList<PurchaseRequisitionSupplier> transactional = TransactionalCollections.begin(this.suppliers);
		final int indexOf = transactional.indexOf(oldPurchaseRequisitionSupplier);

		if (indexOf != -1)
		{
			newPurchaseRequisitionSupplier.setPurchaseRequisitionProduct(this);
			NaturalKeyValidator.isAddedItemValid(transactional, newPurchaseRequisitionSupplier);
			transactional.set(indexOf, newPurchaseRequisitionSupplier);
			oldPurchaseRequisitionSupplier.setPurchaseRequisitionProduct(null);
		}
		onSuppliersCollectionChange(transactional);
	}

	public PurchaseRequisitionSupplier setToSuppliers(int index, PurchaseRequisitionSupplier purchaseRequisitionSupplier)
	{
		final TransactionalList<PurchaseRequisitionSupplier> transactional = TransactionalCollections.begin(this.suppliers);
		purchaseRequisitionSupplier.setPurchaseRequisitionProduct(this);
		NaturalKeyValidator.isAddedItemValid(transactional, purchaseRequisitionSupplier);

		PurchaseRequisitionSupplier removed = transactional.set(index, purchaseRequisitionSupplier);

		if (removed != null)
		{
			removed.setPurchaseRequisitionProduct(null);
		}

		onSuppliersCollectionChange(transactional);
		return removed;
	}

	@OneToMany(fetch = FetchType.LAZY)
	@CollectionTypeInfo(name = "com.netappsid.hibernate.LazyLoadBagType")
	@AccessType(value = "field")
	public List<PurchaseRequisition> getPurchaseRequisitions()
	{
		return purchaseRequisitions;
	}

	protected void onPurchaseRequisitionsCollectionChange(TransactionalList<PurchaseRequisition> purchaseRequisitions)
	{
		TransactionalCollections.end(purchaseRequisitions);
	}

	public void addToPurchaseRequisitions(PurchaseRequisition purchaseRequisition)
	{
		final TransactionalList<PurchaseRequisition> transactional = TransactionalCollections.begin(this.purchaseRequisitions);
		NaturalKeyValidator.isAddedItemValid(transactional, purchaseRequisition);
		transactional.add(purchaseRequisition);
		onPurchaseRequisitionsCollectionChange(transactional);
	}

	public void addToPurchaseRequisitions(PurchaseRequisition purchaseRequisition, int index)
	{
		final TransactionalList<PurchaseRequisition> transactional = TransactionalCollections.begin(this.purchaseRequisitions);
		NaturalKeyValidator.isAddedItemValid(transactional, purchaseRequisition);
		transactional.add(index, purchaseRequisition);
		onPurchaseRequisitionsCollectionChange(transactional);
	}

	public void addManyToPurchaseRequisitions(Collection<PurchaseRequisition> purchaseRequisitions)
	{
		final TransactionalList<PurchaseRequisition> transactional = TransactionalCollections.begin(this.purchaseRequisitions);
		for (PurchaseRequisition purchaseRequisition : purchaseRequisitions)
		{
			NaturalKeyValidator.isAddedItemValid(transactional, purchaseRequisition);
		}

		transactional.addAll(purchaseRequisitions);
		onPurchaseRequisitionsCollectionChange(transactional);
	}

	public void removeFromPurchaseRequisitions(PurchaseRequisition purchaseRequisition)
	{
		final TransactionalList<PurchaseRequisition> transactional = TransactionalCollections.begin(this.purchaseRequisitions);
		transactional.remove(purchaseRequisition);
		onPurchaseRequisitionsCollectionChange(transactional);
	}

	public void removeManyFromPurchaseRequisitions(Collection<PurchaseRequisition> purchaseRequisitions)
	{
		final TransactionalList<PurchaseRequisition> transactional = TransactionalCollections.begin(this.purchaseRequisitions);
		transactional.removeAll(purchaseRequisitions);
		onPurchaseRequisitionsCollectionChange(transactional);
	}

	public void removeAllFromPurchaseRequisitions()
	{
		final TransactionalList<PurchaseRequisition> transactional = TransactionalCollections.begin(this.purchaseRequisitions);
		transactional.clear();
		onPurchaseRequisitionsCollectionChange(transactional);
	}

	public void replacePurchaseRequisitions(PurchaseRequisition oldPurchaseRequisition, PurchaseRequisition newPurchaseRequisition)
	{
		final TransactionalList<PurchaseRequisition> transactional = TransactionalCollections.begin(this.purchaseRequisitions);
		final int indexOf = transactional.indexOf(oldPurchaseRequisition);

		if (indexOf != -1)
		{
			NaturalKeyValidator.isAddedItemValid(transactional, newPurchaseRequisition);
			transactional.set(indexOf, newPurchaseRequisition);
		}
		onPurchaseRequisitionsCollectionChange(transactional);
	}

	public PurchaseRequisition setToPurchaseRequisitions(int index, PurchaseRequisition purchaseRequisition)
	{
		final TransactionalList<PurchaseRequisition> transactional = TransactionalCollections.begin(this.purchaseRequisitions);
		NaturalKeyValidator.isAddedItemValid(transactional, purchaseRequisition);

		PurchaseRequisition removed = transactional.set(index, purchaseRequisition);

		onPurchaseRequisitionsCollectionChange(transactional);
		return removed;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@org.hibernate.validator.NotNull
	@com.netappsid.annotations.NotNull
	@AccessType(value = "field")
	public Product getProduct()
	{
		return product;
	}

	public void setProduct(Product product)
	{
		Product oldValue = this.product;
		this.product = product;
		firePropertyChange(PROPERTYNAME_PRODUCT, oldValue, this.product);
	}

	public void refreshPercentage()
	{
		BigDecimal percentage = BigDecimal.ZERO;

		for (PurchaseRequisitionSupplier purchaseRequisitionSupplier : getSuppliers())
		{
			if (purchaseRequisitionSupplier.getPercentage() != null)
			{
				percentage = percentage.add(purchaseRequisitionSupplier.getPercentage());
			}
		}

		setPercentage(percentage);
	}

	// Business methods

	// Bound properties

	public static final String PROPERTYNAME_PERCENTAGE = "percentage";
	public static final String PROPERTYNAME_SUPPLIERS = "suppliers";
	public static final String PROPERTYNAME_PURCHASEREQUISITIONS = "purchaseRequisitions";
	public static final String PROPERTYNAME_PRODUCT = "product";
}
