package com.mac.custom.bo;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.mac.custom.bo.base.BaseMacBlockingReason;

@Entity
@Table(name = "MacBlockingReason", schema = "MacSchema")
@PrimaryKeyJoinColumn(name = "id")
public class MacBlockingReason extends BaseMacBlockingReason
{

}
