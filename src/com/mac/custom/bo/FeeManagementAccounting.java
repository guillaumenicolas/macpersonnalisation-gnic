package com.mac.custom.bo;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.mac.custom.bo.base.BaseFeeManagementAccounting;
import com.mac.custom.dao.FeeManagementAccountingDAO;
import com.netappsid.annotations.DAO;

@Entity
@Table(name = "FeeManagementAccounting", schema = "MacSchema")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "standard")
@Inheritance(strategy = InheritanceType.JOINED)
@DAO(dao = FeeManagementAccountingDAO.class)
@PrimaryKeyJoinColumn(name = "id")
public class FeeManagementAccounting extends BaseFeeManagementAccounting
{

}
