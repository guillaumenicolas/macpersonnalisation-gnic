package com.mac.custom.bo;

import com.mac.custom.bo.base.BaseMaterialProductManagementAccounting;
import com.mac.custom.dao.MaterialProductManagementAccountingDAO;
import com.netappsid.annotations.DAO;
import com.netappsid.dao.PersistenceListener;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "standard")
@DAO(dao = MaterialProductManagementAccountingDAO.class)
@EntityListeners(value = { PersistenceListener.class })
@Table(name = "MaterialProductManagementAccounting", schema = "MacSchema")
public class MaterialProductManagementAccounting extends BaseMaterialProductManagementAccounting implements Serializable
{
}