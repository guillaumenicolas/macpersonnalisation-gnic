package com.mac.custom.bo;

import com.netappsid.dao.utils.boquery.BOQuery;
import com.netappsid.erp.server.bo.ProductSearch;

public class MacProductSearch extends ProductSearch
{
	private String supplierCatalogNumber;
	private String ancienCodeArticle;

	public String getSupplierCatalogNumber()
	{
		return supplierCatalogNumber;
	}

	public void setSupplierCatalogNumber(String supplierCatalogNumber)
	{
		String oldValue = getSupplierCatalogNumber();
		this.supplierCatalogNumber = supplierCatalogNumber;
		firePropertyChange(PROPERTYNAME_SUPPLIERCATALOGNUMBER, oldValue, supplierCatalogNumber);
	}

	public String getAncienCodeArticle()
	{
		return ancienCodeArticle;
	}

	public void setAncienCodeArticle(String ancienCodeArticle)
	{
		String oldValue = getAncienCodeArticle();
		this.ancienCodeArticle = ancienCodeArticle;
		firePropertyChange(PROPERTYNAME_ANCIENCODEARTICLE, oldValue, ancienCodeArticle);
	}

	@Override
	public void clearSearchParameters()
	{
		super.clearSearchParameters();

		setSupplierCatalogNumber(null);
		setAncienCodeArticle(null);
	}

	@Override
	public BOQuery getBOQuery(Class<?> productClass)
	{
		BOQuery boQuery = super.getBOQuery(productClass);

		if (getSupplierCatalogNumber() != null)
		{
			boQuery.addLike("suppliers.catalogNumber", getSupplierCatalogNumber());
		}
		if (getAncienCodeArticle() != null && MacMaterialProduct.class.isAssignableFrom(productClass))
		{
			boQuery.addILike("ancienCodeArticle", getAncienCodeArticle());
		}
		return boQuery;
	}

	public static final String PROPERTYNAME_SUPPLIERCATALOGNUMBER = "supplierCatalogNumber";
	public static final String PROPERTYNAME_ANCIENCODEARTICLE = "ancienCodeArticle";
}
