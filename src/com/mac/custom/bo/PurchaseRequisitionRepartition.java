package com.mac.custom.bo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.CollectionTypeInfo;

import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.message.SimpleValidationMessage;
import com.mac.custom.dao.PurchaseRequisitionRepartitionDAO;
import com.netappsid.annotations.DAO;
import com.netappsid.dao.PersistenceListener;
import com.netappsid.framework.utils.NaturalKeyValidator;
import com.netappsid.observable.ObservableCollections;
import com.netappsid.transactional.TransactionalCollections;
import com.netappsid.transactional.TransactionalList;
import com.netappsid.validation.Validable;

@MappedSuperclass
@EntityListeners(value = { PersistenceListener.class })
@Table(name = "PurchaseRequisitionRepartition")
@DAO(dao = PurchaseRequisitionRepartitionDAO.class)
public class PurchaseRequisitionRepartition extends com.netappsid.bo.model.Model implements Serializable, Validable
{
	// attributes
	private final List<PurchaseRequisitionProduct> products = ObservableCollections.newObservableArrayList();

	// Operations

	@Transient
	@OneToMany(fetch = FetchType.LAZY)
	@CollectionTypeInfo(name = "com.netappsid.hibernate.LazyLoadBagType")
	@AccessType(value = "field")
	public List<PurchaseRequisitionProduct> getProducts()
	{
		return products;
	}

	protected void onProductsCollectionChange(TransactionalList<PurchaseRequisitionProduct> products)
	{
		TransactionalCollections.end(products);
	}

	public void addToProducts(PurchaseRequisitionProduct purchaseRequisitionProduct)
	{
		final TransactionalList<PurchaseRequisitionProduct> transactional = TransactionalCollections.begin(this.products);
		NaturalKeyValidator.isAddedItemValid(transactional, purchaseRequisitionProduct);
		transactional.add(purchaseRequisitionProduct);
		onProductsCollectionChange(transactional);
	}

	public void addToProducts(PurchaseRequisitionProduct purchaseRequisitionProduct, int index)
	{
		final TransactionalList<PurchaseRequisitionProduct> transactional = TransactionalCollections.begin(this.products);
		NaturalKeyValidator.isAddedItemValid(transactional, purchaseRequisitionProduct);
		transactional.add(index, purchaseRequisitionProduct);
		onProductsCollectionChange(transactional);
	}

	public void addManyToProducts(Collection<PurchaseRequisitionProduct> products)
	{
		final TransactionalList<PurchaseRequisitionProduct> transactional = TransactionalCollections.begin(this.products);
		for (PurchaseRequisitionProduct purchaseRequisitionProduct : products)
		{
			NaturalKeyValidator.isAddedItemValid(transactional, purchaseRequisitionProduct);
		}

		transactional.addAll(products);
		onProductsCollectionChange(transactional);
	}

	public void removeFromProducts(PurchaseRequisitionProduct purchaseRequisitionProduct)
	{
		final TransactionalList<PurchaseRequisitionProduct> transactional = TransactionalCollections.begin(this.products);
		transactional.remove(purchaseRequisitionProduct);
		onProductsCollectionChange(transactional);
	}

	public void removeManyFromProducts(Collection<PurchaseRequisitionProduct> products)
	{
		final TransactionalList<PurchaseRequisitionProduct> transactional = TransactionalCollections.begin(this.products);
		transactional.removeAll(products);
		onProductsCollectionChange(transactional);
	}

	public void removeAllFromProducts()
	{
		final TransactionalList<PurchaseRequisitionProduct> transactional = TransactionalCollections.begin(this.products);
		transactional.clear();
		onProductsCollectionChange(transactional);
	}

	public void replaceProducts(PurchaseRequisitionProduct oldPurchaseRequisitionProduct, PurchaseRequisitionProduct newPurchaseRequisitionProduct)
	{
		final TransactionalList<PurchaseRequisitionProduct> transactional = TransactionalCollections.begin(this.products);
		final int indexOf = transactional.indexOf(oldPurchaseRequisitionProduct);

		if (indexOf != -1)
		{
			NaturalKeyValidator.isAddedItemValid(transactional, newPurchaseRequisitionProduct);
			transactional.set(indexOf, newPurchaseRequisitionProduct);
		}
		onProductsCollectionChange(transactional);
	}

	public PurchaseRequisitionProduct setToProducts(int index, PurchaseRequisitionProduct purchaseRequisitionProduct)
	{
		final TransactionalList<PurchaseRequisitionProduct> transactional = TransactionalCollections.begin(this.products);
		NaturalKeyValidator.isAddedItemValid(transactional, purchaseRequisitionProduct);

		PurchaseRequisitionProduct removed = transactional.set(index, purchaseRequisitionProduct);

		onProductsCollectionChange(transactional);
		return removed;
	}

	@Override
	public ValidationResult validate()
	{
		ValidationResult result = super.validate();
		BigDecimal hundred = new BigDecimal(100);

		for (PurchaseRequisitionProduct purchaseRequisitionProduct : getProducts())
		{
			if (purchaseRequisitionProduct.getPercentage().compareTo(hundred) != 0)
			{
				result.add(new SimpleValidationMessage("Le pourcentage de r\u00E9partition du produit " + purchaseRequisitionProduct.getProduct().getCode()
						+ " doit \u00EAtre \u00E9gale a 100%", Severity.ERROR));
			}
		}
		return result;
	}

	// Business methods

	// Bound properties

	public static final String PROPERTYNAME_PRODUCTS = "products";
}