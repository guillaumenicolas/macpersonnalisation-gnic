package com.mac.custom.bo;

import java.util.List;

import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.message.SimpleValidationMessage;
import com.mac.custom.bo.base.BaseMacMaterialProduct;
import com.netappsid.resources.Translator;

@javax.persistence.Entity
@Table(name = "MacMaterialProduct", schema = "MacSchema")
@PrimaryKeyJoinColumn(name = "id")
public class MacMaterialProduct extends BaseMacMaterialProduct
{
	@Override
	public ValidationResult validate()
	{
		ValidationResult result = super.validate();

		List<MaterialProductManagementAccounting> managementAccountings = getManagementAccountings();
		for (int i = 0; i < managementAccountings.size() - 1; i++)
		{
			for (int j = i + 1; j < managementAccountings.size(); j++)
			{
				if (managementAccountings.get(i).getCompany().getCode().equals(managementAccountings.get(j).getCompany().getCode()))
				{
					result.add(new SimpleValidationMessage(String.format(Translator.getString("managementAccountingMoreThanOneEntryForSameCompany"),
							managementAccountings.get(i).getCompany().getLocalizedDescription()), Severity.ERROR));
					break;
				}
			}
		}

		// Enregistrement comptabilite analytique obligatoire (Au moins 1 de renseigne)
		if (getManagementAccountings() == null || getManagementAccountings().isEmpty())
		{
			result.addError("Enregistrement comptabilit� analytique est obligatoire.");
		}

		// Enregistrement disponibilite societe dans infos financiere obligatoire (Au moins 1 de renseigne)
		if (getCompanyAvailables() == null || getCompanyAvailables().isEmpty())
		{
			result.addError("Enregistrement disponibilit� soci�t� dans informations financi�re est obligatoire.");
		}

		// Unite de mesure d'expedition obligatoire
		if (getShipMeasureGroupMeasure() == null)
		{
			result.addError("Unit� de mesure d'exp�dition est obligatoire.");
		}

		// Unite de mesure de facturation obligatoire
		if (getInvoiceMeasureGroupMeasure() == null)
		{
			result.addError("Unit� de mesure de facturation est obligatoire.");
		}

		// Unite de mesure de vente obligatoire
		if (getSaleMeasureGroupMeasure() == null)
		{
			result.addError("Unit� de mesure de vente est obligatoire.");
		}

		// Unite de mesure de stock obligatoire
		if (getInventoryMeasureGroupMeasure() == null)
		{
			result.addError("Unit� de mesure de stock est obligatoire.");
		}

		return result;
	}
}