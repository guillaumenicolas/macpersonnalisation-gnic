package com.mac.custom.bo;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.mac.custom.bo.base.BaseMacSetup;

@Entity
@Table(name = "MacSetup", schema = "MacSchema")
@PrimaryKeyJoinColumn(name = "id")
public class MacSetup extends BaseMacSetup
{
	public static final String PROPERTYNAME_WORKUNITSPROPERTY = "workUnitsProperty";
	public static final String PROPERTYNAME_WORKINGSTATIONCODEPROPERTY = "workingStationCodeProperty";
	public static final String PROPERTYNAME_SUPPLYINGDELAYPROPERTY = "supplyingDelayProperty";
	public static final String PROPERTYNAME_MANUFACTURINGDELAYPROPERTY = "manufacturingDelayProperty";
	public static final String PROPERTYNAME_MEDIAFOLDER = "mediaFolder";
	public static final String PROPERTYNAME_STOCKMISSINGSERVICEAUTOSENDEMAIL = "stockMissingServiceAutoSendEmail";
	public static final String PROPERTYNAME_STOCKMISSINGSERVICEAUTOAMOUNTTORESOLVE = "stockMissingServiceAutoAmountToResolve";
	public static final String PROPERTYNAME_STOCKMISSINGSERVICEAUTOEMAILFROM = "stockMissingServiceAutoEmailFrom";
	public static final String PROPERTYNAME_STOCKMISSINGSERVICEAUTOEMAILTO = "stockMissingServiceAutoEmailTo";
	public static final String PROPERTYNAME_DISCOUNTGROUPADMIN = "discountGroupAdmin";

	@Transient
	public String getWorkUnitsProperty()
	{
		return (String) getConvertedValue(PROPERTYNAME_WORKUNITSPROPERTY);
	}

	@Transient
	public String getWorkingStationCodeProperty()
	{
		return (String) getConvertedValue(PROPERTYNAME_WORKINGSTATIONCODEPROPERTY);
	}

	@Transient
	public String getSupplyingDelayProperty()
	{
		return (String) getConvertedValue(PROPERTYNAME_SUPPLYINGDELAYPROPERTY);
	}

	@Transient
	public String getManufacturingDelayProperty()
	{
		return (String) getConvertedValue(PROPERTYNAME_MANUFACTURINGDELAYPROPERTY);
	}

	@Transient
	public String getMediaFolder()
	{
		return (String) getConvertedValue(PROPERTYNAME_MEDIAFOLDER);
	}

	@Transient
	public boolean getStockMissingServiceAutoSendEmail()
	{
		Object convertedValue = getConvertedValue(PROPERTYNAME_STOCKMISSINGSERVICEAUTOSENDEMAIL);
		return convertedValue == null ? false : Boolean.valueOf(convertedValue.toString());
	}

	@Transient
	public Long getStockMissingServiceAutoAmountToResolve()
	{
		Object convertedValue = getConvertedValue(PROPERTYNAME_STOCKMISSINGSERVICEAUTOAMOUNTTORESOLVE);
		return convertedValue == null ? null : (Long) convertedValue;
	}

	@Transient
	public String getStockMissingServiceAutoEmailFrom()
	{
		Object convertedValue = getConvertedValue(PROPERTYNAME_STOCKMISSINGSERVICEAUTOEMAILFROM);
		return convertedValue == null ? null : (String) convertedValue;
	}

	@Transient
	public String getStockMissingServiceAutoEmailTo()
	{
		Object convertedValue = getConvertedValue(PROPERTYNAME_STOCKMISSINGSERVICEAUTOEMAILTO);
		return convertedValue == null ? null : (String) convertedValue;
	}

	@Transient
	public Object getDiscountGroupAdmin()
	{
		return getConvertedValue(PROPERTYNAME_DISCOUNTGROUPADMIN);
	}
}
