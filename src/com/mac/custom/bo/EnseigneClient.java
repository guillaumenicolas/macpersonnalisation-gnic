package com.mac.custom.bo;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.mac.custom.bo.base.BaseEnseigneClient;
import com.mac.custom.dao.EnseigneClientDAO;
import com.netappsid.annotations.DAO;
import com.netappsid.dao.PersistenceListener;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "standard")
@DAO(dao = EnseigneClientDAO.class)
@EntityListeners(value = { PersistenceListener.class })
@Table(name = "EnseigneClient", schema = "MacSchema", uniqueConstraints = { @UniqueConstraint(columnNames = { "code" }),
		@UniqueConstraint(columnNames = { "upperCode" }) })
public class EnseigneClient extends BaseEnseigneClient implements Serializable
{
}