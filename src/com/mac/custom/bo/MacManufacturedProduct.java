package com.mac.custom.bo;

import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.message.SimpleValidationMessage;
import com.mac.custom.bo.base.BaseMacManufacturedProduct;
import com.netappsid.resources.Translator;
import java.util.List;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@javax.persistence.Entity
@Table(name = "MacManufacturedProduct", schema = "MacSchema")
@PrimaryKeyJoinColumn(name = "id")
public class MacManufacturedProduct extends BaseMacManufacturedProduct
{

	@Override
	public ValidationResult validate()
	{
		ValidationResult result = super.validate();

		List<ManufacturedProductManagementAccounting> managementAccountings = getManagementAccountings();
		for (int i = 0; i < managementAccountings.size() - 1; i++)
		{
			for (int j = i + 1; j < managementAccountings.size(); j++)
			{
				if (managementAccountings.get(i).getCompany().getCode().equals(managementAccountings.get(j).getCompany().getCode()))
				{
					result.add(new SimpleValidationMessage(String.format(Translator.getString("managementAccountingMoreThanOneEntryForSameCompany"),
							managementAccountings.get(i).getCompany().getLocalizedDescription()), Severity.ERROR));
					break;
				}
			}
		}

		return result;
	}
}