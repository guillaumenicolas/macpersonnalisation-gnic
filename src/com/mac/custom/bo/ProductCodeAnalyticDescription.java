package com.mac.custom.bo;

import com.netappsid.bo.model.LocalizedString;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "ProductCodeAnalyticDescription", schema = "MacSchema", uniqueConstraints = { @UniqueConstraint(columnNames = { "ProductCodeAnalytic_id",
		"languageCode" }) })
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "standard")
public class ProductCodeAnalyticDescription extends LocalizedString
{
	private Serializable id;
	private String languageCode;
	private String value;
	private ProductCodeAnalytic productCodeAnalytic;

	/**
	 * Default constructor used by Hibernate to construct the object
	 */
	public ProductCodeAnalyticDescription()
	{}

	/**
	 * This constructor is used by the BO to set the data
	 */
	public ProductCodeAnalyticDescription(String languageCode, String value)
	{
		this.languageCode = languageCode;
		this.value = value;
	}

	@Override
	@Id
	@GeneratedValue(generator = "naid-uuid")
	@GenericGenerator(name = "naid-uuid", strategy = "com.netappsid.framework.utils.UUIDGenerator")
	@Type(type = "com.netappsid.datatypes.UUIDUserType")
	public Serializable getId()
	{
		return id;
	}

	@Override
	public void setId(Serializable id)
	{
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public ProductCodeAnalytic getProductCodeAnalytic()
	{
		return productCodeAnalytic;
	}

	public void setProductCodeAnalytic(ProductCodeAnalytic productCodeAnalytic)
	{
		this.productCodeAnalytic = productCodeAnalytic;
	}

	@Override
	public String getLanguageCode()
	{
		return languageCode;
	}

	@Override
	public void setLanguageCode(String languageCode)
	{
		this.languageCode = languageCode;
	}

	@Override
	public String getValue()
	{
		return value;
	}

	@Override
	public void setValue(String value)
	{
		this.value = value;
	}
}
