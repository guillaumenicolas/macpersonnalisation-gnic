package com.mac.custom.bo;

import com.mac.custom.bo.base.BaseProductCodeAnalytic;
import com.mac.custom.dao.ProductCodeAnalyticDAO;
import com.netappsid.annotations.DAO;
import com.netappsid.dao.PersistenceListener;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "standard")
@DAO(dao = ProductCodeAnalyticDAO.class)
@EntityListeners(value = { PersistenceListener.class })
@Table(name = "ProductCodeAnalytic", schema = "MacSchema", uniqueConstraints = { @UniqueConstraint(columnNames = { "code" }),
		@UniqueConstraint(columnNames = { "upperCode" }) })
public class ProductCodeAnalytic extends BaseProductCodeAnalytic
{
}