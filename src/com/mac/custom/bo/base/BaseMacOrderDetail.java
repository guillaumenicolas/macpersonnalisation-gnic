package com.mac.custom.bo.base;

import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;

import org.hibernate.Hibernate;
import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CollectionTypeInfo;
import org.hibernate.annotations.Index;

import com.mac.custom.bo.MacOrderDetail;
import com.mac.custom.bo.WorkLoadDetail;
import com.netappsid.bo.EntityStatus;
import com.netappsid.europe.naid.custom.bo.OrderDetailEuro;
import com.netappsid.framework.utils.NaturalKeyValidator;
import com.netappsid.framework.utils.ObservableCollectionUtils;
import com.netappsid.observable.ObservableCollections;
import com.netappsid.transactional.TransactionalCollections;
import com.netappsid.transactional.TransactionalList;

@MappedSuperclass
public abstract class BaseMacOrderDetail extends OrderDetailEuro
{
	private final List<WorkLoadDetail> workLoadDetails = ObservableCollections.newObservableArrayList();

	@Override
	public void setChildEntityStatus(EntityStatus entityStatus)
	{
		super.setChildEntityStatus(entityStatus);

		if (getWorkLoadDetails() != null && Hibernate.isInitialized(getWorkLoadDetails()))
		{
			for (WorkLoadDetail workLoadDetail : getWorkLoadDetails())
			{
				workLoadDetail.setEntityStatus(entityStatus);
			}
		}
	}

	@OneToMany(mappedBy = "orderDetail", fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
	@CollectionTypeInfo(name = "com.netappsid.hibernate.LazyLoadBagType")
	@AccessType(value = "field")//$NON-NLS-1$	
	@Index(name = "macOrderDetail_workLoadDetails_x")
	public List<WorkLoadDetail> getWorkLoadDetails()
	{
		return workLoadDetails;
	}

	public void setWorkLoadDetails(List<WorkLoadDetail> workLoadDetails)
	{
		ObservableCollectionUtils.clearAndAddAll(this.workLoadDetails, workLoadDetails);
	}

	protected void onWorkLoadDetailsCollectionChange(TransactionalList<WorkLoadDetail> workLoadDetails)
	{
		TransactionalCollections.end(workLoadDetails);
	}

	public void addToWorkLoadDetails(WorkLoadDetail workLoadDetail)
	{
		final TransactionalList<WorkLoadDetail> transactional = TransactionalCollections.begin(this.workLoadDetails);
		workLoadDetail.setOrderDetail((MacOrderDetail) this);
		NaturalKeyValidator.isAddedItemValid(transactional, workLoadDetail);
		transactional.add(workLoadDetail);
		onWorkLoadDetailsCollectionChange(transactional);
	}

	public void addToWorkLoadDetails(WorkLoadDetail workLoadDetail, int index)
	{
		final TransactionalList<WorkLoadDetail> transactional = TransactionalCollections.begin(this.workLoadDetails);
		workLoadDetail.setOrderDetail((MacOrderDetail) this);
		NaturalKeyValidator.isAddedItemValid(transactional, workLoadDetail);
		transactional.add(index, workLoadDetail);
		onWorkLoadDetailsCollectionChange(transactional);
	}

	public void addManyToWorkLoadDetails(Collection<WorkLoadDetail> workLoadDetails)
	{
		final TransactionalList<WorkLoadDetail> transactional = TransactionalCollections.begin(this.workLoadDetails);
		for (WorkLoadDetail workLoadDetail : workLoadDetails)
		{
			workLoadDetail.setOrderDetail((MacOrderDetail) this);
		}
		for (WorkLoadDetail workLoadDetail : workLoadDetails)
		{
			NaturalKeyValidator.isAddedItemValid(transactional, workLoadDetail);
		}

		transactional.addAll(workLoadDetails);
		onWorkLoadDetailsCollectionChange(transactional);
	}

	public void removeFromWorkLoadDetails(WorkLoadDetail workLoadDetail)
	{
		final TransactionalList<WorkLoadDetail> transactional = TransactionalCollections.begin(this.workLoadDetails);
		if (transactional.remove(workLoadDetail))
		{
			workLoadDetail.setOrderDetail(null);
		}
		onWorkLoadDetailsCollectionChange(transactional);
	}

	public void removeManyFromDetails(Collection<WorkLoadDetail> workLoadDetails)
	{
		final TransactionalList<WorkLoadDetail> transactional = TransactionalCollections.begin(this.workLoadDetails);
		for (WorkLoadDetail workLoadDetail : workLoadDetails)
		{
			if (transactional.contains(workLoadDetail))
			{
				workLoadDetail.setOrderDetail(null);
			}
		}

		transactional.removeAll(workLoadDetails);
		onWorkLoadDetailsCollectionChange(transactional);
	}

	public void removeAllFromWorkLoadDetails()
	{
		final TransactionalList<WorkLoadDetail> transactional = TransactionalCollections.begin(this.workLoadDetails);
		for (WorkLoadDetail workLoadDetail : workLoadDetails)
		{
			workLoadDetail.setOrderDetail(null);
		}

		transactional.clear();
		onWorkLoadDetailsCollectionChange(transactional);
	}

	public void replaceWorkLoadDetails(WorkLoadDetail oldWorkLoadDetail, WorkLoadDetail newWorkLoadDetail)
	{
		final TransactionalList<WorkLoadDetail> transactional = TransactionalCollections.begin(this.workLoadDetails);
		final int indexOf = transactional.indexOf(oldWorkLoadDetail);

		if (indexOf != -1)
		{
			newWorkLoadDetail.setOrderDetail((MacOrderDetail) this);
			NaturalKeyValidator.isAddedItemValid(transactional, newWorkLoadDetail);
			transactional.set(indexOf, newWorkLoadDetail);
			oldWorkLoadDetail.setOrderDetail(null);
		}
		onWorkLoadDetailsCollectionChange(transactional);
	}

	public WorkLoadDetail setToWorkLoadDetails(int index, WorkLoadDetail workLoadDetail)
	{
		final TransactionalList<WorkLoadDetail> transactional = TransactionalCollections.begin(this.workLoadDetails);
		workLoadDetail.setOrderDetail((MacOrderDetail) this);
		NaturalKeyValidator.isAddedItemValid(transactional, workLoadDetail);

		WorkLoadDetail removed = transactional.set(index, workLoadDetail);

		if (removed != null)
		{
			removed.setOrderDetail(null);
		}

		onWorkLoadDetailsCollectionChange(transactional);
		return removed;
	}

	public static final String PROPERTYNAME_WORKLOADDETAILS = "workLoadDetails";
}
