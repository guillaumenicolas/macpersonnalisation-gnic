package com.mac.custom.bo.base;

import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.AccessType;

import com.mac.custom.bo.ManagementAccounting;
import com.mac.custom.bo.ProductCodeAnalytic;

@MappedSuperclass
public abstract class BaseProductManagementAccounting extends ManagementAccounting
{

	private ProductCodeAnalytic productCodeAnalyticSale;
	private ProductCodeAnalytic productCodeAnalyticPurchase;

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public ProductCodeAnalytic getProductCodeAnalyticSale()
	{
		return productCodeAnalyticSale;
	}

	public void setProductCodeAnalyticSale(ProductCodeAnalytic productCodeAnalytic)
	{
		ProductCodeAnalytic oldValue = this.productCodeAnalyticSale;
		this.productCodeAnalyticSale = productCodeAnalytic;

		firePropertyChange(PROPERTYNAME_PRODUCTCODEANALYTICSALE, oldValue, this.productCodeAnalyticSale);
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public ProductCodeAnalytic getProductCodeAnalyticPurchase()
	{
		return productCodeAnalyticPurchase;
	}

	public void setProductCodeAnalyticPurchase(ProductCodeAnalytic productCodeAnalytic)
	{
		ProductCodeAnalytic oldValue = this.productCodeAnalyticPurchase;
		this.productCodeAnalyticPurchase = productCodeAnalytic;

		firePropertyChange(PROPERTYNAME_PRODUCTCODEANALYTICPURCHASE, oldValue, this.productCodeAnalyticPurchase);
	}

	public static final String PROPERTYNAME_PRODUCTCODEANALYTICSALE = "productCodeAnalyticSale";
	public static final String PROPERTYNAME_PRODUCTCODEANALYTICPURCHASE = "productCodeAnalyticPurchase";
}
