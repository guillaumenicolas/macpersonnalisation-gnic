package com.mac.custom.bo.base;

import java.io.Serializable;

import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.mac.custom.bo.MacWorkflowARC;
import com.netappsid.bo.Entity;
import com.netappsid.erp.server.bo.ManufacturedProduct;
import com.netappsid.erp.server.bo.Property;
import com.netappsid.erp.server.bo.PropertyValue;

@MappedSuperclass
public abstract class BaseMacWorkflowARCProductDelay extends Entity<BaseMacWorkflowARCProductDelay>
{
	private Serializable id;
	private MacWorkflowARC macWorkflowARC;
	private ManufacturedProduct product;
	private PropertyValue<?> propertyValue;
	private Short approvalDelay;
	private Property property;

	@Override
	@Id
	@GeneratedValue(generator = "naid-uuid")
	@GenericGenerator(name = "naid-uuid", strategy = "com.netappsid.framework.utils.UUIDGenerator")
	@Type(type = "com.netappsid.datatypes.UUIDUserType")
	@AccessType(value = "field")
	public Serializable getId()
	{
		return id;
	}

	@Override
	public void setId(Serializable id)
	{
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "workflowarc_id", insertable = false, updatable = false)
	@com.netappsid.annotations.NotNull
	@AccessType(value = "field")
	public MacWorkflowARC getMacWorkflowARC()
	{
		return macWorkflowARC;
	}

	public void setMacWorkflowARC(MacWorkflowARC workflowARC)
	{
		MacWorkflowARC oldValue = this.macWorkflowARC;
		this.macWorkflowARC = workflowARC;
		firePropertyChange(PROPERTYNAME_MACWORKFLOWARC, oldValue, this.macWorkflowARC);
	}

	public void setApprovalDelay(Short approvalDelay)
	{
		Short oldValue = getApprovalDelay();
		this.approvalDelay = approvalDelay;
		firePropertyChange(PROPERTYNAME_APPROVALDELAY, oldValue, approvalDelay);
	}

	@org.hibernate.validator.NotNull
	@com.netappsid.annotations.NotNull
	@org.hibernate.validator.Min(0)
	@com.netappsid.annotations.Min(0)
	@AccessType(value = "field")
	public Short getApprovalDelay()
	{
		return approvalDelay;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "propertyValue_id")
	@org.hibernate.validator.NotNull
	@com.netappsid.annotations.NotNull
	@AccessType(value = "field")
	public PropertyValue<?> getPropertyValue()
	{
		return propertyValue;
	}

	public void setPropertyValue(PropertyValue<?> propertyValue)
	{
		PropertyValue<?> oldValue = this.propertyValue;
		this.propertyValue = propertyValue;
		firePropertyChange(PROPERTYNAME_PROPERTYVALUE, oldValue, this.propertyValue);
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "property_id")
	@org.hibernate.validator.NotNull
	@com.netappsid.annotations.NotNull
	@AccessType(value = "field")
	public Property getProperty()
	{
		return property;
	}

	public void setProperty(Property property)
	{
		Property oldValue = this.property;
		this.property = property;
		firePropertyChange(PROPERTYNAME_PROPERTY, oldValue, this.property);
	}

	public void setProduct(ManufacturedProduct product)
	{
		Object oldValue = getProduct();
		this.product = product;
		firePropertyChange(PROPERTYNAME_PRODUCT, oldValue, product);
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "product_id")
	@org.hibernate.validator.NotNull
	@com.netappsid.annotations.NotNull
	@AccessType(value = "field")
	public ManufacturedProduct getProduct()
	{
		return product;
	}

	public static final String PROPERTYNAME_MACWORKFLOWARC = "macWorkflowARC";
	public static final String PROPERTYNAME_APPROVALDELAY = "approvalDelay";
	public static final String PROPERTYNAME_PRODUCT = "product";
	public static final String PROPERTYNAME_PROPERTYVALUE = "propertyValue";
	public static final String PROPERTYNAME_PROPERTY = "property";
}
