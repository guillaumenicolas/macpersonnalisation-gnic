package com.mac.custom.bo.base;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MapKey;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.Type;

import com.mac.custom.bo.EnseigneClient;
import com.mac.custom.bo.EnseigneClientDescription;
import com.netappsid.annotations.Localized;
import com.netappsid.bo.Entity;

@MappedSuperclass
public abstract class BaseEnseigneClient extends Entity
{

	private Serializable id;
	private String code;
	private String upperCode;
	private Map<String, EnseigneClientDescription> description = new HashMap<String, EnseigneClientDescription>();
	public static final String PROPERTYNAME_DESCRIPTION = "description";

	@Override
	@Id
	@GeneratedValue(generator = "naid-uuid")
	@GenericGenerator(name = "naid-uuid", strategy = "com.netappsid.framework.utils.UUIDGenerator")
	@Type(type = "com.netappsid.datatypes.UUIDUserType")
	@AccessType(value = "field")
	public Serializable getId()
	{
		return id;
	}

	@Override
	public void setId(Serializable id)
	{
		this.id = id;
	}

	@OneToMany(mappedBy = "enseigneClient", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@MapKey(name = "languageCode")
	@Localized
	@org.hibernate.annotations.Fetch(org.hibernate.annotations.FetchMode.SELECT)
	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "standard")
	@Index(name = "enseigneClient_description_x")
	@com.netappsid.annotations.Length(max = 255)
	@AccessType(value = "field")
	public Map<String, EnseigneClientDescription> getDescription()
	{
		return description;
	}

	public void setDescription(Map<String, EnseigneClientDescription> description)
	{
		Map<String, EnseigneClientDescription> oldValue = getDescription();
		this.description = description;
		firePropertyChange(PROPERTYNAME_DESCRIPTION, oldValue, description);
	}

	@Transient
	public String getLocalizedDescription()
	{
		return getDescription(Locale.getDefault().getLanguage());
	}

	public String getDescription(String languageCode)
	{
		if (description.containsKey(languageCode) == true)
		{
			return description.get(languageCode).getValue();
		}
		else
		{
			return null;
		}
	}

	public void setDescription(String languageCode, String value)
	{
		if (description.containsKey(languageCode) == true)
		{
			description.get(languageCode).setValue(value);
		}
		else
		{
			EnseigneClientDescription temp = new EnseigneClientDescription(languageCode, value);
			temp.setEnseigneClient((EnseigneClient) this);
			description.put(languageCode, temp);
		}
		firePropertyChange(PROPERTYNAME_DESCRIPTION, null, description);
	}

	public void setCode(String code)
	{
		String oldValue = getCode();
		String newValue = code;
		if (newValue != null && newValue.length() == 0)
		{
			newValue = null;
		}
		this.code = newValue;
		if (newValue != null)
		{
			setUpperCode(newValue.toUpperCase());
		}
		else
		{
			setUpperCode(null);
		}
		firePropertyChange(PROPERTYNAME_CODE, oldValue, newValue);
	}

	@Column(name = "code")
	@org.hibernate.validator.NotNull
	@com.netappsid.annotations.NotNull
	@org.hibernate.validator.Length(max = 25)
	@com.netappsid.annotations.Length(max = 25)
	@AccessType(value = "field")
	public String getCode()
	{
		return code;
	}

	public void setUpperCode(String upperCode)
	{
		String oldValue = getUpperCode();
		String newValue = upperCode;
		if (newValue != null && newValue.length() == 0)
		{
			newValue = null;
		}
		this.upperCode = newValue;
		firePropertyChange(PROPERTYNAME_UPPERCODE, oldValue, newValue);
	}

	@Column(name = "upperCode")
	@org.hibernate.validator.NotNull
	@org.hibernate.validator.Length(max = 25)
	@com.netappsid.annotations.Length(max = 25)
	@AccessType(value = "field")
	public String getUpperCode()
	{
		return upperCode;
	}

	public static final String PROPERTYNAME_CODE = "code";
	public static final String PROPERTYNAME_UPPERCODE = "upperCode";

	public BaseEnseigneClient()
	{
		super();
	}

}