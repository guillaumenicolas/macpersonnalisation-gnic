package com.mac.custom.bo.base;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.AccessType;

import com.netappsid.europe.naid.custom.bo.DocumentStatusEuro;

@MappedSuperclass
public abstract class BaseMacDocumentStatus extends DocumentStatusEuro
{
	private boolean allowWorkLoadModifications;
	private boolean validateBalance;

	public void setAllowWorkLoadModifications(boolean allowWorkLoadModifications)
	{
		boolean oldValue = isAllowWorkLoadModifications();
		this.allowWorkLoadModifications = allowWorkLoadModifications;
		firePropertyChange(PROPERTYNAME_ALLOWWORKLOADMODIFICATIONS, oldValue, allowWorkLoadModifications);
	}

	@Column(name = "allowWorkLoadModifications")
	@AccessType(value = "field")
	@org.hibernate.validator.NotNull
	@com.netappsid.annotations.NotNull
	public boolean isAllowWorkLoadModifications()
	{
		return allowWorkLoadModifications;
	}

	public void setValidateBalance(boolean validateBalance)
	{
		boolean oldValue = isValidateBalance();
		this.validateBalance = validateBalance;
		firePropertyChange(PROPERTYNAME_VALIDATEBALANCE, oldValue, validateBalance);
	}

	@Column(name = "validateBalance")
	@AccessType(value = "field")
	@org.hibernate.validator.NotNull
	@com.netappsid.annotations.NotNull
	public boolean isValidateBalance()
	{
		return validateBalance;
	}

	public static final String PROPERTYNAME_ALLOWWORKLOADMODIFICATIONS = "allowWorkLoadModifications";
	public static final String PROPERTYNAME_VALIDATEBALANCE = "validateBalance";
}
