package com.mac.custom.bo.base;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.mac.custom.bo.MacOrderDetail;
import com.mac.custom.bo.WorkLoad;
import com.netappsid.annotations.Scale;
import com.netappsid.annotations.Validate;
import com.netappsid.erp.server.bo.GroupMeasure;
import com.netappsid.erp.server.bo.MeasureUnitConversion;
import com.netappsid.erp.server.bo.MeasureUnitMultiplier;
import com.netappsid.erp.server.datatypes.GroupMeasureValue;

@MappedSuperclass
public abstract class BaseWorkLoadDetail extends com.netappsid.bo.Entity implements Serializable
{
	private Serializable id;
	private WorkLoad workLoad;
	private MacOrderDetail orderDetail;
	private GroupMeasureValue quantity;
	private GroupMeasure quantityGroupMeasure;
	private BigDecimal quantityValue;
	private MeasureUnitConversion quantityConversion;
	private MeasureUnitMultiplier quantityConvertedUnit;
	private BigDecimal quantityConvertedValue;
	private GroupMeasureValue quantityProduced;
	private GroupMeasure quantityProducedGroupMeasure;
	private BigDecimal quantityProducedValue;
	private MeasureUnitConversion quantityProducedConversion;
	private MeasureUnitMultiplier quantityProducedConvertedUnit;
	private BigDecimal quantityProducedConvertedValue;

	@Override
	public void setId(Serializable id)
	{
		this.id = id;
	}

	@Override
	@Id
	@GeneratedValue(generator = "naid-uuid")
	@GenericGenerator(name = "naid-uuid", strategy = "com.netappsid.framework.utils.UUIDGenerator")
	@Type(type = "com.netappsid.datatypes.UUIDUserType")
	@AccessType(value = "field")
	public Serializable getId()
	{
		return id;
	}

	public void setWorkLoad(WorkLoad workLoad)
	{
		WorkLoad oldValue = this.workLoad;
		this.workLoad = workLoad;
		firePropertyChange(PROPERTYNAME_WORKLOAD, oldValue, this.workLoad);
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@org.hibernate.validator.NotNull
	@com.netappsid.annotations.NotNull
	@AccessType(value = "field")
	public WorkLoad getWorkLoad()
	{
		return workLoad;
	}

	public void setOrderDetail(MacOrderDetail orderDetail)
	{
		MacOrderDetail oldValue = getOrderDetail();
		this.orderDetail = orderDetail;
		firePropertyChange(PROPERTYNAME_ORDERDETAIL, oldValue, orderDetail);
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@org.hibernate.validator.NotNull
	@com.netappsid.annotations.NotNull
	@AccessType(value = "field")
	public MacOrderDetail getOrderDetail()
	{
		return orderDetail;
	}

	public void setQuantity(GroupMeasureValue quantity)
	{
		GroupMeasureValue oldValue = this.quantity;
		this.quantity = quantity;
		if (quantity != null)
		{
			setQuantityGroupMeasure(quantity.getGroupMeasure());
			setQuantityValue(quantity.getValue());
			setQuantityConvertedUnit(quantity.getConvertedUnit());
			setQuantityConversion(quantity.getConversion());
			setQuantityConvertedValue(quantity.getConvertedValue());
		}
		else
		{
			setQuantityGroupMeasure(null);
			setQuantityConvertedUnit(null);
			setQuantityConvertedValue(null);
			setQuantityConversion(null);
			setQuantityValue(null);
		}
		firePropertyChange(PROPERTYNAME_QUANTITY, oldValue, quantity);
	}

	@com.netappsid.erp.annotations.GroupMeasure(showQuantity = true)
	@Scale(decimal = 10)
	@Validate
	@Transient
	public GroupMeasureValue getQuantity()
	{
		if (quantity == null)
		{
			quantity = new GroupMeasureValue();
			quantity.initialize(getQuantityGroupMeasure(), getQuantityConvertedUnit(), getQuantityConvertedValue(), getQuantityConversion(),
					getQuantityValue(), true);
		}

		return quantity;
	}

	private void setQuantityGroupMeasure(GroupMeasure quantityGroupMeasure)
	{
		GroupMeasure oldValue = getQuantityGroupMeasure();
		this.quantityGroupMeasure = quantityGroupMeasure;
		firePropertyChange(PROPERTYNAME_QUANTITYGROUPMEASURE, oldValue, quantityGroupMeasure);
	}

	@JoinColumn(name = "quantityGroupMeasure_id", insertable = true, updatable = true)
	@ManyToOne(optional = true, cascade = { CascadeType.MERGE }, fetch = FetchType.LAZY)
	@Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
	@AccessType(value = "field")
	public GroupMeasure getQuantityGroupMeasure()
	{
		return quantityGroupMeasure;
	}

	private void setQuantityValue(BigDecimal quantityValue)
	{
		BigDecimal oldValue = getQuantityValue();
		this.quantityValue = quantityValue;
		if (this.quantityValue != null)
		{
			this.quantityValue = this.quantityValue.setScale(10, RoundingMode.HALF_UP);
		}
		firePropertyChange(PROPERTYNAME_QUANTITYVALUE, oldValue, quantityValue);
	}

	@Column(name = "quantityValue", precision = 32, scale = 10)
	@AccessType(value = "field")
	public BigDecimal getQuantityValue()
	{
		return quantityValue;
	}

	private void setQuantityConversion(MeasureUnitConversion quantityConversion)
	{
		MeasureUnitConversion oldValue = getQuantityConversion();
		this.quantityConversion = quantityConversion;
		firePropertyChange(PROPERTYNAME_QUANTITYCONVERSION, oldValue, quantityConversion);
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public MeasureUnitConversion getQuantityConversion()
	{
		return quantityConversion;
	}

	private void setQuantityConvertedUnit(MeasureUnitMultiplier quantityConvertedUnit)
	{
		MeasureUnitMultiplier oldValue = getQuantityConvertedUnit();
		this.quantityConvertedUnit = quantityConvertedUnit;
		firePropertyChange(PROPERTYNAME_QUANTITYCONVERTEDUNIT, oldValue, quantityConvertedUnit);
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public MeasureUnitMultiplier getQuantityConvertedUnit()
	{
		return quantityConvertedUnit;
	}

	private void setQuantityConvertedValue(BigDecimal quantityConvertedValue)
	{
		BigDecimal oldValue = getQuantityConvertedValue();
		this.quantityConvertedValue = quantityConvertedValue;
		if (this.quantityConvertedValue != null)
		{
			this.quantityConvertedValue = this.quantityConvertedValue.setScale(10, RoundingMode.HALF_UP);
		}
		firePropertyChange(PROPERTYNAME_QUANTITYCONVERTEDVALUE, oldValue, quantityConvertedValue);
	}

	@Column(name = "quantityConvertedValue", precision = 32, scale = 10)
	@AccessType(value = "field")
	public BigDecimal getQuantityConvertedValue()
	{
		return quantityConvertedValue;
	}

	public void setQuantityProduced(GroupMeasureValue quantityProduced)
	{
		GroupMeasureValue oldValue = this.quantityProduced;
		this.quantityProduced = quantityProduced;
		if (quantityProduced != null)
		{
			setQuantityProducedGroupMeasure(quantityProduced.getGroupMeasure());
			setQuantityProducedValue(quantityProduced.getValue());
			setQuantityProducedConvertedUnit(quantityProduced.getConvertedUnit());
			setQuantityProducedConversion(quantityProduced.getConversion());
			setQuantityProducedConvertedValue(quantityProduced.getConvertedValue());
		}
		else
		{
			setQuantityProducedGroupMeasure(null);
			setQuantityProducedConvertedUnit(null);
			setQuantityProducedConvertedValue(null);
			setQuantityProducedConversion(null);
			setQuantityProducedValue(null);
		}
		firePropertyChange(PROPERTYNAME_QUANTITYPRODUCED, oldValue, quantityProduced);
	}

	@com.netappsid.erp.annotations.GroupMeasure(showQuantity = true)
	@Scale(decimal = 10)
	@Validate
	@Transient
	public GroupMeasureValue getQuantityProduced()
	{
		if (quantityProduced == null)
		{
			quantityProduced = new GroupMeasureValue();
			quantityProduced.initialize(getQuantityProducedGroupMeasure(), getQuantityProducedConvertedUnit(), getQuantityProducedConvertedValue(),
					getQuantityProducedConversion(), getQuantityProducedValue(), true);
		}

		return quantityProduced;
	}

	private void setQuantityProducedGroupMeasure(GroupMeasure quantityProducedGroupMeasure)
	{
		GroupMeasure oldValue = getQuantityProducedGroupMeasure();
		this.quantityProducedGroupMeasure = quantityProducedGroupMeasure;
		firePropertyChange(PROPERTYNAME_QUANTITYPRODUCEDGROUPMEASURE, oldValue, quantityProducedGroupMeasure);
	}

	@JoinColumn(name = "quantityProducedGroupMeasure_id", insertable = true, updatable = true)//$NON-NLS-1$
	@ManyToOne(optional = true, cascade = { CascadeType.MERGE }, fetch = FetchType.LAZY)
	@Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
	@AccessType(value = "field")
	public GroupMeasure getQuantityProducedGroupMeasure()
	{
		return quantityProducedGroupMeasure;
	}

	private void setQuantityProducedValue(BigDecimal quantityProducedValue)
	{
		BigDecimal oldValue = getQuantityProducedValue();
		this.quantityProducedValue = quantityProducedValue;
		if (this.quantityProducedValue != null)
		{
			this.quantityProducedValue = this.quantityProducedValue.setScale(10, RoundingMode.HALF_UP);
		}
		firePropertyChange(PROPERTYNAME_QUANTITYPRODUCEDVALUE, oldValue, quantityProducedValue);
	}

	@Column(name = "quantityProducedValue", precision = 32, scale = 10)//$NON-NLS-1$ //$NON-NLS-2$
	@AccessType(value = "field")
	public BigDecimal getQuantityProducedValue()
	{
		return quantityProducedValue;
	}

	private void setQuantityProducedConversion(MeasureUnitConversion quantityProducedConversion)
	{
		MeasureUnitConversion oldValue = getQuantityProducedConversion();
		this.quantityProducedConversion = quantityProducedConversion;
		firePropertyChange(PROPERTYNAME_QUANTITYPRODUCEDCONVERSION, oldValue, quantityProducedConversion);
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public MeasureUnitConversion getQuantityProducedConversion()
	{
		return quantityProducedConversion;
	}

	private void setQuantityProducedConvertedUnit(MeasureUnitMultiplier quantityProducedConvertedUnit)
	{
		MeasureUnitMultiplier oldValue = getQuantityProducedConvertedUnit();
		this.quantityProducedConvertedUnit = quantityProducedConvertedUnit;
		firePropertyChange(PROPERTYNAME_QUANTITYPRODUCEDCONVERTEDUNIT, oldValue, quantityProducedConvertedUnit);
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public MeasureUnitMultiplier getQuantityProducedConvertedUnit()
	{
		return quantityProducedConvertedUnit;
	}

	private void setQuantityProducedConvertedValue(BigDecimal quantityProducedConvertedValue)
	{
		BigDecimal oldValue = getQuantityProducedConvertedValue();
		this.quantityProducedConvertedValue = quantityProducedConvertedValue;
		if (this.quantityProducedConvertedValue != null)
		{
			this.quantityProducedConvertedValue = this.quantityProducedConvertedValue.setScale(10, RoundingMode.HALF_UP);
		}
		firePropertyChange(PROPERTYNAME_QUANTITYPRODUCEDCONVERTEDVALUE, oldValue, quantityProducedConvertedValue);
	}

	@Column(name = "quantityProducedConvertedValue", precision = 32, scale = 10)//$NON-NLS-1$ //$NON-NLS-2$
	@AccessType(value = "field")
	public BigDecimal getQuantityProducedConvertedValue()
	{
		return quantityProducedConvertedValue;
	}

	@Transient
	public GroupMeasureValue getQuantityAvailable()
	{
		if (!getQuantityProduced().isValid())
		{
			return getQuantity();
		}
		return getQuantity().substract(getQuantityProduced());
	}

	public static final String PROPERTYNAME_WORKLOAD = "workLoad";
	public static final String PROPERTYNAME_ORDERDETAIL = "orderDetail";
	public static final String PROPERTYNAME_QUANTITY = "quantity";
	public static final String PROPERTYNAME_QUANTITYGROUPMEASURE = "quantityGroupMeasure";
	public static final String PROPERTYNAME_QUANTITYVALUE = "quantityValue";
	public static final String PROPERTYNAME_QUANTITYCONVERSION = "quantityConversion";
	public static final String PROPERTYNAME_QUANTITYCONVERTEDUNIT = "quantityConvertedUnit";
	public static final String PROPERTYNAME_QUANTITYCONVERTEDVALUE = "quantityConvertedValue";
	public static final String PROPERTYNAME_QUANTITYPRODUCED = "quantityProduced"; //$NON-NLS-1$
	public static final String PROPERTYNAME_QUANTITYPRODUCEDGROUPMEASURE = "quantityProducedGroupMeasure"; //$NON-NLS-1$
	public static final String PROPERTYNAME_QUANTITYPRODUCEDVALUE = "quantityProducedValue"; //$NON-NLS-1$
	public static final String PROPERTYNAME_QUANTITYPRODUCEDCONVERSION = "quantityProducedConversion"; //$NON-NLS-1$
	public static final String PROPERTYNAME_QUANTITYPRODUCEDCONVERTEDUNIT = "quantityProducedConvertedUnit"; //$NON-NLS-1$
	public static final String PROPERTYNAME_QUANTITYPRODUCEDCONVERTEDVALUE = "quantityProducedConvertedValue"; //$NON-NLS-1$

}
