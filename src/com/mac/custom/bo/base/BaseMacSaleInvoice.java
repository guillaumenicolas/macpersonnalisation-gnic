package com.mac.custom.bo.base;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.AccessType;

import com.netappsid.erp.server.bo.SaleInvoice;
import com.netappsid.erp.server.bo.ServiceType;

@MappedSuperclass
public abstract class BaseMacSaleInvoice extends SaleInvoice
{
	protected ServiceType serviceType;
	protected String originalOrderNumber;
	
	@ManyToOne(optional = true)
	@AccessType(value = "field")//$NON-NLS-1$
	public ServiceType getServiceType() {
		return serviceType;
	}
	public void setServiceType(ServiceType serviceType) {
		ServiceType oldValue = getServiceType();
		this.serviceType = serviceType;
		firePropertyChange(PROPERTYNAME_SERVICETYPE, oldValue, serviceType);
	}

	@Column(name = "originalOrderNumber")//$NON-NLS-1$ 
	@AccessType(value = "field")//$NON-NLS-1$
	public String getOriginalOrderNumber() {
		return originalOrderNumber;
	}
	public void setOriginalOrderNumber(String originalOrderNumber) {
		String oldValue = getOriginalOrderNumber();
		this.originalOrderNumber = originalOrderNumber;
		firePropertyChange(PROPERTYNAME_ORIGINALORDERNUMBER, oldValue, originalOrderNumber);
	}
	

	public static final String PROPERTYNAME_SERVICETYPE = "serviceType"; //$NON-NLS-1$
	public static final String PROPERTYNAME_ORIGINALORDERNUMBER = "originalOrderNumber"; //$NON-NLS-1$

}