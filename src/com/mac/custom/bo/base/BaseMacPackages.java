package com.mac.custom.bo.base;

import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.AccessType;

import com.netappsid.erp.server.bo.Packages;
import com.netappsid.erp.server.bo.Shipping;

@MappedSuperclass
public class BaseMacPackages extends Packages
{
	private Shipping shipping;

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public Shipping getShipping()
	{
		return shipping;
	}

	public void setShipping(Shipping shipping)
	{
		Shipping oldValue = getShipping();
		this.shipping = shipping;
		firePropertyChange(PROPERTYNAME_SHIPPING, oldValue, shipping, false);
	}

	public static final String PROPERTYNAME_SHIPPING = "shipping";

}
