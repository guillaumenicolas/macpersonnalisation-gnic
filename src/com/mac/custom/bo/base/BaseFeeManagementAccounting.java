package com.mac.custom.bo.base;

import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.AccessType;

import com.mac.custom.bo.FeeCodeAnalytic;
import com.mac.custom.bo.ManagementAccounting;

@MappedSuperclass
public abstract class BaseFeeManagementAccounting extends ManagementAccounting
{
	private FeeCodeAnalytic feeCodeAnalyticSale;
	private FeeCodeAnalytic feeCodeAnalyticPurchase;

	public BaseFeeManagementAccounting()
	{
		super();
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public FeeCodeAnalytic getFeeCodeAnalyticSale()
	{
		return feeCodeAnalyticSale;
	}

	public void setFeeCodeAnalyticSale(FeeCodeAnalytic feeCodeAnalytic)
	{
		FeeCodeAnalytic oldValue = this.feeCodeAnalyticSale;
		this.feeCodeAnalyticSale = feeCodeAnalytic;

		firePropertyChange(PROPERTYNAME_FEECODEANALYTICSALE, oldValue, this.feeCodeAnalyticSale);
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public FeeCodeAnalytic getFeeCodeAnalyticPurchase()
	{
		return feeCodeAnalyticPurchase;
	}

	public void setFeeCodeAnalyticPurchase(FeeCodeAnalytic feeCodeAnalytic)
	{
		FeeCodeAnalytic oldValue = this.feeCodeAnalyticPurchase;
		this.feeCodeAnalyticPurchase = feeCodeAnalytic;

		firePropertyChange(PROPERTYNAME_FEECODEANALYTICPURCHASE, oldValue, this.feeCodeAnalyticPurchase);
	}

	public static final String PROPERTYNAME_FEECODEANALYTICSALE = "feeCodeAnalyticSale";
	public static final String PROPERTYNAME_FEECODEANALYTICPURCHASE = "feeCodeAnalyticPurchase";
}
