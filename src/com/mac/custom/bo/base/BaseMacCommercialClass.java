package com.mac.custom.bo.base;

import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinTable;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;

import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CollectionTypeInfo;
import org.hibernate.annotations.IndexColumn;

import com.mac.custom.bo.ActiviteClient;
import com.mac.custom.bo.EnseigneClient;
import com.netappsid.erp.server.bo.CommercialClass;
import com.netappsid.framework.utils.NaturalKeyValidator;
import com.netappsid.observable.ObservableCollections;
import com.netappsid.transactional.TransactionalCollections;
import com.netappsid.transactional.TransactionalList;

@MappedSuperclass
public abstract class BaseMacCommercialClass extends CommercialClass
{
	private final List<ActiviteClient> activitesClient = ObservableCollections.newObservableArrayList();
	private final List<EnseigneClient> enseignesClient = ObservableCollections.newObservableArrayList();

	@IndexColumn(name = "sequence")
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@CollectionTypeInfo(name = "com.netappsid.hibernate.LazyLoadListType")
	@AccessType(value = "field")
	@JoinTable(schema = "MacSchema")
	public List<ActiviteClient> getActivitesClient()
	{
		return activitesClient;
	}

	protected void onActivitesClientCollectionChange(TransactionalList<ActiviteClient> activitesClient)
	{
		TransactionalCollections.end(activitesClient);
	}

	public void addToActivitesClient(ActiviteClient activiteClient)
	{
		final TransactionalList<ActiviteClient> transactional = TransactionalCollections.begin(this.activitesClient);
		NaturalKeyValidator.isAddedItemValid(transactional, activiteClient);
		transactional.add(activiteClient);
		onActivitesClientCollectionChange(transactional);
	}

	public void addToActivitesClient(ActiviteClient activiteClient, int index)
	{
		final TransactionalList<ActiviteClient> transactional = TransactionalCollections.begin(this.activitesClient);
		NaturalKeyValidator.isAddedItemValid(transactional, activiteClient);
		transactional.add(index, activiteClient);
		onActivitesClientCollectionChange(transactional);
	}

	public void addManyToActivitesClient(Collection<ActiviteClient> activitesClient)
	{
		final TransactionalList<ActiviteClient> transactional = TransactionalCollections.begin(this.activitesClient);
		for (ActiviteClient activiteClient : activitesClient)
		{
			NaturalKeyValidator.isAddedItemValid(transactional, activiteClient);
		}

		transactional.addAll(activitesClient);
		onActivitesClientCollectionChange(transactional);
	}

	public void removeFromActivitesClient(ActiviteClient activiteClient)
	{
		final TransactionalList<ActiviteClient> transactional = TransactionalCollections.begin(this.activitesClient);
		transactional.remove(activiteClient);
		onActivitesClientCollectionChange(transactional);
	}

	public void removeManyFromActivitesClient(Collection<ActiviteClient> activitesClient)
	{
		final TransactionalList<ActiviteClient> transactional = TransactionalCollections.begin(this.activitesClient);
		transactional.removeAll(activitesClient);
		onActivitesClientCollectionChange(transactional);
	}

	public void removeAllFromActivitesClient()
	{
		final TransactionalList<ActiviteClient> transactional = TransactionalCollections.begin(this.activitesClient);
		transactional.clear();
		onActivitesClientCollectionChange(transactional);
	}

	public void replaceActivitesClient(ActiviteClient oldActiviteClient, ActiviteClient newActiviteClient)
	{
		final TransactionalList<ActiviteClient> transactional = TransactionalCollections.begin(this.activitesClient);
		final int indexOf = transactional.indexOf(oldActiviteClient);

		if (indexOf != -1)
		{
			NaturalKeyValidator.isAddedItemValid(transactional, newActiviteClient);
			transactional.set(indexOf, newActiviteClient);
		}
		onActivitesClientCollectionChange(transactional);
	}

	@IndexColumn(name = "sequence")
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@CollectionTypeInfo(name = "com.netappsid.hibernate.LazyLoadListType")
	@AccessType(value = "field")
	@JoinTable(schema = "MacSchema")
	public List<EnseigneClient> getEnseignesClient()
	{
		return enseignesClient;
	}

	protected void onEnseignesClientCollectionChange(TransactionalList<EnseigneClient> enseignesClient)
	{
		TransactionalCollections.end(enseignesClient);
	}

	public void addToEnseignesClient(EnseigneClient enseigneClient)
	{
		final TransactionalList<EnseigneClient> transactional = TransactionalCollections.begin(this.enseignesClient);
		NaturalKeyValidator.isAddedItemValid(transactional, enseigneClient);
		transactional.add(enseigneClient);
		onEnseignesClientCollectionChange(transactional);
	}

	public void addToEnseignesClient(EnseigneClient enseigneClient, int index)
	{
		final TransactionalList<EnseigneClient> transactional = TransactionalCollections.begin(this.enseignesClient);
		NaturalKeyValidator.isAddedItemValid(transactional, enseigneClient);
		transactional.add(index, enseigneClient);
		onEnseignesClientCollectionChange(transactional);
	}

	public void addManyToEnseignesClient(Collection<EnseigneClient> enseignesClient)
	{
		final TransactionalList<EnseigneClient> transactional = TransactionalCollections.begin(this.enseignesClient);
		for (EnseigneClient enseigneClient : enseignesClient)
		{
			NaturalKeyValidator.isAddedItemValid(transactional, enseigneClient);
		}

		transactional.addAll(enseignesClient);
		onEnseignesClientCollectionChange(transactional);
	}

	public void removeFromEnseignesClient(EnseigneClient enseigneClient)
	{
		final TransactionalList<EnseigneClient> transactional = TransactionalCollections.begin(this.enseignesClient);
		transactional.remove(enseigneClient);
		onEnseignesClientCollectionChange(transactional);
	}

	public void removeManyFromEnseignesClient(Collection<EnseigneClient> enseignesClient)
	{
		final TransactionalList<EnseigneClient> transactional = TransactionalCollections.begin(this.enseignesClient);
		transactional.removeAll(enseignesClient);
		onEnseignesClientCollectionChange(transactional);
	}

	public void removeAllFromEnseignesClient()
	{
		final TransactionalList<EnseigneClient> transactional = TransactionalCollections.begin(this.enseignesClient);
		transactional.clear();
		onEnseignesClientCollectionChange(transactional);
	}

	public void replaceEnseignesClient(EnseigneClient oldEnseigneClient, EnseigneClient newEnseigneClient)
	{
		final TransactionalList<EnseigneClient> transactional = TransactionalCollections.begin(this.enseignesClient);
		final int indexOf = transactional.indexOf(oldEnseigneClient);

		if (indexOf != -1)
		{
			NaturalKeyValidator.isAddedItemValid(transactional, newEnseigneClient);
			transactional.set(indexOf, newEnseigneClient);
		}
		onEnseignesClientCollectionChange(transactional);
	}
}
