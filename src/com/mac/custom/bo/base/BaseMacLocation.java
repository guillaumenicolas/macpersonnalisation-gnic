package com.mac.custom.bo.base;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.AccessType;

import com.netappsid.erp.server.bo.Location;

@MappedSuperclass
public abstract class BaseMacLocation extends Location
{
	private boolean store;

	public void setStore(boolean store)
	{
		boolean oldValue = isStore();
		this.store = store;
		firePropertyChange(PROPERTYNAME_STORE, oldValue, store);
	}

	@AccessType(value = "field")
	@Column
	public boolean isStore()
	{
		return store;
	}

	public static final String PROPERTYNAME_STORE = "store";
}
