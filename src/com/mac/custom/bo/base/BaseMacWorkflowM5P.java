package com.mac.custom.bo.base;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.netappsid.bo.Entity;
import com.netappsid.erp.server.bo.BlockingReason;
import com.netappsid.erp.server.bo.DocumentStatus;
import com.netappsid.validation.Validable;

@MappedSuperclass
public abstract class BaseMacWorkflowM5P extends Entity
{
	private Serializable id;
	
	private DocumentStatus m5pStatus;
	private DocumentStatus blockedStatus;
	private DocumentStatus unblockedStatus;
	private DocumentStatus refusedStatus;
	
	private BlockingReason blockingReason;
	private BlockingReason unblockingReason;
	
	private String emailAddressBE;
	private boolean active;
	
	@Id
	@GeneratedValue(generator = "naid-uuid")
	@GenericGenerator(name = "naid-uuid", strategy = "com.netappsid.framework.utils.UUIDGenerator")
	@Type(type = "com.netappsid.datatypes.UUIDUserType")
	@AccessType(value = "field")
	public Serializable getId()
	{
		return id;
	}

	public void setId(Serializable id)
	{
		this.id = id;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@com.netappsid.annotations.NotNull
	@org.hibernate.validator.NotNull
	@AccessType(value = "field")
	public DocumentStatus getM5pStatus()
	{
		return m5pStatus;
	}

	public void setM5pStatus(DocumentStatus m5pStatus)
	{
		DocumentStatus oldValue = this.m5pStatus;
		this.m5pStatus = m5pStatus;
		firePropertyChange(PROPERTYNAME_M5PSTATUS, oldValue, this.m5pStatus);
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@com.netappsid.annotations.NotNull
	@org.hibernate.validator.NotNull
	@AccessType(value = "field")
	public DocumentStatus getBlockedStatus()
	{
		return blockedStatus;
	}

	public void setBlockedStatus(DocumentStatus blockedStatus)
	{
		DocumentStatus oldValue = this.blockedStatus;
		this.blockedStatus = blockedStatus;
		firePropertyChange(PROPERTYNAME_BLOCKEDSTATUS, oldValue, this.blockedStatus);
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@com.netappsid.annotations.NotNull
	@org.hibernate.validator.NotNull
	@AccessType(value = "field")
	public DocumentStatus getUnblockedStatus()
	{
		return unblockedStatus;
	}

	public void setUnblockedStatus(DocumentStatus unblockedStatus)
	{
		DocumentStatus oldValue = this.unblockedStatus;
		this.unblockedStatus = unblockedStatus;
		firePropertyChange(PROPERTYNAME_UNBLOCKEDSTATUS, oldValue, this.unblockedStatus);
	}	
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@com.netappsid.annotations.NotNull
	@org.hibernate.validator.NotNull
	@AccessType(value = "field")
	public DocumentStatus getRefusedStatus()
	{
		return refusedStatus;
	}

	public void setRefusedStatus(DocumentStatus refusedStatus)
	{
		DocumentStatus oldValue = this.refusedStatus;
		this.refusedStatus = refusedStatus;
		firePropertyChange(PROPERTYNAME_REFUSEDSTATUS, oldValue, this.refusedStatus);
	}	

	public void setEmailAddressBE(String emailAddressBE)
	{
		String oldValue = getEmailAddressBE();
		this.emailAddressBE = emailAddressBE;
		firePropertyChange(PROPERTYNAME_EMAILADDRESSBE, oldValue, emailAddressBE);
	}

	@Column(name = "emailAddressBE")
	@com.netappsid.annotations.NotNull
	@org.hibernate.validator.NotNull
	@AccessType(value = "field")
	@org.hibernate.validator.Length(max = 255)
	@com.netappsid.annotations.Length(max = 255)
	public String getEmailAddressBE()
	{
		return emailAddressBE;
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@com.netappsid.annotations.NotNull
	@org.hibernate.validator.NotNull
	@AccessType(value = "field")
	public BlockingReason getBlockingReason()
	{
		return blockingReason;
	}

	public void setBlockingReason(BlockingReason blockingReason)
	{
		BlockingReason oldValue = this.blockingReason;
		this.blockingReason = blockingReason;
		firePropertyChange(PROPERTYNAME_BLOCKINGREASON, oldValue, this.blockingReason);
	}	
	
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@com.netappsid.annotations.NotNull
	@org.hibernate.validator.NotNull
	@AccessType(value = "field")
	public BlockingReason getUnblockingReason()
	{
		return unblockingReason;
	}

	public void setUnblockingReason(BlockingReason unblockingReason)
	{
		BlockingReason oldValue = this.unblockingReason;
		this.unblockingReason = unblockingReason;
		firePropertyChange(PROPERTYNAME_UNBLOCKINGREASON, oldValue, this.unblockingReason);
	}	
	
	public void setActive(boolean active)
	{
		boolean oldValue = isActive();
		this.active = active;
		firePropertyChange(PROPERTYNAME_ACTIVE, oldValue, active);
	}

	@Column(name = "active")
	@AccessType(value = "field")
	public boolean isActive()
	{
		return active;
	}
	
	public static final String PROPERTYNAME_BLOCKINGREASON = "blockingReason";
	public static final String PROPERTYNAME_UNBLOCKINGREASON = "unblockingReason";
	public static final String PROPERTYNAME_M5PSTATUS = "m5pStatus";
	public static final String PROPERTYNAME_BLOCKEDSTATUS = "blockedStatus";
	public static final String PROPERTYNAME_UNBLOCKEDSTATUS = "unblockedStatus";
	public static final String PROPERTYNAME_REFUSEDSTATUS = "refusedStatus";
	public static final String PROPERTYNAME_EMAILADDRESSBE = "emailAddressBE";
	public static final String PROPERTYNAME_ACTIVE = "active";
}
