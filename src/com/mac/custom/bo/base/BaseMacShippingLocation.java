package com.mac.custom.bo.base;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.mac.custom.bo.MacLocation;
import com.mac.custom.bo.MacWarehouse;
import com.netappsid.bo.Entity;

@MappedSuperclass
public abstract class BaseMacShippingLocation<T extends BaseMacShippingLocation<T>> extends Entity<T>
{
	private Serializable id;
	private MacWarehouse warehouse;
	private MacLocation location;
	private int sequence;

	@Override
	@Id
	@GeneratedValue(generator = "naid-uuid")
	@GenericGenerator(name = "naid-uuid", strategy = "com.netappsid.framework.utils.UUIDGenerator")
	@Type(type = "com.netappsid.datatypes.UUIDUserType")
	@AccessType(value = "field")
	public Serializable getId()
	{
		return id;
	}

	@Override
	public void setId(Serializable id)
	{
		this.id = id;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@com.netappsid.annotations.NotNull
	@org.hibernate.validator.NotNull
	@AccessType(value = "field")
	public MacWarehouse getWarehouse()
	{
		return warehouse;
	}

	public void setWarehouse(MacWarehouse warehouse)
	{
		MacWarehouse oldValue = this.warehouse;
		this.warehouse = warehouse;
		firePropertyChange(PROPERTYNAME_WAREHOUSE, oldValue, this.warehouse);
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@com.netappsid.annotations.NotNull
	@org.hibernate.validator.NotNull
	@AccessType(value = "field")
	public MacLocation getLocation()
	{
		return location;
	}

	public void setLocation(MacLocation arcSentStatus)
	{
		MacLocation oldValue = this.location;
		this.location = arcSentStatus;
		firePropertyChange(PROPERTYNAME_LOCATION, oldValue, this.location);
	}

	@Column(insertable = false, updatable = false)
	@AccessType(value = "field")
	public int getSequence()
	{
		return sequence;
	}

	public static final String PROPERTYNAME_WAREHOUSE = "warehouse";
	public static final String PROPERTYNAME_LOCATION = "location";
}
