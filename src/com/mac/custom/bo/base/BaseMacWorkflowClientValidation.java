package com.mac.custom.bo.base;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.netappsid.bo.Entity;
import com.netappsid.erp.server.bo.DocumentStatus;
import com.netappsid.erp.server.bo.Report;

@MappedSuperclass
public abstract class BaseMacWorkflowClientValidation extends Entity
{
	private Serializable id;
	
	private String emailOrigin;
	private Report report;
	
	private String filePath;
	private String filePathLocal;
	
	private boolean active;
	
	@Id
	@GeneratedValue(generator = "naid-uuid")
	@GenericGenerator(name = "naid-uuid", strategy = "com.netappsid.framework.utils.UUIDGenerator")
	@Type(type = "com.netappsid.datatypes.UUIDUserType")
	@AccessType(value = "field")
	public Serializable getId()
	{
		return id;
	}

	public void setId(Serializable id)
	{
		this.id = id;
	}

	public void setEmailOrigin(String emailOrigin)
	{
		String oldValue = getEmailOrigin();
		this.emailOrigin = emailOrigin;
		firePropertyChange(PROPERTYNAME_EMAILORIGIN, oldValue, emailOrigin);
	}

	@Column(name = "emailOrigin")
	@com.netappsid.annotations.NotNull
	@org.hibernate.validator.NotNull
	@AccessType(value = "field")
	@org.hibernate.validator.Length(max = 255)
	@com.netappsid.annotations.Length(max = 255)
	public String getEmailOrigin()
	{
		return emailOrigin;
	}
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@org.hibernate.validator.NotNull
	@com.netappsid.annotations.NotNull
	@AccessType(value = "field")
	public Report getReport()
	{
		return report;
	}

	public void setReport(Report report)
	{
		Report oldValue = this.report;
		this.report = report;
		firePropertyChange(PROPERTYNAME_REPORT, oldValue, this.report);
	}
	
	public void setFilePath(String filePath)
	{
		String oldValue = getFilePath();
		this.filePath = filePath;
		firePropertyChange(PROPERTYNAME_FILEPATH, oldValue, filePath);
	}

	@Column(name = "filePath")
	@com.netappsid.annotations.NotNull
	@org.hibernate.validator.NotNull
	@AccessType(value = "field")
	@org.hibernate.validator.Length(max = 255)
	@com.netappsid.annotations.Length(max = 255)
	public String getFilePath()
	{
		return filePath;
	}

	public void setFilePathLocal(String filePathLocal)
	{
		String oldValue = getFilePathLocal();
		this.filePathLocal = filePathLocal;
		firePropertyChange(PROPERTYNAME_FILEPATHLOCAL, oldValue, filePathLocal);
	}

	@Column(name = "filePathLocal")
	@com.netappsid.annotations.NotNull
	@org.hibernate.validator.NotNull
	@AccessType(value = "field")
	@org.hibernate.validator.Length(max = 255)
	@com.netappsid.annotations.Length(max = 255)
	public String getFilePathLocal()
	{
		return filePathLocal;
	}	
	
	public void setActive(boolean active)
	{
		boolean oldValue = isActive();
		this.active = active;
		firePropertyChange(PROPERTYNAME_ACTIVE, oldValue, active);
	}

	@Column(name = "active") 
	@AccessType(value = "field")
	public boolean isActive()
	{
		return active;
	}
	
	public static final String PROPERTYNAME_EMAILORIGIN = "emailOrigin";
	public static final String PROPERTYNAME_REPORT = "report";
	public static final String PROPERTYNAME_FILEPATH = "filePath";
	public static final String PROPERTYNAME_FILEPATHLOCAL = "filePathLocal";
	public static final String PROPERTYNAME_ACTIVE = "active";
}
