package com.mac.custom.bo.base;

import com.mac.custom.bo.MacManufacturedProduct;
import com.mac.custom.bo.ManufacturedProductManagementAccounting;
import com.netappsid.bo.EntityStatus;
import com.netappsid.europe.naid.custom.bo.ManufacturedProductEuro;
import com.netappsid.framework.utils.NaturalKeyValidator;
import com.netappsid.observable.ObservableCollections;
import com.netappsid.transactional.TransactionalCollections;
import com.netappsid.transactional.TransactionalList;
import java.util.Collection;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import org.hibernate.Hibernate;
import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CollectionTypeInfo;
import org.hibernate.annotations.IndexColumn;

@MappedSuperclass
public class BaseMacManufacturedProduct extends ManufacturedProductEuro
{
	private final List<ManufacturedProductManagementAccounting> managementAccountings = ObservableCollections.newObservableArrayList();

	@Override
	public void setChildEntityStatus(EntityStatus entityStatus)
	{
		super.setChildEntityStatus(entityStatus);

		if (getManagementAccountings() != null && Hibernate.isInitialized(getManagementAccountings()))
		{
			for (ManufacturedProductManagementAccounting manufacturedProductManagementAccounting : getManagementAccountings())
			{
				manufacturedProductManagementAccounting.setEntityStatus(entityStatus);
			}
		}
	}

	@IndexColumn(name = "sequence")
	@JoinColumn(name = "macManufacturedProduct_id")
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
	@CollectionTypeInfo(name = "com.netappsid.hibernate.LazyLoadListType")
	@AccessType(value = "field")//$NON-NLS-1$
	public List<ManufacturedProductManagementAccounting> getManagementAccountings()
	{
		return managementAccountings;
	}

	protected void onManagementAccountingsCollectionChange(TransactionalList<ManufacturedProductManagementAccounting> managementAccountings)
	{
		TransactionalCollections.end(managementAccountings);
	}

	public void addToManagementAccountings(ManufacturedProductManagementAccounting managementAccounting)
	{
		final TransactionalList<ManufacturedProductManagementAccounting> transactional = TransactionalCollections.begin(this.managementAccountings);
		managementAccounting.setMacManufacturedProduct((MacManufacturedProduct) this);
		NaturalKeyValidator.isAddedItemValid(transactional, managementAccounting);
		transactional.add(managementAccounting);
		onManagementAccountingsCollectionChange(transactional);
	}

	public void addToManagementAccountings(ManufacturedProductManagementAccounting managementAccounting, int index)
	{
		final TransactionalList<ManufacturedProductManagementAccounting> transactional = TransactionalCollections.begin(this.managementAccountings);
		managementAccounting.setMacManufacturedProduct((MacManufacturedProduct) this);
		NaturalKeyValidator.isAddedItemValid(transactional, managementAccounting);
		transactional.add(index, managementAccounting);
		onManagementAccountingsCollectionChange(transactional);
	}

	public void addManyToManagementAccountings(Collection<ManufacturedProductManagementAccounting> managementAccountings)
	{
		final TransactionalList<ManufacturedProductManagementAccounting> transactional = TransactionalCollections.begin(this.managementAccountings);
		for (ManufacturedProductManagementAccounting managementAccounting : managementAccountings)
		{
			managementAccounting.setMacManufacturedProduct((MacManufacturedProduct) this);
			NaturalKeyValidator.isAddedItemValid(transactional, managementAccounting);
		}

		transactional.addAll(managementAccountings);
		onManagementAccountingsCollectionChange(transactional);
	}

	public void removeFromManagementAccountings(ManufacturedProductManagementAccounting managementAccounting)
	{
		final TransactionalList<ManufacturedProductManagementAccounting> transactional = TransactionalCollections.begin(this.managementAccountings);
		if (transactional.remove(managementAccounting))
		{
			managementAccounting.setMacManufacturedProduct(null);
		}
		transactional.remove(managementAccounting);
		onManagementAccountingsCollectionChange(transactional);
	}

	public void removeManyFromManagementAccountings(Collection<ManufacturedProductManagementAccounting> managementAccountings)
	{
		final TransactionalList<ManufacturedProductManagementAccounting> transactional = TransactionalCollections.begin(this.managementAccountings);
		for (ManufacturedProductManagementAccounting managementAccounting : managementAccountings)
		{
			if (transactional.contains(managementAccounting))
			{
				managementAccounting.setMacManufacturedProduct(null);
			}
		}
		transactional.removeAll(managementAccountings);
		onManagementAccountingsCollectionChange(transactional);
	}

	public void removeAllFromManagementAccountings()
	{
		final TransactionalList<ManufacturedProductManagementAccounting> transactional = TransactionalCollections.begin(this.managementAccountings);
		for (ManufacturedProductManagementAccounting managementAccounting : this.managementAccountings)
		{
			managementAccounting.setMacManufacturedProduct(null);
		}
		transactional.clear();
		onManagementAccountingsCollectionChange(transactional);
	}

	public void replaceManagementAccountings(ManufacturedProductManagementAccounting oldManagementAccounting,
			ManufacturedProductManagementAccounting newManagementAccounting)
	{
		final TransactionalList<ManufacturedProductManagementAccounting> transactional = TransactionalCollections.begin(this.managementAccountings);
		final int indexOf = transactional.indexOf(oldManagementAccounting);

		if (indexOf != -1)
		{
			newManagementAccounting.setMacManufacturedProduct((MacManufacturedProduct) this);
			NaturalKeyValidator.isAddedItemValid(transactional, newManagementAccounting);
			transactional.set(indexOf, newManagementAccounting);
			newManagementAccounting.setMacManufacturedProduct(null);
		}
		onManagementAccountingsCollectionChange(transactional);
	}

	public ManufacturedProductManagementAccounting setToManagementAccountings(int index, ManufacturedProductManagementAccounting managementAccounting)
	{
		final TransactionalList<ManufacturedProductManagementAccounting> transactional = TransactionalCollections.begin(this.managementAccountings);
		managementAccounting.setMacManufacturedProduct((MacManufacturedProduct) this);
		NaturalKeyValidator.isAddedItemValid(transactional, managementAccounting);

		ManufacturedProductManagementAccounting removed = transactional.set(index, managementAccounting);

		if (removed != null)
		{
			removed.setMacManufacturedProduct(null);
		}
		onManagementAccountingsCollectionChange(transactional);
		return removed;
	}
}