package com.mac.custom.bo.base;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.mac.custom.bo.MacWorkflowARC;
import com.mac.custom.bo.MacWorkflowBlockAuto;
import com.mac.custom.bo.MacWorkflowClientValidation;
import com.mac.custom.bo.MacWorkflowFinanceValidation;
import com.mac.custom.bo.MacWorkflowUnblockManual;
import com.mac.custom.bo.MacWorkflowM5P;
import com.mac.custom.bo.MacWorkflowUnblockAuto;
import com.netappsid.bo.Entity;
import com.netappsid.erp.server.bo.User;

@MappedSuperclass
public abstract class BaseMacWorkflowMaster extends Entity
{
	private Serializable id;
	private MacWorkflowARC wfARC;
	private MacWorkflowM5P wfM5P;
	private MacWorkflowUnblockAuto wfUnblockAuto;
	private MacWorkflowUnblockManual wfUnblockManual;
	private MacWorkflowBlockAuto wfBlockAuto;
	private MacWorkflowFinanceValidation wfFinanceValidation;
	private MacWorkflowClientValidation wfClientValidation;
	private User sysAdmin;
	private User userWF;
	
	@Id
	@GeneratedValue(generator = "naid-uuid")
	@GenericGenerator(name = "naid-uuid", strategy = "com.netappsid.framework.utils.UUIDGenerator")
	@Type(type = "com.netappsid.datatypes.UUIDUserType")
	@AccessType(value = "field")
	public Serializable getId()
	{
		return id;
	}

	public void setId(Serializable id)
	{
		this.id = id;
	}
	
	@ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@AccessType(value = "field")
	public MacWorkflowARC getWfARC()
	{
		return wfARC;
	}

	public void setWfARC(MacWorkflowARC wfARC)
	{
		MacWorkflowARC oldValue = this.wfARC;
		this.wfARC = wfARC;
		firePropertyChange(PROPERTYNAME_WFARC, oldValue, this.wfARC);
	}
	
	@ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@AccessType(value = "field")
	public MacWorkflowM5P getWfM5P()
	{
		return wfM5P;
	}

	public void setWfM5P(MacWorkflowM5P wfM5P)
	{
		MacWorkflowM5P oldValue = this.wfM5P;
		this.wfM5P = wfM5P;
		firePropertyChange(PROPERTYNAME_WFM5P, oldValue, this.wfM5P);
	}
	
	@ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@AccessType(value = "field")
	public MacWorkflowUnblockAuto getWfUnblockAuto()
	{
		return wfUnblockAuto;
	}

	public void setWfUnblockAuto(MacWorkflowUnblockAuto wfUnblockAuto)
	{
		MacWorkflowUnblockAuto oldValue = this.wfUnblockAuto;
		this.wfUnblockAuto = wfUnblockAuto;
		firePropertyChange(PROPERTYNAME_WFUNBLOCKAUTO, oldValue, this.wfUnblockAuto);
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@AccessType(value = "field")
	public MacWorkflowBlockAuto getWfBlockAuto()
	{
		return wfBlockAuto;
	}

	public void setWfBlockAuto(MacWorkflowBlockAuto wfBlockAuto)
	{
		MacWorkflowBlockAuto oldValue = this.wfBlockAuto;
		this.wfBlockAuto = wfBlockAuto;
		firePropertyChange(PROPERTYNAME_WFBLOCKAUTO, oldValue, this.wfBlockAuto);
	}
	
	@ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@AccessType(value = "field")
	public MacWorkflowUnblockManual getWfUnblockManual()
	{
		return wfUnblockManual;
	}

	public void setWfUnblockManual(MacWorkflowUnblockManual wfBlockManual)
	{
		MacWorkflowUnblockManual oldValue = this.wfUnblockManual;
		this.wfUnblockManual = wfBlockManual;
		firePropertyChange(PROPERTYNAME_WFUNBLOCKMANUAL, oldValue, this.wfUnblockManual);
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@AccessType(value = "field")
	public MacWorkflowFinanceValidation getWfFinanceValidation()
	{
		return wfFinanceValidation;
	}

	public void setWfFinanceValidation(MacWorkflowFinanceValidation wfFinanceValidation)
	{
		MacWorkflowFinanceValidation oldValue = this.wfFinanceValidation;
		this.wfFinanceValidation = wfFinanceValidation;
		firePropertyChange(PROPERTYNAME_WFFINANCEVALIDATION, oldValue, this.wfFinanceValidation);
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@AccessType(value = "field")
	public MacWorkflowClientValidation getWfClientValidation()
	{
		return wfClientValidation;
	}

	public void setWfClientValidation(MacWorkflowClientValidation wfClientValidation)
	{
		MacWorkflowClientValidation oldValue = this.wfClientValidation;
		this.wfClientValidation = wfClientValidation;
		firePropertyChange(PROPERTYNAME_WFCLIENTVALIDATION, oldValue, this.wfClientValidation);
	}
	
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public User getSysAdmin()
	{
		return sysAdmin;
	}

	public void setSysAdmin(User sysAdmin)
	{
		User oldValue = this.sysAdmin;
		this.sysAdmin = sysAdmin;
		firePropertyChange(PROPERTYNAME_SYSADMIN, oldValue, this.sysAdmin);
	}
	
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public User getUserWF()
	{
		return userWF;
	}

	public void setUserWF(User userWF)
	{
		User oldValue = this.userWF;
		this.userWF = userWF;
		firePropertyChange(PROPERTYNAME_USERWF, oldValue, this.userWF);
	}
	
	public static final String PROPERTYNAME_WFARC = "wfARC";
	public static final String PROPERTYNAME_WFM5P = "wfM5P";
	public static final String PROPERTYNAME_WFUNBLOCKAUTO = "wfUnblockAuto";
	public static final String PROPERTYNAME_WFBLOCKAUTO = "wfBlockAuto";
	public static final String PROPERTYNAME_WFUNBLOCKMANUAL = "wfUnBlockManual";
	public static final String PROPERTYNAME_WFFINANCEVALIDATION = "wfFinanceValidation";
	public static final String PROPERTYNAME_WFCLIENTVALIDATION = "wfClientValidation";
	public static final String PROPERTYNAME_SYSADMIN = "sysAdmin";
	public static final String PROPERTYNAME_USERWF = "userWF";
}
