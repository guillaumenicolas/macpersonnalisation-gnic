package com.mac.custom.bo.base;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.AccessType;

import com.netappsid.erp.server.bo.ServiceType;
import com.netappsid.europe.naid.custom.bo.QuotationEuro;

@MappedSuperclass
public abstract class BaseMacQuotation extends QuotationEuro
{
	protected ServiceType serviceType;
	protected String originalOrderNumber;
	boolean sentToCommercial;

	@ManyToOne(optional = true)
	@AccessType(value = "field")//$NON-NLS-1$
	public ServiceType getServiceType()
	{
		return serviceType;
	}

	public void setServiceType(ServiceType serviceType)
	{
		ServiceType oldValue = getServiceType();
		this.serviceType = serviceType;
		firePropertyChange(PROPERTYNAME_SERVICETYPE, oldValue, serviceType);
	}

	@Column(name = "originalOrderNumber")//$NON-NLS-1$ 
	@AccessType(value = "field")//$NON-NLS-1$
	public String getOriginalOrderNumber()
	{
		return originalOrderNumber;
	}

	public void setOriginalOrderNumber(String originalOrderNumber)
	{
		String oldValue = getOriginalOrderNumber();
		this.originalOrderNumber = originalOrderNumber;
		firePropertyChange(PROPERTYNAME_ORIGINALORDERNUMBER, oldValue, originalOrderNumber);
	}

	@Column(name = "sentToCommercial")//$NON-NLS-1$ 
	@AccessType(value = "field")//$NON-NLS-1$
	public boolean getSentToCommercial()
	{
		return sentToCommercial;
	}

	public void setSentToCommercial(boolean sentToCommercial)
	{
		boolean oldValue = getSentToCommercial();
		this.sentToCommercial = sentToCommercial;
		firePropertyChange(PROPERTYNAME_SENTTOCOMMERCIAL, oldValue, sentToCommercial);
	}

	public static final String PROPERTYNAME_SERVICETYPE = "serviceType"; //$NON-NLS-1$
	public static final String PROPERTYNAME_ORIGINALORDERNUMBER = "originalOrderNumber"; //$NON-NLS-1$
	public static final String PROPERTYNAME_SENTTOCOMMERCIAL = "sentToCommercial"; //$NON-NLS-1$

}