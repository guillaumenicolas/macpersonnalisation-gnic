package com.mac.custom.bo.base;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;

import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CollectionTypeInfo;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.IndexColumn;
import org.hibernate.annotations.Type;

import com.mac.custom.bo.MacWorkflowARCProductDelay;
import com.netappsid.bo.Entity;
import com.netappsid.erp.server.bo.DocumentStatus;
import com.netappsid.erp.server.bo.WorkShift;
import com.netappsid.framework.utils.NaturalKeyValidator;
import com.netappsid.observable.ObservableCollections;
import com.netappsid.transactional.TransactionalCollections;
import com.netappsid.transactional.TransactionalList;

@MappedSuperclass
public abstract class BaseMacWorkflowARC extends Entity
{
	private Serializable id;
	
	private DocumentStatus initialDelayStatus;
	private DocumentStatus arcSentStatus;
	
	private String arcFilePath;
	private String arcFilePathLocal;
	private String emailOrigin;
	
	private Short approvalDelay;
	private boolean active;

	private final List<MacWorkflowARCProductDelay> approvalProductDelays = ObservableCollections.newObservableArrayList();
	
	private WorkShift workShift;
	
	@Override
	@Id
	@GeneratedValue(generator = "naid-uuid")
	@GenericGenerator(name = "naid-uuid", strategy = "com.netappsid.framework.utils.UUIDGenerator")
	@Type(type = "com.netappsid.datatypes.UUIDUserType")
	@AccessType(value = "field")
	public Serializable getId()
	{
		return id;
	}

	@Override
	public void setId(Serializable id)
	{
		this.id = id;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@com.netappsid.annotations.NotNull
	@org.hibernate.validator.NotNull
	@AccessType(value = "field")
	public DocumentStatus getInitialDelayStatus()
	{
		return initialDelayStatus;
	}

	public void setInitialDelayStatus(DocumentStatus initialDelayStatus)
	{
		DocumentStatus oldValue = this.initialDelayStatus;
		this.initialDelayStatus = initialDelayStatus;
		firePropertyChange(PROPERTYNAME_INITIALDELAYSTATUS, oldValue, this.initialDelayStatus);
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@com.netappsid.annotations.NotNull
	@org.hibernate.validator.NotNull
	@AccessType(value = "field")
	public DocumentStatus getArcSentStatus()
	{
		return arcSentStatus;
	}

	public void setArcSentStatus(DocumentStatus arcSentStatus)
	{
		DocumentStatus oldValue = this.arcSentStatus;
		this.arcSentStatus = arcSentStatus;
		firePropertyChange(PROPERTYNAME_ARCSENTSTATUS, oldValue, this.arcSentStatus);
	}

	public void setArcFilePath(String arcFilePath)
	{
		String oldValue = getArcFilePath();
		this.arcFilePath = arcFilePath;
		firePropertyChange(PROPERTYNAME_ARCFILEPATH, oldValue, arcFilePath);
	}

	@Column(name = "arcFilePath")
	@com.netappsid.annotations.NotNull
	@org.hibernate.validator.NotNull
	@AccessType(value = "field")
	@org.hibernate.validator.Length(max = 255)
	@com.netappsid.annotations.Length(max = 255)
	public String getArcFilePath()
	{
		return arcFilePath;
	}

	public void setArcFilePathLocal(String arcFilePathLocal)
	{
		String oldValue = getArcFilePathLocal();
		this.arcFilePathLocal = arcFilePathLocal;
		firePropertyChange(PROPERTYNAME_ARCFILEPATHLOCAL, oldValue, arcFilePathLocal);
	}

	@Column(name = "arcFilePathLocal")
	@com.netappsid.annotations.NotNull
	@org.hibernate.validator.NotNull
	@AccessType(value = "field")
	@org.hibernate.validator.Length(max = 255)
	@com.netappsid.annotations.Length(max = 255)
	public String getArcFilePathLocal()
	{
		return arcFilePathLocal;
	}

	public void setEmailOrigin(String emailOrigin)
	{
		String oldValue = getEmailOrigin();
		this.emailOrigin = emailOrigin;
		firePropertyChange(PROPERTYNAME_EMAILORIGIN, oldValue, emailOrigin);
	}

	@Column(name = "emailOrigin")
	@com.netappsid.annotations.NotNull
	@org.hibernate.validator.NotNull
	@AccessType(value = "field")
	@org.hibernate.validator.Length(max = 255)
	@com.netappsid.annotations.Length(max = 255)
	public String getEmailOrigin()
	{
		return emailOrigin;
	}

	public void setApprovalDelay(Short approvalDelay)
	{
		Short oldValue = getApprovalDelay();
		this.approvalDelay = approvalDelay;
		firePropertyChange(PROPERTYNAME_APPROVALDELAY, oldValue, approvalDelay);
	}

	@Column(name = "approvalDelay")
	@org.hibernate.validator.NotNull
	@com.netappsid.annotations.NotNull
	@org.hibernate.validator.Min(0)
	@com.netappsid.annotations.Min(0)
	@AccessType(value = "field")
	public Short getApprovalDelay()
	{
		return approvalDelay;
	}
	
	@IndexColumn(name = "sequence")
	@JoinColumn(name = "workflowarc_id")
	@Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@Index(name = "approval_product_delays_x")
	@CollectionTypeInfo(name = "com.netappsid.hibernate.LazyLoadListType")
	@AccessType(value = "field")
	public List<MacWorkflowARCProductDelay> getApprovalProductDelays()
	{
		return approvalProductDelays;
	}

	protected void onApprovalProductDelaysCollectionChange(TransactionalList<MacWorkflowARCProductDelay> approvalProductDelays)
	{
		TransactionalCollections.end(approvalProductDelays);
	}

	public void addToApprovalProductDelays(MacWorkflowARCProductDelay approvalProductDelay)
	{
		final TransactionalList<MacWorkflowARCProductDelay> transactional = TransactionalCollections.begin(this.approvalProductDelays);
		NaturalKeyValidator.isAddedItemValid(transactional, approvalProductDelay);
		transactional.add(approvalProductDelay);
		onApprovalProductDelaysCollectionChange(transactional);
	}

	public void addToApprovalProductDelays(MacWorkflowARCProductDelay approvalProductDelay, int index)
	{
		final TransactionalList<MacWorkflowARCProductDelay> transactional = TransactionalCollections.begin(this.approvalProductDelays);
		NaturalKeyValidator.isAddedItemValid(transactional, approvalProductDelay);
		transactional.add(index, approvalProductDelay);
		onApprovalProductDelaysCollectionChange(transactional);
	}

	public void addManyToApprovalProductDelays(Collection<MacWorkflowARCProductDelay> approvalProductDelays)
	{
		final TransactionalList<MacWorkflowARCProductDelay> transactional = TransactionalCollections.begin(this.approvalProductDelays);
		for (MacWorkflowARCProductDelay approvalProductDelay : approvalProductDelays)
		{
			NaturalKeyValidator.isAddedItemValid(transactional, approvalProductDelay);
		}

		transactional.addAll(approvalProductDelays);
		onApprovalProductDelaysCollectionChange(transactional);
	}

	public void removeFromApprovalProductDelays(MacWorkflowARCProductDelay approvalProductDelay)
	{
		final TransactionalList<MacWorkflowARCProductDelay> transactional = TransactionalCollections.begin(this.approvalProductDelays);
		transactional.remove(approvalProductDelay);
		onApprovalProductDelaysCollectionChange(transactional);
	}

	public void removeManyFromApprovalProductDelays(Collection<MacWorkflowARCProductDelay> approvalProductDelays)
	{
		final TransactionalList<MacWorkflowARCProductDelay> transactional = TransactionalCollections.begin(this.approvalProductDelays);
		transactional.removeAll(approvalProductDelays);
		onApprovalProductDelaysCollectionChange(transactional);
	}

	public void removeAllFromApprovalProductDelays()
	{
		final TransactionalList<MacWorkflowARCProductDelay> transactional = TransactionalCollections.begin(this.approvalProductDelays);
		transactional.clear();
		onApprovalProductDelaysCollectionChange(transactional);
	}

	public void replaceApprovalProductDelays(MacWorkflowARCProductDelay oldApprovalProductDelay, MacWorkflowARCProductDelay newApprovalProductDelays)
	{
		final TransactionalList<MacWorkflowARCProductDelay> transactional = TransactionalCollections.begin(this.approvalProductDelays);
		final int indexOf = transactional.indexOf(oldApprovalProductDelay);

		if (indexOf != -1)
		{
			NaturalKeyValidator.isAddedItemValid(transactional, newApprovalProductDelays);
			transactional.set(indexOf, newApprovalProductDelays);
		}
		onApprovalProductDelaysCollectionChange(transactional);
	}

	public MacWorkflowARCProductDelay setToApprovalProductDelays(int index, MacWorkflowARCProductDelay approvalProductDelay)
	{
		final TransactionalList<MacWorkflowARCProductDelay> transactional = TransactionalCollections.begin(this.approvalProductDelays);
		NaturalKeyValidator.isAddedItemValid(transactional, approvalProductDelay);

		MacWorkflowARCProductDelay removed = transactional.set(index, approvalProductDelay);

		onApprovalProductDelaysCollectionChange(transactional);
		return removed;
	}
	
	public void setActive(boolean active)
	{
		boolean oldValue = isActive();
		this.active = active;
		firePropertyChange(PROPERTYNAME_ACTIVE, oldValue, active);
	}

	@Column(name = "active")//$NON-NLS-1$
	@AccessType(value = "field")//$NON-NLS-1$
	public boolean isActive()
	{
		return active;
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "workShift_id")
	@AccessType(value = "field")
	public WorkShift getWorkShift()
	{
		return workShift;
	}

	public void setWorkShift(WorkShift workShift)
	{
		WorkShift oldValue = this.workShift;
		this.workShift = workShift;
		firePropertyChange(PROPERTYNAME_WORKSHIFT, oldValue, this.workShift);
	}
	
	public static final String PROPERTYNAME_INITIALDELAYSTATUS = "initialDelayStatus";
	public static final String PROPERTYNAME_ARCSENTSTATUS = "arcSentStatus";
	public static final String PROPERTYNAME_ARCFILEPATH = "arcFilePath";
	public static final String PROPERTYNAME_ARCFILEPATHLOCAL = "arcFilePathLocal";
	public static final String PROPERTYNAME_APPROVALDELAY = "approvalDelay";
	public static final String PROPERTYNAME_EMAILORIGIN = "emailOrigin";
	public static final String PROPERTYNAME_ACTIVE = "active";
	public static final String PROPERTYNAME_APPROVALPRODUCTDELAYS = "approvalProductDelays";
	public static final String PROPERTYNAME_WORKSHIFT = "workShift";
}
