package com.mac.custom.bo.base;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;

import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CollectionTypeInfo;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.IndexColumn;
import org.hibernate.annotations.Type;

import com.mac.custom.bo.FeeManagementAccounting;
import com.netappsid.bo.Entity;
import com.netappsid.erp.server.bo.Fee;
import com.netappsid.framework.utils.NaturalKeyValidator;
import com.netappsid.framework.utils.ObservableCollectionUtils;
import com.netappsid.observable.ObservableCollections;
import com.netappsid.transactional.TransactionalCollections;
import com.netappsid.transactional.TransactionalList;

@MappedSuperclass
public abstract class BaseFeeAccounting extends Entity
{
	private Serializable id;
	private Fee fee;
	private final List<FeeManagementAccounting> feeManagementAccountings = ObservableCollections.newObservableArrayList();

	public BaseFeeAccounting()
	{
		super();
	}

	@Override
	@Id
	@GeneratedValue(generator = "naid-uuid")
	@GenericGenerator(name = "naid-uuid", strategy = "com.netappsid.framework.utils.UUIDGenerator")
	@Type(type = "com.netappsid.datatypes.UUIDUserType")
	@AccessType(value = "field")
	public Serializable getId()
	{
		return id;
	}

	@Override
	public void setId(Serializable id)
	{
		this.id = id;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	@org.hibernate.validator.NotNull
	public Fee getFee()
	{
		return fee;
	}

	public void setFee(Fee fee)
	{
		Fee oldValue = this.fee;
		this.fee = fee;

		firePropertyChange(PROPERTYNAME_FEE, oldValue, this.fee);
	}

	@IndexColumn(name = "sequence")//$NON-NLS-1$
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@JoinTable(schema = "MacSchema")
	@Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
	@CollectionTypeInfo(name = "com.netappsid.hibernate.LazyLoadListType")
	@AccessType(value = "field")//$NON-NLS-1$
	public List<FeeManagementAccounting> getFeeManagementAccountings()
	{
		return feeManagementAccountings;
	}

	public void setFeeManagementAccountings(List<FeeManagementAccounting> feeManagementAccountings)
	{
		ObservableCollectionUtils.clearAndAddAll(this.feeManagementAccountings, feeManagementAccountings);
	}

	protected void onFeeManagementAccountingsCollectionChange(TransactionalList<FeeManagementAccounting> feeManagementAccountings)
	{
		TransactionalCollections.end(feeManagementAccountings);
	}

	public void addToFeeManagementAccountings(FeeManagementAccounting feeManagementAccounting)
	{
		final TransactionalList<FeeManagementAccounting> transactional = TransactionalCollections.begin(this.feeManagementAccountings);
		NaturalKeyValidator.isAddedItemValid(transactional, feeManagementAccounting);
		transactional.add(feeManagementAccounting);
		onFeeManagementAccountingsCollectionChange(transactional);
	}

	public void addToFeeManagementAccountings(FeeManagementAccounting feeManagementAccounting, int index)
	{
		final TransactionalList<FeeManagementAccounting> transactional = TransactionalCollections.begin(this.feeManagementAccountings);
		NaturalKeyValidator.isAddedItemValid(transactional, feeManagementAccounting);
		transactional.add(index, feeManagementAccounting);
		onFeeManagementAccountingsCollectionChange(transactional);
	}

	public void addManyToFeeManagementAccountings(Collection<FeeManagementAccounting> feeManagementAccountings)
	{
		final TransactionalList<FeeManagementAccounting> transactional = TransactionalCollections.begin(this.feeManagementAccountings);

		for (FeeManagementAccounting feeManagementAccounting : feeManagementAccountings)
		{
			NaturalKeyValidator.isAddedItemValid(transactional, feeManagementAccounting);
		}

		transactional.addAll(feeManagementAccountings);
		onFeeManagementAccountingsCollectionChange(transactional);
	}

	public void removeFromFeeManagementAccountings(FeeManagementAccounting feeManagementAccounting)
	{
		final TransactionalList<FeeManagementAccounting> transactional = TransactionalCollections.begin(this.feeManagementAccountings);
		transactional.remove(feeManagementAccounting);
		onFeeManagementAccountingsCollectionChange(transactional);
	}

	public void removeManyFromFeeManagementAccountings(Collection<FeeManagementAccounting> feeManagementAccountings)
	{
		final TransactionalList<FeeManagementAccounting> transactional = TransactionalCollections.begin(this.feeManagementAccountings);
		transactional.removeAll(feeManagementAccountings);
		onFeeManagementAccountingsCollectionChange(transactional);
	}

	public void removeAllFromFeeManagementAccountings()
	{
		final TransactionalList<FeeManagementAccounting> transactional = TransactionalCollections.begin(this.feeManagementAccountings);

		transactional.clear();
		onFeeManagementAccountingsCollectionChange(transactional);
	}

	public void replaceFeeManagementAccountings(FeeManagementAccounting oldFeeManagementAccounting, FeeManagementAccounting newFeeManagementAccounting)
	{
		final TransactionalList<FeeManagementAccounting> transactional = TransactionalCollections.begin(this.feeManagementAccountings);
		final int indexOf = transactional.indexOf(oldFeeManagementAccounting);

		if (indexOf != -1)
		{
			NaturalKeyValidator.isAddedItemValid(transactional, newFeeManagementAccounting);
			transactional.set(indexOf, newFeeManagementAccounting);
		}
		onFeeManagementAccountingsCollectionChange(transactional);
	}

	public FeeManagementAccounting setToFeeManagementAccountings(int index, FeeManagementAccounting feeManagementAccounting)
	{
		final TransactionalList<FeeManagementAccounting> transactional = TransactionalCollections.begin(this.feeManagementAccountings);
		NaturalKeyValidator.isAddedItemValid(transactional, feeManagementAccounting);

		FeeManagementAccounting removed = transactional.set(index, feeManagementAccounting);

		onFeeManagementAccountingsCollectionChange(transactional);
		return removed;
	}

	public static final String PROPERTYNAME_FEE = "fee";
	public static final String PROPERTYNAME_FEEMANAGEMENTACCOUNTINGS = "feeManagementAccountings";
}
