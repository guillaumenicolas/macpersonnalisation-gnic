package com.mac.custom.bo.base;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.hibernate.Hibernate;
import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.Cascade;

import com.netappsid.bo.EntityStatus;
import com.netappsid.erp.server.bo.Address;
import com.netappsid.erp.server.bo.PurchaseOrder;

@MappedSuperclass
public abstract class BaseMacPurchaseOrder extends PurchaseOrder
{
	private Address shippingAddress;

	@Override
	public void setChildEntityStatus(EntityStatus entityStatus)
	{
		super.setChildEntityStatus(entityStatus);

		if (getShippingAddress() != null && Hibernate.isInitialized(getShippingAddress()))
		{
			getShippingAddress().setEntityStatus(entityStatus);
		}
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = { CascadeType.MERGE })
	@Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
	@AccessType(value = "field")
	public Address getShippingAddress()
	{
		return shippingAddress;
	}

	public void setShippingAddress(Address shippingAddress)
	{
		Address oldValue = this.shippingAddress;
		this.shippingAddress = shippingAddress;
		firePropertyChange(PROPERTYNAME_SHIPPINGADDRESS, oldValue, this.shippingAddress);
	}

	public static final String PROPERTYNAME_SHIPPINGADDRESS = "shippingAddress";
}
