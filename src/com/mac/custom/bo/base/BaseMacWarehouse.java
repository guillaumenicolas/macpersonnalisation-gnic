package com.mac.custom.bo.base;

import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;

import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CollectionTypeInfo;
import org.hibernate.annotations.IndexColumn;

import com.mac.custom.bo.MacShippingLocation;
import com.mac.custom.bo.MacWarehouse;
import com.netappsid.erp.server.bo.Warehouse;
import com.netappsid.framework.utils.NaturalKeyValidator;
import com.netappsid.observable.ObservableCollections;
import com.netappsid.transactional.TransactionalCollections;
import com.netappsid.transactional.TransactionalList;

@MappedSuperclass
public abstract class BaseMacWarehouse extends Warehouse
{
	private final List<MacShippingLocation> shippingLocations = ObservableCollections.newObservableArrayList();

	@JoinColumn(name = "warehouse_id")
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
	@IndexColumn(name = "sequence")
	@CollectionTypeInfo(name = "com.netappsid.hibernate.LazyLoadListType")
	@AccessType(value = "field")
	public List<MacShippingLocation> getShippingLocations()
	{
		return shippingLocations;
	}

	protected void onShippingLocationsCollectionChange(TransactionalList<MacShippingLocation> shippingLocations)
	{
		TransactionalCollections.end(shippingLocations);
	}

	public void addToShippingLocations(MacShippingLocation shippingLocation)
	{
		final TransactionalList<MacShippingLocation> transactional = TransactionalCollections.begin(this.shippingLocations);
		shippingLocation.setWarehouse((MacWarehouse) this);
		NaturalKeyValidator.isAddedItemValid(transactional, shippingLocation);
		transactional.add(shippingLocation);
		onShippingLocationsCollectionChange(transactional);
	}

	public void addToShippingLocations(MacShippingLocation shippingLocation, int index)
	{
		final TransactionalList<MacShippingLocation> transactional = TransactionalCollections.begin(this.shippingLocations);
		shippingLocation.setWarehouse((MacWarehouse) this);
		NaturalKeyValidator.isAddedItemValid(transactional, shippingLocation);
		transactional.add(index, shippingLocation);
		onShippingLocationsCollectionChange(transactional);
	}

	public void addManyToShippingLocations(Collection<MacShippingLocation> shippingLocations)
	{
		final TransactionalList<MacShippingLocation> transactional = TransactionalCollections.begin(this.shippingLocations);
		for (MacShippingLocation shippingLocation : shippingLocations)
		{
			shippingLocation.setWarehouse((MacWarehouse) this);
		}
		for (MacShippingLocation shippingLocation : shippingLocations)
		{
			NaturalKeyValidator.isAddedItemValid(transactional, shippingLocation);
		}

		transactional.addAll(shippingLocations);
		onShippingLocationsCollectionChange(transactional);
	}

	public void removeFromShippingLocations(MacShippingLocation shippingLocation)
	{
		final TransactionalList<MacShippingLocation> transactional = TransactionalCollections.begin(this.shippingLocations);
		if (transactional.remove(shippingLocation))
		{
			shippingLocation.setWarehouse(null);
		}
		onShippingLocationsCollectionChange(transactional);
	}

	public void removeManyFromShippingLocations(Collection<MacShippingLocation> shippingLocations)
	{
		final TransactionalList<MacShippingLocation> transactional = TransactionalCollections.begin(this.shippingLocations);
		for (MacShippingLocation shippingLocation : shippingLocations)
		{
			if (transactional.contains(shippingLocation))
			{
				shippingLocation.setWarehouse(null);
			}
		}

		transactional.removeAll(shippingLocations);
		onShippingLocationsCollectionChange(transactional);
	}

	public void removeAllFromShippingLocations()
	{
		final TransactionalList<MacShippingLocation> transactional = TransactionalCollections.begin(this.shippingLocations);
		for (MacShippingLocation shippingLocation : shippingLocations)
		{
			shippingLocation.setWarehouse(null);
		}

		transactional.clear();
		onShippingLocationsCollectionChange(transactional);
	}

	public void replaceShippingLocations(MacShippingLocation oldShippingLocation, MacShippingLocation newShippingLocation)
	{
		final TransactionalList<MacShippingLocation> transactional = TransactionalCollections.begin(this.shippingLocations);
		final int indexOf = transactional.indexOf(oldShippingLocation);

		if (indexOf != -1)
		{
			newShippingLocation.setWarehouse((MacWarehouse) this);
			NaturalKeyValidator.isAddedItemValid(transactional, newShippingLocation);
			transactional.set(indexOf, newShippingLocation);
			oldShippingLocation.setWarehouse(null);
		}
		onShippingLocationsCollectionChange(transactional);
	}

	public MacShippingLocation setToShippingLocations(int index, MacShippingLocation shippingLocation)
	{
		final TransactionalList<MacShippingLocation> transactional = TransactionalCollections.begin(this.shippingLocations);
		shippingLocation.setWarehouse((MacWarehouse) this);
		NaturalKeyValidator.isAddedItemValid(transactional, shippingLocation);

		MacShippingLocation removed = transactional.set(index, shippingLocation);

		if (removed != null)
		{
			removed.setWarehouse(null);
		}

		onShippingLocationsCollectionChange(transactional);
		return removed;
	}

	public static final String PROPERTYNAME_LOCATIONS = "shippingLocations";
}
