package com.mac.custom.bo.base;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MapKey;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.Type;

import com.mac.custom.bo.FeeCodeAnalytic;
import com.mac.custom.bo.FeeCodeAnalyticDescription;
import com.netappsid.annotations.Localized;
import com.netappsid.bo.Entity;
import com.netappsid.erp.server.bo.Company;

@MappedSuperclass
public abstract class BaseFeeCodeAnalytic extends Entity
{
	private Serializable id;
	private String code;
	private String upperCode;
	private Map<String, FeeCodeAnalyticDescription> description = new HashMap<String, FeeCodeAnalyticDescription>();
	private Company company;

	public BaseFeeCodeAnalytic()
	{
		super();
	}

	@Override
	@Id
	@GeneratedValue(generator = "naid-uuid")
	@GenericGenerator(name = "naid-uuid", strategy = "com.netappsid.framework.utils.UUIDGenerator")
	@Type(type = "com.netappsid.datatypes.UUIDUserType")
	@AccessType(value = "field")
	public Serializable getId()
	{
		return id;
	}

	@Override
	public void setId(Serializable id)
	{
		this.id = id;
	}

	@OneToMany(mappedBy = "feeCodeAnalytic", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@MapKey(name = "languageCode")
	@Localized
	@org.hibernate.annotations.Fetch(org.hibernate.annotations.FetchMode.SELECT)
	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "standard")
	@Index(name = "feeCodeAnalytic_description_x")
	@com.netappsid.annotations.Length(max = 255)
	@AccessType(value = "field")
	public Map<String, FeeCodeAnalyticDescription> getDescription()
	{
		return description;
	}

	public void setDescription(Map<String, FeeCodeAnalyticDescription> description)
	{
		Map<String, FeeCodeAnalyticDescription> oldValue = getDescription();
		this.description = description;
		firePropertyChange(PROPERTYNAME_DESCRIPTION, oldValue, description);
	}

	@Transient
	public String getLocalizedDescription()
	{
		return getDescription(Locale.getDefault().getLanguage());
	}

	public String getDescription(String languageCode)
	{
		if (description.containsKey(languageCode) == true)
		{
			return description.get(languageCode).getValue();
		}
		else
		{
			return null;
		}
	}

	public void setDescription(String languageCode, String value)
	{
		if (description.containsKey(languageCode) == true)
		{
			description.get(languageCode).setValue(value);
		}
		else
		{
			FeeCodeAnalyticDescription temp = new FeeCodeAnalyticDescription(languageCode, value);
			temp.setFeeCodeAnalytic((FeeCodeAnalytic) this);
			description.put(languageCode, temp);
		}
		firePropertyChange(PROPERTYNAME_DESCRIPTION, null, description);
	}

	public void setCode(String code)
	{
		String oldValue = getCode();
		String newValue = code;
		if (newValue != null && newValue.length() == 0)
		{
			newValue = null;
		}
		this.code = newValue;
		if (newValue != null)
		{
			setUpperCode(newValue.toUpperCase());
		}
		else
		{
			setUpperCode(null);
		}
		firePropertyChange(PROPERTYNAME_CODE, oldValue, newValue);
	}

	@Column(name = "code")
	@org.hibernate.validator.NotNull
	@com.netappsid.annotations.NotNull
	@org.hibernate.validator.Length(max = 25)
	@com.netappsid.annotations.Length(max = 25)
	@AccessType(value = "field")
	public String getCode()
	{
		return code;
	}

	public void setUpperCode(String upperCode)
	{
		String oldValue = getUpperCode();
		String newValue = upperCode;
		if (newValue != null && newValue.length() == 0)
		{
			newValue = null;
		}
		this.upperCode = newValue;
		firePropertyChange(PROPERTYNAME_UPPERCODE, oldValue, newValue);
	}

	@Column(name = "upperCode")
	@org.hibernate.validator.NotNull
	@org.hibernate.validator.Length(max = 25)
	@com.netappsid.annotations.Length(max = 25)
	@AccessType(value = "field")
	public String getUpperCode()
	{
		return upperCode;
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public Company getCompany()
	{
		return company;
	}

	public void setCompany(Company company)
	{
		Company oldValue = this.company;
		this.company = company;

		firePropertyChange(PROPERTYNAME_COMPANY, oldValue, this.company);
	}

	public static final String PROPERTYNAME_CODE = "code";
	public static final String PROPERTYNAME_UPPERCODE = "upperCode";
	public static final String PROPERTYNAME_COMPANY = "company";
	public static final String PROPERTYNAME_DESCRIPTION = "description";
}