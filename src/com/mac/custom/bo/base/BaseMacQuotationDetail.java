package com.mac.custom.bo.base;

import com.netappsid.europe.naid.custom.bo.QuotationDetailEuro;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class BaseMacQuotationDetail extends QuotationDetailEuro
{
}