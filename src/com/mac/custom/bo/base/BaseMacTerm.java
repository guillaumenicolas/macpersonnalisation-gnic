package com.mac.custom.bo.base;

import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.AccessType;

import com.netappsid.erp.server.bo.PaymentMode;
import com.netappsid.europe.naid.custom.bo.TermEuro;

@MappedSuperclass
public abstract class BaseMacTerm extends TermEuro
{
	private PaymentMode paymentMode;

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public PaymentMode getPaymentMode()
	{
		return paymentMode;
	}

	public void setPaymentMode(PaymentMode paymentMode)
	{
		PaymentMode oldValue = this.paymentMode;
		this.paymentMode = paymentMode;

		firePropertyChange(PROPERTYNAME_PAYMENTMODE, oldValue, this.paymentMode);
	}

	public static final String PROPERTYNAME_PAYMENTMODE = "paymentMode";
}