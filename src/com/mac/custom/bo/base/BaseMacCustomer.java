package com.mac.custom.bo.base;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.Type;

import com.mac.custom.bo.ActiviteClient;
import com.mac.custom.bo.CustomerLedgerEntry;
import com.mac.custom.bo.EnseigneClient;
import com.mac.custom.bo.MacRegionCommerciale;
import com.mac.custom.enums.TypeClientEnum;
import com.netappsid.annotations.Scale;
import com.netappsid.datatypes.MonetaryAmount;
import com.netappsid.erp.server.bo.Customer;
import com.netappsid.erp.server.bo.User;
import com.netappsid.europe.naid.custom.bo.CustomerEuro;
import com.netappsid.framework.utils.NaturalKeyValidator;
import com.netappsid.framework.utils.ObservableCollectionUtils;
import com.netappsid.observable.ObservableCollections;
import com.netappsid.transactional.TransactionalCollections;
import com.netappsid.transactional.TransactionalList;

@MappedSuperclass
public class BaseMacCustomer extends CustomerEuro
{
	private MacRegionCommerciale macRegionCommerciale;
	private String codeProspect;
	private TypeClientEnum typeClient;
	private String categorieCA;
	private MonetaryAmount objectifCA;
	private User analysteCredit;
	private String ancienneCotation;
	private String nouvelleCotation;
	private Date miseAJourCotation;
	private String codeSolvabilite;
	private Customer enCoursA;
	private String codeNAF;

	private ActiviteClient activiteClient;
	private EnseigneClient enseigneClient;

	private MonetaryAmount montantEcheanceDepassee;
	private MonetaryAmount montantEncours30Jours;
	private MonetaryAmount montantEncours60Jours;
	private MonetaryAmount montantEncours90Jours;
	private MonetaryAmount montantEncoursSuperieur90Jours;
	private Date encoursDateTransfert;

	private final List<CustomerLedgerEntry> entries = ObservableCollections.newObservableArrayList();

	@Transient
	public List<CustomerLedgerEntry> getEntries()
	{
		return entries;
	}

	public void setEntries(List<CustomerLedgerEntry> entries)
	{
		Boolean wasDirty = isDirty();
		ObservableCollectionUtils.clearAndAddAll(this.entries, entries);
		setDirty(wasDirty);
	}

	protected void onEntriesCollectionChange(TransactionalList<CustomerLedgerEntry> entries)
	{
		TransactionalCollections.end(entries);
	}

	public void addToEntries(CustomerLedgerEntry CustLedgerEntry)
	{
		final TransactionalList<CustomerLedgerEntry> transactional = TransactionalCollections.begin(this.entries);
		NaturalKeyValidator.isAddedItemValid(transactional, CustLedgerEntry);
		transactional.add(CustLedgerEntry);
		onEntriesCollectionChange(transactional);
	}

	public void addToEntries(CustomerLedgerEntry CustLedgerEntry, int index)
	{
		final TransactionalList<CustomerLedgerEntry> transactional = TransactionalCollections.begin(this.entries);
		NaturalKeyValidator.isAddedItemValid(transactional, CustLedgerEntry);
		transactional.add(index, CustLedgerEntry);
		onEntriesCollectionChange(transactional);
	}

	public void addManyToEntries(Collection<CustomerLedgerEntry> entries)
	{
		final TransactionalList<CustomerLedgerEntry> transactional = TransactionalCollections.begin(this.entries);

		for (CustomerLedgerEntry CustLedgerEntry : entries)
		{
			NaturalKeyValidator.isAddedItemValid(transactional, CustLedgerEntry);
		}

		transactional.addAll(entries);
		onEntriesCollectionChange(transactional);
	}

	public void removeFromEntries(CustomerLedgerEntry CustLedgerEntry)
	{
		final TransactionalList<CustomerLedgerEntry> transactional = TransactionalCollections.begin(this.entries);
		transactional.remove(CustLedgerEntry);
		onEntriesCollectionChange(transactional);
	}

	public void removeManyFromEntries(Collection<CustomerLedgerEntry> entries)
	{
		final TransactionalList<CustomerLedgerEntry> transactional = TransactionalCollections.begin(this.entries);
		transactional.removeAll(entries);
		onEntriesCollectionChange(transactional);
	}

	public void removeAllFromEntries()
	{
		final TransactionalList<CustomerLedgerEntry> transactional = TransactionalCollections.begin(this.entries);

		transactional.clear();
		onEntriesCollectionChange(transactional);
	}

	public void replaceEntries(CustomerLedgerEntry oldCustLedgerEntry, CustomerLedgerEntry newCustLedgerEntry)
	{
		final TransactionalList<CustomerLedgerEntry> transactional = TransactionalCollections.begin(this.entries);
		final int indexOf = transactional.indexOf(oldCustLedgerEntry);

		if (indexOf != -1)
		{
			NaturalKeyValidator.isAddedItemValid(transactional, newCustLedgerEntry);
			transactional.set(indexOf, newCustLedgerEntry);
		}
		onEntriesCollectionChange(transactional);
	}

	public CustomerLedgerEntry setToEntries(int index, CustomerLedgerEntry CustLedgerEntry)
	{
		final TransactionalList<CustomerLedgerEntry> transactional = TransactionalCollections.begin(this.entries);
		NaturalKeyValidator.isAddedItemValid(transactional, CustLedgerEntry);

		CustomerLedgerEntry removed = transactional.set(index, CustLedgerEntry);

		onEntriesCollectionChange(transactional);
		return removed;
	}

	public void setMontantEcheanceDepassee(MonetaryAmount montantEcheanceDepassee)
	{
		MonetaryAmount oldValue = getMontantEcheanceDepassee();
		this.montantEcheanceDepassee = montantEcheanceDepassee;
		if (this.montantEcheanceDepassee != null)
		{
			this.montantEcheanceDepassee.setScale(2);
		}
		firePropertyChange(PROPERTYNAME_MONTANTECHEANCEDEPASSEE, oldValue, montantEcheanceDepassee);
	}

	@Type(type = "com.netappsid.datatypes.MonetaryAmountUserType")
	@Columns(columns = { @Column(name = "MNT_ECHU", precision = 32, scale = 2), @Column(name = "MNT_ECHU_CUR"), @Column(name = "MNT_ECHU_TR_CUR"),
			@Column(name = "MNT_ECHU_EX_RATE", precision = 19, scale = 10) })
	@Scale(decimal = 2)
	@AccessType(value = "field")
	public MonetaryAmount getMontantEcheanceDepassee()
	{
		return montantEcheanceDepassee;
	}

	public void setMontantEncours30Jours(MonetaryAmount montantEncours30Jours)
	{
		MonetaryAmount oldValue = getMontantEncours30Jours();
		this.montantEncours30Jours = montantEncours30Jours;
		if (this.montantEncours30Jours != null)
		{
			this.montantEncours30Jours.setScale(2);
		}
		firePropertyChange(PROPERTYNAME_MONTANTENCOURS30JOURS, oldValue, montantEncours30Jours);
	}

	@Type(type = "com.netappsid.datatypes.MonetaryAmountUserType")
	@Columns(columns = { @Column(name = "MNT_ENCOUR_30J", precision = 32, scale = 2), @Column(name = "MNT_ENCOUR_30J_CUR"),
			@Column(name = "MNT_ENCOUR_30J_TR_CUR"), @Column(name = "MNT_ENCOUR_30J_EX_RATE", precision = 19, scale = 10) })
	@Scale(decimal = 2)
	@AccessType(value = "field")
	public MonetaryAmount getMontantEncours30Jours()
	{
		return montantEncours30Jours;
	}

	public void setMontantEncours60Jours(MonetaryAmount montantEncours60Jours)
	{
		MonetaryAmount oldValue = getMontantEncours60Jours();
		this.montantEncours60Jours = montantEncours60Jours;
		if (this.montantEncours60Jours != null)
		{
			this.montantEncours60Jours.setScale(2);
		}
		firePropertyChange(PROPERTYNAME_MONTANTENCOURS60JOURS, oldValue, montantEncours60Jours);
	}

	@Type(type = "com.netappsid.datatypes.MonetaryAmountUserType")
	@Columns(columns = { @Column(name = "MNT_ENCOUR_60J", precision = 32, scale = 2), @Column(name = "MNT_ENCOUR_60J_CUR"),
			@Column(name = "MNT_ENCOUR_60J_TR_CUR"), @Column(name = "MNT_ENCOUR_60J_EX_RATE", precision = 19, scale = 10) })
	@Scale(decimal = 2)
	@AccessType(value = "field")
	public MonetaryAmount getMontantEncours60Jours()
	{
		return montantEncours60Jours;
	}

	public void setMontantEncours90Jours(MonetaryAmount montantEncours90Jours)
	{
		MonetaryAmount oldValue = getMontantEncours90Jours();
		this.montantEncours90Jours = montantEncours90Jours;
		if (this.montantEncours90Jours != null)
		{
			this.montantEncours90Jours.setScale(2);
		}
		firePropertyChange(PROPERTYNAME_MONTANTENCOURS90JOURS, oldValue, montantEncours90Jours);
	}

	@Type(type = "com.netappsid.datatypes.MonetaryAmountUserType")
	@Columns(columns = { @Column(name = "MNT_ENCOUR_90J", precision = 32, scale = 2), @Column(name = "MNT_ENCOUR_90J_CUR"),
			@Column(name = "MNT_ENCOUR_90J_TR_CUR"), @Column(name = "MNT_ENCOUR_90J_EX_RATE", precision = 19, scale = 10) })
	@Scale(decimal = 2)
	@AccessType(value = "field")
	public MonetaryAmount getMontantEncours90Jours()
	{
		return montantEncours90Jours;
	}

	public void setMontantEncoursSuperieur90Jours(MonetaryAmount montantEncoursSuperieur90Jours)
	{
		MonetaryAmount oldValue = getMontantEncoursSuperieur90Jours();
		this.montantEncoursSuperieur90Jours = montantEncoursSuperieur90Jours;
		if (this.montantEncoursSuperieur90Jours != null)
		{
			this.montantEncoursSuperieur90Jours.setScale(2);
		}
		firePropertyChange(PROPERTYNAME_MONTANTENCOURSSUPERIEUR90JOURS, oldValue, montantEncoursSuperieur90Jours);
	}

	@Type(type = "com.netappsid.datatypes.MonetaryAmountUserType")
	@Columns(columns = { @Column(name = "MNT_ENCOUR_SUP_90J", precision = 32, scale = 2), @Column(name = "MNT_ENCOUR_SUP_90J_CUR"),
			@Column(name = "MNT_ENCOUR_SUP_90J_TR_CUR"), @Column(name = "MNT_ENCOUR_SUP_90J_EX_RATE", precision = 19, scale = 10) })
	@Scale(decimal = 2)
	@AccessType(value = "field")
	public MonetaryAmount getMontantEncoursSuperieur90Jours()
	{
		return montantEncoursSuperieur90Jours;
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public MacRegionCommerciale getMacRegionCommerciale()
	{
		return macRegionCommerciale;
	}

	public void setMacRegionCommerciale(MacRegionCommerciale macRegionCommerciale)
	{
		MacRegionCommerciale oldValue = this.macRegionCommerciale;
		this.macRegionCommerciale = macRegionCommerciale;

		firePropertyChange(PROPERTYNAME_MACREGIONCOMMERCIALE, oldValue, this.macRegionCommerciale);
	}

	@AccessType(value = "field")
	@org.hibernate.validator.Length(max = 255)
	public String getCodeProspect()
	{
		return codeProspect;
	}

	public void setCodeProspect(String codeProspect)
	{
		String oldValue = getCodeProspect();

		this.codeProspect = codeProspect;
		firePropertyChange(PROPERTYNAME_MACCODEPROSPECT, oldValue, codeProspect);
	}

	@Column(name = "typeClient")
	@Enumerated(EnumType.STRING)
	@AccessType(value = "field")
	@org.hibernate.validator.NotNull
	public TypeClientEnum getTypeClient()
	{
		return typeClient;
	}

	public void setTypeClient(TypeClientEnum typeClient)
	{
		TypeClientEnum oldValue = this.typeClient;
		this.typeClient = typeClient;

		firePropertyChange(PROPERTYNAME_TYPECLIENT, oldValue, typeClient);
	}

	@Column(name = "ENCOUR_DATE_TRANSFER")
	@AccessType(value = "field")
	public Date getEncoursDateTransfert()
	{
		return encoursDateTransfert;
	}

	public void setEncoursDateTransfert(Date encoursDateTransfert)
	{
		Date oldValue = getEncoursDateTransfert();
		this.encoursDateTransfert = encoursDateTransfert;

		firePropertyChange(PROPERTYNAME_ENCOURSDATETRANSFERT, oldValue, encoursDateTransfert);
	}

	@AccessType(value = "field")
	@org.hibernate.validator.Length(max = 255)
	public String getCategorieCA()
	{
		return categorieCA;
	}

	public void setCategorieCA(String categorieCA)
	{
		String oldValue = getCategorieCA();

		this.categorieCA = categorieCA;
		firePropertyChange(PROPERTYNAME_CATEGORIECA, oldValue, categorieCA);
	}

	public void setObjectifCA(MonetaryAmount objectifCA)
	{
		MonetaryAmount oldValue = getObjectifCA();
		this.objectifCA = objectifCA;
		if (this.objectifCA != null)
		{
			this.objectifCA.setScale(2);
		}
		firePropertyChange(PROPERTYNAME_OBJECTIFCA, oldValue, objectifCA);
	}

	@Type(type = "com.netappsid.datatypes.MonetaryAmountUserType")
	@Columns(columns = { @Column(name = "objectifCA_amount", precision = 32, scale = 2), @Column(name = "objectifCA_currency"),
			@Column(name = "objectifCA_transactionCurrency"), @Column(name = "objectifCA_exchangeRate", precision = 19, scale = 10) })
	@Scale(decimal = 2)
	@AccessType(value = "field")
	public MonetaryAmount getObjectifCA()
	{
		return objectifCA;
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public ActiviteClient getActiviteClient()
	{
		return activiteClient;
	}

	public void setActiviteClient(ActiviteClient activiteClient)
	{
		ActiviteClient oldValue = this.activiteClient;
		this.activiteClient = activiteClient;

		firePropertyChange(PROPERTYNAME_ACTIVITECLIENT, oldValue, this.activiteClient);
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public EnseigneClient getEnseigneClient()
	{
		return enseigneClient;
	}

	public void setEnseigneClient(EnseigneClient enseigneClient)
	{
		EnseigneClient oldValue = this.enseigneClient;
		this.enseigneClient = enseigneClient;

		firePropertyChange(PROPERTYNAME_ENSEIGNECLIENT, oldValue, this.enseigneClient);
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public User getAnalysteCredit()
	{
		return analysteCredit;
	}

	public void setAnalysteCredit(User analysteCredit)
	{
		User oldValue = getAnalysteCredit();
		this.analysteCredit = analysteCredit;
		firePropertyChange(PROPERTYNAME_ANALYSTECREDIT, oldValue, analysteCredit);
	}

	@AccessType(value = "field")
	@org.hibernate.validator.Length(max = 255)
	public String getAncienneCotation()
	{
		return ancienneCotation;
	}

	public void setAncienneCotation(String ancienneCotation)
	{
		String oldValue = getAncienneCotation();

		this.ancienneCotation = ancienneCotation;
		firePropertyChange(PROPERTYNAME_ANCIENNECOTATION, oldValue, ancienneCotation);
	}

	@AccessType(value = "field")
	@org.hibernate.validator.Length(max = 255)
	public String getNouvelleCotation()
	{
		return nouvelleCotation;
	}

	public void setNouvelleCotation(String nouvelleCotation)
	{
		String oldValue = getNouvelleCotation();

		this.nouvelleCotation = nouvelleCotation;
		firePropertyChange(PROPERTYNAME_NOUVELLECOTATION, oldValue, nouvelleCotation);
	}

	@Column(name = "miseAJourCotation")
	@Temporal(TemporalType.DATE)
	@AccessType(value = "field")
	public Date getMiseAJourCotation()
	{
		return miseAJourCotation;
	}

	public void setMiseAJourCotation(Date miseAJourCotation)
	{
		Date oldValue = getMiseAJourCotation();

		this.miseAJourCotation = miseAJourCotation;
		firePropertyChange(PROPERTYNAME_MISEAJOURCOTATION, oldValue, miseAJourCotation);
	}

	@AccessType(value = "field")
	@org.hibernate.validator.Length(max = 255)
	public String getCodeSolvabilite()
	{
		return codeSolvabilite;
	}

	public void setCodeSolvabilite(String codeSolvabilite)
	{
		String oldValue = getCodeSolvabilite();

		this.codeSolvabilite = codeSolvabilite;
		firePropertyChange(PROPERTYNAME_CODESOLVABILITE, oldValue, codeSolvabilite);
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public Customer getEnCoursA()
	{
		return enCoursA;
	}

	public void setEnCoursA(Customer enCoursA)
	{
		Customer oldValue = getEnCoursA();

		this.enCoursA = enCoursA;
		firePropertyChange(PROPERTYNAME_ENCOURSA, oldValue, enCoursA);
	}

	@AccessType(value = "field")
	@org.hibernate.validator.Length(max = 255)
	public String getCodeNAF()
	{
		return codeNAF;
	}

	public void setCodeNAF(String codeNAF)
	{
		String oldValue = getCodeNAF();

		this.codeNAF = codeNAF;
		firePropertyChange(PROPERTYNAME_CODENAF, oldValue, codeNAF);
	}

	public static final String PROPERTYNAME_MACREGIONCOMMERCIALE = "macRegionCommerciale";
	public static final String PROPERTYNAME_ACTIVITECLIENT = "activiteClient";
	public static final String PROPERTYNAME_ENSEIGNECLIENT = "enseigneClient";
	public static final String PROPERTYNAME_MACCODEPROSPECT = "codeProspect";
	public static final String PROPERTYNAME_TYPECLIENT = "typeClient";
	public static final String PROPERTYNAME_CATEGORIECA = "categorieCA";
	public static final String PROPERTYNAME_OBJECTIFCA = "objectifCA";
	public static final String PROPERTYNAME_ANALYSTECREDIT = "analysteCredit";
	public static final String PROPERTYNAME_ANCIENNECOTATION = "ancienneCotation";
	public static final String PROPERTYNAME_NOUVELLECOTATION = "nouvelleCotation";
	public static final String PROPERTYNAME_MISEAJOURCOTATION = "miseAJourCotation";
	public static final String PROPERTYNAME_CODESOLVABILITE = "codeSolvabilite";
	public static final String PROPERTYNAME_ENCOURSA = "enCoursA";
	public static final String PROPERTYNAME_CODENAF = "codeNAF";
	public static final String PROPERTYNAME_MONTANTECHEANCEDEPASSEE = "montantEcheanceDepassee";
	public static final String PROPERTYNAME_MONTANTENCOURS30JOURS = "montantEncours30Jours";
	public static final String PROPERTYNAME_MONTANTENCOURS60JOURS = "montantEncours60Jours";
	public static final String PROPERTYNAME_MONTANTENCOURS90JOURS = "montantEncours90Jours";
	public static final String PROPERTYNAME_MONTANTENCOURSSUPERIEUR90JOURS = "montantEncoursSuperieur90Jours";
	public static final String PROPERTYNAME_ENCOURSDATETRANSFERT = "encoursDateTransfert";
}