package com.mac.custom.bo.base;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import org.hibernate.annotations.AccessType;

import com.mac.custom.bo.CostCode;
import com.mac.custom.bo.ProductCodeAnalytic;
import com.netappsid.erp.server.bo.AccountingSegment;
import com.netappsid.erp.server.bo.ReceivingDetail;

@MappedSuperclass
public abstract class BaseMacReceivingDetail extends ReceivingDetail
{
}