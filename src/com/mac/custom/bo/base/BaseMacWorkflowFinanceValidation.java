package com.mac.custom.bo.base;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.netappsid.bo.Entity;
import com.netappsid.erp.server.bo.DocumentStatus;

@MappedSuperclass
public abstract class BaseMacWorkflowFinanceValidation extends Entity
{
	private Serializable id;
	
	private String emailOrigin;
	private String emailCompta;
	
	private boolean active;
	
	@Id
	@GeneratedValue(generator = "naid-uuid")
	@GenericGenerator(name = "naid-uuid", strategy = "com.netappsid.framework.utils.UUIDGenerator")
	@Type(type = "com.netappsid.datatypes.UUIDUserType")
	@AccessType(value = "field")
	public Serializable getId()
	{
		return id;
	}

	public void setId(Serializable id)
	{
		this.id = id;
	}

	public void setEmailOrigin(String emailOrigin)
	{
		String oldValue = getEmailOrigin();
		this.emailOrigin = emailOrigin;
		firePropertyChange(PROPERTYNAME_EMAILORIGIN, oldValue, emailOrigin);
	}

	@Column(name = "emailOrigin")
	@com.netappsid.annotations.NotNull
	@org.hibernate.validator.NotNull
	@AccessType(value = "field")
	@org.hibernate.validator.Length(max = 255)
	@com.netappsid.annotations.Length(max = 255)
	public String getEmailOrigin()
	{
		return emailOrigin;
	}
	
	public void setEmailCompta(String emailCompta)
	{
		String oldValue = getEmailCompta();
		this.emailCompta = emailCompta;
		firePropertyChange(PROPERTYNAME_EMAILCOMPTA, oldValue, emailCompta);
	}

	@Column(name = "emailCompta")
	@com.netappsid.annotations.NotNull
	@org.hibernate.validator.NotNull
	@AccessType(value = "field")
	@org.hibernate.validator.Length(max = 255)
	@com.netappsid.annotations.Length(max = 255)
	public String getEmailCompta()
	{
		return emailCompta;
	}
	
	public void setActive(boolean active)
	{
		boolean oldValue = isActive();
		this.active = active;
		firePropertyChange(PROPERTYNAME_ACTIVE, oldValue, active);
	}

	@Column(name = "active")//$NON-NLS-1$ 
	@AccessType(value = "field")//$NON-NLS-1$
	public boolean isActive()
	{
		return active;
	}
	
	public static final String PROPERTYNAME_EMAILORIGIN = "emailOrigin";
	public static final String PROPERTYNAME_EMAILCOMPTA = "emailCompta";
	public static final String PROPERTYNAME_ACTIVE = "active";
}
