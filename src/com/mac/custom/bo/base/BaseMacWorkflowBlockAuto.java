package com.mac.custom.bo.base;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.netappsid.bo.Entity;
import com.netappsid.erp.server.bo.BlockingReason;
import com.netappsid.erp.server.bo.DocumentStatus;

@MappedSuperclass
public abstract class BaseMacWorkflowBlockAuto extends Entity
{
	private Serializable id;

	private BlockingReason blockingReason;
	private BlockingReason blockingReasonProforma;
	private DocumentStatus blockedStatus;
	private DocumentStatus initialStatus;

	private String emailOrigin;

	private boolean active;

	@Override
	@Id
	@GeneratedValue(generator = "naid-uuid")
	@GenericGenerator(name = "naid-uuid", strategy = "com.netappsid.framework.utils.UUIDGenerator")
	@Type(type = "com.netappsid.datatypes.UUIDUserType")
	@AccessType(value = "field")
	public Serializable getId()
	{
		return id;
	}

	@Override
	public void setId(Serializable id)
	{
		this.id = id;
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@com.netappsid.annotations.NotNull
	@org.hibernate.validator.NotNull
	@AccessType(value = "field")
	public BlockingReason getBlockingReason()
	{
		return blockingReason;
	}

	public void setBlockingReason(BlockingReason blockingReason)
	{
		BlockingReason oldValue = this.blockingReason;
		this.blockingReason = blockingReason;
		firePropertyChange(PROPERTYNAME_BLOCKINGREASON, oldValue, this.blockingReason);
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@com.netappsid.annotations.NotNull
	@org.hibernate.validator.NotNull
	@AccessType(value = "field")
	public BlockingReason getBlockingReasonProforma()
	{
		return blockingReasonProforma;
	}

	public void setBlockingReasonProforma(BlockingReason blockingReasonProforma)
	{
		BlockingReason oldValue = this.blockingReasonProforma;
		this.blockingReasonProforma = blockingReasonProforma;
		firePropertyChange(PROPERTYNAME_BLOCKINGREASONPROFORMA, oldValue, this.blockingReasonProforma);
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@com.netappsid.annotations.NotNull
	@org.hibernate.validator.NotNull
	@AccessType(value = "field")
	public DocumentStatus getBlockedStatus()
	{
		return blockedStatus;
	}

	public void setBlockedStatus(DocumentStatus blockedStatus)
	{
		DocumentStatus oldValue = this.blockedStatus;
		this.blockedStatus = blockedStatus;
		firePropertyChange(PROPERTYNAME_BLOCKEDSTATUS, oldValue, this.blockedStatus);
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@com.netappsid.annotations.NotNull
	@org.hibernate.validator.NotNull
	@AccessType(value = "field")
	public DocumentStatus getInitialStatus()
	{
		return initialStatus;
	}

	public void setInitialStatus(DocumentStatus initialStatus)
	{
		DocumentStatus oldValue = this.blockedStatus;
		this.initialStatus = initialStatus;
		firePropertyChange(PROPERTYNAME_INITIALSTATUS, oldValue, this.initialStatus);
	}

	public void setActive(boolean active)
	{
		boolean oldValue = isActive();
		this.active = active;
		firePropertyChange(PROPERTYNAME_ACTIVE, oldValue, active);
	}

	@Column(name = "active")
	@AccessType(value = "field")
	public boolean isActive()
	{
		return active;
	}

	public void setEmailOrigin(String emailOrigin)
	{
		String oldValue = getEmailOrigin();
		this.emailOrigin = emailOrigin;
		firePropertyChange(PROPERTYNAME_EMAILORIGIN, oldValue, emailOrigin);
	}

	@Column(name = "emailOrigin")
	@com.netappsid.annotations.NotNull
	@org.hibernate.validator.NotNull
	@AccessType(value = "field")
	@org.hibernate.validator.Length(max = 255)
	@com.netappsid.annotations.Length(max = 255)
	public String getEmailOrigin()
	{
		return emailOrigin;
	}

	public static final String PROPERTYNAME_BLOCKINGREASON = "blockingReason";
	public static final String PROPERTYNAME_BLOCKINGREASONPROFORMA = "blockingReasonProforma";
	public static final String PROPERTYNAME_BLOCKEDSTATUS = "blockedStatus";
	public static final String PROPERTYNAME_INITIALSTATUS = "initialStatus";
	public static final String PROPERTYNAME_EMAILORIGIN = "emailOrigin";
	public static final String PROPERTYNAME_ACTIVE = "active";
}
