package com.mac.custom.bo.base;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.AccessType;

import com.mac.custom.bo.MacManufacturedProduct;

@MappedSuperclass
public abstract class BaseManufacturedProductManagementAccounting extends BaseProductManagementAccounting
{
	private MacManufacturedProduct macManufacturedProduct;

	public BaseManufacturedProductManagementAccounting()
	{
		super();
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "macManufacturedProduct_id", insertable = false, updatable = false)//$NON-NLS-1$
	@AccessType(value = "field")//$NON-NLS-1$
	public MacManufacturedProduct getMacManufacturedProduct()
	{
		return macManufacturedProduct;
	}

	public void setMacManufacturedProduct(MacManufacturedProduct manufacturedProduct)
	{
		MacManufacturedProduct oldValue = this.macManufacturedProduct;
		this.macManufacturedProduct = manufacturedProduct;

		firePropertyChange(PROPERTYNAME_MACMANUFACTUREDPRODUCT, oldValue, this.macManufacturedProduct);
	}

	public static final String PROPERTYNAME_MACMANUFACTUREDPRODUCT = "macManufacturedProduct";
}