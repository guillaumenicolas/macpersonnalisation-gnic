package com.mac.custom.bo.base;

import com.mac.custom.bo.MacMaterialProduct;
import com.mac.custom.bo.MaterialProductManagementAccounting;
import com.netappsid.bo.EntityStatus;
import com.netappsid.europe.naid.custom.bo.MaterialProductEuro;
import com.netappsid.framework.utils.NaturalKeyValidator;
import com.netappsid.observable.ObservableCollections;
import com.netappsid.transactional.TransactionalCollections;
import com.netappsid.transactional.TransactionalList;
import java.util.Collection;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import org.hibernate.Hibernate;
import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CollectionTypeInfo;
import org.hibernate.annotations.IndexColumn;

@MappedSuperclass
public class BaseMacMaterialProduct extends MaterialProductEuro
{
	private String ancienCodeArticle;
	private final List<MaterialProductManagementAccounting> managementAccountings = ObservableCollections.newObservableArrayList();

	@Override
	public void setChildEntityStatus(EntityStatus entityStatus)
	{
		super.setChildEntityStatus(entityStatus);

		if (getManagementAccountings() != null && Hibernate.isInitialized(getManagementAccountings()))
		{
			for (MaterialProductManagementAccounting materialProductManagementAccounting : getManagementAccountings())
			{
				materialProductManagementAccounting.setEntityStatus(entityStatus);
			}
		}
	}

	@AccessType(value = "field")
	@org.hibernate.validator.Length(max = 100)
	public String getAncienCodeArticle()
	{
		return ancienCodeArticle;
	}

	public void setAncienCodeArticle(String ancienCodeArticle)
	{
		String oldValue = getAncienCodeArticle();
		this.ancienCodeArticle = ancienCodeArticle;
		firePropertyChange(PROPERTYNAME_ANCIENCODEARTICLE, oldValue, ancienCodeArticle);
	}

	//
	@IndexColumn(name = "sequence")
	@JoinColumn(name = "macMaterialProduct_id")
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
	@CollectionTypeInfo(name = "com.netappsid.hibernate.LazyLoadListType")
	@AccessType(value = "field")//$NON-NLS-1$
	public List<MaterialProductManagementAccounting> getManagementAccountings()
	{
		return managementAccountings;
	}

	protected void onManagementAccountingsCollectionChange(TransactionalList<MaterialProductManagementAccounting> managementAccountings)
	{
		TransactionalCollections.end(managementAccountings);
	}

	public void addToManagementAccountings(MaterialProductManagementAccounting managementAccounting)
	{
		final TransactionalList<MaterialProductManagementAccounting> transactional = TransactionalCollections.begin(this.managementAccountings);
		managementAccounting.setMacMaterialProduct((MacMaterialProduct) this);
		NaturalKeyValidator.isAddedItemValid(transactional, managementAccounting);
		transactional.add(managementAccounting);
		onManagementAccountingsCollectionChange(transactional);
	}

	public void addToManagementAccountings(MaterialProductManagementAccounting managementAccounting, int index)
	{
		final TransactionalList<MaterialProductManagementAccounting> transactional = TransactionalCollections.begin(this.managementAccountings);
		managementAccounting.setMacMaterialProduct((MacMaterialProduct) this);
		NaturalKeyValidator.isAddedItemValid(transactional, managementAccounting);
		transactional.add(index, managementAccounting);
		onManagementAccountingsCollectionChange(transactional);
	}

	public void addManyToManagementAccountings(Collection<MaterialProductManagementAccounting> managementAccountings)
	{
		final TransactionalList<MaterialProductManagementAccounting> transactional = TransactionalCollections.begin(this.managementAccountings);
		for (MaterialProductManagementAccounting managementAccounting : managementAccountings)
		{
			managementAccounting.setMacMaterialProduct((MacMaterialProduct) this);
			NaturalKeyValidator.isAddedItemValid(transactional, managementAccounting);
		}

		transactional.addAll(managementAccountings);
		onManagementAccountingsCollectionChange(transactional);
	}

	public void removeFromManagementAccountings(MaterialProductManagementAccounting managementAccounting)
	{
		final TransactionalList<MaterialProductManagementAccounting> transactional = TransactionalCollections.begin(this.managementAccountings);
		if (transactional.remove(managementAccounting))
		{
			managementAccounting.setMacMaterialProduct(null);
		}
		transactional.remove(managementAccounting);
		onManagementAccountingsCollectionChange(transactional);
	}

	public void removeManyFromManagementAccountings(Collection<MaterialProductManagementAccounting> managementAccountings)
	{
		final TransactionalList<MaterialProductManagementAccounting> transactional = TransactionalCollections.begin(this.managementAccountings);

		for (MaterialProductManagementAccounting managementAccounting : managementAccountings)
		{
			if (transactional.contains(managementAccounting))
			{
				managementAccounting.setMacMaterialProduct(null);
			}
		}
		transactional.removeAll(managementAccountings);
		onManagementAccountingsCollectionChange(transactional);
	}

	public void removeAllFromManagementAccountings()
	{
		final TransactionalList<MaterialProductManagementAccounting> transactional = TransactionalCollections.begin(this.managementAccountings);
		for (MaterialProductManagementAccounting managementAccounting : this.managementAccountings)
		{
			managementAccounting.setMacMaterialProduct(null);
		}
		transactional.clear();
		onManagementAccountingsCollectionChange(transactional);
	}

	public void replaceManagementAccountings(MaterialProductManagementAccounting oldManagementAccounting,
			MaterialProductManagementAccounting newManagementAccounting)
	{
		final TransactionalList<MaterialProductManagementAccounting> transactional = TransactionalCollections.begin(this.managementAccountings);
		final int indexOf = transactional.indexOf(oldManagementAccounting);

		if (indexOf != -1)
		{
			newManagementAccounting.setMacMaterialProduct((MacMaterialProduct) this);
			NaturalKeyValidator.isAddedItemValid(transactional, newManagementAccounting);
			transactional.set(indexOf, newManagementAccounting);
			newManagementAccounting.setMacMaterialProduct(null);
		}
		onManagementAccountingsCollectionChange(transactional);
	}

	public MaterialProductManagementAccounting setToManagementAccountings(int index, MaterialProductManagementAccounting managementAccounting)
	{
		final TransactionalList<MaterialProductManagementAccounting> transactional = TransactionalCollections.begin(this.managementAccountings);
		managementAccounting.setMacMaterialProduct((MacMaterialProduct) this);
		NaturalKeyValidator.isAddedItemValid(transactional, managementAccounting);

		MaterialProductManagementAccounting removed = transactional.set(index, managementAccounting);

		if (removed != null)
		{
			removed.setMacMaterialProduct(null);
		}

		onManagementAccountingsCollectionChange(transactional);
		return removed;
	}

	public static final String PROPERTYNAME_ANCIENCODEARTICLE = "ancienCodeArticle";
}