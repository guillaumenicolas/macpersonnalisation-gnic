package com.mac.custom.bo.base;

import com.mac.custom.bo.WorkLoad;
import com.netappsid.bo.model.Model;
import com.netappsid.erp.server.bo.ResourceEfficiency;

public class BaseWorkCalendarDetail extends Model
{
	private WorkLoad workLoad;
	private ResourceEfficiency resourceEfficiency;

	public WorkLoad getWorkLoad()
	{
		return workLoad;
	}

	public void setWorkLoad(WorkLoad workLoad)
	{
		WorkLoad oldValue = this.workLoad;
		this.workLoad = workLoad;

		firePropertyChange(PROPERTYNAME_WORKLOAD, oldValue, this.workLoad);
	}

	public ResourceEfficiency getResourceEfficiency()
	{
		return resourceEfficiency;
	}

	public void setResourceEfficiency(ResourceEfficiency resourceEfficiency)
	{
		ResourceEfficiency oldValue = this.resourceEfficiency;
		this.resourceEfficiency = resourceEfficiency;

		firePropertyChange(PROPERTYNAME_RESOURCEEFFICIENCY, oldValue, this.resourceEfficiency);
	}

	public static final String PROPERTYNAME_WORKLOAD = "workLoad";
	public static final String PROPERTYNAME_RESOURCEEFFICIENCY = "resourceEfficiency";
}
