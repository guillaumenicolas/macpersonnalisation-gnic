package com.mac.custom.bo.base;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.AccessType;

import com.netappsid.erp.server.bo.Report;

@MappedSuperclass
public abstract class BaseMacReport extends Report
{
	private boolean arc;
	
	public void setArc(boolean arc)
	{
		boolean oldValue = isArc();
		this.arc = arc;
		firePropertyChange(PROPERTYNAME_ARC, oldValue, arc);
	}

	@Column(name = "arc")
	@AccessType(value = "field")
	public boolean isArc()
	{
		return arc;
	}
	
	public static final String PROPERTYNAME_ARC = "arc"; 
}
