package com.mac.custom.bo.base;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.AccessType;

import com.mac.custom.bo.MacMaterialProduct;

@MappedSuperclass
public abstract class BaseMaterialProductManagementAccounting extends BaseProductManagementAccounting
{
	private MacMaterialProduct macMaterialProduct;

	public BaseMaterialProductManagementAccounting()
	{
		super();
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "macMaterialProduct_id", insertable = false, updatable = false)//$NON-NLS-1$
	@AccessType(value = "field")//$NON-NLS-1$
	public MacMaterialProduct getMacMaterialProduct()
	{
		return macMaterialProduct;
	}

	public void setMacMaterialProduct(MacMaterialProduct materialProduct)
	{
		MacMaterialProduct oldValue = this.macMaterialProduct;
		this.macMaterialProduct = materialProduct;
		firePropertyChange(PROPERTYNAME_MACMATERIALPRODUCT, oldValue, this.macMaterialProduct);
	}

	public static final String PROPERTYNAME_MACMATERIALPRODUCT = "macMaterialProduct";
}