package com.mac.custom.bo.base;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MapKey;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;

import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.Type;

import com.mac.custom.bo.MacWorkflowARC;
import com.mac.custom.bo.MacWorkflowBlockAuto;
import com.mac.custom.bo.MacWorkflowClientValidation;
import com.mac.custom.bo.MacWorkflowFinanceValidation;
import com.mac.custom.bo.MacWorkflowUnblockManual;
import com.mac.custom.bo.MacWorkflowM5P;
import com.mac.custom.bo.MacWorkflowUnblockAuto;
import com.netappsid.annotations.Localized;
import com.netappsid.bo.Entity;
import com.netappsid.erp.server.bo.User;

@MappedSuperclass
public class BaseMacPropertyGroup extends Entity 
{
	private String groupe;
	private String code;
	private Integer ordre;
	private Serializable id;
	@Override
	@Id
	@GeneratedValue(generator = "naid-uuid")
	@GenericGenerator(name = "naid-uuid", strategy = "com.netappsid.framework.utils.UUIDGenerator")
	@Type(type = "com.netappsid.datatypes.UUIDUserType")
	@AccessType(value = "field")
	public Serializable getId()
	{
		return id;
	}

	@Override
	public void setId(Serializable id)
	{
		this.id = id;
	}
	
	@Column(name = "groupe")
	@org.hibernate.validator.NotNull
	@org.hibernate.validator.Length(max =50)
	@com.netappsid.annotations.Length(max = 50)
	@AccessType(value = "field")
	public String getGroupe()
	{
		return groupe;
	}
	@Column(name = "code")
	@org.hibernate.validator.NotNull
	@org.hibernate.validator.Length(max =50)
	@com.netappsid.annotations.Length(max = 50)
	@AccessType(value = "field")
	public String getCode()
	{
		return code;
	}
	@Column(name = "ordre")
	@org.hibernate.validator.NotNull
	@org.hibernate.validator.Length(max =50)
	@com.netappsid.annotations.Length(max = 50)
	@AccessType(value = "field")
	public Integer getOrdre()
	{
		return ordre;
	}


	public void setGroupe(String groupe)
	{
		String oldValue = this.groupe;
		this.groupe = groupe;
		firePropertyChange(PROPERTYNAME_GROUPE, oldValue, this.groupe);
	}
	
	public void setCode(String code)
	{
		String oldValue = this.code;
		this.code=code;
		firePropertyChange(PROPERTYNAME_CODE, oldValue, this.code);
	}
	public void setOrdre(Integer ordre)
	{
		Integer oldValue = this.ordre;
		this.ordre = ordre;
		firePropertyChange(PROPERTYNAME_ORDRE, oldValue, this.ordre);
	}
	public static final String PROPERTYNAME_GROUPE= "groupe";
	public static final String PROPERTYNAME_CODE= "code";
	public static final String PROPERTYNAME_ORDRE= "ordre";
	
}
