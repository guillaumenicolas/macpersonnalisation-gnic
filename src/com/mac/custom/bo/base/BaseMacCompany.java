package com.mac.custom.bo.base;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.AccessType;

import com.netappsid.annotations.Scale;
import com.netappsid.erp.server.bo.AvailableMeasureUnit;
import com.netappsid.erp.server.bo.Company;
import com.netappsid.erp.server.bo.InventoryAdjustmentType;
import com.netappsid.erp.server.bo.Location;
import com.netappsid.erp.server.bo.WorkingStation;

@MappedSuperclass
public abstract class BaseMacCompany extends Company
{
	private AvailableMeasureUnit workUnitAvailableMeasureUnit;
	private BigDecimal defaultMaterialProductWorkUnit;
	private WorkingStation defaultMaterialProductWorkingStation;
	private Integer defaultMaterialProductSupplyingDelay;
	private Integer defaultMaterialProductManufacturingDelay;
	private InventoryAdjustmentType defaultInventoryAdjustmentType;
	private Location defaultInventoryAdjustmentLocation;

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public AvailableMeasureUnit getWorkUnitAvailableMeasureUnit()
	{
		return workUnitAvailableMeasureUnit;
	}

	public void setWorkUnitAvailableMeasureUnit(AvailableMeasureUnit workUnitAvailableMeasureUnit)
	{
		AvailableMeasureUnit oldValue = this.workUnitAvailableMeasureUnit;
		this.workUnitAvailableMeasureUnit = workUnitAvailableMeasureUnit;
		firePropertyChange(PROPERTYNAME_WORKUNITAVAILABLEMEASUREUNIT, oldValue, this.workUnitAvailableMeasureUnit);
	}

	@Column(name = "defaultMaterialProductWorkUnit", precision = 32, scale = 10)
	@Scale(decimal = 10)
	@AccessType(value = "field")
	public BigDecimal getDefaultMaterialProductWorkUnit()
	{
		return defaultMaterialProductWorkUnit;
	}

	public void setDefaultMaterialProductWorkUnit(BigDecimal defaultMaterialProductWorkUnit)
	{
		BigDecimal oldValue = this.defaultMaterialProductWorkUnit;
		this.defaultMaterialProductWorkUnit = defaultMaterialProductWorkUnit;
		firePropertyChange(PROPERTYNAME_DEFAULTMATERIALPRODUCTWORKUNIT, oldValue, this.defaultMaterialProductWorkUnit);
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public WorkingStation getDefaultMaterialProductWorkingStation()
	{
		return defaultMaterialProductWorkingStation;
	}

	public void setDefaultMaterialProductWorkingStation(WorkingStation defaultMaterialProductWorkingStation)
	{
		WorkingStation oldValue = this.defaultMaterialProductWorkingStation;
		this.defaultMaterialProductWorkingStation = defaultMaterialProductWorkingStation;
		firePropertyChange(PROPERTYNAME_DEFAULTMATERIALPRODUCTWORKINGSTATION, oldValue, this.defaultMaterialProductWorkingStation);
	}

	@Column(name = "defaultMaterialProductSupplyingDelay")
	@AccessType(value = "field")
	public Integer getDefaultMaterialProductSupplyingDelay()
	{
		return defaultMaterialProductSupplyingDelay;
	}

	public void setDefaultMaterialProductSupplyingDelay(Integer defaultMaterialProductSupplyingDelay)
	{
		Integer oldValue = this.defaultMaterialProductSupplyingDelay;
		this.defaultMaterialProductSupplyingDelay = defaultMaterialProductSupplyingDelay;
		firePropertyChange(PROPERTYNAME_DEFAULTMATERIALPRODUCTSUPPLYINGDELAY, oldValue, this.defaultMaterialProductSupplyingDelay);
	}

	@Column(name = "defaultMaterialProductManufacturingDelay")
	@AccessType(value = "field")
	public Integer getDefaultMaterialProductManufacturingDelay()
	{
		return defaultMaterialProductManufacturingDelay;
	}

	public void setDefaultMaterialProductManufacturingDelay(Integer defaultMaterialProductManufacturingDelay)
	{
		Integer oldValue = this.defaultMaterialProductManufacturingDelay;
		this.defaultMaterialProductManufacturingDelay = defaultMaterialProductManufacturingDelay;
		firePropertyChange(PROPERTYNAME_DEFAULTMATERIALPRODUCTMANUFACTURINGDELAY, oldValue, this.defaultMaterialProductManufacturingDelay);
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public InventoryAdjustmentType getDefaultInventoryAdjustmentType()
	{
		return defaultInventoryAdjustmentType;
	}

	public void setDefaultInventoryAdjustmentType(InventoryAdjustmentType defaultInventoryAdjustmentType)
	{
		InventoryAdjustmentType oldValue = this.defaultInventoryAdjustmentType;
		this.defaultInventoryAdjustmentType = defaultInventoryAdjustmentType;
		firePropertyChange(PROPERTYNAME_DEFAULTINVENTORYADJUSTMENTTYPE, oldValue, this.defaultInventoryAdjustmentType);
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public Location getDefaultInventoryAdjustmentLocation()
	{
		return defaultInventoryAdjustmentLocation;
	}

	public void setDefaultInventoryAdjustmentLocation(Location defaultInventoryAdjustmentLocation)
	{
		Location oldValue = this.defaultInventoryAdjustmentLocation;
		this.defaultInventoryAdjustmentLocation = defaultInventoryAdjustmentLocation;
		firePropertyChange(PROPERTYNAME_DEFAULTINVENTORYADJUSTMENTLOCATION, oldValue, this.defaultInventoryAdjustmentLocation);
	}

	public static final String PROPERTYNAME_WORKUNITAVAILABLEMEASUREUNIT = "workUnitAvailableMeasureUnit";
	public static final String PROPERTYNAME_DEFAULTMATERIALPRODUCTWORKUNIT = "defaultMaterialProductWorkUnit";
	public static final String PROPERTYNAME_DEFAULTMATERIALPRODUCTWORKINGSTATION = "defaultMaterialProductWorkingStation";
	public static final String PROPERTYNAME_DEFAULTMATERIALPRODUCTSUPPLYINGDELAY = "defaultMaterialProductSupplyingDelay";
	public static final String PROPERTYNAME_DEFAULTMATERIALPRODUCTMANUFACTURINGDELAY = "defaultMaterialProductManufacturingDelay";
	public static final String PROPERTYNAME_DEFAULTINVENTORYADJUSTMENTTYPE = "defaultInventoryAdjustmentType";
	public static final String PROPERTYNAME_DEFAULTINVENTORYADJUSTMENTLOCATION = "defaultInventoryAdjustmentLocation";
}
