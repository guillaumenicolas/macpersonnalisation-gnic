package com.mac.custom.bo.base;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.AccessType;

import com.netappsid.annotations.GreaterOrEqual;
import com.netappsid.erp.server.bo.ServiceType;
import com.netappsid.europe.naid.custom.bo.OrderEuro;

@MappedSuperclass
public abstract class BaseMacOrder extends OrderEuro
{
	private boolean enablePrintCommentAndNoteOnShipping;
	private boolean enablePrintCommentAndNoteOnPackage;
	private boolean enablePrintCommentAndNoteOnSaleInvoice;
	private boolean enablePrintCommentAndNoteOnWorkOrder;
	private Date firstCommunicatedShippingDate;

	protected ServiceType serviceType;
	protected String originalOrderNumber;

	public void setEnablePrintCommentAndNoteOnShipping(boolean enable)
	{
		boolean oldValue = isEnablePrintCommentAndNoteOnShipping();
		this.enablePrintCommentAndNoteOnShipping = enable;
		firePropertyChange(PROPERTYNAME_ENABLEPRINTCOMMENTANDNOTEONSHIPPING, oldValue, enable);
	}

	@Column(name = "enablePrintCommentAndNoteOnShipping")//$NON-NLS-1$ 
	@AccessType(value = "field")//$NON-NLS-1$
	public boolean isEnablePrintCommentAndNoteOnShipping()
	{
		return enablePrintCommentAndNoteOnShipping;
	}

	public void setEnablePrintCommentAndNoteOnPackage(boolean enable)
	{
		boolean oldValue = isEnablePrintCommentAndNoteOnPackage();
		this.enablePrintCommentAndNoteOnPackage = enable;
		firePropertyChange(PROPERTYNAME_ENABLEPRINTCOMMENTANDNOTEONPACKAGE, oldValue, enable);
	}

	@Column(name = "enablePrintCommentAndNoteOnPackage")//$NON-NLS-1$ 
	@AccessType(value = "field")//$NON-NLS-1$
	public boolean isEnablePrintCommentAndNoteOnPackage()
	{
		return enablePrintCommentAndNoteOnPackage;
	}

	public void setEnablePrintCommentAndNoteOnSaleInvoice(boolean enable)
	{
		boolean oldValue = isEnablePrintCommentAndNoteOnSaleInvoice();
		this.enablePrintCommentAndNoteOnSaleInvoice = enable;
		firePropertyChange(PROPERTYNAME_ENABLEPRINTCOMMENTANDNOTEONINVOICE, oldValue, enable);
	}

	@Column(name = "enablePrintCommentAndNoteOnSaleInvoice")//$NON-NLS-1$ 
	@AccessType(value = "field")//$NON-NLS-1$
	public boolean isEnablePrintCommentAndNoteOnSaleInvoice()
	{
		return enablePrintCommentAndNoteOnSaleInvoice;
	}

	public void setEnablePrintCommentAndNoteOnWorkOrder(boolean enable)
	{
		boolean oldValue = isEnablePrintCommentAndNoteOnWorkOrder();
		this.enablePrintCommentAndNoteOnWorkOrder = enable;
		firePropertyChange(PROPERTYNAME_ENABLEPRINTCOMMENTANDNOTEONWORKORDER, oldValue, enable);
	}

	@Column(name = "enablePrintCommentAndNoteOnWorkOrder")//$NON-NLS-1$ 
	@AccessType(value = "field")//$NON-NLS-1$
	public boolean isEnablePrintCommentAndNoteOnWorkOrder()
	{
		return enablePrintCommentAndNoteOnWorkOrder;
	}

	public void setFirstCommunicatedShippingDate(Date firstCommunicatedShippingDate)
	{
		Date oldValue = getShippingDate();
		this.firstCommunicatedShippingDate = firstCommunicatedShippingDate;
		firePropertyChange(PROPERTYNAME_FIRSTCOMMUNICATEDSHIPPINGDATE, oldValue, firstCommunicatedShippingDate);
	}

	@Column(name = "firstCommunicatedShippingDate")
	@Temporal(TemporalType.DATE)
	@GreaterOrEqual(object = "date")
	@AccessType(value = "field")
	public Date getFirstCommunicatedShippingDate()
	{
		return firstCommunicatedShippingDate;
	}

	@ManyToOne(optional = true)
	@AccessType(value = "field")//$NON-NLS-1$
	public ServiceType getServiceType()
	{
		return serviceType;
	}

	public void setServiceType(ServiceType serviceType)
	{
		ServiceType oldValue = getServiceType();
		this.serviceType = serviceType;
		firePropertyChange(PROPERTYNAME_SERVICETYPE, oldValue, serviceType);
	}

	@Column(name = "originalOrderNumber")//$NON-NLS-1$ 
	@AccessType(value = "field")//$NON-NLS-1$
	public String getOriginalOrderNumber()
	{
		return originalOrderNumber;
	}

	public void setOriginalOrderNumber(String originalOrderNumber)
	{
		String oldValue = getOriginalOrderNumber();
		this.originalOrderNumber = originalOrderNumber;
		firePropertyChange(PROPERTYNAME_ORIGINALORDERNUMBER, oldValue, originalOrderNumber);
	}

	public static final String PROPERTYNAME_ENABLEPRINTCOMMENTANDNOTEONSHIPPING = "enablePrintCommentAndNoteOnShipping"; //$NON-NLS-1$
	public static final String PROPERTYNAME_ENABLEPRINTCOMMENTANDNOTEONPACKAGE = "enablePrintCommentAndNoteOnPackage"; //$NON-NLS-1$
	public static final String PROPERTYNAME_ENABLEPRINTCOMMENTANDNOTEONINVOICE = "enablePrintCommentAndNoteOnSaleInvoice"; //$NON-NLS-1$
	public static final String PROPERTYNAME_ENABLEPRINTCOMMENTANDNOTEONWORKORDER = "enablePrintCommentAndNoteOnWorkOrder"; //$NON-NLS-1$
	public static final String PROPERTYNAME_FIRSTCOMMUNICATEDSHIPPINGDATE = "firstCommunicatedShippingDate";
	public static final String PROPERTYNAME_SERVICETYPE = "serviceType"; //$NON-NLS-1$
	public static final String PROPERTYNAME_ORIGINALORDERNUMBER = "originalOrderNumber"; //$NON-NLS-1$

}