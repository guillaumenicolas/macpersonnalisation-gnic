package com.mac.custom.bo.base;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.Hibernate;
import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CollectionTypeInfo;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.Type;

import com.mac.custom.bo.WorkLoad;
import com.mac.custom.bo.WorkLoadDetail;
import com.netappsid.bo.Entity;
import com.netappsid.bo.EntityStatus;
import com.netappsid.erp.server.bo.Resource;
import com.netappsid.framework.utils.NaturalKeyValidator;
import com.netappsid.observable.ObservableCollections;
import com.netappsid.transactional.TransactionalCollections;
import com.netappsid.transactional.TransactionalSet;

@MappedSuperclass
public abstract class BaseWorkLoad extends com.netappsid.bo.Entity implements Serializable
{
	private Serializable id;
	private Date date;
	private Resource resource;
	private final Set<WorkLoadDetail> details = ObservableCollections.newObservableArrayListSet();

	@Override
	public void setChildEntityStatus(EntityStatus entityStatus)
	{
		super.setChildEntityStatus(entityStatus);

		if (getDetails() != null && Hibernate.isInitialized(getDetails()))
		{
			for (WorkLoadDetail detail : getDetails())
			{
				detail.setEntityStatus(entityStatus);
			}
		}
	}

	@Override
	public void setId(Serializable id)
	{
		this.id = id;
	}

	@Override
	@Id
	@GeneratedValue(generator = "naid-uuid")
	@GenericGenerator(name = "naid-uuid", strategy = "com.netappsid.framework.utils.UUIDGenerator")
	@Type(type = "com.netappsid.datatypes.UUIDUserType")
	@AccessType(value = "field")
	public Serializable getId()
	{
		return id;
	}

	public void setDate(Date date)
	{
		Date oldValue = getDate();
		this.date = date;
		firePropertyChange(PROPERTYNAME_DATE, oldValue, date);
	}

	@Column(name = "date")
	@org.hibernate.validator.NotNull
	@com.netappsid.annotations.NotNull
	@Temporal(TemporalType.DATE)
	@AccessType(value = "field")
	public Date getDate()
	{
		return date;
	}

	public void setResource(Resource resource)
	{
		Resource oldValue = getResource();
		this.resource = resource;
		firePropertyChange(PROPERTYNAME_RESOURCE, oldValue, resource);
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@org.hibernate.validator.NotNull
	@com.netappsid.annotations.NotNull
	@AccessType(value = "field")
	public Resource getResource()
	{
		return resource;
	}

	@Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
	@OneToMany(mappedBy = "workLoad", fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@Index(name = "workLoad_details_x")
	@CollectionTypeInfo(name = "com.netappsid.hibernate.ListSetType")
	@AccessType(value = "field")
	public Set<WorkLoadDetail> getDetails()
	{
		return details;
	}

	protected void onDetailsCollectionChange(TransactionalSet<WorkLoadDetail> details)
	{
		TransactionalCollections.end(details);
	}

	public void addToDetails(WorkLoadDetail workLoadDetail)
	{
		final TransactionalSet<WorkLoadDetail> transactional = TransactionalCollections.begin(this.details);
		workLoadDetail.setWorkLoad((WorkLoad) this);
		NaturalKeyValidator.isAddedItemValid(transactional, workLoadDetail);
		transactional.add(workLoadDetail);
		onDetailsCollectionChange(transactional);
	}

	public void addManyToDetails(Collection<WorkLoadDetail> details)
	{
		final TransactionalSet<WorkLoadDetail> transactional = TransactionalCollections.begin(this.details);
		for (WorkLoadDetail workLoadDetail : details)
		{
			workLoadDetail.setWorkLoad((WorkLoad) this);
		}
		for (WorkLoadDetail workLoadDetail : details)
		{
			NaturalKeyValidator.isAddedItemValid(transactional, workLoadDetail);
		}

		transactional.addAll(details);
		onDetailsCollectionChange(transactional);
	}

	public void removeFromDetails(WorkLoadDetail workLoadDetail)
	{
		final TransactionalSet<WorkLoadDetail> transactional = TransactionalCollections.begin(this.details);
		if (transactional.remove(workLoadDetail))
		{
			workLoadDetail.setWorkLoad(null);
		}
		onDetailsCollectionChange(transactional);
	}

	public void removeManyFromDetails(Collection<WorkLoadDetail> details)
	{
		final TransactionalSet<WorkLoadDetail> transactional = TransactionalCollections.begin(this.details);
		for (WorkLoadDetail workLoadDetail : details)
		{
			if (transactional.contains(workLoadDetail))
			{
				workLoadDetail.setWorkLoad(null);
			}
		}

		transactional.removeAll(details);
		onDetailsCollectionChange(transactional);
	}

	public void removeAllFromDetails()
	{
		final TransactionalSet<WorkLoadDetail> transactional = TransactionalCollections.begin(this.details);
		for (WorkLoadDetail workLoadDetail : details)
		{
			workLoadDetail.setWorkLoad(null);
		}

		transactional.clear();
		onDetailsCollectionChange(transactional);
	}

	public void replaceDetails(WorkLoadDetail oldWorkLoadDetail, WorkLoadDetail newWorkLoadDetail)
	{
		final TransactionalSet<WorkLoadDetail> transactional = TransactionalCollections.begin(this.details);

		if (transactional.remove(oldWorkLoadDetail))
		{
			oldWorkLoadDetail.setWorkLoad(null);
			newWorkLoadDetail.setWorkLoad((WorkLoad) this);
			NaturalKeyValidator.isAddedItemValid(transactional, newWorkLoadDetail);
			transactional.add(newWorkLoadDetail);
		}
		onDetailsCollectionChange(transactional);
	}

	public static final String PROPERTYNAME_DATE = "date";
	public static final String PROPERTYNAME_RESOURCE = "resource";
	public static final String PROPERTYNAME_DETAILS = "details";
}
