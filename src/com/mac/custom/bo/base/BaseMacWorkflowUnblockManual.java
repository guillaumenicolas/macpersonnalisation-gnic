package com.mac.custom.bo.base;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.netappsid.bo.Entity;
import com.netappsid.erp.server.bo.DocumentStatus;

@MappedSuperclass
public abstract class BaseMacWorkflowUnblockManual extends Entity
{
	private Serializable id;
	
	private DocumentStatus unblockedStatus;
	
	private boolean active;
	
	@Id
	@GeneratedValue(generator = "naid-uuid")
	@GenericGenerator(name = "naid-uuid", strategy = "com.netappsid.framework.utils.UUIDGenerator")
	@Type(type = "com.netappsid.datatypes.UUIDUserType")
	@AccessType(value = "field")
	public Serializable getId()
	{
		return id;
	}

	public void setId(Serializable id)
	{
		this.id = id;
	}
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@com.netappsid.annotations.NotNull
	@org.hibernate.validator.NotNull
	@AccessType(value = "field")
	public DocumentStatus getUnblockedStatus()
	{
		return unblockedStatus;
	}

	public void setUnblockedStatus(DocumentStatus unblockedStatus)
	{
		DocumentStatus oldValue = this.unblockedStatus;
		this.unblockedStatus = unblockedStatus;
		firePropertyChange(PROPERTYNAME_UNBLOCKEDSTATUS, oldValue, this.unblockedStatus);
	}	

	
	public void setActive(boolean active)
	{
		boolean oldValue = isActive();
		this.active = active;
		firePropertyChange(PROPERTYNAME_ACTIVE, oldValue, active);
	}

	@Column(name = "active")
	@AccessType(value = "field")
	public boolean isActive()
	{
		return active;
	}
	
	public static final String PROPERTYNAME_UNBLOCKEDSTATUS = "unblockedStatus";
	public static final String PROPERTYNAME_ACTIVE = "active";
}
