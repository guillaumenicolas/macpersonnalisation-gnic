package com.mac.custom.bo.base;

import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.AccessType;

import com.netappsid.erp.server.bo.BlockingReason;

@MappedSuperclass
public abstract class BaseMacBlockingReason extends BlockingReason
{
	private boolean excludeFormOrdersInProductionTotal;

	@AccessType(value = "field")
	public boolean isExcludeFormOrdersInProductionTotal()
	{
		return excludeFormOrdersInProductionTotal;
	}

	public void setExcludeFormOrdersInProductionTotal(boolean excludeFormOrdersInProductionTotal)
	{
		boolean oldValue = this.isExcludeFormOrdersInProductionTotal();
		this.excludeFormOrdersInProductionTotal = excludeFormOrdersInProductionTotal;
		firePropertyChange(PROPERTYNAME_EXCLUDEFROMORDERINPRODUCTIONTOTAL, oldValue, excludeFormOrdersInProductionTotal);
	}

	public static final String PROPERTYNAME_EXCLUDEFROMORDERINPRODUCTIONTOTAL = "excludeFormOrdersInProductionTotal";
}
