package com.mac.custom.bo.base;

import java.io.Serializable;

import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.mac.custom.bo.CostCode;
import com.mac.custom.bo.CostCodeDetail;
import com.netappsid.bo.Entity;
import com.netappsid.erp.server.bo.Company;

@MappedSuperclass
public abstract class BaseManagementAccounting extends Entity
{
	private Serializable id;
	private Company company;
	private CostCode costCodeSale;
	private CostCode costCodePurchase;
	private CostCodeDetail costCodeDetailSale;
	private CostCodeDetail costCodeDetailPurchase;

	public BaseManagementAccounting()
	{
		super();
	}

	@Override
	@Id
	@GeneratedValue(generator = "naid-uuid")
	@GenericGenerator(name = "naid-uuid", strategy = "com.netappsid.framework.utils.UUIDGenerator")
	@Type(type = "com.netappsid.datatypes.UUIDUserType")
	@AccessType(value = "field")
	public Serializable getId()
	{
		return id;
	}

	@Override
	public void setId(Serializable id)
	{
		this.id = id;
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	@com.netappsid.annotations.NotNull
	public Company getCompany()
	{
		return company;
	}

	public void setCompany(Company company)
	{
		Company oldValue = this.company;
		this.company = company;

		firePropertyChange(PROPERTYNAME_COMPANY, oldValue, this.company);
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public CostCode getCostCodeSale()
	{
		return costCodeSale;
	}

	public void setCostCodeSale(CostCode costCode)
	{
		CostCode oldValue = this.costCodeSale;
		this.costCodeSale = costCode;

		firePropertyChange(PROPERTYNAME_COSTCODESALE, oldValue, this.costCodeSale);
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public CostCode getCostCodePurchase()
	{
		return costCodePurchase;
	}

	public void setCostCodePurchase(CostCode costCode)
	{
		CostCode oldValue = this.costCodePurchase;
		this.costCodePurchase = costCode;

		firePropertyChange(PROPERTYNAME_COSTCODEPURCHASE, oldValue, this.costCodePurchase);
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public CostCodeDetail getCostCodeDetailSale()
	{
		return costCodeDetailSale;
	}

	public void setCostCodeDetailSale(CostCodeDetail costCodeDetail)
	{
		CostCodeDetail oldValue = this.costCodeDetailSale;
		this.costCodeDetailSale = costCodeDetail;

		firePropertyChange(PROPERTYNAME_COSTCODEDETAILSALE, oldValue, this.costCodeDetailSale);
	}

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@AccessType(value = "field")
	public CostCodeDetail getCostCodeDetailPurchase()
	{
		return costCodeDetailPurchase;
	}

	public void setCostCodeDetailPurchase(CostCodeDetail costCodeDetail)
	{
		CostCodeDetail oldValue = this.costCodeDetailPurchase;
		this.costCodeDetailPurchase = costCodeDetail;

		firePropertyChange(PROPERTYNAME_COSTCODEDETAILPURCHASE, oldValue, this.costCodeDetailPurchase);
	}

	public static final String PROPERTYNAME_CODE = "code";
	public static final String PROPERTYNAME_UPPERCODE = "upperCode";
	public static final String PROPERTYNAME_COMPANY = "company";
	public static final String PROPERTYNAME_COSTCODESALE = "costCodeSale";
	public static final String PROPERTYNAME_COSTCODEPURCHASE = "costCodePurchase";
	public static final String PROPERTYNAME_COSTCODEDETAILSALE = "costCodeDetailSale";
	public static final String PROPERTYNAME_COSTCODEDETAILPURCHASE = "costCodeDetailPurchase";
}