package com.mac.custom.bo;

import java.util.Date;

import org.apache.log4j.Logger;

import com.mac.custom.dao.MacQuotationDetailSearchDAO;
import com.netappsid.annotations.DAO;
import com.netappsid.bo.model.Model;
import com.netappsid.dao.utils.boquery.BOQuery;
import com.netappsid.erp.server.bo.Product;
import com.netappsid.erp.server.bo.ProductQuickLibrary;
import com.netappsid.erp.server.bo.ProductSearch;
import com.netappsid.erp.server.bo.SaleTransaction;
import com.netappsid.erp.server.bo.User;

@DAO(dao = MacQuotationDetailSearchDAO.class)
public class MacQuotationDetailSearch extends Model
{
	public static final String PROPERTYNAME_MACQUOTATIONNUMBER = "macQuotationNumber";
	public static final String PROPERTYNAME_DESCRIPTION = "description";
	public static final String PROPERTYNAME_MACQUOTATION = "macQuotation";
	public static final String PROPERTYNAME_PONUMBER = "poNumber";
	public static final String PROPERTYNAME_DATEFROM = "dateFrom";
	public static final String PROPERTYNAME_DATETO = "dateTo";
	public static final String PROPERTYNAME_PRODUCT = "product";
	public static final String PROPERTYNAME_PRODUCTQUICKLIBRARY = "productQuickLibrary";
	public static final String PROPERTYNAME_MACCUSTOMER = "macCustomer";
	public static final String PROPERTYNAME_USER = "user";

	@SuppressWarnings("unused")
	private static final Logger LOGGER = Logger.getLogger(ProductSearch.class);

	private String macQuotationNumber;
	private String poNumber;
	private MacQuotation macQuotation;
	private Product product;
	private ProductQuickLibrary productQuickLibrary;
	private MacCustomer macCustomer;
	private Date dateFrom;
	private Date dateTo;
	private User user;

	// private Map<String, ProductDescription> description = new HashMap<String, ProductDescription>();

	public BOQuery getBOQuery(Class<?> beanClass)
	{
		BOQuery boQuery = new BOQuery(beanClass);
		if (getMacQuotationNumber() != null)
		{
			boQuery.addLike("saleTransaction.number", getMacQuotationNumber());
		}
		if (getMacQuotation() != null)
		{
			boQuery.addEqual("saleTransaction", getMacQuotation());
		}
		if (getPoNumber() != null)
		{
			boQuery.addLike("saleTransaction.PONumber", getPoNumber());
		}
		if (getDateFrom() != null)
		{
			boQuery.addGreaterOrEqual("saleTransaction.date", getDateFrom());
		}
		if (getDateTo() != null)
		{
			boQuery.addLowerOrEqual("saleTransaction.date", getDateTo());
		}
		if (getProduct() != null)
		{
			boQuery.addEqual("product", getProduct());
		}
		if (getMacCustomer() != null)
		{
			boQuery.addEqual("saleTransaction.customer", getMacCustomer());
		}
		if (getUser() != null)
		{
			boQuery.addEqual("saleTransaction.user", getUser());
		}

		return boQuery;
	}

	public void clearSearchParameters()
	{
		setMacQuotationNumber(null);
		setMacQuotation(null);
		setPoNumber(null);
		setProduct(null);
		setDateFrom(null);
		setDateTo(null);
		setProductQuickLibrary(null);
		setMacCustomer(null);
	}

	public String getMacQuotationNumber()
	{
		return macQuotationNumber;
	}

	public MacQuotation getMacQuotation()
	{
		return macQuotation;
	}

	public void setMacQuotationNumber(String macQuotationNumber)
	{
		String oldValue = getMacQuotationNumber();
		this.macQuotationNumber = macQuotationNumber;
		firePropertyChange(PROPERTYNAME_MACQUOTATIONNUMBER, oldValue, macQuotationNumber);
	}

	public void setMacQuotation(MacQuotation macQuotation)
	{
		SaleTransaction oldValue = getMacQuotation();
		this.macQuotation = macQuotation;
		firePropertyChange(PROPERTYNAME_MACQUOTATION, oldValue, macQuotation);
	}

	public Date getDateFrom()
	{
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom)
	{
		Date oldValue = getDateFrom();
		this.dateFrom = dateFrom;
		firePropertyChange(PROPERTYNAME_DATEFROM, oldValue, dateFrom);
	}

	public Date getDateTo()
	{
		return dateTo;
	}

	public void setDateTo(Date dateTo)
	{
		Date oldValue = getDateTo();
		this.dateTo = dateTo;
		firePropertyChange(PROPERTYNAME_DATETO, oldValue, dateTo);
	}

	public Product getProduct()
	{
		return product;
	}

	public void setProduct(Product product)
	{
		Product oldValue = getProduct();
		this.product = product;
		firePropertyChange(PROPERTYNAME_PRODUCT, oldValue, product);
	}

	public String getPoNumber()
	{
		return poNumber;
	}

	public void setPoNumber(String poNumber)
	{
		String oldValue = getPoNumber();
		this.poNumber = poNumber;
		firePropertyChange(PROPERTYNAME_PONUMBER, oldValue, poNumber);
	}

	public ProductQuickLibrary getProductQuickLibrary()
	{
		return productQuickLibrary;
	}

	public void setProductQuickLibrary(ProductQuickLibrary productQuickLibrary)
	{
		ProductQuickLibrary oldValue = getProductQuickLibrary();
		this.productQuickLibrary = productQuickLibrary;
		firePropertyChange(PROPERTYNAME_PRODUCTQUICKLIBRARY, oldValue, productQuickLibrary);
	}

	public MacCustomer getMacCustomer()
	{
		return macCustomer;
	}

	public void setMacCustomer(MacCustomer macCustomer)
	{
		MacCustomer oldValue = getMacCustomer();
		this.macCustomer = macCustomer;
		firePropertyChange(PROPERTYNAME_MACCUSTOMER, oldValue, macCustomer);
	}

	public User getUser()
	{
		return user;
	}

	public void setUser(User user)
	{
		User oldValue = getUser();
		this.user = user;
		firePropertyChange(PROPERTYNAME_USER, oldValue, user);
	}
}
