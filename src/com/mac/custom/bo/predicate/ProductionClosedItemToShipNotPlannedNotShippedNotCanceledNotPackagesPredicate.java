package com.mac.custom.bo.predicate;

import java.math.BigDecimal;

import org.hibernate.Hibernate;

import com.netappsid.erp.server.bo.ItemToShip;
import com.netappsid.erp.server.bo.Packages;
import com.netappsid.erp.server.bo.StockReserved;
import com.netappsid.erp.server.bo.predicate.ItemToShipNotPlannedNotShippedNotCanceledPredicate;

public class ProductionClosedItemToShipNotPlannedNotShippedNotCanceledNotPackagesPredicate extends ItemToShipNotPlannedNotShippedNotCanceledPredicate
{
	@Override
	public boolean apply(ItemToShip itemToShip)
	{
		if (Packages.class.isAssignableFrom(Hibernate.getClass(itemToShip)))
		{
			return false;
		}

		BigDecimal qtyStock = BigDecimal.ZERO;
		boolean apply = true;
		if (!itemToShip.getStocksReserved().isEmpty())
		{
			for (StockReserved stockReserved : itemToShip.getStocksReserved())
			{
				qtyStock = qtyStock.add(stockReserved.getStock().getQuantityConvertedValue());
			}

			if (qtyStock.compareTo(itemToShip.getQuantityConvertedValue()) == -1)
			{
				apply = false;
			}
		}

		return super.apply(itemToShip) && apply;
	}
}
