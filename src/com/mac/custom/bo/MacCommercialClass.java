package com.mac.custom.bo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.mac.custom.bo.base.BaseMacCommercialClass;
import com.mac.custom.dao.MacCommercialClassDAO;
import com.netappsid.annotations.DAO;
import com.netappsid.framework.utils.ObservableCollectionUtils;
import com.netappsid.observable.ObservableCollections;

@javax.persistence.Entity
@Table(name = "MacCommercialClass", schema = "MacSchema")
@PrimaryKeyJoinColumn(name = "id")
@DAO(dao = MacCommercialClassDAO.class)
public class MacCommercialClass extends BaseMacCommercialClass
{
	private final List<Serializable> activitesIds = ObservableCollections.newObservableArrayList();
	private final List<Serializable> enseignesIds = ObservableCollections.newObservableArrayList();

	@Transient
	public List<Serializable> getActivitesClientId()
	{
		ArrayList<Serializable> newIds = new ArrayList<Serializable>();

		for (ActiviteClient activiteClient : getActivitesClient())
		{
			newIds.add(activiteClient.getId());
		}
		ObservableCollectionUtils.clearAndAddAll(activitesIds, newIds);
		return activitesIds;
	}

	@Transient
	public List<Serializable> getEnseignesClientId()
	{
		ArrayList<Serializable> newIds = new ArrayList<Serializable>();

		for (EnseigneClient enseigneClient : getEnseignesClient())
		{
			newIds.add(enseigneClient.getId());
		}
		ObservableCollectionUtils.clearAndAddAll(enseignesIds, newIds);
		return enseignesIds;
	}
}
