package com.mac.custom.bo;

import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.mac.custom.bo.base.BaseMacPurchaseOrder;
import com.mac.custom.dao.MacPurchaseOrderDAO;
import com.netappsid.annotations.DAO;

@javax.persistence.Entity
@Table(name = "MacPurchaseOrder", schema = "MacSchema")
@PrimaryKeyJoinColumn(name = "id")
@DAO(dao = MacPurchaseOrderDAO.class)
public class MacPurchaseOrder extends BaseMacPurchaseOrder
{

}
