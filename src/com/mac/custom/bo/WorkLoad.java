package com.mac.custom.bo;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import com.mac.custom.bo.base.BaseWorkLoad;
import com.mac.custom.dao.WorkLoadDAO;
import com.netappsid.annotations.DAO;
import com.netappsid.erp.server.datatypes.GroupMeasureValue;

@Entity
@Table(name = "WorkLoad", schema = "MacSchema", uniqueConstraints = { @UniqueConstraint(columnNames = { "date", "resource_id" }) })
@DAO(dao = WorkLoadDAO.class)
public class WorkLoad extends BaseWorkLoad
{
	private GroupMeasureValue quantityToProduce;
	private GroupMeasureValue quantityProduced;
	private GroupMeasureValue quantityAvailable;
	private GroupMeasureValue quantity;
	private GroupMeasureValue quantityBlocked;

	/**
	 * Clears all cached quantities for this object.
	 */
	public void invalidateQuantities()
	{
		quantityToProduce = null;
		quantityProduced = null;
		quantityAvailable = null;
		quantity = null;
		quantityBlocked = null;
	}

	public void loadQuantities()
	{
		getQuantityToProduce();
		getQuantity();
		getQuantityProduced();
		getQuantityAvailable();
		getQuantityBlocked();
	}

	@Transient
	public GroupMeasureValue getQuantityAvailable()
	{
		if (quantityAvailable == null)
		{
			for (WorkLoadDetail detail : getDetails())
			{
				if (quantityAvailable == null)
				{
					quantityAvailable = detail.getQuantity();
				}
				else
				{
					quantityAvailable = quantityAvailable.add(detail.getQuantity());
				}

				if (detail.getQuantityProduced() != null && detail.getQuantityProduced().isValid())
				{
					quantityAvailable = quantityAvailable.substract(detail.getQuantityProduced());
				}
			}
		}
		return quantityAvailable;
	}

	@Transient
	public GroupMeasureValue getQuantity()
	{
		if (quantity == null)
		{
			for (WorkLoadDetail detail : getDetails())
			{
				if (quantity == null)
				{
					quantity = detail.getQuantity();
				}
				else
				{
					quantity = quantity.add(detail.getQuantity());
				}
			}
		}
		return quantity;
	}

	@Transient
	public GroupMeasureValue getQuantityBlocked()
	{
		if (quantityBlocked == null)
		{
			for (WorkLoadDetail detail : getDetails())
			{
				MacOrder order = detail.getOrderDetail().getMacOrder();

				if (order.isBlockProductionEffective())
				{
					if (quantityBlocked == null)
					{
						quantityBlocked = detail.getQuantity();
					}
					else
					{
						quantityBlocked = quantityBlocked.add(detail.getQuantity());
					}
				}
			}
		}
		return quantityBlocked;
	}

	@Transient
	public GroupMeasureValue getQuantityToProduce()
	{
		if (quantityToProduce == null)
		{
			for (WorkLoadDetail detail : getDetails())
			{
				if (quantityToProduce == null)
				{
					quantityToProduce = detail.getQuantityToProduce();
				}
				else
				{
					quantityToProduce = quantityToProduce.add(detail.getQuantityToProduce());
				}
			}
		}
		return quantityToProduce;
	}

	@Transient
	public GroupMeasureValue getQuantityProduced()
	{
		if (quantityProduced == null)
		{
			for (WorkLoadDetail detail : getDetails())
			{
				if (detail.getQuantityProduced() != null && detail.getQuantityProduced().isValid())
				{
					if (quantityProduced == null)
					{
						quantityProduced = detail.getQuantityProduced();
					}
					else
					{
						quantityProduced = quantityProduced.add(detail.getQuantityProduced());
					}
				}
			}
		}
		return quantityProduced;
	}
}
