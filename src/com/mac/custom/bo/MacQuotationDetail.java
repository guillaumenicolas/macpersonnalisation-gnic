package com.mac.custom.bo;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.Hibernate;

import com.mac.custom.bo.base.BaseMacQuotationDetail;
import com.netappsid.bo.model.Model;
import com.netappsid.datatypes.MonetaryAmount;
import com.netappsid.erp.server.Variable;
import com.netappsid.erp.server.bo.Discount;
import com.netappsid.erp.server.bo.SaleOrderDetailLocation;
import com.netappsid.erp.server.bo.SaleTransactionDetailDiscount;
import com.netappsid.erp.server.bo.SaleTransactionDetailVariableDiscount;
import com.netappsid.erp.server.bo.TransactionAvailableDiscount;
import com.netappsid.erp.server.datatypes.GroupMeasureValue;

@Entity
@Table(name = "MacQuotationDetail", schema = "MacSchema")
@PrimaryKeyJoinColumn(name = "id")
public class MacQuotationDetail extends BaseMacQuotationDetail
{
	@Transient
	public String getLocation()
	{
		// One detail to one location (perso mac)
		return !this.getLocations().isEmpty() ? this.getLocations().get(0).getLocation() : "";
	}

	public void setLocation(String location)
	{
		// Take the only one location
		SaleOrderDetailLocation oldLocation = !this.getLocations().isEmpty() ? this.getLocations().get(0) : null;
		String oldLocationStr = oldLocation != null ? oldLocation.getLocation() : "";

		if (location != null)
		{
			if (oldLocation != null)
			{
				// if exists then replace
				oldLocation.setLocation(location);
			}
			else
			{
				// else add this new location
				SaleOrderDetailLocation newLocation = new SaleOrderDetailLocation();
				newLocation.setLocation(location);
				newLocation.setQuantity(new GroupMeasureValue(getProduct().getSaleMeasure().getGroupMeasure(), getQuantityValue(), getProduct()
						.getDefaultUnitConversion(), getProduct().getDefaultConvertedUnit()));
				addToLocations(newLocation);
			}
		}
		else
		{
			removeFromLocations(oldLocation);
		}
		firePropertyChange(PROPERTYNAME_LOCATION, oldLocationStr, location);
	}

	@Override
	public void setQuantity(GroupMeasureValue quantity)
	{
		super.setQuantity(quantity);
		refreshLocationQuantity();
	}

	// Also in MacOrderDetail. Please make sure they remain consistent with each other.
	@Transient
	private SaleTransactionDetailVariableDiscount getDetailByDiscountCode(String code)
	{
		SaleTransactionDetailVariableDiscount detail = null;
		List<SaleTransactionDetailDiscount> discounts = this.getDiscounts();
		for (SaleTransactionDetailDiscount saleTransactionDetailDiscount : discounts)
		{
			Discount discount = saleTransactionDetailDiscount.getDiscount();
			if (Variable.class.isAssignableFrom(Hibernate.getClass(saleTransactionDetailDiscount)) && discount != null && discount.getCode().equals(code))
			{
				detail = (SaleTransactionDetailVariableDiscount) Model.getTarget(saleTransactionDetailDiscount);
				break;
			}

		}

		return detail;
	}

	// Also in MacOrderDetail. Please make sure they remain consistent with each other.
	@Transient
	public BigDecimal getTauxR1()
	{
		return getTaux(R1);
	}

	// Also in MacOrderDetail. Please make sure they remain consistent with each other.
	public void setTauxR1(BigDecimal rate)
	{
		setTaux(R1, rate);
	}

	// Also in MacOrderDetail. Please make sure they remain consistent with each other.
	@Transient
	public BigDecimal getTauxR2()
	{
		return getTaux(R2);
	}

	// Also in MacOrderDetail. Please make sure they remain consistent with each other.
	public void setTauxR2(BigDecimal rate)
	{
		setTaux(R2, rate);
	}

	// Also in MacOrderDetail. Please make sure they remain consistent with each other.
	@Transient
	public BigDecimal getTauxR3()
	{
		return getTaux(R3);
	}

	// Also in MacOrderDetail. Please make sure they remain consistent with each other.
	public void setTauxR3(BigDecimal rate)
	{
		setTaux(R3, rate);
	}

	// Also in MacOrderDetail. Please make sure they remain consistent with each other.
	@Transient
	public BigDecimal getTauxR4()
	{
		return getTaux(R4);
	}

	// Also in MacOrderDetail. Please make sure they remain consistent with each other.
	public void setTauxR4(BigDecimal rate)
	{
		setTaux(R4, rate);
	}

	// Also in MacOrderDetail. Please make sure they remain consistent with each other.
	@Transient
	private BigDecimal getTaux(String code)
	{
		SaleTransactionDetailDiscount detail = getDetailByDiscountCode(code);
		if (detail == null)
		{
			return null;
		}
		return detail.getValue();
	}

	// Also in MacOrderDetail. Please make sure they remain consistent with each other.
	private void setTaux(String code, BigDecimal rate)
	{
		if (rate.compareTo(BigDecimal.ZERO) < 0 || rate.compareTo(new BigDecimal(100)) > 0)
		{
			return;
		}
		SaleTransactionDetailVariableDiscount detail = getDetailByDiscountCode(code);
		if (detail == null)
		{
			MacQuotation quotation = (MacQuotation) Model.getTarget(getTransaction());
			List<TransactionAvailableDiscount> availableDiscounts = quotation.getAvailableDiscounts();
			for (TransactionAvailableDiscount availableDiscount : availableDiscounts)
			{
				if (code.equals(availableDiscount.getDiscountCode()))
				{
					if (!availableDiscount.isIncluded())
					{
						availableDiscount.setIncluded(true);
						detail = getDetailByDiscountCode(code);
					}
					break;
				}
			}
		}

		if (detail == null)
		{
			return;
		}

		detail.setOverwriteRate(true);
		detail.setRate(rate);

		// No need for firePropertyChange, it is done in calculateTotal().
		calculateAllTotals();
	}

	// Also in MacOrderDetail. Please make sure they remain consistent with each other.
	@Transient
	public MonetaryAmount getMontantR1()
	{
		return getMontant(R1);
	}

	// Also in MacOrderDetail. Please make sure they remain consistent with each other.
	public void setMontantR1(MonetaryAmount amount)
	{
		setMontant(R1, amount);
	}

	// Also in MacOrderDetail. Please make sure they remain consistent with each other.
	@Transient
	public MonetaryAmount getMontantR2()
	{
		return getMontant(R2);
	}

	// Also in MacOrderDetail. Please make sure they remain consistent with each other.
	public void setMontantR2(MonetaryAmount amount)
	{
		setMontant(R2, amount);
	}

	// Also in MacOrderDetail. Please make sure they remain consistent with each other.
	@Transient
	public MonetaryAmount getMontantR3()
	{
		return getMontant(R3);
	}

	// Also in MacOrderDetail. Please make sure they remain consistent with each other.
	public void setMontantR3(MonetaryAmount amount)
	{
		setMontant(R3, amount);
	}

	// Also in MacOrderDetail. Please make sure they remain consistent with each other.
	@Transient
	public MonetaryAmount getMontantR4()
	{
		return getMontant(R4);
	}

	// Also in MacOrderDetail. Please make sure they remain consistent with each other.
	public void setMontantR4(MonetaryAmount amount)
	{
		setMontant(R4, amount);
	}

	// Also in MacOrderDetail. Please make sure they remain consistent with each other.
	@Transient
	private MonetaryAmount getMontant(String code)
	{
		SaleTransactionDetailDiscount detail = getDetailByDiscountCode(code);
		if (detail == null)
		{
			return null;
		}
		return detail.getTotalAmount();
	}

	// Also in MacOrderDetail. Please make sure they remain consistent with each other.
	private void setMontant(String code, MonetaryAmount targetAmount)
	{
		SaleTransactionDetailVariableDiscount detail = getDetailByDiscountCode(code);
		if (detail == null)
		{
			MacQuotation quotation = (MacQuotation) Model.getTarget(getTransaction());
			List<TransactionAvailableDiscount> availableDiscounts = quotation.getAvailableDiscounts();
			for (TransactionAvailableDiscount availableDiscount : availableDiscounts)
			{
				if (code.equals(availableDiscount.getDiscountCode()))
				{
					if (!availableDiscount.isIncluded())
					{
						availableDiscount.setIncluded(true);
						detail = getDetailByDiscountCode(code);
					}
					break;
				}
			}
		}

		if (detail == null)
		{
			return;
		}

		BigDecimal oldRate = getTaux(code);
		BigDecimal newRate;
		RoundingMode roundingMode = RoundingMode.HALF_UP;
		MathContext mathContext = new MathContext(32, roundingMode);

		// Clear previous rate to make calculations on full remaining amount
		setTaux(code, BigDecimal.ZERO);
		if (targetAmount == null || targetAmount.getValue().compareTo(BigDecimal.ZERO) == 0)
		{
			// Amount of 0 == rate of 0
			newRate = BigDecimal.ZERO;
		}
		else
		{
			// No previous discount, calculate rate from remaining price
			MonetaryAmount totalPrice = this.getTotalPrice();
			if (totalPrice == null || totalPrice.getValue().compareTo(BigDecimal.ZERO) == 0)
			{
				newRate = oldRate;
			}
			else
			{
				newRate = targetAmount.multiply(100).divide(totalPrice, mathContext).getValue().setScale(10, roundingMode);
			}
		}

		// If the value is invalid, revert to old rate
		if (newRate.compareTo(BigDecimal.ZERO) < 0 || newRate.compareTo(new BigDecimal(100)) > 0)
		{
			newRate = oldRate;
		}
		setTaux(code, newRate);
	}

	// Also in MacOrderDetail. Please make sure they remain consistent with each other.
	@Override
	public void calculateTotal()
	{
		MonetaryAmount oldAmountR1 = getMontantR1();
		MonetaryAmount oldAmountR2 = getMontantR2();
		MonetaryAmount oldAmountR3 = getMontantR3();
		MonetaryAmount oldAmountR4 = getMontantR4();
		super.calculateTotal();

		BigDecimal rateR1 = getTauxR1();
		BigDecimal rateR2 = getTauxR2();
		BigDecimal rateR3 = getTauxR3();
		BigDecimal rateR4 = getTauxR4();
		MonetaryAmount amountR1 = getMontantR1();
		MonetaryAmount amountR2 = getMontantR2();
		MonetaryAmount amountR3 = getMontantR3();
		MonetaryAmount amountR4 = getMontantR4();

		// For rate, pass null as an old rate to insure both panels showing the data are updated
		firePropertyChange(PROPERTYNAME_TAUXR1, null, rateR1);
		firePropertyChange(PROPERTYNAME_TAUXR2, null, rateR2);
		firePropertyChange(PROPERTYNAME_TAUXR3, null, rateR3);
		firePropertyChange(PROPERTYNAME_TAUXR4, null, rateR4);
		firePropertyChange(PROPERTYNAME_MONTANTR1, oldAmountR1, amountR1);
		firePropertyChange(PROPERTYNAME_MONTANTR2, oldAmountR2, amountR2);
		firePropertyChange(PROPERTYNAME_MONTANTR3, oldAmountR3, amountR3);
		firePropertyChange(PROPERTYNAME_MONTANTR4, oldAmountR4, amountR4);
	}

	/**
	 * Refreshes the quantity of this quotation detail's {@link SaleOrderDetailLocation} (if there is one), with the assumption that there won't be multiple
	 * ones.<br/>
	 * If there's no location, does nothing.
	 */
	private void refreshLocationQuantity()
	{

		// If quantity gets changed while there is no product, we don't have enough data to fill out a Location.
		// We can't refresh.
		if (getProduct() != null)
		{

			// Much like in setLocation(), work on the assumption that there's not more than one location
			SaleOrderDetailLocation oldLocation = !this.getLocations().isEmpty() ? this.getLocations().get(0) : null;

			// If there is no location, there is no quantity to refresh; only proceed if we have a location
			if (oldLocation != null)
			{
				oldLocation.setQuantity(new GroupMeasureValue(getProduct().getSaleMeasure().getGroupMeasure(), getQuantityValue(), getProduct()
						.getDefaultUnitConversion(), getProduct().getDefaultConvertedUnit()));
			}
		}
	}

	private static final String R1 = "R1";
	private static final String R2 = "R2";
	private static final String R3 = "R3";
	private static final String R4 = "R4";

	public static final String PROPERTYNAME_LOCATION = "location";
	public static final String PROPERTYNAME_TAUX_PREFIX = "taux";
	public static final String PROPERTYNAME_TAUXR1 = PROPERTYNAME_TAUX_PREFIX + R1;
	public static final String PROPERTYNAME_TAUXR2 = PROPERTYNAME_TAUX_PREFIX + R2;
	public static final String PROPERTYNAME_TAUXR3 = PROPERTYNAME_TAUX_PREFIX + R3;
	public static final String PROPERTYNAME_TAUXR4 = PROPERTYNAME_TAUX_PREFIX + R4;
	public static final String PROPERTYNAME_MONTANT_PREFIX = "montant";
	public static final String PROPERTYNAME_MONTANTR1 = PROPERTYNAME_MONTANT_PREFIX + R1;
	public static final String PROPERTYNAME_MONTANTR2 = PROPERTYNAME_MONTANT_PREFIX + R2;
	public static final String PROPERTYNAME_MONTANTR3 = PROPERTYNAME_MONTANT_PREFIX + R3;
	public static final String PROPERTYNAME_MONTANTR4 = PROPERTYNAME_MONTANT_PREFIX + R4;
}