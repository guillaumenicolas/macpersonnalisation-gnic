package com.mac.custom.bo;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Date;

import org.hibernate.annotations.AccessType;
import org.joda.time.Days;

import com.netappsid.annotations.Scale;
import com.netappsid.bo.model.Model;
import com.netappsid.datatypes.MonetaryAmount;

public class CustomerLedgerEntry extends Model
{
	private final MonetaryAmount invoiceAmount;
	private final MonetaryAmount current;
	private final MonetaryAmount firstPeriod;
	private final MonetaryAmount secondPeriod;
	private final MonetaryAmount thirdPeriod;
	private final MonetaryAmount fourthPeriod;
	private final String invoiceNumber;
	private final Date invoiceDate;
	private final Date overdueDate;
	private final int numberOfDays;
	private final int numberOfOverdueDays;

	public CustomerLedgerEntry()
	{
		this("", BigDecimal.ZERO, new Date(), new Date(), "");
	}

	public CustomerLedgerEntry(String invoiceNumber, BigDecimal invoiceAmount, Date invoiceDate, Date overdueDate, String currencyCode)
	{
		Currency currentCurrency = Currency.getInstance(currencyCode);
		this.invoiceAmount = new MonetaryAmount(invoiceAmount, currentCurrency);
		this.invoiceNumber = invoiceNumber;
		this.invoiceDate = invoiceDate;
		this.overdueDate = overdueDate;
		this.numberOfDays = Days.daysBetween(new org.joda.time.DateTime(invoiceDate), new org.joda.time.DateTime(new Date())).getDays();
		this.numberOfOverdueDays = Days.daysBetween(new org.joda.time.DateTime(overdueDate), new org.joda.time.DateTime(new Date())).getDays();
		this.current = new MonetaryAmount(numberOfDays <= 30 ? invoiceAmount : BigDecimal.ZERO, currentCurrency);
		this.firstPeriod = new MonetaryAmount(numberOfDays > 30 && numberOfDays <= 60 ? invoiceAmount : BigDecimal.ZERO, currentCurrency);
		this.secondPeriod = new MonetaryAmount(numberOfDays > 60 && numberOfDays <= 90 ? invoiceAmount : BigDecimal.ZERO, currentCurrency);
		this.thirdPeriod = new MonetaryAmount(numberOfDays > 90 && numberOfDays <= 120 ? invoiceAmount : BigDecimal.ZERO, currentCurrency);
		this.fourthPeriod = new MonetaryAmount(numberOfDays > 120 ? invoiceAmount : BigDecimal.ZERO, currentCurrency);
	}

	@Scale(decimal = 2)
	public MonetaryAmount getInvoiceAmount()
	{
		return invoiceAmount;
	}

	@Scale(decimal = 2)
	public MonetaryAmount getCurrent()
	{
		return current;
	}

	@Scale(decimal = 2)
	public MonetaryAmount getFirstPeriod()
	{
		return firstPeriod;
	}

	@Scale(decimal = 2)
	public MonetaryAmount getSecondPeriod()
	{
		return secondPeriod;
	}

	@Scale(decimal = 2)
	@AccessType(value = "field")
	public MonetaryAmount getThirdPeriod()
	{
		return thirdPeriod;
	}

	@Scale(decimal = 2)
	public MonetaryAmount getFourthPeriod()
	{
		return fourthPeriod;
	}

	public String getInvoiceNumber()
	{
		return invoiceNumber;
	}

	public Date getInvoiceDate()
	{
		return invoiceDate;
	}

	public int getNumberOfDays()
	{
		return numberOfDays;
	}

	public int getNumberOfOverdueDays()
	{
		return numberOfOverdueDays;
	}

	public Date getOverdueDate()
	{
		return overdueDate;
	}
}
