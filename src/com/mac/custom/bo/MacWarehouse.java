package com.mac.custom.bo;

import java.util.Collection;

import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.jgoodies.validation.ValidationResult;
import com.mac.custom.bo.base.BaseMacWarehouse;
import com.mac.custom.dao.MacWarehouseDAO;
import com.netappsid.annotations.DAO;
import com.netappsid.resources.Translator;

@javax.persistence.Entity
@DAO(dao = MacWarehouseDAO.class)
@Table(name = "MacWarehouse", schema = "MacSchema")
@PrimaryKeyJoinColumn(name = "id")
public class MacWarehouse extends BaseMacWarehouse
{
	public void addToShippingLocationsFromLocation(MacLocation location)
	{
		MacShippingLocation macShippingLocation = new MacShippingLocation();
		macShippingLocation.setWarehouse(this);
		macShippingLocation.setLocation(location);
		addToShippingLocations(macShippingLocation);
	}

	public void addManyToShippingLocationsFromLocation(Collection<MacLocation> locations)
	{
		addManyToShippingLocations(Collections2.transform(locations, new Function<MacLocation, MacShippingLocation>()
			{
				@Override
				public MacShippingLocation apply(MacLocation location)
				{
					MacShippingLocation macShippingLocation = new MacShippingLocation();
					macShippingLocation.setWarehouse(MacWarehouse.this);
					macShippingLocation.setLocation(location);
					return macShippingLocation;
				}
			}));
	}

	@Override
	public ValidationResult validate()
	{
		ValidationResult validate = super.validate();
		for (MacShippingLocation msl : getShippingLocations())
		{
			if (!getLocations().contains(msl.getLocation()))
			{
				validate.addError(Translator.getString("WarehouseShippingLocationsNotAllInLocations"));
				break;
			}
		}
		return validate;
	}
}
