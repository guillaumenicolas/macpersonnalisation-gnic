package com.mac.custom.bo;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.mac.custom.bo.base.BaseMacWorkflowUnblockManual;
import com.mac.custom.dao.MacWorkflowUnblockManualDAO;
import com.netappsid.annotations.DAO;
import com.netappsid.dao.PersistenceListener;
import com.netappsid.validation.Validable;

@Entity
@EntityListeners(value = { PersistenceListener.class })
@DAO(dao = MacWorkflowUnblockManualDAO.class)
@Table(name = "MacWorkflowUnblockManual", schema = "MacSchema")
@PrimaryKeyJoinColumn(name = "id")
public class MacWorkflowUnblockManual extends BaseMacWorkflowUnblockManual implements Serializable, Validable
{
	
}
