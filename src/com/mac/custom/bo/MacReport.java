package com.mac.custom.bo;

import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.mac.custom.bo.base.BaseMacReport;

@javax.persistence.Entity
@Table(name = "MacReport", schema = "MacSchema")
@PrimaryKeyJoinColumn(name = "id")
public class MacReport extends BaseMacReport
{

}
