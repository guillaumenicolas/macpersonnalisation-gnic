package com.mac.custom.bo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.mac.custom.bo.base.BaseMacStocktakingDetail;
import com.netappsid.datatypes.MonetaryAmount;
import com.netappsid.erp.server.bo.GroupingModel;
import com.netappsid.erp.server.bo.InventoryProduct;
import com.netappsid.erp.server.datatypes.GroupMeasureValue;

@javax.persistence.Entity
@Table(name = "MacStocktakingDetail", schema = "MacSchema")
@PrimaryKeyJoinColumn(name = "id")
public class MacStocktakingDetail extends BaseMacStocktakingDetail
{
	@Transient
	public GroupingModel getPurchaseFamily()
	{
		InventoryProduct inventoryProduct = getInventoryProduct();
		if (inventoryProduct == null)
		{
			return null;
		}
		List<GroupingModel> groupingModels = inventoryProduct.getGroupingModels();
		if (groupingModels == null || groupingModels.size() <= 0)
		{
			return null;
		}
		return groupingModels.get(0);
	}

	@Transient
	public GroupingModel getPurchaseSubFamily()
	{
		InventoryProduct inventoryProduct = getInventoryProduct();
		if (inventoryProduct == null)
		{
			return null;
		}
		List<GroupingModel> groupingModels = inventoryProduct.getGroupingModels();
		if (groupingModels == null || groupingModels.size() <= 1)
		{
			return null;
		}
		return groupingModels.get(1);
	}

	@Transient
	public BigDecimal getDpaStockUnit()
	{
		InventoryProduct inventoryProduct = getInventoryProduct();
		if (inventoryProduct != null)
		{
			MonetaryAmount lastPurchasePrice = inventoryProduct.getLastPurchasePrice();
			if (lastPurchasePrice != null)
			{
				GroupMeasureValue stockQuantity = getStockQuantity();
				if (stockQuantity != null)
				{
					GroupMeasureValue lastPurchasePriceMeasure = inventoryProduct.getLastPurchasePriceMeasure();
					if (lastPurchasePriceMeasure != null)
					{
						GroupMeasureValue stockQtyInLastPurchaseUnit = new GroupMeasureValue(stockQuantity, BigDecimal.ONE).to(lastPurchasePriceMeasure);
						if (stockQtyInLastPurchaseUnit != null)
						{
							return lastPurchasePrice.multiply(stockQtyInLastPurchaseUnit.getValue()).getValue();
						}
					}
				}
			}
		}
		return null;
	}

	@Override
	public void setStockQuantity(GroupMeasureValue stockQuantity)
	{
		super.setStockQuantity(stockQuantity);

		// force gui refresh of computed values
		firePropertyChange(PROPERTYNAME_DPASTOCKUNIT, null, getDpaStockUnit());
		firePropertyChange(PROPERTYNAME_DPASTOCKUNITINDOUBLE, null, getDpaStockUnitInDouble());
		firePropertyChange(PROPERTYNAME_THEORETICAPPRECIATION, null, getTheoreticAppreciation());
		firePropertyChange(PROPERTYNAME_INVENTORYAPPRECIATION, null, getInventoryAppreciation());
		firePropertyChange(PROPERTYNAME_APPRECIATIONGAP, null, getAppreciationGap());
	}

	@Override
	public void setNewQuantity(GroupMeasureValue newQuantity)
	{
		super.setNewQuantity(newQuantity);

		// force gui refresh of computed values
		firePropertyChange(PROPERTYNAME_INVENTORYAPPRECIATION, null, getInventoryAppreciation());
		firePropertyChange(PROPERTYNAME_APPRECIATIONGAP, null, getAppreciationGap());
	}

	@Transient
	public Double getDpaStockUnitInDouble()
	{
		BigDecimal dpaStockUnit = getDpaStockUnit();
		if (dpaStockUnit != null)
		{
			return dpaStockUnit.setScale(2, RoundingMode.HALF_UP).doubleValue();
		}
		return null;
	}

	@Transient
	public Double getTheoreticAppreciation()
	{
		BigDecimal stockQuantity = getStockQuantityValue();
		if (stockQuantity != null)
		{
			BigDecimal dpaStockUnit = getDpaStockUnit();
			if (dpaStockUnit != null)
			{
				return dpaStockUnit.multiply(stockQuantity).setScale(2, RoundingMode.HALF_UP).doubleValue();
			}
		}
		return null;
	}

	@Transient
	public Double getInventoryAppreciation()
	{
		BigDecimal newQuantity = getNewQuantityValue();
		if (newQuantity != null)
		{
			BigDecimal dpaStockUnit = getDpaStockUnit();
			if (dpaStockUnit != null)
			{
				return dpaStockUnit.multiply(newQuantity).setScale(2, RoundingMode.HALF_UP).doubleValue();
			}
		}
		return null;
	}

	@Transient
	public Double getAppreciationGap()
	{
		Double theoreticAppreciation = getTheoreticAppreciation();
		if (theoreticAppreciation != null)
		{
			Double inventoryAppreciation = getInventoryAppreciation();
			if (inventoryAppreciation != null)
			{
				return theoreticAppreciation - inventoryAppreciation;
			}
		}
		return null;
	}

	public static final String PROPERTYNAME_APPRECIATIONGAP = "appreciationGap";
	public static final String PROPERTYNAME_INVENTORYAPPRECIATION = "inventoryAppreciation";
	public static final String PROPERTYNAME_THEORETICAPPRECIATION = "theoreticAppreciation";
	public static final String PROPERTYNAME_DPASTOCKUNIT = "dpaStockUnit";
	public static final String PROPERTYNAME_DPASTOCKUNITINDOUBLE = "dpaStockUnitInDouble";
}
