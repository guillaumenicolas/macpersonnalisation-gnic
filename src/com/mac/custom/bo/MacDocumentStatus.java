package com.mac.custom.bo;

import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.mac.custom.bo.base.BaseMacDocumentStatus;
import com.mac.custom.dao.MacDocumentStatusDAO;
import com.netappsid.annotations.DAO;

@javax.persistence.Entity
@Table(name = "MacDocumentStatus", schema = "MacSchema")
@PrimaryKeyJoinColumn(name = "id")
@DAO(dao = MacDocumentStatusDAO.class)
public class MacDocumentStatus extends BaseMacDocumentStatus
{
	@Transient
	public boolean isSaveFirstCommunicatedShippingDateRequired()
	{
		return "ARCE".equals(getUpperCode());
	}
}
