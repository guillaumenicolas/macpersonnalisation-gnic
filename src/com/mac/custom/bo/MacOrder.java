package com.mac.custom.bo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.PredicateUtils;

import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.message.SimpleValidationMessage;
import com.mac.custom.bo.base.BaseMacOrder;
import com.mac.custom.bo.predicate.ProductionClosedItemToShipNotPlannedNotShippedNotCanceledNotPackagesPredicate;
import com.mac.custom.enums.SaleTypeEnum;
import com.mac.custom.utils.PostponedReasonsUtils;
import com.mac.custom.workflow.WorkflowUtils;
import com.netappsid.bo.model.Model;
import com.netappsid.erp.client.utils.ERPServicesManager;
import com.netappsid.erp.server.bo.BlockingHistory;
import com.netappsid.erp.server.bo.BlockingReason;
import com.netappsid.erp.server.bo.DocumentStatus;
import com.netappsid.erp.server.bo.ItemToShip;
import com.netappsid.erp.server.bo.MaterialProduct;
import com.netappsid.erp.server.bo.Note;
import com.netappsid.erp.server.bo.Order;
import com.netappsid.erp.server.bo.OrderDetail;
import com.netappsid.erp.server.bo.Quotation;
import com.netappsid.erp.server.bo.TransactionDetail;
import com.netappsid.erp.server.services.utils.SetupUtils;
import com.netappsid.europe.naid.custom.bo.OrderDetailEuro;
import com.netappsid.europe.naid.custom.bo.PostponedReason;
import com.netappsid.framework.utils.ObservableCollectionUtils;
import com.netappsid.observable.ObservableCollections;
import com.netappsid.resources.Translator;
import com.netappsid.transactional.TransactionalList;

@javax.persistence.Entity
@Table(name = "MacOrder", schema = "MacSchema")
@PrimaryKeyJoinColumn(name = "id")
public class MacOrder extends BaseMacOrder
{
	public static final String AUTOMATIC_DATE_POSTPONED_REASON_CODE = "UO";
	private final static String TYPE_NOTE_COMPTA = "COMPTA";
	private final static String BLOCKINGREASON_DEBLOQUEE_FINANCE = "BF-D";

	private boolean resetingShippingDate = false;
	private Boolean modeDateExpSpec = null;
	private List<PostponedReason> availablePostponedReasons;
	private boolean originalDocumentStatusInitialized;
	private MacDocumentStatus originalDocumentStatus;

	@Override
	public ValidationResult validate()
	{
		ValidationResult vr = super.validate();

		MacWorkflowMaster wfMaster = WorkflowUtils.getWFMaster();
		//
		// if(wfMaster != null && wfMaster.getWfM5P() != null && wfMaster.getWfM5P().isActive())
		// {
		// if(getDocumentStatus() != null && getdocDocumentStatus().hasSameID(wfMaster.getWfM5P().getM5pStatus()))
		// {
		// vr.add(new SimpleValidationMessage(Translator.getString("cannotSaveWhenM5P"), Severity.ERROR));
		// }
		// }

		if (getSaleType() != null && getSaleType().getCode() != null && getSaleType().getUpperCode().equals(SaleTypeEnum.sav.getUpperCode()))
		{
			if (getServiceType() == null || getServiceType().getCode() == null || getServiceType().getCode().equals(""))
			{
				vr.add(new SimpleValidationMessage(Translator.getString("cannotSaveWhenSAVServiceTypeNotCompleted", Severity.ERROR)));
			}
			if (getOriginalOrderNumber() == null || getOriginalOrderNumber().equals(""))
			{
				vr.add(new SimpleValidationMessage(Translator.getString("cannotSaveWhenSAVOriginalOrderNumberNotCompleted", Severity.ERROR)));
			}
		}
		if (getDocumentStatus() != null && getDocumentStatus().getUpperCode().equals("DVAL"))
		{
			if (isBlockRareBird())
			{
				vr.add(new SimpleValidationMessage(Translator.getString("delaiValideStatusCannotBeUseBecauseOrderContainsNotConfirmedRareBirdDetails",
						Severity.ERROR)));
			}
			else if (!orderHasAllShippingDates())
			{
				vr.add(new SimpleValidationMessage(Translator.getString("delaiValideStatusCannotBeUseBecauseOrderOrDetailsHasNoShippingDate", Severity.ERROR)));
			}
		}

		// If we are canceling an order, make sure all the details can be cancelled.
		if (DocumentStatus.CANCEL.equals(getDocumentStatus()))
		{
			for (OrderDetail detail : getDetails())
			{
				if (!detail.isCancel())
				{
					ValidationResult validateCancellation = detail.validateCancellation();
					if (!validateCancellation.isEmpty())
					{
						vr.addAllFrom(validateCancellation);
					}
				}
			}
		}

		return vr;
	}

	private boolean orderHasAllShippingDates()
	{
		if (getShippingDate() == null)
		{
			return false;
		}

		for (OrderDetail orderDetail : getDetails())
		{
			if (!orderDetail.isCancel() && orderDetail.getShippingDate() == null)
			{
				return false;
			}
		}

		return true;
	}

	@Override
	public void refreshDocumentStatusFlow()
	{
		List<DocumentStatus> newDocumentStatusFlow;
		if (getCompany() != null)
		{
			newDocumentStatusFlow = getDocumentServicesLocator().get().getDocumentStatusFlow(getCompany().getId(), Order.class, getDocumentStatus());
		}
		else
		{
			newDocumentStatusFlow = new ArrayList<DocumentStatus>();
		}

		ObservableCollectionUtils.clearAndAddAll(getDocumentStatusFlow(), newDocumentStatusFlow);
	}

	@Override
	public boolean isTransactionDetailSeverityOkForSave(TransactionDetail detail)
	{
		MacWorkflowMaster wfMaster = WorkflowUtils.getWFMaster();

		if (detail.containsConfiguration() && detail.getConfiguratorValidationSeverity().equals(com.netappsid.erp.Severity.ERROR))
		{
			if (wfMaster != null && wfMaster.getWfM5P() != null && wfMaster.getWfM5P().isActive())
			{
				if (detail instanceof OrderDetailEuro && ((OrderDetailEuro) detail).isRareBird())
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}

		return true;
	}

	@Override
	public List<String> getDetailFieldExclude(TransactionDetail detail, boolean keepDiscount)
	{
		List<String> fieldExclude = super.getDetailFieldExclude(detail, keepDiscount);

		fieldExclude.add(MacOrderDetail.PROPERTYNAME_WORKLOADDETAILS);

		return fieldExclude;
	}

	@Override
	protected void onDetailsCollectionChange(TransactionalList<OrderDetail> details)
	{
		super.onDetailsCollectionChange(details);

		// Whenever a detail is added/removed, we need to reset the shipping date if the order is in ASAP mode. A flag has been added to prevent a dialog
		// prompting to reciprocate the shipping date to the details, since it wouldn't make sense in this context.
		if (isShipASAP())
		{
			setResetingShippingDate(true);
			setShippingDate(null);
			setResetingShippingDate(false);
		}
	}

	@Transient
	public boolean isResetingShippingDate()
	{
		return resetingShippingDate;
	}

	public void setResetingShippingDate(boolean resetingShippingDate)
	{
		this.resetingShippingDate = resetingShippingDate;
	}

	@Override
	public void beforeSaving()
	{
		super.beforeSaving();

		Quotation quotation = getQuotation();
		if (quotation != null)
		{
			if (quotation.getNewStatusHistory() != null && SetupUtils.INSTANCE.getMainSetup().getAutomaticServiceUser() != null)
			{
				quotation.getNewStatusHistory().setUser(SetupUtils.INSTANCE.getMainSetup().getAutomaticServiceUser());
			}
			quotation.setDocumentStatus(ERPServicesManager.getDocumentStatusServices().getInternalDocumentStatus(DocumentStatus.CLOSE));
		}
	}

	@Transient
	public MacCustomer getMacCustomer()
	{
		return (MacCustomer) Model.getTarget(getCustomer());
	}

	@Transient
	public MacDocumentStatus getMacDocumentStatus()
	{
		return (MacDocumentStatus) Model.getTarget(getDocumentStatus());
	}

	@Transient
	public BlockingHistory getLastBlockingHistory()
	{
		if (getBlockingHistories() != null)
		{
			if (getBlockingHistories().size() > 0)
			{
				return Model.getTarget(getBlockingHistories().get(getBlockingHistories().size() - 1));
			}
			else
			{
				return null;
			}
		}
		else
		{
			return null;
		}
	}

	@Transient
	public BlockingReason getLastBlockingHistoryBlockingReason()
	{
		if (getBlockingHistories() != null)
		{
			if (getBlockingHistories().size() > 0)
			{
				return Model.getTarget(getBlockingHistories().get(getBlockingHistories().size() - 1).getBlockingReason());
			}
			else
			{
				return null;
			}
		}
		else
		{
			return null;
		}
	}

	@Transient
	public Boolean getUnblockedProductionAndShipping()
	{
		if (getBlockingHistories() != null)
		{
			if (getBlockingHistories().size() > 0)
			{
				BlockingReason br = Model.getTarget(getBlockingHistories().get(getBlockingHistories().size() - 1).getBlockingReason());
				if (br != null)
				{
					if (br.getCode() != null)
					{
						if (br.getCode().equals(BLOCKINGREASON_DEBLOQUEE_FINANCE))
						{
							boolean debloqueeProd = (getBlockProduction() == null || getBlockProduction() == false);
							boolean debloqueeExped = (getBlockShipping() == null || getBlockShipping() == false);
							if (debloqueeProd && debloqueeExped)
							{
								return Boolean.TRUE;
							}
						}
					}
				}
			}
		}
		return Boolean.FALSE;
	}

	// Ajout dans liste d'un booleen pour Faire apparaitre les commandes
	// avec date specifique.
	@Transient
	public Boolean getModeDateExpSpec()
	{
		Boolean modeDateExpSpec = true;

		if (isShipASAP() || isShipToConfirm())
		{
			modeDateExpSpec = false;
		}

		return modeDateExpSpec;
	}

	// Gestion du critere de recherche "Mode Date Expedition specifique"
	public void setModeDateExpSpec(Boolean modeDateExpSpec)
	{
		Boolean oldValue = getModeDateExpSpec();
		this.modeDateExpSpec = modeDateExpSpec;
		firePropertyChange(PROPERTYNAME_MODEDATEEXPSPEC, oldValue, modeDateExpSpec);
	}

	/**
	 * Returns all postponed reasons except for the automatic date one, which is a system-defined one. The list is being sorted prior to being returned.
	 * 
	 * @return
	 */
	@Transient
	public List<PostponedReason> getAvailablePostponedReasons()
	{
		if (availablePostponedReasons == null)
		{
			availablePostponedReasons = ObservableCollections.newObservableArrayList();

			PostponedReasonsUtils postponedReasonsUtils = new PostponedReasonsUtils();
			availablePostponedReasons.addAll(postponedReasonsUtils.getAvailablePostponedReasons());
		}

		return availablePostponedReasons;
	}

	@Transient
	@Override
	public List<String> getOrderFieldExclude()
	{
		List<String> excludedFields = super.getOrderFieldExclude();
		excludedFields.add(PROPERTYNAME_FIRSTCOMMUNICATEDSHIPPINGDATE);
		return excludedFields;
	}

	@Transient
	public MacDocumentStatus getOriginalDocumentStatus()
	{
		updateOriginalDocumentStatus();
		return originalDocumentStatus;
	}

	@Override
	public void setDocumentStatus(DocumentStatus documentStatus)
	{
		updateOriginalDocumentStatus();
		super.setDocumentStatus(documentStatus);
	}

	private void updateOriginalDocumentStatus()
	{
		if (!originalDocumentStatusInitialized)
		{
			originalDocumentStatusInitialized = true;
			originalDocumentStatus = getMacDocumentStatus();
		}
	}

	@Transient
	public List<ItemToShip> getItemsToShipNotPlannedNotShippedNotCanceledAndMaterial()
	{
		setItemToShipPredicate(new ProductionClosedItemToShipNotPlannedNotShippedNotCanceledNotPackagesPredicate());
		List<ItemToShip> itemToShips = new ArrayList<ItemToShip>();
		itemToShips.addAll(getItemsToShipNotPlannedNotShippedAndNotCanceled());

		for (ItemToShip itemToShip : getItemsToShip())
		{
			// Ajout des ITS qui sont des materiaux
			if (itemToShip.getInventoryProduct() != null
					&& MaterialProduct.class.isAssignableFrom(Model.getTarget(itemToShip.getInventoryProduct()).getClass())
					&& !CollectionUtils.exists(itemToShips, PredicateUtils.identityPredicate(itemToShip)))
			{
				itemToShips.add(itemToShip);
			}
		}

		return itemToShips;
	}

	@Transient
	public String getNoteCompta()
	{
		for (Note note : getNotes())
		{
			if (note.getTypeNote() != null)
			{
				if (note.getTypeNote().getCode().equals(TYPE_NOTE_COMPTA))
				{
					return note.getLocalizedNote();
				}
			}
		}

		return null;
	}

	public static final String PROPERTYNAME_MODEDATEEXPSPEC = "modeDateExpSpec";
	public static final String PROPERTYNAME_LASTBLOCKINGHISTORY = "lastBlockingHistory";
	public static final String PROPERTYNAME_UNBLOCKEDPRODUCTIONANDSHIPPING = "unblockedProductionAndShipping";

}
