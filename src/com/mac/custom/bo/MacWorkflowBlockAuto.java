package com.mac.custom.bo;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.mac.custom.bo.base.BaseMacWorkflowBlockAuto;
import com.mac.custom.dao.MacWorkflowBlockAutoDAO;
import com.netappsid.annotations.DAO;
import com.netappsid.dao.PersistenceListener;
import com.netappsid.validation.Validable;

@Entity
@EntityListeners(value = { PersistenceListener.class })
@DAO(dao = MacWorkflowBlockAutoDAO.class)
@Table(name = "MacWorkflowBlockAuto", schema = "MacSchema")
@PrimaryKeyJoinColumn(name = "id")
public class MacWorkflowBlockAuto extends BaseMacWorkflowBlockAuto implements Serializable, Validable
{
	
}
