package com.mac.custom.bo;

import com.mac.custom.bo.base.BaseManufacturedProductManagementAccounting;
import com.mac.custom.dao.ManufacturedProductManagementAccountingDAO;
import com.netappsid.annotations.DAO;
import com.netappsid.dao.PersistenceListener;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "standard")
@DAO(dao = ManufacturedProductManagementAccountingDAO.class)
@EntityListeners(value = { PersistenceListener.class })
@Table(name = "ManufacturedProductManagementAccounting", schema = "MacSchema")
public class ManufacturedProductManagementAccounting extends BaseManufacturedProductManagementAccounting implements Serializable
{
}