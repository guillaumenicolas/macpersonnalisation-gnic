package com.mac.custom.bo;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.netappsid.bo.model.LocalizedString;

@Entity
@Table(name = "ActiviteClientDescription", schema = "MacSchema", uniqueConstraints = { @UniqueConstraint(columnNames = { "activiteClient_id", "languageCode" }) })
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "standard")
public class ActiviteClientDescription extends LocalizedString
{
	private Serializable id;
	private String languageCode;
	private String value;
	private ActiviteClient activiteClient;

	/**
	 * Default constructor used by Hibernate to construct the object
	 */
	public ActiviteClientDescription()
	{}

	/**
	 * This constructor is used by the BO to set the data
	 */
	public ActiviteClientDescription(String languageCode, String value)
	{
		this.languageCode = languageCode;
		this.value = value;
	}

	@Id
	@GeneratedValue(generator = "naid-uuid")
	@GenericGenerator(name = "naid-uuid", strategy = "com.netappsid.framework.utils.UUIDGenerator")
	@Type(type = "com.netappsid.datatypes.UUIDUserType")
	public Serializable getId()
	{
		return id;
	}

	public void setId(Serializable id)
	{
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public ActiviteClient getActiviteClient()
	{
		return activiteClient;
	}

	public void setActiviteClient(ActiviteClient activiteClient)
	{
		this.activiteClient = activiteClient;
	}

	public String getLanguageCode()
	{
		return languageCode;
	}

	public void setLanguageCode(String languageCode)
	{
		this.languageCode = languageCode;
	}

	public String getValue()
	{
		return value;
	}

	public void setValue(String value)
	{
		this.value = value;
	}

	public static final String PROPERTYNAME_ACTIVITECLIENT = "activiteClient"; //$NON-NLS-1$

}
