package com.mac.custom.bo;

import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.mac.custom.bo.base.BaseMacTerm;
import com.mac.custom.dao.MacTermDAO;
import com.netappsid.annotations.DAO;

@javax.persistence.Entity
@DAO(dao = MacTermDAO.class)
@Table(name = "MacTerm", schema = "MacSchema")
@PrimaryKeyJoinColumn(name = "id")
public class MacTerm extends BaseMacTerm
{
}