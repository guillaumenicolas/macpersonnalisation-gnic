package com.mac.custom.bo;

import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.mac.custom.bo.base.BaseMacPackages;

@javax.persistence.Entity
@Table(name = "MacPackages", schema = "MacSchema")
@PrimaryKeyJoinColumn(name = "id")
public class MacPackages extends BaseMacPackages
{

}
