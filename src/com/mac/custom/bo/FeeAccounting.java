package com.mac.custom.bo;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.message.SimpleValidationMessage;
import com.mac.custom.bo.base.BaseFeeAccounting;
import com.mac.custom.dao.FeeAccountingDAO;
import com.netappsid.annotations.DAO;
import com.netappsid.dao.PersistenceListener;
import com.netappsid.resources.Translator;

@DAO(dao = FeeAccountingDAO.class)
@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "standard")
@EntityListeners(value = { PersistenceListener.class })
@Table(name = "FeeAccounting", schema = "MacSchema")
public class FeeAccounting extends BaseFeeAccounting
{
	@Override
	public ValidationResult validate()
	{
		ValidationResult result = super.validate();

		List<FeeManagementAccounting> managementAccountings = getFeeManagementAccountings();
		for (int i = 0; i < managementAccountings.size() - 1; i++)
		{
			for (int j = i + 1; j < managementAccountings.size(); j++)
			{
				if (managementAccountings.get(i).getCompany().getCode().equals(managementAccountings.get(j).getCompany().getCode()))
				{
					result.add(new SimpleValidationMessage(String.format(Translator.getString("managementAccountingMoreThanOneEntryForSameCompany"),
							managementAccountings.get(i).getCompany().getLocalizedDescription()), Severity.ERROR));
					break;
				}
			}
		}

		return result;
	}
}
