package com.mac.custom.bo;

import java.util.List;

import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.Hibernate;

import com.mac.custom.bo.base.BaseMacReceivingDetail;
import com.netappsid.bo.model.Model;
import com.netappsid.erp.server.bo.AccountingSegment;
import com.netappsid.erp.server.bo.Auxiliary;
import com.netappsid.erp.server.bo.AuxiliaryGeneralLedgerType;
import com.netappsid.erp.server.bo.CommercialEntity;
import com.netappsid.erp.server.bo.GeneralLedgerDefinition;
import com.netappsid.erp.server.bo.GeneralLedgerDefinitionType;
import com.netappsid.erp.server.bo.InventoryProduct;
import com.netappsid.erp.server.bo.ItemToReceive;
import com.netappsid.erp.server.bo.Product;
import com.netappsid.erp.server.bo.ProductAuxiliaryGeneralLedger;
import com.netappsid.erp.server.bo.PurchaseOrderDetailToReceive;

@javax.persistence.Entity
@Table(name = "MacReceivingDetail", schema = "MacSchema")
@PrimaryKeyJoinColumn(name = "id")
public class MacReceivingDetail extends BaseMacReceivingDetail
{
	public static final String SEGMENTATIONACHAT = "Achats";
	public static final String SEGMENTATIONCONTROLRECEPTION = "Contr\u00f4le-r\u00e9ception";

	@Transient
	public AccountingSegment getSegmentationAchat()
	{
		return retrieveSegmentation(SEGMENTATIONACHAT);
	}

	@Transient
	public AccountingSegment getSegmentationControlReception()
	{
		return retrieveSegmentation(SEGMENTATIONCONTROLRECEPTION);
	}

	private AccountingSegment retrieveSegmentation(String segString)
	{
		CommercialEntity supplier = getReceiving().getCommercialEntity();
		Auxiliary auxiliary = supplier.getAuxiliary(getReceiving().getCompany());
		GeneralLedgerDefinition achatGeneralLedgerDefinition;
		GeneralLedgerDefinition controlReceptionGeneralLedgerDefinition;
		InventoryProduct inventoryProduct = getItemToReceive().getInventoryProduct();

		List<ProductAuxiliaryGeneralLedger> productAuxiliaryGeneralLedgerList = inventoryProduct.getProductAuxiliaryGeneralLedgers();
		for (ProductAuxiliaryGeneralLedger productAuxiliaryGeneralLedger : productAuxiliaryGeneralLedgerList)
		{
			if (productAuxiliaryGeneralLedger.getAuxiliary().equals(auxiliary))
			{
				List<AuxiliaryGeneralLedgerType> segmentationTypes = productAuxiliaryGeneralLedger.getAuxiliaryGeneralLedgerTypes();
				// pour chaque type de segmentation d'un auxiliaire
				for (AuxiliaryGeneralLedgerType auxiliaryGeneralLedgerType : segmentationTypes)
				{
					// on r�cup�re le type de segmentation et on cherche la segmentation d'achat et de controle reception
					GeneralLedgerDefinitionType generalLedgerDefinitionType = auxiliaryGeneralLedgerType.getGeneralLedgerDefinitionType();
					if (generalLedgerDefinitionType != null)
					{
						if (generalLedgerDefinitionType.getLocalizedDescription().equals(SEGMENTATIONACHAT) && segString.equals(SEGMENTATIONACHAT))
						{
							achatGeneralLedgerDefinition = auxiliaryGeneralLedgerType.getGeneralLedgerDefinition();
							return achatGeneralLedgerDefinition.getSegment2();
						}
						if (generalLedgerDefinitionType.getLocalizedDescription().equals(SEGMENTATIONCONTROLRECEPTION)
								&& segString.equals(SEGMENTATIONCONTROLRECEPTION))
						{
							controlReceptionGeneralLedgerDefinition = auxiliaryGeneralLedgerType.getGeneralLedgerDefinition();
							return controlReceptionGeneralLedgerDefinition.getSegment2();
						}
					}
				}
			}
		}

		return null;
	}

	@Transient
	public CostCode getComptaAnalytiqueCodeCoutAchat()
	{
		Product product = Model.getTarget(getItemToReceive().getInventoryProduct());

		if (product.getClass().isAssignableFrom(MacManufacturedProduct.class))
		{
			List<ManufacturedProductManagementAccounting> managementAccountings = ((MacManufacturedProduct) product).getManagementAccountings();
			for (int i = 0; i < managementAccountings.size(); i++)
			{
				if (managementAccountings.get(i).getCompany().getCode().equals(getReceiving().getCompany().getCode()))
				{
					return managementAccountings.get(i).getCostCodePurchase();
				}
			}
		}
		else if (product.getClass().isAssignableFrom(MacMaterialProduct.class))
		{
			List<MaterialProductManagementAccounting> managementAccountings = ((MacMaterialProduct) product).getManagementAccountings();
			for (int i = 0; i < managementAccountings.size(); i++)
			{
				if (managementAccountings.get(i).getCompany().getCode().equals(getReceiving().getCompany().getCode()))
				{
					return managementAccountings.get(i).getCostCodePurchase();
				}
			}
		}
		return null;
	}

	@Transient
	public ProductCodeAnalytic getComptaAnalytiqueCodeProduitAchat()
	{
		Product product = Model.getTarget(getItemToReceive().getInventoryProduct());

		if (product.getClass().isAssignableFrom(MacManufacturedProduct.class))
		{
			List<ManufacturedProductManagementAccounting> managementAccountings = ((MacManufacturedProduct) product).getManagementAccountings();
			for (int i = 0; i < managementAccountings.size(); i++)
			{
				if (managementAccountings.get(i).getCompany().getCode().equals(getReceiving().getCompany().getCode()))
				{
					return managementAccountings.get(i).getProductCodeAnalyticPurchase();
				}
			}
		}
		else if (product.getClass().isAssignableFrom(MacMaterialProduct.class))
		{

			List<MaterialProductManagementAccounting> managementAccountings = ((MacMaterialProduct) product).getManagementAccountings();
			for (int i = 0; i < managementAccountings.size(); i++)
			{
				if (managementAccountings.get(i).getCompany().getCode().equals(getReceiving().getCompany().getCode()))
				{
					return managementAccountings.get(i).getProductCodeAnalyticPurchase();
				}
			}
		}
		return null;
	}

	@Transient
	public String getCatalogNumber()
	{
		String catalogNumber = "";
		ItemToReceive itemToReceive = Model.getTarget(getItemToReceive());

		if (PurchaseOrderDetailToReceive.class.isAssignableFrom(Hibernate.getClass(itemToReceive)))
		{
			PurchaseOrderDetailToReceive purchaseOrderDetailToReceive = (PurchaseOrderDetailToReceive) itemToReceive;
			if (purchaseOrderDetailToReceive != null && purchaseOrderDetailToReceive.getPurchaseOrderDetail() != null)
			{
				catalogNumber = purchaseOrderDetailToReceive.getPurchaseOrderDetail().getCatalogNumber();
			}
		}

		return catalogNumber;

	}

	public static final String PROPERTYNAME_SEGMENTATIONACHAT = "segmentationAchat";
	public static final String PROPERTYNAME_SEGMENTATIONCONTROLERECEPTION = "segmentationControlReception";
	public static final String PROPERTYNAME_COMPTACODECOUTACHAT = "comptaAnalytiqueCodeCoutAchat";
	public static final String PROPERTYNAME_COMPTACODEPRODUITACHAT = "comptaAnalytiqueCodeProduitAchat";
}