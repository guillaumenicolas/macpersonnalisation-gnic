package com.mac.custom.bo;

import java.util.List;

import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.jgoodies.validation.ValidationResult;
import com.mac.custom.bo.base.BaseMacLocation;
import com.netappsid.erp.server.bo.Location;
import com.netappsid.resources.Translator;

@javax.persistence.Entity
@Table(name = "MacLocation", schema = "MacSchema")
@PrimaryKeyJoinColumn(name = "id")
public class MacLocation extends BaseMacLocation
{
	public void setDefaultSubLocation(MacLocation defaultLocation)
	{
		List<Location> subLocations = getSubLocations();
		if (subLocations != null && subLocations.contains(defaultLocation))
		{
			subLocations.remove(defaultLocation);
			subLocations.add(0, defaultLocation);
		}
	}

	@Transient
	public MacLocation getDefaultSubLocation()
	{
		if (getSubLocations().isEmpty())
		{
			return null;
		}
		return (MacLocation) getSubLocations().get(0);
	}

	@Override
	public ValidationResult validate()
	{
		final ValidationResult validate = super.validate();

		if (isStore())
		{
			if (isIncludeInMRPCalculation())
			{
				validate.addError(Translator.getString("locationFormChangeStore.error.includeInMRPCalculation"));
			}
			if (isQuarantine())
			{
				validate.addError(Translator.getString("locationFormChangeStore.error.quarantine"));
			}
		}
		else
		{
			if (!getSubLocations().isEmpty())
			{
				validate.addError(Translator.getString("locationFormChangeStore.error.subLocations"));
			}
		}

		return validate;
	}
}
