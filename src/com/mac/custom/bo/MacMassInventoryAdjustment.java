package com.mac.custom.bo;

import com.netappsid.erp.server.bo.InventoryProduct;
import com.netappsid.erp.server.datatypes.GroupMeasureValue;
import com.netappsid.erp.server.model.MassInventoryAdjustment;

public class MacMassInventoryAdjustment extends MassInventoryAdjustment
{
	@Override
	public void setInventoryProduct(InventoryProduct inventoryProduct)
	{
		super.setInventoryProduct(inventoryProduct);
		if (inventoryProduct != null && inventoryProduct.getInventoryMeasure() != null)
		{
			setQuantity(new GroupMeasureValue(inventoryProduct.getInventoryMeasureGroupMeasure(), null, inventoryProduct.getDefaultUnitConversion(),
					inventoryProduct.getDefaultConvertedUnit()));
		}
	}
}
