package com.mac.custom.bo;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Transient;

import com.mac.custom.bo.base.BaseWorkCalendarDetail;
import com.mac.custom.services.workLoad.beans.WorkLoadServicesBean;
import com.mac.custom.services.workLoad.interfaces.WorkLoadServices;
import com.netappsid.erp.server.datatypes.GroupMeasureValue;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.observable.ObservableCollections;
import com.netappsid.observable.ObservableList;

public class WorkCalendarDetail extends BaseWorkCalendarDetail
{
	private final ServiceLocator<WorkLoadServices> workLoadServicesLocator = new ServiceLocator<WorkLoadServices>(WorkLoadServicesBean.class);
	private final ObservableList<WorkLoadDetail> workLoadDetails = ObservableCollections.newObservableArrayList();
	private boolean workLoadDetailsInitialized;

	@Transient
	public List<WorkLoadDetail> getWorkLoadDetails()
	{
		if (!workLoadDetailsInitialized)
		{
			workLoadDetails.addAll(workLoadServicesLocator.get().getDetails(getWorkLoad()));
			workLoadDetailsInitialized = true;
		}

		return workLoadDetails;
	}

	@Override
	public void setWorkLoad(WorkLoad workLoad)
	{
		workLoadDetailsInitialized = false;
		workLoadDetails.clear();

		super.setWorkLoad(workLoad);
	}

	@Transient
	public GroupMeasureValue getAvailableQuantity()
	{
		GroupMeasureValue availableQuantity = getAbsoluteAvailableQuantity();

		if (availableQuantity != null)
		{
			if (!availableQuantity.isGreaterThanOrEqualsZero())
			{
				availableQuantity.setValue(BigDecimal.ZERO);
			}
		}

		return availableQuantity;
	}

	@Transient
	public boolean isLoadCompleted()
	{
		return getAbsoluteAvailableQuantity() != null && !getAbsoluteAvailableQuantity().isGreaterThanZero();
	}

	@Transient
	public boolean isOverLoad()
	{
		return getAbsoluteAvailableQuantity() != null && !getAbsoluteAvailableQuantity().isGreaterThanOrEqualsZero();
	}

	@Transient
	private GroupMeasureValue getAbsoluteAvailableQuantity()
	{
		GroupMeasureValue absoluteQuantity = null;
		GroupMeasureValue resourceQuantity = getResourceEfficiency().getActualTimeMeasure();
		GroupMeasureValue loadQuantity = getWorkLoad().getQuantityAvailable();

		if (resourceQuantity != null && resourceQuantity.isValid())
		{
			absoluteQuantity = new GroupMeasureValue(resourceQuantity);

			if (loadQuantity != null && loadQuantity.isValid())
			{
				absoluteQuantity = absoluteQuantity.substract(loadQuantity);
			}
		}

		return absoluteQuantity;
	}
}
