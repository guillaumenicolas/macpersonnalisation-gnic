package com.mac.custom.bo;

import com.mac.custom.bo.base.BaseCostCodeDetail;
import com.mac.custom.dao.CostCodeDetailDAO;
import com.netappsid.annotations.DAO;
import com.netappsid.dao.PersistenceListener;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "standard")
@DAO(dao = CostCodeDetailDAO.class)
@EntityListeners(value = { PersistenceListener.class })
@Table(name = "CostCodeDetail", schema = "MacSchema", uniqueConstraints = { @UniqueConstraint(columnNames = { "code" }),
		@UniqueConstraint(columnNames = { "upperCode" }) })
public class CostCodeDetail extends BaseCostCodeDetail
{
}