package com.mac.custom.bo;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.mac.custom.bo.base.BaseMacWorkflowUnblockAuto;
import com.mac.custom.dao.MacWorkflowUnblockAutoDAO;
import com.netappsid.annotations.DAO;
import com.netappsid.dao.PersistenceListener;
import com.netappsid.validation.Validable;

@Entity
@EntityListeners(value = { PersistenceListener.class })
@DAO(dao = MacWorkflowUnblockAutoDAO.class)
@Table(name = "MacWorkflowUnblockAuto", schema = "MacSchema")
@PrimaryKeyJoinColumn(name = "id")
public class MacWorkflowUnblockAuto extends BaseMacWorkflowUnblockAuto implements Serializable, Validable
{
	
}
