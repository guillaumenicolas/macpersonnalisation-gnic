package com.mac.custom.bo;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.message.SimpleValidationMessage;
import com.mac.custom.bo.base.BaseMacWorkflowMaster;
import com.mac.custom.dao.MacWorkflowMasterDAO;
import com.netappsid.annotations.DAO;
import com.netappsid.dao.PersistenceListener;
import com.netappsid.erp.server.bo.User;
import com.netappsid.resources.Translator;
import com.netappsid.validation.Validable;

@Entity
@EntityListeners(value = { PersistenceListener.class })
@DAO(dao = MacWorkflowMasterDAO.class)
@Table(name = "MacWorkflowMaster", schema = "MacSchema")
@PrimaryKeyJoinColumn(name = "id")
public class MacWorkflowMaster extends BaseMacWorkflowMaster implements Serializable, Validable
{
	@Override
	public ValidationResult validate() 
	{
		ValidationResult result = super.validate();
		
		//MASTER
		if(getUserWF() == null)
		{
			result.add(new SimpleValidationMessage(Translator.getString("userWF") + Translator.getString("IsMandatory"), Severity.ERROR));
		}
		
		if(getSysAdmin() == null)
		{
			result.add(new SimpleValidationMessage(Translator.getString("sysAdmin") + Translator.getString("IsMandatory"), Severity.ERROR));
		}
		
		//ARC
		if(getWfARC() != null && getWfARC().getApprovalDelay() == null)
		{
			result.add(new SimpleValidationMessage(Translator.getString("approvalDelay") + Translator.getString("IsMandatory"), Severity.ERROR)); 
		}
		
		if(getWfARC() != null && getWfARC().getArcFilePath() == null)
		{
			result.add(new SimpleValidationMessage(Translator.getString("arcFilePath") + Translator.getString("IsMandatory"), Severity.ERROR)); 
		}
		
		if(getWfARC() != null && getWfARC().getArcSentStatus() == null)
		{
			result.add(new SimpleValidationMessage(Translator.getString("arcSentStatus") + Translator.getString("IsMandatory"), Severity.ERROR)); 
		}
		
		if(getWfARC() != null && getWfARC().getInitialDelayStatus() == null)
		{
			result.add(new SimpleValidationMessage(Translator.getString("initialDelayStatus") + Translator.getString("IsMandatory"), Severity.ERROR)); 
		}
		
		if(getWfARC() != null && getWfARC().getArcFilePathLocal() == null)
		{
			result.add(new SimpleValidationMessage(Translator.getString("arcFilePathLocal") + Translator.getString("IsMandatory"), Severity.ERROR)); 
		}
		
		if(getWfARC() != null && getWfARC().getEmailOrigin() == null)
		{
			result.add(new SimpleValidationMessage(Translator.getString("emailOrigin") + Translator.getString("IsMandatory"), Severity.ERROR)); 
		}
		
		//M5P
		if(getWfM5P() != null && getWfM5P().getBlockedStatus() == null)
		{
			result.add(new SimpleValidationMessage(Translator.getString("blockedStatus") + Translator.getString("IsMandatory"), Severity.ERROR)); 
		}
		
		if(getWfM5P() != null && getWfM5P().getEmailAddressBE() == null)
		{
			result.add(new SimpleValidationMessage(Translator.getString("emailAddressBE") + Translator.getString("IsMandatory"), Severity.ERROR)); 
		}
		
		if(getWfM5P() != null && getWfM5P().getM5pStatus() == null)
		{
			result.add(new SimpleValidationMessage(Translator.getString("m5pStatus") + Translator.getString("IsMandatory"), Severity.ERROR)); 
		}
		
		if(getWfM5P() != null && getWfM5P().getUnblockedStatus() == null)
		{
			result.add(new SimpleValidationMessage(Translator.getString("unblockedStatus") + Translator.getString("IsMandatory"), Severity.ERROR)); 
		}
		
		if(getWfM5P() != null && getWfM5P().getRefusedStatus() == null)
		{
			result.add(new SimpleValidationMessage(Translator.getString("refusedStatus") + Translator.getString("IsMandatory"), Severity.ERROR)); 
		}
		
		if(getWfM5P() != null && getWfM5P().getBlockingReason() == null)
		{
			result.add(new SimpleValidationMessage(Translator.getString("blockedReason") + Translator.getString("IsMandatory"), Severity.ERROR)); 
		}
		
		if(getWfM5P() != null && getWfM5P().getUnblockingReason() == null)
		{
			result.add(new SimpleValidationMessage(Translator.getString("unblockedReason") + Translator.getString("IsMandatory"), Severity.ERROR)); 
		}
		
		//Declocage auto
		if(getWfUnblockAuto() != null && getWfUnblockAuto().getUnblockingReason() == null)
		{
			result.add(new SimpleValidationMessage(Translator.getString("unblockedReason") + Translator.getString("IsMandatory"), Severity.ERROR)); 
		}
		
		if(getWfUnblockAuto() != null && getWfUnblockAuto().getUnblockedStatus() == null)
		{
			result.add(new SimpleValidationMessage(Translator.getString("unblockedStatus") + Translator.getString("IsMandatory"), Severity.ERROR)); 
		}
		
		//Blocage auto
		if(getWfBlockAuto() != null && getWfBlockAuto().getBlockingReason() == null)
		{
			result.add(new SimpleValidationMessage(Translator.getString("blockedReason") + Translator.getString("IsMandatory"), Severity.ERROR)); 
		}
		
		if(getWfBlockAuto() != null && getWfBlockAuto().getBlockedStatus() == null)
		{
			result.add(new SimpleValidationMessage(Translator.getString("blockedStatus") + Translator.getString("IsMandatory"), Severity.ERROR)); 
		}
		
		if(getWfBlockAuto() != null && getWfBlockAuto().getInitialStatus() == null)
		{
			result.add(new SimpleValidationMessage(Translator.getString("initialStatus") + Translator.getString("IsMandatory"), Severity.ERROR)); 
		}
		
		//Blocage manuel
		if(getWfUnblockManual() != null && getWfUnblockManual().getUnblockedStatus() == null)
		{
			result.add(new SimpleValidationMessage(Translator.getString("blockedStatus") + Translator.getString("IsMandatory"), Severity.ERROR)); 
		}
		
		//Validation finance
		if(getWfFinanceValidation() != null && getWfFinanceValidation().getEmailCompta() == null)
		{
			result.add(new SimpleValidationMessage(Translator.getString("emailCompta") + Translator.getString("IsMandatory"), Severity.ERROR)); 
		}
		
		if(getWfFinanceValidation() != null && getWfFinanceValidation().getEmailOrigin() == null)
		{
			result.add(new SimpleValidationMessage(Translator.getString("emailOrigin") + Translator.getString("IsMandatory"), Severity.ERROR)); 
		}
		
		//Validation client
		if(getWfClientValidation() != null && getWfClientValidation().getReport() == null)
		{
			result.add(new SimpleValidationMessage(Translator.getString("report") + Translator.getString("IsMandatory"), Severity.ERROR)); 
		}
		
		if(getWfClientValidation() != null && getWfClientValidation().getEmailOrigin() == null)
		{
			result.add(new SimpleValidationMessage(Translator.getString("emailOrigin") + Translator.getString("IsMandatory"), Severity.ERROR)); 
		}
		
		if(getWfClientValidation() != null && getWfClientValidation().getFilePath() == null)
		{
			result.add(new SimpleValidationMessage(Translator.getString("filePath") + Translator.getString("IsMandatory"), Severity.ERROR)); 
		}
		
		if(getWfClientValidation() != null && getWfClientValidation().getFilePathLocal() == null)
		{
			result.add(new SimpleValidationMessage(Translator.getString("filePathLocal") + Translator.getString("IsMandatory"), Severity.ERROR)); 
		}
		
		return result;
	}
}
