package com.mac.custom.bo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.mac.custom.bo.base.BaseMacCompany;
import com.mac.custom.dao.MacCompanyDAO;
import com.netappsid.annotations.DAO;
import com.netappsid.erp.server.bo.AvailableMeasureUnit;
import com.netappsid.erp.server.bo.AvailableTimeMeasureConfiguration;
import com.netappsid.erp.server.services.beans.AvailableTimeMeasureConfigurationServicesBean;
import com.netappsid.erp.server.services.interfaces.AvailableTimeMeasureConfigurationServices;
import com.netappsid.framework.utils.ServiceLocator;

@Entity
@Table(name = "MacCompany", schema = "MacSchema")
@PrimaryKeyJoinColumn(name = "id")
@DAO(dao = MacCompanyDAO.class)
public class MacCompany extends BaseMacCompany
{
	private final ServiceLocator<AvailableTimeMeasureConfigurationServices> availableTimeMeasureConfigurationServicesLocator = new ServiceLocator<AvailableTimeMeasureConfigurationServices>(
			AvailableTimeMeasureConfigurationServicesBean.class);

	private List<Serializable> workUnitAvailableMeasureUnitIds;

	@Transient
	public List<Serializable> getWorkUnitAvailableMeasureUnitIds()
	{
		if (workUnitAvailableMeasureUnitIds == null)
		{
			workUnitAvailableMeasureUnitIds = new ArrayList<Serializable>();
			AvailableTimeMeasureConfiguration availableTimeMeasureConfiguration = availableTimeMeasureConfigurationServicesLocator.get()
					.loadAvailableTimeMeasureConfiguration();
			for (AvailableMeasureUnit availableMeasureUnit : availableTimeMeasureConfiguration.getAvailablesMeasureUnit())
			{
				workUnitAvailableMeasureUnitIds.add(availableMeasureUnit.getId());
			}
		}

		return workUnitAvailableMeasureUnitIds;
	}
}
