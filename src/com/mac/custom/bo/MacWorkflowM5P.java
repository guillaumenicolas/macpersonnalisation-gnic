package com.mac.custom.bo;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.mac.custom.bo.base.BaseMacWorkflowM5P;
import com.mac.custom.dao.MacWorkflowM5PDAO;
import com.netappsid.annotations.DAO;
import com.netappsid.dao.PersistenceListener;
import com.netappsid.validation.Validable;

@Entity
@EntityListeners(value = { PersistenceListener.class })
@DAO(dao = MacWorkflowM5PDAO.class)
@Table(name = "MacWorkflowM5P", schema = "MacSchema")
@PrimaryKeyJoinColumn(name = "id")
public class MacWorkflowM5P extends BaseMacWorkflowM5P implements Serializable, Validable
{
	
}
