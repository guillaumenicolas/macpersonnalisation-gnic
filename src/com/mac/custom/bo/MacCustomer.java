package com.mac.custom.bo;

import java.util.List;

import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.message.SimpleValidationMessage;
import com.mac.custom.bo.base.BaseMacCustomer;
import com.mac.custom.bo.model.EncoursClient;
import com.mac.custom.factory.EncoursClientFactory;
import com.netappsid.erp.server.bo.CustomerSalesRep;
import com.netappsid.erp.server.bo.SalesRep;
import com.netappsid.erp.server.listmodel.QuotationList;
import com.netappsid.resources.Translator;

@javax.persistence.Entity
@Table(name = "MacCustomer", schema = "MacSchema")
@PrimaryKeyJoinColumn(name = "id")
public class MacCustomer extends BaseMacCustomer
{

	private final QuotationList quotationList;
	private EncoursClient encoursClient;

	public MacCustomer()
	{
		quotationList = new QuotationList();
	}

	@Transient
	public QuotationList getQuotationList()
	{
		return quotationList;
	}

	@Transient
	public SalesRep getFirstSalesRep()
	{
		List<CustomerSalesRep> salesReps = getSalesReps();
		if (!salesReps.isEmpty())
		{
			if (salesReps.get(0) != null)
				return salesReps.get(0).getSalesRep();
		}
		return null;
	}

	@Transient
	public String getSalesRepsNames()
	{
		String names = "";
		int index = 1;
		for (CustomerSalesRep customerSalesRep : getSalesReps())
		{
			if (customerSalesRep.getSalesRep() != null)
			{
				if (customerSalesRep.getSalesRep().getName() != null)
				{
					names += customerSalesRep.getSalesRep().getName();
				}
			}
			if (index < getSalesReps().size())
			{
				names += ",";
			}
			index++;
		}

		return names;
	}

	@Override
	protected boolean mustValidateSiret()
	{
		return false;
	}

	@Override
	public ValidationResult validate()
	{
		ValidationResult result = super.validate();

		if (!validateSiren(getSiren()))
		{
			result.add(new SimpleValidationMessage(Translator.getString("SIREN") + Translator.getString("IsMandatory"), Severity.ERROR));
		}

		return result;
	}

	public boolean validateSiren(String siren)
	{
		if (siren == null)
		{
			return false;
		}

		if (siren.length() != 9)
		{
			return false;
		}

		int totalPairs = 0;
		int totalImpairs = 0;

		for (int i = 8; i >= 0; --i)
		{
			final int digit = siren.charAt(i) - 48;

			if (digit < 0 || digit > 9)
			{
				continue;
			}

			if ((i + 1) % 2 != 0) // current digit's index is even? (non-zero based)
			{
				totalImpairs += digit;
			}
			else
			{
				int result = digit * 2;

				if (result > 9)
				{
					String remainderString = String.valueOf(result);
					final int remainder1 = remainderString.charAt(0) - 48;
					final int remainder2 = remainderString.charAt(1) - 48;
					result = remainder1 + remainder2;
				}

				totalPairs += result;
			}
		}

		final int total = totalPairs + totalImpairs;

		return (total % 10 == 0) ? true : false;
	}

	@Transient
	public EncoursClient getEncoursClient()
	{
		if (encoursClient == null && getId() != null)
		{
			EncoursClientFactory encoursClientFactory = new EncoursClientFactory();
			setEncoursClient(encoursClientFactory.create(this));
		}
		return encoursClient;
	}

	public void setEncoursClient(EncoursClient encoursClient)
	{
		this.encoursClient = encoursClient;
		firePropertyChange(PROPERTYNAME_ENCOURSCLIENT, null, encoursClient, false);
	}

	public static final String PROPERTYNAME_ENCOURSCLIENT = "encoursClient";
}