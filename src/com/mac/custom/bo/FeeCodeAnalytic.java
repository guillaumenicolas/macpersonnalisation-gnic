package com.mac.custom.bo;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.mac.custom.bo.base.BaseFeeCodeAnalytic;
import com.mac.custom.dao.FeeCodeAnalyticDAO;
import com.netappsid.annotations.DAO;
import com.netappsid.dao.PersistenceListener;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "standard")
@DAO(dao = FeeCodeAnalyticDAO.class)
@EntityListeners(value = { PersistenceListener.class })
@Table(name = "FeeCodeAnalytic", schema = "MacSchema", uniqueConstraints = { @UniqueConstraint(columnNames = { "code" }),
		@UniqueConstraint(columnNames = { "upperCode" }) })
public class FeeCodeAnalytic extends BaseFeeCodeAnalytic
{
}