package com.mac.custom.bo;

import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.mac.custom.bo.base.BaseMacShippingLocation;

@javax.persistence.Entity
@Table(name = "MacShippingLocation", schema = "MacSchema")
@PrimaryKeyJoinColumn(name = "id")
public class MacShippingLocation extends BaseMacShippingLocation<MacShippingLocation>
{

}
