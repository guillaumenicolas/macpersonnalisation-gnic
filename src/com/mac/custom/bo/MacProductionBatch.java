package com.mac.custom.bo;

import javax.persistence.Transient;

import com.netappsid.erp.server.bo.ProductionBatch;

public class MacProductionBatch extends ProductionBatch
{
	public MacProductionBatch()
	{}

	public MacProductionBatch(ProductionBatch productionBatch)
	{
		setId(productionBatch.getId());
		setCode(productionBatch.getCode());
		setClosed(productionBatch.isClosed());
		setConfirm(productionBatch.isConfirm());
		setConfirmComment(productionBatch.getConfirmComment());
		setDescription(productionBatch.getDescription());
		setDateConfirm(productionBatch.getDateConfirm());
		setDate(productionBatch.getDate());
		setNbOfBatchesAlreadyCreated(productionBatch.getNbOfBatchesAlreadyCreated());
		addManyToWorkOrders(productionBatch.getWorkOrders());
	}

	@Transient
	public String getRelatedOrderNumber()
	{
		if (!getWorkOrders().isEmpty())
		{
			if (getWorkOrders().get(0).getExplodableDocument() != null)
			{
				return getWorkOrders().get(0).getExplodableDocument().getNumber();
			}
		}
		return "";
	}
}
