package com.mac.custom.bo;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.mac.custom.bo.base.BaseWorkLoadDetail;
import com.netappsid.erp.server.datatypes.GroupMeasureValue;

@Entity
@Table(name = "WorkLoadDetail", schema = "MacSchema")
public class WorkLoadDetail extends BaseWorkLoadDetail
{
	private transient GroupMeasureValue quantityToProduce;

	@Transient
	public boolean isBlocked()
	{
		boolean isBlocked = false;

		if (getOrderDetail() != null && getOrderDetail().getTransaction() != null)
		{
			isBlocked = ((MacOrder) getOrderDetail().getTransaction()).isBlockProductionEffective();
		}

		return isBlocked;
	}

	public void updateQuantityProduced(GroupMeasureValue quantity)
	{
		if (getQuantityProduced().getValue() == null)
		{
			setQuantityProduced(new GroupMeasureValue(quantity));
		}
		else
		{
			setQuantityProduced(getQuantityProduced().add(quantity.to(getQuantityProduced())));
		}
	}

	@Transient
	public GroupMeasureValue getQuantityToProduce()
	{
		if (quantityToProduce == null)
		{
			if (getQuantity() != null && getQuantity().isValid())
			{
				quantityToProduce = new GroupMeasureValue(getQuantity());
				if (getQuantityProduced() != null && getQuantityProduced().isValid())
				{
					quantityToProduce = quantityToProduce.substract(getQuantityProduced());
				}
			}
		}
		return quantityToProduce;
	}

	@Transient
	public boolean isProduced()
	{
		if (getQuantity() != null && getQuantity().isValid())
		{
			if (getQuantityProduced() != null && getQuantityProduced().isValid())
			{
				return getQuantity().compareTo(getQuantityProduced()) == 0;
			}
		}
		return false;
	}
}
