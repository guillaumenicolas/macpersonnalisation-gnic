package com.mac.custom.bo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.AccessType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.Type;

import com.mac.custom.bo.localization.MacRegionCommercialeDescription;
import com.mac.custom.dao.MacRegionCommercialeDAO;
import com.netappsid.annotations.DAO;
import com.netappsid.annotations.Localized;
import com.netappsid.dao.PersistenceListener;

@Entity
@EntityListeners(value = { PersistenceListener.class })
@DAO(dao = MacRegionCommercialeDAO.class)
@Table(name = "MacRegionCommerciale", schema = "MacSchema", uniqueConstraints = { @UniqueConstraint(columnNames = { "code" }) } )
@PrimaryKeyJoinColumn(name = "id")
public class MacRegionCommerciale extends com.netappsid.bo.Entity<MacRegionCommerciale> implements Serializable
{
	// attributes
	private Serializable id;
	private String code;
	private Map<String, MacRegionCommercialeDescription> description = new HashMap<String, MacRegionCommercialeDescription>();
	
	public MacRegionCommerciale()
	{
	}

	// Operations

	@Id
	@GeneratedValue(generator = "naid-uuid")
	@GenericGenerator(name = "naid-uuid", strategy = "com.netappsid.framework.utils.UUIDGenerator")
	@Type(type = "com.netappsid.datatypes.UUIDUserType")
	@AccessType(value = "field")
	public Serializable getId()
	{
		return id;
	}

	public void setId(Serializable id)
	{
		this.id = id;
	}

	public void setCode(String code)
	{
		String oldValue = getCode();
		String newValue = code;
		if (newValue != null && newValue.length() == 0)
		{
			newValue = null;
		}
		this.code = newValue;

		firePropertyChange(PROPERTYNAME_CODE, oldValue, newValue);
	}

	@Column(name = "code")//$NON-NLS-1$
	@com.netappsid.annotations.Length(max = 255)
	@AccessType(value = "field")
	public String getCode()
	{
		return code;
	}
	
	public void setDescription(Map<String, MacRegionCommercialeDescription> description)
	{
		Map<String, MacRegionCommercialeDescription> oldValue = getDescription();
		this.description = description;
		firePropertyChange(PROPERTYNAME_DESCRIPTION, oldValue, description);
	}

	@OneToMany(mappedBy = "macRegionCommerciale", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)//$NON-NLS-1$
	@MapKey(name = "languageCode")//$NON-NLS-1$
	@Localized
	@org.hibernate.annotations.Fetch(org.hibernate.annotations.FetchMode.SELECT)
	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "standard")
	@Index(name = "macRegionCommerciale_description_x")//$NON-NLS-1$
	@com.netappsid.annotations.NotNull
	@AccessType(value = "field")
	public Map<String, MacRegionCommercialeDescription> getDescription()
	{
		return description;
	}

	@Transient
	public String getLocalizedDescription()
	{
		return getDescription(Locale.getDefault().getLanguage());
	}

	public String getDescription(String languageCode)
	{
		if (description.containsKey(languageCode) == true)
		{
			return description.get(languageCode).getValue();
		}
		else
		{
			return null;
		}
	}

	public void setDescription(String languageCode, String value)
	{
		if (description.containsKey(languageCode) == true)
		{
			description.get(languageCode).setValue(value);
		}
		else
		{
			MacRegionCommercialeDescription temp = new MacRegionCommercialeDescription(languageCode, value);
			temp.setMacRegionCommerciale((MacRegionCommerciale) this);
			description.put(languageCode, temp);
		}
		firePropertyChange(PROPERTYNAME_DESCRIPTION, null, description);
	}
	
	// Bound properties

	public static final String PROPERTYNAME_CODE = "code"; //$NON-NLS-1$
	public static final String PROPERTYNAME_DESCRIPTION = "description"; //$NON-NLS-1$
}