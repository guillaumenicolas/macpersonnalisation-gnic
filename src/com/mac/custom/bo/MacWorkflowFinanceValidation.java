package com.mac.custom.bo;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.message.SimpleValidationMessage;
import com.mac.custom.bo.base.BaseMacWorkflowARC;
import com.mac.custom.bo.base.BaseMacWorkflowFinanceValidation;
import com.mac.custom.dao.MacWorkflowARCDAO;
import com.mac.custom.dao.MacWorkflowFinanceValidationDAO;
import com.netappsid.annotations.DAO;
import com.netappsid.dao.PersistenceListener;
import com.netappsid.resources.Translator;
import com.netappsid.validation.Validable;

@Entity
@EntityListeners(value = { PersistenceListener.class })
@DAO(dao = MacWorkflowFinanceValidationDAO.class)
@Table(name = "MacWorkflowFinanceValidation", schema = "MacSchema")
@PrimaryKeyJoinColumn(name = "id")
public class MacWorkflowFinanceValidation extends BaseMacWorkflowFinanceValidation implements Serializable, Validable
{

}
