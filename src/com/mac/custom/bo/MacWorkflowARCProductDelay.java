package com.mac.custom.bo;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import com.mac.custom.bo.base.BaseMacWorkflowARCProductDelay;
import com.mac.custom.dao.MacWorkflowARCProductDelayDAO;
import com.netappsid.annotations.DAO;
import com.netappsid.erp.server.factory.PropertyValueFactory;

@Entity
@DAO(dao = MacWorkflowARCProductDelayDAO.class)
@Table(name = "MacWorkflowARCProductDelay", schema = "MacSchema", uniqueConstraints = { @UniqueConstraint(columnNames = { "product_id", "property_id", "propertyValue_id" })})
public class MacWorkflowARCProductDelay extends BaseMacWorkflowARCProductDelay
{
	@Transient
	public void setValue(Object value)
	{
		if (value != null)
		{
			setPropertyValue(PropertyValueFactory.INSTANCE.create(getProperty(), false, value));
		}
		else
		{
			setPropertyValue(null);
		}
	}
}
