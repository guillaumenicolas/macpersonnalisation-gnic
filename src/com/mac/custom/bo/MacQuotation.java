package com.mac.custom.bo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.message.SimpleValidationMessage;
import com.mac.custom.bo.base.BaseMacQuotation;
import com.mac.custom.enums.SaleTypeEnum;
import com.mac.custom.workflow.WorkflowUtils;
import com.netappsid.bo.model.Model;
import com.netappsid.erp.server.bo.DocumentStatus;
import com.netappsid.erp.server.bo.Quotation;
import com.netappsid.erp.server.bo.TransactionDetail;
import com.netappsid.europe.naid.custom.bo.QuotationDetailEuro;
import com.netappsid.framework.utils.ObservableCollectionUtils;
import com.netappsid.resources.Translator;

@javax.persistence.Entity
@Table(name = "MacQuotation", schema = "MacSchema")
@PrimaryKeyJoinColumn(name = "id")
public class MacQuotation extends BaseMacQuotation
{
	@Override
	public ValidationResult validate()
	{
		ValidationResult vr = super.validate();

		// MacWorkflowMaster wfMaster = WorkfowUtils.getWFMaster();
		//
		// if(wfMaster != null && wfMaster.getWfM5P() != null && wfMaster.getWfM5P().isActive())
		// {
		// if(getDocumentStatus().hasSameID(wfMaster.getWfM5P().getM5pStatus()))
		// {
		// vr.add(new SimpleValidationMessage(Translator.getString("cannotSaveWhenM5P"), Severity.ERROR));
		// }
		// }

		if (getSaleType() != null && getSaleType().getCode() != null && getSaleType().getUpperCode().equals(SaleTypeEnum.sav.getUpperCode()))
		{
			if (getServiceType() == null || getServiceType().getCode() == null || getServiceType().getCode().equals(""))
			{
				vr.add(new SimpleValidationMessage(Translator.getString("cannotSaveWhenSAVServiceTypeNotCompleted", Severity.ERROR)));
			}
			if (getOriginalOrderNumber() == null || getOriginalOrderNumber().equals(""))
			{
				vr.add(new SimpleValidationMessage(Translator.getString("cannotSaveWhenSAVOriginalOrderNumberNotCompleted", Severity.ERROR)));
			}
		}
		return vr;
	}

	@Transient
	public MacCustomer getMacCustomer()
	{
		return (MacCustomer) Model.getTarget(getCustomer());
	}

	@Override
	public void refreshDocumentStatusFlow()
	{
		List<DocumentStatus> newDocumentStatusFlow;
		if (getCompany() != null)
		{
			newDocumentStatusFlow = getDocumentServicesLocator().get().getDocumentStatusFlow(getCompany().getId(), Quotation.class, getDocumentStatus());
		}
		else
		{
			newDocumentStatusFlow = new ArrayList<DocumentStatus>();
		}

		ObservableCollectionUtils.clearAndAddAll(getDocumentStatusFlow(), newDocumentStatusFlow);
	}

	@Override
	public boolean isTransactionDetailSeverityOkForSave(TransactionDetail detail)
	{
		MacWorkflowMaster wfMaster = WorkflowUtils.getWFMaster();

		if (detail.containsConfiguration() && detail.getConfiguratorValidationSeverity().equals(com.netappsid.erp.Severity.ERROR))
		{
			if (wfMaster != null && wfMaster.getWfM5P().getM5pStatus().hasSameID(detail.getTransaction().getDocumentStatus()))
			{
				if (detail instanceof QuotationDetailEuro && ((QuotationDetailEuro) detail).isRareBird())
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}

		return true;
	}
}