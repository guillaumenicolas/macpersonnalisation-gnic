package com.mac.custom.bo;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.jgoodies.validation.ValidationResult;
import com.mac.custom.bo.base.BaseMacWorkflowARC;
import com.mac.custom.dao.MacWorkflowARCDAO;
import com.netappsid.annotations.DAO;
import com.netappsid.dao.PersistenceListener;
import com.netappsid.erp.server.enums.ShiftTypeEnum;
import com.netappsid.resources.Translator;
import com.netappsid.validation.Validable;

@Entity
@EntityListeners(value = { PersistenceListener.class })
@DAO(dao = MacWorkflowARCDAO.class)
@Table(name = "MacWorkflowARC", schema = "MacSchema")
@PrimaryKeyJoinColumn(name = "id")
public class MacWorkflowARC extends BaseMacWorkflowARC implements Serializable, Validable
{
	@Override
	public ValidationResult validate()
	{
		ValidationResult validation = super.validate();
		if (getWorkShift() != null && getWorkShift().getShiftType() != ShiftTypeEnum.intervalShift)
		{
			validation.addError(Translator.getString("WorkShiftForWorkflowARCMustBeInterval"));
		}
		return validation;
	}
}
