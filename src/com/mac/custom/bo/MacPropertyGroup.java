package com.mac.custom.bo;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.mac.custom.bo.base.BaseMacPropertyGroup;
import com.mac.custom.dao.EnseigneClientDAO;
import com.netappsid.annotations.DAO;
import com.netappsid.dao.PersistenceListener;

@javax.persistence.Entity
@Table(name = "PropertyGroup", schema = "MacUtils")
@PrimaryKeyJoinColumn(name = "id")
public class MacPropertyGroup extends BaseMacPropertyGroup
{

}