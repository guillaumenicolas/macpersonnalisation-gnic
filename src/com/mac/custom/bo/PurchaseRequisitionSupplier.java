package com.mac.custom.bo;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.AccessType;

import com.netappsid.annotations.Scale;
import com.netappsid.dao.PersistenceListener;
import com.netappsid.erp.server.bo.Supplier;

@MappedSuperclass
@EntityListeners(value = { PersistenceListener.class })
@Table(name = "PurchaseRequisitionSupplier")
public class PurchaseRequisitionSupplier extends com.netappsid.bo.model.Model implements Serializable
{
	// attributes
	private BigDecimal percentage;
	private PurchaseRequisitionProduct purchaseRequisitionProduct;
	private Supplier supplier;

	// Operations

	public void setPercentage(BigDecimal percentage)
	{
		BigDecimal oldValue = getPercentage();
		this.percentage = percentage;
		firePropertyChange(PROPERTYNAME_PERCENTAGE, oldValue, percentage);
		if (getPurchaseRequisitionProduct() != null)
		{
			getPurchaseRequisitionProduct().refreshPercentage();

		}
	}

	@Column(name = "percentage", precision = 32, scale = 10)
	@Scale(decimal = 10)
	@AccessType(value = "field")
	public BigDecimal getPercentage()
	{
		return percentage;
	}

	@Transient
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@org.hibernate.validator.NotNull
	@com.netappsid.annotations.NotNull
	@AccessType(value = "field")
	public PurchaseRequisitionProduct getPurchaseRequisitionProduct()
	{
		return purchaseRequisitionProduct;
	}

	public void setPurchaseRequisitionProduct(PurchaseRequisitionProduct purchaseRequisitionProduct)
	{
		PurchaseRequisitionProduct oldValue = this.purchaseRequisitionProduct;
		this.purchaseRequisitionProduct = purchaseRequisitionProduct;
		firePropertyChange(PROPERTYNAME_PURCHASEREQUISITIONPRODUCT, oldValue, this.purchaseRequisitionProduct);
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@org.hibernate.validator.NotNull
	@com.netappsid.annotations.NotNull
	@AccessType(value = "field")
	public Supplier getSupplier()
	{
		return supplier;
	}

	public void setSupplier(Supplier supplier)
	{
		Supplier oldValue = this.supplier;
		this.supplier = supplier;
		firePropertyChange(PROPERTYNAME_SUPPLIER, oldValue, this.supplier);
	}

	// Business methods

	// Bound properties

	public static final String PROPERTYNAME_PERCENTAGE = "percentage";
	public static final String PROPERTYNAME_PURCHASEREQUISITIONPRODUCT = "purchaseRequisitionProduct";
	public static final String PROPERTYNAME_SUPPLIER = "supplier";
}
