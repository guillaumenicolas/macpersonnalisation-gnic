package com.mac.custom.utils;

import java.io.FileOutputStream;
import java.util.List;
import java.util.ListIterator;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class MacExcelUtils
{
	public final static String DEFAULT_WORKSHEETNAME = "ExportedSheet";
	public final static String DEFAULT_FONT_NAME = "Arial";

	/**
	 * @param reportList
	 * @param workSheetName
	 * @param autosizecolumns
	 * @return
	 */
	public static HSSFWorkbook prepareWorkBook(HSSFWorkbook currentWorkBook, List<List<String>> reportList, String workSheetName, boolean autosizecolumns)
	{

		final String METHOD_NAME = "convertListToExcel";
		System.out.println("Entered " + METHOD_NAME);

		if (workSheetName == null || workSheetName.isEmpty())
		{
			workSheetName = "ExportedSheet";
		}
		int lengthOfColumns = reportList.get(0).size();

		// Do the POI Things to generate the Excel File create a new workbook
		if (currentWorkBook == null)
			currentWorkBook = new HSSFWorkbook();
		HSSFSheet sheet = currentWorkBook.createSheet(workSheetName);

		/*******************************************
		 * This Part Contains The Cell Formatting Stuff.
		 * ********************************************/
		// Create a Font For Header
		HSSFFont headerFont = currentWorkBook.createFont();
		headerFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		headerFont.setFontName(DEFAULT_FONT_NAME);

		// create a style for the header Columns
		HSSFCellStyle columnHeaderStyle = currentWorkBook.createCellStyle();
		columnHeaderStyle.setFont(headerFont);

		// Create a Font For Data Rows
		HSSFFont rowFont = currentWorkBook.createFont();
		rowFont.setFontName(DEFAULT_FONT_NAME);

		// create a style for the Records In Next Rows
		HSSFCellStyle rowCellStyle = currentWorkBook.createCellStyle();
		rowCellStyle.setFont(rowFont);
		/**********************************************/

		HSSFRow[] headerRow = new HSSFRow[reportList.size()];
		ListIterator<List<String>> outerListIterator = reportList.listIterator();

		// Create Header Row
		if (outerListIterator.hasNext())
		{

			int rowIndex = outerListIterator.nextIndex();
			List<String> innerList = outerListIterator.next();

			headerRow[rowIndex] = sheet.createRow((short) rowIndex);
			HSSFCell[] headerColumns = new HSSFCell[lengthOfColumns];

			ListIterator<String> innerListIterator = innerList.listIterator();
			while (innerListIterator.hasNext())
			{
				int columnIndex = innerListIterator.nextIndex();
				headerColumns[columnIndex] = headerRow[rowIndex].createCell(columnIndex);

				String cellValue = innerListIterator.next();
				headerColumns[columnIndex].setCellValue(cellValue);
				headerColumns[columnIndex].setCellStyle(columnHeaderStyle);
				if (autosizecolumns)
					sheet.autoSizeColumn((short) (columnIndex));
			}
		}

		// Create other Row(s)
		while (outerListIterator.hasNext())
		{

			int rowIndex = outerListIterator.nextIndex();
			List<String> innerList = outerListIterator.next();

			headerRow[rowIndex] = sheet.createRow((short) rowIndex);
			HSSFCell[] headerColumns = new HSSFCell[innerList.size()];

			// Create Data for the Header Row
			ListIterator<String> innerListIterator = innerList.listIterator();
			while (innerListIterator.hasNext())
			{
				int columnIndex = innerListIterator.nextIndex();
				headerColumns[columnIndex] = headerRow[rowIndex].createCell(columnIndex);

				String cellValue = innerListIterator.next();
				headerColumns[columnIndex].setCellValue(cellValue);
				headerColumns[columnIndex].setCellStyle(rowCellStyle);
				if (autosizecolumns)
					sheet.autoSizeColumn((short) (columnIndex));
			}
		}

		return currentWorkBook;
	}

	public static void saveToExcelFile(HSSFWorkbook workBook, String filePath)
	{
		try
		{
			System.out.println("Please wait : Writing workbook in file : " + filePath);
			FileOutputStream fileOut = new FileOutputStream(filePath);
			workBook.write(fileOut);
			fileOut.close();
			System.out.println("File created : " + filePath);
		}
		catch (Exception e)
		{
			System.err.println("WorkBook cannot be save into file : " + filePath + "  probably in use!");
			String newFilePath = filePath.replaceFirst(".xls", "_new.xls");
			try
			{
				System.err.println("WorkBook will be save into file : " + newFilePath);
				FileOutputStream fileOut = new FileOutputStream(newFilePath);
				workBook.write(fileOut);
				fileOut.close();
				System.out.println("File created : " + newFilePath);
			}
			catch (Exception e1)
			{
				System.err.println("WorkBook cannot be save into file : " + newFilePath);
				e1.printStackTrace();
			}
		}
	}
}