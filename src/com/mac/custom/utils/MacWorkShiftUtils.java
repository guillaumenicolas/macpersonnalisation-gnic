package com.mac.custom.utils;

import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.LocalTime;

import com.google.common.base.Preconditions;
import com.netappsid.erp.server.bo.AbstractShift;
import com.netappsid.erp.server.bo.IntervalShift;
import com.netappsid.erp.server.bo.WorkShift;
import com.netappsid.erp.server.bo.WorkShiftException;
import com.netappsid.erp.server.enums.ShiftTypeEnum;
import com.netappsid.erp.server.utils.WorkShiftUtils;

public final class MacWorkShiftUtils extends WorkShiftUtils
{
	/**
	 * Apply the intervals in the work shift to the original date until the delay is reached.
	 * 
	 * @param date
	 *            The original date. Must not be null.
	 * @param delay
	 *            The delay to apply in milliseconds. Must be >= 0.
	 * @param workShift
	 *            The workdays calendar to use. It must be by intervals.
	 * @return The final date applying the delay on the initial date with working hours in the workShift. This date will be in the intervals of the workshift
	 *         event if the delay is 0.
	 * @throws IllegalArgumentException
	 *             If the delay is < 0 or the workShift is not by intervals.
	 */
	public Date applyWorkShiftWorkedHoursInterval(Date date, long delay, WorkShift workShift) throws IllegalArgumentException
	{
		Preconditions.checkArgument(workShift.getShiftType() == ShiftTypeEnum.intervalShift, "The WorkShift must be by intervals");
		Preconditions.checkArgument(delay >= 0, "The delay must be >= 0");
		Preconditions.checkNotNull(date);

		DateTime currentDateTime = new DateTime(date);
		Date finalDate = null;

		// try with 1000 days before giving up
		for (int i = 0; i < 1000 && finalDate == null; ++i)
		{
			List<? extends AbstractShift> shifts = getShifts(currentDateTime.toDate(), workShift);
			for (AbstractShift shift : shifts)
			{
				// we already check that the shift was in intervals
				IntervalShift intervalShift = com.netappsid.bo.model.Model.getTarget(shift);
				// build the interval for the current date in loop
				Interval interval = new Interval(new LocalTime(intervalShift.getTimeFrom()).toDateTime(currentDateTime),
						new LocalTime(intervalShift.getTimeTo()).toDateTime(currentDateTime));
				if (interval.contains(date.getTime()))
				{
					interval = new Interval(date.getTime(), interval.getEnd().getMillis());
				}
				// if the current interval doesn't cover the remaining delay
				if (interval.toDurationMillis() <= delay)
				{
					delay -= interval.toDurationMillis();
				}
				else
				{
					// only exit route from loop
					finalDate = interval.getStart().plus(delay).toDate();
				}
			}
			// try tomorrow...
			currentDateTime = currentDateTime.plusDays(1);
		}

		return finalDate;
	}

	private List<? extends AbstractShift> getShifts(Date date, WorkShift workShift)
	{
		WorkShiftException workShiftException = getWorkShiftException(date, workShift);
		List<AbstractShift> shifts;
		if (workShiftException != null)
		{
			shifts = workShiftException.getShifts();
		}
		else
		{
			shifts = getShiftsForDate(date, workShift);
		}
		return shifts;
	}
}
