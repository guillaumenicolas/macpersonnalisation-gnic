package com.mac.custom.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.google.common.collect.Lists;
import com.mac.custom.bo.MacPropertyGroup;
import com.netappsid.erp.server.bo.PropertyValue;
import com.netappsid.erp.server.enums.PropertyTypeEnum;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.Loader;

public class RareBirdUtils
{
	public static String generateRareBirdDescription(List<PropertyValue> propertyValues)
	{
		List<String> propertyValueString = Lists.newArrayList();
		for (PropertyValue propertyValue : getListeProprieteFiltre(propertyValues, "DEVIS_ARC"))
		{
			propertyValueString.add(propertyValue.getProperty().getLocalizedDescription()
					+ " : "
					+ (propertyValue.getType() == PropertyTypeEnum.NUMERIC ? Float.valueOf(propertyValue.getDisplayableValue().trim()).floatValue()
							: propertyValue.getDisplayableValue()));
		}
		return StringUtils.join(propertyValueString, '\n');
	}

	// TODO This is a copy/paste of the utils.ReportUtils.getListeProprieteFiltre() method from the MACEditions project. MACEditions already has a dependency
	// towards MacPersonalisation, perhaps this method from ReportUtils should simply be moved here instead...
	public static List<PropertyValue> getListeProprieteFiltre(List<PropertyValue> listePropertyValue, String code)
	{
		final Map<Integer, PropertyValue> listePropertyValueFiltre = new TreeMap<Integer, PropertyValue>();

		ServiceLocator<Loader> loaderLocator = new ServiceLocator<Loader>(LoaderBean.class);

		List<Integer> joinSpecList = new ArrayList<Integer>();

		// test si la propriété est présente dans la table de paramétrage MacUtils.PropertyGroup
		for (PropertyValue propertyValue : listePropertyValue)
		{
			List<Criterion> criteriaList = new ArrayList<Criterion>();
			criteriaList.add(Restrictions.eq(MacPropertyGroup.PROPERTYNAME_GROUPE, code));
			String codePropriete = propertyValue.getProperty().getCode();
			criteriaList.add(Restrictions.eq(MacPropertyGroup.PROPERTYNAME_CODE, codePropriete));
			List<MacPropertyGroup> listeMacPropertyGroup = loaderLocator.get().findByCriteria(MacPropertyGroup.class, MacPropertyGroup.class, criteriaList,
					null, null);

			for (MacPropertyGroup macPropertyGroup : listeMacPropertyGroup)
			{
				listePropertyValueFiltre.put(macPropertyGroup.getOrdre(), propertyValue);
			}

		}

		return new ArrayList<PropertyValue>(listePropertyValueFiltre.values());
	}
}
