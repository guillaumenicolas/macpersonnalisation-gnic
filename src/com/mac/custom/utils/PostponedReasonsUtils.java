package com.mac.custom.utils;

import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.Transient;

import com.netappsid.dao.utils.boquery.BOQuery;
import com.netappsid.europe.naid.custom.bo.PostponedReason;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.observable.ObservableCollections;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.Loader;

public class PostponedReasonsUtils
{
	private ServiceLocator<Loader> loaderServiceLocator;
	public static final String AUTOMATIC_DATE_POSTPONED_REASON_CODE = "UO";

	public PostponedReasonsUtils()
	{
		this(new ServiceLocator<Loader>(LoaderBean.class));
	}

	public PostponedReasonsUtils(ServiceLocator<Loader> loaderServiceLocator)
	{
		setLoaderServiceLocator(loaderServiceLocator);
	}

	@Transient
	protected ServiceLocator<Loader> getLoaderServiceLocator()
	{
		return loaderServiceLocator;
	}

	public void setLoaderServiceLocator(ServiceLocator<Loader> loaderServiceLocator)
	{
		this.loaderServiceLocator = loaderServiceLocator;
	}

	@Transient
	public List<PostponedReason> getAvailablePostponedReasons()
	{
		BOQuery query = new BOQuery(PostponedReason.class);
		query.addNotEqual(PostponedReason.PROPERTYNAME_CODE, AUTOMATIC_DATE_POSTPONED_REASON_CODE);

		List<PostponedReason> availablePostponedReasons = ObservableCollections.newObservableArrayList();
		availablePostponedReasons.addAll(getLoaderServiceLocator().get().findByBOQuery(PostponedReason.class, query));

		Collections.sort(availablePostponedReasons, new Comparator<PostponedReason>()
			{
				@Override
				public int compare(PostponedReason postponedReason1, PostponedReason postponedReason2)
				{
					return Collator.getInstance().compare(String.valueOf(postponedReason1.getLocalizedDescription()),
							String.valueOf(postponedReason2.getLocalizedDescription()));
				}
			});
		return availablePostponedReasons;
	}

}
