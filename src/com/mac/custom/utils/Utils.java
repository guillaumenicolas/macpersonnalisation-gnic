package com.mac.custom.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.jboss.security.SecurityAssociation;

public class Utils
{
	public static String getLoginName()
	{
		String loginName = SecurityAssociation.getPrincipal() == null ? null : SecurityAssociation.getPrincipal().getName();

		if (loginName == null)
		{
			loginName = "NAID";
		}
		else if (loginName.equals("admin"))
		{
			loginName = "NAID";
		}

		return loginName;
	}

	public static boolean CopierFichier(File Source, File Destination)
	{
		boolean resultat = false;
		FileInputStream filesource = null;
		FileOutputStream fileDestination = null;
		try
		{
			filesource = new FileInputStream(Source);
			fileDestination = new FileOutputStream(Destination);
			byte buffer[] = new byte[512 * 1024];
			int nblecture;
			while ((nblecture = filesource.read(buffer)) != -1)
			{
				fileDestination.write(buffer, 0, nblecture);
			}
			resultat = true;
		}
		catch (FileNotFoundException nf)
		{
			nf.printStackTrace();
		}
		catch (IOException io)
		{
			io.printStackTrace();
		}
		finally
		{
			try
			{
				filesource.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			try
			{
				fileDestination.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return resultat;
	}

	public static boolean appendFichier(List<File> arrayFile, File Destination) throws FileNotFoundException
	{
		boolean resultat = false;
		FileInputStream filesource = null;
		FileOutputStream fileDestination = null;
		fileDestination = new FileOutputStream(Destination);

		byte buffer[] = new byte[512 * 1024];
		int nblecture;
		for (File Source : arrayFile)
		{
			try
			{
				filesource = new FileInputStream(Source);
				while ((nblecture = filesource.read(buffer)) != -1)
				{
					fileDestination.write(buffer, 0, nblecture);
				}
				resultat = true;
			}
			catch (FileNotFoundException nf)
			{
				nf.printStackTrace();
			}
			catch (IOException io)
			{
				io.printStackTrace();
			}
			finally
			{
				try
				{
					filesource.close();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}

		}
		try
		{
			fileDestination.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return resultat;
	}
}
