package com.mac.custom.utils;

import java.util.List;

import com.netappsid.erp.server.bo.Deposit;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.Loader;

public class DepositUtils
{
	private final ServiceLocator<Loader> loaderServicesServiceLocator = new ServiceLocator<Loader>(LoaderBean.class);

	private Deposit defaultDeposit;

	public Deposit getDefaultDeposit()
	{
		if (defaultDeposit == null)
		{
			List<Deposit> deposits = loaderServicesServiceLocator.get().findByField(Deposit.class, Deposit.class, "code", "NA", true);
			if (!deposits.isEmpty())
			{
				defaultDeposit = deposits.get(0);
			}
		}

		return defaultDeposit;
	}
}
