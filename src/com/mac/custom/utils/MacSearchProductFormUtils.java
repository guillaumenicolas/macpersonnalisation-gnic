package com.mac.custom.utils;

import java.util.List;
import java.util.Locale;

import com.mac.custom.form.MacSearchProductForm;
import com.netappsid.erp.client.gui.DefaultProductFormPanel;
import com.netappsid.erp.client.gui.SearchProductForm;
import com.netappsid.erp.client.gui.components.ProductSearchInfo;
import com.netappsid.erp.client.utils.ConfigurationSearchForm;
import com.netappsid.erp.client.utils.SearchProductFormUtils;
import com.netappsid.erp.server.bo.CommercialEntity;
import com.netappsid.erp.server.bo.Configuration;
import com.netappsid.erp.server.bo.Document;
import com.netappsid.erp.server.bo.Product;
import com.netappsid.framework.gui.dialog.ExceptionValidator;
import com.netappsid.framework.utils.CurrentContextClassLoaderUtils;
import com.netappsid.resources.Translator;

/**
 * A Mac custom that extends {@link SearchProductFormUtils}, not to override methods (that's not possible because they are static) but to have access to some of
 * its methods that we don't want to make public.
 * 
 * @author ftaillefer
 * 
 */
public class MacSearchProductFormUtils extends SearchProductFormUtils
{

	/**
	 * <p>
	 * Creates and configures a {@link SearchProductForm}, then shows it to the user and returns the selected products.<br/>
	 * Many of this method's parameters are sent directly to {@link #getSearchProductForm(Document, boolean, boolean, Locale, List)} (and defined there).
	 * </p>
	 * This is a copy/paste of the same method in {@link SearchProductFormUtils}, because we can't override static methods and we need this version to call the
	 * {@link #getSearchProductForm(Document, boolean, boolean, Locale, List)} defined in this class. I tried to limit the amount of copied code as much as I
	 * could.
	 * 
	 * @param document
	 * @param productSearchInfo
	 *            A list that will receive the selected products. This list will also be the return value of this method.
	 * @param includeCustomSearchForm
	 * @param includeDefaultSearchForm
	 * @param locale
	 * @param productClasses
	 * @return
	 */
	public static List<ProductSearchInfo> showProductSearchForm(Document document, List<ProductSearchInfo> productSearchInfo, boolean includeCustomSearchForm,
			boolean includeDefaultSearchForm, Locale locale, List<Class<? extends Product>> productClasses)
	{
		// Remember the current ClassLoader, to guarantee that we leave this method with that ClassLoader set, even if we crash while using the configurator.
		ClassLoader previousContextClassLoader = Thread.currentThread().getContextClassLoader();
		try
		{
			// Obtain the form
			SearchProductForm<CommercialEntity> searchProductForm = getSearchProductForm(document, includeCustomSearchForm, includeDefaultSearchForm, locale,
					productClasses);

			// Show the form
			showSearchProductForm(productSearchInfo, searchProductForm);
		}
		catch (Exception exc)
		{
			// If we got an exception creating or showing the form, tell the user about it
			ExceptionValidator.validateException(exc, Translator.getString("Error"));
		}
		finally
		{
			// Make sure the ClassLoader is the one we had when we arrived
			CurrentContextClassLoaderUtils.setContextClassLoader(previousContextClassLoader);
		}
		return productSearchInfo;
	}

	/**
	 * Creates, configures and returns a {@link SearchProductForm}
	 * 
	 * @param document
	 *            The document that should be passed to the {@link SearchProductForm}.
	 * @param includeCustomSearchForm
	 *            Indicates whether the {@link SearchProductForm} should include an entry point to the included configurations. If this is <code>true</code>,
	 *            the {@link SearchProductForm} will include for each {@link Configuration} a tab containing the configuration's {@link ConfigurationSearchForm}
	 *            .
	 * @param includeDefaultSearchForm
	 *            Indicates whether the {@link SearchProductForm} should include an "Advanced search" tab. This tab contains a {@link DefaultProductFormPanel}.
	 * @param locale
	 *            The locale of the current user
	 * @param productClasses
	 *            A list of product classes to limit the available products to in the "Advanced search" tab. If this is empty or null, all product classes will
	 *            be included.
	 * @return The form
	 */
	private static SearchProductForm<CommercialEntity> getSearchProductForm(Document document, boolean includeCustomSearchForm,
			boolean includeDefaultSearchForm, Locale locale, List<Class<? extends Product>> productClasses)
	{

		// We're going to create a SearchProductForm. We have all the parameters we'll need except 2

		// 1- We need a list of ConfigurationSearchForms. If we don't want them, then the list will remain empty.
		// If we want them, build them here
		List<ConfigurationSearchForm> configurationSearchForms = prepareConfigurationSearchForms(document, includeCustomSearchForm);

		// 2- We want the document's commercial entity (if it has one)
		CommercialEntity commercialEntity = getCommercialEntityForSearchProductForm(document);

		// What's left to do is create the actual custom SearchProductForm
		return new MacSearchProductForm<CommercialEntity>(configurationSearchForms, commercialEntity, document.getCompany(), includeDefaultSearchForm, locale,
				productClasses, document);
	}
}
