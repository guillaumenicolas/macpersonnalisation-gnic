package com.mac.custom.utils;

//CSV Exporter  
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRCsvExporterParameter;

import org.apache.log4j.Logger;

import com.mac.custom.services.report.beans.MacReportServicesBean;
import com.netappsid.erp.server.bo.Setup;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.Loader;

public class ImpressionJasper
{

	/**
	 * @param args
	 * @throws JRException
	 * @throws Exception
	 * @throws SQLException
	 */

	public static void genereCSV(JasperPrint jasperPrint, String csvFile, String fieldDelimiter) throws JRException
	{
		JRCsvExporter csvExporter = new JRCsvExporter();
		csvExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		csvExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, csvFile);

		// DELIMETER Provider parameter
		csvExporter.setParameter(JRCsvExporterParameter.FIELD_DELIMITER, fieldDelimiter);
		csvExporter.setParameter(JRCsvExporterParameter.RECORD_DELIMITER, "\r\n");

		csvExporter.exportReport();
	}

	public static ArrayList<Object> ExportJasper(String[] args, Connection connection) throws SQLException, Exception
	{

		// Param�tres en Entr�e

		String fileJasper = args[0];
		String InParameter = args[1];
		String separateur = args[2];
		String separateurPrincipal = args[3];
		String outPutDir = args[4];
		String fieldDelimiter = args[5];
		String fileCible = args[6];

		ArrayList returnObject = new ArrayList();

		try
		{
			String str[] = InParameter.split(separateurPrincipal);

			// S�parateur "" pour les fichier au format text
			if (fieldDelimiter.equals("text"))
			{
				fieldDelimiter = " ";
			}

			// - Param�tres � envoyer au rapport
			Map Reportparameter = new HashMap();
			for (String string : str)
			{
				String str2[] = string.split(separateur);
				Reportparameter.put(str2[0], str2[1]);
			}

			JasperPrint jasperPrint = JasperFillManager.fillReport(fileJasper, Reportparameter, connection);
			returnObject.add(jasperPrint);
			returnObject.add(outPutDir);
			returnObject.add(fieldDelimiter);
			returnObject.add(fileCible);
			return returnObject;

		}
		catch (Exception e)
		{

			e.printStackTrace();
			System.out.println(e);
			return null;
		}

	}

	public static ArrayList<Object> genereImpression(String InParameter, Connection connection, String entity)
	{
		final ServiceLocator<Loader> loaderServiceLocator = new ServiceLocator<Loader>(LoaderBean.class);

		final Logger logger = Logger.getLogger(MacReportServicesBean.class);
		String jasperFile;
		String separatorParam;
		String separatorPrincipal;
		String fileOUT;
		String fieldDelimiter;
		String[] args = null;
		ArrayList<Object> arrayList = null;

		Setup setup = loaderServiceLocator.get().findAll(Setup.class, Setup.class).get(0);

		final StringBuffer sql = new StringBuffer();

		sql.append("SELECT [jasperFile],[separatorParam],[separatorPrincipal],[directoryOUT],[fieldDelimiter], [fileCible] FROM [BC360].[MacUtils].[ParamExportReport] where [entity] = '"
				+ entity + "'");

		PreparedStatement statement;
		try
		{
			statement = connection.prepareStatement(sql.toString());

			ResultSet resulset = statement.executeQuery();
			while (resulset.next())
			{
				jasperFile = setup.getPathReporting() + resulset.getString(1);

				separatorParam = resulset.getString(2);
				separatorPrincipal = resulset.getString(3);
				fileOUT = resulset.getString(4);
				fieldDelimiter = resulset.getString(5);
				String fileCible = resulset.getString(6);
				args = new String[] { jasperFile, InParameter, separatorParam, separatorPrincipal, fileOUT, fieldDelimiter, fileCible };
				final StringBuffer sql2 = new StringBuffer();
				try
				{
					// TRACE

					sql2.append("INSERT INTO [BC360].[MacUtils].[Trace]([TraceDate],[TraceText])VALUES(GETDATE(),'" + jasperFile + " - "
							+ InParameter.replace("'", "") + " - " + fileOUT + "')");
					PreparedStatement statement2;

					statement2 = connection.prepareStatement(sql2.toString());

					boolean resulset2 = statement2.execute();

				}
				catch (SQLException e)
				{

					logger.error("Erreur lors de la requ�te : " + sql2.toString() + " \n code erreur :" + e);
					System.out.println(e);
					return null;
				}

				arrayList = com.mac.custom.utils.ImpressionJasper.ExportJasper(args, connection);

			}
		}
		catch (SQLException e)
		{

			logger.error("Erreur lors de la requ�te : " + sql.toString() + " \n code erreur :" + e);
			System.out.println(e);
			return null;
		}

		catch (Exception e)
		{

			logger.error("Erreur lors de l'export du rapport : \n arguments = " + args + " \n code erreur :" + e);
			System.out.println(e);
			return null;
		}
		return arrayList;
	}
}
