package com.mac.custom.utils;

import com.mac.custom.bo.MacSetup;
import com.netappsid.erp.server.services.utils.SetupUtils;

public class MacSetupUtils
{
	public static MacSetup getMainMacSetup()
	{
		return (MacSetup) SetupUtils.INSTANCE.getMainSetup();
	}
}
