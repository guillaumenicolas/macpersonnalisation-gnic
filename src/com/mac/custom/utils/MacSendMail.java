package com.mac.custom.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.netappsid.erp.server.services.utils.SetupUtils;

public class MacSendMail
{
	public final static String MESSAGE_TYPE_TEXT = "text/plain";
	public final static String MESSAGE_TYPE_HTML = "text/html";

	String recipients; // comma ',' separated list of recipients
	String recipientsCC; // comma ',' separated list of recipients
	InternetAddress from;
	String subject = "";
	List<MimeBodyPart> bodyPartContentList = new ArrayList<MimeBodyPart>();

	List<String> attachedFiles = new ArrayList<String>();
	boolean sentWithSuccess = false;

	public void sendMultiPart()
	{
		sentWithSuccess = false;
		if (from != null)
		{
			if (recipients != null && !recipients.isEmpty())
			{
				// OK - all environnement variable are ready for sending an email
				// Get system properties
				Properties properties = System.getProperties();

				// Setup mail server
				properties.setProperty("mail.smtp.host", getSmtp_host());

				// Get the default Session object.
				Session session = Session.getDefaultInstance(properties);

				try
				{
					// Create a default MimeMessage object.
					MimeMessage message = new MimeMessage(session);

					// Set From: header field of the header.
					message.setFrom(from);

					// Set To: header field of the header.
					message.addRecipients(Message.RecipientType.TO, recipients);
					message.addRecipients(Message.RecipientType.CC, recipientsCC);

					// Set Subject: header field
					message.setSubject(subject);

					// Create a multipart message
					Multipart multipart = new MimeMultipart();

					// Fill the message
					for (MimeBodyPart messageBodyPart : bodyPartContentList)
					{
						multipart.addBodyPart(messageBodyPart);
					}

					// Part two is attachment for each files
					for (String filename : getAttachedFiles())
					{
						MimeBodyPart messageBodyPart = new MimeBodyPart();
						DataSource source = new FileDataSource(filename);
						messageBodyPart.setDataHandler(new DataHandler(source));
						messageBodyPart.setFileName(filename);
						multipart.addBodyPart(messageBodyPart);
					}

					// Send the complete message parts
					message.setContent(multipart);

					// Send message
					Transport.send(message);
					sentWithSuccess = true;
					System.out.println("Sent message successfully.... to : " + recipients);
				}
				catch (MessagingException mex)
				{
					System.err.println("Error while sending message! ....");
					mex.printStackTrace();
				}

			}
			else
			{
				System.err.println("Erreur : recipients addresses are not set !");
			}
		}
		else
		{
			System.err.println("Erreur : from address is not set !");
		}
	}

	public void sendContentWithType(String content, String messageType)
	{
		sentWithSuccess = false;
		if (from != null)
		{
			if (recipients != null && !recipients.isEmpty())
			{
				// OK - all environnement variable are ready for sending an email
				// Get system properties
				Properties properties = System.getProperties();

				// Setup mail server
				properties.setProperty("mail.smtp.host", getSmtp_host());

				// Get the default Session object.
				Session session = Session.getDefaultInstance(properties);

				try
				{
					// Create a default MimeMessage object.
					MimeMessage message = new MimeMessage(session);

					// Set From: header field of the header.
					message.setFrom(from);

					// Set To: header field of the header.
					message.addRecipients(Message.RecipientType.TO, recipients);

					// Set Subject: header field
					message.setSubject(subject);

					// Create the message part
					BodyPart messageBodyPart = new MimeBodyPart();
					// Fill the message
					messageBodyPart.setContent(content, messageType);

					// Create a multipart message
					Multipart multipart = new MimeMultipart();

					// Set text message part
					multipart.addBodyPart(messageBodyPart);

					if (attachedFiles.size() > 0)
					{
						// Part two is attachment for each files
						for (String filename : getAttachedFiles())
						{
							messageBodyPart = new MimeBodyPart();
							DataSource source = new FileDataSource(filename);
							messageBodyPart.setDataHandler(new DataHandler(source));
							messageBodyPart.setFileName(filename.substring(filename.lastIndexOf("\\")));
							multipart.addBodyPart(messageBodyPart);
						}
						// Send the complete message parts
						message.setContent(multipart);
					}
					else
					{
						// send only the HTML message without files attachments
						message.setContent(content, messageType);
					}
					// Send message
					Transport.send(message);
					sentWithSuccess = true;
					System.out.println("Sent message successfully.... to : " + recipients);
				}
				catch (MessagingException mex)
				{
					System.err.println("Error while sending message! ....");
					mex.printStackTrace();
				}

			}
			else
			{
				System.err.println("Erreur : recipients addresses are not set !");
			}
		}
		else
		{
			System.err.println("Erreur : from address is not set !");
		}
	}

	public static void main(String[] args)
	{
		// Recipient's email ID needs to be mentioned.
		String toPascal = "pascal.bey@franciaflex.com";
		String toGuillaume = "gnic@france-fermetures.fr";

		// Sender's email ID needs to be mentioned
		String from = toPascal;

		// Assuming you are sending email from localhost
		// String host = "localhost";

		// Get system properties
		Properties properties = System.getProperties();

		// Setup mail server
		properties.setProperty("mail.smtp.host", SetupUtils.INSTANCE.getMainSetup().getMailSmtpHost());

		// Get the default Session object.
		Session session = Session.getDefaultInstance(properties);

		try
		{
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(toPascal));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(toGuillaume));

			// Set Subject: header field
			message.setSubject("ARC Devis du jour");

			// Send the actual HTML message, as big as you like
			message.setContent("<h1>DEVIS No</h1>", "text/html");

			// Send message
			Transport.send(message);
			System.out.println("Sent message successfully....");
		}
		catch (MessagingException mex)
		{
			mex.printStackTrace();
		}
	}

	public void deleteFilesFromDrive(List<String> files2Attach)
	{
		for (String filePath : files2Attach)
		{
			File file = new File(filePath);
			if (file.exists())
			{
				file.delete();
			}
		}
	}

	public void sendMail(String emailSendFrom, String emailSendTo, String emailSendCCTo, String attachedFilesPath, String subject, String content)
	{
		try
		{
			setFrom(new InternetAddress(emailSendFrom));
			setRecipients(emailSendTo);
			setRecipientsCC(emailSendCCTo);
			setSubject(subject);

			List<MimeBodyPart> listBodyPart = new ArrayList<MimeBodyPart>();
			MimeBodyPart partText = new MimeBodyPart();
			try
			{
				partText.setText(content);
			}
			catch (MessagingException e)
			{
				e.printStackTrace();
			}
			listBodyPart.add(partText);
			setBodyPartContentList(listBodyPart);
			List<String> files2Attach = new ArrayList<String>();
			files2Attach.add(attachedFilesPath);
			setAttachedFiles(files2Attach);
			sendMultiPart();
			deleteFilesFromDrive(files2Attach);
			if (!isSentWithSuccess())
			{
				System.err.println("Echec de l'envoi du mail !!!");
			}
		}
		catch (AddressException e)
		{
			e.printStackTrace();
		}
	}

	public InternetAddress getFrom()
	{
		return from;
	}

	public void setFrom(InternetAddress from)
	{
		this.from = from;
	}

	public String getSmtp_host()
	{
		return SetupUtils.INSTANCE.getMainSetup().getMailSmtpHost();
	}

	public String getRecipients()
	{
		return recipients;
	}

	public String getRecipientsCC()
	{
		return recipientsCC;
	}

	public void setRecipients(String recipients)
	{
		this.recipients = recipients;
	}

	public void setRecipientsCC(String recipientsCC)
	{
		this.recipientsCC = recipientsCC;
	}

	public String getSubject()
	{
		return subject;
	}

	public void setSubject(String subject)
	{
		this.subject = subject;
	}

	public List<String> getAttachedFiles()
	{
		return attachedFiles;
	}

	public void setAttachedFiles(List<String> attachedFiles)
	{
		this.attachedFiles = attachedFiles;
	}

	public boolean isSentWithSuccess()
	{
		return sentWithSuccess;
	}

	public List<MimeBodyPart> getBodyPartContentList()
	{
		return bodyPartContentList;
	}

	public void setBodyPartContentList(List<MimeBodyPart> bodyPartContentList)
	{
		this.bodyPartContentList = bodyPartContentList;
	}
}