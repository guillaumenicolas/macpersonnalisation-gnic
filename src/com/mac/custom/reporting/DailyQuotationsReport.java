package com.mac.custom.reporting;

import static org.apache.commons.lang.ArrayUtils.*;

import java.io.File;
import java.io.FilenameFilter;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.SendFailedException;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.mac.custom.bo.MacCustomer;
import com.mac.custom.bo.MacQuotation;
import com.mac.custom.bo.MacWorkflowARC;
import com.mac.custom.services.beans.MacQuotationServicesBean;
import com.mac.custom.services.interfaces.MacQuotationServices;
import com.mac.custom.services.report.beans.MacGenericReportingServicesBean;
import com.mac.custom.services.report.interfaces.MacGenericReportingServices;
import com.netappsid.datatypes.MonetaryAmount;
import com.netappsid.erp.server.bo.Communication;
import com.netappsid.erp.server.bo.DocumentStatus;
import com.netappsid.erp.server.bo.Email;
import com.netappsid.erp.server.bo.ReportType;
import com.netappsid.erp.server.bo.SalesRep;
import com.netappsid.erp.server.bo.StatusHistory;
import com.netappsid.erp.server.mail.FileAsBytes;
import com.netappsid.erp.server.services.beans.EMailServiceBean;
import com.netappsid.erp.server.services.interfaces.EMailService;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.resources.Translator;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.Loader;
import com.netappsid.services.interfaces.local.PersistenceLocal;
import com.netappsid.utils.ExceptionUtils;
import com.sun.mail.smtp.SMTPAddressSucceededException;

public class DailyQuotationsReport
{
	public static String DATEFORMAT_FR = "dd/MM/yyyy";
	public static String DATEFORMAT_FILE = "yyyyMMdd";

	private static Logger logger = Logger.getLogger(DailyQuotationsReport.class);

	protected static DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
	protected static DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	protected static DateFormat dateFileFormat = new SimpleDateFormat("yyyyMMdd");
	protected static DateFormat dateTimeFileFormat = new SimpleDateFormat("yyyyMMdd-hhmmss");

	public static int NO_ERROR = 0;
	public static int ERROR_DATE = -1;

	public static String DAILY_QUOTATIONS = "Devis du jour";

	public static String DOCUMENTSTATUS_DEVISENVOYE = "DENV";

	private final Date dateDuTraitement;
	private String emailSendFrom = "advfaber.noreply@faberfrance.fr";

	private final ServiceLocator<Loader> loaderServiceLocator = new ServiceLocator<Loader>(LoaderBean.class);
	private final ServiceLocator<MacQuotationServices> macQuotationServiceLocator = new ServiceLocator<MacQuotationServices>(MacQuotationServicesBean.class);
	private final ServiceLocator<EMailService> emailServiceLocator = new ServiceLocator<EMailService>(EMailServiceBean.class);

	private final ServiceLocator<MacGenericReportingServices> macGenericReportingServiceServiceLocator = new ServiceLocator<MacGenericReportingServices>(
			MacGenericReportingServicesBean.class);

	@EJB(mappedName = "erp/PersistenceBean/local")
	public PersistenceLocal persistence;

	public DailyQuotationsReport()
	{
		dateDuTraitement = new Date();
		MacWorkflowARC workflowARC = loaderServiceLocator.get().findFirstByField(MacWorkflowARC.class, MacWorkflowARC.class,
				MacWorkflowARC.PROPERTYNAME_ACTIVE, true);
		if (workflowARC != null)
		{
			emailSendFrom = workflowARC.getEmailOrigin();
		}
	}

	protected void sendMailToCommercialEntity(Email email, String subject, String baseFolder, SalesRep commercial)
	{

		try
		{
			List<String> recipients = Arrays.asList(email.getValue());

			List<String> attachedFiles = null;
			if (baseFolder != null && !baseFolder.isEmpty())
			{
				attachedFiles = retrieveAttachedFileNamesToTreat(baseFolder, commercial, dateDuTraitement);
			}

			List<FileAsBytes> attachments = new ArrayList<FileAsBytes>();
			for (String filePath : attachedFiles)
			{
				File attachedFile = new File(filePath);
				FileAsBytes fileAsBytes = new FileAsBytes(attachedFile);
				attachments.add(fileAsBytes);
			}

			String content = initializeEmailContent(DAILY_QUOTATIONS, commercial, attachments);

			boolean emailSentSuccessfully = false;

			try
			{
				emailServiceLocator.get().sendMessage(emailSendFrom, recipients, subject, content, attachments);
			}
			catch (Exception e)
			{
				emailSentSuccessfully = processEmailException(e, "Email daily quotation", recipients);
			}

			if (emailSentSuccessfully)
			{
				List<String> macQuotationNumbers = new ArrayList<String>();
				for (FileAsBytes fileAsBytes : attachments)
				{
					String fileName = fileAsBytes.getName();
					String orderNumber = fileName.toUpperCase().replace(".PDF", "");
					macQuotationNumbers.add(orderNumber);
				}

				attachments.clear();

				macQuotationServiceLocator.get().setSentToCommercial(macQuotationNumbers, true);

				moveAttachedFilesToSuccessFolder(attachedFiles, baseFolder, commercial, dateDuTraitement);
			}
		}
		catch (Exception e)
		{
			logger.error(e, e);
		}
	}

	protected boolean processEmailException(Exception exception, CharSequence emailSentDescription, List<String> mailToAddresses)
	{
		Throwable cause = MessagingException.class.isAssignableFrom(exception.getClass()) ? exception : exception.getCause();
		boolean emailSentSuccesfully = false;

		if (SendFailedException.class.isInstance(cause))
		{
			SendFailedException sendFailedException = (SendFailedException) cause;
			Address[] validUnsent = sendFailedException.getValidUnsentAddresses();
			Address[] invalid = sendFailedException.getInvalidAddresses();
			Address[] valid = sendFailedException.getValidSentAddresses();

			StringBuilder builder = new StringBuilder();
			int nbValidSend = ArrayUtils.getLength(valid);

			int total = ArrayUtils.getLength(invalid) + ArrayUtils.getLength(validUnsent) + nbValidSend;

			if ((validUnsent != null && validUnsent.length > 0) || (invalid != null && invalid.length > 0))
			{
				builder.append((String.format(Translator.getString("TheEmailHasNotBeenSentToAddress"), emailSentDescription, total - nbValidSend, total,
						StringUtils.join(addAll(validUnsent, invalid), ", "))));
				logger.error(builder.toString());
			}
			else if (ExceptionUtils.getInnerThrowable(cause, SMTPAddressSucceededException.class) == null)
			{

				builder.append(String.format(Translator.getString("TheEmailHasNotBeenSent"), emailSentDescription));
				logger.error(builder.toString());
			}
			else
			{
				emailSentSuccesfully = true;
			}

		}
		else if (MessagingException.class.isInstance(cause))
		{
			logger.error(Translator.getString("ErrorInServerSmtpConfiguration"));
		}
		else
		{
			logger.error(String.format(Translator.getString("TheEmailHasNotBeenSentToAddress"), StringUtils.join(mailToAddresses, ";")));
			logger.error(cause, cause);
		}

		return emailSentSuccesfully;
	}

	private List<String> retrieveAttachedFileNamesToTreat(String baseFolder, SalesRep commercial, Date date)
	{
		List<String> attachedFileNames = new ArrayList<String>();

		String folder = getCommercialFolderToTreat(baseFolder, commercial, date);

		File directory = new File(folder);
		File[] fileList = directory.listFiles(new FilenameFilter()
			{
				@Override
				public boolean accept(File dir, String name)
				{
					return name.toLowerCase().endsWith(".pdf");
				}
			});
		if (fileList != null)
		{
			for (File file : fileList)
			{
				attachedFileNames.add(file.getAbsolutePath());
			}
		}
		return attachedFileNames;
	}

	private String initializeEmailContent(String whichContent, SalesRep commercial, List<FileAsBytes> attachments)
	{
		String content = "";
		if (whichContent.equals(DAILY_QUOTATIONS))
		{
			DateFormat df = new SimpleDateFormat(DATEFORMAT_FR);

			content = "<h3>FABER - Derniers DEVIS en date du : "
					+ df.format(dateDuTraitement)
					+ "</h3>"
					+ StringEscapeUtils.escapeHtml(commercial.getName())
					+ ", <br><br>"
					+ "Veuillez trouver ci-joint les derniers devis en date du jour."
					+ "<br>"
					+ StringEscapeUtils
							.escapeHtml("S'il manque un devis, veuillez demander \u00E0 votre ADV de passer le devis au statut \"Devis envoy\u00e9\". Merci.");

			content = "FABER - Derniers DEVIS en date du : " + df.format(dateDuTraitement) + "\n\n" + commercial.getName() + ", \n"
					+ "Veuillez trouver ci-joint les derniers devis en date du jour.\n" + "\nVoici les montants associ\u00E9s : \n\n";

			for (FileAsBytes fileAsBytes : attachments)
			{
				String quotationNumber = fileAsBytes.getName().toUpperCase().replace(".PDF", "");
				String customerName = findCustomerName(quotationNumber);
				MonetaryAmount quotationAmount = findQuotationAmount(quotationNumber);

				content += quotationNumber + " => " + (customerName == null ? "?" : customerName) + " : " + (quotationAmount == null ? "?" : quotationAmount)
						+ "\n";
			}
			content += "\nCordialement\n";
		}
		return content;
	}

	public void sendMailToCommercialEntities(String baseFolder, List<SalesRep> emailsAEnvoyerList)
	{
		for (SalesRep commercial : emailsAEnvoyerList)
		{
			Email email = null;
			if (commercial != null)
			{
				boolean emailFound = false;
				// List<Communication> communicationList =
				for (Communication communication : commercial.getCommunications())
				{
					if (communication instanceof Email)
					{
						emailFound = true;
						email = (Email) communication;
						DateFormat df = new SimpleDateFormat(DATEFORMAT_FR);
						sendMailToCommercialEntity(email, "Rapport des Devis du Jour " + df.format(dateDuTraitement) + " - envoi automatique", baseFolder,
								commercial);
						// System.out.println("EMAIL sent to : " + email.getValue());
					}
				}
				if (emailFound == false)
				{
					SalesRep commercialManager = commercial.getSalesManager();
					sendMailToADVNoEmailSetForCommercial(commercialManager, commercial, baseFolder);
				}
			}
		}
	}

	private void sendMailToADVNoEmailSetForCommercial(SalesRep commercialManager, SalesRep commercial, String baseFolder)
	{
		String sendTo = null;
		if (commercialManager != null && commercialManager.getCommunications() != null)
		{
			for (Communication communication : commercialManager.getCommunications())
			{
				if (communication.getClass().isAssignableFrom(Email.class))
				{
					sendTo = communication.getValue();
					break;
				}
			}
		}

		String subject = "Pas d'adresse Email pour un commercial";
		String content = "Bonjour, \nVeuillez SVP mettre \u00E0 jour le Commercial : " + commercial.getName()
				+ "\ncar il n'a pas d'adresse Email.\n\nEt veuillez SVP lui transmettre ces devis\n\n" + "Cordialement\nLe Service Informatique";

		if (sendTo == null)
		{
			sendTo = "pascal.bey@franciaflex.com";
			if (commercialManager != null)
			{
				content += "\n\nNote: Le responsable du commercial (ADV) " + commercialManager.getName() + " n'a pas de Email de configur�.";
			}
			else
			{
				content += "\n\nNote: Le commercial n'a pas de responsable d'attribu� (ADV).";
			}
		}

		List<String> recipients = Arrays.asList(sendTo);

		List<String> attachedFiles = null;
		if (baseFolder != null && !baseFolder.isEmpty())
		{
			attachedFiles = retrieveAttachedFileNamesToTreat(baseFolder, commercial, dateDuTraitement);
		}

		List<FileAsBytes> attachments = new ArrayList<FileAsBytes>();
		for (String filePath : attachedFiles)
		{
			File attachedFile = new File(filePath);
			FileAsBytes fileAsBytes = new FileAsBytes(attachedFile);
			attachments.add(fileAsBytes);
		}

		try
		{
			emailServiceLocator.get().sendMessage(emailSendFrom, recipients, subject, content, attachments);
		}
		catch (Exception e)
		{
			processEmailException(e, "No email found for sales rep", recipients);
		}
	}

	public String getCommercialFolderToTreat(String baseFolder, SalesRep commercial, Date date)
	{
		DateFormat df = new SimpleDateFormat(DATEFORMAT_FILE);
		String folder = baseFolder + "/PDFATraiter/" + df.format(date) + "/" + commercial.getName();
		return folder;
	}

	private String getCommercialFolderTreated(String baseFolder, SalesRep commercial, Date date)
	{
		DateFormat df = new SimpleDateFormat(DATEFORMAT_FILE);
		String folder = baseFolder + "/TraitementOK/" + df.format(date) + "/" + commercial.getName();
		return folder;
	}

	private void moveAttachedFilesToSuccessFolder(List<String> attachedFiles, String baseFolder, SalesRep commercial, Date date)
	{
		String folderSrc = getCommercialFolderToTreat(baseFolder, commercial, date);
		String folderDest = getCommercialFolderTreated(baseFolder, commercial, date);

		for (String filePath : attachedFiles)
		{
			File file = new File(filePath);
			File dest = new File(folderDest + "/" + file.getName());
			File destDir = new File(folderDest);
			if (!destDir.exists())
			{
				destDir.mkdirs();
			}
			file.renameTo(dest);
		}
		File srcFolder = new File(folderSrc);
		srcFolder.delete();
	}

	private List<Serializable> findQuotationsByStatusSentToCommercial()
	{
		final StringBuilder hqlQuery = new StringBuilder();
		String macQuotationClass = MacQuotation.class.getSimpleName();

		hqlQuery.append("SELECT " + macQuotationClass + "." + MacQuotation.PROPERTYNAME_ID).append("\n");
		hqlQuery.append("FROM " + macQuotationClass + " macQuotation").append("\n");
		hqlQuery.append("INNER JOIN macQuotation." + MacQuotation.PROPERTYNAME_STATUSHISTORIES + " statusHistory").append("\n");
		hqlQuery.append("INNER JOIN statusHistory." + StatusHistory.PROPERTYNAME_DOCUMENTSTATUS + " documentStatus").append("\n");
		hqlQuery.append("WHERE documentStatus." + DocumentStatus.PROPERTYNAME_CODE + " = :statutDevisEnvoye ").append("\n");
		hqlQuery.append("AND macQuotation." + MacQuotation.PROPERTYNAME_SENTTOCOMMERCIAL + " = false ").append("\n");

		List<String> parameterNames = new ArrayList<String>();
		List<Object> parameterValues = new ArrayList<Object>();
		parameterNames.add("statutDevisEnvoye");
		parameterValues.add(DOCUMENTSTATUS_DEVISENVOYE);

		List<Serializable> todaysQuotationList = loaderServiceLocator.get().findByQuery(Serializable.class, null, hqlQuery.toString(), parameterNames,
				parameterValues, -1);

		return todaysQuotationList;
	}

	public void reportDailyQuotationsAndSendEmails(String destFolder) throws Exception
	{
		List<Serializable> todaysQuotations = findQuotationsByStatusSentToCommercial();

		if (todaysQuotations == null || todaysQuotations.isEmpty())
			return;

		ReportType reportType = loaderServiceLocator.get()
				.findByField(ReportType.class, ReportType.class, DocumentStatus.PROPERTYNAME_INTERNALID, ReportType.QUOTATION_ID).get(0);

		if (reportType.getDefaultReport() != null)
		{
			String reportName = reportType.getDefaultReport().getDetails().get(0).getFilePath();
			List<SalesRep> emailsAEnvoyerList = new ArrayList<SalesRep>();

			if (todaysQuotations != null && !todaysQuotations.isEmpty())
			{
				for (Serializable macQuotationID : todaysQuotations)
				{
					List<SalesRep> salesReps = getMacGenericReportingServiceServiceLocator().generateQuotationToBeSent(macQuotationID, destFolder,
							dateDuTraitement, reportName);
					for (SalesRep salesRep : salesReps)
					{
						if (!emailsAEnvoyerList.contains(salesRep))
						{
							emailsAEnvoyerList.add(salesRep);
						}
					}
				}
				// send Emails
				sendMailToCommercialEntities(destFolder, emailsAEnvoyerList);
			}
		}

	}

	private String findCustomerName(String quotationNumber)
	{
		String object = null;
		if (quotationNumber != null)
		{
			final StringBuilder hqlQuery = new StringBuilder();
			hqlQuery.append("SELECT macCustomer." + MacCustomer.PROPERTYNAME_NAME).append("\n");
			hqlQuery.append("FROM " + MacQuotation.class.getSimpleName() + " macQuotation").append("\n");
			hqlQuery.append("INNER JOIN macQuotation." + MacQuotation.PROPERTYNAME_CUSTOMER + " macCustomer").append("\n");
			hqlQuery.append("WHERE macQuotation." + MacQuotation.PROPERTYNAME_NUMBER + " = :macQuotationNumber ");

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("macQuotationNumber", quotationNumber);

			object = loaderServiceLocator.get().findByQuery(hqlQuery.toString(), parameters, true);
		}

		return object;
	}

	private MonetaryAmount findQuotationAmount(String quotationNumber)
	{
		MonetaryAmount object = null;
		if (quotationNumber != null)
		{
			final StringBuilder hqlQuery = new StringBuilder();
			hqlQuery.append("SELECT macQuotation." + MacQuotation.PROPERTYNAME_TOTAL).append("\n");
			hqlQuery.append("FROM MacQuotation macQuotation").append("\n");
			hqlQuery.append("WHERE macQuotation." + MacQuotation.PROPERTYNAME_NUMBER + " = :macQuotationNumber ");

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("macQuotationNumber", quotationNumber);

			object = loaderServiceLocator.get().findByQuery(hqlQuery.toString(), parameters, true);
		}

		return object;
	}

	public MacGenericReportingServices getMacGenericReportingServiceServiceLocator()
	{
		return macGenericReportingServiceServiceLocator.get();
	}
}
