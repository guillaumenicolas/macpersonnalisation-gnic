package com.mac.custom.reporting;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.text.NumberFormatter;

import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.mac.custom.bo.MacManufacturedProduct;
import com.netappsid.erp.server.bo.DocumentStatus;
import com.netappsid.erp.server.bo.InventoryProduct;
import com.netappsid.erp.server.bo.ProductWarehouse;
import com.netappsid.erp.server.bo.Shipping;
import com.netappsid.erp.server.bo.User;
import com.netappsid.erp.server.bo.Warehouse;
import com.netappsid.erp.server.datatypes.GroupMeasureValue;
import com.netappsid.erp.server.services.beans.DocumentStatusServicesBean;
import com.netappsid.erp.server.services.interfaces.DocumentStatusServices;
import com.netappsid.framework.utils.ServiceLocator;
import com.netappsid.framework.utils.SystemVariable;
import com.netappsid.gui.utils.FormatterFactory;
import com.netappsid.services.beans.LoaderBean;
import com.netappsid.services.interfaces.Loader;

public class ReportUtils
{
	public static double getProductWarehouseQuantity(Serializable productId, Serializable warehouseId)
	{
		double quantity = 0d;
		if (productId != null && warehouseId != null)
		{
			ServiceLocator<Loader> loaderLocator = new ServiceLocator<Loader>(LoaderBean.class);

			List<Criterion> criteriaList = new ArrayList<Criterion>();
			List<String> subcriteriaList = new ArrayList<String>();
			List<Integer> joinSpecList = new ArrayList<Integer>();

			criteriaList.add(Restrictions.eq(Warehouse.PROPERTYNAME_ID, warehouseId));
			subcriteriaList.add(ProductWarehouse.PROPERTYNAME_WAREHOUSE);
			joinSpecList.add(CriteriaSpecification.INNER_JOIN);

			criteriaList.add(Restrictions.eq(InventoryProduct.PROPERTYNAME_ID, productId));
			subcriteriaList.add(ProductWarehouse.PROPERTYNAME_INVENTORYPRODUCT);
			joinSpecList.add(CriteriaSpecification.INNER_JOIN);
			List<ProductWarehouse> productWarehouses = loaderLocator.get().findByCriteria(ProductWarehouse.class, MacManufacturedProduct.class, criteriaList,
					subcriteriaList, joinSpecList);

			if (!productWarehouses.isEmpty())
			{
				GroupMeasureValue realQuantity = productWarehouses.get(0).getRealQuantityInStock();
				if (realQuantity != null)
				{
					quantity = realQuantity.getValue().doubleValue();
				}
			}
		}

		return quantity;
	}

	public static String getProductWarehouseQuantityString(Serializable productId, Serializable warehouseId)
	{
		String quantity = new String();
		if (productId != null && warehouseId != null)
		{
			ServiceLocator<Loader> loaderLocator = new ServiceLocator<Loader>(LoaderBean.class);

			List<Criterion> criteriaList = new ArrayList<Criterion>();
			List<String> subcriteriaList = new ArrayList<String>();
			List<Integer> joinSpecList = new ArrayList<Integer>();

			criteriaList.add(Restrictions.eq(Warehouse.PROPERTYNAME_ID, warehouseId));
			subcriteriaList.add(ProductWarehouse.PROPERTYNAME_WAREHOUSE);
			joinSpecList.add(CriteriaSpecification.INNER_JOIN);

			criteriaList.add(Restrictions.eq(InventoryProduct.PROPERTYNAME_ID, productId));
			subcriteriaList.add(ProductWarehouse.PROPERTYNAME_INVENTORYPRODUCT);
			joinSpecList.add(CriteriaSpecification.INNER_JOIN);
			List<ProductWarehouse> productWarehouses = loaderLocator.get().findByCriteria(ProductWarehouse.class, MacManufacturedProduct.class, criteriaList,
					subcriteriaList, joinSpecList);

			if (!productWarehouses.isEmpty())
			{
				GroupMeasureValue realQuantity = productWarehouses.get(0).getRealQuantityInStock();
				if (realQuantity != null)
				{
					DecimalFormat decimalFormat = new DecimalFormat();
					decimalFormat.setMinimumFractionDigits(2);
					decimalFormat.setMaximumFractionDigits(4);

					NumberFormatter numberFormatter = FormatterFactory.getEmptyNumberFormatter(decimalFormat, null);
					numberFormatter.setValueClass(BigDecimal.class);
					quantity = realQuantity.toString(numberFormatter);
				}
			}
		}

		return quantity;
	}

	public static void closeShipping(Serializable shippingId)
	{
		DocumentStatusServices documentStatusServicesLocator = new ServiceLocator<DocumentStatusServices>(DocumentStatusServicesBean.class).get();
		User user = SystemVariable.getVariable("user");
		documentStatusServicesLocator.setDocumentStatus(Arrays.asList(shippingId), Shipping.class, DocumentStatus.CLOSE, user != null ? user.getId() : null);
	}
}
