package com.mac.custom.workflow;

import java.util.HashSet;
import java.util.Set;

import com.mac.custom.bo.MacWorkflowMaster;
import com.mac.custom.services.workflow.bean.MacWorkflowServicesBean;
import com.mac.custom.services.workflow.interfaces.local.MacWorkflowServicesLocal;
import com.netappsid.erp.server.bo.Email;
import com.netappsid.erp.server.bo.Fax;
import com.netappsid.framework.utils.ServiceLocator;

public class WorkflowARCUtils
{
	/**
	 * Validatioln des rapports par defaut du client afin de valider si on a un ARC avec au minimum un contact valide pour r�ception de l'ARC par email ou fax
	 * 
	 * @param documentId
	 *            ID fu document de vente
	 * @return True ou false
	 */
	public static boolean isARCReportValid(String documentId)
	{
		ServiceLocator<MacWorkflowServicesLocal> wfServicesServiceLocator = new ServiceLocator<MacWorkflowServicesLocal>(MacWorkflowServicesBean.class);

		boolean result = false;
		
		try
		{
			result = wfServicesServiceLocator.get().isARCReportValid(documentId);
		}
		catch (Exception e)
		{
			// TODO
		}

		return result;
	}

	/**
	 * Retourne la liste des adresses emails des contacts du rapport ARC
	 * 
	 * @param documentId
	 *            Numero de la commande ou devis
	 * @return Liste des adresses email
	 */
	public static String getARCCommunicationsEmails(String documentId)
	{
		return getARCCommunications(documentId, Email.class);
	}

	/**
	 * Retourne la liste des numeros de fax des contacts du rapport ARC
	 * 
	 * @param documentId
	 *            Numero de la commande ou devis
	 * @return Liste des numeros de fax
	 */
	public static String getARCCommunicationsFax(String documentId)
	{
		return getARCCommunications(documentId, Fax.class);
	}

	/**
	 * Retourne la liste des numeros de fax/courriels des contacts du rapport ARC
	 * 
	 * @param documentId
	 *            Numero de la commande ou devis
	 * @param type
	 *            Fax ou Courriel
	 * @return Liste des numeros de fax/courriels
	 */
	private static String getARCCommunications(String documentId, Class type)
	{
		ServiceLocator<MacWorkflowServicesLocal> wfServicesServiceLocator = new ServiceLocator<MacWorkflowServicesLocal>(MacWorkflowServicesBean.class);

		String result = "";
		
		try
		{
			result = wfServicesServiceLocator.get().getARCCommunications(documentId, type);
		}
		catch (Exception e)
		{
			// TODO
		}

		return result;
	}

	/**
	 * Change le statut du document pour "ARC envoye"
	 * 
	 * @param documentId
	 *            ID du document
	 */
	public static void changeStatusARCSent(String documentId)
	{
		MacWorkflowMaster wfMaster = WorkflowUtils.getWFMaster();

		if (wfMaster != null)
		{
			WorkflowUtils.changeDocumentStatus(documentId, wfMaster.getWfARC().getArcSentStatus());
		}
	}

	/**
	 * Change le statut du document a "a exploser" si le statut est "ARC envoye"
	 * 
	 * @param documentId
	 *            ID du document
	 */
	public static void changeStatusToExplode(String documentId)
	{
		ServiceLocator<MacWorkflowServicesLocal> wfServicesServiceLocator = new ServiceLocator<MacWorkflowServicesLocal>(MacWorkflowServicesBean.class);

		try
		{
			wfServicesServiceLocator.get().changeStatusToExplode(documentId);
		}
		catch (Exception e)
		{
			// TODO
		}
	}

	/**
	 * Generation de l'edition et sauvegarde dans (disque et commande)
	 * 
	 * @param documentId
	 *            Numero de la commande ou du devis
	 * @return Nom du fichier genere
	 */
	public static String generateARCFile(String documentId)
	{
		ServiceLocator<MacWorkflowServicesLocal> wfServicesServiceLocator = new ServiceLocator<MacWorkflowServicesLocal>(MacWorkflowServicesBean.class);

		String result = "";
		try
		{
			result = wfServicesServiceLocator.get().generateARCFile(documentId);
		}
		catch (Exception e)
		{
			// Deja traite dans le service
		}

		return result;
	}

	/**
	 * Retourne la liste des adv du document et optionnellement les admins WF
	 * 
	 * @param documentId
	 *            ID du devis ou de la commande
	 * @param includeAdmins
	 *            Inclure les admins?
	 * @return Liste des acteurs
	 */
	public static Set<String> getADVsActorsForSaleOrder(String documentId, boolean addAdmins)
	{
		ServiceLocator<MacWorkflowServicesLocal> wfServicesServiceLocator = new ServiceLocator<MacWorkflowServicesLocal>(MacWorkflowServicesBean.class);

		Set<String> usersCode = new HashSet<String>();

		try
		{
			usersCode.addAll(wfServicesServiceLocator.get().getADVsActorsForSaleOrder(documentId, addAdmins));
		}
		catch (Exception e)
		{
			// TODO
		}

		return usersCode;		
	}
}
