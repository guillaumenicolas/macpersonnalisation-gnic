package com.mac.custom.workflow;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.mac.custom.bo.MacWorkflowMaster;
import com.mac.custom.services.workflow.bean.MacWorkflowServicesBean;
import com.mac.custom.services.workflow.interfaces.MacWorkflowServices;
import com.mac.custom.services.workflow.interfaces.local.MacWorkflowServicesLocal;
import com.netappsid.erp.server.bo.DocumentStatus;
import com.netappsid.framework.utils.ServiceLocator;

public class WorkflowUtils
{
	public static final String WF_ADMIN = "WF_ADMIN";
	public static final String EMAIL = "Courriel";
	public static final String FAX = "Fax";
	private static final String WFMASTER = "wfMaster";
	private static final Cache<String, MacWorkflowMaster> wfMasterCache = CacheBuilder.newBuilder().maximumSize(1).expireAfterAccess(1, TimeUnit.MINUTES)
			.build();

	/**
	 * Modifie le status d'un document de vente
	 * 
	 * @param documentId
	 *            Document UUID
	 * @param newStatus
	 *            Nouveau status
	 */
	public static void changeDocumentStatus(String documentId, String newStatus)
	{
		ServiceLocator<MacWorkflowServicesLocal> wfServicesServiceLocator = new ServiceLocator<MacWorkflowServicesLocal>(MacWorkflowServicesBean.class);

		try
		{
			wfServicesServiceLocator.get().changeDocumentStatus(documentId, newStatus);
		}
		catch (Exception e)
		{
			// TODO
		}
	}

	/**
	 * Modifie le status d'un document de vente
	 * 
	 * @param documentId
	 *            Document UUID
	 * @param newStatus
	 *            Nouveau status
	 */
	public static void changeDocumentStatus(String documentId, DocumentStatus newStatus)
	{
		ServiceLocator<MacWorkflowServicesLocal> wfServicesServiceLocator = new ServiceLocator<MacWorkflowServicesLocal>(MacWorkflowServicesBean.class);

		try
		{
			wfServicesServiceLocator.get().changeDocumentStatus(documentId, newStatus);
		}
		catch (Exception e)
		{
			// TODO
		}
	}

	/**
	 * Retourne le BO WFMaster
	 * 
	 * @return WFMaster
	 */
	public static MacWorkflowMaster getWFMaster()
	{
		ServiceLocator<MacWorkflowServices> wfServicesServiceLocator = new ServiceLocator<MacWorkflowServices>(MacWorkflowServicesBean.class);
		
		if (wfMasterCache.getIfPresent(WFMASTER) == null)
		{
			MacWorkflowMaster wfMaster = wfServicesServiceLocator.get().getWFMaster();
			
			if (wfMaster != null)
			{
				wfMasterCache.put(WFMASTER, wfMaster);
			}
		}

		return wfMasterCache.getIfPresent(WFMASTER);
	}

	/**
	 * Returns all users configured in Business Core
	 * 
	 * @return Set of users
	 */
	public static Set<String> getAllActors()
	{
		ServiceLocator<MacWorkflowServicesLocal> wfServicesServiceLocator = new ServiceLocator<MacWorkflowServicesLocal>(MacWorkflowServicesBean.class);

		Set<String> usersCode = new HashSet<String>();
		try
		{
			usersCode.addAll(wfServicesServiceLocator.get().getAllActors());
		}
		catch (Exception e)
		{
			// Deja traite dans le service
		}

		return usersCode;
	}

	/**
	 * Retourne une liste qui contient les utilisateurs du groupe passe en parametre et optionnellement les administrateurs
	 * 
	 * @param groupName
	 *            Nom du groupe
	 * @param addAdmins
	 *            Inclure les administrateurs?
	 * @return List d'utilisateurs
	 */
	public static Set<String> getAllActorsForGroup(String groupName, boolean addAdmins)
	{
		ServiceLocator<MacWorkflowServicesLocal> wfServicesServiceLocator = new ServiceLocator<MacWorkflowServicesLocal>(MacWorkflowServicesBean.class);

		Set<String> usersCode = new HashSet<String>();
		try
		{
			usersCode.addAll(wfServicesServiceLocator.get().getAllActorsForGroup(groupName, addAdmins));
		}
		catch (Exception e)
		{
			// Deja traite dans le service
		}

		return usersCode;
	}

	/**
	 * Retourne une liste qui contient l'utilisateur passe en parametre et optionnellement les administrateurs
	 * 
	 * @param userCode
	 *            Code utilisateur
	 * @param addAdmins
	 *            Inclure les administrateurs?
	 * @return List d'utilisateurs
	 */
	public static Set<String> getSpecificActor(String userCode, boolean addAdmins)
	{
		Set<String> usersCode = new HashSet<String>();

		usersCode.add(userCode);

		if (addAdmins)
		{
			return addWFAdmin(usersCode);
		}
		else
		{
			return usersCode;
		}
	}

	/**
	 * Ajoute les membres de WF_ADMIN aux utilisateurs
	 * 
	 * @param currentUsers
	 *            Liste courrante
	 * @return Liste avec lles administrateurs
	 */
	protected static Set<String> addWFAdmin(Set<String> currentUsers)
	{
		ServiceLocator<MacWorkflowServicesLocal> wfServicesServiceLocator = new ServiceLocator<MacWorkflowServicesLocal>(MacWorkflowServicesBean.class);

		Set<String> usersCode = new HashSet<String>();
		try
		{
			usersCode.addAll(wfServicesServiceLocator.get().addWFAdmin(currentUsers));
		}
		catch (Exception e)
		{
			// Deja traite dans le service
		}

		return usersCode;
	}

	/**
	 * Retourne la liste des adresses emails des ADV associée au document
	 * 
	 * @param documentId
	 *            ID de la commande ou devis
	 * @return Liste des adresses email
	 */
	public static String getADVsEmailAddressesForSaleOrder(String documentId)
	{
		ServiceLocator<MacWorkflowServicesLocal> wfServicesServiceLocator = new ServiceLocator<MacWorkflowServicesLocal>(MacWorkflowServicesBean.class);

		String result = "";
		
		try
		{
			result = wfServicesServiceLocator.get().getADVsEmailAddressesForSaleOrder(documentId);
		}
		catch (Exception e)
		{
			// TODO
		}

		return result;
	}

	/**
	 * Retourne la liste des adresses emails des ATC associée au document
	 * 
	 * @param documentId
	 *            ID de la commande ou devis
	 * @return Liste des adresses email
	 */
	public static String getATCsEmailAddressesForSaleOrder(String documentId)
	{
		ServiceLocator<MacWorkflowServicesLocal> wfServicesServiceLocator = new ServiceLocator<MacWorkflowServicesLocal>(MacWorkflowServicesBean.class);

		String result = "";
		
		try
		{
			result = wfServicesServiceLocator.get().getATCsEmailAddressesForSaleOrder(documentId);
		}
		catch (Exception e)
		{
			// TODO
		}

		return result;
	}

	/**
	 * Retourne la liste des adresses emails des ADV associée au client
	 * 
	 * @param customerId
	 *            ID du client
	 * @return Liste des adresses email
	 */
	public static String getADVsEmailAddressesForCustomer(String customerId)
	{
		ServiceLocator<MacWorkflowServicesLocal> wfServicesServiceLocator = new ServiceLocator<MacWorkflowServicesLocal>(MacWorkflowServicesBean.class);

		String result = "";
		
		try
		{
			result = wfServicesServiceLocator.get().getADVsEmailAddressesForCustomer(customerId);
		}
		catch (Exception e)
		{
			// TODO
		}

		return result;
	}

	/**
	 * Retourne la liste des adresses emails des ATC associée au client
	 * 
	 * @param customerId
	 *            ID du client
	 * @return Liste des adresses email
	 */
	public static String getATCsEmailAddressesForCustomer(String customerId)
	{
		ServiceLocator<MacWorkflowServicesLocal> wfServicesServiceLocator = new ServiceLocator<MacWorkflowServicesLocal>(MacWorkflowServicesBean.class);

		String result = "";
		
		try
		{
			result = wfServicesServiceLocator.get().getATCsEmailAddressesForCustomer(customerId);
		}
		catch (Exception e)
		{
			// TODO
		}

		return result;
	}
}
