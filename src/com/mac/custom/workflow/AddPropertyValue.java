package com.mac.custom.workflow;

import java.awt.event.ActionEvent;

import javax.inject.Inject;

import org.xml.sax.Attributes;

import com.mac.custom.bo.MacWorkflowARCProductDelay;
import com.netappsid.action.AbstractBoundAction;
import com.netappsid.action.event.ActionNotifyEvent;
import com.netappsid.component.DocumentPane;
import com.netappsid.component.DocumentPaneDelegate;
import com.netappsid.component.FormDialog;
import com.netappsid.component.FormDialog.ButtonType;
import com.netappsid.component.swing.adapter.DocumentPaneAdapter;
import com.netappsid.erp.server.bo.Property;
import com.netappsid.erp.server.bo.PropertyValueModel;
import com.netappsid.erp.server.factory.PropertyValueFactory;
import com.netappsid.framework.gui.components.IntelligentPanel;
import com.netappsid.framework.gui.components.model.NAIDPresentationModel;
import com.netappsid.framework.gui.components.model.NAIDPresentationModelFactory;
import com.netappsid.gui.utils.WindowUtils.WindowPosition;
import com.netappsid.resources.icons.Icon;
import com.netappsid.resources.icons.IconSize;
import com.netappsid.undoredo.UndoRedoManager;

public class AddPropertyValue extends AbstractBoundAction
{
	private static String PROPERTYVALUEMODEL = "propertyValueModel";
	private NAIDPresentationModelFactory naidPresentationModelFactory;

	public AddPropertyValue(Attributes attributes)
	{
		super(attributes);
		putValue(PROPERTY_ICON, Icon.ADD.getIcon(IconSize.SMALL));
		setToolTipText("addDetail");
	}

	@Override
	public void doAction(ActionEvent arg0)
	{
		if (fireBefore(new ActionNotifyEvent(this, getModel(), getModel().getBean())))
		{
			MacWorkflowARCProductDelay delay = (MacWorkflowARCProductDelay) getModel().getBean();
	
			// We need to manually create the IntelligentPanel in order to specify
			// the same PresentationModel as the current form
			UndoRedoManager undoRedoManager = getIntelligentPanel().getUndoRedoManager();
			DocumentPane intelligentPanel = createFormIntelligentPanel(undoRedoManager);
	
			if (delay.getProperty() != null)
			{
				FormDialog dialog = getComponentFactory().newFormDialog(intelligentPanel, PROPERTYVALUEMODEL, this.getClass().getClassLoader(), ButtonType.OK,
						ButtonType.CANCEL);
				NAIDPresentationModel presentationModel = dialog.getDocumentPane().getPresentationModel(PROPERTYVALUEMODEL);

				PropertyValueModel propertyValueModel = new PropertyValueModel();
				propertyValueModel.setProperty(delay.getProperty());
				presentationModel.setBean(propertyValueModel);

				dialog.setParentModel(getModel());
				dialog.setModel(presentationModel);
				dialog.setPosition(WindowPosition.CENTER);
				dialog.setTitle(getLocalizedTitle());
				dialog.setModal(true);
				dialog.setVisible(true);

				if (dialog.getSelectedButton() == ButtonType.OK)
				{
					Object value = ((PropertyValueModel) presentationModel.getBean()).getValue();
					if (value != null)
					{
						getModel().setValue(MacWorkflowARCProductDelay.PROPERTYNAME_PROPERTYVALUE, PropertyValueFactory.INSTANCE.create((Property) getModel().getModel(MacWorkflowARCProductDelay.PROPERTYNAME_PROPERTY).getValue(), false, value));
					}
					else
					{
						getModel().setValue(MacWorkflowARCProductDelay.PROPERTYNAME_PROPERTYVALUE, null);
					}
					
					fireAfter(new ActionNotifyEvent(this, getModel(), getModel().getBean()));
				}

				dialog.dispose();
			}
			else
			{
				getModel().setValue(MacWorkflowARCProductDelay.PROPERTYNAME_PROPERTYVALUE, null);
				
				fireAfter(new ActionNotifyEvent(this, getModel(), getModel().getBean()));
			}
		}
	}

	/**
	 * Creates an IntelligentPanel that will be used as root panel for the FormDialog
	 */
	protected DocumentPane createFormIntelligentPanel(UndoRedoManager undoRedoManager)
	{
		// TODO ComponentFactory
		final DocumentPaneDelegate documentPaneDelegate = new DocumentPaneDelegate(getNaidPresentationModelFactory(), undoRedoManager, false);
		return new DocumentPaneAdapter(new IntelligentPanel(documentPaneDelegate));
	}

	protected NAIDPresentationModelFactory getNaidPresentationModelFactory()
	{
		return naidPresentationModelFactory;
	}

	@Inject
	public void setNaidPresentationModelFactory(NAIDPresentationModelFactory naidPresentationModelFactory)
	{
		this.naidPresentationModelFactory = naidPresentationModelFactory;
	}
}