package com.mac.custom.workflow;

import java.util.HashSet;
import java.util.Set;

import com.mac.custom.services.workflow.bean.MacWorkflowServicesBean;
import com.mac.custom.services.workflow.interfaces.local.MacWorkflowServicesLocal;
import com.netappsid.framework.utils.ServiceLocator;

public class WorkflowUnblockAutoUtils 
{
	/**
	 * Retourne la liste des adv du document et optionnellement les admins WF
	 * @param documentId ID du devis ou de la commande
	 * @param includeAdmins Inclure les admins?
	 * @return Liste des acteurs
	 */
	public static Set<String> getADVsActorsForSaleOrder(String documentId, boolean addAdmins)
	{
		ServiceLocator<MacWorkflowServicesLocal> wfServicesServiceLocator = new ServiceLocator<MacWorkflowServicesLocal>(MacWorkflowServicesBean.class);

		Set<String> usersCode = new HashSet<String>();

		try
		{
			usersCode.addAll(wfServicesServiceLocator.get().getADVsActorsForSaleOrder(documentId, addAdmins));
		}
		catch (Exception e)
		{
			// TODO
		}

		return usersCode;
	}
}
