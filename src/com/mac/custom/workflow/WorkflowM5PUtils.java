package com.mac.custom.workflow;

import java.util.HashSet;
import java.util.Set;

import com.mac.custom.bo.MacWorkflowMaster;
import com.mac.custom.services.workflow.bean.MacWorkflowServicesBean;
import com.mac.custom.services.workflow.interfaces.local.MacWorkflowServicesLocal;
import com.netappsid.framework.utils.ServiceLocator;

public class WorkflowM5PUtils
{
	/**
	 * Change le statut du document pour "Bloqué M5P"
	 * 
	 * @param documentId
	 *            ID du document
	 */
	public static void changeStatusM5PBlocked(String documentId)
	{
		MacWorkflowMaster wfMaster = WorkflowUtils.getWFMaster();

		if (wfMaster != null)
		{
			// WorkflowUtils.changeDocumentStatus(documentId, wfMaster.getWfM5P().getBlockedStatus());
		}
	}

	/**
	 * Change le statut du document pour "débloqué M5P"
	 * 
	 * @param documentId
	 *            ID du document
	 */
	public static void changeStatusM5PUnblocked(String documentId)
	{
		MacWorkflowMaster wfMaster = WorkflowUtils.getWFMaster();

		if (wfMaster != null)
		{
			// WorkflowUtils.changeDocumentStatus(documentId, wfMaster.getWfM5P().getUnblockedStatus());
		}
	}

	/**
	 * Change le statut du document pour "M5P refusé"
	 * 
	 * @param documentId
	 *            ID du document
	 */
	public static void changeStatusM5PRefused(String documentId)
	{
		MacWorkflowMaster wfMaster = WorkflowUtils.getWFMaster();

		if (wfMaster != null)
		{
			WorkflowUtils.changeDocumentStatus(documentId, wfMaster.getWfM5P().getRefusedStatus());
		}
	}

	/**
	 * Flag le détail comme étant accepté M5p ou refusé M5P
	 * 
	 * @param id
	 *            ID du détail
	 * @param isAccepted
	 *            Accepte ou refuse
	 * @param internalNote
	 *            Internal note
	 * @param comment
	 *            Commentaire du detail de commande
	 * @param confirmationNumer
	 *            Numéro de confirmation (optionnel)
	 */
	public static void setDetailM5PAccepted(boolean isAccepted, String id, String internalNote, String comment, String confirmationNumber)
	{
		ServiceLocator<MacWorkflowServicesLocal> wfServicesServiceLocator = new ServiceLocator<MacWorkflowServicesLocal>(MacWorkflowServicesBean.class);

		try
		{
			wfServicesServiceLocator.get().setDetailM5PAccepted(isAccepted, id, internalNote, comment, confirmationNumber);
		}
		catch (Exception e)
		{
			// TODO
		}
	}

	/**
	 * Flag le détail comme étant accepté M5p ou refusé M5P
	 * 
	 * @param id
	 *            ID du détail
	 * @param isAccepted
	 *            Accepte ou refuse
	 * @param internalNote
	 *            Internal note
	 * @param confirmationNumer
	 *            Numéro de confirmation (optionnel)
	 */
	public static void setDetailM5PAccepted(boolean isAccepted, String id, String internalNote, String confirmationNumber)
	{
		setDetailM5PAccepted(isAccepted, id, internalNote, null, confirmationNumber);
	}

	/**
	 * Permet de modifier le prix brut du détail
	 * 
	 * @param id
	 *            ID du détail
	 * @param price
	 *            Prix brut
	 */
	public static void setDetailPrice(String id, String price)
	{
		ServiceLocator<MacWorkflowServicesLocal> wfServicesServiceLocator = new ServiceLocator<MacWorkflowServicesLocal>(MacWorkflowServicesBean.class);

		try
		{
			wfServicesServiceLocator.get().setDetailPrice(id, price);
		}
		catch (Exception e)
		{
			// TODO
		}
	}

	/**
	 * Est-ce qu'il y a au minimum un detail de type M5P non valide ?
	 * 
	 * @param id
	 *            Numero du document
	 * @return Valide ou non
	 */
	public static boolean saleOrderDetailsAllValid(String id)
	{
		ServiceLocator<MacWorkflowServicesLocal> wfServicesServiceLocator = new ServiceLocator<MacWorkflowServicesLocal>(MacWorkflowServicesBean.class);

		boolean result = false;
		
		try
		{
			result = wfServicesServiceLocator.get().saleOrderDetailsAllValid(id);
		}
		catch (Exception e)
		{
			// TODO
		}

		return result;
	}

	/**
	 * Retourne l'identifiant unique du détail de commande
	 * 
	 * @param id
	 *            ID du détail de commande
	 * @return Identifiant unique
	 */
	public static String getTransactionDetailIdentifier(String id)
	{
		ServiceLocator<MacWorkflowServicesLocal> wfServicesServiceLocator = new ServiceLocator<MacWorkflowServicesLocal>(MacWorkflowServicesBean.class);

		String result = "";
		
		try
		{
			result = wfServicesServiceLocator.get().getTransactionDetailIdentifier(id);
		}
		catch (Exception e)
		{
			// TODO
		}

		return result;
	}

	/**
	 * Retourne la liste des adv du document et optionnellement les admins WF
	 * 
	 * @param documentId
	 *            ID du devis ou de la commande
	 * @param includeAdmins
	 *            Inclure les admins?
	 * @return Liste des acteurs
	 */
	public static Set<String> getADVsActorsForSaleOrder(String documentId, boolean addAdmins)
	{
		ServiceLocator<MacWorkflowServicesLocal> wfServicesServiceLocator = new ServiceLocator<MacWorkflowServicesLocal>(MacWorkflowServicesBean.class);

		Set<String> usersCode = new HashSet<String>();

		try
		{
			usersCode.addAll(wfServicesServiceLocator.get().getADVsActorsForSaleOrder(documentId, addAdmins));
		}
		catch (Exception e)
		{
			// TODO
		}

		return usersCode;
	}

}
