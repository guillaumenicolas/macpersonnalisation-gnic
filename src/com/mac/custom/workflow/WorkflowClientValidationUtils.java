package com.mac.custom.workflow;

import java.util.HashSet;
import java.util.Set;

import com.mac.custom.services.workflow.bean.MacWorkflowServicesBean;
import com.mac.custom.services.workflow.interfaces.local.MacWorkflowServicesLocal;
import com.netappsid.erp.server.bo.Email;
import com.netappsid.erp.server.bo.Fax;
import com.netappsid.framework.utils.ServiceLocator;

public class WorkflowClientValidationUtils 
{
	/**
	 * Est-ce que les rapports par defaut du client sont valide pour la validation client
	 * @param customerId ID du client
	 * @return Valide?
	 */
	public static boolean isReportValid(String customerId)
	{
		ServiceLocator<MacWorkflowServicesLocal> wfServicesServiceLocator = new ServiceLocator<MacWorkflowServicesLocal>(MacWorkflowServicesBean.class);

		boolean result = false;

		try
		{
			result = wfServicesServiceLocator.get().isReportValid(customerId);
		}
		catch (Exception e)
		{
			// TODO
		}

		return result;
	}
	
	/**
	 * Retourne la liste des adv du document et optionnellement les admins WF
	 * @param customerId ID du client
	 * @param addAdmins On ajoute les admins?
	 * @return Liste des acteurs
	 */
	public static Set<String> getADVsActorsForCustomer(String customerId, boolean addAdmins)
	{
		ServiceLocator<MacWorkflowServicesLocal> wfServicesServiceLocator = new ServiceLocator<MacWorkflowServicesLocal>(MacWorkflowServicesBean.class);

		Set<String> usersCode = new HashSet<String>();

		try
		{
			usersCode.addAll(wfServicesServiceLocator.get().getADVsActorsForCustomer(customerId, addAdmins));
		}
		catch (Exception e)
		{
			// TODO
		}

		return usersCode;
	}
	
	/**
	 * Generation du rapport envoye au client
	 * @param clientId ID du client
	 * @return Nom du fichier genere
	 */
	public static String generateClientFile(String clientId)
	{
		ServiceLocator<MacWorkflowServicesLocal> wfServicesServiceLocator = new ServiceLocator<MacWorkflowServicesLocal>(MacWorkflowServicesBean.class);
		
		String result = "";
		try
		{
			result = wfServicesServiceLocator.get().generateClientFile(clientId);	
		}
		catch(Exception e)
		{
			//Deja traite dans le service
		}
		
		return result;
	}
	
	/**
	 * Retourne la liste des adresses emails des contacts du rapport client
	 * @param customerId ID du client
	 * @return Liste des adresses email
	 */
	public static String getCommunicationsEmails(String customerId)
	{
		return getCommunications(customerId, Email.class);
	}
	
	/**
	 * Retourne la liste des numeros de fax des contacts du rapport client
	 * @param customerId ID du client
	 * @return Liste des numeros de fax
	 */
	public static String getCommunicationsFax(String customerId)
	{
		return getCommunications(customerId, Fax.class);
	}
	
	/**
	 * Retourne la liste des numeros de fax/courriels des contacts du rapport client
	 * @param customerId ID du client
	 * @param type Fax ou Courriel
	 * @return Liste des numeros de fax/courriels
	 */
	private static String getCommunications(String customerId, Class type)
	{
		ServiceLocator<MacWorkflowServicesLocal> wfServicesServiceLocator = new ServiceLocator<MacWorkflowServicesLocal>(MacWorkflowServicesBean.class);

		String result = "";
		
		try
		{
			result = wfServicesServiceLocator.get().getCommunications(customerId, type);
		}
		catch (Exception e)
		{
			// TODO
		}

		return result;
	}
	
}
