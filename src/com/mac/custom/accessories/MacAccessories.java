package com.mac.custom.accessories;

import java.util.List;

import com.mac.custom.bo.MacOrder;
import com.netappsid.erp.server.bo.Enumeration;
import com.netappsid.erp.server.bo.OrderDetail;
import com.netappsid.erp.server.bo.Property;
import com.netappsid.erp.server.bo.PropertyValue;

public class MacAccessories
{
	// calcul de la quantite de cle Allen
	@SuppressWarnings("rawtypes")
	public static Integer getQteCleAllen(MacOrder order)
	{
		// Il faut 1 cle allen pour 10 stores.
		int nbStore = 10;

		List<OrderDetail> details = order.getDetails();
		int nbCleAllen = 0;
		if (details != null)
		{
			int nbCloisoon = 0;
			for (OrderDetail detail : details)
			{
				String productCode = detail.getProduct().getCode();
				if (productCode != null && productCode.equals("VEN"))
				{
					// Cherche si la description du produit venitien contient le mot cloisoon
					if (detail.getLocalizedDescription() != null && detail.getLocalizedDescription().contains("Cloisoon"))
					{
						nbCloisoon += detail.getQuantityValue().intValue();
					}
					// Sinon dans les autres cas, on v�rifie si la sous famille de remise est celle du Cloisoon
					else if (detail.getProperties() != null)
					{
						for (PropertyValue propertyValue : detail.getProperties())
						{
							Property property = propertyValue.getProperty();
							if (property.getCode() != null && property.getCode().equals("typeModele") && propertyValue.getValue() != null)
							{
								Enumeration enumValue = (Enumeration) propertyValue.getValue();
								if (enumValue != null && enumValue.getCode().contains("ven_alu_cloisoon"))
									nbCloisoon += detail.getQuantityValue().intValue();
							}
						}
					}
				}
			}
			if (nbCloisoon > 0)
			{
				nbCleAllen = (int) Math.ceil(nbCloisoon / nbStore) + 1;
			}
		}

		return nbCleAllen;
	}
}