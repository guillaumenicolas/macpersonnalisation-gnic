
--This will cause the transaction to rollback automatically in case of an error
SET XACT_ABORT ON

--We'll do this in a transaction so that a failure won't leave the db in a half-deleted state
BEGIN TRANSACTION
GO

DELETE FROM MacSchema.MacOrder
GO
DELETE FROM MacSchema.MacQuotation
GO

COMMIT TRANSACTION
GO