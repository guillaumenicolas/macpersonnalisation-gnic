CREATE SCHEMA MacSchema;
GO
CREATE TABLE MacSchema.MacCustomer
    (
        id uniqueIdentifier NOT NULL				 ,
        macRegionCommerciale_id uniqueIdentifier NULL,
        PRIMARY KEY (id)
    );
GO    
ALTER TABLE MacSchema.MacCustomer ADD CONSTRAINT FKD710722DB34F3B0F FOREIGN KEY (id) REFERENCES EuropeCustomSchema.CustomerEuro;
GO
INSERT INTO MacSchema.MacCustomer ( id, macRegionCommerciale_id ) SELECT id,NULL FROM Customer;
GO
CREATE TABLE MacSchema.MacRegionCommerciale
    (
        id uniqueIdentifier NOT NULL,
        code nvarchar(255) NULL     ,
        PRIMARY KEY (id)			,
        UNIQUE (code)
    );
GO
ALTER TABLE MacSchema.MacCustomer ADD CONSTRAINT FKD710722D7AD88A2A FOREIGN KEY (macRegionCommerciale_id) REFERENCES MacSchema.MacRegionCommerciale;
GO
CREATE TABLE MacSchema.MacRegionCommercialeDescription
    (
        id uniqueIdentifier NOT NULL                 ,
        languageCode nvarchar(255) NULL              ,
        value        nvarchar(MAX) NULL              ,
        macRegionCommerciale_id uniqueIdentifier NULL,
        PRIMARY KEY (id)                             ,
        UNIQUE (macRegionCommerciale_id, languageCode)
    );
GO
CREATE INDEX macRegionCommerciale_description_x ON MacSchema.MacRegionCommercialeDescription (macRegionCommerciale_id);
GO
ALTER TABLE MacSchema.MacRegionCommercialeDescription ADD CONSTRAINT FK7E99F847AD88A2A FOREIGN KEY (macRegionCommerciale_id) REFERENCES MacSchema.MacRegionCommerciale;
GO

--Code Prospect Column (lien entre CRM et 360)
ALTER TABLE MacSchema.MacCustomer ADD codeProspect NVARCHAR(255) NULL;
GO

-- Type Client
ALTER TABLE MacSchema.MacCustomer ADD typeClient NVARCHAR(255) NULL;
GO
UPDATE MacSchema.MacCustomer SET typeClient = 'CLIENT';
GO
ALTER TABLE MacSchema.MacCustomer ALTER COLUMN typeClient NVARCHAR(255) NOT NULL;


------ VERSION 1.1.0 RELEASED ------


ALTER TABLE MacSchema.MacCustomer ADD categorieCA NVARCHAR(255) null
GO
ALTER TABLE MacSchema.MacCustomer ADD objectifCA_amount NUMERIC(32,2) null
GO
ALTER TABLE MacSchema.MacCustomer ADD objectifCA_currency NVARCHAR(255) null
GO
ALTER TABLE MacSchema.MacCustomer ADD objectifCA_transactionCurrency NVARCHAR(255) null
GO
ALTER TABLE MacSchema.MacCustomer ADD objectifCA_exchangeRate NUMERIC(19,10) null
GO

-- Typologie
create table MacSchema.ActiviteClient (id uniqueIdentifier not null, code nvarchar(25) not null, upperCode nvarchar(25) not null, primary key (id), unique (code), unique (upperCode));
GO
create table MacSchema.ActiviteClientDescription (id uniqueIdentifier not null, languageCode nvarchar(255) null, value nvarchar(255) null, activiteClient_id uniqueIdentifier null, primary key (id), unique (activiteClient_id, languageCode));
GO
create table MacSchema.EnseigneClient (id uniqueIdentifier not null, code nvarchar(25) not null, upperCode nvarchar(25) not null, primary key (id), unique (code), unique (upperCode));
GO
create table MacSchema.EnseigneClientDescription (id uniqueIdentifier not null, languageCode nvarchar(255) null, value nvarchar(255) null, enseigneClient_id uniqueIdentifier null, primary key (id), unique (enseigneClient_id, languageCode));
GO
alter table MacSchema.ActiviteClientDescription add constraint ActiviteCliDes_activiteClient_id_fkey foreign key (activiteClient_id) references MacSchema.ActiviteClient;
GO
alter table MacSchema.EnseigneClientDescription add constraint EnseigneCliDes_enseigneClient_id_fkey foreign key (enseigneClient_id) references MacSchema.EnseigneClient;
GO

create table MacSchema.MacCommercialClass (id uniqueIdentifier not null, primary key (id));
GO
create table MacSchema.MacCommercialClass_ActiviteClient (MacCommercialClass_id uniqueIdentifier not null, activitesClient_id uniqueIdentifier not null, sequence int not null, primary key (MacCommercialClass_id, sequence));
GO
create table MacSchema.MacCommercialClass_EnseigneClient (MacCommercialClass_id uniqueIdentifier not null, enseignesClient_id uniqueIdentifier not null, sequence int not null, primary key (MacCommercialClass_id, sequence));
GO
alter table MacSchema.MacCommercialClass_ActiviteClient add constraint MacComClass_ActiviteCli_MacCommercialClass_id_fkey foreign key (MacCommercialClass_id) references MacSchema.MacCommercialClass;
GO
alter table MacSchema.MacCommercialClass_ActiviteClient add constraint MacComClass_ActiviteCli_activitesClient_id_fkey foreign key (activitesClient_id) references MacSchema.ActiviteClient;
GO
alter table MacSchema.MacCommercialClass_EnseigneClient add constraint MacComClass_EnseigneCli_MacCommercialClass_id_fkey foreign key (MacCommercialClass_id) references MacSchema.MacCommercialClass;
GO
alter table MacSchema.MacCommercialClass_EnseigneClient add constraint MacComClass_EnseigneCli_enseignesClient_id_fkey foreign key (enseignesClient_id) references MacSchema.EnseigneClient;
GO
alter table MacSchema.MacCommercialClass add constraint MacComClass_id_fkey foreign key (id) references CommercialClass;
GO

insert into MacSchema.MacCommercialClass select id from CommercialClass;
GO
ALTER TABLE MacSchema.MacCustomer ADD activiteClient_id uniqueIdentifier null;
GO
ALTER TABLE MacSchema.MacCustomer ADD enseigneClient_id uniqueIdentifier null;
GO
alter table MacSchema.MacCustomer add constraint MacCus_activiteClient_id_fkey foreign key (activiteClient_id) references MacSchema.ActiviteClient;
GO
alter table MacSchema.MacCustomer add constraint MacCus_enseigneClient_id_fkey foreign key (enseigneClient_id) references MacSchema.EnseigneClient;
GO
create index activiteClient_description_x on MacSchema.ActiviteClientDescription (activiteClient_id);
GO
create index enseigneClient_description_x on MacSchema.EnseigneClientDescription (enseigneClient_id);
GO


---- v1.2.0 ----

ALTER TABLE MacSchema.MacCustomer ADD ancienneCotation nvarchar(255) null
GO
ALTER TABLE MacSchema.MacCustomer ADD codeNAF nvarchar(255) null
GO
ALTER TABLE MacSchema.MacCustomer ADD codeSolvabilite nvarchar(255) null
GO
ALTER TABLE MacSchema.MacCustomer ADD miseAJourCotation datetime null
GO
ALTER TABLE MacSchema.MacCustomer ADD nouvelleCotation nvarchar(255) null
GO
ALTER TABLE MacSchema.MacCustomer ADD analysteCredit_id uniqueIdentifier null
GO
ALTER TABLE MacSchema.MacCustomer ADD enCoursA_id uniqueIdentifier null
GO 
ALTER TABLE MacSchema.MacCustomer add constraint macCust_enCoursA_id_fkey foreign key (enCoursA_id) references Customer
GO
ALTER TABLE MacSchema.MacCustomer add constraint macCust_analtCre_id_fkey foreign key (analysteCredit_id) references Users
GO

---- fix commercialclass ON INSTALLLED 1.1.0 VERSION (FABER ONLY) ----

alter table MacSchema.CommercialClass_ActiviteClient drop constraint CommercialCla_ActiviteCli_activitesClient_id_fkey
GO
alter table MacSchema.CommercialClass_ActiviteClient drop constraint CommercialCla_ActiviteCli_CommercialClass_id_fkey
GO
alter table MacSchema.CommercialClass_EnseigneClient drop constraint CommercialCla_EnseigneCli_enseignesClient_id_fkey
GO
alter table MacSchema.CommercialClass_EnseigneClient drop constraint CommercialCla_EnseigneCli_CommercialClass_id_fkey
GO
drop table MacSchema.CommercialClass_ActiviteClient
GO
drop table MacSchema.CommercialClass_EnseigneClient
GO
---- trouver la contrainte d'unicite ----
ALTER TABLE MacSchema.MacCommercialClass_ActiviteClient DROP CONSTRAINT UQ__MacComme__9EE8FAAD35FE8BDA
GO
---- trouver la contrainte d'unicite ----
ALTER TABLE MacSchema.MacCommercialClass_EnseigneClient DROP CONSTRAINT UQ__MacComme__7E29BE873CAB8969
GO


---- v1.3.0 ----
/*
	ERP-3402Faber : FB130415-14 : TermesDePaiements : Phase I
*/
CREATE TABLE MacSchema.PaymentMode(
	id UNIQUEIDENTIFIER NOT NULL,
	code NVARCHAR(25) NOT NULL,
	upperCode NVARCHAR(25) NOT NULL
)
ALTER TABLE MacSchema.PaymentMode ADD CONSTRAINT paymentMod_pkey PRIMARY KEY NONCLUSTERED (id)
GO
ALTER TABLE MacSchema.PaymentMode ADD CONSTRAINT UQ_paymentMod_code UNIQUE NONCLUSTERED (code)
GO
ALTER TABLE MacSchema.PaymentMode ADD CONSTRAINT UQ_paymentMod_upperCod UNIQUE NONCLUSTERED (upperCode)
GO

CREATE TABLE MacSchema.PaymentModeDescription(
	id UNIQUEIDENTIFIER NOT NULL,
	languageCode NVARCHAR(255) NULL,
	value NVARCHAR(255) NULL,
	paymentMode_id UNIQUEIDENTIFIER NULL
)
GO
ALTER TABLE MacSchema.PaymentModeDescription ADD CONSTRAINT paymentModDesc_pkey PRIMARY KEY NONCLUSTERED (id)
GO
ALTER TABLE MacSchema.PaymentModeDescription ADD CONSTRAINT UQ_paymentModDesc_paymentMod_id_languageCod UNIQUE NONCLUSTERED (paymentMode_id, languageCode)
GO

CREATE TABLE MacSchema.MacTerm(
	id UNIQUEIDENTIFIER NOT NULL,
	paymentMode_id UNIQUEIDENTIFIER NULL
)
GO
ALTER TABLE MacSchema.MacTerm ADD CONSTRAINT macTer_pkey PRIMARY KEY NONCLUSTERED (id)
GO
ALTER TABLE MacSchema.MacTerm ADD CONSTRAINT macTer_id_fkey FOREIGN KEY (id) REFERENCES dbo.Term
GO
ALTER TABLE MacSchema.MacTerm ADD CONSTRAINT macTer_paymentMod_id_fkey FOREIGN KEY(paymentMode_id) REFERENCES MacSchema.PaymentMode(id)
GO

INSERT INTO macschema.MacTerm
SELECT term.id, null
FROM Term
GO

---- v1.5.0 ----

ALTER TABLE MacSchema.MacCustomer
	ADD paymentMode_id UNIQUEIDENTIFIER NULL
GO
ALTER TABLE MacSchema.MacCustomer ADD CONSTRAINT macCus_paymentMod_id_fkey FOREIGN KEY(paymentMode_id) REFERENCES MacSchema.PaymentMode(id)
GO

---- v1.6.0 ----

CREATE TABLE MacSchema.MacMaterialProduct(
	id UNIQUEIDENTIFIER NOT NULL,
	ancienCodeArticle NVARCHAR(100) NULL
)
GO
ALTER TABLE MacSchema.MacMaterialProduct ADD CONSTRAINT macMatPro_pkey PRIMARY KEY NONCLUSTERED (id)
GO
ALTER TABLE MacSchema.MacMaterialProduct ADD CONSTRAINT macMatPro_id_fkey FOREIGN KEY (id) REFERENCES dbo.MaterialProduct
GO
INSERT INTO MacSchema.MacMaterialProduct (id, ancienCodeArticle)
SELECT id, NULL FROM dbo.MaterialProduct
GO

ALTER TABLE MacSchema.MacCustomer ADD 
	MNT_ECHU NUMERIC(32, 10) NULL,
	MNT_ECHU_CUR NVARCHAR(255) NULL,
	MNT_ECHU_TR_CUR NVARCHAR(255) NULL,
	MNT_ECHU_EX_RATE NUMERIC(19, 10) NULL,
	
	MNT_ENCOUR_30J NUMERIC(32, 10) NULL,
	MNT_ENCOUR_30J_CUR NVARCHAR(255) NULL,
	MNT_ENCOUR_30J_TR_CUR NVARCHAR(255) NULL,
	MNT_ENCOUR_30J_EX_RATE NUMERIC(19, 10) NULL,
	
	MNT_ENCOUR_60J NUMERIC(32, 10) NULL,
	MNT_ENCOUR_60J_CUR NVARCHAR(255) NULL,
	MNT_ENCOUR_60J_TR_CUR NVARCHAR(255) NULL,
	MNT_ENCOUR_60J_EX_RATE NUMERIC(19, 10) NULL,
	
	MNT_ENCOUR_90J NUMERIC(32, 10) NULL,
	MNT_ENCOUR_90J_CUR NVARCHAR(255) NULL,
	MNT_ENCOUR_90J_TR_CUR NVARCHAR(255) NULL,
	MNT_ENCOUR_90J_EX_RATE NUMERIC(19, 10) NULL,
	
	MNT_ENCOUR_SUP_90J NUMERIC(32, 10) NULL,
	MNT_ENCOUR_SUP_90J_CUR NVARCHAR(255) NULL,
	MNT_ENCOUR_SUP_90J_TR_CUR NVARCHAR(255) NULL,
	MNT_ENCOUR_SUP_90J_EX_RATE NUMERIC(19, 10) NULL
	
GO

ALTER TABLE MacSchema.MacCustomer
	ADD ENCOUR_DATE_TRANSFER DATETIME NULL
GO

---- v1.7.0 ----

CREATE TABLE MacSchema.MacQuotation(
	id UNIQUEIDENTIFIER NOT NULL
)
GO

ALTER TABLE MacSchema.MacQuotation ADD CONSTRAINT macQuo_pkey PRIMARY KEY NONCLUSTERED (id)
GO

ALTER TABLE MacSchema.MacQuotation ADD CONSTRAINT macQuo_id_fkey FOREIGN KEY (id) REFERENCES dbo.Quotation
GO

INSERT INTO macschema.MacQuotation
SELECT Quotation.id
FROM Quotation
GO

CREATE TABLE MacSchema.MacOrder(
	id UNIQUEIDENTIFIER NOT NULL
)
GO

ALTER TABLE MacSchema.MacOrder ADD CONSTRAINT macOrd_pkey PRIMARY KEY NONCLUSTERED (id)
GO

ALTER TABLE MacSchema.MacOrder ADD CONSTRAINT macOrd_id_fkey FOREIGN KEY (id) REFERENCES dbo.Orders
GO

INSERT INTO macschema.MacOrder
SELECT Orders.id
FROM Orders
GO

create table MacSchema.MacReport (arc tinyint null, id uniqueIdentifier not null, primary key (id))
GO

create table MacSchema.MacWorkflowARC (id uniqueIdentifier not null, approvalDelay smallint not null check (approvalDelay>=0), arcFilePath nvarchar(255) null, arcSentStatus_id uniqueIdentifier null, initialDelayStatus_id uniqueIdentifier null, primary key (id))
GO

create table MacSchema.MacWorkflowMaster (id uniqueIdentifier not null, sysAdmin_id uniqueIdentifier null, wfARC_id uniqueIdentifier null, wfM5P_id uniqueIdentifier null, primary key (id))
GO

create table MacSchema.MacWorkflowM5P (id uniqueIdentifier not null, active tinyint null, emailAddressBE nvarchar(255) not null, blockedStatus_id uniqueIdentifier not null, blockingReason_id uniqueIdentifier not null, m5pStatus_id uniqueIdentifier not null, refusedStatus_id uniqueIdentifier not null, unblockedStatus_id uniqueIdentifier not null, unblockingReason_id uniqueIdentifier not null, primary key (id));
GO

alter table MacSchema.MacReport add constraint macRep_id_fkey foreign key (id) references Report
GO

alter table MacSchema.MacWorkflowARC add constraint macWorARC_arcSenSta_id_fkey foreign key (arcSentStatus_id) references DocumentStatus
GO

alter table MacSchema.MacWorkflowARC add constraint macWorARC_iniDelSta_id_fkey foreign key (initialDelayStatus_id) references DocumentStatus
GO

alter table MacSchema.MacWorkflowMaster add constraint macWorMas_wfARC_id_fkey foreign key (wfARC_id) references MacSchema.MacWorkflowARC
GO

alter table MacSchema.MacWorkflowMaster add constraint macWorMas_sysAdm_id foreign key (sysAdmin_id) references Users
GO

CREATE TABLE MacSchema.DeclencheurEnCours 
  ( 
     id              UNIQUEIDENTIFIER NOT NULL, 
     entityClassName NVARCHAR(255) NOT NULL, 
     entityId        UNIQUEIDENTIFIER NULL, 
     PRIMARY KEY (id) 
  ) 
GO 

alter table MacSchema.MacWorkflowMaster add constraint macWorMas_sysAdm_id_fkey foreign key (sysAdmin_id) references Users
GO

ALTER TABLE MacSchema.MacWorkflowARC ADD active tinyint
GO

UPDATE MacSchema.MacWorkflowARC SET active = 1
GO

UPDATE MacSchema.MacWorkflowM5P SET active = 1
GO

ALTER TABLE MacSchema.MacWorkflowARC ALTER COLUMN initialDelayStatus_id uniqueidentifier NOT NULL
GO

ALTER TABLE MacSchema.MacWorkflowARC ALTER COLUMN arcSentStatus_id uniqueidentifier NOT NULL
GO

ALTER TABLE MacSchema.MacWorkflowARC ALTER COLUMN arcFilePath nvarchar(255) NOT NULL
GO

ALTER TABLE MacSchema.MacWorkflowM5P ALTER COLUMN m5pStatus_id uniqueidentifier NOT NULL
GO

ALTER TABLE MacSchema.MacWorkflowM5P ALTER COLUMN blockedStatus_id uniqueidentifier NOT NULL
GO

ALTER TABLE MacSchema.MacWorkflowM5P ALTER COLUMN emailAddressBE nvarchar(255) NOT NULL
GO

ALTER TABLE MacSchema.MacWorkflowM5P ALTER COLUMN blockingReason_id uniqueidentifier NOT NULL
GO

ALTER TABLE MacSchema.MacWorkflowM5P ALTER COLUMN unblockingReason_id uniqueidentifier NOT NULL
GO

ALTER TABLE MacSchema.MacWorkflowM5P ALTER COLUMN refusedStatus_id uniqueidentifier NOT NULL
GO

ALTER TABLE MacSchema.MacWorkflowM5P ALTER COLUMN unblockedStatus_id uniqueidentifier NOT NULL
GO

ALTER TABLE MacSchema.MacWorkflowARC ADD arcRefusedStatus_id uniqueidentifier not NULL
GO

alter table MacSchema.MacWorkflowARC add constraint macWorARC_arcRefSta_id_fkey foreign key (arcRefusedStatus_id) references DocumentStatus
GO

/*
	ERP-3644 - *** LE SCRIPT ERP DOIT ETRE EXECUTE AVANT ***
*/
INSERT INTO dbo.PaymentMode SELECT * FROM MacSchema.PaymentMode
GO

INSERT INTO dbo.PaymentModeDescription SELECT * FROM MacSchema.PaymentModeDescription
GO

ALTER TABLE  MacSchema.MacTerm DROP CONSTRAINT macTer_paymentMod_id_fkey 
GO

ALTER TABLE MacSchema.MacTerm ADD CONSTRAINT macTer_paymentMod_id_fkey FOREIGN KEY(paymentMode_id) REFERENCES dbo.PaymentMode(id)
GO

ALTER TABLE MacSchema.MacCustomer DROP CONSTRAINT macCus_paymentMod_id_fkey
GO

DROP TABLE  MacSchema.PaymentModeDescription
GO

DROP TABLE MacSchema.PaymentMode
GO

UPDATE dbo.CommercialEntity SET paymentMode_id = 
MacSchema.MacCustomer.paymentMode_id FROM MacSchema.MacCustomer
INNER JOIN dbo.CommercialEntity ON dbo.CommercialEntity.id = MacSchema.MacCustomer.id
GO

ALTER TABLE MacSchema.MacCustomer DROP COLUMN paymentMode_id
GO

---- v1.8.0 ----

ALTER TABLE MacSchema.MacWorkflowMaster ADD wfUnblockAuto_id uniqueIdentifier
GO

create table MacSchema.MacWorkflowUnblockAuto (id uniqueIdentifier not null, active tinyint null, unblockedStatus_id uniqueIdentifier not null, unblockingReason_id uniqueIdentifier not null, primary key (id));
GO

alter table MacSchema.MacWorkflowUnblockAuto add constraint macWorUnbAut_unblockingReason_id_fkey foreign key (unblockingReason_id) references BlockingReason;
GO

alter table MacSchema.MacWorkflowMaster add constraint macWorMas_wfUnblockAuto_id_fkey foreign key (wfUnblockAuto_id) references MacSchema.MacWorkflowUnblockAuto;
GO

alter table MacSchema.MacWorkflowMaster add userWF_id uniqueIdentifier 
GO

alter table MacSchema.MacWorkflowMaster add constraint macWorMas_userWF_id_fkey foreign key (userWF_id) references Users;
GO

insert into MacSchema.MacReport 
	select 0, report.id from Report where report.id not in (select id from MacSchema.MacReport)
GO

---- v1.9.0 ----

ALTER TABLE MacSchema.MacWorkflowARC ADD emailOrigin nvarchar(255) not null DEFAULT('')
GO

ALTER TABLE MacSchema.MacWorkflowARC ADD arcFilePathLocal nvarchar(255) not null DEFAULT('')
GO

ALTER TABLE MacSchema.MacWorkflowMaster ADD wfBlockAuto_id uniqueIdentifier
GO

create table MacSchema.MacWorkflowBlockAuto (id uniqueIdentifier not null, active tinyint null, blockedStatus_id uniqueIdentifier not null, blockingReason_id uniqueIdentifier not null, primary key (id));
GO

alter table MacSchema.MacWorkflowBlockAuto add constraint macWorBloAut_blockingReason_id_fkey foreign key (blockingReason_id) references BlockingReason;
GO

alter table MacSchema.MacWorkflowMaster add constraint macWorMas_wfblockAuto_id_fkey foreign key (wfBlockAuto_id) references MacSchema.MacWorkflowBlockAuto;
GO

ALTER TABLE MacSchema.MacWorkflowMaster ADD wfUnblockManual_id uniqueIdentifier
GO

create table MacSchema.MacWorkflowUnblockManual (id uniqueIdentifier not null, active tinyint null, unblockedStatus_id uniqueIdentifier not null, primary key (id));
GO

alter table MacSchema.MacWorkflowMaster add constraint macWorMas_wfunblockManual_id_fkey foreign key (wfunblockManual_id) references MacSchema.MacWorkflowUnblockManual;
GO

ALTER TABLE MacSchema.MacWorkflowBlockAuto ADD emailOrigin nvarchar(255) not null DEFAULT('')
GO

---- v1.10.0 ----

/*
** ERP-3609
*/
CREATE TABLE MacSchema.CostCode
(
	id UNIQUEIDENTIFIER NOT NULL,
	code NVARCHAR(25) NOT NULL, 
	upperCode NVARCHAR(25) NOT NULL,
	company_id UNIQUEIDENTIFIER NOT NULL
)
GO	

ALTER TABLE [MacSchema].[CostCode] ADD CONSTRAINT [PK__CostCode_Id] PRIMARY KEY CLUSTERED ([id])
GO
ALTER TABLE [MacSchema].[CostCode] ADD CONSTRAINT [UQ_CostCode_Code] UNIQUE (code);
GO
ALTER TABLE [MacSchema].[CostCode] ADD CONSTRAINT [UQ_CostCode_UpperCode] UNIQUE (upperCode);
GO
ALTER TABLE [MacSchema].[CostCode] ADD CONSTRAINT [FKC__CostCode_Company] FOREIGN KEY ([company_id]) REFERENCES [dbo].[company] ([id])
GO
	
CREATE TABLE MacSchema.CostCodeDescription (
	id UNIQUEIDENTIFIER NOT NULL,
	languageCode nvarchar(255) NULL,
	value NVARCHAR(255) NULL,
	costCode_id UNIQUEIDENTIFIER NULL
)
GO

ALTER TABLE [MacSchema].[CostCodeDescription] ADD CONSTRAINT [PK__CostCodeDescription] PRIMARY KEY CLUSTERED ([id])
GO
ALTER TABLE [MacSchema].[CostCodeDescription] ADD CONSTRAINT [UQ__CostCodeDescriptionCostCodeId] UNIQUE NONCLUSTERED ([costCode_id], [languageCode])
GO
ALTER TABLE [MacSchema].[CostCodeDescription] ADD CONSTRAINT [FKC__CostCodeDescription_CostCode] FOREIGN KEY ([costCode_id]) REFERENCES [MacSchema].[CostCode] ([id])
GO
CREATE INDEX costCode_description_x on MacSchema.CostCodeDescription (costCode_id);
GO

CREATE TABLE [MacSchema].[CostCodeDetail] (
	id UNIQUEIDENTIFIER NOT NULL,
	code NVARCHAR(25) NOT NULL, 
	upperCode NVARCHAR(25) NOT NULL,
	company_id UNIQUEIDENTIFIER NOT NULL
)

ALTER TABLE [MacSchema].[CostCodeDetail] ADD CONSTRAINT [PK__CostCodeDetail_Id] PRIMARY KEY CLUSTERED ([id])
GO
ALTER TABLE [MacSchema].[CostCodeDetail] ADD CONSTRAINT [UQ_CostCodeDetail_Code] UNIQUE (code);
GO
ALTER TABLE [MacSchema].[CostCodeDetail] ADD CONSTRAINT [UQ_CostCodeDetail_UpperCode] UNIQUE (upperCode);
GO
ALTER TABLE [MacSchema].[CostCodeDetail] ADD CONSTRAINT [FKC__CostCodeDetail_Company] FOREIGN KEY ([company_id]) REFERENCES [dbo].[company] ([id])
GO

CREATE TABLE [MacSchema].[CostCodeDetailDescription] (
	id UNIQUEIDENTIFIER NOT NULL,
	languageCode nvarchar(255) NULL,
	value NVARCHAR(255) NULL,
	costCodeDetail_id UNIQUEIDENTIFIER NULL
)

ALTER TABLE [MacSchema].[CostCodeDetailDescription] ADD CONSTRAINT [PK__CostCodeDetailDescription] PRIMARY KEY CLUSTERED ([id])
GO
ALTER TABLE [MacSchema].[CostCodeDetailDescription] ADD CONSTRAINT [UQ__CostCodeDetailDescriptionCostCodeDetailId] UNIQUE NONCLUSTERED ([costCodeDetail_id], [languageCode])
GO
ALTER TABLE [MacSchema].[CostCodeDetailDescription] ADD CONSTRAINT [FKC__CostCodeDetailDescription_CostCodeDetail] FOREIGN KEY ([costCodeDetail_id]) REFERENCES [MacSchema].[CostCodeDetail] ([id])
GO
CREATE INDEX costCodeDetail_description_x on MacSchema.CostCodeDetailDescription (costCodeDetail_id);
GO

----------------------------
CREATE TABLE MacSchema.ProductCodeAnalytic
(
	id UNIQUEIDENTIFIER NOT NULL,
	code NVARCHAR(25) NOT NULL, 
	upperCode NVARCHAR(25) NOT NULL,
	company_id UNIQUEIDENTIFIER NOT NULL
)
GO	

ALTER TABLE [MacSchema].[ProductCodeAnalytic] ADD CONSTRAINT [PK__ProductCodeAnalytic_Id] PRIMARY KEY CLUSTERED ([id])
GO
ALTER TABLE [MacSchema].[ProductCodeAnalytic] ADD CONSTRAINT [UQ_ProductCodeAnalytic_Code] UNIQUE (code);
GO
ALTER TABLE [MacSchema].[ProductCodeAnalytic] ADD CONSTRAINT [UQ_ProductCodeAnalytic_UpperCode] UNIQUE (upperCode);
GO
ALTER TABLE [MacSchema].[ProductCodeAnalytic] ADD CONSTRAINT [FKC__ProductCodeAnalytic_Company] FOREIGN KEY ([company_id]) REFERENCES [dbo].[company] ([id])
GO

CREATE TABLE [MacSchema].[ProductCodeAnalyticDescription] (
	id UNIQUEIDENTIFIER NOT NULL,
	languageCode nvarchar(255) NULL,
	value NVARCHAR(255) NULL,
	productCodeAnalytic_id UNIQUEIDENTIFIER NULL
)

ALTER TABLE [MacSchema].[ProductCodeAnalyticDescription] ADD CONSTRAINT [PK__ProductCodeAnalyticDescription] PRIMARY KEY CLUSTERED ([id])
GO
ALTER TABLE [MacSchema].[ProductCodeAnalyticDescription] ADD CONSTRAINT [UQ__ProductCodeAnalyticDescriptionProductCodeAnalyticId] UNIQUE NONCLUSTERED ([productCodeAnalytic_id], [languageCode])
GO
ALTER TABLE [MacSchema].[ProductCodeAnalyticDescription] ADD CONSTRAINT [FKC__ProductCodeAnalyticDescription_ProductCodeAnalytic] FOREIGN KEY ([productCodeAnalytic_id]) REFERENCES [MacSchema].[ProductCodeAnalytic] ([id])
GO
CREATE INDEX productCodeAnalytic_description_x on MacSchema.productCodeAnalyticDescription (productCodeAnalytic_id);
GO

CREATE TABLE MacSchema.MacManufacturedProduct(
	id UNIQUEIDENTIFIER NOT NULL,
)
GO
ALTER TABLE MacSchema.MacManufacturedProduct ADD CONSTRAINT macManuPro_pkey PRIMARY KEY NONCLUSTERED (id)
GO
ALTER TABLE MacSchema.MacManufacturedProduct ADD CONSTRAINT macManuPro_id_fkey FOREIGN KEY (id) REFERENCES dbo.ManufacturedProduct
GO

INSERT INTO MacSchema.MacManufacturedProduct (id)
SELECT id FROM dbo.ManufacturedProduct
GO

CREATE TABLE [MacSchema].[MaterialProductManagementAccounting] (
	id UNIQUEIDENTIFIER NOT NULL,
	sequence int NULL,
	company_id UNIQUEIDENTIFIER NOT NULL,
	costCodeSale_id UNIQUEIDENTIFIER NULL,
	costCodePurchase_id UNIQUEIDENTIFIER NULL,
	costCodeDetailSale_id UNIQUEIDENTIFIER NULL,
	costCodeDetailPurchase_id UNIQUEIDENTIFIER NULL,
	productCodeAnalyticSale_id UNIQUEIDENTIFIER NULL,
	productCodeAnalyticPurchase_id UNIQUEIDENTIFIER NULL,
	macMaterialProduct_id UNIQUEIDENTIFIER NULL
)

ALTER TABLE [MacSchema].[MaterialProductManagementAccounting] ADD CONSTRAINT [PK__MateManagementAccounting_Id] PRIMARY KEY CLUSTERED ([id])
GO
ALTER TABLE [MacSchema].[MaterialProductManagementAccounting] ADD CONSTRAINT [FKC__MateManagementAccounting_Company] FOREIGN KEY ([company_id]) REFERENCES [dbo].[company] ([id])
GO
ALTER TABLE [MacSchema].[MaterialProductManagementAccounting] ADD CONSTRAINT [FKC__MateManagementAccounting_costCode_Sale] FOREIGN KEY (costCodeSale_id) REFERENCES [MacSchema].[costCode] ([id])
GO
ALTER TABLE [MacSchema].[MaterialProductManagementAccounting] ADD CONSTRAINT [FKC__MateManagementAccounting_costCode_Purchase] FOREIGN KEY (costCodePurchase_id) REFERENCES [MacSchema].[costCode] ([id])
GO
ALTER TABLE [MacSchema].[MaterialProductManagementAccounting] ADD CONSTRAINT [FKC__MateManagementAccounting_costCodeDetail_Sale] FOREIGN KEY (costCodeDetailSale_id) REFERENCES [MacSchema].[costCodeDetail] ([id])
GO
ALTER TABLE [MacSchema].[MaterialProductManagementAccounting] ADD CONSTRAINT [FKC__MateManagementAccounting_costCodeDetail_Purchase] FOREIGN KEY (costCodeDetailPurchase_id) REFERENCES [MacSchema].[costCodeDetail] ([id])
GO
ALTER TABLE [MacSchema].[MaterialProductManagementAccounting] ADD CONSTRAINT [FKC__MateManagementAccounting_productCodeAnalytic_Sale] FOREIGN KEY (productCodeAnalyticSale_id) REFERENCES [MacSchema].[productCodeAnalytic] ([id])
GO
ALTER TABLE [MacSchema].[MaterialProductManagementAccounting] ADD CONSTRAINT [FKC__MateManagementAccounting_productCodeAnalytic_Purchase] FOREIGN KEY (productCodeAnalyticPurchase_id) REFERENCES [MacSchema].[productCodeAnalytic] ([id])
GO
ALTER TABLE [MacSchema].[MaterialProductManagementAccounting] ADD CONSTRAINT [FKC__MateManagementAccounting_InventoryProduct] FOREIGN KEY (macMaterialProduct_id) REFERENCES [MacSchema].[MacMaterialProduct] ([id])
GO


CREATE TABLE [MacSchema].[ManufacturedProductManagementAccounting] (
	id UNIQUEIDENTIFIER NOT NULL,
	sequence int NULL,
	company_id UNIQUEIDENTIFIER NOT NULL,
	costCodeSale_id UNIQUEIDENTIFIER NULL,
	costCodePurchase_id UNIQUEIDENTIFIER NULL,
	costCodeDetailSale_id UNIQUEIDENTIFIER NULL,
	costCodeDetailPurchase_id UNIQUEIDENTIFIER NULL,
	productCodeAnalyticSale_id UNIQUEIDENTIFIER NULL,
	productCodeAnalyticPurchase_id UNIQUEIDENTIFIER NULL,
	macManufacturedProduct_id UNIQUEIDENTIFIER NULL
)

ALTER TABLE [MacSchema].[ManufacturedProductManagementAccounting] ADD CONSTRAINT [PK__ManuManagementAccounting_Id] PRIMARY KEY CLUSTERED ([id])
GO
ALTER TABLE [MacSchema].[ManufacturedProductManagementAccounting] ADD CONSTRAINT [FKC__ManuManagementAccounting_Company] FOREIGN KEY ([company_id]) REFERENCES [dbo].[company] ([id])
GO
ALTER TABLE [MacSchema].[ManufacturedProductManagementAccounting] ADD CONSTRAINT [FKC__ManuManagementAccounting_costCode_Sale] FOREIGN KEY (costCodeSale_id) REFERENCES [MacSchema].[costCode] ([id])
GO
ALTER TABLE [MacSchema].[ManufacturedProductManagementAccounting] ADD CONSTRAINT [FKC__ManuManagementAccounting_costCode_Purchase] FOREIGN KEY (costCodePurchase_id) REFERENCES [MacSchema].[costCode] ([id])
GO
ALTER TABLE [MacSchema].[ManufacturedProductManagementAccounting] ADD CONSTRAINT [FKC__ManuManagementAccounting_costCodeDetail_Sale] FOREIGN KEY (costCodeDetailSale_id) REFERENCES [MacSchema].[costCodeDetail] ([id])
GO
ALTER TABLE [MacSchema].[ManufacturedProductManagementAccounting] ADD CONSTRAINT [FKC__ManuManagementAccounting_costCodeDetail_Purchase] FOREIGN KEY (costCodeDetailPurchase_id) REFERENCES [MacSchema].[costCodeDetail] ([id])
GO
ALTER TABLE [MacSchema].[ManufacturedProductManagementAccounting] ADD CONSTRAINT [FKC__ManuManagementAccounting_productCodeAnalytic_Sale] FOREIGN KEY (productCodeAnalyticSale_id) REFERENCES [MacSchema].[productCodeAnalytic] ([id])
GO
ALTER TABLE [MacSchema].[ManufacturedProductManagementAccounting] ADD CONSTRAINT [FKC__ManuManagementAccounting_productCodeAnalytic_Purchase] FOREIGN KEY (productCodeAnalyticPurchase_id) REFERENCES [MacSchema].[productCodeAnalytic] ([id])
GO
ALTER TABLE [MacSchema].[ManufacturedProductManagementAccounting] ADD CONSTRAINT [FKC__ManuManagementAccounting_InventoryProduct] FOREIGN KEY (macManufacturedProduct_id) REFERENCES [MacSchema].[MacManufacturedProduct] ([id])
GO

---- MACPersonnalisation v1.11.0 ----
/*
** ERP-3852
*/

ALTER TABLE MacSchema.MacOrder
	ADD enablePrintCommentAndNoteOnShipping tinyint NOT NULL DEFAULT 0,
	enablePrintCommentAndNoteOnPackage tinyint NOT NULL DEFAULT 0,
	enablePrintCommentAndNoteOnSaleInvoice tinyint NOT NULL DEFAULT 0,
	enablePrintCommentAndNoteOnWorkOrder tinyint NOT NULL DEFAULT 0
GO

/*
** ERP-3845
*/
CREATE TABLE MacSchema.MacOrderDetail
(
	ID UNIQUEIDENTIFIER NOT NULL
)
GO	

ALTER TABLE [MacSchema].[MacOrderDetail] ADD CONSTRAINT [macOrderDetail_pkey] PRIMARY KEY CLUSTERED ([id])
GO

INSERT INTO MacSchema.MacOrderDetail
SELECT ID FROM EuropeCustomSchema.OrderDetailEuro
GO

---- MACPersonnalisation v1.12.0 ----

--Workflow validation finance
create table MacSchema.MacWorkflowFinanceValidation (id uniqueIdentifier not null, active tinyint null, emailCompta nvarchar(255) not null, emailOrigin nvarchar(255) not null, primary key (id))
GO

ALTER TABLE MacSchema.MacWorkflowMaster ADD wfFinanceValidation_id uniqueIdentifier
GO

alter table MacSchema.MacWorkflowMaster add constraint macWorMas_wfFinanceValidation_id foreign key (wfFinanceValidation_id) references MacSchema.MacWorkflowFinanceValidation
GO

--Workflow validation client
create table MacSchema.MacWorkflowClientValidation (id uniqueIdentifier not null, active tinyint null, emailOrigin nvarchar(255) not null, report_id uniqueIdentifier not null, primary key (id))
GO

ALTER TABLE MacSchema.MacWorkflowMaster ADD wfClientValidation_id uniqueIdentifier
GO

alter table MacSchema.MacWorkflowMaster add constraint macWorMas_wfClientValidation_id foreign key (wfClientValidation_id) references MacSchema.MacWorkflowClientValidation
GO

alter table MacSchema.MacWorkflowClientValidation add constraint macWorCli_report_id foreign key (report_id) references Report
GO

ALTER TABLE MacSchema.MacWorkflowClientValidation ADD filePath nvarchar(255) not null DEFAULT('')
GO

ALTER TABLE MacSchema.MacWorkflowClientValidation ADD filePathLocal nvarchar(255) not null DEFAULT('')
GO

---- MACPersonnalisation v1.13.0 ----
 
--	AJUSTEMENT
INSERT INTO MacSchema.MacOrder (id) 
SELECT EuropeCustomSchema.OrderEuro.id
FROM EuropeCustomSchema.OrderEuro
LEFT JOIN MacSchema.MacOrder ON MacSchema.MacOrder.id = EuropeCustomSchema.OrderEuro.id
WHERE MacSchema.MacOrder.id IS NULL
GO

INSERT INTO MacSchema.MacOrderDetail (id) 
SELECT EuropeCustomSchema.OrderDetailEuro.id
FROM EuropeCustomSchema.OrderDetailEuro
LEFT JOIN MacSchema.MacOrderDetail ON MacSchema.MacOrderDetail.id = EuropeCustomSchema.OrderDetailEuro.id
WHERE MacSchema.MacOrderDetail.id IS NULL
GO

INSERT INTO MacSchema.MacQuotation (id) 
SELECT EuropeCustomSchema.QuotationEuro.id
FROM EuropeCustomSchema.QuotationEuro
LEFT JOIN MacSchema.MacQuotation ON MacSchema.MacQuotation.id = EuropeCustomSchema.QuotationEuro.id
WHERE MacSchema.MacQuotation.id IS NULL
GO

CREATE TABLE MacSchema.FeeAccounting 
  ( 
     id     UNIQUEIDENTIFIER NOT NULL, 
     fee_id UNIQUEIDENTIFIER NULL 
  )
GO
ALTER TABLE MacSchema.FeeAccounting ADD CONSTRAINT feeAcc_pkey PRIMARY KEY NONCLUSTERED (id)
GO
ALTER TABLE MacSchema.FeeAccounting ADD CONSTRAINT feeAcc_fee_id UNIQUE (fee_id);
GO
ALTER TABLE MacSchema.FeeAccounting ADD CONSTRAINT feeAcc_fee_id_fkey FOREIGN KEY (fee_id) REFERENCES Fee (id)
GO

CREATE TABLE MacSchema.FeeCodeAnalytic 
  ( 
     id         UNIQUEIDENTIFIER NOT NULL, 
     code       NVARCHAR(25) NOT NULL, 
     upperCode  NVARCHAR(25) NOT NULL, 
     company_id UNIQUEIDENTIFIER NOT NULL 
  )
GO	

ALTER TABLE MacSchema.FeeCodeAnalytic ADD CONSTRAINT feeCodAna_id_pkey PRIMARY KEY NONCLUSTERED (id)
GO
ALTER TABLE MacSchema.FeeCodeAnalytic ADD CONSTRAINT feeCodAna_code_ukey UNIQUE (code)
GO
ALTER TABLE MacSchema.FeeCodeAnalytic ADD CONSTRAINT feeCodAna_upperCod_uq UNIQUE (upperCode)
GO
ALTER TABLE MacSchema.FeeCodeAnalytic ADD CONSTRAINT feeCodAna_company_Id_fkey FOREIGN KEY (company_id) REFERENCES Company (id)
GO

CREATE TABLE MacSchema.FeeCodeAnalyticDescription 
  ( 
     id                 UNIQUEIDENTIFIER NOT NULL, 
     languageCode       NVARCHAR(255) NULL, 
     value              NVARCHAR(255) NULL, 
     feeCodeAnalytic_id UNIQUEIDENTIFIER NULL 
  )
GO
ALTER TABLE MacSchema.FeeCodeAnalyticDescription ADD CONSTRAINT feeCodAnaDes_id_pkey PRIMARY KEY NONCLUSTERED (id)
GO
ALTER TABLE MacSchema.FeeCodeAnalyticDescription ADD CONSTRAINT feeCodAnaDes_feeCodAna_LanCod_ukey UNIQUE NONCLUSTERED (feeCodeAnalytic_id, languageCode)
GO
ALTER TABLE MacSchema.FeeCodeAnalyticDescription ADD CONSTRAINT feeCodAnaDes_feeCodeAnalytic_Id_fkey FOREIGN KEY (feeCodeAnalytic_id) REFERENCES MacSchema.FeeCodeAnalytic (id)
GO
CREATE INDEX feeCodAna_des_x on MacSchema.feeCodeAnalyticDescription (feeCodeAnalytic_id)
GO

CREATE TABLE MacSchema.FeeManagementAccounting 
  ( 
     id                         UNIQUEIDENTIFIER NOT NULL, 
     company_id                 UNIQUEIDENTIFIER NOT NULL, 
     costCodeSale_id            UNIQUEIDENTIFIER NULL, 
     costCodePurchase_id        UNIQUEIDENTIFIER NULL, 
     costCodeDetailSale_id      UNIQUEIDENTIFIER NULL, 
     costCodeDetailPurchase_id  UNIQUEIDENTIFIER NULL, 
     feeCodeAnalyticSale_id     UNIQUEIDENTIFIER NULL, 
     feeCodeAnalyticPurchase_id UNIQUEIDENTIFIER NULL 
  )
GO
ALTER TABLE MacSchema.FeeManagementAccounting ADD CONSTRAINT feeManAcc_id_pkey PRIMARY KEY NONCLUSTERED (id)
GO
ALTER TABLE MacSchema.FeeManagementAccounting ADD CONSTRAINT feeManAcc_company_Id_fkey FOREIGN KEY (company_id) REFERENCES Company (id)
GO
ALTER TABLE MacSchema.FeeManagementAccounting ADD CONSTRAINT feeManAcc_costCodSal_id_fkey FOREIGN KEY (costCodeSale_id) REFERENCES MacSchema.costCode (id)
GO
ALTER TABLE MacSchema.FeeManagementAccounting ADD CONSTRAINT feeManAcc_costCodPur_id_fkey FOREIGN KEY (costCodePurchase_id) REFERENCES MacSchema.costCode (id)
GO
ALTER TABLE MacSchema.FeeManagementAccounting ADD CONSTRAINT feeManAcc_costCodDetSal_id_fkey FOREIGN KEY (costCodeDetailSale_id) REFERENCES MacSchema.costCodeDetail (id)
GO
ALTER TABLE MacSchema.FeeManagementAccounting ADD CONSTRAINT feeManAcc_costCodDetPur_id_fkey FOREIGN KEY (costCodeDetailPurchase_id) REFERENCES MacSchema.costCodeDetail (id)
GO
ALTER TABLE MacSchema.FeeManagementAccounting ADD CONSTRAINT feeManAcc_feeCodAnaSal_id_fkey FOREIGN KEY (feeCodeAnalyticSale_id) REFERENCES MacSchema.FeeCodeAnalytic (id)
GO
ALTER TABLE MacSchema.FeeManagementAccounting ADD CONSTRAINT feeManAcc_feeCodAnaPur_id_fkey FOREIGN KEY (feeCodeAnalyticPurchase_id) REFERENCES MacSchema.FeeCodeAnalytic (id)
GO

CREATE TABLE MacSchema.FeeAccounting_FeeManagementAccounting 
  ( 
     feeManagementAccountings_id UNIQUEIDENTIFIER NOT NULL, 
     feeAccounting_id            UNIQUEIDENTIFIER NULL, 
     sequence                    INT NULL 
  )
GO
ALTER TABLE MacSchema.FeeAccounting_FeeManagementAccounting ADD CONSTRAINT feeAcc_feeManAcc_feeManAcc_pkey PRIMARY KEY NONCLUSTERED (feeManagementAccountings_id)
GO
ALTER TABLE MacSchema.FeeAccounting_FeeManagementAccounting ADD CONSTRAINT feeAcc_feeManAcc_feeAcc_id_fkey FOREIGN KEY (feeAccounting_id) REFERENCES MacSchema.FeeAccounting (id)
GO

---- MACPersonnalisation v1.14.0 ----

/*
** ERP-3929
*/
CREATE TABLE MacSchema.MacQuotationDetail
(
	ID UNIQUEIDENTIFIER NOT NULL
)
GO	

ALTER TABLE MacSchema.MacQuotationDetail ADD CONSTRAINT macQuotationDetail_pkey PRIMARY KEY NONCLUSTERED (id)
GO

INSERT INTO MacSchema.MacQuotationDetail
SELECT ID FROM EuropeCustomSchema.QuotationDetailEuro
GO

---- MACPersonnalisation v1.15.0 ----

CREATE TABLE macschema.WorkLoad 
  ( 
     id          UNIQUEIDENTIFIER NOT NULL, 
     date        DATETIME NOT NULL, 
     resource_id UNIQUEIDENTIFIER NOT NULL 
  ) 
GO
ALTER TABLE MacSchema.WorkLoad ADD CONSTRAINT workLoa_pkey PRIMARY KEY NONCLUSTERED (id) 
GO
ALTER TABLE MacSchema.WorkLoad ADD CONSTRAINT workLoa_res_resource_id_fkey FOREIGN KEY (resource_id) REFERENCES Resource (id) 
GO
ALTER TABLE MacSchema.WorkLoad ADD CONSTRAINT workLoa_date_resource_id_uq UNIQUE NONCLUSTERED (date, resource_id) 
GO
CREATE TABLE MacSchema.WorkLoadDetail 
  ( 
     id                       UNIQUEIDENTIFIER NOT NULL, 
     workLoad_id              UNIQUEIDENTIFIER, 
     orderDetail_id           UNIQUEIDENTIFIER NOT NULL, 
     quantityConvertedValue   NUMERIC(32, 10), 
     quantityValue            NUMERIC(32, 10), 
     quantityConversion_id    UNIQUEIDENTIFIER, 
     quantityConvertedUnit_id UNIQUEIDENTIFIER, 
     quantityGroupMeasure_id  UNIQUEIDENTIFIER 
  )
GO
ALTER TABLE MacSchema.WorkLoadDetail ADD CONSTRAINT workLoaDet_pkey PRIMARY KEY NONCLUSTERED (id) 
GO
CREATE INDEX workload_details_x ON MacSchema.workLoadDetail (workLoad_id) 
GO
ALTER TABLE MacSchema.WorkLoadDetail ADD CONSTRAINT workLoaDet_quantityGroMea_id_fkey FOREIGN KEY (quantityGroupMeasure_id) REFERENCES GroupMeasure (id) 
GO 
ALTER TABLE MacSchema.WorkLoadDetail ADD CONSTRAINT workLoaDet_orderDet_id_fkey FOREIGN KEY (orderDetail_id) REFERENCES macschema.MacOrderDetail (id); 
GO 
ALTER TABLE MacSchema.WorkLoadDetail ADD CONSTRAINT workLoaDet_workLoad_id_fkey FOREIGN KEY (workLoad_id) REFERENCES macschema.WorkLoad (id); 
GO 
ALTER TABLE MacSchema.WorkLoadDetail ADD CONSTRAINT workLoaDet_quantityCon_id_fkey FOREIGN KEY (quantityConversion_id) REFERENCES MeasureUnitConversion (id)
GO
ALTER TABLE MacSchema.WorkLoadDetail ADD CONSTRAINT workLoaDet_quantityConUni_id_fkey FOREIGN KEY (quantityConvertedUnit_id) REFERENCES MeasureUnitMultiplier (id) 
GO
CREATE INDEX orderdetail_workloaddetails_x ON macschema.workloaddetail (orderDetail_id) 
GO 
CREATE TABLE MacSchema.MacSetup 
  ( 
     id UNIQUEIDENTIFIER NOT NULL 
  )
GO
ALTER TABLE MacSchema.MacSetup ADD CONSTRAINT macSet_pkey PRIMARY KEY NONCLUSTERED (id) 
GO
ALTER TABLE MacSchema.MacSetup ADD CONSTRAINT macSet_id_fkey FOREIGN KEY (id) REFERENCES Setup (id) 
GO
INSERT INTO MacSchema.MacSetup (id) SELECT id FROM Setup
GO
CREATE TABLE MacSchema.MacCompany 
  ( 
     id                              UNIQUEIDENTIFIER NOT NULL, 
     workUnitAvailableMeasureUnit_id UNIQUEIDENTIFIER 
  )
GO
ALTER TABLE MacSchema.MacCompany ADD CONSTRAINT macCom_pkey PRIMARY KEY NONCLUSTERED (id) 
GO
ALTER TABLE MacSchema.MacCompany ADD CONSTRAINT macCom_id_fkey FOREIGN KEY (id) REFERENCES Company (id) 
GO 
ALTER TABLE MacSchema.MacCompany ADD CONSTRAINT macCom_workUniAvaMeaUni_id_fkey FOREIGN KEY (workUnitAvailableMeasureUnit_id) REFERENCES AvailableMeasureUnit 
GO 
INSERT INTO MacSchema.MacCompany (id) SELECT id FROM Company
GO

---- MACPersonnalisation v1.16.0 ----

--- Modification MacWorkflowBlockAuto
ALTER TABLE MacSchema.MacWorkflowBlockAuto ADD initialStatus_id uniqueIdentifier 
GO
ALTER TABLE MacSchema.MacWorkflowBlockAuto ADD CONSTRAINT macWorBlo_initialStatus_id FOREIGN KEY (initialStatus_id) REFERENCES DocumentStatus;


---- MACPersonnalisation v1.17.0 ----

--- Creation des MacPackages
CREATE TABLE macschema.MacPackages
(
id UNIQUEIDENTIFIER NOT NULL,
codeMac NVARCHAR(25) NULL
)
GO
ALTER TABLE MacSchema.MacPackages ADD CONSTRAINT macPac_pkey PRIMARY KEY NONCLUSTERED (id)
GO

ALTER TABLE MacSchema.MacPackages ADD CONSTRAINT macPac_id_fkey FOREIGN KEY(id) REFERENCES Packages (id)
GO

--- ADJUSTMENT des MacPackages
INSERT INTO macschema.MacPackages
SELECT packages.id,
codemac
FROM (SELECT order_id AS idOrder,
number
+ RIGHT('00000'+ CONVERT(VARCHAR, Count(*)), 5) AS codeMac
FROM packages,
document
WHERE order_id = dbo.Document.id
GROUP BY order_id,
number) AS codeMacByOrder,
packages
WHERE dbo.Packages.order_id = codeMacByOrder.idorder
GO

---- MACPersonnalisation v1.18.0 ----

ALTER TABLE MacSchema.MacQuotation ADD [serviceType_id] [uniqueidentifier] NULL;
GO

ALTER TABLE MacSchema.MacQuotation ADD CONSTRAINT macQuo_SerTyp_id_fkey FOREIGN KEY(serviceType_id) REFERENCES ServiceType (id)
GO

ALTER TABLE MacSchema.MacQuotation ADD 	[originalOrderNumber] [nvarchar](30) NULL;
GO

ALTER TABLE MacSchema.MacOrder ADD [serviceType_id] [uniqueidentifier] NULL;
GO

ALTER TABLE MacSchema.MacOrder ADD CONSTRAINT macOrd_SerTyp_id_fkey FOREIGN KEY(serviceType_id) REFERENCES ServiceType (id)
GO

ALTER TABLE MacSchema.MacOrder ADD 	[originalOrderNumber] [nvarchar](30) NULL;
GO

ALTER TABLE MacSchema.MacWorkflowARC DROP CONSTRAINT macWorARC_arcRefSta_id_fkey
GO

ALTER TABLE MacSchema.MacWorkflowARC DROP COLUMN arcRefusedStatus_id
GO

---- MACPersonnalisation v 1.19.0 ----

-- Table MacSaleInvoice
CREATE TABLE MacSchema.MacSaleInvoice 
  ( 
     id                       UNIQUEIDENTIFIER NOT NULL, 
     serviceType_id           UNIQUEIDENTIFIER NULL, 
     originalOrderNumber      nvarchar(30) NULL
  );
GO
ALTER TABLE MacSchema.MacSaleInvoice ADD CONSTRAINT macSal_pkey PRIMARY KEY NONCLUSTERED (id) 
GO

ALTER TABLE MacSchema.MacSaleInvoice ADD CONSTRAINT macSal_SerTyp_id_fkey FOREIGN KEY(serviceType_id) REFERENCES ServiceType (id);
GO

INSERT INTO MacSchema.MacSaleInvoice ( id, serviceType_id, originalOrderNumber ) SELECT id,NULL,NULL FROM SaleInvoice;
GO

-- Table MacReceivingDetail
CREATE TABLE MacSchema.MacReceivingDetail 
  ( 
     id                       UNIQUEIDENTIFIER NOT NULL
  );
GO
ALTER TABLE MacSchema.MacReceivingDetail ADD CONSTRAINT macRec_pkey PRIMARY KEY NONCLUSTERED (id) 
GO

INSERT into MacSchema.MacReceivingDetail SELECT id FROM ReceivingDetail
GO

CREATE TABLE MacUtils.PropertyGroup 
(
	id                       UNIQUEIDENTIFIER NOT NULL, 
	groupe					 NVARCHAR(50),
	code					 NVARCHAR(50),
	ordre					 INT,
	PRIMARY KEY (id)
)
GO

---- MACPersonnalisation v 1.20.0 ----
ALTER TABLE MacSchema.MacCompany ADD defaultMaterialProductManufacturingDelay INT
GO
ALTER TABLE MacSchema.MacCompany ADD defaultMaterialProductSupplyingDelay INT
GO
ALTER TABLE MacSchema.MacCompany ADD defaultMaterialProductWorkUnit NUMERIC(32, 10)
GO
ALTER TABLE MacSchema.MacCompany ADD defaultMaterialProductWorkingStation_id UNIQUEIDENTIFIER
GO
ALTER TABLE MacSchema.MacCompany ADD CONSTRAINT macCom_defaultMatProWorSta_id_fkey FOREIGN KEY (defaultMaterialProductWorkingStation_id) REFERENCES WorkingStation (id)
GO

INSERT INTO EuropeCustomSchema.PostponedReason (id, code) VALUES (NEWID(), 'UO')
GO

INSERT INTO EuropeCustomSchema.PostponedReasonDescription (id, languageCode, value, postponedReason_id)
	SELECT NEWID(), 'fr', 'Calcul automatique de la date', id FROM EuropeCustomSchema.PostponedReason WHERE code = 'UO'
GO

CREATE TABLE MacSchema.MacBlockingReason
(
   id uniqueIdentifier NOT NULL,
   excludeFormOrdersInProductionTotal TINYINT NULL
)
GO

ALTER TABLE MacSchema.MacBlockingReason ADD CONSTRAINT macBloRea_pkey PRIMARY KEY NONCLUSTERED (id) 
GO

ALTER TABLE MacSchema.MacBlockingReason ADD CONSTRAINT macBloRea_id_fkey FOREIGN KEY (id) REFERENCES BlockingReason
GO

INSERT INTO MacSchema.MacBlockingReason (id, excludeFormOrdersInProductionTotal) SELECT id, 0 FROM BlockingReason
GO	

------- v1.21.0 --------

ALTER TABLE MacSchema.WorkLoadDetail ADD quantityProducedConvertedValue NUMERIC(32, 10) NULL
GO

ALTER TABLE MacSchema.WorkLoadDetail ADD quantityProducedValue NUMERIC(32, 10) NULL
GO

ALTER TABLE MacSchema.WorkLoadDetail ADD quantityProducedConversion_id UNIQUEIDENTIFIER NULL
GO

ALTER TABLE MacSchema.WorkLoadDetail ADD quantityProducedConvertedUnit_id UNIQUEIDENTIFIER NULL
GO

ALTER TABLE MacSchema.WorkLoadDetail ADD quantityProducedGroupMeasure_id UNIQUEIDENTIFIER NULL
GO

ALTER TABLE MacSchema.WorkLoadDetail ADD CONSTRAINT workLoaDet_quantityProGroMea_id_fkey FOREIGN KEY (quantityGroupMeasure_id) REFERENCES GroupMeasure (id) 
GO 

ALTER TABLE MacSchema.WorkLoadDetail ADD CONSTRAINT workLoaDet_quantityProCon_id_fkey FOREIGN KEY (quantityConversion_id) REFERENCES MeasureUnitConversion (id)
GO

ALTER TABLE MacSchema.WorkLoadDetail ADD CONSTRAINT workLoaDet_quantityProConUni_id_fkey FOREIGN KEY (quantityConvertedUnit_id) REFERENCES MeasureUnitMultiplier (id) 
GO

------- v1.22.0 --------
-- Fermeture des lots dont tous les WorkOrderRoutingStep sont dans le panier de production
SELECT * FROM productionbatch
UPDATE ProductionBatch SET closed = 1 
WHERE id NOT IN 
( 
SELECT DISTINCT ProductionBatch.id 
FROM	WorkOrderRoutingStep LEFT JOIN 
		ProductionBatch ON productionbatch.id = workorderroutingstep.productionBatch_id LEFT JOIN 
		ProductionBasket ON productionbasket.workOrderRoutingStep_id = WorkOrderRoutingStep.id INNER JOIN 
		WorkOrder ON workOrder.id = WorkOrderRoutingStep.workOrder_id INNER JOIN 
		Document ON WorkOrder.explodableDocument_id = document.id INNER JOIN 
		DocumentStatus ON documentstatus.id = workOrder.documentStatus_id 
WHERE ProductionBatch.id IS NOT NULL AND DocumentStatus.internalId NOT IN(3,6) AND ProductionBasket.id IS NULL)
GO

------- v1.23.0 --------
-- flag de test d'envoi du devis aux commerciaux
ALTER TABLE MacSchema.MacQuotation ADD sentToCommercial tinyint NOT NULL DEFAULT 0
GO

------- v1.24.0 --------
CREATE TABLE MacSchema.MacDocumentStatus(
	id UNIQUEIDENTIFIER NOT NULL,
	allowWorkLoadModifications TINYINT NOT NULL
)
GO
ALTER TABLE MacSchema.MacDocumentStatus ADD CONSTRAINT macDocSta_pkey PRIMARY KEY NONCLUSTERED (id)
GO
ALTER TABLE MacSchema.MacDocumentStatus ADD CONSTRAINT macDocSta_id_fkey FOREIGN KEY(id) REFERENCES EuropeCustomSchema.DocumentStatusEuro(id)
GO
INSERT INTO MacSchema.MacDocumentStatus SELECT id, 1 FROM EuropeCustomSchema.DocumentStatusEuro
GO

-- create and set firstCommunicatedShippingDate
ALTER TABLE MacSchema.MacOrder ADD firstCommunicatedShippingDate datetime NULL
GO
UPDATE MacSchema.MacOrder SET firstCommunicatedShippingDate = StatusHistory.date
FROM MacSchema.MacOrder
INNER JOIN Document ON MacOrder.id = Document.id
INNER JOIN StatusHistory ON StatusHistory.id =
	(SELECT TOP 1 StatusHistory.id FROM StatusHistory
	INNER JOIN DocumentStatus ON StatusHistory.documentStatus_id = DocumentStatus.id AND DocumentStatus.upperCode = 'ARCE'
	WHERE document_id = MacOrder.id
	ORDER BY date)

------- v1.25.0 --------
CREATE TABLE MacSchema.MacPurchaseOrder
(
	id UNIQUEIDENTIFIER NOT NULL,
	shippingAddress_id UNIQUEIDENTIFIER,
)
GO
ALTER TABLE MacSchema.MacPurchaseOrder ADD CONSTRAINT macPurOrd_pkey PRIMARY KEY NONCLUSTERED (id)
GO
ALTER TABLE MacSchema.MacPurchaseOrder ADD CONSTRAINT macPurOrd_id_fkey FOREIGN KEY (id) REFERENCES PurchaseOrder (id)
GO
ALTER TABLE MacSchema.MacPurchaseOrder ADD CONSTRAINT macPurOrder_shiAdd_id_fkey FOREIGN KEY (shippingAddress_id) REFERENCES Address (id)
GO
INSERT INTO MacSchema.MacPurchaseOrder SELECT id, null FROM PurchaseOrder
GO

------- v1.26.0 --------

-- Change approval delay from hours to days
UPDATE MacSchema.MacWorkflowARC SET approvalDelay = CEILING(approvalDelay/24.0)
GO

CREATE TABLE MacSchema.MacWorkflowARCProductDelay
(
	id uniqueIdentifier NOT null,
	approvalDelay smallint NOT null ,
	product_id uniqueIdentifier NOT null,
	property_id uniqueIdentifier NOT null,
	propertyValue_id uniqueIdentifier NOT null,
	workflowarc_id uniqueIdentifier null,
	sequence int null, PRIMARY KEY (id),
	CONSTRAINT UQ_MacWorARCProDel_product_id_property_id_propertyVal_id UNIQUE (product_id, property_id, propertyValue_id),
	CONSTRAINT CK_MacWorkflowARCProductDelay_approvalDelay CHECK (approvalDelay>=0)
)
CREATE INDEX approval_product_delays_x on MacSchema.MacWorkflowARCProductDelay (workflowarc_id);
ALTER TABLE MacSchema.MacWorkflowARCProductDelay ADD CONSTRAINT macWorARCProDel_propertyVal_id_fkey FOREIGN KEY (propertyValue_id) REFERENCES PropertyValue;
ALTER TABLE MacSchema.MacWorkflowARCProductDelay ADD CONSTRAINT macWorARCProDel_property_id_fkey FOREIGN KEY (property_id) REFERENCES Property;
ALTER TABLE MacSchema.MacWorkflowARCProductDelay ADD CONSTRAINT macWorARCProDel_workflowarc_id_fkey FOREIGN KEY (workflowarc_id) REFERENCES MacSchema.MacWorkflowARC;
ALTER TABLE MacSchema.MacWorkflowARCProductDelay ADD CONSTRAINT macWorARCProDel_product_id_fkey FOREIGN KEY (product_id) REFERENCES ManufacturedProduct;
GO

ALTER TABLE MacSchema.MacWorkflowARC ADD workShift_id uniqueIdentifier null;
ALTER TABLE MacSchema.MacWorkflowARC ADD CONSTRAINT macWorARC_workShift_id_fkey FOREIGN KEY (workShift_id) REFERENCES WorkShift;
GO

------- v1.27.0 --------
UPDATE MacSchema.MacWorkflowARC SET approvalDelay = approvalDelay*8
UPDATE MacSchema.MacWorkflowARCProductDelay SET approvalDelay = approvalDelay*8
GO

CREATE TABLE MacSchema.MacLocation
(
	id UNIQUEIDENTIFIER NOT NULL,
	store TINYINT NOT NULL DEFAULT 0
)
ALTER TABLE MacSchema.MacLocation ADD CONSTRAINT macLoc_pkey PRIMARY KEY NONCLUSTERED (id)
ALTER TABLE MacSchema.MacLocation ADD CONSTRAINT macLoc_id_fkey FOREIGN KEY (id) REFERENCES Location (id)
INSERT INTO MacSchema.MacLocation SELECT id, 0 FROM Location
GO

--Fix for a bad script copy-paste creating the column with NOT NULL even if the original script doesn't 
ALTER TABLE MacSchema.MacWorkflowARCProductDelay ALTER COLUMN workflowarc_id uniqueidentifier NULL
GO

UPDATE Orders SET initialReceptionDate = Document.date FROM Orders, Document WHERE Orders.id = Document.id
GO

ALTER TABLE MacSchema.MacDocumentStatus ADD validateBalance tinyint NULL default 0;
GO
UPDATE MacSchema.MacDocumentStatus set validateBalance = 0
GO

ALTER TABLE MacSchema.MacWorkflowBlockAuto ADD blockingReasonProforma_id uniqueIdentifier null;
GO

UPDATE MacSchema.MacWorkflowBlockAuto SET blockingReasonProforma_id = blockingReason_id;
GO

ALTER TABLE MacSchema.MacWorkflowBlockAuto ALTER COLUMN blockingReasonProforma_id uniqueIdentifier not null;
GO

ALTER TABLE MacSchema.MacWorkflowBlockAuto ADD CONSTRAINT macWorBloAut_blockingReasonProforma_id_fkey FOREIGN KEY (blockingReasonProforma_id) REFERENCES BlockingReason;
GO

------- v1.28.0 --------
CREATE TABLE MacSchema.MacStocktakingDetail
(
	id UNIQUEIDENTIFIER NOT NULL,
	CONSTRAINT macStoDet_pkey PRIMARY KEY NONCLUSTERED (id),
	CONSTRAINT macStoDet_id_fkey FOREIGN KEY (id) REFERENCES StocktakingDetail (id)
)
INSERT INTO MacSchema.MacStocktakingDetail SELECT id FROM StocktakingDetail
GO

-- modifying the default deposit date to the future to ensure it is always valid
UPDATE Deposit SET date = '2500-01-01' WHERE code = 'NA'

--- Ajout de deux nouveaux paramÃ¨tres sur la sociÃ©tÃ© : Type d'ajustement de stock et Localisation

ALTER TABLE [MacSchema].[MacCompany] ADD defaultInventoryAdjustmentType_id [uniqueidentifier] NULL;
GO
ALTER TABLE [MacSchema].[MacCompany] ADD defaultInventoryAdjustmentLocation_id [uniqueidentifier] NULL;
GO
ALTER TABLE [MacSchema].[MacCompany]  WITH CHECK ADD  CONSTRAINT [macCom_defInvAdjTyp_id_fkey] FOREIGN KEY([defaultInventoryAdjustmentType_id])
REFERENCES [dbo].[InventoryAdjustmentType] ([id]);
GO
ALTER TABLE [MacSchema].[MacCompany]  WITH CHECK ADD  CONSTRAINT [macCom_defInvAdjLoc_id_fkey] FOREIGN KEY([defaultInventoryAdjustmentLocation_id])
REFERENCES [dbo].[Location] ([id]);
GO

------- v1.29.0 - To be released --------

CREATE TABLE MacSchema.MacWarehouse
(
	id uniqueidentifier NOT NULL,
	CONSTRAINT macWar_pkey PRIMARY KEY (id ASC),
	CONSTRAINT macWar_id_fkey FOREIGN KEY(id) REFERENCES Warehouse (id)
)
GO

INSERT INTO MacSchema.MacWarehouse (id) SELECT id FROM Warehouse
GO

CREATE TABLE MacSchema.MacShippingLocation(
	id uniqueidentifier NOT NULL,
	warehouse_id uniqueidentifier NULL,
	location_id uniqueidentifier NOT NULL,
	sequence int NULL,
	CONSTRAINT macShiLoc_pkey PRIMARY KEY (id ASC),
	CONSTRAINT macShiLoc_location_id_fkey FOREIGN KEY(location_id) REFERENCES MacSchema.MacLocation (id),
	CONSTRAINT macShiLoc_warehouse_id_fkey FOREIGN KEY(warehouse_id) REFERENCES MacSchema.MacWarehouse (id)
)
GO

------- v1.30.0 --------

--Bad primary key 
ALTER TABLE MacSchema.FeeAccounting_FeeManagementAccounting DROP CONSTRAINT feeAcc_feeManAcc_feeManAcc_pkey
GO
ALTER TABLE MacSchema.FeeAccounting_FeeManagementAccounting ADD CONSTRAINT FeeAccFeeManAcc_feeManAcc_id_uq UNIQUE NONCLUSTERED (feeManagementAccountings_id)
GO
------ Expédition partielle avec colis vides -------


ALTER TABLE MacSchema.MacPackages DROP COLUMN codeMac
GO

ALTER TABLE MacSchema.MacPackages ADD shipping_id UNIQUEIDENTIFIER
GO

ALTER TABLE MacSchema.MacPackages ADD CONSTRAINT macPac_shipping_id_fkey FOREIGN KEY (shipping_id) REFERENCES Shipping (id)
GO

DELETE FROM MacSchema.MacPackages
GO

INSERT INTO MacSchema.MacPackages SELECT id, null FROM Packages
GO
