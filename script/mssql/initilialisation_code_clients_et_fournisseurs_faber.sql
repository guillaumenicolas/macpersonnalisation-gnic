-- Ce script sql sert � initialiser les codes de clients existants pour une soci�t� dont les codes sont automatiquement g�n�r�s  
-- Remplacer les chaines de caract�res "PREFIX-" et "-SUFFIX" par les vrais pr�fix et suffixes initialis�s dans la fiche soci�t�
-- Une fois l'initialisation compl�t�, il est important d'aller changer le num�ro s�quentiel dans la fiche soci�t� par le prochain num�ro � attribuer

UPDATE CommercialEntity SET code = ('PREFIX-' + 
    CONVERT(
    VARCHAR(255),
    (SELECT COUNT(PCOUNT.id)+1
    FROM CommercialEntity AS PCOUNT
    WHERE PCOUNT.id < CommercialEntity.id AND PCOUNT.company_id = CommercialEntity.company_id)
    ) + '-SUFFIX')
FROM CommercialEntity
INNER JOIN Customer ON CommercialEntity.id = Customer.id

-- Ce script sql sert � initialiser les codes de fournisseurs existants pour une soci�t� dont les codes sont automatiquement g�n�r�s  
-- Remplacer les chaines de caract�res "PREFIX-" et "-SUFFIX" par les vrais pr�fix et suffixes initialis�s dans la fiche soci�t�
-- Une fois l'initialisation compl�t�, il est important d'aller changer le num�ro s�quentiel dans la fiche soci�t� par le prochain num�ro � attribuer

UPDATE CommercialEntity SET code = ('PREFIX-' + 
    CONVERT(
    VARCHAR(255),
    (SELECT COUNT(PCOUNT.id)+1
    FROM CommercialEntity AS PCOUNT
    WHERE PCOUNT.id < CommercialEntity.id AND PCOUNT.company_id = CommercialEntity.company_id)
    ) + '-SUFFIX')
FROM CommercialEntity
INNER JOIN Supplier ON CommercialEntity.id = Supplier.id
