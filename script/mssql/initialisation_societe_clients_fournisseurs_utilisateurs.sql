-- Ce script sert � initialiser la valeur du champ compagnie dans les utilisateurs avec la soci�t� Faber (code FA)
-- Noter que la soci�t� Faber (code FA) doit exister dans la base de donn�es, autrement l'initialisation �chouera

update Users set company_id = (select id from company where code = 'FA')


-- Ce script sert � initialiser la valeur du champ compagnie dans les clients et fournisseurs avec la soci�t� Faber (code FA)
-- Noter que la soci�t� Faber (code FA) doit exister dans la base de donn�es, autrement l'initialisation �chouera

update CommercialEntity set company_id = (select id from company where code = 'FA')
